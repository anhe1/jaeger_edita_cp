﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.CP.Builder {
    public interface IEmpleadoGridViewBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IEmpleadoTempleteGridViewBuilder Templetes();
    }

    public interface IEmpleadoTempleteGridViewBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IEmpleadoTempleteGridViewBuilder Master();
    }

    public interface IEmpleadoColumnsGridViewBuilder : IGridViewColumnsBuild {
        IEmpleadoColumnsGridViewBuilder Activo();
        IEmpleadoColumnsGridViewBuilder IdEmpleado();
        IEmpleadoColumnsGridViewBuilder Clave();
        IEmpleadoColumnsGridViewBuilder ApellidoPaterno();
        IEmpleadoColumnsGridViewBuilder ApellidoMaterno();
        IEmpleadoColumnsGridViewBuilder Nombre();
        IEmpleadoColumnsGridViewBuilder RFC();
        IEmpleadoColumnsGridViewBuilder CURP();
        IEmpleadoColumnsGridViewBuilder Depatamento();
        IEmpleadoColumnsGridViewBuilder Puesto();
        IEmpleadoColumnsGridViewBuilder SalarioDiario();
        IEmpleadoColumnsGridViewBuilder SalarioDiarioIntegrado();
        IEmpleadoColumnsGridViewBuilder JornadasTrabajo();
        IEmpleadoColumnsGridViewBuilder Correo();
        IEmpleadoColumnsGridViewBuilder FechaNuevo();
        IEmpleadoColumnsGridViewBuilder Creo();
        IEmpleadoColumnsGridViewBuilder Modifica();
        IEmpleadoColumnsGridViewBuilder FechaModifica();
    }
}
