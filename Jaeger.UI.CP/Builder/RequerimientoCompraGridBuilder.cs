﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.CP.Services {
    public class RequerimientoCompraGridBuilder : GridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IRequerimientoCompraGridBuilder, IRequerimientoCompraTempleteGridBuilder,
        IRequerimientoCompraColumnsGridBuilder {

        public RequerimientoCompraGridBuilder() : base() { }

        public IRequerimientoCompraTempleteGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }
        public IRequerimientoCompraTempleteGridBuilder Master() {
            this.Version().Serie().Folio().Status().ComboDepartamento().FechaEmision().Receptor().Contacto().Tipo().
                Creo().GranTotal().FechaAutoriza().Autoriza().Adquisiciones().FechaAdquisiciones();
            return this;
        }

        public IRequerimientoCompraTempleteGridBuilder GetConceptos() {
            this.Cantidad().Unidad().IdOrden().Descripcion().Marca().Especificacion().Nota().Unitario().SubTotal().Descuento().Importe();
            return this;
        }

        #region informacion del comprobante
        public IRequerimientoCompraColumnsGridBuilder ComboDepartamento() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdDepartamento",
                HeaderText = "Departamento",
                Name = "IdDepartamento",
                Width = 150
            });
            return this;
        }
        public IRequerimientoCompraColumnsGridBuilder Version() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Version",
                HeaderText = "Ver.",
                IsPinned = true,
                Name = "Version",
                Width = 50,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }
        public IRequerimientoCompraColumnsGridBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                FormatString = FormarStringFolio,
                ReadOnly = true
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Status() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(string),
                FieldName = "IdStatus",
                HeaderText = "Status",
                Name = "IdStatus",
                Width = 75,
                DisplayMember = "Descriptor",
                ValueMember = "Id",
                ReadOnly = true
            });
            return this;
        }
        public IRequerimientoCompraColumnsGridBuilder Serie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// obtener columna del numero de orden PEDPRD o cliente
        /// </summary>
        public IRequerimientoCompraColumnsGridBuilder ColNumOrden() {
            this._Columns.Add(new GridViewTextBoxColumn {
                HeaderText = "No. Orden",
                Name = "IdPedido",
                FieldName = "IdPedido",
                Width = 75
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Contacto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                HeaderText = "Contacto",
                Name = "Contacto",
                FieldName = "Contacto",
                Width = 175
            });
            return this;
        }

        /// <summary>
        /// prioridad
        /// </summary>
        public IRequerimientoCompraColumnsGridBuilder Tipo() {
            this._Columns.Add(new GridViewComboBoxColumn {
                HeaderText = "Prioridad",
                Name = "IdTipo",
                FieldName = "IdTipo",
                Width = 100
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder ColFechaAcuerdo() {
            this._Columns.Add(new GridViewDateTimeColumn {
                HeaderText = "Fec. \r\nRequerida",
                Name = "FechaAcuerdo",
                FieldName = "FechaAcuerdo",
                FormatString = GridTelerikCommon.FormatStringDate,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder ColTelefono() {
            this._Columns.Add(new GridViewTextBoxColumn {
                HeaderText = "Teléfono",
                FieldName = "Telefono",
                Name = "Telefono",
                Width = 100,
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Receptor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                HeaderText = "Cliente",
                IsPinned = true,
                FieldName = "ReceptorNombre",
                Name = "Cliente",
                Width = 280,
                ReadOnly = true
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder FechaEmision() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. \r\nEmisión",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Autoriza() {
            this._Columns.Add(new GridViewTextBoxColumn {
                HeaderText = "Autoriza",
                Name = "Autoriza",
                FieldName = "Autoriza",
                FormatString = GridTelerikCommon.FormatStringDate,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder FechaAutoriza() {
            this._Columns.Add(new GridViewDateTimeColumn {
                HeaderText = "Fec. \r\nAutoriza",
                Name = "FechaAutoriza",
                FieldName = "FechaAutoriza",
                FormatString = GridTelerikCommon.FormatStringDate,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Adquisiciones() {
            this._Columns.Add(new GridViewTextBoxColumn {
                HeaderText = "Recepción \r\n Adquisiciones",
                Name = "Adquisiciones",
                FieldName = "Adquisiciones",
                FormatString = GridTelerikCommon.FormatStringDate,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder FechaAdquisiciones() {
            this._Columns.Add(new GridViewDateTimeColumn {
                HeaderText = "Fec. \r\nAdquisiciones",
                Name = "FechaCompras",
                FieldName = "FechaCompras",
                FormatString = GridTelerikCommon.FormatStringDate,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }
        #endregion

        #region conceptos
        public IRequerimientoCompraColumnsGridBuilder IdOrden() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                Name = "IdOrden",
                HeaderText = "#Orden",
                FieldName = "IdOrden",
                FormatString = this.FormarStringFolio,
                Width = 65,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }
        public IRequerimientoCompraColumnsGridBuilder Unitario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Unitario",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Unitario",
                Name = "Unitario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }
        public IRequerimientoCompraColumnsGridBuilder Cantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Cantidad",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Cantidad",
                Name = "Cantidad",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }
        public IRequerimientoCompraColumnsGridBuilder Unidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 75
            });
            return this;
        }
        public IRequerimientoCompraColumnsGridBuilder ColProducto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                HeaderText = "Producto",
                FieldName = "Producto",
                Name = "Producto",
                Width = 340
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                HeaderText = "Descripción",
                FieldName = "Descripcion",
                Name = "Descripcion",
                Width = 340
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Marca() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                HeaderText = "Marca",
                FieldName = "Marca",
                Name = "Marca",
                Width = 180
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Especificacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                HeaderText = "Especificación",
                FieldName = "Especificacion",
                Name = "Especificacion",
                Width = 180
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Nota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                HeaderText = "Nota",
                FieldName = "Nota",
                Name = "Nota",
                Width = 140
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder SubTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                HeaderText = "SubTotal",
                FieldName = "SubTotal",
                Name = "SubTotal",
                Width = 75,
                FormatString = GridTelerikCommon.FormatStringMoney,
                TextAlignment = ContentAlignment.MiddleRight,
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Descuento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                HeaderText = "Descuento",
                FieldName = "Descuento",
                Name = "Descuento",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleRight,
                FormatString = GridTelerikCommon.FormatStringMoney
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                HeaderText = "Importe",
                FieldName = "Importe",
                Name = "Importe",
                Width = 75,
                FormatString = GridTelerikCommon.FormatStringMoney,
                TextAlignment = ContentAlignment.MiddleRight,
            });
            return this;
        }

        public IRequerimientoCompraColumnsGridBuilder GranTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                HeaderText = "Total",
                Name = "Total",
                FieldName = "Total",
                FormatString = GridTelerikCommon.FormatStringMoney,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }
        #endregion
    }
}
