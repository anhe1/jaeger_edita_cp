﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.CP.Services {
    public interface IRequerimientoCompraGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IRequerimientoCompraTempleteGridBuilder Templetes();
    }
    public interface IRequerimientoCompraTempleteGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IRequerimientoCompraTempleteGridBuilder Master();
        IRequerimientoCompraTempleteGridBuilder GetConceptos();
    }

    public interface IRequerimientoCompraColumnsGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IRequerimientoCompraColumnsGridBuilder Version();
        IRequerimientoCompraColumnsGridBuilder Serie();
        IRequerimientoCompraColumnsGridBuilder Folio();
        IRequerimientoCompraColumnsGridBuilder Status();
        IRequerimientoCompraColumnsGridBuilder ComboDepartamento();
        IRequerimientoCompraColumnsGridBuilder FechaEmision();
        IRequerimientoCompraColumnsGridBuilder Receptor();
        IRequerimientoCompraColumnsGridBuilder Contacto();
        IRequerimientoCompraColumnsGridBuilder Tipo();
        IRequerimientoCompraColumnsGridBuilder Creo();
        IRequerimientoCompraColumnsGridBuilder GranTotal();
        IRequerimientoCompraColumnsGridBuilder FechaAutoriza();
        IRequerimientoCompraColumnsGridBuilder Autoriza();
        IRequerimientoCompraColumnsGridBuilder Adquisiciones();
        IRequerimientoCompraColumnsGridBuilder FechaAdquisiciones();
        IRequerimientoCompraColumnsGridBuilder Cantidad();
        IRequerimientoCompraColumnsGridBuilder Unidad();
        IRequerimientoCompraColumnsGridBuilder IdOrden();
        IRequerimientoCompraColumnsGridBuilder Descripcion();
        IRequerimientoCompraColumnsGridBuilder Marca();
        IRequerimientoCompraColumnsGridBuilder Especificacion();
        IRequerimientoCompraColumnsGridBuilder Nota();
        IRequerimientoCompraColumnsGridBuilder Unitario();
        IRequerimientoCompraColumnsGridBuilder SubTotal();
        IRequerimientoCompraColumnsGridBuilder Descuento();
        IRequerimientoCompraColumnsGridBuilder Importe();
    }
}
