﻿using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CP.Services {
    public static class RemisionadoMenuItems {
        public static RadMenuItem RemisionCliente() {
            return new RadMenuItem { Text = "Crear remisión cliente", Name = "cpvnt_brem_historial" };
        }

        public static RadMenuItem RemisionInterna() {
            return new RadMenuItem { Text = "Crear Remisión (Interna)", Name = "cppro_brem_emitido" };
        }
    }
}
