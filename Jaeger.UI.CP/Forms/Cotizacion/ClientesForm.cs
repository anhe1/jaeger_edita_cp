﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Cotizacion {
    /// <summary>
    /// Clientes
    /// </summary>
    public class ClientesForm : Cotizador.Forms.Empresa.ContribuyentesForm {
        public ClientesForm(UIMenuElement menuElement) : base() {
            this.TContribuyente.MenuElement = menuElement;
            this.IdClase = 2;
            this.Load += this.ClientesForm_Load;
        }

        private void ClientesForm_Load(object sender, System.EventArgs e) {
            this.TContribuyente.ShowClase = false;
            this.Text = "Cotizaciones: Clientes";
            this._Service = new Aplication.Cotizador.Services.ContribuyentesService();
        }
    }
}
