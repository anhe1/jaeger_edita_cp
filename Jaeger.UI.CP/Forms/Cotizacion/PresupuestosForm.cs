﻿using System;

namespace Jaeger.UI.CP.Forms.Cotizacion {
    public class PresupuestosForm : Cotizador.Forms.Cotizacion.PresupuestosForm {
        public PresupuestosForm() : base() {
            this.Load += PresupuestosForm_Load;
        }

        private void PresupuestosForm_Load(object sender, EventArgs e) {
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }
    }
}
