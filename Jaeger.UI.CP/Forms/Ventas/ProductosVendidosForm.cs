﻿using System;
using System.Linq;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Almacen.PT.Builder;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Cotizador.Ventas.Entities;

namespace Jaeger.UI.CP.Forms.Ventas {
    /// <summary>
    /// Productos vendidos, hereda desde CP la informacion de la orden de produccion, cotizacion
    /// </summary>
    public class ProductosVendidosForm : Cotizador.Forms.Ventas.ProductoVendidoCatalogoForm {
        #region declaraciones
        protected internal Aplication.Almacen.PT.Contracts.IRemisionadoService _Remisionado;
        private BindingList<RemisionPartidaModel> dataRemisionado;
        #endregion

        public ProductosVendidosForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += ProductosVendidosForm_Load;
        }

        private void ProductosVendidosForm_Load(object sender, EventArgs e) {
            using (IRemisionadoGridBuilder view = new RemisionadoGridBuilder()) {
                this.gridTemplateRemision.Standard();
                this.gridTemplateRemision.Columns.AddRange(view.Templetes().RemisionYConceptosConImportes().Build());
            }
            this.gridTemplateRemision.HierarchyDataProvider = new GridViewEventDataProvider(this.gridTemplateRemision);
            this._Remisionado = new Aplication.CP.Service.RemisionadoService();
        }

        #region acciones del grid
        public override void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var rowview = e.ParentRow.DataBoundItem as ProductoVendidoModel;
            if (e.Template.Caption == this.gridTemplateRemision.Caption) {
                using (var espera = new Waiting1Form(this.GetRemisionado)) {
                    espera.Text = "Consultando remisiones ...";
                    espera.ShowDialog(this);
                }

                if (this.dataRemisionado != null) {
                    foreach (var item in this.dataRemisionado) {
                        var row = e.Template.Rows.NewRow();
                        e.SourceCollection.Add(RemisionadoGridBuilder.ConvertTo(row, item));
                    }
                }
            }
        }
        #endregion

        protected virtual void GetRemisionado() {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as ProductoVendidoModel;
                if (seleccionado != null) {
                    var query = Aplication.Almacen.PT.Services.RemisionadoService.Query().WithConceptos(seleccionado.IdOrden).Build();
                    this.dataRemisionado = new BindingList<RemisionPartidaModel>(this._Remisionado.GetList<RemisionPartidaModel>(query).ToList());
                    if (this.dataRemisionado == null) {
                        this.dataRemisionado = new BindingList<RemisionPartidaModel>();
                    }
                }
            }
        }
    }
}
