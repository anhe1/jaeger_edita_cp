﻿using System;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Ventas {
    /// <summary>
    /// catalogo de vendedores
    /// </summary>
    internal class VendedoresCatalogoForm : Cotizador.Forms.Ventas.VendedoresCatalogoForm {

        public VendedoresCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += VendedoresCatalogoForm_Load;
        }

        private void VendedoresCatalogoForm_Load(object sender, EventArgs e) {
            this.Service = new VendedorService();
        }
    }
}
