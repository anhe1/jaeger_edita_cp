﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Ventas {
    /// <summary>
    /// Formulario para listado de remisiones por cobrar al cliente
    /// </summary>
    public class RemisionadoPorCobrarForm : RemisionadoForm {
        public RemisionadoPorCobrarForm(UIMenuElement menuElement, IRemisionadoService service) : base(menuElement) {
            this.Text = "Remisionado: Por Cobrar";
            this.Load += this.RemisionadoPorCobrarForm_Load;
            this._Service = service;
        }

        private void RemisionadoPorCobrarForm_Load(object sender, EventArgs e) {
            this.TRemision.ShowPeriodo = false;
            this.TRemision.Herramientas.Items.Remove(this._PorCobrar);
        }

        public override void Actualizar() {
            // status 4 es por cobrar
            this._DataSource = new BindingList<RemisionSingleModel>(
                this._Service.GetList<RemisionSingleModel>(
                    Aplication.Almacen.PT.Services.RemisionadoService.Query().WithYear(this.TRemision.GetEjercicio()).WithIdStatus(Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Por_Cobrar).Build()).ToList()
                );
        }
    }
}
