﻿using System;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.CP.Forms.Almacen.PT;

namespace Jaeger.UI.CP.Forms.Ventas {
    /// <summary>
    /// Formulario para listado de remisiones fiscales al cliente
    /// </summary>
    public class RemisionadoForm : UI.Almacen.PT.Forms.RemisionadoForm {
        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = true;
            this.Load += this.RemisionesFiscalesForm_Load;
        }

        private void RemisionesFiscalesForm_Load(object sender, EventArgs e) {
            this.ShowValues = true;
            this.Text = "Ventas: Remisionado";
            this._Service = new RemisionadoService();
        }

        public override void TRemision_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new RemisionFiscalForm(this._Service) { MdiParent = ParentForm };
            _nuevo.Show();
        }

        public override void TRemision_PorCobrar_Click(object sender, EventArgs e) {
            var porcobrar = new RemisionadoPorCobrarForm(this._MenuElement, this._Service) { MdiParent = ParentForm };
            porcobrar.Show();
        }
    }
}
