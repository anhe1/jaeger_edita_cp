﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Ventas {
    /// <summary>
    /// formulario del reporte de ventas anual
    /// </summary>
    public class VentasReporte1Form : Cotizador.Forms.Ventas.VentasReporte1Form {
        public VentasReporte1Form(UIMenuElement menuElement) : base() { }
    }
}
