﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Cotizador.Ventas.Entities;

namespace Jaeger.UI.CP.Forms.Ventas {
    public class VentasReporte2Form : Cotizador.Forms.Ventas.VentasReporte2Form {
        public VentasReporte2Form(UIMenuElement menuElement) : base() {
            this.Load += VentasReporte2Form_Load;
        }

        private void VentasReporte2Form_Load(object sender, System.EventArgs e) {
            this._Service = new Aplication.Cotizador.Services.VentasService();
            this.IdVendedor.DataSource = this._Service.GetList<VendedorModel>(Aplication.Cotizador.Services.VentasService.Query().OnlyActive().Build());
        }
    }
}
