﻿using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Almacen.DP {
    public class RemisionadoPartidaForm : UI.Almacen.DP.Forms.RemisionadoPartidaForm {
        public RemisionadoPartidaForm() {
        }

        public RemisionadoPartidaForm(UIMenuElement menuElement) : base(menuElement) {
        }

        public override void OnLoad() {
            this.Service = new RemisionadoInternoService();
        }
    }
}
