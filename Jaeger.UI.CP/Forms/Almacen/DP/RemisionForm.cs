﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Almacen.DP.Builder;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Cotizador.Produccion.Entities;

namespace Jaeger.UI.CP.Forms.Almacen.DP {
    public partial class RemisionForm : RadForm {
        #region declaraciones
        protected internal RadContextMenu menuContextualConceptos = new RadContextMenu();
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem menuContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        protected RadMenuItem SinDuplicados = new RadMenuItem { Text = "Sin duplicados", CheckOnClick = true, IsChecked = true };
        #endregion

        #region constructor
        public RemisionForm(int indice) {
            InitializeComponent();
            this.Text = "DP: Remisión";
            this.ComprobanteControl.Indice = indice;
        }

        public RemisionForm(RemisionDetailModel comprobante, int indice) {
            InitializeComponent();
            this.Text = "DP: Remisión";
            this.ComprobanteControl.Comprobante = comprobante;
            this.ComprobanteControl.Indice = indice;
        }
        #endregion

        private void RemisionInternaForm_Load(object sender, EventArgs e) {
            this.OnStart.RunWorkerAsync();
            this.menuContextDuplicar.Image = Properties.Resources.copy_16px;
            this.menuContextualConceptos.Items.AddRange(this.menuContextAplicar, this.menuContextDuplicar);
            this.menuContextAplicar.Click += this.MenuContextAplicar_Click;
            this.menuContextDuplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Complementos.Text = "Herramientas";
            this.TConceptos.Complementos.Items.Add(this.SinDuplicados);
            this.GridConceptos.Columns.AddRange(new RemisionGridBuilder().Templetes().Conceptos().Build());
        }

        #region barra de herramientas conceptos del comprobante
        private void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            if (this.ComprobanteControl.Comprobante.Conceptos.Count == 0) {
                this.ComprobanteControl.Comprobante.Conceptos = new BindingList<RemisionConceptoDetailModel>();
                this.GridConceptos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                //    this.GridConceptoParte.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                //    this.GridConceptoImpuestos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                //    this.GridConceptoInformacionAduanera.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                //    this.GridConceptoParte.DataMember = "Parte";
                //    this.GridConceptoImpuestos.DataMember = "Impuestos";
                //    this.GridConceptoInformacionAduanera.DataMember = "InformacionAduanera";
            }

            this.ComprobanteControl.Comprobante.Conceptos.Add(new RemisionConceptoDetailModel { Activo = true });
        }

        public virtual void TConcepto_Remove_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_Remision_RemoverObjeto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as RemisionConceptoDetailModel;
                    if (seleccionado.IdMovimiento == 0) {
                        try {
                            this.ComprobanteControl.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.GridConceptos.Rows.Count == 0) {
                                this.ComprobanteControl.Comprobante.Conceptos = new BindingList<RemisionConceptoDetailModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.GridConceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void TConcepto_Duplicar_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as RemisionConceptoDetailModel;
                if (seleccionado != null) {
                    var duplicado = seleccionado.Clone();
                    this.ComprobanteControl.Comprobante.Conceptos.Add(duplicado);
                }
            }
        }

        public virtual void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.ComprobanteControl.Comprobante.IdCliente == 0) {
                RadMessageBox.Show(this, Properties.Resources.msg_Cliente_Seleccionar, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var _catalogoProductos = new Cotizador.Forms.Produccion.BuscarForm()) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.TConceptos_Agregar_Producto;
                _catalogoProductos.ShowDialog(this);
            }
        }

        public virtual void TConceptos_Agregar_Producto(object sender, OrdenProduccionDetailModel e) {
            if (this.ComprobanteControl.Comprobante.Conceptos.Count == 0) {
                this.ComprobanteControl.Comprobante.Conceptos = new BindingList<RemisionConceptoDetailModel>();
                this.GridConceptos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            }
            if (e != null) {
                var nuevo = new RemisionConceptoDetailModel {
                    Efecto = Domain.Almacen.DP.ValueObjects.ValeAlmPTTipoEnum.Egreso,
                    IdPedido = e.IdOrden,
                    Cantidad = e.Cantidad,
                    Unitario = e.ValorUnitario,
                    Activo = e.IsActive,
                    Producto = e.Nombre,
                    Cliente = e.Cliente,
                    IdCliente = e.IdDirectorio,
                    UnidadFactor = e.Unidad,
                    Unidad = e.Unidad.ToString(),
                    Identificador = string.Format("{0}-{1:000000}-{2:000000}-{3:000000}", 0, e.IdOrden, e.IdDirectorio, e.PEDPRD_COM_ID)
                };

                if (e.Componentes != null) {
                    if (e.Componentes.Count > 0) {
                        nuevo.IdComprobante = e.Componentes[0].IdComponente;
                        nuevo.Componente = e.Componentes[0].Nombre;
                    }
                }

                if (this.SinDuplicados.IsChecked == true) {
                    var ed = this.ComprobanteControl.Comprobante.Conceptos.Where(it => it.Identificador == nuevo.Identificador);
                    if (ed.Count() > 0) {
                        RadMessageBox.Show(this, "Properties.Resources.msg_Objeto_Dulicado", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                }

                this.ComprobanteControl.Comprobante.Conceptos.Add(nuevo);
            }
        }
        #endregion

        #region menu contextual
        public virtual void MenuContextAplicar_Click(object sender, EventArgs e) {
            var d = this.GridConceptos.CurrentCell.Value;
            foreach (var item in this.GridConceptos.Rows) {
                item.Cells[this.GridConceptos.CurrentColumn.Index].Value = d;
            }
        }
        #endregion

        #region acciones del grid
        public virtual void GridConceptos_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (this.ComprobanteControl.Comprobante.IsEditable == false) {
                return;
            }
            if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridConceptos.CurrentRow.ViewInfo.ViewTemplate == this.GridConceptos.MasterTemplate) {
                    this.menuContextAplicar.Enabled = this.GridConceptos.CurrentColumn.Name == "Unidad" |
                        this.GridConceptos.CurrentColumn.Name == "IdPedido" |
                        this.GridConceptos.CurrentColumn.Name == "Cantidad" |
                        this.GridConceptos.CurrentColumn.Name == "Componente" |
                        this.GridConceptos.CurrentColumn.Name == "Producto";
                }
                e.ContextMenu = this.menuContextualConceptos.DropDown;
            }
        }

        public virtual void GridConceptos_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {

        }

        public virtual void GridConceptos_CellValidating(object sender, CellValidatingEventArgs e) {
            if (e.Column.Name == "IdPedido") {
                var editor = e.ActiveEditor as RadTextBoxEditor;
                if (editor != null) {
                    var editorElement = editor.EditorElement as RadTextBoxEditorElement;
                    editorElement.KeyPress -= this.EditorElement_KeyPress;
                    editorElement.KeyPress += this.EditorElement_KeyPress;
                }
            }
        }

        private void EditorElement_KeyPress(object sender, KeyPressEventArgs e) {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else {
                char keyChar = e.KeyChar;
                e.Handled = !(keyChar.ToString() == char.ConvertFromUtf32(8));
            }
        }
        #endregion

        #region eventos
        public virtual void Comprobante_BindingCompleted(object sender, EventArgs e) {
            this.Text = "Remisión: " + this.ComprobanteControl.Text;
            this.TConceptos.IsEditable = this.ComprobanteControl.Comprobante.IsEditable;
            this.ToolBarConceptoParte.IsEditable = this.ComprobanteControl.Comprobante.IsEditable;

            this.GridConceptos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
            this.GridConceptos.AllowEditRow = this.ComprobanteControl.Comprobante.IsEditable;

            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "TotalDescuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIVA.DataBindings.Clear();
            this.TrasladoIVA.DataBindings.Add("Value", this.ComprobanteControl.Comprobante, "TotalTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.ComprobanteControl.Comprobante, "GTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            if (this.ComprobanteControl.Comprobante.Folio > 0) {
                this.Text = string.Format("DP Remisión {0}", this.ComprobanteControl.Comprobante.Folio.ToString("#000000"));
            }
        }
        #endregion

        #region OnLoad
        private void OnStart_DoWork(object sender, DoWorkEventArgs e) {
            // crear el servicio
            this.ComprobanteControl.Start();
            this.ComprobanteControl.General.IdSerie.Enabled = false;

            // cambios en el grid de conceptos
            this.GridConceptos.Columns["IdPedido"].ReadOnly = false;
            this.GridConceptos.Columns["Cliente"].ReadOnly = false;
            this.GridConceptos.Columns["Componente"].ReadOnly = false;
        }

        private void OnStart_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.ComprobanteControl.TComprobante.Cerrar.Click += this.TComprobante_Cerrar_Click;
            this.TConceptos.Nuevo.Click += this.TConcepto_Nuevo_Click;
            this.TConceptos.Remover.Click += this.TConcepto_Remove_Click;
            this.TConceptos.Duplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Productos.Click += this.TConcepto_Productos_Click;
            this.ComprobanteControl.TComprobante.Actualizar.PerformClick();

        }
        #endregion
    }
}
