﻿namespace Jaeger.UI.CP.Forms.Almacen.DP {
    partial class RemisionControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.TabControl = new Telerik.WinControls.UI.RadPageView();
            this.PageComprobante = new Telerik.WinControls.UI.RadPageViewPage();
            this.General = new Jaeger.UI.CP.Forms.Almacen.DP.RemisionGeneralControl();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.TComprobante = new Jaeger.UI.Common.Forms.TbDocumentoControl();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.PageComprobante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.PageComprobante);
            this.TabControl.DefaultPage = this.PageComprobante;
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 30);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedPage = this.PageComprobante;
            this.TabControl.Size = new System.Drawing.Size(1350, 151);
            this.TabControl.TabIndex = 2;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).ItemAlignment = Telerik.WinControls.UI.StripViewItemAlignment.Near;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).ItemFitMode = Telerik.WinControls.UI.StripViewItemFitMode.Shrink;
            // 
            // PageComprobante
            // 
            this.PageComprobante.Controls.Add(this.General);
            this.PageComprobante.ItemSize = new System.Drawing.SizeF(85F, 28F);
            this.PageComprobante.Location = new System.Drawing.Point(10, 37);
            this.PageComprobante.Name = "PageComprobante";
            this.PageComprobante.Size = new System.Drawing.Size(1329, 103);
            this.PageComprobante.Text = "Comprobante";
            // 
            // General
            // 
            this.General.Dock = System.Windows.Forms.DockStyle.Fill;
            this.General.Location = new System.Drawing.Point(0, 0);
            this.General.Margin = new System.Windows.Forms.Padding(4);
            this.General.MaximumSize = new System.Drawing.Size(0, 102);
            this.General.MinimumSize = new System.Drawing.Size(0, 102);
            this.General.Name = "General";
            this.General.Size = new System.Drawing.Size(1329, 102);
            this.General.TabIndex = 1;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.ShowActualizar = true;
            this.TComprobante.ShowComplemento = true;
            this.TComprobante.ShowEnviar = true;
            this.TComprobante.ShowGuardar = true;
            this.TComprobante.ShowIdDocumento = true;
            this.TComprobante.ShowImprimir = true;
            this.TComprobante.ShowPDF = true;
            this.TComprobante.Size = new System.Drawing.Size(1350, 30);
            this.TComprobante.TabIndex = 2;
            // 
            // RemisionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.TComprobante);
            this.MinimumSize = new System.Drawing.Size(1350, 180);
            this.Name = "RemisionControl";
            this.Size = new System.Drawing.Size(1350, 181);
            this.Load += new System.EventHandler(this.RemisionInternaControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.PageComprobante.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal RemisionGeneralControl General;
        protected internal Jaeger.UI.Common.Forms.TbDocumentoControl TComprobante;
        protected internal Telerik.WinControls.UI.RadPageView TabControl;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageComprobante;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}
