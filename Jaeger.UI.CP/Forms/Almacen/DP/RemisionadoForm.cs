﻿using System;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Cotizador.Produccion.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CP.Forms.Almacen.DP {
    /// <summary>
    /// 
    /// </summary>
    public class RemisionadoForm : UI.Almacen.DP.Forms.RemisionadoForm {
        #region declaraciones
        protected internal Aplication.Cotizador.Contracts.IOrdenProduccionService ordenProduccionService;
        protected internal Aplication.Almacen.PT.Contracts.IRemisionadoService remisionadoService;
        protected internal RadMenuItem _RemisionCliente;
        #endregion

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this._RemisionCliente = UI.Almacen.PT.Builder.RemisionadoGridBuilder.RemisionCliente();
            this.TRemision.menuContextual.Items.Add(this._RemisionCliente);
            this._RemisionCliente.Click += this.RemisionCliente_Click;
            var d2 = new Domain.Base.ValueObjects.UIAction(ConfigService.GeMenuElement(this._RemisionCliente.Name).Permisos);
            this._RemisionCliente.Visibility = (d2.Agregar ? ElementVisibility.Visible : ElementVisibility.Collapsed);
        }

        public override void OnLoad() {
            this.Service = new RemisionadoInternoService();
            this.ordenProduccionService = new OrdenProduccionService();
        }

        public override void TRemision_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new RemisionForm(0) { MdiParent = ParentForm };
            _nuevo.Show();
        }

        public virtual void RemisionCliente_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GridData.CurrentRow.DataBoundItem as RemisionDetailModel;
            if (seleccionado != null) {
                using (var espera = new Waiting1Form(this.GetConceptos)) {
                    espera.ShowDialog(this);
                }
                if (seleccionado.Conceptos != null) {
                    if (seleccionado.Conceptos.Count > 0) {
                        var indices = seleccionado.Conceptos.Select(it => it.IdPedido).ToArray();
                        var ordenes = this.ordenProduccionService.GetList<OrdenProduccionDetailModel>(OrdenProduccionService.Query().ByIdOrden(indices).Build()).ToList();
                        if (ordenes != null) {
                            var verificado = ordenes.Where(it => it.IdVerificado == 0).Count();
                            if (verificado > 0) {
                                RadMessageBox.Show(this, Properties.Resources.msg_OrdenNoVerificada, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                                return;
                            }
                            if (this.remisionadoService == null) {
                                this.remisionadoService = new RemisionadoService();
                            }
                            if (ordenes.Count > 0) {
                                var remision = RemisionService.Create().Build();
                                if (remision != null) {
                                    foreach (var item in ordenes) {
                                        remision.Conceptos.Add(RemisionService.Convertir(item));
                                    }
                                    var nuevoRemision = new PT.RemisionFiscalForm(this.remisionadoService, (Domain.Almacen.PT.Entities.RemisionDetailModel)remision) { MdiParent = this.ParentForm };
                                    nuevoRemision.Show();
                                }
                            }
                        }
                    }
                }
            }
        }

        public override void TRemision_Editar_Click(object sender, EventArgs e) {
            if (this.TRemision.GridData.CurrentRow != null) {
                var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
                if (seleccionado != null) {

                }
            }
        }

        public override void TRemision_Cancelar_Click(object sender, EventArgs e) {
            if (this.TRemision.GridData.CurrentRow != null) {
                var seleccionado = this.TRemision.GridData.CurrentRow.DataBoundItem as RemisionDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdStatus == 0) {
                        RadMessageBox.Show(this, Properties.Resources.msg_RemisionCancelada, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                    } else {
                        if (RadMessageBox.Show(this, Properties.Resources.msg_RemisionCancelar, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                            var cancela = new UI.Almacen.DP.Forms.RemisionStatusForm(seleccionado);
                            cancela.Selected += this.TRemision_Cancela_Selected;
                            cancela.ShowDialog(this);
                        }
                    }
                }
            }
        }

        public override void PorPartida_Click(object sender, EventArgs e) {
            var porPartida = new RemisionadoPartidaForm() { MdiParent = ParentForm };
            porPartida.Show();
        }
    }
}
