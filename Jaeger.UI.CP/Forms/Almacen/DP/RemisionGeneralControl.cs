﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Cotizador.Contracts;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Cotizador.Empresa.Entities;
using Jaeger.Domain.Cotizador.Ventas.Entities;

namespace Jaeger.UI.CP.Forms.Almacen.DP {
    public partial class RemisionGeneralControl : UserControl {
        #region
        protected BackgroundWorker DirectorioBackground;
        protected BindingList<ContribuyenteDetailModel> clientes;
        protected IContribuyentesService Service;
        protected IDepartamentosService Departamentos;
        protected IVendedorService Vendedor;
        #endregion

        public RemisionGeneralControl() {
            InitializeComponent();
        }

        private void RemisionGeneralControl_Load(object sender, EventArgs e) {
            this.Enabled = false;
            using (Cotizador.Forms.Empresa.Builder.IDirectorioGridBuilder view = new Cotizador.Forms.Empresa.Builder.DirectorioGridBuilder()) {
                this.IdDomicilio.Columns.AddRange(view.Templetes().Remision0().Build());
            };

            this.DirectorioBackground = new BackgroundWorker();
            this.DirectorioBackground.DoWork += PrepararContribuyentes_DoWork;
            this.DirectorioBackground.RunWorkerCompleted += PrepararContribuyentes_RunWorkerCompleted;

            this.Folio.KeyPress += new KeyPressEventHandler(TelerikTextBoxExtension.TxbFolio_KeyPress);

            this.FechaEmision.SetEditable(false);
        }

        public virtual void Start() {
            this.Departamentos = new DepartamentosService();
            this.Vendedor = new VendedorService();

            this.Receptor.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.Receptor.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.Receptor.EditorControl.TableElement.MinSize = new Size(300, 200);

            // catalogo formas de pago
            this.UsoRemision.DataSource = RemisionInternaService.GetUsos();
            this.UsoRemision.SelectedIndex = -1;
            this.UsoRemision.SelectedValue = null;

            // catalogo metodos de pago
            this.Departamento.DataSource = this.Departamentos.GetDepartamentos();
            this.Departamento.SelectedIndex = -1;
            this.Departamento.SelectedValue = null;

            // metodo de envio
            this.IdMetodoEnvio.DataSource = RemisionInternaService.GetMetodoEnvio();
            this.IdMetodoEnvio.SelectedIndex = -1;
            this.IdMetodoEnvio.SelectedValue = null;

            this.IdVendedor.DataSource = this.Vendedor.GetList<VendedorModel>(VendedorService.Query().OnlyActive().Build());

            this.Enabled = true;
        }

        public void LoadContribuyente(CFDISubTipoEnum subTipo) {
            this.Service = new ContribuyentesService(TipoRelacionComericalEnum.Cliente);
            this.Receptor.Enabled = false;
            if (!this.DirectorioBackground.IsBusy) {
                this.DirectorioBackground.RunWorkerAsync();
            }
        }

        private void BContribuyente_Actualizar_Click(object sender, EventArgs e) {
            if (this.DirectorioBackground.IsBusy == false) {
                this.Receptor.Enabled = false;
                this.DirectorioBackground.RunWorkerAsync();
            }
        }

        private void BContribuyente_Nuevo_Click(object sender, EventArgs e) {
            //using (var nuevo = new ContribuyenteForm()) {
            //    nuevo.Text = "Nuevo Cliente";
            //    nuevo.ShowDialog(this);
            //    this.buttonActualizarDirectorio.PerformClick();
            //}
        }

        private void BuscarCliente_Click(object sender, EventArgs e) {
            var TCliente = new UI.Cotizador.Forms.Empresa.SearchForm(2);
            TCliente.Selected += this.TCliente_Selected;
            TCliente.ShowDialog(this);
        }

        private void TCliente_Selected(object sender, ContribuyenteDetailModel e) {
            if (e != null) {
                this.Receptor_DropDownOpened(sender, new EventArgs());
                this.Receptor.Text = e.Nombre;
            }
        }

        private void Receptor_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.Receptor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var receptor = temporal.DataBoundItem as ContribuyenteDetailModel;
                if (receptor != null) {
                    this.ReceptorRFC.Text = receptor.RFC;
                    this.Receptor.Text = receptor.Nombre;
                    this.ReceptorClave.Text = receptor.Clave;
                    this.IdDirectorio.Value = receptor.IdDirectorio;
                    //this.FactorPactado.Value = receptor.FactorDescuento;
                    //this.TasaIVAPactado.Value = receptor.FactorIvaPactado;

                    //if (receptor.Domicilios != null) {
                    //    this.IdDomicilio.DataSource = receptor.Domicilios;
                    //}

                    //if (receptor.Vendedores != null) {
                    //    this.IdVendedor.DataSource = receptor.Vendedores;
                    //    this.IdCondiciones.SelectedIndex = -1;
                    //    this.IdCondiciones.SelectedValue = null;
                    //}

                    if (receptor.Contactos == null && this.Recibe.Items.Count == 0) {
                        this.Recibe.Items.Add(new RadListDataItem("OSMO1 - Omar Soto"));
                        this.Recibe.Items.Add(new RadListDataItem("MORO1 - Romero Melín Aurelia Mónica"));
                        this.Recibe.Items.Add(new RadListDataItem("MAGO1 - Gómez Júarez María Isabel"));
                        this.Recibe.Items.Add(new RadListDataItem("XXXX1 - García Ruiz Martín"));
                        this.Recibe.Items.Add(new RadListDataItem("XXXX2 - Martínez Badillo Enrique"));
                        this.Recibe.AutoSizeItems = true;
                        this.Recibe.DropDownListElement.AutoSizeMode = RadAutoSizeMode.FitToAvailableSize;
                        this.Recibe.DropDownListElement.AutoSize = true;
                        this.Recibe.DropDownMinSize = new Size(200, 100);
                        this.Recibe.DropDownSizingMode = SizingMode.RightBottom;
                        //    this.Recibe.DataSource = receptor.Contactos;
                        //    this.Recibe.ValueMember = "Contacto";
                        //    this.Recibe.DisplayMember = "Contacto";
                    }
                } else {
                    RadMessageBox.Show(this, "Properties.Resources.msg_Objeto_NoValido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void Receptor_DropDownOpened(object sender, EventArgs e) {
            this.Receptor.DataSource = this.clientes;
        }

        private void IdVendedor_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.IdVendedor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var vendedor = temporal.DataBoundItem as VendedorModel;
                if (vendedor != null) {
                    this.IdVendedor2.Value = vendedor.IdVendedor;
                } else {
                    RadMessageBox.Show(this, "Properties.Resources.Message_Objeto_NoValido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void IdDomicilio_SelectedValueChange(object sender, EventArgs e) {
            var temporal = this.IdDomicilio.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                //var vendedor = temporal.DataBoundItem as DomicilioFiscalDetailModel;
                //if (vendedor != null) {
                //    this.IdDomicilio2.Value = vendedor.Id;
                //} else {
                //    RadMessageBox.Show(this, "Properties.Resources.Message_Objeto_NoValido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                //}
            }
        }

        private void PrepararContribuyentes_DoWork(object sender, DoWorkEventArgs e) {
            this.IdDomicilio.ValueMember = "Id";
            this.IdDomicilio.DisplayMember = "Completo";

            this.bClienteActualizar.Enabled = false;
            this.bClienteBuscar.Enabled = false;
            this.Receptor.Enabled = false;
            this.IdDomicilio.Enabled = false;
            var d2 = ContribuyenteService.Query().ByIdClase(2).OnlyActive().Build();
            this.clientes = new BindingList<ContribuyenteDetailModel>(this.Service.GetList<ContribuyenteDetailModel>(d2).ToList());
        }

        private void PrepararContribuyentes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.bClienteActualizar.Enabled = true;
            this.bClienteBuscar.Enabled = true;
            this.Receptor.AutoSizeDropDownToBestFit = true;
            this.Receptor.SelectedIndex = -1;
            this.Receptor.SelectedValue = null;
            this.Receptor.Enabled = true;
            this.Receptor.Enabled = true;
            this.Receptor.SelectedValueChanged += new EventHandler(this.Receptor_SelectedValueChanged);
            this.Receptor.DropDownOpened += new EventHandler(this.Receptor_DropDownOpened);

            this.IdVendedor.SelectedValue = null;
            this.IdVendedor.SelectedIndex = -1;
            this.IdVendedor.SelectedValueChanged += this.IdVendedor_SelectedValueChanged;

            this.IdDomicilio.SelectedValue = null;
            this.IdDomicilio.SelectedIndex = -1;
            this.IdDomicilio.SelectedValueChanged += new EventHandler(this.IdDomicilio_SelectedValueChange);
            this.IdDomicilio.Enabled = true;
        }
    }
}
