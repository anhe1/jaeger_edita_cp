﻿using System;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Aplication.CP.Service;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CP.Forms.Almacen.DP {
    public partial class RemisionControl : UserControl {
        protected internal RemisionDetailModel _Comprobante;

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public RemisionControl() {
            InitializeComponent();
        }

        #region propiedades
        public RemisionDetailModel Comprobante {
            get { return this._Comprobante; }
            set { this._Comprobante = value; }
        }

        public Aplication.Almacen.DP.Contracts.IRemisionService Service {
            get; set;
        }

        public int Indice { get; set; }
        #endregion

        public virtual void Start() {
            if (this.Indice <= 0) {
                if (this.Comprobante != null)
                    if (this.Comprobante.Conceptos == null)
                        this.Comprobante = null;
            } else {
                this.Comprobante = new RemisionDetailModel { IdRemision = this.Indice };
            }

            this.Service = new RemisionInternaService();
            this.OnLoad_DoWork();
        }

        private void RemisionInternaControl_Load(object sender, EventArgs e) {
            this.TComprobante.FilePDF.Visibility = ElementVisibility.Collapsed;
            this.TComprobante.IdDocumento.TextBoxElement.TextBoxItem.ReadOnly = true;
            this.TComprobante.IdDocumento.TextBoxElement.TextBoxItem.TextAlign = HorizontalAlignment.Center;
            this.TComprobante.Actualizar.Click += this.TComprobante_Actualizar_Click;
            this.TComprobante.Nuevo.Click += this.TComprobante_Nuevo_Click;
            this.TComprobante.Duplicar.Click += this.TComprobante_Duplicar_Click;
            this.TComprobante.Guardar.Click += this.TComprobante_Guardar_Click;
            this.TComprobante.Imprimir.Click += this.Imprimir_Click;
        }

        #region barra de herramientas
        public virtual void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            this.Text = Properties.Resources.msg_RemisionNew;
            if (this.Comprobante == null) {
                this.Comprobante = this.Service.GetNew();
            } else if (this.Comprobante.IdRemision > 0) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.Text = Properties.Resources.msg_RemisionGet;
                    espera.ShowDialog(this); 
                    if (!string.IsNullOrEmpty(this.Comprobante.Serie) | string.IsNullOrEmpty(this.Comprobante.Folio.ToString())) {
                        this.Text = string.Concat(this.Comprobante.Serie, " ", this.Comprobante.Folio);
                    } else if (!string.IsNullOrEmpty(this.Comprobante.Folio.ToString())) {
                        this.Text = string.Concat(this.Comprobante.Folio);
                    }
                }
            }

            if (this.Comprobante.IsEditable) {
                this.General.LoadContribuyente(0);
            }
            this.CreateBinding();

        }

        public virtual void TComprobante_Nuevo_Click(object sender, EventArgs e) {
            this.Comprobante = null;
            this.TComprobante.Actualizar.PerformClick();
        }

        public virtual void TComprobante_Duplicar_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No disponible", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
        }

        protected internal void TComprobante_Guardar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, "Properties.Resources.msg_ErrorCaptura", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }

            using (var espera = new Waiting1Form(this.SaveComprobante)) {
                espera.Text = "Properties.Resources.msg_ComprobanteSave";
                espera.ShowDialog(this);
            }
        }

        private void Imprimir_Click(object sender, EventArgs e) {
            if (this.Comprobante.IdRemision > 0) {
                using (var espera = new Waiting1Form(this.GetPrinter)) {
                    espera.ShowDialog(this);
                    var printer = (RemisionDetailPrinter)this.Comprobante.Tag;
                    if (printer != null) {
                        var imprimir = new UI.Almacen.DP.Forms.ReporteForm(printer);
                        imprimir.Show();
                    }
                }
            }
        }

        public virtual void TComprobante_Cancelar_Click(object sender, EventArgs e) {
            if (ValidacionService.UUID(this.Comprobante.IdDocumento)) {
                if (RadMessageBox.Show(this, "Properties.Resources.msg_Comprobante_Cancelar", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {

                }
            }
        }
        #endregion

        #region metodos privados
        private void OnLoad_DoWork() {
            this.General.Start();
            this.TComprobante.Status.DataSource = RemisionInternaService.GetStatus();

            this.TComprobante.Opciones.Items.Remove(this.TComprobante.Nuevo);
            this.TComprobante.Opciones.Items.Remove(this.TComprobante.Duplicar);
            this.TComprobante.Opciones.Items.Remove(this.TComprobante.Cancelar);
            this.TComprobante.Opciones.Items.Remove(this.TComprobante.SendEmail);
            this.TComprobante.Opciones.Items.Remove(this.TComprobante.Complemento);

            if (ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production == false) {
                this.TComprobante.Emisor.ToolTipText = "Properties.Resources.msg_Modo_Prueba";
                //this.TComprobante.Emisor.Image = Properties.Resources.more_info_16px;
            }
            this.TComprobante.Emisor.Text = ConfigService.Synapsis.Empresa.RFC;
        }

        private void CreateBinding() {
            this.TComprobante.Status.DataBindings.Clear();
            this.TComprobante.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Receptor.DataBindings.Clear();
            this.General.ReceptorRFC.DataBindings.Clear();
            this.General.ReceptorRFC.ReadOnly = true;
            this.General.Receptor.SetEditable(this.Comprobante.IsEditable);

            this.General.lblVersion.DataBindings.Clear();
            this.General.lblVersion.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.lblContribuyente.Text = "Receptor";
            this.General.Receptor.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.ReceptorRFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Recibe.DataBindings.Clear();
            this.General.Recibe.ReadOnly = !this.Comprobante.IsEditable;
            this.General.Recibe.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Departamento.DataBindings.Clear();
            this.General.Departamento.SetEditable(this.Comprobante.IsEditable);
            this.General.Departamento.DataBindings.Add("SelectedValue", this.Comprobante, "IdDepartamento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TComprobante.IdDocumento.DataBindings.Clear();
            this.TComprobante.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.ReceptorClave.DataBindings.Clear();
            this.General.ReceptorClave.DataBindings.Add("Text", this.Comprobante, "ReceptorClave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdDirectorio.DataBindings.Clear();
            this.General.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Folio.DataBindings.Clear();
            this.General.Folio.ReadOnly = !this.Comprobante.IsEditable;
            this.General.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdSerie.DataBindings.Clear();
            this.General.IdSerie.SetEditable(this.Comprobante.IsEditable);
            this.General.IdSerie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdDomicilio.DataBindings.Clear();
            this.General.IdDomicilio.SetEditable(this.Comprobante.IsEditable);
            this.General.IdDomicilio.DataBindings.Add("Text", this.Comprobante, "DomicilioEntrega", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdDomicilio2.DataBindings.Clear();
            this.General.IdDomicilio2.DataBindings.Add("Value", this.Comprobante, "IdDomicilio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdGuia.DataBindings.Clear();
            this.General.IdGuia.ReadOnly = !this.Comprobante.IsEditable;
            this.General.IdGuia.DataBindings.Add("Text", this.Comprobante, "NoGuia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdVendedor.DataBindings.Clear();
            this.General.IdVendedor.SetEditable(this.Comprobante.IsEditable);
            this.General.IdVendedor.DataBindings.Add("Text", this.Comprobante, "Vendedor", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.IdVendedor2.DataBindings.Clear();
            this.General.IdVendedor2.DataBindings.Add("Value", this.Comprobante, "IdVendedor", false, DataSourceUpdateMode.OnPropertyChanged);

            this.General.TipoCambio.DataBindings.Clear();
            this.General.TipoCambio.SetEditable(this.Comprobante.IsEditable);
            this.General.TipoCambio.DataBindings.Add("Value", this.Comprobante, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Decimales.DataBindings.Clear();
            this.General.Decimales.SetEditable(this.Comprobante.IsEditable);
            this.General.Decimales.DataBindings.Add("Value", this.Comprobante, "PrecisionDecimal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Nota.DataBindings.Clear();
            this.General.Nota.ReadOnly = !this.Comprobante.IsEditable;
            this.General.Nota.DataBindings.Add("Text", this.Comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.UsoRemision.DataBindings.Clear();
            this.General.UsoRemision.SetEditable(this.Comprobante.IsEditable);
            this.General.UsoRemision.DataBindings.Add("SelectedValue", this.Comprobante, "IdTipoDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.IdMetodoEnvio.DataBindings.Clear();
            this.General.IdMetodoEnvio.SetEditable(this.Comprobante.IsEditable);
            this.General.IdMetodoEnvio.DataBindings.Add("SelectedValue", this.Comprobante, "IdMetodoEnvio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.Moneda.DataBindings.Clear();
            this.General.Moneda.SetEditable(this.Comprobante.IsEditable);
            this.General.Moneda.DataBindings.Add("Text", this.Comprobante, "ClaveMoneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FechaEmision.DataBindings.Clear();
            this.General.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.General.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.FechaEntrega.DataBindings.Clear();
            this.General.FechaEntrega.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            this.General.FechaEntrega.DateTimePickerElement.ReadOnly = true;
            this.General.FechaEntrega.DateTimePickerElement.TextBoxElement.TextAlign = HorizontalAlignment.Center;
            this.General.FechaEntrega.DataBindings.Add("Value", this.Comprobante, "FechaEntrega", true, DataSourceUpdateMode.OnPropertyChanged);
            this.General.FechaEntrega.SetEditable(this.Comprobante.IsEditable);

            this.General.Creo.DataBindings.Clear();
            this.General.Creo.ReadOnly = true;
            this.General.Creo.DataBindings.Add("Text", this.Comprobante, "Creo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.General.bClienteActualizar.Enabled = this.Comprobante.IsEditable;
            this.General.bClienteNuevo.Enabled = this.Comprobante.IsEditable;
            this.General.bClienteBuscar.Enabled = this.Comprobante.IsEditable;

            this.TComprobante.FilePDF.DataBindings.Clear();
            this.TComprobante.FilePDF.DataBindings.Add("Enabled", this.Comprobante, "IdRemision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Inabilitar();
            this.OnBindingClompleted(null);
        }

        private void Inabilitar() {
            this.TComprobante.Guardar.Enabled = this.Comprobante.IsEditable;
            this.TComprobante.Imprimir.Enabled = !this.Comprobante.IsEditable;
            this.General.Folio.ReadOnly = !this.Comprobante.IsEditable;
            this.General.IdSerie.ReadOnly = !this.Comprobante.IsEditable;
        }

        private void GetComprobante() {
            this.Comprobante = this.Service.GetRemision(this.Comprobante.IdRemision);
        }

        private void GetPrinter() {
            this.Comprobante.Tag = this.Service.GetPrinter(this.Comprobante.IdRemision);
        }

        private void SaveComprobante() {
            this.Comprobante = this.Service.Save(this.Comprobante);
            this.TComprobante.Actualizar.PerformClick();
        }

        private bool Validar() {
            this.errorProvider1.Clear();

            if (this.Comprobante.IdCliente == 0) {
                this.errorProvider1.SetError(this.General.Receptor, "No se selecciono al cliente.");
                return false;
            }

            //if (this.Comprobante.IdVendedor == 0) {
            //    this.errorProvider1.SetError(this.General.IdVendedor, "No se selecciono al vendedor.");
            //    return false;
            //}

            //if (this.Comprobante.IdDomicilio == 0) {
            //    this.errorProvider1.SetError(this.General.IdDomicilio, "No se selecciono dirección de embarque.");
            //    return false;
            //}

            if (string.IsNullOrEmpty(this.Comprobante.Contacto)) {
                this.errorProvider1.SetError(this.General.Recibe, "Es necesario incluir el nombre del receptor.");
                return false;
            }

            if (this.Comprobante.Conceptos.Count == 0) {
                MessageBox.Show(this, "No se agregaron conceptos a este documento.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (this.Comprobante.IdCliente == 0) {
                this.errorProvider1.SetError(this.General.Receptor, "Selecciona a un receptor");
                return false;
            }

            return true;
        }
        #endregion
    }
}
