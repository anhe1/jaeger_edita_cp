﻿namespace Jaeger.UI.CP.Forms.Almacen.DP {
    partial class RemisionGeneralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor1 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TipoCambio = new Telerik.WinControls.UI.RadSpinEditor();
            this.RadLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaEntrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaEntrega = new Telerik.WinControls.UI.RadLabel();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.IdSerie = new Telerik.WinControls.UI.RadDropDownList();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Departamento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ReceptorClave = new Telerik.WinControls.UI.RadTextBox();
            this.UsoRemision = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.IdDomicilio = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.IdMetodoEnvio = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ReceptorRFC = new Telerik.WinControls.UI.RadTextBox();
            this.Receptor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.lblSerie = new Telerik.WinControls.UI.RadLabel();
            this.lblContribuyente = new Telerik.WinControls.UI.RadLabel();
            this.lblCreo = new Telerik.WinControls.UI.RadLabel();
            this.lblReceptorRFC = new Telerik.WinControls.UI.RadLabel();
            this.Creo = new Telerik.WinControls.UI.RadTextBox();
            this.lblEnvio = new Telerik.WinControls.UI.RadLabel();
            this.lblTipoCambio = new Telerik.WinControls.UI.RadLabel();
            this.lblDomicilio = new Telerik.WinControls.UI.RadLabel();
            this.lblMoneda = new Telerik.WinControls.UI.RadLabel();
            this.lblMetodoPago = new Telerik.WinControls.UI.RadLabel();
            this.lblFormaPago = new Telerik.WinControls.UI.RadLabel();
            this.IdDirectorio = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblVendedor = new Telerik.WinControls.UI.RadLabel();
            this.IdVendedor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblVersion = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.IdDepartamento = new Telerik.WinControls.UI.RadSpinEditor();
            this.bClienteBuscar = new Telerik.WinControls.UI.RadButton();
            this.TasaIVAPactado = new Telerik.WinControls.UI.RadSpinEditor();
            this.FactorPactado = new Telerik.WinControls.UI.RadSpinEditor();
            this.IdDomicilio2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.IdVendedor2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblRecibe = new Telerik.WinControls.UI.RadLabel();
            this.Recibe = new Telerik.WinControls.UI.RadDropDownList();
            this.lblDecimales = new Telerik.WinControls.UI.RadLabel();
            this.bClienteNuevo = new Telerik.WinControls.UI.RadButton();
            this.bClienteActualizar = new Telerik.WinControls.UI.RadButton();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.Decimales = new Telerik.WinControls.UI.RadSpinEditor();
            this.RadLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.IdGuia = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoRemision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoRemision.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoRemision.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMetodoEnvio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMetodoEnvio.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMetodoEnvio.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContribuyente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceptorRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnvio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bClienteBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TasaIVAPactado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FactorPactado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecibe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recibe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bClienteNuevo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bClienteActualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdGuia)).BeginInit();
            this.SuspendLayout();
            // 
            // TipoCambio
            // 
            this.TipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCambio.DecimalPlaces = 2;
            this.TipoCambio.Enabled = false;
            this.TipoCambio.Location = new System.Drawing.Point(987, 55);
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.Size = new System.Drawing.Size(54, 20);
            this.TipoCambio.TabIndex = 17;
            this.TipoCambio.TabStop = false;
            this.TipoCambio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RadLabel9
            // 
            this.RadLabel9.Location = new System.Drawing.Point(3, 133);
            this.RadLabel9.Name = "RadLabel9";
            this.RadLabel9.Size = new System.Drawing.Size(67, 18);
            this.RadLabel9.TabIndex = 0;
            this.RadLabel9.Text = "Documento:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Location = new System.Drawing.Point(498, 7);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ShowItemToolTips = false;
            this.FechaEmision.Size = new System.Drawing.Size(211, 20);
            this.FechaEmision.TabIndex = 2;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEntrega.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaEntrega.Location = new System.Drawing.Point(810, 7);
            this.FechaEntrega.Name = "FechaEntrega";
            this.FechaEntrega.NullText = "--/--/----";
            this.FechaEntrega.ReadOnly = true;
            this.FechaEntrega.Size = new System.Drawing.Size(105, 20);
            this.FechaEntrega.TabIndex = 3;
            this.FechaEntrega.TabStop = false;
            this.FechaEntrega.Text = "05/12/2017";
            this.FechaEntrega.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEmision.Location = new System.Drawing.Point(425, 8);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(70, 18);
            this.lblFechaEmision.TabIndex = 6;
            this.lblFechaEmision.Text = "Fec. Emisión:";
            // 
            // lblFechaEntrega
            // 
            this.lblFechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEntrega.Location = new System.Drawing.Point(720, 8);
            this.lblFechaEntrega.Name = "lblFechaEntrega";
            this.lblFechaEntrega.Size = new System.Drawing.Size(85, 18);
            this.lblFechaEntrega.TabIndex = 8;
            this.lblFechaEntrega.Text = "Fec. de Entrega:";
            // 
            // Folio
            // 
            this.Folio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.Location = new System.Drawing.Point(260, 7);
            this.Folio.Name = "Folio";
            this.Folio.NullText = "Folio";
            this.Folio.Size = new System.Drawing.Size(121, 20);
            this.Folio.TabIndex = 1;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IdSerie
            // 
            this.IdSerie.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.IdSerie.Location = new System.Drawing.Point(76, 7);
            this.IdSerie.Name = "IdSerie";
            this.IdSerie.NullText = "Serie";
            this.IdSerie.Size = new System.Drawing.Size(125, 20);
            this.IdSerie.TabIndex = 0;
            // 
            // Moneda
            // 
            this.Moneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Moneda.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Enabled = false;
            this.Moneda.Location = new System.Drawing.Point(1100, 55);
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(67, 20);
            this.Moneda.TabIndex = 19;
            this.Moneda.TabStop = false;
            // 
            // Departamento
            // 
            this.Departamento.AutoSizeDropDownToBestFit = true;
            this.Departamento.DisplayMember = "Descriptor";
            this.Departamento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Departamento.NestedRadGridView
            // 
            this.Departamento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "IdDepartamento";
            gridViewTextBoxColumn3.FormatString = "{0:00#}";
            gridViewTextBoxColumn3.HeaderText = "Id";
            gridViewTextBoxColumn3.Name = "IdDepartamento";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.FieldName = "Departamento";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Departamento";
            gridViewTextBoxColumn5.FieldName = "Descriptor";
            gridViewTextBoxColumn5.HeaderText = "Descriptor";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Descriptor";
            this.Departamento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.Departamento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamento.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Departamento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Departamento.EditorControl.Name = "NestedRadGridView";
            this.Departamento.EditorControl.ReadOnly = true;
            this.Departamento.EditorControl.ShowGroupPanel = false;
            this.Departamento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamento.EditorControl.TabIndex = 0;
            this.Departamento.Location = new System.Drawing.Point(89, 32);
            this.Departamento.Name = "Departamento";
            this.Departamento.NullText = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(227, 20);
            this.Departamento.TabIndex = 6;
            this.Departamento.TabStop = false;
            this.Departamento.ValueMember = "IdDepartamento";
            // 
            // ReceptorClave
            // 
            this.ReceptorClave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReceptorClave.Location = new System.Drawing.Point(448, 55);
            this.ReceptorClave.MaxLength = 5;
            this.ReceptorClave.Name = "ReceptorClave";
            this.ReceptorClave.NullText = "Clave";
            this.ReceptorClave.Size = new System.Drawing.Size(65, 20);
            this.ReceptorClave.TabIndex = 10;
            this.ReceptorClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UsoRemision
            // 
            this.UsoRemision.AutoSizeDropDownToBestFit = true;
            this.UsoRemision.DisplayMember = "Descriptor";
            this.UsoRemision.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // UsoRemision.NestedRadGridView
            // 
            this.UsoRemision.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.UsoRemision.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsoRemision.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UsoRemision.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.UsoRemision.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.UsoRemision.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.UsoRemision.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "Id";
            gridViewTextBoxColumn6.FormatString = "{0:00#}";
            gridViewTextBoxColumn6.HeaderText = "Clave";
            gridViewTextBoxColumn6.Name = "Id";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.FieldName = "Descripcion";
            gridViewTextBoxColumn7.HeaderText = "Descripción";
            gridViewTextBoxColumn7.Name = "Descripcion";
            gridViewTextBoxColumn8.FieldName = "Descriptor";
            gridViewTextBoxColumn8.HeaderText = "Descriptor";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Descriptor";
            this.UsoRemision.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.UsoRemision.EditorControl.MasterTemplate.EnableGrouping = false;
            this.UsoRemision.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.UsoRemision.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.UsoRemision.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.UsoRemision.EditorControl.Name = "NestedRadGridView";
            this.UsoRemision.EditorControl.ReadOnly = true;
            this.UsoRemision.EditorControl.ShowGroupPanel = false;
            this.UsoRemision.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.UsoRemision.EditorControl.TabIndex = 0;
            this.UsoRemision.Location = new System.Drawing.Point(448, 32);
            this.UsoRemision.Name = "UsoRemision";
            this.UsoRemision.NullText = "Uso de Remisión";
            this.UsoRemision.Size = new System.Drawing.Size(203, 20);
            this.UsoRemision.TabIndex = 7;
            this.UsoRemision.TabStop = false;
            this.UsoRemision.ValueMember = "Id";
            // 
            // IdDomicilio
            // 
            this.IdDomicilio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // IdDomicilio.NestedRadGridView
            // 
            this.IdDomicilio.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.IdDomicilio.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdDomicilio.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.IdDomicilio.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.IdDomicilio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.IdDomicilio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.IdDomicilio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.IdDomicilio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.IdDomicilio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.IdDomicilio.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.IdDomicilio.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.IdDomicilio.EditorControl.Name = "NestedRadGridView";
            this.IdDomicilio.EditorControl.ReadOnly = true;
            this.IdDomicilio.EditorControl.ShowGroupPanel = false;
            this.IdDomicilio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.IdDomicilio.EditorControl.TabIndex = 0;
            this.IdDomicilio.Location = new System.Drawing.Point(67, 79);
            this.IdDomicilio.Name = "IdDomicilio";
            this.IdDomicilio.NullText = "Embarque";
            this.IdDomicilio.Size = new System.Drawing.Size(333, 20);
            this.IdDomicilio.TabIndex = 20;
            this.IdDomicilio.TabStop = false;
            // 
            // IdMetodoEnvio
            // 
            this.IdMetodoEnvio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdMetodoEnvio.AutoSizeDropDownHeight = true;
            this.IdMetodoEnvio.AutoSizeDropDownToBestFit = true;
            this.IdMetodoEnvio.DisplayMember = "Descriptor";
            this.IdMetodoEnvio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // IdMetodoEnvio.NestedRadGridView
            // 
            this.IdMetodoEnvio.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.IdMetodoEnvio.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdMetodoEnvio.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.IdMetodoEnvio.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.IdMetodoEnvio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.IdMetodoEnvio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.IdMetodoEnvio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.DataType = typeof(int);
            gridViewTextBoxColumn9.FieldName = "Id";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "Id";
            gridViewTextBoxColumn10.FieldName = "Descriptor";
            gridViewTextBoxColumn10.HeaderText = "Descriptor";
            gridViewTextBoxColumn10.Name = "Descriptor";
            this.IdMetodoEnvio.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.IdMetodoEnvio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.IdMetodoEnvio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.IdMetodoEnvio.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.IdMetodoEnvio.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.IdMetodoEnvio.EditorControl.Name = "NestedRadGridView";
            this.IdMetodoEnvio.EditorControl.ReadOnly = true;
            this.IdMetodoEnvio.EditorControl.ShowGroupPanel = false;
            this.IdMetodoEnvio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.IdMetodoEnvio.EditorControl.TabIndex = 0;
            this.IdMetodoEnvio.Location = new System.Drawing.Point(777, 55);
            this.IdMetodoEnvio.Name = "IdMetodoEnvio";
            this.IdMetodoEnvio.NullText = "Método de Envío";
            this.IdMetodoEnvio.Size = new System.Drawing.Size(138, 20);
            this.IdMetodoEnvio.TabIndex = 16;
            this.IdMetodoEnvio.TabStop = false;
            this.IdMetodoEnvio.ValueMember = "Id";
            // 
            // ReceptorRFC
            // 
            this.ReceptorRFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReceptorRFC.Location = new System.Drawing.Point(622, 55);
            this.ReceptorRFC.Name = "ReceptorRFC";
            this.ReceptorRFC.NullText = "Registro Federal";
            this.ReceptorRFC.ReadOnly = true;
            this.ReceptorRFC.Size = new System.Drawing.Size(110, 20);
            this.ReceptorRFC.TabIndex = 15;
            this.ReceptorRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Receptor
            // 
            this.Receptor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Receptor.AutoSizeDropDownHeight = true;
            this.Receptor.AutoSizeDropDownToBestFit = true;
            this.Receptor.DisplayMember = "Nombre";
            // 
            // Receptor.NestedRadGridView
            // 
            this.Receptor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Receptor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Receptor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Receptor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Receptor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Receptor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Receptor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Receptor.EditorControl.MasterTemplate.AllowSearchRow = true;
            gridViewTextBoxColumn11.FieldName = "Id";
            gridViewTextBoxColumn11.HeaderText = "Id";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "Id";
            gridViewTextBoxColumn12.FieldName = "Nombre";
            gridViewTextBoxColumn12.HeaderText = "Nombre";
            gridViewTextBoxColumn12.Name = "Nombre";
            gridViewTextBoxColumn12.Width = 220;
            gridViewTextBoxColumn13.FieldName = "RFC";
            gridViewTextBoxColumn13.HeaderText = "RFC";
            gridViewTextBoxColumn13.Name = "_drctr_rfc";
            gridViewTextBoxColumn14.FieldName = "ResidenciaFiscal";
            gridViewTextBoxColumn14.HeaderText = "Residencia Fiscal";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "_drctr_resfis";
            gridViewTextBoxColumn15.FieldName = "ClaveUsoCFDI";
            gridViewTextBoxColumn15.HeaderText = "Uso CFDI";
            gridViewTextBoxColumn15.Name = "ClaveUsoCFDI";
            this.Receptor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15});
            this.Receptor.EditorControl.MasterTemplate.EnableGrouping = false;
            filterDescriptor1.Operator = Telerik.WinControls.Data.FilterOperator.StartsWith;
            filterDescriptor1.PropertyName = "Nombre";
            this.Receptor.EditorControl.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor1});
            this.Receptor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Receptor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Receptor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.Receptor.EditorControl.Name = "NestedRadGridView";
            this.Receptor.EditorControl.ReadOnly = true;
            this.Receptor.EditorControl.ShowGroupPanel = false;
            this.Receptor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Receptor.EditorControl.TabIndex = 0;
            this.Receptor.Location = new System.Drawing.Point(67, 55);
            this.Receptor.Name = "Receptor";
            this.Receptor.NullText = "Denominación o Razon Social del Receptor";
            this.Receptor.Size = new System.Drawing.Size(380, 20);
            this.Receptor.TabIndex = 9;
            this.Receptor.TabStop = false;
            this.Receptor.ValueMember = "Id";
            // 
            // lblFolio
            // 
            this.lblFolio.Location = new System.Drawing.Point(212, 8);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 4;
            this.lblFolio.Text = "Folio:";
            // 
            // lblSerie
            // 
            this.lblSerie.Location = new System.Drawing.Point(37, 8);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(33, 18);
            this.lblSerie.TabIndex = 2;
            this.lblSerie.Text = "Serie:";
            // 
            // lblContribuyente
            // 
            this.lblContribuyente.Location = new System.Drawing.Point(3, 56);
            this.lblContribuyente.Name = "lblContribuyente";
            this.lblContribuyente.Size = new System.Drawing.Size(54, 18);
            this.lblContribuyente.TabIndex = 10;
            this.lblContribuyente.Text = "Receptor:";
            // 
            // lblCreo
            // 
            this.lblCreo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCreo.Location = new System.Drawing.Point(948, 8);
            this.lblCreo.Name = "lblCreo";
            this.lblCreo.Size = new System.Drawing.Size(32, 18);
            this.lblCreo.TabIndex = 28;
            this.lblCreo.Text = "Creó:";
            // 
            // lblReceptorRFC
            // 
            this.lblReceptorRFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReceptorRFC.Location = new System.Drawing.Point(588, 56);
            this.lblReceptorRFC.Name = "lblReceptorRFC";
            this.lblReceptorRFC.Size = new System.Drawing.Size(28, 18);
            this.lblReceptorRFC.TabIndex = 13;
            this.lblReceptorRFC.Text = "RFC:";
            // 
            // Creo
            // 
            this.Creo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Creo.Location = new System.Drawing.Point(987, 7);
            this.Creo.MaxLength = 10;
            this.Creo.Name = "Creo";
            this.Creo.NullText = "Creó";
            this.Creo.Size = new System.Drawing.Size(82, 20);
            this.Creo.TabIndex = 4;
            this.Creo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblEnvio
            // 
            this.lblEnvio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnvio.Location = new System.Drawing.Point(735, 56);
            this.lblEnvio.Name = "lblEnvio";
            this.lblEnvio.Size = new System.Drawing.Size(36, 18);
            this.lblEnvio.TabIndex = 16;
            this.lblEnvio.Text = "Envío:";
            // 
            // lblTipoCambio
            // 
            this.lblTipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoCambio.Location = new System.Drawing.Point(922, 56);
            this.lblTipoCambio.Name = "lblTipoCambio";
            this.lblTipoCambio.Size = new System.Drawing.Size(59, 18);
            this.lblTipoCambio.TabIndex = 37;
            this.lblTipoCambio.Text = "T. Cambio:";
            // 
            // lblDomicilio
            // 
            this.lblDomicilio.Location = new System.Drawing.Point(3, 80);
            this.lblDomicilio.Name = "lblDomicilio";
            this.lblDomicilio.Size = new System.Drawing.Size(59, 18);
            this.lblDomicilio.TabIndex = 20;
            this.lblDomicilio.Text = "Embarque:";
            // 
            // lblMoneda
            // 
            this.lblMoneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneda.Location = new System.Drawing.Point(1044, 56);
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(50, 18);
            this.lblMoneda.TabIndex = 18;
            this.lblMoneda.Text = "Moneda:";
            // 
            // lblMetodoPago
            // 
            this.lblMetodoPago.Location = new System.Drawing.Point(2, 33);
            this.lblMetodoPago.Name = "lblMetodoPago";
            this.lblMetodoPago.Size = new System.Drawing.Size(81, 18);
            this.lblMetodoPago.TabIndex = 31;
            this.lblMetodoPago.Text = "Departamento:";
            // 
            // lblFormaPago
            // 
            this.lblFormaPago.Location = new System.Drawing.Point(346, 33);
            this.lblFormaPago.Name = "lblFormaPago";
            this.lblFormaPago.Size = new System.Drawing.Size(92, 18);
            this.lblFormaPago.TabIndex = 33;
            this.lblFormaPago.Text = "Uso de Remisión:";
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDirectorio.Location = new System.Drawing.Point(1243, 28);
            this.IdDirectorio.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.ShowBorder = false;
            this.IdDirectorio.ShowUpDownButtons = false;
            this.IdDirectorio.Size = new System.Drawing.Size(37, 20);
            this.IdDirectorio.TabIndex = 29;
            this.IdDirectorio.TabStop = false;
            // 
            // lblVendedor
            // 
            this.lblVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVendedor.Location = new System.Drawing.Point(922, 32);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.Size = new System.Drawing.Size(58, 18);
            this.lblVendedor.TabIndex = 89;
            this.lblVendedor.Text = "Vendedor:";
            // 
            // IdVendedor
            // 
            this.IdVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdVendedor.AutoSizeDropDownToBestFit = true;
            this.IdVendedor.DisplayMember = "ClaveVendedor";
            this.IdVendedor.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // IdVendedor.NestedRadGridView
            // 
            this.IdVendedor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.IdVendedor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdVendedor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.IdVendedor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.IdVendedor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.IdVendedor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.IdVendedor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn16.DataType = typeof(int);
            gridViewTextBoxColumn16.FieldName = "IdVendedor";
            gridViewTextBoxColumn16.HeaderText = "IdVendedor";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "IdVendedor";
            gridViewTextBoxColumn17.FieldName = "ClaveVendedor";
            gridViewTextBoxColumn17.HeaderText = "Vendedor";
            gridViewTextBoxColumn17.Name = "ClaveVendedor";
            gridViewTextBoxColumn18.FieldName = "Nombre";
            gridViewTextBoxColumn18.HeaderText = "Nombre";
            gridViewTextBoxColumn18.Name = "Nombre";
            gridViewTextBoxColumn18.Width = 150;
            gridViewTextBoxColumn19.FieldName = "Descriptor";
            gridViewTextBoxColumn19.HeaderText = "Descriptor";
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "Descriptor";
            this.IdVendedor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19});
            this.IdVendedor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.IdVendedor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.IdVendedor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.IdVendedor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.IdVendedor.EditorControl.Name = "NestedRadGridView";
            this.IdVendedor.EditorControl.ReadOnly = true;
            this.IdVendedor.EditorControl.ShowGroupPanel = false;
            this.IdVendedor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.IdVendedor.EditorControl.TabIndex = 0;
            this.IdVendedor.Location = new System.Drawing.Point(987, 31);
            this.IdVendedor.Name = "IdVendedor";
            this.IdVendedor.NullText = "Vendedor";
            this.IdVendedor.Size = new System.Drawing.Size(180, 20);
            this.IdVendedor.TabIndex = 8;
            this.IdVendedor.TabStop = false;
            this.IdVendedor.ValueMember = "IdVendedor";
            // 
            // lblVersion
            // 
            this.lblVersion.Location = new System.Drawing.Point(5, 8);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(26, 18);
            this.lblVersion.TabIndex = 91;
            this.lblVersion.Text = "Ver.";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.IdDepartamento);
            this.groupBox1.Controls.Add(this.bClienteBuscar);
            this.groupBox1.Controls.Add(this.ReceptorClave);
            this.groupBox1.Controls.Add(this.TasaIVAPactado);
            this.groupBox1.Controls.Add(this.FactorPactado);
            this.groupBox1.Controls.Add(this.IdDomicilio2);
            this.groupBox1.Controls.Add(this.IdVendedor2);
            this.groupBox1.Controls.Add(this.lblRecibe);
            this.groupBox1.Controls.Add(this.Recibe);
            this.groupBox1.Controls.Add(this.lblVersion);
            this.groupBox1.Controls.Add(this.lblVendedor);
            this.groupBox1.Controls.Add(this.IdVendedor);
            this.groupBox1.Controls.Add(this.Departamento);
            this.groupBox1.Controls.Add(this.IdDirectorio);
            this.groupBox1.Controls.Add(this.lblDecimales);
            this.groupBox1.Controls.Add(this.TipoCambio);
            this.groupBox1.Controls.Add(this.bClienteNuevo);
            this.groupBox1.Controls.Add(this.RadLabel9);
            this.groupBox1.Controls.Add(this.bClienteActualizar);
            this.groupBox1.Controls.Add(this.lblFormaPago);
            this.groupBox1.Controls.Add(this.Nota);
            this.groupBox1.Controls.Add(this.Decimales);
            this.groupBox1.Controls.Add(this.lblMetodoPago);
            this.groupBox1.Controls.Add(this.FechaEmision);
            this.groupBox1.Controls.Add(this.RadLabel14);
            this.groupBox1.Controls.Add(this.FechaEntrega);
            this.groupBox1.Controls.Add(this.lblMoneda);
            this.groupBox1.Controls.Add(this.lblFechaEmision);
            this.groupBox1.Controls.Add(this.lblDomicilio);
            this.groupBox1.Controls.Add(this.lblFechaEntrega);
            this.groupBox1.Controls.Add(this.lblTipoCambio);
            this.groupBox1.Controls.Add(this.lblNota);
            this.groupBox1.Controls.Add(this.lblEnvio);
            this.groupBox1.Controls.Add(this.Folio);
            this.groupBox1.Controls.Add(this.Creo);
            this.groupBox1.Controls.Add(this.IdSerie);
            this.groupBox1.Controls.Add(this.lblReceptorRFC);
            this.groupBox1.Controls.Add(this.lblCreo);
            this.groupBox1.Controls.Add(this.Moneda);
            this.groupBox1.Controls.Add(this.lblContribuyente);
            this.groupBox1.Controls.Add(this.lblSerie);
            this.groupBox1.Controls.Add(this.lblFolio);
            this.groupBox1.Controls.Add(this.UsoRemision);
            this.groupBox1.Controls.Add(this.Receptor);
            this.groupBox1.Controls.Add(this.ReceptorRFC);
            this.groupBox1.Controls.Add(this.IdDomicilio);
            this.groupBox1.Controls.Add(this.IdMetodoEnvio);
            this.groupBox1.Controls.Add(this.IdGuia);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "";
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1200, 102);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            // 
            // IdDepartamento
            // 
            this.IdDepartamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDepartamento.Location = new System.Drawing.Point(1213, 111);
            this.IdDepartamento.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDepartamento.Name = "IdDepartamento";
            this.IdDepartamento.ShowBorder = false;
            this.IdDepartamento.ShowUpDownButtons = false;
            this.IdDepartamento.Size = new System.Drawing.Size(37, 20);
            this.IdDepartamento.TabIndex = 374;
            this.IdDepartamento.TabStop = false;
            // 
            // bClienteBuscar
            // 
            this.bClienteBuscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClienteBuscar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.bClienteBuscar.Enabled = false;
            this.bClienteBuscar.Image = global::Jaeger.UI.CP.Properties.Resources.search_16px;
            this.bClienteBuscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.bClienteBuscar.Location = new System.Drawing.Point(541, 55);
            this.bClienteBuscar.Name = "bClienteBuscar";
            this.bClienteBuscar.Size = new System.Drawing.Size(20, 20);
            this.bClienteBuscar.TabIndex = 11;
            this.bClienteBuscar.Click += new System.EventHandler(this.BuscarCliente_Click);
            // 
            // TasaIVAPactado
            // 
            this.TasaIVAPactado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TasaIVAPactado.DecimalPlaces = 4;
            this.TasaIVAPactado.Location = new System.Drawing.Point(1214, 7);
            this.TasaIVAPactado.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.TasaIVAPactado.Name = "TasaIVAPactado";
            this.TasaIVAPactado.ShowBorder = false;
            this.TasaIVAPactado.ShowUpDownButtons = false;
            this.TasaIVAPactado.Size = new System.Drawing.Size(37, 20);
            this.TasaIVAPactado.TabIndex = 96;
            this.TasaIVAPactado.TabStop = false;
            // 
            // FactorPactado
            // 
            this.FactorPactado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FactorPactado.DecimalPlaces = 4;
            this.FactorPactado.Location = new System.Drawing.Point(1214, 32);
            this.FactorPactado.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.FactorPactado.Name = "FactorPactado";
            this.FactorPactado.ShowBorder = false;
            this.FactorPactado.ShowUpDownButtons = false;
            this.FactorPactado.Size = new System.Drawing.Size(37, 20);
            this.FactorPactado.TabIndex = 30;
            this.FactorPactado.TabStop = false;
            // 
            // IdDomicilio2
            // 
            this.IdDomicilio2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDomicilio2.Location = new System.Drawing.Point(1214, 55);
            this.IdDomicilio2.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDomicilio2.Name = "IdDomicilio2";
            this.IdDomicilio2.ShowBorder = false;
            this.IdDomicilio2.ShowUpDownButtons = false;
            this.IdDomicilio2.Size = new System.Drawing.Size(37, 20);
            this.IdDomicilio2.TabIndex = 31;
            this.IdDomicilio2.TabStop = false;
            // 
            // IdVendedor2
            // 
            this.IdVendedor2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdVendedor2.Location = new System.Drawing.Point(1214, 79);
            this.IdVendedor2.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdVendedor2.Name = "IdVendedor2";
            this.IdVendedor2.ShowBorder = false;
            this.IdVendedor2.ShowUpDownButtons = false;
            this.IdVendedor2.Size = new System.Drawing.Size(37, 20);
            this.IdVendedor2.TabIndex = 30;
            this.IdVendedor2.TabStop = false;
            // 
            // lblRecibe
            // 
            this.lblRecibe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRecibe.Location = new System.Drawing.Point(403, 80);
            this.lblRecibe.Name = "lblRecibe";
            this.lblRecibe.Size = new System.Drawing.Size(42, 18);
            this.lblRecibe.TabIndex = 21;
            this.lblRecibe.Text = "Recibe:";
            // 
            // Recibe
            // 
            this.Recibe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Recibe.Location = new System.Drawing.Point(451, 79);
            this.Recibe.MaxLength = 32;
            this.Recibe.Name = "Recibe";
            this.Recibe.NullText = "Recibe";
            this.Recibe.Size = new System.Drawing.Size(132, 20);
            this.Recibe.TabIndex = 22;
            // 
            // lblDecimales
            // 
            this.lblDecimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDecimales.Location = new System.Drawing.Point(1074, 8);
            this.lblDecimales.Name = "lblDecimales";
            this.lblDecimales.Size = new System.Drawing.Size(30, 18);
            this.lblDecimales.TabIndex = 39;
            this.lblDecimales.Text = "Dec.:";
            // 
            // bClienteNuevo
            // 
            this.bClienteNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClienteNuevo.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.bClienteNuevo.Enabled = false;
            this.bClienteNuevo.Image = global::Jaeger.UI.CP.Properties.Resources.add_16px;
            this.bClienteNuevo.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.bClienteNuevo.Location = new System.Drawing.Point(563, 55);
            this.bClienteNuevo.Name = "bClienteNuevo";
            this.bClienteNuevo.Size = new System.Drawing.Size(20, 20);
            this.bClienteNuevo.TabIndex = 12;
            this.bClienteNuevo.Text = "radButton1";
            this.bClienteNuevo.Click += new System.EventHandler(this.BContribuyente_Nuevo_Click);
            // 
            // bClienteActualizar
            // 
            this.bClienteActualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bClienteActualizar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.bClienteActualizar.Enabled = false;
            this.bClienteActualizar.Image = global::Jaeger.UI.CP.Properties.Resources.refresh_16px;
            this.bClienteActualizar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.bClienteActualizar.Location = new System.Drawing.Point(519, 55);
            this.bClienteActualizar.Name = "bClienteActualizar";
            this.bClienteActualizar.Size = new System.Drawing.Size(20, 20);
            this.bClienteActualizar.TabIndex = 12;
            this.bClienteActualizar.Text = "radButton1";
            this.bClienteActualizar.Click += new System.EventHandler(this.BContribuyente_Actualizar_Click);
            // 
            // Nota
            // 
            this.Nota.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Nota.Location = new System.Drawing.Point(777, 79);
            this.Nota.Name = "Nota";
            this.Nota.NullText = "Observaciones";
            this.Nota.Size = new System.Drawing.Size(390, 20);
            this.Nota.TabIndex = 26;
            // 
            // Decimales
            // 
            this.Decimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Decimales.Enabled = false;
            this.Decimales.Location = new System.Drawing.Point(1115, 7);
            this.Decimales.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.Decimales.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.Decimales.Name = "Decimales";
            this.Decimales.NullableValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.Decimales.Size = new System.Drawing.Size(52, 20);
            this.Decimales.TabIndex = 5;
            this.Decimales.TabStop = false;
            this.Decimales.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Decimales.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // RadLabel14
            // 
            this.RadLabel14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadLabel14.Location = new System.Drawing.Point(587, 80);
            this.RadLabel14.Name = "RadLabel14";
            this.RadLabel14.Size = new System.Drawing.Size(31, 18);
            this.RadLabel14.TabIndex = 23;
            this.RadLabel14.Text = "Guía:";
            // 
            // lblNota
            // 
            this.lblNota.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNota.Location = new System.Drawing.Point(738, 80);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(33, 18);
            this.lblNota.TabIndex = 25;
            this.lblNota.Text = "Nota:";
            this.lblNota.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // IdGuia
            // 
            this.IdGuia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdGuia.Location = new System.Drawing.Point(622, 79);
            this.IdGuia.MaxLength = 32;
            this.IdGuia.Name = "IdGuia";
            this.IdGuia.NullText = "Guía";
            this.IdGuia.Size = new System.Drawing.Size(108, 20);
            this.IdGuia.TabIndex = 24;
            // 
            // RemisionGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(0, 102);
            this.MinimumSize = new System.Drawing.Size(1200, 102);
            this.Name = "RemisionGeneralControl";
            this.Size = new System.Drawing.Size(1200, 102);
            this.Load += new System.EventHandler(this.RemisionGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoRemision.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoRemision.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UsoRemision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMetodoEnvio.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMetodoEnvio.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMetodoEnvio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReceptorRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Receptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContribuyente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceptorRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnvio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMetodoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bClienteBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TasaIVAPactado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FactorPactado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecibe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recibe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bClienteNuevo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bClienteActualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdGuia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal Telerik.WinControls.UI.RadLabel RadLabel9;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEntrega;
        internal Telerik.WinControls.UI.RadLabel lblFechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblFechaEntrega;
        internal Telerik.WinControls.UI.RadTextBox Folio;
        internal Telerik.WinControls.UI.RadDropDownList IdSerie;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Departamento;
        internal Telerik.WinControls.UI.RadTextBox ReceptorClave;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox UsoRemision;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox IdDomicilio;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox IdMetodoEnvio;
        internal Telerik.WinControls.UI.RadTextBox ReceptorRFC;
        internal Telerik.WinControls.UI.RadLabel lblFolio;
        internal Telerik.WinControls.UI.RadLabel lblSerie;
        internal Telerik.WinControls.UI.RadLabel lblContribuyente;
        internal Telerik.WinControls.UI.RadLabel lblCreo;
        internal Telerik.WinControls.UI.RadLabel lblReceptorRFC;
        internal Telerik.WinControls.UI.RadTextBox Creo;
        internal Telerik.WinControls.UI.RadLabel lblEnvio;
        internal Telerik.WinControls.UI.RadLabel lblTipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblDomicilio;
        internal Telerik.WinControls.UI.RadLabel lblMoneda;
        internal Telerik.WinControls.UI.RadLabel lblMetodoPago;
        internal Telerik.WinControls.UI.RadLabel lblFormaPago;
        internal Telerik.WinControls.UI.RadSpinEditor TipoCambio;
        internal Telerik.WinControls.UI.RadButton bClienteActualizar;
        internal Telerik.WinControls.UI.RadButton bClienteNuevo;
        public Telerik.WinControls.UI.RadSpinEditor IdDirectorio;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Receptor;
        internal Telerik.WinControls.UI.RadLabel lblVendedor;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox IdVendedor;
        protected internal Telerik.WinControls.UI.RadLabel lblVersion;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        internal Telerik.WinControls.UI.RadLabel lblDecimales;
        internal Telerik.WinControls.UI.RadSpinEditor Decimales;
        protected internal Telerik.WinControls.UI.RadTextBox Nota;
        internal Telerik.WinControls.UI.RadLabel lblNota;
        internal Telerik.WinControls.UI.RadLabel lblRecibe;
        internal Telerik.WinControls.UI.RadDropDownList Recibe;
        protected internal Telerik.WinControls.UI.RadSpinEditor IdVendedor2;
        protected internal Telerik.WinControls.UI.RadSpinEditor IdDomicilio2;
        internal Telerik.WinControls.UI.RadSpinEditor FactorPactado;
        internal Telerik.WinControls.UI.RadSpinEditor TasaIVAPactado;
        public Telerik.WinControls.UI.RadButton bClienteBuscar;
        protected internal Telerik.WinControls.UI.RadSpinEditor IdDepartamento;
        internal Telerik.WinControls.UI.RadLabel RadLabel14;
        internal Telerik.WinControls.UI.RadTextBox IdGuia;
    }
}
