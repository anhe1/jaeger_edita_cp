﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Almacen.PT {
    public class RemisionadoPartidaForm : UI.Almacen.PT.Forms.RemisionadoPartidaForm {
        public RemisionadoPartidaForm(UIMenuElement menuElement, Aplication.Almacen.PT.Contracts.IRemisionadoService service) : base(menuElement, service) {
            this.Load += RemisionadoPartidaForm_Load;
        }

        private void RemisionadoPartidaForm_Load(object sender, EventArgs e) {
            this._Service = new Aplication.CP.Service.RemisionadoService();
        }
    }
}
