﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Cotizador.Empresa.Entities;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Cotizador.Ventas.Entities;
using Jaeger.Util;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CP.Forms.Almacen.PT {
    public partial class RemisionFiscalControl : UserControl {
        private bool _ReadOnly = false;
        private string _DataError = string.Empty;
        public RemisionDetailModel Comprobante;
        public Aplication.Almacen.PT.Contracts.IRemisionService Service;
        public Aplication.Cotizador.Contracts.IVendedorService VendedorService;

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public RemisionFiscalControl() {
            InitializeComponent();
        }

        private void RemisionFiscalControl_Load(object sender, EventArgs e) {
            this.TDocumento.Nuevo.Click += this.Nuevo_Click;
            this.TDocumento.Actualizar.Click += this.Actualizar_Click;
            this.TDocumento.Guardar.Click += this.Guardar_Click;
            this.TDocumento.Imprimir.Click += this.Imprimir_Click;
            this.TDocumento.FilePDF.Click += this.FilePDF_Click;

            this.TDocumento.Status.DataSource = RemisionService.GetStatus();
            this.TDocumento.Status.SetReadOnly(true);

            this.MetodoEnvio.DataSource = RemisionService.GetMetodoEnvio();
            this.MetodoEnvio.SelectedIndex = -1;
            this.MetodoEnvio.SelectedValue = null;

            if (ConfigService.Synapsis != null) {
                this.VendedorService = new VendedorService();
                if (this.VendedorService != null)
                    this.Vendedor.DataSource = this.VendedorService.GetList<VendedorModel>(Aplication.Cotizador.Services.VendedorService.Query().OnlyActive().Build());
            }

            this.Condiciones.DataSource = RemisionService.GetCondicionPago();
            this.Receptor.Selected += this.Receptor_Selected;
            if (ConfigService.Synapsis != null)
                this.TDocumento.Emisor.Text = ConfigService.Synapsis.Empresa.RFC;
        }

        private void FilePDF_Click(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(this.Comprobante.UrlFilePDF) && !string.IsNullOrEmpty(this.Comprobante.IdDocumento)) {
                var openFileDialog = new OpenFileDialog() { Filter = "*.pdf|*.PDF", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
                if (openFileDialog.ShowDialog() != DialogResult.OK) {
                    return;
                }

                if (System.IO.File.Exists(openFileDialog.FileName) == false) {
                    RadMessageBox.Show(this, "No se pudo tener acceso al archivo: " + openFileDialog.FileName, "Reemplazar archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
                this.Comprobante.Tag = openFileDialog.FileName;
                using (var espera = new Waiting1Form(this.UploadPDF)) {
                    espera.Text = "Cargando ...";
                    espera.ShowDialog(this);
                }
            } else {
                if (ValidacionService.URL(this.Comprobante.UrlFilePDF)) {
                    var savefiledialog = new SaveFileDialog {
                        FileName = System.IO.Path.GetFileName(this.Comprobante.UrlFilePDF)
                    };

                    if (savefiledialog.ShowDialog(this) != DialogResult.OK)
                        return;

                    if (FileService.DownloadFile(this.Comprobante.UrlFilePDF, savefiledialog.FileName)) {
                        DialogResult dr = RadMessageBox.Show(this, String.Concat("Se descargo correctamente el archivo. ", savefiledialog.FileName, " ¿Quieres abrir el documento?"), "Descarga", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                        if (dr == DialogResult.Yes) {
                            try {
                                System.Diagnostics.Process.Start(savefiledialog.FileName);
                            } catch (Exception ex) {
                                string message = string.Format("El archivo no se puede abrir en su sistema.\nError message: {0}", ex.Message);
                                RadMessageBox.Show(this, message, "Abrir archivo", MessageBoxButtons.OK, RadMessageIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        private void Receptor_Selected(object sender, ContribuyenteDetailModel e) {
            if (e != null) {
                if (e.Credito > 0) {
                    this.Comprobante.CondicionPago = RemisionService.GetCondicionPago((int)RemisionCondicionesPagoEnum.ConCredito).Descriptor;
                } else {
                    this.Comprobante.CondicionPago = RemisionService.GetCondicionPago((int)RemisionCondicionesPagoEnum.PagoContraEntrega).Descriptor;
                }
                this.Comprobante.Formato = e.Remisionado;
                this.Recibe.DataSource = e.Contactos;
                this.Recibe.DisplayMember = "Contacto";
                this.Recibe.ValueMember = "Contacto";
            }
        }

        public bool ReadOnly {
            get { return _ReadOnly; }
            set {
                _ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        public virtual void Nuevo_Click(object sender, EventArgs e) {
            this.Comprobante = null;
            this.TDocumento.Actualizar.PerformClick();
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            if (this.Comprobante == null) {
                this.Comprobante = (RemisionDetailModel)RemisionService.Create().Build();
            } else {
                if (this.Comprobante.IdRemision > 0) {
                    using (var espera = new Waiting1Form(this.Cargar)) {
                        espera.ShowDialog(this);
                    }
                }
            }

            if (this.Comprobante.IsEditable) {
                this.Receptor.IdClase = 2;
                this.Receptor.Init();
                this.Receptor.bkWorker.RunWorkerCompleted += this.BkWorker_RunWorkerCompleted;
            } else {
                this.CreateBinding();
            }
        }

        private void BkWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e) {
            this.CreateBinding();
        }

        public virtual void Guardar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, "Properties.Resources.msg_ErrorCaptura", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                if (!string.IsNullOrEmpty(this._DataError)) {
                    RadMessageBox.Show(this, this._DataError, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
                return;
            }

            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.ShowDialog(this);
            }

            this.Actualizar_Click(sender, e);
        }

        public virtual void Imprimir_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, "Properties.Resources.msg_ErrorCaptura", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var espera = new Waiting2Form(this.GetPrinter)) {
                espera.ShowDialog(this);
            }
            if (!string.IsNullOrEmpty(this.Comprobante.IdDocumento)) {
                var printer = new RemisionDetailPrinter(this.Comprobante);
                printer.Formato = Domain.Base.ValueObjects.RemisionFormatoEnum.SinPagare;
                var reporte = new UI.Almacen.PT.Forms.ReporteForm(printer);
                reporte.Show();
            }
            this.Actualizar_Click(sender, e);
        }

        public virtual void UploadPDF() {
            var d1 = this.Service.UpdatePDF(this.Comprobante);
        }

        #region metodos privados
        public virtual void CreateBinding() {
            this.TDocumento.Status.DataBindings.Clear();
            this.TDocumento.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.lblVersion.DataBindings.Clear();
            this.lblVersion.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Folio.DataBindings.Clear();
            this.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.IdSerie.DataBindings.Clear();
            this.IdSerie.SetEditable(this.Comprobante.IsEditable);
            this.IdSerie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Nombre.DataBindings.Clear();
            this.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.RFC.DataBindings.Clear();
            this.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.IdDocumento.DataBindings.Clear();
            this.TDocumento.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Clave.DataBindings.Clear();
            this.Receptor.Clave.DataBindings.Add("Text", this.Comprobante, "ReceptorClave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.IdDirectorio.DataBindings.Clear();
            this.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.IdDomicilio.DataBindings.Clear();
            this.Receptor.IdDomicilio.DataBindings.Add("Text", this.Comprobante, "IdDomicilio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Domicilio.DataBindings.Clear();
            this.Receptor.Domicilio.DataBindings.Add("Text", this.Comprobante, "DomicilioEntrega", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FactorPactado.DataBindings.Clear();
            this.FactorPactado.DataBindings.Add("Value", this.Comprobante, "FactorPactado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TasaIVAPactado.DataBindings.Clear();
            this.TasaIVAPactado.DataBindings.Add("Value", this.Comprobante, "TasaIVAPactado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Recibe.DataBindings.Clear();
            this.Recibe.SetEditable(this.Comprobante.IsEditable);
            this.Recibe.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Guia.DataBindings.Clear();
            this.Guia.ReadOnly = !this.Comprobante.IsEditable;
            this.Guia.DataBindings.Add("Text", this.Comprobante, "NoGuia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Vendedor.DataBindings.Clear();
            this.Vendedor.SetEditable(this.Comprobante.IsEditable);
            this.Vendedor.DataBindings.Add("Text", this.Comprobante, "Vendedor", true, DataSourceUpdateMode.OnPropertyChanged);
            this.IdVendedor.DataBindings.Clear();
            this.IdVendedor.DataBindings.Add("Value", this.Comprobante, "IdVendedor", false, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoCambio.DataBindings.Clear();
            this.TipoCambio.SetEditable(this.Comprobante.IsEditable);
            this.TipoCambio.DataBindings.Add("Value", this.Comprobante, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Decimales.DataBindings.Clear();
            this.Decimales.SetEditable(this.Comprobante.IsEditable);
            this.Decimales.DataBindings.Add("Value", this.Comprobante, "PrecisionDecimal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Nota.DataBindings.Clear();
            this.Nota.ReadOnly = !this.Comprobante.IsEditable;
            this.Nota.DataBindings.Add("Text", this.Comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.MetodoEnvio.DataBindings.Clear();
            this.MetodoEnvio.SetEditable(this.Comprobante.IsEditable);
            this.MetodoEnvio.DataBindings.Add("SelectedValue", this.Comprobante, "IdMetodoEnvio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.MetodoPago.DataBindings.Clear();
            this.MetodoPago.SetEditable(this.Comprobante.IsEditable);
            this.MetodoPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveMetodoPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FormaPago.DataBindings.Clear();
            this.FormaPago.SetEditable(this.Comprobante.IsEditable);
            this.FormaPago.DataBindings.Add("SelectedValue", this.Comprobante, "ClaveFormaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Condiciones.DataBindings.Clear();
            this.Condiciones.SetEditable(false);
            this.Condiciones.DataBindings.Add("Text", this.Comprobante, "CondicionPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Moneda.DataBindings.Clear();
            this.Moneda.SetEditable(this.Comprobante.IsEditable);
            this.Moneda.DataBindings.Add("Text", this.Comprobante, "ClaveMoneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEmision.DataBindings.Clear();
            this.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEntrega.DataBindings.Clear();
            this.FechaEntrega.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            this.FechaEntrega.DateTimePickerElement.ReadOnly = true;
            this.FechaEntrega.DateTimePickerElement.TextBoxElement.TextAlign = HorizontalAlignment.Center;
            this.FechaEntrega.DataBindings.Add("Value", this.Comprobante, "FechaEntrega", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaEntrega.SetEditable(this.Comprobante.IsEditable);

            this.IdPedido.DataBindings.Clear();
            this.IdPedido.DataBindings.Add("Text", this.Comprobante, "IdPedido", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Creo.DataBindings.Clear();
            this.Creo.ReadOnly = true;
            this.Creo.DataBindings.Add("Text", this.Comprobante, "Creo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.FilePDF.DataBindings.Clear();
            this.TDocumento.FilePDF.DataBindings.Add("Enabled", this.Comprobante, "IdRemision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.Imprimir.DataBindings.Clear();
            this.TDocumento.Imprimir.DataBindings.Add("Enabled", this.Comprobante, "IdRemision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDocumento.Guardar.DataBindings.Clear();
            this.TDocumento.Guardar.DataBindings.Add("Enabled", this.Comprobante, "IsEditable", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Receptor.ReadOnly = !this.Comprobante.IsEditable;
            this.OnBindingClompleted(null);
        }

        private void CreateReadOnly() { }

        private void Cargar() {
            this.Comprobante = this.Service.GetById(this.Comprobante.IdRemision);
        }

        private void Guardar() {
            this.Comprobante = this.Service.Save(this.Comprobante);
        }

        private void GetPrinter() {
            this.Comprobante = this.Service.GetPrinter(this.Comprobante.IdRemision);
        }

        public virtual bool Validar() {
            this._DataError = string.Empty;
            this.Advertencia.Clear();
            if (this.Comprobante.IdCliente == 0 || string.IsNullOrEmpty(this.Comprobante.ReceptorNombre)) {
                this.Receptor.Advertencia.SetError(this.Receptor.Nombre, "Selecciona un receptor válido para este comprobante.");
                return false;
            }

            if (this.Comprobante.Conceptos != null) {
                if (this.Comprobante.Conceptos.Count == 0) {
                    this._DataError = "Faltan conceptos para este comprobante";
                    return false;
                } else {
                    var d0 = this.Comprobante.Conceptos.Where(it => it.Cantidad == 0).Count();
                    if (d0 > 0) {
                        this._DataError = "Uno o más conceptos no tienen una cantidad válida.";
                        return false;
                    }
                }
            } else {
                this._DataError = "Objeto no válido.";
                return false;
            }

            if (string.IsNullOrEmpty(this.Comprobante.Vendedor)) {
                this.Advertencia.SetError(this.Vendedor, "Por favor selecciona un vendedor.");
                return false;
            }

            if (string.IsNullOrEmpty(this.Comprobante.Contacto)) {
                this.Advertencia.SetError(this.Recibe, "Por favor ingresa un nombre o referencia.");
                return false;
            }
            return true;
        }
        #endregion
    }
}
