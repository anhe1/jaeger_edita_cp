﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.UI.CP.Forms.Almacen.PT {
    public partial class RemisionFiscalForm : RadForm {
        #region declaraciones
        protected internal RadContextMenu menuContextualConceptos = new RadContextMenu();
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem _menuContextCopiar = new RadMenuItem { Text = "Copiar", Name = "menuContextCopiar" };
        protected internal RadMenuItem menuContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        protected internal RadMenuItem SinDuplicados = new RadMenuItem { Text = "Sin duplicados", CheckOnClick = true, IsChecked = true };
        protected internal bool ShowValues = false;
        #endregion

        public RemisionFiscalForm(Aplication.Almacen.PT.Contracts.IRemisionService service) {
            InitializeComponent();
            this.Text = "Remisión: Nuevo";
            this.TRemision.Service = service;
        }

        public RemisionFiscalForm(Aplication.Almacen.PT.Contracts.IRemisionService service, RemisionDetailModel model) {
            InitializeComponent();
            this.Text = "Remisión: Nuevo";
            this.TRemision.Service = service;
            this.TRemision.Comprobante = model;
        }

        private void RemisionFiscalForm_Load(object sender, EventArgs e) {
            UI.Almacen.PT.Builder.IRemisionadoGridBuilder d0 = new UI.Almacen.PT.Builder.RemisionadoGridBuilder();
            this.GridConceptos.Columns.AddRange(d0.Templetes().ConceptosSinValores().Build());
            this.GridConceptos.ContextMenuOpening += this.GridConceptos_ContextMenuOpening;

            this.PanelTotales.Visible = this.ShowValues;

            this.TRemision.BindingCompleted += this.TRemision_BindingCompleted;
            this.TRemision.Actualizar_Click(sender, e);

            this.TConceptos.Nuevo.Click += this.TConcepto_Nuevo_Click;
            this.TConceptos.Remover.Click += this.TConcepto_Remove_Click;
            this.TConceptos.Duplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Productos.Click += this.TConcepto_Productos_Click;

            this.TRemision.TDocumento.Cerrar.Click += this.Cerrar_Click;

            this.menuContextualConceptos.Items.Add(this.menuContextAplicar);
            this.menuContextualConceptos.Items.Add(this._menuContextCopiar);
            this.menuContextAplicar.Click += this.MenuContextAplicar_Click;
            this._menuContextCopiar.Click += MenuContextCopiar_Click;

        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TRemision_BindingCompleted(object sender, EventArgs e) {
            this.GridConceptos.DataSource = this.TRemision.Comprobante.Conceptos;
            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.TRemision.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.TRemision.Comprobante, "TotalDescuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TrasladoIVA.DataBindings.Clear();
            this.TrasladoIVA.DataBindings.Add("Value", this.TRemision.Comprobante, "TotalTrasladoIVA", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.TRemision.Comprobante, "Total", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TConceptos.IsEditable = this.TRemision.Comprobante.IsEditable;
            this.TConceptoParte.IsEditable = this.TRemision.Comprobante.IsEditable;
            this.GridConceptos.ReadOnly = !this.TRemision.Comprobante.IsEditable;
            this.GridConceptoParte.ReadOnly = !this.TRemision.Comprobante.IsEditable;
            this.TConceptos.Nuevo.Enabled = false;
            this.TConceptos.Agregar.Enabled = false;
            this.TConceptos.Duplicar.Enabled = false;
            if (this.TRemision.Comprobante.Folio > 0) {
                this.Text = string.Format("Remisión: {0}", this.TRemision.Comprobante.Folio.ToString("#000000"));
            }
        }

        #region barra de herramientas conceptos del comprobante
        public virtual void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            if (this.TRemision.Comprobante.Conceptos.Count == 0) {
                this.TRemision.Comprobante.Conceptos = new BindingList<RemisionConceptoDetailModel>();
                this.GridConceptos.DataSource = this.TRemision.Comprobante.Conceptos;
            }

            this.TRemision.Comprobante.Conceptos.Add(new RemisionConceptoDetailModel { Activo = true });
        }

        public virtual void TConcepto_Remove_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_Remision_RemoverObjeto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as RemisionConceptoDetailModel;
                    if (seleccionado.IdMovimiento == 0) {
                        try {
                            this.TRemision.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.GridConceptos.Rows.Count == 0) {
                                this.TRemision.Comprobante.Conceptos = new BindingList<RemisionConceptoDetailModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.GridConceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void TConcepto_Duplicar_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as RemisionConceptoDetailModel;
                if (seleccionado != null) {
                    var duplicado = seleccionado.Clone();
                    this.TRemision.Comprobante.Conceptos.Add(duplicado);
                }
            }
        }

        public virtual void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.TRemision.Comprobante.IdCliente == 0) {
                RadMessageBox.Show(this, "Es necesario primero seleccionar un cliente", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var _catalogoProductos = new UI.Cotizador.Forms.Produccion.BuscarForm()) {
                _catalogoProductos.IsVerified = true;
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.Producto_Selected;
                _catalogoProductos.ShowDialog(this);
            }
        }

        private void Producto_Selected(object sender, Domain.Cotizador.Produccion.Entities.OrdenProduccionDetailModel e) {
            if (e != null) {

                if (this.SinDuplicados.IsChecked == true) {
                    var ed = this.TRemision.Comprobante.Conceptos.Where(it => it.IdPedido == e.IdOrden);
                    if (ed.Count() > 0) {
                        RadMessageBox.Show(this, "El producto o modelo ya se encuentra en la lista actual.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                }
                this.TRemision.Comprobante.Conceptos.Add(RemisionService.Convertir(e));
            }
        }
        #endregion

        #region menu contextual
        public virtual void MenuContextAplicar_Click(object sender, EventArgs e) {
            var d = this.GridConceptos.CurrentCell.Value;
            foreach (var item in this.GridConceptos.Rows) {
                item.Cells[this.GridConceptos.CurrentColumn.Index].Value = d;
            }
        }

        public virtual void MenuContextCopiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.GridConceptos.CurrentCell.Value.ToString());
        }

        #endregion

        #region acciones del grid de conceptos
        public virtual void GridConceptos_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (this.TRemision.Comprobante.IsEditable == false) {
                return;
            }
            if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridConceptos.CurrentRow.ViewInfo.ViewTemplate == this.GridConceptos.MasterTemplate) {
                    this.menuContextAplicar.Enabled = this.GridConceptos.CurrentColumn.Name == "ClaveUnidad" |
                        this.GridConceptos.CurrentColumn.Name == "Unidad" |
                        this.GridConceptos.CurrentColumn.Name == "NumPedido" |
                        this.GridConceptos.CurrentColumn.Name == "Cantidad" |
                        this.GridConceptos.CurrentColumn.Name == "ValorUnitario" |
                        this.GridConceptos.CurrentColumn.Name == "ClaveProducto" |
                        this.GridConceptos.CurrentColumn.Name == "ObjetoImp";
                }
                e.ContextMenu = this.menuContextualConceptos.DropDown;
            }
        }
        #endregion
    }
}
