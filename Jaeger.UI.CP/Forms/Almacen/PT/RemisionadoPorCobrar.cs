﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Almacen.PT {
    public class RemisionadoPorCobrar : UI.Almacen.PT.Forms.RemisionadoForm {
        public RemisionadoPorCobrar(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Remisionado: Por Cobrar";
        }
    }
}
