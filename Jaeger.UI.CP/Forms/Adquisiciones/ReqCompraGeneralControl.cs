﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.CP.Contracts;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Aplication.Cotizador.Contracts;

namespace Jaeger.UI.CP.Forms.Adquisiciones {
    public partial class ReqCompraGeneralControl : UserControl {
        protected BackgroundWorker preparar_contribuyentes;
        protected IContribuyenteService service;
        protected IDepartamentosService departamentos;
        protected Aplication.Cotizador.Contracts.IVendedorService vendedor;

        public ReqCompraGeneralControl() {
            InitializeComponent();
        }

        private void RemisionGeneralControl_Load(object sender, EventArgs e) {
            this.Enabled = false;

            this.preparar_contribuyentes = new BackgroundWorker();
            this.preparar_contribuyentes.DoWork += PrepararContribuyentes_DoWork;
            this.preparar_contribuyentes.RunWorkerCompleted += PrepararContribuyentes_RunWorkerCompleted;

            this.Folio.KeyPress += new KeyPressEventHandler(HelperTelerikExtension.TxbFolio_KeyPress);

            this.FechaEmision.SetEditable(false);
        }

        public virtual void Start() {
            this.departamentos = new DepartamentosService();
            this.vendedor = new VendedorService();

            this.Departamento.DataSource = this.departamentos.GetDepartamentos();
            this.Departamento.SelectedIndex = -1;
            this.Departamento.SelectedValue = null;

            this.IdPrioridad.DataSource = ReqCompraService.GetPrioridad();
            this.IdPrioridad.SelectedIndex = -1;
            this.IdPrioridad.SelectedValue = null;

            this.IdVendedor.DataSource = this.vendedor.GetList<VendedorModel>(VendedorService.Query().OnlyActive().Build());
            this.Receptor.Init();
            this.Enabled = true;
        }

        public void LoadContribuyente(CFDISubTipoEnum subTipo) {
            
            this.Receptor.Init();
        }

        private void IdVendedor_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.IdVendedor.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var vendedor = temporal.DataBoundItem as VendedorModel;
                if (vendedor != null) {
                    this.IdVendedor2.Value = vendedor.IdVendedor;
                } else {
                    RadMessageBox.Show(this, "Properties.Resources.Message_Objeto_NoValido", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        private void PrepararContribuyentes_DoWork(object sender, DoWorkEventArgs e) {
            this.Receptor.Init();

        }

        private void PrepararContribuyentes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

            this.IdVendedor.SelectedValue = null;
            this.IdVendedor.SelectedIndex = -1;
            this.IdVendedor.SelectedValueChanged += this.IdVendedor_SelectedValueChanged;

        }
    }
}
