﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Base;
using Jaeger.Util.Services;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Cotizador.Produccion.Entities;

namespace Jaeger.UI.CP.Forms.Adquisiciones {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private readonly EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.Almacen.DP");
        private readonly EmbeddedResources domainCP = new EmbeddedResources("Jaeger.Domain.CP");

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(ReqCompraPrinter)) {
                this.CrearReqCompra();
            } else if (this.CurrentObject.GetType() == typeof(OrdenProduccionEtiquetaPrinter)) {
                this.CrearEtiquetas();
            } else if (this.CurrentObject.GetType() == typeof(MembretePrinter)) {
                this.CrearMembrete();
            }
        }

        private void CrearRemision() {
            var current = (RemisionDetailPrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Almacen.DP.Reports.RemisionInternav20.rdlc");

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Remision");
            var d = DbConvert.ConvertToDataTable<RemisionDetailPrinter>(new List<RemisionDetailPrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearRemisionL() {
            var current = (IEnumerable<RemisionDetailPrinter>)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Almacen.DP.Reports.RemisionInternav20L.rdlc");

            this.Procesar();
            this.SetDisplayName("Remision");
            var d = DbConvert.ConvertToDataTable<RemisionDetailPrinter>(current.ToList());
            this.SetDataSource("Comprobante", d);
            this.Finalizar();
        }

        private void CrearReqCompra() {
            var current = (ReqCompraPrinter)this.CurrentObject;
            this.LoadDefinition = this.domainCP.GetStream("Jaeger.Domain.CP.Empresa.Reports.ReqCompraV20.rdlc");

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Remision");
            var d = DbConvert.ConvertToDataTable<ReqCompraPrinter>(new List<ReqCompraPrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearEtiquetas() {
            var current = (OrdenProduccionEtiquetaPrinter)this.CurrentObject;
            this.LoadDefinition = this.domainCP.GetStream("Jaeger.Domain.CP.Produccion.Reports.EtiquetaReportev20.rdlc");
            
            var etiquetas = new List<OrdenProduccionEtiquetaPrinter>();
            for (int i = 0; i < current.Duplicado; i++) {
                etiquetas.Add(new OrdenProduccionEtiquetaPrinter(current));
            }
            this.Procesar();
            this.SetDisplayName("Etiquetado");
            var d = DbConvert.ConvertToDataTable<OrdenProduccionEtiquetaPrinter>(etiquetas);
            this.SetDataSource("Etiqueta", d);
            this.Finalizar();
        }

        private void CrearMembrete() {
            var current = (MembretePrinter)this.CurrentObject;
            if (current.Formato == 1) {         // produccion en proceso
                this.LoadDefinition = this.domainCP.GetStream("Jaeger.Domain.CP.Produccion.Reports.Membrete1Reportev20.rdlc");
            } else if (current.Formato == 2) { // producto terminado
                this.LoadDefinition = this.domainCP.GetStream("Jaeger.Domain.CP.Produccion.Reports.Membrete2Reportev20.rdlc");
            } else if (current.Formato == 3) { // maquila
                this.LoadDefinition = this.domainCP.GetStream("Jaeger.Domain.CP.Produccion.Reports.Membrete3Reportev20.rdlc");
            } else { // produccion en proceso
                this.LoadDefinition = this.domainCP.GetStream("Jaeger.Domain.CP.Produccion.Reports.Membrete1Reportev20.rdlc");
            }

            this.Procesar();
            this.SetDisplayName("Etiquetado");
            var d = DbConvert.ConvertToDataTable<MembretePrinter>(new List<MembretePrinter>() { current });
            this.SetDataSource("Membrete", d);
            this.Finalizar();
        }
    }
}
