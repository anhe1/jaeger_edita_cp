﻿namespace Jaeger.UI.CP.Forms.Adquisiciones {
    partial class ReqsCompraForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TRequierimiento = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.gridCompras = new Telerik.WinControls.UI.RadGridView();
            this.gridConceptos = new Telerik.WinControls.UI.GridViewTemplate();
            ((System.ComponentModel.ISupportInitialize)(this.gridCompras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCompras.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TRequierimiento
            // 
            this.TRequierimiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TRequierimiento.Enabled = false;
            this.TRequierimiento.Location = new System.Drawing.Point(0, 0);
            this.TRequierimiento.Name = "TRequierimiento";
            this.TRequierimiento.ShowActualizar = true;
            this.TRequierimiento.ShowAutosuma = false;
            this.TRequierimiento.ShowCancelar = true;
            this.TRequierimiento.ShowCerrar = true;
            this.TRequierimiento.ShowEditar = false;
            this.TRequierimiento.ShowEjercicio = true;
            this.TRequierimiento.ShowExportarExcel = false;
            this.TRequierimiento.ShowFiltro = true;
            this.TRequierimiento.ShowHerramientas = false;
            this.TRequierimiento.ShowImprimir = true;
            this.TRequierimiento.ShowNuevo = true;
            this.TRequierimiento.ShowPeriodo = true;
            this.TRequierimiento.Size = new System.Drawing.Size(1001, 30);
            this.TRequierimiento.TabIndex = 0;
            // 
            // gridCompras
            // 
            this.gridCompras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCompras.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridCompras.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridConceptos});
            this.gridCompras.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridCompras.Name = "gridCompras";
            this.gridCompras.ShowGroupPanel = false;
            this.gridCompras.Size = new System.Drawing.Size(1001, 512);
            this.gridCompras.TabIndex = 5;
            // 
            // gridConceptos
            // 
            this.gridConceptos.Caption = "Conceptos";
            this.gridConceptos.ViewDefinition = tableViewDefinition1;
            // 
            // ReqsCompraForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 542);
            this.Controls.Add(this.gridCompras);
            this.Controls.Add(this.TRequierimiento);
            this.Name = "ReqsCompraForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Req. Compra";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReqsCompraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridCompras.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCompras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarCommonControl TRequierimiento;
        protected internal Telerik.WinControls.UI.RadGridView gridCompras;
        private Telerik.WinControls.UI.GridViewTemplate gridConceptos;
    }
}