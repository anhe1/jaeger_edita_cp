﻿namespace Jaeger.UI.CP.Forms.Adquisiciones {
    partial class ReqCompraGeneralControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TipoCambio = new Telerik.WinControls.UI.RadSpinEditor();
            this.RadLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaEntrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaEntrega = new Telerik.WinControls.UI.RadLabel();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.IdSerie = new Telerik.WinControls.UI.RadDropDownList();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Departamento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.IdOrdenCompra = new Telerik.WinControls.UI.RadTextBox();
            this.IdPrioridad = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.lblSerie = new Telerik.WinControls.UI.RadLabel();
            this.lblCreo = new Telerik.WinControls.UI.RadLabel();
            this.Creo = new Telerik.WinControls.UI.RadTextBox();
            this.lblPrioridad = new Telerik.WinControls.UI.RadLabel();
            this.lblTipoCambio = new Telerik.WinControls.UI.RadLabel();
            this.lblMoneda = new Telerik.WinControls.UI.RadLabel();
            this.lblOrdenCompra = new Telerik.WinControls.UI.RadLabel();
            this.lblDepartamento = new Telerik.WinControls.UI.RadLabel();
            this.lblVendedor = new Telerik.WinControls.UI.RadLabel();
            this.IdVendedor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblVersion = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.IdDepartamento = new Telerik.WinControls.UI.RadSpinEditor();
            this.TasaIVAPactado = new Telerik.WinControls.UI.RadSpinEditor();
            this.FactorPactado = new Telerik.WinControls.UI.RadSpinEditor();
            this.IdVendedor2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblRecibe = new Telerik.WinControls.UI.RadLabel();
            this.Contacto = new Telerik.WinControls.UI.RadDropDownList();
            this.lblDecimales = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.Decimales = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.directorioControl1 = new Cotizador.Forms.Empresa.DirectorioControl();
            this.Receptor = new Cotizador.Forms.Empresa.DirectorioControl();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdOrdenCompra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPrioridad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPrioridad.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPrioridad.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrioridad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrdenCompra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TasaIVAPactado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FactorPactado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecibe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            this.SuspendLayout();
            // 
            // TipoCambio
            // 
            this.TipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCambio.DecimalPlaces = 2;
            this.TipoCambio.Enabled = false;
            this.TipoCambio.Location = new System.Drawing.Point(993, 31);
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.Size = new System.Drawing.Size(54, 20);
            this.TipoCambio.TabIndex = 22;
            this.TipoCambio.TabStop = false;
            this.TipoCambio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RadLabel9
            // 
            this.RadLabel9.Location = new System.Drawing.Point(3, 133);
            this.RadLabel9.Name = "RadLabel9";
            this.RadLabel9.Size = new System.Drawing.Size(67, 18);
            this.RadLabel9.TabIndex = 0;
            this.RadLabel9.Text = "Documento:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Location = new System.Drawing.Point(708, 7);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ShowItemToolTips = false;
            this.FechaEmision.Size = new System.Drawing.Size(211, 20);
            this.FechaEmision.TabIndex = 6;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEntrega.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaEntrega.Location = new System.Drawing.Point(810, 31);
            this.FechaEntrega.Name = "FechaEntrega";
            this.FechaEntrega.NullText = "--/--/----";
            this.FechaEntrega.ReadOnly = true;
            this.FechaEntrega.Size = new System.Drawing.Size(110, 20);
            this.FechaEntrega.TabIndex = 20;
            this.FechaEntrega.TabStop = false;
            this.FechaEntrega.Text = "05/12/2017";
            this.FechaEntrega.Value = new System.DateTime(2017, 12, 5, 22, 46, 27, 169);
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEmision.Location = new System.Drawing.Point(634, 8);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(70, 18);
            this.lblFechaEmision.TabIndex = 5;
            this.lblFechaEmision.Text = "Fec. Emisión:";
            // 
            // lblFechaEntrega
            // 
            this.lblFechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEntrega.Location = new System.Drawing.Point(711, 32);
            this.lblFechaEntrega.Name = "lblFechaEntrega";
            this.lblFechaEntrega.Size = new System.Drawing.Size(97, 18);
            this.lblFechaEntrega.TabIndex = 19;
            this.lblFechaEntrega.Text = "Fec. de Requerida:";
            // 
            // Folio
            // 
            this.Folio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.Location = new System.Drawing.Point(276, 7);
            this.Folio.Name = "Folio";
            this.Folio.NullText = "Folio";
            this.Folio.Size = new System.Drawing.Size(108, 20);
            this.Folio.TabIndex = 4;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IdSerie
            // 
            this.IdSerie.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.IdSerie.Location = new System.Drawing.Point(92, 7);
            this.IdSerie.Name = "IdSerie";
            this.IdSerie.NullText = "Serie";
            this.IdSerie.Size = new System.Drawing.Size(125, 20);
            this.IdSerie.TabIndex = 2;
            // 
            // Moneda
            // 
            this.Moneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Moneda.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Enabled = false;
            this.Moneda.Location = new System.Drawing.Point(1106, 31);
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(67, 20);
            this.Moneda.TabIndex = 24;
            this.Moneda.TabStop = false;
            // 
            // Departamento
            // 
            this.Departamento.AutoSizeDropDownToBestFit = true;
            this.Departamento.DisplayMember = "Descriptor";
            this.Departamento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Departamento.NestedRadGridView
            // 
            this.Departamento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "IdDepto";
            gridViewTextBoxColumn3.HeaderText = "ID";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn4.FieldName = "Nombre";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn5.FieldName = "Descriptor";
            gridViewTextBoxColumn5.HeaderText = "Descriptor";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Descriptor";
            this.Departamento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.Departamento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamento.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Departamento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Departamento.EditorControl.Name = "NestedRadGridView";
            this.Departamento.EditorControl.ReadOnly = true;
            this.Departamento.EditorControl.ShowGroupPanel = false;
            this.Departamento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamento.EditorControl.TabIndex = 0;
            this.Departamento.Location = new System.Drawing.Point(92, 31);
            this.Departamento.Name = "Departamento";
            this.Departamento.NullText = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(292, 20);
            this.Departamento.TabIndex = 13;
            this.Departamento.TabStop = false;
            this.Departamento.ValueMember = "IdDepto";
            // 
            // IdOrdenCompra
            // 
            this.IdOrdenCompra.Location = new System.Drawing.Point(618, 31);
            this.IdOrdenCompra.MaxLength = 32;
            this.IdOrdenCompra.Name = "IdOrdenCompra";
            this.IdOrdenCompra.NullText = "Ord. Compra";
            this.IdOrdenCompra.Size = new System.Drawing.Size(88, 20);
            this.IdOrdenCompra.TabIndex = 17;
            // 
            // IdPrioridad
            // 
            this.IdPrioridad.AutoSizeDropDownHeight = true;
            this.IdPrioridad.AutoSizeDropDownToBestFit = true;
            this.IdPrioridad.DisplayMember = "Descriptor";
            this.IdPrioridad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // IdPrioridad.NestedRadGridView
            // 
            this.IdPrioridad.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.IdPrioridad.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdPrioridad.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.IdPrioridad.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.IdPrioridad.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.IdPrioridad.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.IdPrioridad.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn6.DataType = typeof(int);
            gridViewTextBoxColumn6.FieldName = "Id";
            gridViewTextBoxColumn6.HeaderText = "Clave";
            gridViewTextBoxColumn6.Name = "Id";
            gridViewTextBoxColumn7.FieldName = "Descriptor";
            gridViewTextBoxColumn7.HeaderText = "Descriptor";
            gridViewTextBoxColumn7.Name = "Descriptor";
            this.IdPrioridad.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.IdPrioridad.EditorControl.MasterTemplate.EnableGrouping = false;
            this.IdPrioridad.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.IdPrioridad.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.IdPrioridad.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.IdPrioridad.EditorControl.Name = "NestedRadGridView";
            this.IdPrioridad.EditorControl.ReadOnly = true;
            this.IdPrioridad.EditorControl.ShowGroupPanel = false;
            this.IdPrioridad.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.IdPrioridad.EditorControl.TabIndex = 0;
            this.IdPrioridad.Location = new System.Drawing.Point(444, 31);
            this.IdPrioridad.Name = "IdPrioridad";
            this.IdPrioridad.NullText = "Prioridad";
            this.IdPrioridad.Size = new System.Drawing.Size(132, 20);
            this.IdPrioridad.TabIndex = 15;
            this.IdPrioridad.TabStop = false;
            this.IdPrioridad.ValueMember = "Id";
            // 
            // lblFolio
            // 
            this.lblFolio.Location = new System.Drawing.Point(228, 8);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 3;
            this.lblFolio.Text = "Folio:";
            // 
            // lblSerie
            // 
            this.lblSerie.Location = new System.Drawing.Point(53, 8);
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(33, 18);
            this.lblSerie.TabIndex = 1;
            this.lblSerie.Text = "Serie:";
            // 
            // lblCreo
            // 
            this.lblCreo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCreo.Location = new System.Drawing.Point(954, 8);
            this.lblCreo.Name = "lblCreo";
            this.lblCreo.Size = new System.Drawing.Size(32, 18);
            this.lblCreo.TabIndex = 7;
            this.lblCreo.Text = "Creó:";
            // 
            // Creo
            // 
            this.Creo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Creo.Location = new System.Drawing.Point(993, 7);
            this.Creo.MaxLength = 10;
            this.Creo.Name = "Creo";
            this.Creo.NullText = "Creó";
            this.Creo.Size = new System.Drawing.Size(82, 20);
            this.Creo.TabIndex = 9;
            this.Creo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPrioridad
            // 
            this.lblPrioridad.Location = new System.Drawing.Point(387, 32);
            this.lblPrioridad.Name = "lblPrioridad";
            this.lblPrioridad.Size = new System.Drawing.Size(54, 18);
            this.lblPrioridad.TabIndex = 14;
            this.lblPrioridad.Text = "Prioridad:";
            // 
            // lblTipoCambio
            // 
            this.lblTipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoCambio.Location = new System.Drawing.Point(928, 32);
            this.lblTipoCambio.Name = "lblTipoCambio";
            this.lblTipoCambio.Size = new System.Drawing.Size(59, 18);
            this.lblTipoCambio.TabIndex = 21;
            this.lblTipoCambio.Text = "T. Cambio:";
            // 
            // lblMoneda
            // 
            this.lblMoneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneda.Location = new System.Drawing.Point(1050, 32);
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(50, 18);
            this.lblMoneda.TabIndex = 23;
            this.lblMoneda.Text = "Moneda:";
            // 
            // lblOrdenCompra
            // 
            this.lblOrdenCompra.Location = new System.Drawing.Point(580, 32);
            this.lblOrdenCompra.Name = "lblOrdenCompra";
            this.lblOrdenCompra.Size = new System.Drawing.Size(32, 18);
            this.lblOrdenCompra.TabIndex = 16;
            this.lblOrdenCompra.Text = "O. C.:";
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.Location = new System.Drawing.Point(5, 32);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(81, 18);
            this.lblDepartamento.TabIndex = 12;
            this.lblDepartamento.Text = "Departamento:";
            // 
            // lblVendedor
            // 
            this.lblVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVendedor.Location = new System.Drawing.Point(928, 56);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.Size = new System.Drawing.Size(58, 18);
            this.lblVendedor.TabIndex = 30;
            this.lblVendedor.Text = "Vendedor:";
            // 
            // IdVendedor
            // 
            this.IdVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdVendedor.AutoSizeDropDownToBestFit = true;
            this.IdVendedor.DisplayMember = "Clave";
            this.IdVendedor.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // IdVendedor.NestedRadGridView
            // 
            this.IdVendedor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.IdVendedor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdVendedor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.IdVendedor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.IdVendedor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.IdVendedor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.IdVendedor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "IdVendedor";
            gridViewTextBoxColumn8.HeaderText = "IdVendedor";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "IdVendedor";
            gridViewTextBoxColumn9.FieldName = "Clave";
            gridViewTextBoxColumn9.HeaderText = "Vendedor";
            gridViewTextBoxColumn9.Name = "Clave";
            gridViewTextBoxColumn10.FieldName = "Nombre";
            gridViewTextBoxColumn10.HeaderText = "Nombre";
            gridViewTextBoxColumn10.Name = "Nombre";
            gridViewTextBoxColumn10.Width = 150;
            this.IdVendedor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.IdVendedor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.IdVendedor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.IdVendedor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.IdVendedor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.IdVendedor.EditorControl.Name = "NestedRadGridView";
            this.IdVendedor.EditorControl.ReadOnly = true;
            this.IdVendedor.EditorControl.ShowGroupPanel = false;
            this.IdVendedor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.IdVendedor.EditorControl.TabIndex = 0;
            this.IdVendedor.Location = new System.Drawing.Point(993, 55);
            this.IdVendedor.Name = "IdVendedor";
            this.IdVendedor.NullText = "Vendedor";
            this.IdVendedor.Size = new System.Drawing.Size(180, 20);
            this.IdVendedor.TabIndex = 31;
            this.IdVendedor.TabStop = false;
            this.IdVendedor.ValueMember = "IdVendedor";
            // 
            // lblVersion
            // 
            this.lblVersion.Location = new System.Drawing.Point(5, 8);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(26, 18);
            this.lblVersion.TabIndex = 91;
            this.lblVersion.Text = "Ver.";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox1.Controls.Add(this.directorioControl1);
            this.groupBox1.Controls.Add(this.IdDepartamento);
            this.groupBox1.Controls.Add(this.TasaIVAPactado);
            this.groupBox1.Controls.Add(this.FactorPactado);
            this.groupBox1.Controls.Add(this.IdVendedor2);
            this.groupBox1.Controls.Add(this.lblRecibe);
            this.groupBox1.Controls.Add(this.Contacto);
            this.groupBox1.Controls.Add(this.lblVersion);
            this.groupBox1.Controls.Add(this.lblVendedor);
            this.groupBox1.Controls.Add(this.IdVendedor);
            this.groupBox1.Controls.Add(this.Departamento);
            this.groupBox1.Controls.Add(this.lblDecimales);
            this.groupBox1.Controls.Add(this.TipoCambio);
            this.groupBox1.Controls.Add(this.RadLabel9);
            this.groupBox1.Controls.Add(this.Nota);
            this.groupBox1.Controls.Add(this.Decimales);
            this.groupBox1.Controls.Add(this.lblDepartamento);
            this.groupBox1.Controls.Add(this.FechaEmision);
            this.groupBox1.Controls.Add(this.lblOrdenCompra);
            this.groupBox1.Controls.Add(this.FechaEntrega);
            this.groupBox1.Controls.Add(this.lblMoneda);
            this.groupBox1.Controls.Add(this.lblFechaEmision);
            this.groupBox1.Controls.Add(this.lblFechaEntrega);
            this.groupBox1.Controls.Add(this.lblTipoCambio);
            this.groupBox1.Controls.Add(this.lblNota);
            this.groupBox1.Controls.Add(this.lblPrioridad);
            this.groupBox1.Controls.Add(this.Folio);
            this.groupBox1.Controls.Add(this.Creo);
            this.groupBox1.Controls.Add(this.IdSerie);
            this.groupBox1.Controls.Add(this.lblCreo);
            this.groupBox1.Controls.Add(this.Moneda);
            this.groupBox1.Controls.Add(this.lblSerie);
            this.groupBox1.Controls.Add(this.lblFolio);
            this.groupBox1.Controls.Add(this.IdPrioridad);
            this.groupBox1.Controls.Add(this.IdOrdenCompra);
            this.groupBox1.Controls.Add(this.Receptor);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.HeaderText = "";
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1180, 104);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            // 
            // IdDepartamento
            // 
            this.IdDepartamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDepartamento.Location = new System.Drawing.Point(1193, 111);
            this.IdDepartamento.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDepartamento.Name = "IdDepartamento";
            this.IdDepartamento.ShowBorder = false;
            this.IdDepartamento.ShowUpDownButtons = false;
            this.IdDepartamento.Size = new System.Drawing.Size(37, 20);
            this.IdDepartamento.TabIndex = 374;
            this.IdDepartamento.TabStop = false;
            // 
            // TasaIVAPactado
            // 
            this.TasaIVAPactado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TasaIVAPactado.DecimalPlaces = 4;
            this.TasaIVAPactado.Location = new System.Drawing.Point(1194, 7);
            this.TasaIVAPactado.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.TasaIVAPactado.Name = "TasaIVAPactado";
            this.TasaIVAPactado.ShowBorder = false;
            this.TasaIVAPactado.ShowUpDownButtons = false;
            this.TasaIVAPactado.Size = new System.Drawing.Size(37, 20);
            this.TasaIVAPactado.TabIndex = 96;
            this.TasaIVAPactado.TabStop = false;
            // 
            // FactorPactado
            // 
            this.FactorPactado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FactorPactado.DecimalPlaces = 4;
            this.FactorPactado.Location = new System.Drawing.Point(1194, 32);
            this.FactorPactado.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.FactorPactado.Name = "FactorPactado";
            this.FactorPactado.ShowBorder = false;
            this.FactorPactado.ShowUpDownButtons = false;
            this.FactorPactado.Size = new System.Drawing.Size(37, 20);
            this.FactorPactado.TabIndex = 30;
            this.FactorPactado.TabStop = false;
            // 
            // IdVendedor2
            // 
            this.IdVendedor2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdVendedor2.Location = new System.Drawing.Point(1194, 79);
            this.IdVendedor2.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdVendedor2.Name = "IdVendedor2";
            this.IdVendedor2.ShowBorder = false;
            this.IdVendedor2.ShowUpDownButtons = false;
            this.IdVendedor2.Size = new System.Drawing.Size(37, 20);
            this.IdVendedor2.TabIndex = 30;
            this.IdVendedor2.TabStop = false;
            // 
            // lblRecibe
            // 
            this.lblRecibe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRecibe.Location = new System.Drawing.Point(580, 56);
            this.lblRecibe.Name = "lblRecibe";
            this.lblRecibe.Size = new System.Drawing.Size(54, 18);
            this.lblRecibe.TabIndex = 27;
            this.lblRecibe.Text = "Contacto:";
            // 
            // Contacto
            // 
            this.Contacto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Contacto.Location = new System.Drawing.Point(638, 55);
            this.Contacto.MaxLength = 32;
            this.Contacto.Name = "Contacto";
            this.Contacto.NullText = "Contacto";
            this.Contacto.Size = new System.Drawing.Size(282, 20);
            this.Contacto.TabIndex = 29;
            // 
            // lblDecimales
            // 
            this.lblDecimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDecimales.Location = new System.Drawing.Point(1080, 8);
            this.lblDecimales.Name = "lblDecimales";
            this.lblDecimales.Size = new System.Drawing.Size(30, 18);
            this.lblDecimales.TabIndex = 10;
            this.lblDecimales.Text = "Dec.:";
            // 
            // Nota
            // 
            this.Nota.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Nota.Location = new System.Drawing.Point(665, 79);
            this.Nota.Name = "Nota";
            this.Nota.NullText = "Observaciones";
            this.Nota.Size = new System.Drawing.Size(508, 20);
            this.Nota.TabIndex = 35;
            // 
            // Decimales
            // 
            this.Decimales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Decimales.Enabled = false;
            this.Decimales.Location = new System.Drawing.Point(1121, 7);
            this.Decimales.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.Decimales.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.Decimales.Name = "Decimales";
            this.Decimales.NullableValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.Decimales.Size = new System.Drawing.Size(52, 20);
            this.Decimales.TabIndex = 11;
            this.Decimales.TabStop = false;
            this.Decimales.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.Decimales.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lblNota
            // 
            this.lblNota.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNota.Location = new System.Drawing.Point(580, 80);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(81, 18);
            this.lblNota.TabIndex = 34;
            this.lblNota.Text = "Observaciones:";
            this.lblNota.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // directorioControl1
            // 
            this.directorioControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.directorioControl1.IdClase = 0;
            this.directorioControl1.LDomicilio = "Domicilio:";
            this.directorioControl1.LNombre = "Proveedor sugerido:";
            this.directorioControl1.Location = new System.Drawing.Point(4, 79);
            this.directorioControl1.Margin = new System.Windows.Forms.Padding(4);
            this.directorioControl1.Name = "directorioControl1";
            this.directorioControl1.ShowDomicilio = false;
            this.directorioControl1.ShowRFC = false;
            this.directorioControl1.Size = new System.Drawing.Size(572, 20);
            this.directorioControl1.TabIndex = 33;
            // 
            // Receptor
            // 
            this.Receptor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Receptor.IdClase = 0;
            this.Receptor.LDomicilio = "Domicilio:";
            this.Receptor.LNombre = "Cliente:";
            this.Receptor.Location = new System.Drawing.Point(6, 55);
            this.Receptor.Margin = new System.Windows.Forms.Padding(4);
            this.Receptor.Name = "Receptor";
            this.Receptor.ShowDomicilio = false;
            this.Receptor.ShowRFC = false;
            this.Receptor.Size = new System.Drawing.Size(570, 20);
            this.Receptor.TabIndex = 26;
            // 
            // ReqCompraGeneralControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(0, 104);
            this.MinimumSize = new System.Drawing.Size(1180, 104);
            this.Name = "ReqCompraGeneralControl";
            this.Size = new System.Drawing.Size(1180, 104);
            this.Load += new System.EventHandler(this.RemisionGeneralControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdOrdenCompra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPrioridad.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPrioridad.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPrioridad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSerie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrioridad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrdenCompra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TasaIVAPactado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FactorPactado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdVendedor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecibe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Contacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDecimales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Decimales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal Telerik.WinControls.UI.RadLabel RadLabel9;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEntrega;
        internal Telerik.WinControls.UI.RadLabel lblFechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblFechaEntrega;
        internal Telerik.WinControls.UI.RadTextBox Folio;
        internal Telerik.WinControls.UI.RadDropDownList IdSerie;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Departamento;
        internal Telerik.WinControls.UI.RadTextBox IdOrdenCompra;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox IdPrioridad;
        internal Telerik.WinControls.UI.RadLabel lblFolio;
        internal Telerik.WinControls.UI.RadLabel lblSerie;
        internal Telerik.WinControls.UI.RadLabel lblCreo;
        internal Telerik.WinControls.UI.RadTextBox Creo;
        internal Telerik.WinControls.UI.RadLabel lblPrioridad;
        internal Telerik.WinControls.UI.RadLabel lblTipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblMoneda;
        internal Telerik.WinControls.UI.RadLabel lblOrdenCompra;
        internal Telerik.WinControls.UI.RadLabel lblDepartamento;
        internal Telerik.WinControls.UI.RadSpinEditor TipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblVendedor;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox IdVendedor;
        protected internal Telerik.WinControls.UI.RadLabel lblVersion;
        private Telerik.WinControls.UI.RadGroupBox groupBox1;
        internal Telerik.WinControls.UI.RadLabel lblDecimales;
        internal Telerik.WinControls.UI.RadSpinEditor Decimales;
        protected internal Telerik.WinControls.UI.RadTextBox Nota;
        internal Telerik.WinControls.UI.RadLabel lblNota;
        internal Telerik.WinControls.UI.RadLabel lblRecibe;
        internal Telerik.WinControls.UI.RadDropDownList Contacto;
        protected internal Telerik.WinControls.UI.RadSpinEditor IdVendedor2;
        internal Telerik.WinControls.UI.RadSpinEditor FactorPactado;
        internal Telerik.WinControls.UI.RadSpinEditor TasaIVAPactado;
        protected internal Telerik.WinControls.UI.RadSpinEditor IdDepartamento;
        protected internal Cotizador.Forms.Empresa.DirectorioControl Receptor;
        protected internal Cotizador.Forms.Empresa.DirectorioControl directorioControl1;
    }
}
