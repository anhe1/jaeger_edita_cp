﻿namespace Jaeger.UI.CP.Forms.Adquisiciones {
    partial class ReqCompraForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TComprobante = new Jaeger.UI.Common.Forms.TbDocumentoControl();
            this.TabControl = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptos = new Telerik.WinControls.UI.RadPageViewPage();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.GridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.TConceptos = new Jaeger.UI.Common.Forms.ToolBarConceptoControl();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.TabConceptoParte = new Telerik.WinControls.UI.RadPageView();
            this.PageConceptoParte = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridConceptoParte = new Telerik.WinControls.UI.RadGridView();
            this.ToolBarConceptoParte = new Jaeger.UI.Common.Forms.ToolBarConceptoControl();
            this.PanelTotales = new Telerik.WinControls.UI.RadPanel();
            this.Total = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Descuento = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.SubTotal = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.RadLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.OnStart = new System.ComponentModel.BackgroundWorker();
            this.ComprobanteControl = new Jaeger.UI.CP.Forms.Adquisiciones.ReqCompraGeneralControl();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
            this.TabControl.SuspendLayout();
            this.PageConceptos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabConceptoParte)).BeginInit();
            this.TabConceptoParte.SuspendLayout();
            this.PageConceptoParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).BeginInit();
            this.PanelTotales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.ShowIdDocumento = false;
            this.TComprobante.Size = new System.Drawing.Size(1181, 30);
            this.TComprobante.TabIndex = 3;
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.PageConceptos);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 134);
            this.TabControl.Margin = new System.Windows.Forms.Padding(1);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedPage = this.PageConceptos;
            this.TabControl.Size = new System.Drawing.Size(1181, 459);
            this.TabControl.TabIndex = 5;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Top;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabControl.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            // 
            // PageConceptos
            // 
            this.PageConceptos.Controls.Add(this.radSplitContainer1);
            this.PageConceptos.ItemSize = new System.Drawing.SizeF(69F, 28F);
            this.PageConceptos.Location = new System.Drawing.Point(8, 32);
            this.PageConceptos.Name = "PageConceptos";
            this.PageConceptos.Size = new System.Drawing.Size(1165, 422);
            this.PageConceptos.Text = "Conceptos";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1165, 422);
            this.radSplitContainer1.TabIndex = 3;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.GridConceptos);
            this.splitPanel1.Controls.Add(this.TConceptos);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1165, 253);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.1226415F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 59);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // GridConceptos
            // 
            this.GridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptos.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptos.MasterTemplate.AllowDeleteRow = false;
            this.GridConceptos.MasterTemplate.AllowRowResize = false;
            this.GridConceptos.MasterTemplate.EnableGrouping = false;
            this.GridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.GridConceptos.Name = "GridConceptos";
            this.GridConceptos.Size = new System.Drawing.Size(1165, 223);
            this.GridConceptos.TabIndex = 194;
            // 
            // TConceptos
            // 
            this.TConceptos.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConceptos.IsEditable = true;
            this.TConceptos.Location = new System.Drawing.Point(0, 0);
            this.TConceptos.Margin = new System.Windows.Forms.Padding(1);
            this.TConceptos.Name = "TConceptos";
            this.TConceptos.ShowAgregar = true;
            this.TConceptos.ShowComboConceptos = false;
            this.TConceptos.ShowDuplicar = false;
            this.TConceptos.ShowNuevo = true;
            this.TConceptos.ShowProductos = true;
            this.TConceptos.ShowRemover = true;
            this.TConceptos.ShowUnidades = false;
            this.TConceptos.Size = new System.Drawing.Size(1165, 30);
            this.TConceptos.TabIndex = 195;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.TabConceptoParte);
            this.splitPanel2.Controls.Add(this.PanelTotales);
            this.splitPanel2.Location = new System.Drawing.Point(0, 257);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1165, 165);
            this.splitPanel2.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 165);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.1226415F);
            this.splitPanel2.SizeInfo.MaximumSize = new System.Drawing.Size(0, 165);
            this.splitPanel2.SizeInfo.MinimumSize = new System.Drawing.Size(0, 165);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -59);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // TabConceptoParte
            // 
            this.TabConceptoParte.Controls.Add(this.PageConceptoParte);
            this.TabConceptoParte.DefaultPage = this.PageConceptoParte;
            this.TabConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.TabConceptoParte.Name = "TabConceptoParte";
            this.TabConceptoParte.SelectedPage = this.PageConceptoParte;
            this.TabConceptoParte.Size = new System.Drawing.Size(933, 165);
            this.TabConceptoParte.TabIndex = 169;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).ShowItemPinButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).ShowItemCloseButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.TabConceptoParte.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(2, 1, 2, 2);
            // 
            // PageConceptoParte
            // 
            this.PageConceptoParte.Controls.Add(this.GridConceptoParte);
            this.PageConceptoParte.Controls.Add(this.ToolBarConceptoParte);
            this.PageConceptoParte.ItemSize = new System.Drawing.SizeF(96F, 28F);
            this.PageConceptoParte.Location = new System.Drawing.Point(7, 33);
            this.PageConceptoParte.Name = "PageConceptoParte";
            this.PageConceptoParte.Size = new System.Drawing.Size(919, 125);
            this.PageConceptoParte.Text = "Concepto: Parte";
            // 
            // GridConceptoParte
            // 
            this.GridConceptoParte.AutoGenerateHierarchy = true;
            this.GridConceptoParte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridConceptoParte.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridConceptoParte.MasterTemplate.AllowAddNewRow = false;
            this.GridConceptoParte.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.FieldName = "Cantidad";
            gridViewTextBoxColumn8.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn8.HeaderText = "Cantidad";
            gridViewTextBoxColumn8.Name = "Cantidad";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 80;
            gridViewTextBoxColumn9.FieldName = "Unidad";
            gridViewTextBoxColumn9.HeaderText = "Unidad";
            gridViewTextBoxColumn9.Name = "Unidad";
            gridViewTextBoxColumn9.Width = 80;
            gridViewTextBoxColumn10.FieldName = "ClaveProdServ";
            gridViewTextBoxColumn10.HeaderText = "Clv. Producto";
            gridViewTextBoxColumn10.Name = "ClaveProdServ";
            gridViewTextBoxColumn10.Width = 80;
            gridViewTextBoxColumn11.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn11.HeaderText = "No. Ident.";
            gridViewTextBoxColumn11.Name = "NoIdentificacion";
            gridViewTextBoxColumn11.Width = 90;
            gridViewTextBoxColumn12.FieldName = "Descripcion";
            gridViewTextBoxColumn12.HeaderText = "Descripción";
            gridViewTextBoxColumn12.Name = "Descripcion";
            gridViewTextBoxColumn12.Width = 250;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "ValorUnitario";
            gridViewTextBoxColumn13.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn13.HeaderText = "Valor \n\rUnitario";
            gridViewTextBoxColumn13.Name = "ValorUnitario";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 80;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "Importe";
            gridViewTextBoxColumn14.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn14.HeaderText = "Importe";
            gridViewTextBoxColumn14.Name = "Importe";
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 80;
            this.GridConceptoParte.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.GridConceptoParte.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.GridConceptoParte.Name = "GridConceptoParte";
            this.GridConceptoParte.ShowGroupPanel = false;
            this.GridConceptoParte.Size = new System.Drawing.Size(919, 95);
            this.GridConceptoParte.TabIndex = 0;
            // 
            // ToolBarConceptoParte
            // 
            this.ToolBarConceptoParte.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBarConceptoParte.Enabled = false;
            this.ToolBarConceptoParte.IsEditable = true;
            this.ToolBarConceptoParte.Location = new System.Drawing.Point(0, 0);
            this.ToolBarConceptoParte.Name = "ToolBarConceptoParte";
            this.ToolBarConceptoParte.ShowAgregar = true;
            this.ToolBarConceptoParte.ShowComboConceptos = false;
            this.ToolBarConceptoParte.ShowDuplicar = true;
            this.ToolBarConceptoParte.ShowNuevo = true;
            this.ToolBarConceptoParte.ShowProductos = true;
            this.ToolBarConceptoParte.ShowRemover = true;
            this.ToolBarConceptoParte.ShowUnidades = true;
            this.ToolBarConceptoParte.Size = new System.Drawing.Size(919, 30);
            this.ToolBarConceptoParte.TabIndex = 1;
            // 
            // PanelTotales
            // 
            this.PanelTotales.Controls.Add(this.Total);
            this.PanelTotales.Controls.Add(this.Descuento);
            this.PanelTotales.Controls.Add(this.SubTotal);
            this.PanelTotales.Controls.Add(this.RadLabel28);
            this.PanelTotales.Controls.Add(this.RadLabel31);
            this.PanelTotales.Controls.Add(this.RadLabel24);
            this.PanelTotales.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotales.Location = new System.Drawing.Point(933, 0);
            this.PanelTotales.Name = "PanelTotales";
            this.PanelTotales.Size = new System.Drawing.Size(232, 165);
            this.PanelTotales.TabIndex = 170;
            // 
            // Total
            // 
            this.Total.Location = new System.Drawing.Point(97, 51);
            this.Total.Mask = "n4";
            this.Total.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Total.Name = "Total";
            this.Total.NullText = "SubTotal";
            this.Total.Size = new System.Drawing.Size(125, 20);
            this.Total.TabIndex = 7;
            this.Total.TabStop = false;
            this.Total.Text = "0.0000";
            this.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Descuento
            // 
            this.Descuento.Location = new System.Drawing.Point(97, 29);
            this.Descuento.Mask = "n4";
            this.Descuento.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Descuento.Name = "Descuento";
            this.Descuento.NullText = "SubTotal";
            this.Descuento.Size = new System.Drawing.Size(125, 20);
            this.Descuento.TabIndex = 2;
            this.Descuento.TabStop = false;
            this.Descuento.Text = "0.0000";
            this.Descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SubTotal
            // 
            this.SubTotal.Location = new System.Drawing.Point(97, 8);
            this.SubTotal.Mask = "n4";
            this.SubTotal.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.NullText = "SubTotal";
            this.SubTotal.Size = new System.Drawing.Size(125, 20);
            this.SubTotal.TabIndex = 1;
            this.SubTotal.TabStop = false;
            this.SubTotal.Text = "0.0000";
            this.SubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RadLabel28
            // 
            this.RadLabel28.Location = new System.Drawing.Point(16, 9);
            this.RadLabel28.Name = "RadLabel28";
            this.RadLabel28.Size = new System.Drawing.Size(50, 18);
            this.RadLabel28.TabIndex = 179;
            this.RadLabel28.Text = "SubTotal";
            // 
            // RadLabel31
            // 
            this.RadLabel31.Location = new System.Drawing.Point(16, 52);
            this.RadLabel31.Name = "RadLabel31";
            this.RadLabel31.Size = new System.Drawing.Size(31, 18);
            this.RadLabel31.TabIndex = 182;
            this.RadLabel31.Text = "Total";
            // 
            // RadLabel24
            // 
            this.RadLabel24.Location = new System.Drawing.Point(16, 30);
            this.RadLabel24.Name = "RadLabel24";
            this.RadLabel24.Size = new System.Drawing.Size(59, 18);
            this.RadLabel24.TabIndex = 176;
            this.RadLabel24.Text = "Descuento";
            // 
            // OnStart
            // 
            this.OnStart.DoWork += new System.ComponentModel.DoWorkEventHandler(this.OnStart_DoWork);
            this.OnStart.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OnStart_RunWorkerCompleted);
            // 
            // ComprobanteControl
            // 
            this.ComprobanteControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ComprobanteControl.Location = new System.Drawing.Point(0, 30);
            this.ComprobanteControl.Margin = new System.Windows.Forms.Padding(4);
            this.ComprobanteControl.MaximumSize = new System.Drawing.Size(0, 102);
            this.ComprobanteControl.MinimumSize = new System.Drawing.Size(1050, 104);
            this.ComprobanteControl.Name = "ComprobanteControl";
            this.ComprobanteControl.Size = new System.Drawing.Size(1181, 104);
            this.ComprobanteControl.TabIndex = 4;
            // 
            // ReqCompraForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 593);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.ComprobanteControl);
            this.Controls.Add(this.TComprobante);
            this.Name = "ReqCompraForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Req. Compra";
            this.Load += new System.EventHandler(this.ReqCompraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
            this.TabControl.ResumeLayout(false);
            this.PageConceptos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabConceptoParte)).EndInit();
            this.TabConceptoParte.ResumeLayout(false);
            this.PageConceptoParte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridConceptoParte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTotales)).EndInit();
            this.PanelTotales.ResumeLayout(false);
            this.PanelTotales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Common.Forms.TbDocumentoControl TComprobante;
        private ReqCompraGeneralControl ComprobanteControl;
        protected internal Telerik.WinControls.UI.RadPageView TabControl;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageConceptos;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        protected internal Telerik.WinControls.UI.RadGridView GridConceptos;
        protected internal Common.Forms.ToolBarConceptoControl TConceptos;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        internal Telerik.WinControls.UI.RadPageView TabConceptoParte;
        internal Telerik.WinControls.UI.RadPageViewPage PageConceptoParte;
        protected internal Telerik.WinControls.UI.RadGridView GridConceptoParte;
        protected internal Common.Forms.ToolBarConceptoControl ToolBarConceptoParte;
        private Telerik.WinControls.UI.RadPanel PanelTotales;
        private Telerik.WinControls.UI.RadMaskedEditBox Total;
        private Telerik.WinControls.UI.RadMaskedEditBox Descuento;
        private Telerik.WinControls.UI.RadMaskedEditBox SubTotal;
        internal Telerik.WinControls.UI.RadLabel RadLabel28;
        internal Telerik.WinControls.UI.RadLabel RadLabel31;
        internal Telerik.WinControls.UI.RadLabel RadLabel24;
        protected internal System.ComponentModel.BackgroundWorker OnStart;
    }
}