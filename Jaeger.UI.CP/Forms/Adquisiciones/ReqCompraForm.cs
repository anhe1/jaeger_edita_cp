﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.CP.Contracts;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Cotizador.Produccion.Entities;
using Jaeger.UI.CP.Services;

namespace Jaeger.UI.CP.Forms.Adquisiciones {
    public partial class ReqCompraForm : RadForm {
        protected ReqCompraDetailModel Comprobante;
        protected internal RadContextMenu menuContextualConceptos = new RadContextMenu();
        protected internal RadMenuItem menuContextAplicar = new RadMenuItem { Text = "Aplicar a todos" };
        protected internal RadMenuItem menuContextDuplicar = new RadMenuItem { Text = "Duplicar" };
        protected RadMenuItem SinDuplicados = new RadMenuItem { Text = "Sin duplicados", CheckOnClick = true, IsChecked = true };
        protected IReqCompraService Service;

        public int Indice { get; set; }

        public ReqCompraForm() {
            InitializeComponent();
        }

        private void ReqCompraForm_Load(object sender, EventArgs e) {
            this.OnStart.RunWorkerAsync();
            this.menuContextDuplicar.Image = Properties.Resources.copy_16px;
            this.menuContextualConceptos.Items.AddRange(this.menuContextAplicar, this.menuContextDuplicar);
            this.menuContextAplicar.Click += this.MenuContextAplicar_Click;
            this.menuContextDuplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Complementos.Text = "Herramientas";
            this.TConceptos.Complementos.Items.Add(this.SinDuplicados);

            this.GridConceptos.ContextMenuOpening += this.GridConceptos_ContextMenuOpening;
        }

        public virtual void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            this.Text = Properties.Resources.msg_RemisionNew;
            if (this.Comprobante == null) {
                this.Comprobante = this.Service.GetNew();
            } else if (this.Comprobante.Folio > 0) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.Text = Properties.Resources.msg_RemisionGet;
                    espera.ShowDialog(this);
                    if (!string.IsNullOrEmpty(this.Comprobante.Serie) | string.IsNullOrEmpty(this.Comprobante.Folio.ToString())) {
                        this.Text = string.Concat(this.Comprobante.Serie, " ", this.Comprobante.Folio);
                    } else if (!string.IsNullOrEmpty(this.Comprobante.Folio.ToString())) {
                        this.Text = string.Concat(this.Comprobante.Folio);
                    }
                }
            }

            if (this.Comprobante.IsEditable) {
                this.ComprobanteControl.LoadContribuyente(0);
            }
            this.CreateBinding();
        }

        private void TComprobante_Imprimir_Click(object sender, EventArgs e) {
            if (this.Comprobante.Folio > 0) {
                if (this.Comprobante != null) {
                    var _d1 = this.Service.GetPrinter(this.Comprobante.Folio);
                    if (_d1 != null) {
                        var _reporte = new ReporteForm(_d1);
                        _reporte.Show();
                    }
                }
            }
        }

        #region barra de herramientas conceptos del comprobante
        protected internal void TComprobante_Guardar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                MessageBox.Show("Revisa");
                return;
            }

            using (var espera = new Waiting1Form(this.SaveComprobante)) {
                espera.Text = Properties.Resources.msg_ComprobanteSave;
                espera.ShowDialog(this);
            }
        }

        private void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            if (this.Comprobante.Conceptos.Count == 0) {
                this.Comprobante.Conceptos = new BindingList<ReqCompraPartidaDetailModel>();
                this.GridConceptos.DataSource = this.Comprobante.Conceptos;
                //    this.GridConceptoParte.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                //    this.GridConceptoImpuestos.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                //    this.GridConceptoInformacionAduanera.DataSource = this.ComprobanteControl.Comprobante.Conceptos;
                //    this.GridConceptoParte.DataMember = "Parte";
                //    this.GridConceptoImpuestos.DataMember = "Impuestos";
                //    this.GridConceptoInformacionAduanera.DataMember = "InformacionAduanera";
            }

            this.Comprobante.Conceptos.Add(new ReqCompraPartidaDetailModel { Activo = true });
        }

        public virtual void TConcepto_Remove_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_Remision_RemoverObjeto, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as ReqCompraPartidaDetailModel;
                    if (seleccionado.IdConcepto == 0) {
                        try {
                            this.Comprobante.Conceptos.Remove(seleccionado);
                        } catch (Exception ex) {
                            if (this.GridConceptos.Rows.Count == 0) {
                                this.Comprobante.Conceptos = new BindingList<ReqCompraPartidaDetailModel>();
                            }
                            Console.WriteLine(ex.Message);
                        }
                    } else {
                        seleccionado.Activo = false;
                        this.GridConceptos.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        public virtual void TConcepto_Duplicar_Click(object sender, EventArgs e) {
            if (this.GridConceptos.CurrentRow != null) {
                var seleccionado = this.GridConceptos.CurrentRow.DataBoundItem as ReqCompraPartidaDetailModel;
                if (seleccionado != null) {
                    var duplicado = seleccionado.Clone();
                    this.Comprobante.Conceptos.Add((ReqCompraPartidaDetailModel)duplicado);
                }
            }
        }

        public virtual void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.Comprobante.IdCliente == 0) {
                RadMessageBox.Show(this, Properties.Resources.msg_Cliente_Seleccionar, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var _catalogoProductos = new Cotizador.Forms.Produccion.BuscarForm()) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.TConceptos_Agregar_Producto;
                _catalogoProductos.ShowDialog(this);
            }
        }

        public virtual void TConceptos_Agregar_Producto(object sender, OrdenProduccionDetailModel e) {
            if (this.Comprobante.Conceptos.Count == 0) {
                this.Comprobante.Conceptos = new BindingList<ReqCompraPartidaDetailModel>();
                this.GridConceptos.DataSource = this.Comprobante.Conceptos;
            }
            if (e != null) {
                var nuevo = new ReqCompraPartidaDetailModel {
                    IdPedPrd = e.IdOrden,
                    Cantidad = e.Cantidad,
                    Unitario = e.ValorUnitario,
                    Activo = e.IsActive,
                    Descripcion = e.Nombre,
                    Unidad = e.Unidad.ToString(),
                };

                if (e.Componentes != null) {
                    if (e.Componentes.Count > 0) {
                        //nuevo.IdComprobante = e.Componentes[0].IdComponente;
                        //nuevo.Componente = e.Componentes[0].Nombre;
                    }
                }

                //if (this.SinDuplicados.IsChecked == true) {
                //    var ed = this.Comprobante.Conceptos.Where(it => it.Identificador == nuevo.Identificador);
                //    if (ed.Count() > 0) {
                //        RadMessageBox.Show(this, Properties.Resources.msg_Remision_ProductoDuplicado, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                //        return;
                //    }
                //}

                this.Comprobante.Conceptos.Add(nuevo);
            }
        }
        #endregion

        #region menu contextual
        public virtual void MenuContextAplicar_Click(object sender, EventArgs e) {
            var d = this.GridConceptos.CurrentCell.Value;
            foreach (var item in this.GridConceptos.Rows) {
                item.Cells[this.GridConceptos.CurrentColumn.Index].Value = d;
            }
        }
        #endregion

        #region acciones del grid
        public virtual void GridConceptos_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (this.Comprobante.IsEditable == false) {
                return;
            }
            if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridConceptos.CurrentRow.ViewInfo.ViewTemplate == this.GridConceptos.MasterTemplate) {
                    this.menuContextAplicar.Enabled = this.GridConceptos.CurrentColumn.Name == "ClaveUnidad" |
                        this.GridConceptos.CurrentColumn.Name == "Unidad" |
                        this.GridConceptos.CurrentColumn.Name == "NumPedido" |
                        this.GridConceptos.CurrentColumn.Name == "CantidadS" |
                        this.GridConceptos.CurrentColumn.Name == "ValorUnitario" |
                        this.GridConceptos.CurrentColumn.Name == "ClaveProducto" |
                        this.GridConceptos.CurrentColumn.Name == "ObjetoImp";
                }
                e.ContextMenu = this.menuContextualConceptos.DropDown;
            }
        }
        #endregion

        #region eventos
        public virtual void Comprobante_BindingCompleted(object sender, EventArgs e) {
            this.Text = "Req. Compra: " + this.ComprobanteControl.Text;
            this.TConceptos.IsEditable = this.Comprobante.IsEditable;
            this.ToolBarConceptoParte.IsEditable = this.Comprobante.IsEditable;

            this.GridConceptos.DataSource = this.Comprobante.Conceptos;
            this.GridConceptos.AllowEditRow = this.Comprobante.IsEditable;

            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.Comprobante, "TotalDescuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.Comprobante, "GTotal", true, DataSourceUpdateMode.OnPropertyChanged);
        }
        #endregion

        private void CreateBinding() {
            this.TComprobante.Status.DataBindings.Clear();
            this.TComprobante.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.lblVersion.DataBindings.Clear();
            this.ComprobanteControl.lblVersion.DataBindings.Add("Text", this.Comprobante, "Version", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Receptor.DataBindings.Clear();
            this.ComprobanteControl.Receptor.RFC.DataBindings.Clear();
            this.ComprobanteControl.Receptor.RFC.ReadOnly = true;
            this.ComprobanteControl.Receptor.Nombre.SetEditable(this.Comprobante.IsEditable);

            this.ComprobanteControl.Receptor.Nombre.DataBindings.Add("Text", this.Comprobante, "ReceptorNombre", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.ComprobanteControl.ReceptorRFC.DataBindings.Add("Text", this.Comprobante, "ReceptorRFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Contacto.DataBindings.Clear();
            this.ComprobanteControl.Contacto.ReadOnly = !this.Comprobante.IsEditable;
            this.ComprobanteControl.Contacto.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Departamento.DataBindings.Clear();
            this.ComprobanteControl.Departamento.SetEditable(this.Comprobante.IsEditable);
            this.ComprobanteControl.Departamento.DataBindings.Add("SelectedValue", this.Comprobante, "IdDepartamento", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.TComprobante.IdDocumento.DataBindings.Clear();
            //this.TComprobante.IdDocumento.DataBindings.Add("Text", this.Comprobante, "IdDocumento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Receptor.Clave.DataBindings.Clear();
            //this.ComprobanteControl.ReceptorClave.DataBindings.Add("Text", this.Comprobante, "ReceptorClave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Receptor.IdDirectorio.DataBindings.Clear();
            this.ComprobanteControl.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Folio.DataBindings.Clear();
            this.ComprobanteControl.Folio.ReadOnly = !this.Comprobante.IsEditable;
            this.ComprobanteControl.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.IdSerie.DataBindings.Clear();
            this.ComprobanteControl.IdSerie.SetEditable(this.Comprobante.IsEditable);
            this.ComprobanteControl.IdSerie.DataBindings.Add("Text", this.Comprobante, "Serie", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.ComprobanteControl.IdDomicilio.DataBindings.Clear();
            //this.ComprobanteControl.IdDomicilio.SetEditable(this.Comprobante.IsEditable);
            //this.ComprobanteControl.IdDomicilio.DataBindings.Add("Text", this.Comprobante, "DomicilioEntrega", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.ComprobanteControl.IdDomicilio2.DataBindings.Clear();
            //this.ComprobanteControl.IdDomicilio2.DataBindings.Add("Value", this.Comprobante, "IdDomicilio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.IdOrdenCompra.DataBindings.Clear();
            this.ComprobanteControl.IdOrdenCompra.ReadOnly = !this.Comprobante.IsEditable;
            //this.ComprobanteControl.IdGuia.DataBindings.Add("Text", this.Comprobante, "NoGuia", true, DataSourceUpdateMode.OnPropertyChanged);

            //this.ComprobanteControl.IdVendedor.DataBindings.Clear();
            //this.ComprobanteControl.IdVendedor.SetEditable(this.Comprobante.IsEditable);
            //this.ComprobanteControl.IdVendedor.DataBindings.Add("Text", this.Comprobante, "Vendedor", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.ComprobanteControl.IdVendedor2.DataBindings.Clear();
            //this.ComprobanteControl.IdVendedor2.DataBindings.Add("Value", this.Comprobante, "IdVendedor", false, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.TipoCambio.DataBindings.Clear();
            this.ComprobanteControl.TipoCambio.SetEditable(this.Comprobante.IsEditable);
            //this.ComprobanteControl.TipoCambio.DataBindings.Add("Value", this.Comprobante, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Decimales.DataBindings.Clear();
            this.ComprobanteControl.Decimales.SetEditable(this.Comprobante.IsEditable);
            //this.ComprobanteControl.Decimales.DataBindings.Add("Value", this.Comprobante, "PrecisionDecimal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Nota.DataBindings.Clear();
            this.ComprobanteControl.Nota.ReadOnly = !this.Comprobante.IsEditable;
            this.ComprobanteControl.Nota.DataBindings.Add("Text", this.Comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.IdPrioridad.DataBindings.Clear();
            this.ComprobanteControl.IdPrioridad.SetEditable(this.Comprobante.IsEditable);
            this.ComprobanteControl.IdPrioridad.DataBindings.Add("SelectedValue", this.Comprobante, "IdTipo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Moneda.DataBindings.Clear();
            this.ComprobanteControl.Moneda.SetEditable(this.Comprobante.IsEditable);
            //this.ComprobanteControl.Moneda.DataBindings.Add("Text", this.Comprobante, "ClaveMoneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.FechaEmision.DataBindings.Clear();
            this.ComprobanteControl.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.ComprobanteControl.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaNuevo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.FechaEntrega.DataBindings.Clear();
            this.ComprobanteControl.FechaEntrega.SetEditable(this.Comprobante.IsEditable);
            this.ComprobanteControl.FechaEntrega.DataBindings.Add("Value", this.Comprobante, "FechaAcuerdo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ComprobanteControl.Creo.DataBindings.Clear();
            this.ComprobanteControl.Creo.ReadOnly = true;
            this.ComprobanteControl.Creo.DataBindings.Add("Text", this.Comprobante, "Creo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TComprobante.FilePDF.DataBindings.Clear();
            this.TComprobante.FilePDF.DataBindings.Add("Enabled", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SubTotal.DataBindings.Clear();
            this.SubTotal.DataBindings.Add("Value", this.Comprobante, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.Comprobante, "TotalDescuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Total.DataBindings.Clear();
            this.Total.DataBindings.Add("Text", this.Comprobante, "Total", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Inabilitar();
        }

        private void Inabilitar() {
            this.TComprobante.Guardar.Enabled = this.Comprobante.IsEditable;
            this.TComprobante.Imprimir.Enabled = this.Comprobante.IsEditable;
            this.ComprobanteControl.Folio.ReadOnly = !this.Comprobante.IsEditable;
            this.ComprobanteControl.IdSerie.ReadOnly = !this.Comprobante.IsEditable;
        }

        private void GetComprobante() {
            this.Comprobante = this.Service.GetComprobante(this.Comprobante.Folio);
        }

        private void SaveComprobante() {
            this.Comprobante = this.Service.Save(this.Comprobante);
        }

        private bool Validar() {
            return true;
        }

        #region OnLoad
        private void OnStart_DoWork(object sender, DoWorkEventArgs e) {
            // crear el servicio
            this.ComprobanteControl.Start();
            this.Service = new ReqCompraService();

            this.TComprobante.Opciones.Items.Remove(this.TComprobante.Nuevo);
            this.TComprobante.Opciones.Items.Remove(this.TComprobante.Duplicar);
            this.TComprobante.Opciones.Items.Remove(this.TComprobante.Cancelar);
            this.TComprobante.Opciones.Items.Remove(this.TComprobante.SendEmail);
            this.TComprobante.Opciones.Items.Remove(this.TComprobante.Complemento);

            if (ConfigService.Synapsis.ProveedorAutorizado.Certificacion.Production == false) {
                this.TComprobante.Emisor.ToolTipText = Properties.Resources.msg_Modo_Prueba;
                this.TComprobante.Emisor.Image = Properties.Resources.more_info_16px;
            }

            this.TComprobante.Status.DataSource = ReqCompraService.GetStatus();
            this.TComprobante.Status.Enabled = false;
            this.TComprobante.Emisor.Text = ConfigService.Synapsis.Empresa.RFC;
            this.TComprobante.Actualizar.Click += this.TComprobante_Actualizar_Click;
            this.TComprobante.Guardar.Click += this.TComprobante_Guardar_Click;
            this.TComprobante.Imprimir.Click += this.TComprobante_Imprimir_Click;

            using (IRequerimientoCompraGridBuilder view = new RequerimientoCompraGridBuilder()) 
                this.GridConceptos.Columns.AddRange(view.Templetes().GetConceptos().Build());
            //this.GridConceptos.Columns[UI.Almacen.DP.Services.GridRemisionService.ColIdPedido.Name].ReadOnly = false;

            this.ComprobanteControl.IdSerie.Enabled = false;
        }

        private void OnStart_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.TComprobante.Cerrar.Click += this.TComprobante_Cerrar_Click;
            this.TConceptos.Nuevo.Click += this.TConcepto_Nuevo_Click;
            this.TConceptos.Remover.Click += this.TConcepto_Remove_Click;
            this.TConceptos.Duplicar.Click += this.TConcepto_Duplicar_Click;
            this.TConceptos.Productos.Click += this.TConcepto_Productos_Click;
            this.TComprobante.Actualizar.PerformClick();
        }
        #endregion
    }
}
