﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.CP.Services;
using Jaeger.Aplication.CP.Contracts;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Aplication.Cotizador.Contracts;

namespace Jaeger.UI.CP.Forms.Adquisiciones {
    public partial class ReqsCompraForm : RadForm {
        protected IReqsCompraService service;
        protected IDepartamentosService departamentoService;
        protected BackgroundWorker bgWorker = new BackgroundWorker();
        private BindingList<ReqCompraDetailModel> datos;
        protected internal Domain.Base.ValueObjects.UIAction _permisos;

        public ReqsCompraForm() {
            InitializeComponent();
        }

        public ReqsCompraForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void ReqsCompraForm_Load(object sender, EventArgs e) {
            this.gridCompras.TelerikGridCommon();
            this.gridConceptos.Standard();

            this.bgWorker.DoWork += this.BgWorker_DoWork;
            this.bgWorker.RunWorkerCompleted += this.BgWorker_RunWorkerCompleted;
            this.bgWorker.RunWorkerAsync();

            this.TRequierimiento.Nuevo.Enabled = this._permisos.Agregar;
            this.TRequierimiento.Cancelar.Enabled = this._permisos.Cancelar;
        }

        #region barra de herramientas
        private void TRequerimiento_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new Adquisiciones.ReqCompraForm() { MdiParent = ParentForm };
            _nuevo.Show();
        }

        private void TRequerimiento_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando...";
                espera.ShowDialog(this);
            }
            this.gridCompras.DataSource = this.datos;
        }

        private void TRequerimiento_Imprimir_Click(object sender, EventArgs e) {
            if (this.gridCompras.CurrentRow != null) {
                var _remision = this.gridCompras.CurrentRow.DataBoundItem as ReqCompraDetailModel;
                if (_remision != null) {
                    var _d1 = this.service.GetPrinter(_remision.Folio);
                    if (_d1 != null) {
                        var _reporte = new Adquisiciones.ReporteForm(_d1);
                        _reporte.Show();
                    }
                }
            }
        }

        private void TRequerimiento_Filtro_Click(object sender, EventArgs e) {
            this.gridCompras.ShowFilteringRow = this.TRequierimiento.Filtro.ToggleState != ToggleState.On;
            if (this.gridCompras.ShowFilteringRow == false)
                this.gridCompras.FilterDescriptors.Clear();
        }

        private void TRequerimiento_Cancelar_Click(object sender, EventArgs e) {
            if (this.gridCompras.CurrentRow != null) {

            }
        }

        private void TRequerimiento_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        private void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.gridConceptos.Caption) {
                using (var espera = new Waiting1Form(this.GetConceptos)) {
                    espera.ShowDialog(this);
                }
                var seleccionado = this.gridCompras.CurrentRow.DataBoundItem as ReqCompraDetailModel;
                if (seleccionado != null) {
                    foreach (var item in seleccionado.Conceptos) {
                        var _row = e.Template.Rows.NewRow();
                        _row.Cells["Cantidad"].Value = item.Cantidad;
                        _row.Cells["Unidad"].Value = item.Unidad;
                        _row.Cells["IdOrden"].Value = item.IdPedPrd;
                        _row.Cells["Descripcion"].Value = item.Descripcion;
                        _row.Cells["Marca"].Value = item.Marca;
                        _row.Cells["Especificacion"].Value = item.Especificacion;
                        _row.Cells["Nota"].Value = item.Nota;
                        _row.Cells["Unitario"].Value = item.Unitario;
                        _row.Cells["SubTotal"].Value = item.SubTotal;
                        _row.Cells["Descuento"].Value = item.Descuento;
                        _row.Cells["Importe"].Value = item.Importe;
                        e.SourceCollection.Add(_row);
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.datos = this.service.GetList(this.TRequierimiento.GetEjercicio(), this.TRequierimiento.GetMes());
        }

        private void GetConceptos() {
            var seleccionado = this.gridCompras.CurrentRow.DataBoundItem as ReqCompraDetailModel;
            if (seleccionado != null) {
                var d1 = this.service.GetComprobante(seleccionado.Folio);
                if (d1 != null) {
                    seleccionado.Conceptos = d1.Conceptos;
                }
            }
        }

        private void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.TRequierimiento.Nuevo.Click += this.TRequerimiento_Nuevo_Click;
            this.TRequierimiento.Cancelar.Click += this.TRequerimiento_Cancelar_Click;
            this.TRequierimiento.Actualizar.Click += this.TRequerimiento_Actualizar_Click;
            this.TRequierimiento.Imprimir.Click += this.TRequerimiento_Imprimir_Click;
            this.TRequierimiento.Filtro.Click += this.TRequerimiento_Filtro_Click;
            this.TRequierimiento.Cerrar.Click += this.TRequerimiento_Cerrar_Click;
            this.gridCompras.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.gridConceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.gridConceptos);
        }

        private void BgWorker_DoWork(object sender, DoWorkEventArgs e) {
            this.service = new ReqsCompraService();
            this.departamentoService = new DepartamentosService();
            using (IRequerimientoCompraGridBuilder view = new RequerimientoCompraGridBuilder()) {
                this.gridCompras.Columns.AddRange(view.Templetes().Master().Build());
                this.gridConceptos.Columns.AddRange(view.Templetes().GetConceptos().Build());
            }

            var deptos = this.departamentoService.GetDepartamentos();
            var departamentos = this.gridCompras.Columns["IdDepartamento"] as GridViewComboBoxColumn;
            departamentos.DataSource = deptos;
            departamentos.ValueMember = "IdDepto";
            departamentos.DisplayMember = "Nombre";

            var status = this.gridCompras.Columns["IdStatus"] as GridViewComboBoxColumn;
            status.DataSource = ReqCompraService.GetStatus();
            status.DisplayMember = "Descriptor";
            status.ValueMember = "Id";

            var prioridad = this.gridCompras.Columns["IdTipo"] as GridViewComboBoxColumn;
            prioridad.DataSource = ReqCompraService.GetPrioridad();
            prioridad.DisplayMember = "Descriptor";
            prioridad.ValueMember = "Id";

            this.TRequierimiento.Enabled = true;
        }
        #endregion
    }
}
