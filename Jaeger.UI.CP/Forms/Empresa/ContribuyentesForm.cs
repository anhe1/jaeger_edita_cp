﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Cotizador.Empresa.Entities;

namespace Jaeger.UI.CP.Forms.Empresa {
    /// <summary>
    /// Directorio
    /// </summary>
    public class ContribuyentesForm : Cotizador.Forms.Empresa.ContribuyentesForm {
        public ContribuyentesForm(UIMenuElement menuElement) : base() {
            this.TContribuyente.MenuElement = menuElement;
            this.Load += ContribuyentesForm_Load;
        }

        private void ContribuyentesForm_Load(object sender, System.EventArgs e) {
            this.Text = "Empresa: Directorio";
            this._Service = new Aplication.Cotizador.Services.ContribuyenteService();
            this.TContribuyente.IdClase.DisplayMember = "Descripcion";
            this.TContribuyente.IdClase.ValueMember = "IdClase";
            this.TContribuyente.IdClase.DataSource = this._Service.GetList<ContribuyenteClaseModel>(new List<IConditional> { new Conditional("DIRCLA_A", "1") });
        }
    }
}
