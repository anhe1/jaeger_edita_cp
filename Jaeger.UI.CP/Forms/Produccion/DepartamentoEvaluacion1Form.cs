﻿/// develop: 220720172304
/// purpose:
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.Charting;
using System.Windows.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Cotizador.Contracts;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Cotizador.Produccion.Entities;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// Reporte de tiempos y cantidades por departamento
    /// </summary>
    public partial class DepartamentoEvaluacion1Form : RadForm {
        protected IProduccionService Service;
        private List<ComponenteLineaProduccion> DataSource;
        private List<ComponenteLineaProduccion> DataSourceCopy;

        public DepartamentoEvaluacion1Form(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void DepartamentoEvaluacion1Form_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            using (Cotizador.Forms.Produccion.Builder.IOrdenProduccionGridBuilder builder = new Cotizador.Forms.Produccion.Builder.OrdenProduccionGridBuilder()) {
                this.gridData.Standard();
                this.gridData.Columns.AddRange(builder.Templetes().AvanceProduccion().Build());
            }

            this.Service = new ProduccionService();
            var sem = this.PrimerDiaSemana();
            this.TPeriodo.FInicial.Value = sem;
            this.TPeriodo.FFinal.Value = sem.AddDays(6);
            using (var espera = new UI.Common.Forms.Waiting2Form(this.Inicia)) {
                espera.Text = "un momento ...";
                espera.ShowDialog(this);
            }

            this.TPeriodo.Actualizar.Click += this.TEvaluacion_Actualizar_Click;
            this.TPeriodo.Imprimir.Click += this.TEvaluacion_Imprimir_Click;
            this.TPeriodo.Cerrar.Click += this.TEvaluacion_Cerrar_Click;
        }

        #region barra de herramientas
        private void TEvaluacion_Actualizar_Click(object sender, EventArgs e) {
            if (this.TPeriodo.Departamento.SelectedItem == null) {
                RadMessageBox.Show(this, "Selecciona un departamento", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            Wating4Form f = new Wating4Form(() => { this.Consultar(); }, "Cargando datos ...", false, true);
            f.ShowDialog(this);
            string Res = f.Message;
            if (!string.IsNullOrEmpty(Res))
                MessageBox.Show(Res);
            f.Dispose();

            List<string> lista_centro = this.DataSource.Select(it => it.Centro).Distinct().ToList();
            this.ChartTiempo.Series.Clear();
            this.ChartTiempo.Series.Add(new BarSeries());
            this.ChartTiempo.Series.Add(new BarSeries());
            this.ChartTiempo.Series[0].DataPoints.Clear();
            this.ChartTiempo.Series[1].DataPoints.Clear();
            this.ChartCentro.Series[0].DataPoints.Clear();
            this.ChartCentro.Series[1].DataPoints.Clear();
            this.ChartOficial.Series[0].DataPoints.Clear();
            this.ChartOficial.Series[1].DataPoints.Clear();
            this.ChartSalarios.Series[0].DataPoints.Clear();
            this.ChartSalarios.Series[1].DataPoints.Clear();
            this.ChartTiempo.Series[0].ShowLabels = true;
            this.ChartTiempo.Series[1].ShowLabels = true;

            var totalte = 0;
            foreach (var item in lista_centro) {
                var tiempoEstimado = this.DataSource.Where(it => it.Centro == item).Sum(it => it.TiempoEstimado) /60;
                var tiempoReal = this.DataSource.Where(it => it.Centro == item).Sum(it => it.TiempoReal) /60;
                totalte = totalte + tiempoEstimado;
                
                this.ChartTiempo.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = tiempoEstimado, Value = tiempoEstimado });
                this.ChartTiempo.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = tiempoReal, Value = tiempoReal });

                var tres = this.DataSource.Where(it => it.Centro == item).Sum(it => it.CantidadEstimada);
                var cuatro = this.DataSource.Where(it => it.Centro == item).Sum(it => it.CantidadReal);

                this.ChartCentro.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = tres, Value = tres });
                this.ChartCentro.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = cuatro, Value = cuatro });
            }

            List<string> lista_oficiales = this.DataSource.Select(it => it.Oficial).Distinct().ToList();
            foreach (var item in lista_oficiales) {
                if (item != null) {
                    var uno = this.DataSource.Where(it => it.Oficial == item).Sum(it => it.TiempoEstimado) / 60;
                    var dos = this.DataSource.Where(it => it.Oficial == item).Sum(it => it.TiempoReal) / 60;
                    this.ChartOficial.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = item.ToUpper(), Label = $"{uno}", Value = uno });
                    this.ChartOficial.Series[0].ShowLabels = true;
                    this.ChartOficial.Series[0].SyncLinesToLabelsColor = false;
                    this.ChartOficial.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = item.ToUpper(), Label = $"{dos}", Value = dos });
                    this.ChartOficial.Series[1].ShowLabels = true;

                    var cinco = this.DataSourceCopy.Where(it => it.Oficial == item).Min(it => it.ValorMinuto);
                    var tres = this.DataSource.Where(it => it.Oficial == item).Sum(it => it.TiempoEstimado);
                    var cuatro = this.DataSource.Where(it => it.Oficial == item).Sum(it => it.TiempoReal);

                    this.ChartSalarios.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = (double)(tres * cinco), Value = (double)(tres * cinco) });
                    this.ChartSalarios.Series[0].ShowLabels = true;
                    this.ChartSalarios.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = (double)(cuatro * cinco), Value = (double)(cuatro * cinco) });
                    this.ChartSalarios.Series[1].ShowLabels = true;
                }
            }
            this.gridData.DataSource = this.DataSource;
        }

        private void TEvaluacion_Imprimir_Click(object sender, EventArgs e) {
            var document = new RadPrintDocument {
                Landscape = true
            };
            this.ChartTiempo.PrintPreview(document);
            this.ChartCentro.PrintPreview(document);
            this.ChartOficial.PrintPreview(document);
            this.ChartSalarios.PrintPreview(document);
        }

        private void TEvaluacion_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        /// <summary>
        /// funcion para devolver el primer dia de la semana
        /// </summary>
        private DateTime PrimerDiaSemana() {
            DateTime dt = DateTime.Now.AddDays(-6);
            DateTime wkStDt = dt.AddDays(1 - Convert.ToDouble(dt.DayOfWeek));
            DateTime fechadesdesemana = wkStDt.Date;
            return fechadesdesemana;
        }

        private void Inicia() {
            var deptos = this.Service.GetList<Domain.Cotizador.Empresa.Entities.DepartamentoModel>(Aplication.Cotizador.Services.DepartamentosService.Query().OnlyActive().Build());
            this.TPeriodo.Departamento.DataSource = deptos;
            this.TPeriodo.Departamento.DisplayMember = "Departamento";
            this.TPeriodo.Departamento.ValueMember = "IdDepartamento";
        }

        private void Consultar() {
            var seleccioando = this.TPeriodo.Departamento.SelectedItem.DataBoundItem as Domain.Cotizador.Empresa.Entities.DepartamentoModel;
            if (seleccioando != null) {
                var condiciones = new List<IConditional> {
                    new Conditional("EDP1_ID", seleccioando.IdDepartamento.ToString()),
                new Conditional("PEDPRO_FEC_FIN", this.TPeriodo.FInicial.Value.ToShortDateString(), ConditionalTypeEnum.GreaterThanOrEqual),
                new Conditional("PEDPRO_FEC_FIN", this.TPeriodo.FFinal.Value.ToShortDateString(), ConditionalTypeEnum.LessThanOrEqual)
            };
                this.DataSource = this.Service.GetList<ComponenteLineaProduccion>(condiciones).ToList();
                this.DataSourceCopy = new List<ComponenteLineaProduccion>(this.DataSource.ToList());
            }
        }

        private void ChartView_LabelFormatting(object sender, ChartViewLabelFormattingEventArgs e) {
            e.LabelElement.Text = e.LabelElement.Text;
        }
    }
}
