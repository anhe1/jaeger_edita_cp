﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Nomina.Entities;
using System.Windows.Forms;

namespace Jaeger.UI.CP.Forms.Produccion {
    public partial class EmpleadoCatalogoForm : RadForm {
        #region declaraciones
        protected BindingList<EmpleadoModel> DataSource;
        protected EmpleadosService Service;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public EmpleadoCatalogoForm() {
            InitializeComponent();
        }

        private void EmpleadoCatalogoForm_Load(object sender, EventArgs e) {
            this.TEmpleado.ShowHerramientas = true;
            this.TEmpleado.Nuevo.Click += TEmpleado_Nuevo_Click;
            this.TEmpleado.Editar.Click += TEmpleado_Editar_Click;
            this.TEmpleado.Actualizar.Click += TEmpleado_Actualizar_Click;
            this.TEmpleado.Cerrar.Click += TEmpleado_Cerrar_Click;
        }

        #region barra de herramientas
        private void TEmpleado_Nuevo_Click(object sender, EventArgs eventArgs) {
            var nuevo = new EmpleadoForm(this.Service);
            nuevo.ShowDialog(this);
        }

        private void TEmpleado_Editar_Click(object sender, EventArgs eventArgs) {
            var seleccionado = this.TEmpleado.GridData.CurrentRow.DataBoundItem as EmpleadoModel;
            if (seleccionado != null) {
                var nuevo = new EmpleadoForm(this.Service, seleccionado);
                nuevo.ShowDialog(this);
            }
        }

        private void TEmpleado_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.ShowDialog(this);
            }
            this.TEmpleado.GridData.DataSource = this.DataSource;
        }

        private void TEmpleado_Cerrar_Click(object sender, EventArgs eventArgs) {
            this.Close();
        }
        #endregion

        #region metodos
        

        protected virtual void Consulta() {
            if (this.TEmpleado.RegistroInactivo.IsChecked == false) {
                this.DataSource = new BindingList<EmpleadoModel>(
                                this.Service.GetList<EmpleadoModel>(new List<Domain.Base.Builder.IConditional> { new Conditional("CTEMP_A", "1") }).ToList()
                                );
            } else {
                this.DataSource = new BindingList<EmpleadoModel>(
                                this.Service.GetList<EmpleadoModel>(new List<Domain.Base.Builder.IConditional>()).ToList()
                                );
            }
        }
        #endregion
    }
}
