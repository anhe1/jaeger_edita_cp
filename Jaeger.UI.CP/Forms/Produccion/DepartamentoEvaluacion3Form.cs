﻿/// develop: 220720172304
/// purpose:
using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.Charting;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Cotizador.Contracts;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Cotizador.Produccion.Entities;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// Reporte del area de produccion
    /// </summary>
    public partial class DepartamentoEvaluacion3Form : RadForm {
        #region 
        protected IProduccionService Service;
        private List<ComponenteLineaProduccion> DataSource;
        private List<ComponenteLineaProduccion> DataSourceCopy;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public DepartamentoEvaluacion3Form(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void DepartamentoEvaluacion2Form_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.TPeriodo.ShowDepartamento = false;
            using (Cotizador.Forms.Produccion.Builder.IOrdenProduccionGridBuilder builder = new Cotizador.Forms.Produccion.Builder.OrdenProduccionGridBuilder()) {
                this.TDatos.GridData.Standard();
                this.TDatos.GridData.Columns.AddRange(builder.Templetes().AvanceProduccion().Build());
            }

            this.Service = new ProduccionService();
            var sem = Domain.Base.Services.DateTimeExtension.FirstDayWeek();
            this.TPeriodo.FInicial.Value = sem;
            this.TPeriodo.FFinal.Value = sem.AddDays(6);

            using (var espera = new Common.Forms.Waiting2Form(this.Inicia)) {
                espera.Text = "un momento ...";
                espera.ShowDialog(this);
            }

            this.TPeriodo.Actualizar.Click += this.TEvaluacion_Actualizar_Click;
            this.TPeriodo.Imprimir.Click += this.TEvaluacion_Imprimir_Click;
            this.TPeriodo.Cerrar.Click += this.TEvaluacion_Cerrar_Click;
        }

        #region barra de herramientas
        private void TEvaluacion_Actualizar_Click(object sender, EventArgs e) {
            if (this.TPeriodo.Departamento.SelectedItem == null) {
                RadMessageBox.Show(this, "Selecciona un departamento", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            using (var espera = new Common.Forms.Waiting2Form(this.Consultar)) {
                espera.Text = "Actualizando, espere un momento...";
                espera.ShowDialog(this);
            }

            var diasDelPeriodo = this.DataSource.Select(it => it.FechaFinal.Value.Day).Distinct().Count();
            var centros = this.DataSource.Select(it => it.Centro).Distinct().ToList();
            var oficiales = this.DataSource.Select(it => it.Oficial).Distinct().ToList();

            this.PorCentro.Series[0].DataPoints.Clear();
            this.PorCentro.Series[1].DataPoints.Clear();
            this.PorCentro.Series[2].DataPoints.Clear();

            this.PorDepartamento.Series[0].DataPoints.Clear();
            this.PorDepartamento.Series[1].DataPoints.Clear();

            this.PorCantidad.Series[0].DataPoints.Clear();
            this.PorCantidad.Series[1].DataPoints.Clear();

            this.PorOficial.Series[0].DataPoints.Clear();
            this.PorOficial.Series[1].DataPoints.Clear();
            this.PorOficial.Series[2].DataPoints.Clear();

            this.Prueba();

            foreach (var oficial in oficiales) {
                if (oficial != null) {
                    var cantidadEstimada = this.DataSource.Where(it => it.Oficial == oficial).Sum(it => it.CantidadEstimada);
                    var cantidadReal = this.DataSource.Where(it => it.Oficial == oficial).Sum(it => it.CantidadReal);

                    this.PorCantidad.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = oficial.ToUpper(), Label = cantidadEstimada.ToString(), Value = cantidadEstimada });
                    this.PorCantidad.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = oficial.ToUpper(), Label = cantidadReal.ToString(), Value = cantidadReal });
                    this.PorCantidad.Series[0].ShowLabels = true;
                    this.PorCantidad.Series[1].ShowLabels = true;

                    var tiempoEstimado = this.DataSource.Where(it => it.Oficial == oficial).Sum(it => it.TiempoEstimado) / 60;
                    var tiempoReal = this.DataSource.Where(it => it.Oficial == oficial).Sum(it => it.TiempoReal) / 60;

                    this.PorOficial.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = oficial, Label = tiempoEstimado.ToString() + " hrs.", Value = (double)(tiempoEstimado) });
                    this.PorOficial.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = oficial, Label = tiempoReal.ToString() + " hrs.", Value = (double)(tiempoReal) });
                    this.PorOficial.Series[2].DataPoints.Add(new CategoricalDataPoint { Category = oficial, Label = (48 - tiempoReal).ToString() + " hrs.", Value = 48 });
                    this.PorOficial.Series[2].ShowLabels = true;
                }
            }

            this.TDatos.GridData.DataSource = this.DataSource;
        }

        private void TEvaluacion_Imprimir_Click(object sender, EventArgs e) {
            var document = new RadPrintDocument {
                Landscape = true
            };
            this.PorCentro.PrintPreview(document);
            this.PorDepartamento.PrintPreview(document);
            this.PorCantidad.PrintPreview(document);
            this.PorOficial.PrintPreview(document);
        }

        private void TEvaluacion_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        private void Inicia() {
            var deptos = this.Service.GetList<Domain.Cotizador.Empresa.Entities.DepartamentoModel>(Aplication.Cotizador.Services.DepartamentosService.Query().OnlyActive().Build());
            this.TPeriodo.Departamento.DataSource = deptos;
            this.TPeriodo.Departamento.DisplayMember = "Departamento";
            this.TPeriodo.Departamento.ValueMember = "IdDepartamento";

            this.comboBox1.DataSource = deptos;
            this.comboBox1.DisplayMember = "Departamento";
            this.comboBox1.ValueMember = "IdDepartamento";
        }

        private void Consultar() {
            var seleccioando = this.TPeriodo.Departamento.SelectedItem.DataBoundItem as Domain.Cotizador.Empresa.Entities.DepartamentoModel;
            if (seleccioando != null) {
                var condiciones = new List<IConditional> {
                    //new Conditional("EDP1_ID", seleccioando.IdDepartamento.ToString()),
                new Conditional("PEDPRO_FEC_FIN", this.TPeriodo.FInicial.Value.ToShortDateString(), ConditionalTypeEnum.GreaterThanOrEqual),
                new Conditional("PEDPRO_FEC_FIN", this.TPeriodo.FFinal.Value.ToShortDateString(), ConditionalTypeEnum.LessThanOrEqual)
            };
                this.DataSource = this.Service.GetList<ComponenteLineaProduccion>(condiciones).ToList();
                this.DataSourceCopy = new List<ComponenteLineaProduccion>(this.DataSource.ToList());
            }
        }

        private void Prueba(int idDepartamento = 0) {
            var diasDelPeriodo = 0;
            var centros = new List<string>();

            this.PorCentro.Series[0].DataPoints.Clear();
            this.PorCentro.Series[1].DataPoints.Clear();
            this.PorCentro.Series[2].DataPoints.Clear();

            this.PorDepartamento.Series[0].DataPoints.Clear();
            this.PorDepartamento.Series[1].DataPoints.Clear();

            if (idDepartamento > 0) {
                centros = this.DataSource.Where(it => it.IdDepartamento == idDepartamento).Select(it => it.Centro).Distinct().ToList();
                diasDelPeriodo = this.DataSource.Where(it => it.IdDepartamento == idDepartamento).Select(it => it.FechaFinal.Value.Day).Distinct().Count();
            } else {
                centros = this.DataSource.Select(it => it.Centro).Distinct().ToList();
                diasDelPeriodo = this.DataSource.Select(it => it.FechaFinal.Value.Day).Distinct().Count();
            }

            foreach (var centro in centros) {
                var tiempoEstimado = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.TiempoEstimado) / 60;
                var tiempoReal = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.TiempoReal) / 60;

                this.PorCentro.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = tiempoEstimado.ToString() + " hrs.", Value = tiempoEstimado });
                this.PorCentro.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = tiempoReal.ToString() + " hrs.", Value = tiempoReal });
                this.PorCentro.Series[2].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = (48 - tiempoReal).ToString() + " hrs.", Value = diasDelPeriodo * 8 });
                this.PorCentro.Series[2].ShowLabels = true;

                var cantidadEstimada = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.CantidadEstimada);
                var cantidadReal = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.CantidadReal);
                this.PorDepartamento.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = cantidadEstimada.ToString(), Value = cantidadEstimada });
                this.PorDepartamento.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = cantidadReal.ToString(), Value = cantidadReal });
                this.PorDepartamento.Series[1].ShowLabels = true;
            }
        }

        private void ChartView_LabelFormatting(object sender, ChartViewLabelFormattingEventArgs e) {
            e.LabelElement.BackColor = this.BackColor;
            e.LabelElement.Text = e.LabelElement.Text;
        }

        private void Filtrar_Click(object sender, EventArgs e) {
            if (this.comboBox1.SelectedValue != null) {
                int index = (int)this.comboBox1.SelectedValue;
                this.Prueba(index);
            }
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e) {
            if (!checkBox1.Checked) {
                this.Filtrar.Enabled = false;
                this.comboBox1.Enabled = false;
                this.Prueba(0);
            } else {
                this.Filtrar.Enabled = true;
                this.comboBox1.Enabled = true;
            }
        }
    }
}
