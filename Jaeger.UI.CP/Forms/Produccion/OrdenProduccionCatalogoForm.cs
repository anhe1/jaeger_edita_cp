﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Cotizador.Produccion.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// formulario de ordenes de produccion
    /// </summary>
    public partial class OrdenProduccionCatalogoForm : Cotizador.Forms.Produccion.OrdenProduccionGridForm {
        #region declaraciones
        protected internal Aplication.Almacen.PT.Contracts.IRemisionadoService RemisionService;
        protected internal Aplication.Almacen.DP.Contracts.IRemisionadoService RemisionInternaService;
        protected internal BindingList<Domain.Almacen.PT.Entities.RemisionPartidaModel> DataRemisionado;
        protected internal BindingList<Domain.Almacen.DP.Entities.RemisionPartidaModel> DataRemisionSingle;
        protected internal GridViewTemplate GridRemisionInterna = new GridViewTemplate() { Caption = "Remisión Interna" };
        protected internal GridViewTemplate GridTemplateRemision = new GridViewTemplate() { Caption = "Remisionado" };
        protected internal RadMenuItem MenuItemCertificado = new RadMenuItem { Text = "Certificado", Name = "Certificado", ToolTipText = "Certificado de calidad" };
        protected internal RadMenuItem MenuItemCrearRemisionInterna = new RadMenuItem { Text = "Crear Remisión (Interna)", Name = "cppro_brem_emitido" };
        protected internal RadMenuItem MenuItemCrearRemision;
        #endregion

        public OrdenProduccionCatalogoForm(UIMenuElement menuElement) {
            this.TOrden.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.MenuItemCrearRemision = Services.RemisionadoMenuItems.RemisionCliente();
            this.Load += this.OrdenProduccionCatalogoForm_Load;
        }

        private void OrdenProduccionCatalogoForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;

            using (UI.Almacen.DP.Builder.IRemisionGridBuilder view = new UI.Almacen.DP.Builder.RemisionGridBuilder()) {
                this.GridRemisionInterna.Columns.AddRange(view.Templetes().RemisionConceptos().Build());
            }
            using (UI.Almacen.PT.Builder.IRemisionadoGridBuilder view = new UI.Almacen.PT.Builder.RemisionadoGridBuilder()) {
                this.GridTemplateRemision.Columns.AddRange(view.Templetes().RemisionYConceptosSinValores().Build());
            }

            this.TOrden.ShowHerramientas = true;
            this.TOrden.GridData.AllowEditRow = true;
            this.TOrden.GridData.MasterTemplate.Templates.AddRange(this.GridTemplateRemision, this.GridRemisionInterna);
            this.TOrden.Herramientas.Items.Add(this.MenuItemCertificado);
            this.TOrden.menuContextual.Items.Add(this.MenuItemCrearRemisionInterna);
            this.TOrden.menuContextual.Items.Add(this.MenuItemCrearRemision);

            var crearRemisionInterna = new Domain.Base.ValueObjects.UIAction(ConfigService.GeMenuElement(MenuItemCrearRemisionInterna.Name).Permisos);
            this.MenuItemCrearRemisionInterna.Enabled = crearRemisionInterna.Agregar;
            var crearRemision = new Domain.Base.ValueObjects.UIAction(ConfigService.GeMenuElement(MenuItemCrearRemision.Name).Permisos);
            this.MenuItemCrearRemision.Visibility = (crearRemision.Agregar ? ElementVisibility.Visible : ElementVisibility.Collapsed);

            this.MenuItemCertificado.Click += this.TOrden_Certificado_Click;
            this.MenuItemCrearRemisionInterna.Click += this.CrearRemisionInterna_Click;
            this.MenuItemCrearRemision.Click += this.CrearRemision_Click;
            this.TOrden.Cerrar.Click += this.TOrden_Cerrar_Click;
            
            this.GridRemisionInterna.Standard();
            this.GridTemplateRemision.Standard();
            this.GridRemisionInterna.HierarchyDataProvider = new GridViewEventDataProvider(this.GridRemisionInterna);
            this.GridTemplateRemision.HierarchyDataProvider = new GridViewEventDataProvider(this.GridTemplateRemision);
            this.TOrden.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
        }

        #region menu contextual
        public virtual void CrearRemisionInterna_Click(object sender, EventArgs e) {
            var seleccion = this.TOrden.GridData.SelectedRows.Where(it => it.IsSelected == true).Select(x => x.DataBoundItem as OrdenProduccionDetailModel).ToList();
            if (seleccion != null) {
                if (seleccion.Count > 0) {
                    var nuevaRemision = new Domain.Almacen.DP.Entities.RemisionDetailModel();
                    foreach (var seleccionado in seleccion) {
                        nuevaRemision.Conceptos.Add(new Domain.Almacen.DP.Entities.RemisionConceptoDetailModel {
                            IdPedido = seleccionado.IdOrden,
                            Activo = true,
                            Cantidad = seleccionado.Cantidad,
                            Cliente = seleccionado.Cliente,
                            IdCliente = seleccionado.IdDirectorio,
                            Producto = seleccionado.Nombre,
                            Efecto = Domain.Almacen.DP.ValueObjects.ValeAlmPTTipoEnum.Egreso,
                            Unitario = seleccionado.ValorUnitario,
                            UnidadFactor = seleccionado.Unidad,
                            Unidad = seleccionado.Unidad.ToString(),
                            Identificador = string.Format("{0}-{1:000000}-{2:000000}-{3:000000}", 0, seleccionado.IdOrden, seleccionado.IdDirectorio, seleccionado.PEDPRD_COM_ID)
                        });
                    }
                    var nuevo = new Almacen.DP.RemisionForm(nuevaRemision, 0) { MdiParent = this.ParentForm };
                    nuevo.Show();
                }
            }
        }

        public virtual void CrearRemision_Click(object sender, EventArgs e) {
            var seleccion = this.TOrden.GridData.SelectedRows.Where(it => it.IsSelected == true).Select(x => x.DataBoundItem as OrdenProduccionDetailModel).ToList();
            if (seleccion != null) {
                if (seleccion.Count > 0) {
                    var noVerificado = seleccion.Where(it => it.IdVerificado == 0).ToList();
                    if (noVerificado.Count > 0) {
                        RadMessageBox.Show(this, Properties.Resources.msg_OrdenNoVerificada, "Atención", MessageBoxButtons.OK);
                        return;
                    }

                    var nuevaRemision = new Domain.Almacen.PT.Entities.RemisionDetailModel();
                    foreach (var seleccionado in seleccion) {
                        nuevaRemision.Conceptos.Add(Aplication.CP.Service.RemisionService.Convertir(seleccionado));
                    }
                    var nuevo = new Almacen.PT.RemisionFiscalForm(RemisionService, nuevaRemision) { MdiParent = this.ParentForm };
                    nuevo.Show();
                }
            }
        }
        
        #endregion

        #region barra de herramientas
        public virtual void TOrden_Certificado_Click(object sender, EventArgs eventArgs) {
            var selected = this.TOrden.GetCurrent<OrdenProduccionDetailModel>();
            if (selected != null) {
                var orden = new Domain.CCalidad.Entities.OrdenProduccionModel {
                    IdOrden = selected.IdOrden,
                    Descripcion = selected.Nombre,
                    Cliente = selected.Cliente
                };
                var certificado = new OrdenProduccionCertificadoForm(orden);
                certificado.ShowDialog(this);
            }
        }

        public virtual void TOrden_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
           if (e.Template.Caption == this.GridTemplateRemision.Caption) {
                var rowview = e.ParentRow.DataBoundItem as OrdenProduccionDetailModel;
                if (rowview != null) {
                    using (var espera = new Waiting1Form(this.Remisionado)) {
                        espera.Text = "Consultando remisiones ...";
                        espera.ShowDialog(this);
                    }

                    if (this.DataRemisionado != null) {
                        foreach (var item in this.DataRemisionado) {
                            var row = e.Template.Rows.NewRow();
                            UI.Almacen.PT.Builder.RemisionadoGridBuilder.ConvertToSinImportes(row, item);
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.GridRemisionInterna.Caption) {
                var rowview = e.ParentRow.DataBoundItem as OrdenProduccionDetailModel;
                if (rowview != null) {
                    using (var espera = new Waiting1Form(this.RemisionadoInterno)) {
                        espera.Text = "Consultando remisiones internas ...";
                        espera.ShowDialog(this);
                    }
                    if (this.DataRemisionSingle != null) {
                        foreach (var item in this.DataRemisionSingle) {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["Folio"].Value = item.Folio;
                            row.Cells["IdStatus"].Value = item.IdStatus;
                            row.Cells["FechaEmision"].Value = item.FechaEmision;
                            row.Cells["Cantidad"].Value = item.Cantidad;
                            row.Cells["Unidad"].Value = item.Unidad;
                            row.Cells["Cliente"].Value = item.ReceptorNombre;
                            row.Cells["IdPedido"].Value = item.IdPedido;
                            row.Cells["Cliente"].Value = item.Cliente;
                            row.Cells["Producto"].Value = item.Producto;
                            row.Cells["Identificador"].Value = item.Identificador;
                            row.Cells["IdDocumento"].Value = item.IdDocumento;
                            row.Cells["Creo"].Value = item.Creo;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        public virtual void Remisionado() {
            var _seleccionado = this.TOrden.GridData.CurrentRow.DataBoundItem as OrdenProduccionDetailModel;
            if (_seleccionado != null) {
                var d0 = Aplication.Almacen.PT.Services.RemisionadoService.Query().WithConceptos(_seleccionado.IdOrden).Build();
                this.DataRemisionado = new BindingList<Domain.Almacen.PT.Entities.RemisionPartidaModel>(this.RemisionService.GetList<Domain.Almacen.PT.Entities.RemisionPartidaModel>(d0).ToList());
                if (this.DataRemisionado == null) {
                    this.DataRemisionado = new BindingList<Domain.Almacen.PT.Entities.RemisionPartidaModel>();
                }
            }
        }

        public virtual void RemisionadoInterno() {
            var _seleccionado = this.TOrden.GridData.CurrentRow.DataBoundItem as OrdenProduccionDetailModel;
            if (_seleccionado != null) {
                var query = Aplication.Almacen.DP.Services.RemisionService.Query().ByOrden(_seleccionado.IdOrden).Build();
                this.DataRemisionSingle = this.RemisionInternaService.GetList<Domain.Almacen.DP.Entities.RemisionPartidaModel>(query);
                if (this.DataRemisionSingle == null) {
                    this.DataRemisionSingle = new BindingList<Domain.Almacen.DP.Entities.RemisionPartidaModel>();
                }
            }
        }

        public virtual void Crear_RemisionCliente() {
            var seleccion = this.TOrden.GridData.SelectedRows.Where(it => it.IsSelected == true).Select(x => x.DataBoundItem as OrdenProduccionDetailModel).ToList();
            if (seleccion != null) {
                if (seleccion.Count > 0) {
                    var noVerificado = seleccion.Where(it => it.IdVerificado == 0).ToList();
                    if (noVerificado.Count > 0) {
                        RadMessageBox.Show(this, Properties.Resources.msg_OrdenNoVerificada, "Atención", MessageBoxButtons.OK);
                        return;
                    }
                    Aplication.Almacen.PT.Builder.IRemisionBuilder d2 = new Aplication.Almacen.PT.Builder.RemisionBuilder();
                    foreach (var item in seleccion) {
                        d2.AddConcepto(Aplication.CP.Service.RemisionService.Convertir(item));
                    }
                    var nuevo = new Almacen.PT.RemisionFiscalForm(this.RemisionService, d2.Build() as Domain.Almacen.PT.Entities.RemisionDetailModel) { MdiParent = this.ParentForm };
                    nuevo.Show();
                }
            }
        }
        #endregion
    }
}
