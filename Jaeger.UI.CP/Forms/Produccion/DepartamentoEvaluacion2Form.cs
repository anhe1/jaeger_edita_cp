﻿/// develop: 220720172304
/// purpose:
using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.Charting;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Cotizador.Contracts;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Cotizador.Produccion.Entities;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// Reporte de tiempos y cantidades por departamento por oficial
    /// </summary>
    public partial class DepartamentoEvaluacion2Form : RadForm {
        protected IProduccionService Service;
        private List<ComponenteLineaProduccion> DataSource;
        private List<ComponenteLineaProduccion> DataSourceCopy;

        public DepartamentoEvaluacion2Form(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void DepartamentoEvaluacion2Form_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            using (Cotizador.Forms.Produccion.Builder.IOrdenProduccionGridBuilder builder = new Cotizador.Forms.Produccion.Builder.OrdenProduccionGridBuilder()) {
                this.gridData.Standard();
                this.gridData.Columns.AddRange(builder.Templetes().AvanceProduccion().Build());
            }

            this.Service = new ProduccionService();
            var sem = Domain.Base.Services.DateTimeExtension.FirstDayWeek();
            this.TPeriodo.FInicial.Value = sem;
            this.TPeriodo.FFinal.Value = sem.AddDays(6);
            using (var espera = new UI.Common.Forms.Waiting2Form(this.Inicia)) {
                espera.Text = "un momento ...";
                espera.ShowDialog(this);
            }

            this.TPeriodo.Actualizar.Click += this.TEvaluacion_Actualizar_Click;
            this.TPeriodo.Imprimir.Click += this.TEvaluacion_Imprimir_Click;
            this.TPeriodo.Cerrar.Click += this.TEvaluacion_Cerrar_Click;
        }

        #region barra de herramientas
        private void TEvaluacion_Actualizar_Click(object sender, EventArgs e) {
            if (this.TPeriodo.Departamento.SelectedItem == null) {
                RadMessageBox.Show(this, "Selecciona un departamento", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            using (var espera = new UI.Common.Forms.Waiting2Form(this.Consultar)) {
                espera.Text = "Actualizando, espere un momento...";
                espera.ShowDialog(this);
            }

            var centros = this.DataSource.Select(it => it.Centro).Distinct().ToList();
            var oficiales = this.DataSource.Select(it => it.Oficial).Distinct().ToList();
            var procesos = this.DataSource.Select(it => it.Proceso).Distinct().ToList();
            var dias = this.DataSource.Select(it => it.FechaFinal.Value.Day).Distinct().Count();

            this.ChartTiempo.Series[0].DataPoints.Clear();
            this.ChartTiempo.Series[1].DataPoints.Clear();
            this.ChartCentro.Series[0].DataPoints.Clear();
            this.ChartCentro.Series[1].DataPoints.Clear();
            this.ChartOficial.Series[0].DataPoints.Clear();
            this.ChartOficial.Series[1].DataPoints.Clear();
            this.ChartSalarios.Series[0].DataPoints.Clear();
            this.ChartSalarios.Series[1].DataPoints.Clear();
            this.ChartSalarios.Series[2].DataPoints.Clear();

            foreach (var centro in centros) {
                var tiempoEstimado = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.TiempoEstimado) / 60;
                var tiempoReal = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.TiempoReal) / 60;

                this.ChartTiempo.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = tiempoEstimado.ToString() + " hrs.", Value = tiempoEstimado});
                this.ChartTiempo.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = tiempoReal.ToString() + " hrs.", Value = tiempoReal});
                var cantidadEstimada = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.CantidadEstimada);
                var cantidadReal = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.CantidadReal);
                this.ChartCentro.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = cantidadEstimada.ToString(), Value = cantidadEstimada });
                this.ChartCentro.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = centro, Label = cantidadReal.ToString(), Value = cantidadReal });
                this.ChartCentro.Series[1].ShowLabels = true;
            }

            foreach (var oficial in oficiales) {
                if (oficial != null) {
                    var cantidadEstimada = this.DataSource.Where(it => it.Oficial == oficial).Sum(it => it.CantidadEstimada);
                    var cantidadReal = this.DataSource.Where(it => it.Oficial == oficial).Sum(it => it.CantidadReal);
                    this.ChartOficial.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = oficial.ToUpper(), Label = cantidadEstimada.ToString(), Value = cantidadEstimada });
                    this.ChartOficial.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = oficial.ToUpper(), Label = cantidadReal.ToString(), Value = cantidadReal });
                    this.ChartOficial.Series[0].ShowLabels = true;
                    this.ChartOficial.Series[1].ShowLabels = true;


                    var tiempoEstimado = this.DataSource.Where(it => it.Oficial == oficial).Sum(it => it.TiempoEstimado) / 60;
                    var tiempoReal = this.DataSource.Where(it => it.Oficial == oficial).Sum(it => it.TiempoReal) / 60;
                    var h0 = tiempoReal + new Random().Next(-1, 3);

                    this.ChartSalarios.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = oficial, Label = tiempoEstimado.ToString() , Value = (double)(tiempoEstimado) });
                    this.ChartSalarios.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = oficial, Label = tiempoReal.ToString(), Value = (double)(tiempoReal) });
                    this.ChartSalarios.Series[2].DataPoints.Add(new CategoricalDataPoint { Category = oficial, Label = h0.ToString() , Value = h0 });
                }
            }

            foreach (var proceso in procesos) {
                var tiempoEstimado = this.DataSource.Where(it => it.Proceso == proceso).Sum(it => it.TiempoEstimado) / 60;
                var tiempoReal = this.DataSource.Where(it => it.Proceso == proceso).Sum(it => it.TiempoReal) / 60;
                
            }

            this.ChartTiempo.Title = string.Format("{0}: Producción por Centro (Tie. Est. vs Real)", this.TPeriodo.Departamento.Text, this.TPeriodo.FInicial.Value.ToString("dddd, dd MMMM yyyy"), this.TPeriodo.FFinal.Value.ToString("dddd, dd MMMM yyyy"));
            this.ChartCentro.Title = string.Format("{0}: Producción por Centro (Cantidad Est. vs Real)", this.TPeriodo.Departamento.Text, this.TPeriodo.FInicial.Value.ToString("dddd, dd MMMM yyyy"), this.TPeriodo.FFinal.Value.ToString("dddd, dd MMMM yyyy"));
            this.ChartOficial.Title = string.Format("{0}: Producción por Oficial (Cant. Est. vs Cant. Real)", this.TPeriodo.Departamento.Text, this.TPeriodo.FInicial.Value.ToString("dddd, dd MMMM yyyy"), this.TPeriodo.FFinal.Value.ToString("dddd, dd MMMM yyyy"));
            this.ChartSalarios.Title = string.Format("{0}: Producción por Oficial (Tie. Estimado vs Tie. Real)", this.TPeriodo.Departamento.Text, this.TPeriodo.FInicial.Value.ToString("dddd, dd MMMM yyyy"), this.TPeriodo.FFinal.Value.ToString("dddd, dd MMMM yyyy"));
            this.gridData.DataSource = this.DataSource;
        }

        private void TEvaluacion_Imprimir_Click(object sender, EventArgs e) {
            var document = new RadPrintDocument {
                Landscape = true
            };
            this.ChartTiempo.PrintPreview(document);
            this.ChartCentro.PrintPreview(document);
            this.ChartOficial.PrintPreview(document);
            this.ChartSalarios.PrintPreview(document);
        }

        private void TEvaluacion_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        private void Inicia() {
            var deptos = this.Service.GetList<Domain.Cotizador.Empresa.Entities.DepartamentoModel>(Aplication.Cotizador.Services.DepartamentosService.Query().OnlyActive().Build());
            this.TPeriodo.Departamento.DataSource = deptos;
            this.TPeriodo.Departamento.DisplayMember = "Departamento";
            this.TPeriodo.Departamento.ValueMember = "IdDepartamento";
        }

        private void Consultar() {
            var seleccioando = this.TPeriodo.Departamento.SelectedItem.DataBoundItem as Domain.Cotizador.Empresa.Entities.DepartamentoModel;
            if (seleccioando != null) {
                var condiciones = new List<IConditional> {
                    new Conditional("EDP1_ID", seleccioando.IdDepartamento.ToString()),
                new Conditional("PEDPRO_FEC_FIN", this.TPeriodo.FInicial.Value.ToShortDateString(), ConditionalTypeEnum.GreaterThanOrEqual),
                new Conditional("PEDPRO_FEC_FIN", this.TPeriodo.FFinal.Value.ToShortDateString(), ConditionalTypeEnum.LessThanOrEqual)
            };
                this.DataSource = this.Service.GetList<ComponenteLineaProduccion>(condiciones).ToList();
                this.DataSourceCopy = new List<ComponenteLineaProduccion>(this.DataSource.ToList());
            }
        }

        private void ChartView_LabelFormatting(object sender, ChartViewLabelFormattingEventArgs e) {
            e.LabelElement.Text = e.LabelElement.Text;
        }
    }
}
