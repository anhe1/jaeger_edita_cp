﻿using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// Historial de ordenes de produccion
    /// </summary>
    public class OrdenProduccionHistorialForm : OrdenProduccionCatalogoForm {
        public OrdenProduccionHistorialForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Produccion: Ordenes Historial";
            this.Load += OrdenProduccionHistorialForm_Load;
        }

        private void OrdenProduccionHistorialForm_Load(object sender, System.EventArgs e) {
            this.TOrden.Service = new OrdenProduccionService();
            this.RemisionInternaService = new Aplication.CP.Service.RemisionadoInternoService();
        }
    }
}
