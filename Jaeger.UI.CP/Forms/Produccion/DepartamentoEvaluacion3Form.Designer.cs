﻿namespace Jaeger.UI.CP.Forms.Produccion {
    partial class DepartamentoEvaluacion3Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries1 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries2 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.LineSeries lineSeries1 = new Telerik.WinControls.UI.LineSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea2 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis2 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis2 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries3 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries4 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.LineSeries lineSeries2 = new Telerik.WinControls.UI.LineSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea3 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis3 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis3 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries5 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries6 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea4 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis4 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis4 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries7 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries8 = new Telerik.WinControls.UI.BarSeries();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.Panel1 = new Telerik.WinControls.UI.SplitPanel();
            this.PorOficial = new Telerik.WinControls.UI.RadChartView();
            this.Panel2 = new Telerik.WinControls.UI.SplitPanel();
            this.PorCentro = new Telerik.WinControls.UI.RadChartView();
            this.checkBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.Filtrar = new Telerik.WinControls.UI.RadButton();
            this.comboBox1 = new Telerik.WinControls.UI.RadDropDownList();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.Panel3 = new Telerik.WinControls.UI.SplitPanel();
            this.PorCantidad = new Telerik.WinControls.UI.RadChartView();
            this.Panel4 = new Telerik.WinControls.UI.SplitPanel();
            this.PorDepartamento = new Telerik.WinControls.UI.RadChartView();
            this.TPeriodo = new Jaeger.UI.Common.Forms.TbPeriodoControl();
            this.PagesView = new Telerik.WinControls.UI.RadPageView();
            this.PageEvaluacion = new Telerik.WinControls.UI.RadPageViewPage();
            this.PageDatos = new Telerik.WinControls.UI.RadPageViewPage();
            this.TDatos = new Jaeger.UI.Common.Forms.GridStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel1)).BeginInit();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PorOficial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel2)).BeginInit();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PorCentro)).BeginInit();
            this.PorCentro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filtrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel3)).BeginInit();
            this.Panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PorCantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel4)).BeginInit();
            this.Panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PorDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PagesView)).BeginInit();
            this.PagesView.SuspendLayout();
            this.PageEvaluacion.SuspendLayout();
            this.PageDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1318, 625);
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radSplitContainer2);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1318, 314);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.005464471F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 5);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.Panel1);
            this.radSplitContainer2.Controls.Add(this.Panel2);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1318, 314);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // Panel1
            // 
            this.Panel1.Controls.Add(this.PorOficial);
            this.Panel1.Location = new System.Drawing.Point(0, 0);
            this.Panel1.Name = "Panel1";
            // 
            // 
            // 
            this.Panel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.Panel1.Size = new System.Drawing.Size(657, 314);
            this.Panel1.TabIndex = 0;
            this.Panel1.TabStop = false;
            this.Panel1.Text = "splitPanel4";
            // 
            // PorOficial
            // 
            this.PorOficial.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            categoricalAxis1.LabelRotationAngle = 300D;
            categoricalAxis1.Title = "";
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.LabelFormat = "{0:n0}";
            linearAxis1.LabelRotationAngle = 300D;
            linearAxis1.TickOrigin = null;
            linearAxis1.Title = "Horas";
            this.PorOficial.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.PorOficial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PorOficial.LegendTitle = "Tie. Hrs.";
            this.PorOficial.Location = new System.Drawing.Point(0, 0);
            this.PorOficial.Name = "PorOficial";
            barSeries1.HorizontalAxis = categoricalAxis1;
            barSeries1.LabelFormat = "{0:n0}";
            barSeries1.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries1.LegendTitle = "Tie. Est.";
            barSeries1.ShowLabels = true;
            barSeries1.VerticalAxis = linearAxis1;
            barSeries2.HorizontalAxis = categoricalAxis1;
            barSeries2.LabelFormat = "{0:n0}";
            barSeries2.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries2.LegendTitle = "Tie. Real";
            barSeries2.ShowLabels = true;
            barSeries2.VerticalAxis = linearAxis1;
            lineSeries1.HorizontalAxis = categoricalAxis1;
            lineSeries1.LabelAngle = 90D;
            lineSeries1.LabelDistanceToPoint = 15D;
            lineSeries1.LegendTitle = "Hrs. Reloj";
            lineSeries1.VerticalAxis = linearAxis1;
            this.PorOficial.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries1,
            barSeries2,
            lineSeries1});
            this.PorOficial.ShowGrid = false;
            this.PorOficial.ShowLegend = true;
            this.PorOficial.ShowTitle = true;
            this.PorOficial.ShowToolTip = true;
            this.PorOficial.ShowTrackBall = true;
            this.PorOficial.Size = new System.Drawing.Size(657, 314);
            this.PorOficial.TabIndex = 1;
            this.PorOficial.Title = "Tie. Estimado vs Real por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.PorOficial.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Tie. Estimado vs Real por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.PorOficial.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.PorCentro);
            this.Panel2.Location = new System.Drawing.Point(661, 0);
            this.Panel2.Name = "Panel2";
            // 
            // 
            // 
            this.Panel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.Panel2.Size = new System.Drawing.Size(657, 314);
            this.Panel2.TabIndex = 1;
            this.Panel2.TabStop = false;
            this.Panel2.Text = "splitPanel5";
            // 
            // PorCentro
            // 
            this.PorCentro.AreaDesign = cartesianArea2;
            categoricalAxis2.IsPrimary = true;
            categoricalAxis2.LabelFitMode = Telerik.Charting.AxisLabelFitMode.MultiLine;
            categoricalAxis2.LabelRotationAngle = 300D;
            categoricalAxis2.Title = "";
            linearAxis2.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis2.IsPrimary = true;
            linearAxis2.LabelFitMode = Telerik.Charting.AxisLabelFitMode.MultiLine;
            linearAxis2.LabelFormat = "{0:n0}";
            linearAxis2.LabelRotationAngle = 300D;
            linearAxis2.TickOrigin = null;
            linearAxis2.Title = "Horas";
            this.PorCentro.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis2,
            linearAxis2});
            this.PorCentro.Controls.Add(this.checkBox1);
            this.PorCentro.Controls.Add(this.Filtrar);
            this.PorCentro.Controls.Add(this.comboBox1);
            this.PorCentro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PorCentro.LegendTitle = "Tie. Hrs.";
            this.PorCentro.Location = new System.Drawing.Point(0, 0);
            this.PorCentro.Name = "PorCentro";
            barSeries3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(202)))), ((int)(((byte)(238)))));
            barSeries3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(202)))), ((int)(((byte)(238)))));
            barSeries3.HorizontalAxis = categoricalAxis2;
            barSeries3.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries3.LegendTitle = "Tie. Est.";
            barSeries3.ShowLabels = true;
            barSeries3.VerticalAxis = linearAxis2;
            barSeries4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(169)))), ((int)(((byte)(215)))));
            barSeries4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(169)))), ((int)(((byte)(215)))));
            barSeries4.HorizontalAxis = categoricalAxis2;
            barSeries4.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries4.LegendTitle = "Tie. Real";
            barSeries4.ShowLabels = true;
            barSeries4.VerticalAxis = linearAxis2;
            lineSeries2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(123)))), ((int)(((byte)(169)))));
            lineSeries2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(123)))), ((int)(((byte)(169)))));
            lineSeries2.HorizontalAxis = categoricalAxis2;
            lineSeries2.LabelAngle = 90D;
            lineSeries2.LabelDistanceToPoint = 15D;
            lineSeries2.LegendTitle = "Hrs. Reloj";
            lineSeries2.VerticalAxis = linearAxis2;
            this.PorCentro.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries3,
            barSeries4,
            lineSeries2});
            this.PorCentro.ShowGrid = false;
            this.PorCentro.ShowLegend = true;
            this.PorCentro.ShowTitle = true;
            this.PorCentro.ShowToolTip = true;
            this.PorCentro.Size = new System.Drawing.Size(657, 314);
            this.PorCentro.TabIndex = 0;
            this.PorCentro.Title = "Tie. Estimado vs Real por Centro";
            this.PorCentro.LabelFormatting += new Telerik.WinControls.UI.ChartViewLabelFormattingEventHandler(this.ChartView_LabelFormatting);
            ((Telerik.WinControls.UI.RadChartElement)(this.PorCentro.GetChildAt(0))).CustomFontSize = 12F;
            ((Telerik.WinControls.UI.ChartTitleElement)(this.PorCentro.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Tie. Estimado vs Real por Centro";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.PorCentro.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            ((Telerik.WinControls.UI.ChartLegendElement)(this.PorCentro.GetChildAt(0).GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Segoe UI", 9F);
            ((Telerik.WinControls.UI.ChartLegendElement)(this.PorCentro.GetChildAt(0).GetChildAt(0).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.Location = new System.Drawing.Point(452, 3);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(113, 18);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Por Departamento";
            this.checkBox1.CheckStateChanged += new System.EventHandler(this.checkBox1_CheckStateChanged);
            // 
            // Filtrar
            // 
            this.Filtrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Filtrar.Enabled = false;
            this.Filtrar.Location = new System.Drawing.Point(579, 23);
            this.Filtrar.Name = "Filtrar";
            this.Filtrar.Size = new System.Drawing.Size(75, 23);
            this.Filtrar.TabIndex = 2;
            this.Filtrar.Text = "Filtrar";
            this.Filtrar.Click += new System.EventHandler(this.Filtrar_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.Enabled = false;
            this.comboBox1.Location = new System.Drawing.Point(452, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 1;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer3);
            this.splitPanel2.Location = new System.Drawing.Point(0, 318);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1318, 307);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.005464491F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -5);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.Panel3);
            this.radSplitContainer3.Controls.Add(this.Panel4);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer3.Size = new System.Drawing.Size(1318, 307);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            // 
            // Panel3
            // 
            this.Panel3.Controls.Add(this.PorCantidad);
            this.Panel3.Location = new System.Drawing.Point(0, 0);
            this.Panel3.Name = "Panel3";
            // 
            // 
            // 
            this.Panel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.Panel3.Size = new System.Drawing.Size(657, 307);
            this.Panel3.TabIndex = 0;
            this.Panel3.TabStop = false;
            this.Panel3.Text = "splitPanel6";
            // 
            // PorCantidad
            // 
            this.PorCantidad.AreaDesign = cartesianArea3;
            categoricalAxis3.IsPrimary = true;
            categoricalAxis3.LabelRotationAngle = 300D;
            categoricalAxis3.Title = "";
            linearAxis3.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis3.IsPrimary = true;
            linearAxis3.LabelFormat = "{0:n0}";
            linearAxis3.LabelRotationAngle = 300D;
            linearAxis3.TickOrigin = null;
            linearAxis3.Title = "Cant. Producida";
            this.PorCantidad.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis3,
            linearAxis3});
            this.PorCantidad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PorCantidad.LegendTitle = "Cantidad.";
            this.PorCantidad.Location = new System.Drawing.Point(0, 0);
            this.PorCantidad.Name = "PorCantidad";
            barSeries5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            barSeries5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            barSeries5.HorizontalAxis = categoricalAxis3;
            barSeries5.LabelFormat = "{0:n0}";
            barSeries5.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries5.LegendTitle = "Est.";
            barSeries5.VerticalAxis = linearAxis3;
            barSeries6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(135)))), ((int)(((byte)(90)))));
            barSeries6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(135)))), ((int)(((byte)(90)))));
            barSeries6.HorizontalAxis = categoricalAxis3;
            barSeries6.LabelFormat = "{0:n0}";
            barSeries6.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries6.LegendTitle = "Real";
            barSeries6.VerticalAxis = linearAxis3;
            this.PorCantidad.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries5,
            barSeries6});
            this.PorCantidad.ShowGrid = false;
            this.PorCantidad.ShowLegend = true;
            this.PorCantidad.ShowTitle = true;
            this.PorCantidad.Size = new System.Drawing.Size(657, 307);
            this.PorCantidad.TabIndex = 1;
            this.PorCantidad.Title = "Producción por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.PorCantidad.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.PorCantidad.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // Panel4
            // 
            this.Panel4.Controls.Add(this.PorDepartamento);
            this.Panel4.Location = new System.Drawing.Point(661, 0);
            this.Panel4.Name = "Panel4";
            // 
            // 
            // 
            this.Panel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.Panel4.Size = new System.Drawing.Size(657, 307);
            this.Panel4.TabIndex = 2;
            this.Panel4.TabStop = false;
            this.Panel4.Text = "splitPanel3";
            // 
            // PorDepartamento
            // 
            this.PorDepartamento.AreaDesign = cartesianArea4;
            categoricalAxis4.IsPrimary = true;
            categoricalAxis4.LabelRotationAngle = 300D;
            categoricalAxis4.Title = "";
            linearAxis4.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis4.IsPrimary = true;
            linearAxis4.LabelFormat = "{0:n0}";
            linearAxis4.LabelRotationAngle = 300D;
            linearAxis4.TickOrigin = null;
            linearAxis4.Title = "Cantidad Producida";
            this.PorDepartamento.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis4,
            linearAxis4});
            this.PorDepartamento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PorDepartamento.LegendTitle = "Cantidad";
            this.PorDepartamento.Location = new System.Drawing.Point(0, 0);
            this.PorDepartamento.Name = "PorDepartamento";
            barSeries7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            barSeries7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            barSeries7.HorizontalAxis = categoricalAxis4;
            barSeries7.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries7.LegendTitle = "Cant. Est.";
            barSeries7.ShowLabels = true;
            barSeries7.VerticalAxis = linearAxis4;
            barSeries8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(135)))), ((int)(((byte)(90)))));
            barSeries8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(135)))), ((int)(((byte)(90)))));
            barSeries8.HorizontalAxis = categoricalAxis4;
            barSeries8.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries8.LegendTitle = "Cant. Real";
            barSeries8.VerticalAxis = linearAxis4;
            this.PorDepartamento.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries7,
            barSeries8});
            this.PorDepartamento.ShowGrid = false;
            this.PorDepartamento.ShowLegend = true;
            this.PorDepartamento.ShowTitle = true;
            this.PorDepartamento.ShowToolTip = true;
            this.PorDepartamento.Size = new System.Drawing.Size(657, 307);
            this.PorDepartamento.TabIndex = 1;
            this.PorDepartamento.Title = "Producción por Centro (Cant. Est. vs Real)";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.PorDepartamento.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción por Centro (Cant. Est. vs Real)";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.PorDepartamento.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // TPeriodo
            // 
            this.TPeriodo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPeriodo.FFinalCaption = " al:";
            this.TPeriodo.FInicialCaption = "Período del:";
            this.TPeriodo.Location = new System.Drawing.Point(0, 0);
            this.TPeriodo.Name = "TPeriodo";
            this.TPeriodo.ShowActualizar = true;
            this.TPeriodo.ShowCerrar = true;
            this.TPeriodo.ShowDepartamento = true;
            this.TPeriodo.ShowFFinal = true;
            this.TPeriodo.ShowFInicial = true;
            this.TPeriodo.ShowImprimir = true;
            this.TPeriodo.Size = new System.Drawing.Size(1339, 30);
            this.TPeriodo.TabIndex = 1;
            // 
            // PagesView
            // 
            this.PagesView.Controls.Add(this.PageEvaluacion);
            this.PagesView.Controls.Add(this.PageDatos);
            this.PagesView.DefaultPage = this.PageEvaluacion;
            this.PagesView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PagesView.Location = new System.Drawing.Point(0, 30);
            this.PagesView.Name = "PagesView";
            this.PagesView.SelectedPage = this.PageEvaluacion;
            this.PagesView.Size = new System.Drawing.Size(1339, 673);
            this.PagesView.TabIndex = 1;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PagesView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageEvaluacion
            // 
            this.PageEvaluacion.Controls.Add(this.radSplitContainer1);
            this.PageEvaluacion.ItemSize = new System.Drawing.SizeF(69F, 28F);
            this.PageEvaluacion.Location = new System.Drawing.Point(10, 37);
            this.PageEvaluacion.Name = "PageEvaluacion";
            this.PageEvaluacion.Size = new System.Drawing.Size(1318, 625);
            this.PageEvaluacion.Text = "Evaluación";
            // 
            // PageDatos
            // 
            this.PageDatos.Controls.Add(this.TDatos);
            this.PageDatos.ItemSize = new System.Drawing.SizeF(45F, 28F);
            this.PageDatos.Location = new System.Drawing.Point(10, 37);
            this.PageDatos.Name = "PageDatos";
            this.PageDatos.Size = new System.Drawing.Size(1441, 692);
            this.PageDatos.Text = "Datos";
            // 
            // TDatos
            // 
            this.TDatos.Caption = "";
            this.TDatos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TDatos.Location = new System.Drawing.Point(0, 0);
            this.TDatos.Name = "TDatos";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TDatos.Permisos = uiAction1;
            this.TDatos.ReadOnly = false;
            this.TDatos.ShowActualizar = false;
            this.TDatos.ShowAutorizar = false;
            this.TDatos.ShowAutosuma = false;
            this.TDatos.ShowCerrar = false;
            this.TDatos.ShowEditar = false;
            this.TDatos.ShowExportarExcel = false;
            this.TDatos.ShowFiltro = true;
            this.TDatos.ShowHerramientas = false;
            this.TDatos.ShowImagen = false;
            this.TDatos.ShowImprimir = false;
            this.TDatos.ShowNuevo = false;
            this.TDatos.ShowRemover = false;
            this.TDatos.ShowSeleccionMultiple = false;
            this.TDatos.Size = new System.Drawing.Size(1441, 692);
            this.TDatos.TabIndex = 0;
            // 
            // DepartamentoEvaluacion3Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1339, 703);
            this.Controls.Add(this.PagesView);
            this.Controls.Add(this.TPeriodo);
            this.Name = "DepartamentoEvaluacion3Form";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Producción: Evaluación por Oficial";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DepartamentoEvaluacion2Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Panel1)).EndInit();
            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PorOficial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel2)).EndInit();
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PorCentro)).EndInit();
            this.PorCentro.ResumeLayout(false);
            this.PorCentro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filtrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Panel3)).EndInit();
            this.Panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PorCantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel4)).EndInit();
            this.Panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PorDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PagesView)).EndInit();
            this.PagesView.ResumeLayout(false);
            this.PageEvaluacion.ResumeLayout(false);
            this.PageDatos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel Panel1;
        private Telerik.WinControls.UI.RadChartView PorCentro;
        private Telerik.WinControls.UI.SplitPanel Panel2;
        private Telerik.WinControls.UI.RadChartView PorDepartamento;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel Panel3;
        private Telerik.WinControls.UI.RadChartView PorOficial;
        private Common.Forms.TbPeriodoControl TPeriodo;
        private Telerik.WinControls.UI.RadPageView PagesView;
        private Telerik.WinControls.UI.RadPageViewPage PageEvaluacion;
        private Telerik.WinControls.UI.RadPageViewPage PageDatos;
        private Telerik.WinControls.UI.SplitPanel Panel4;
        private Telerik.WinControls.UI.RadChartView PorCantidad;
        private Common.Forms.GridStandarControl TDatos;
        private Telerik.WinControls.UI.RadDropDownList comboBox1;
        private Telerik.WinControls.UI.RadButton Filtrar;
        private Telerik.WinControls.UI.RadCheckBox checkBox1;
    }
}
