﻿/// develop: 220720172304
/// purpose:
using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Cotizador.Forms.Produccion;
using Jaeger.Aplication.Cotizador.Contracts;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Cotizador.Produccion.Entities;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// Reporte del area de produccion
    /// </summary>
    public partial class DepartamentoEvaluacion5Form : RadForm {
        #region
        protected IProduccionService Service;
        private List<ComponenteLineaProduccionRegistro> DataSource;
        private ProduccionReporte Reporte;
        private List<EmpleadoModel> Empleados;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public DepartamentoEvaluacion5Form(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void DepartamentoEvaluacion2Form_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.TPeriodo.ShowDepartamento = false;
            this.TPeriodo.Actualizar.Text = "Crear";
            using (Cotizador.Forms.Produccion.Builder.IOrdenProduccionGridBuilder builder = new Cotizador.Forms.Produccion.Builder.OrdenProduccionGridBuilder()) {
                this.TDatos.GridData.Standard();
                this.TDatos.GridData.Columns.AddRange(builder.Templetes().AvanceProduccion().Build());
            }
            this.Reporte = new ProduccionReporte();
            this.Service = new ProduccionService();
            var sem = Domain.Base.Services.DateTimeExtension.FirstDayWeek();
            this.TPeriodo.FInicial.Value = sem;
            this.TPeriodo.FFinal.Value = sem.AddDays(6);

            using (var espera = new Common.Forms.Waiting2Form(this.Inicia)) {
                espera.Text = "un momento ...";
                espera.ShowDialog(this);
            }
            this.radGridView1.DataSource = this.Reporte.Empleados;
            this.TPeriodo.Actualizar.Click += this.TEvaluacion_Actualizar_Click;
            this.TPeriodo.Imprimir.Click += this.TEvaluacion_Imprimir_Click;
            this.TPeriodo.Cerrar.Click += this.TEvaluacion_Cerrar_Click;
            this.PorSalarios.LabelFormatting += this.ChartView_LabelFormatting;
        }

        #region barra de herramientas
        private void TEvaluacion_Actualizar_Click(object sender, EventArgs e) {
            if (this.TPeriodo.Departamento.SelectedItem == null) {
                RadMessageBox.Show(this, "Selecciona un departamento", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            using (var espera = new Common.Forms.Waiting2Form(this.Consultar)) {
                espera.Text = "Actualizando, espere un momento...";
                espera.ShowDialog(this);
            }
            this.Reporte.Clear();
            this.Settings(this.PorCentro);
            this.Settings(this.PorSalarios);
            this.Settings(this.PorOficial);

            this.EvluacionCentros();
            this.EvaluacionOficial();
            this.TDatos.GridData.DataSource = this.DataSource;
        }

        private void TEvaluacion_Imprimir_Click(object sender, EventArgs e) {
            var printer = new ReporteForm(this.Reporte);
            printer.ShowDialog();
        }

        private void TEvaluacion_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region
        private void Filtrar_Click(object sender, EventArgs e) {
            if (this.comboBox1.SelectedValue != null) {
                int index = (int)this.comboBox1.SelectedValue;
                this.EvluacionCentros(index);
            }
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e) {
            if (!checkBox1.Checked) {
                this.Filtrar.Enabled = false;
                this.comboBox1.Enabled = false;
                this.EvluacionCentros(0);
            } else {
                this.Filtrar.Enabled = true;
                this.comboBox1.Enabled = true;
            }
        }
        #endregion

        private void EvaluacionOficial() {
            var oficiales = this.Reporte.Empleados.Select(it => it.Label).ToList(); //this.DataSource.Select(it => it.Oficial.ToUpper().Trim()).Distinct().OrderBy(it => it.Substring(0)).ToList();

            foreach (var oficial in oficiales) {
                if (oficial != null) {
                    var operador = new ProduccionReporte.ProduccionOficialRecord{
                        Label = oficial,
                        // value1 sera el tiempo estimado
                        Value1 = this.DataSource.Where(it => it.Oficial.Trim().ToUpper() == oficial).Sum(it => it.TiempoEstimado) / 60,
                        // value2 sera el tiempo real
                        Value2 = this.DataSource.Where(it => it.Oficial.Trim().ToUpper() == oficial).Sum(it => it.TiempoReal) / 60,
                    };

                    operador.Value1 = operador.Value1 + this.DataSource.Where(it => it.Auxiliar.Trim().ToUpper().Contains(oficial)).Sum(it => it.TiempoEstimado) / 60;
                    operador.Value2 = operador.Value2 + this.DataSource.Where(it => it.Auxiliar.Trim().ToUpper().Contains(oficial)).Sum(it => it.TiempoReal) / 60;
                        
                    var Jornadas1 = this.DataSource.Where(it => it.Oficial.Trim().ToUpper() == oficial).Select(it => it.FechaFinal.Value.Day).Distinct().Count();
                    var Jornadas2 = this.DataSource.Where(it => it.Auxiliar.Trim().ToUpper().Contains(oficial)).Select(it => it.FechaFinal.Value.Day).Distinct().Count();
                    operador.Jornadas = Jornadas1 + Jornadas2;
                    if (operador.Value2 > 0)
                    this.Reporte.PorOficial.Add(operador);

                    var salario = new ProduccionReporte.ProduccionSalarioRecord {
                        Label = oficial,
                        Value1 = (int)this.DataSource.Where(it => it.Oficial.Trim().ToUpper() == oficial).DistinctBy(it => it.IdLinea).Sum(it => it.TotalManoObra),
                        Value2 = (int)this.DataSource.Where(it => it.Oficial.Trim().ToUpper() == oficial).Sum(it => it.TotalManoObraR)
                    };
                    if (operador.Value2 > 0)
                        this.Reporte.Salarios.Add(salario);
                }
            }

            var bar0 = this.PorOficial.Series[0] as BarSeries;
            bar0.ValueMember = "Value1";
            bar0.CategoryMember = "Label";
            bar0.DisplayMember = "Value1";
            bar0.LabelFormat = "{0} hrs.";
            bar0.DataSource = this.Reporte.PorOficial;

            var bar1 = this.PorOficial.Series[1] as BarSeries;
            bar1.ValueMember = "Value2";
            bar1.CategoryMember = "Label";
            bar1.DisplayMember = "Value2";
            bar1.LabelFormat = "{0} hrs.";
            bar1.DataSource = this.Reporte.PorOficial;

            var linea2 = this.PorOficial.Series[2] as LineSeries;
            linea2.ValueMember = "HorasReloj";
            linea2.CategoryMember = "Label";
            linea2.DisplayMember = "HorasReloj";
            linea2.LabelFormat = "{0} hrs.";
            linea2.DataSource = this.Reporte.PorOficial;

            var bar00 = this.PorSalarios.Series[0] as BarSeries;
            bar00.ValueMember = "Value1";
            bar00.CategoryMember = "Label";
            bar00.DisplayMember = "Value1";
            bar00.DataSource = this.Reporte.Salarios;

            var bar11 = this.PorSalarios.Series[1] as BarSeries;
            bar11.ValueMember = "Value2";
            bar11.CategoryMember = "Label";
            bar11.DisplayMember = "Value2";
            bar11.DataSource = this.Reporte.Salarios;

            var bar22 = this.PorSalarios.Series[2] as BarSeries;
            bar22.ValueMember = "Diferencia";
            bar22.CategoryMember = "Label";
            bar22.DisplayMember = "Diferencia";
            bar22.DataSource = this.Reporte.Salarios;
        }

        private void EvluacionCentros(int idDepartamento = 0) {
            var diasDelPeriodo = 0;
            var centros = new List<string>();

            if (idDepartamento > 0) {
                centros = this.DataSource.Where(it => it.IdDepartamento == idDepartamento).Select(it => it.Centro).Distinct().ToList();
                diasDelPeriodo = this.DataSource.Where(it => it.IdDepartamento == idDepartamento).Select(it => it.FechaFinal.Value.Day).Distinct().Count();
            } else {
                centros = this.DataSource.Select(it => it.Centro).Distinct().ToList();
                diasDelPeriodo = this.DataSource.Select(it => it.FechaFinal.Value.Day).Distinct().Count();
            }

            foreach (var centro in centros) {
                var centro1 = new ProduccionReporte.ProduccionCentroRecord {
                    Label = centro,
                    Value1 = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.TiempoEstimado) / 60,
                    Value2 = this.DataSource.Where(it => it.Centro == centro).Sum(it => it.TiempoReal) / 60
                };
                this.Reporte.Centros.Add(centro1);
            }

            var bar1 = this.PorCentro.Series[0] as BarSeries;
            bar1.ValueMember = "Value1";
            bar1.CategoryMember = "Label";
            bar1.DisplayMember = "Value1";
            bar1.LabelFormat = "{0} hrs.";
            bar1.DataSource = this.Reporte.Centros;

            var bar2 = this.PorCentro.Series[1] as BarSeries;
            bar2.ValueMember = "Value2";
            bar2.CategoryMember = "Label";
            bar2.DisplayMember = "Value2";
            bar2.LabelFormat = "{0} hrs.";
            bar2.DataSource = this.Reporte.Centros;

           
        }

        private void Settings(RadChartView chartView) {
            foreach (var item in chartView.Series) {
                item.DataPoints.Clear();
                item.LabelRotationAngle = -90;
                item.ShowLabels = true;
            }
        }

        private void ChartView_LabelFormatting(object sender, ChartViewLabelFormattingEventArgs e) {
            e.LabelElement.BackColor = System.Drawing.Color.Transparent;
            e.LabelElement.Text = e.LabelElement.Text;
            e.LabelElement.BorderWidth = 0;
            if (e.LabelElement.Text.Contains("-")) {
                e.LabelElement.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void Inicia() {
            var deptos = this.Service.GetList<Domain.Cotizador.Empresa.Entities.DepartamentoModel>(DepartamentosService.Query().OnlyActive().Build());
            this.TPeriodo.Departamento.DataSource = deptos;
            this.TPeriodo.Departamento.DisplayMember = "Departamento";
            this.TPeriodo.Departamento.ValueMember = "IdDepartamento";

            this.comboBox1.DataSource = deptos;
            this.comboBox1.DisplayMember = "Departamento";
            this.comboBox1.ValueMember = "IdDepartamento";

            this.Empleados = this.Service.GetList<EmpleadoModel>(new List<IConditional> { new Conditional("CTEMP_A", "1") }).OrderBy(it => it.Clave).ToList();

            foreach (var item in this.Empleados) {
                this.Reporte.Empleados.Add(new ProduccionReporte.ProduccionEmpleadoRecord { Label = item.Clave, Nombre = item.NombreCompleto });
            }
            
        }

        private void Consultar() {
            var seleccioando = this.TPeriodo.Departamento.SelectedItem.DataBoundItem as Domain.Cotizador.Empresa.Entities.DepartamentoModel;
            if (seleccioando != null) {
                var condiciones = new List<IConditional> {
                new Conditional("PEDPROE_FEC_FIN", this.TPeriodo.FInicial.Value.ToShortDateString(), ConditionalTypeEnum.GreaterThanOrEqual),
                new Conditional("PEDPROE_FEC_FIN", this.TPeriodo.FFinal.Value.ToShortDateString(), ConditionalTypeEnum.LessThanOrEqual)
            };
                this.DataSource = this.Service.GetList<ComponenteLineaProduccionRegistro>(condiciones).ToList();
            }
        }
    }

    public static class Prueeba {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source) {
                if (seenKeys.Add(keySelector(element))) {
                    yield return element;
                }
            }
        }
    }
}
