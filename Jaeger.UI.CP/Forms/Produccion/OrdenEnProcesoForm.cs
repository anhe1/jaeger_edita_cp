﻿using Telerik.WinControls;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// Ordenes de Produccion en status conocidos en proceso
    /// </summary>
    public class OrdenEnProcesoForm : OrdenProduccionCatalogoForm {
        public OrdenEnProcesoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "CP: Orden en Proceso";
            this.TOrden.ShowPeriodo = false;
            this.TOrden.Cancelar.Visibility = ElementVisibility.Collapsed;
            this.TOrden.Status = new int[] { 6, 7, 8, 21, 22, 23, 24, 25 };
            this.Load += OrdenProduccionHistorialForm_Load;
        }

        protected virtual void OrdenProduccionHistorialForm_Load(object sender, System.EventArgs e) {
            this.TOrden.Service = new OrdenProduccionService();
            this.RemisionService = new Aplication.CP.Service.RemisionadoService();
            this.RemisionInternaService = new Aplication.CP.Service.RemisionadoInternoService();
        }
    }
}
