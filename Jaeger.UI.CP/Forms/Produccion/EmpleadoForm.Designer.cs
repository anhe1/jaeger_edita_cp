﻿namespace Jaeger.UI.CP.Forms.Produccion {
    partial class EmpleadoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.ButtonRFC = new Telerik.WinControls.UI.RadButton();
            this.FechaNacimientoLabel = new Telerik.WinControls.UI.RadLabel();
            this.GeneroLabel = new Telerik.WinControls.UI.RadLabel();
            this.Genero = new Telerik.WinControls.UI.RadDropDownList();
            this.FechaNacimiento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.SegundoApellidoLabel = new Telerik.WinControls.UI.RadLabel();
            this.PrimerApellidoLabel = new Telerik.WinControls.UI.RadLabel();
            this.NombresLabel = new Telerik.WinControls.UI.RadLabel();
            this.RFCLabel = new Telerik.WinControls.UI.RadLabel();
            this.CURPLabel = new Telerik.WinControls.UI.RadLabel();
            this.ClaveLabel = new Telerik.WinControls.UI.RadLabel();
            this.NumeroLabel = new Telerik.WinControls.UI.RadLabel();
            this.TxbSegundoApellido = new Telerik.WinControls.UI.RadTextBox();
            this.PrimerApellido = new Telerik.WinControls.UI.RadTextBox();
            this.Nombre = new Telerik.WinControls.UI.RadTextBox();
            this.TxbRFC = new Telerik.WinControls.UI.RadTextBox();
            this.TxbCURP = new Telerik.WinControls.UI.RadTextBox();
            this.TxbClave = new Telerik.WinControls.UI.RadTextBox();
            this.TxbNum = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.label25 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.switchActivo = new Telerik.WinControls.UI.RadToggleSwitch();
            this.CiudadNacimientoLabel = new Telerik.WinControls.UI.RadLabel();
            this.CiudadNacimiento = new Telerik.WinControls.UI.RadTextBox();
            this.EntidadFedNacimientoLabel = new Telerik.WinControls.UI.RadLabel();
            this.EntidadFedNacimiento = new Telerik.WinControls.UI.RadDropDownList();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.Cancelar = new Telerik.WinControls.UI.RadButton();
            this.Guardar = new Telerik.WinControls.UI.RadButton();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaNacimientoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeneroLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Genero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SegundoApellidoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimerApellidoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombresLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURPLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSegundoApellido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimerApellido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox)).BeginInit();
            this.groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CiudadNacimientoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CiudadNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntidadFedNacimientoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntidadFedNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonRFC
            // 
            this.ButtonRFC.Location = new System.Drawing.Point(141, 153);
            this.ButtonRFC.Name = "ButtonRFC";
            this.ButtonRFC.Size = new System.Drawing.Size(38, 23);
            this.ButtonRFC.TabIndex = 101;
            this.ButtonRFC.Text = "RFC";
            // 
            // FechaNacimientoLabel
            // 
            this.FechaNacimientoLabel.Location = new System.Drawing.Point(139, 94);
            this.FechaNacimientoLabel.Name = "FechaNacimientoLabel";
            this.FechaNacimientoLabel.Size = new System.Drawing.Size(114, 18);
            this.FechaNacimientoLabel.TabIndex = 76;
            this.FechaNacimientoLabel.Text = "Fecha de Nacimiento:";
            // 
            // GeneroLabel
            // 
            this.GeneroLabel.Location = new System.Drawing.Point(9, 94);
            this.GeneroLabel.Name = "GeneroLabel";
            this.GeneroLabel.Size = new System.Drawing.Size(45, 18);
            this.GeneroLabel.TabIndex = 75;
            this.GeneroLabel.Text = "Genero:";
            // 
            // Genero
            // 
            this.Genero.Location = new System.Drawing.Point(12, 112);
            this.Genero.Name = "Genero";
            this.Genero.Size = new System.Drawing.Size(125, 20);
            this.Genero.TabIndex = 74;
            // 
            // FechaNacimiento
            // 
            this.FechaNacimiento.Location = new System.Drawing.Point(142, 112);
            this.FechaNacimiento.Name = "FechaNacimiento";
            this.FechaNacimiento.Size = new System.Drawing.Size(175, 20);
            this.FechaNacimiento.TabIndex = 72;
            this.FechaNacimiento.TabStop = false;
            this.FechaNacimiento.Text = "lunes, 21 de enero de 2019";
            this.FechaNacimiento.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 174);
            // 
            // SegundoApellidoLabel
            // 
            this.SegundoApellidoLabel.Location = new System.Drawing.Point(335, 51);
            this.SegundoApellidoLabel.Name = "SegundoApellidoLabel";
            this.SegundoApellidoLabel.Size = new System.Drawing.Size(96, 18);
            this.SegundoApellidoLabel.TabIndex = 68;
            this.SegundoApellidoLabel.Text = "Apellido Materno:";
            // 
            // PrimerApellidoLabel
            // 
            this.PrimerApellidoLabel.Location = new System.Drawing.Point(172, 51);
            this.PrimerApellidoLabel.Name = "PrimerApellidoLabel";
            this.PrimerApellidoLabel.Size = new System.Drawing.Size(92, 18);
            this.PrimerApellidoLabel.TabIndex = 67;
            this.PrimerApellidoLabel.Text = "Apellido Paterno:";
            // 
            // NombresLabel
            // 
            this.NombresLabel.Location = new System.Drawing.Point(6, 51);
            this.NombresLabel.Name = "NombresLabel";
            this.NombresLabel.Size = new System.Drawing.Size(62, 18);
            this.NombresLabel.TabIndex = 66;
            this.NombresLabel.Text = "Nombre(s):";
            // 
            // RFCLabel
            // 
            this.RFCLabel.Location = new System.Drawing.Point(9, 136);
            this.RFCLabel.Name = "RFCLabel";
            this.RFCLabel.Size = new System.Drawing.Size(50, 18);
            this.RFCLabel.TabIndex = 65;
            this.RFCLabel.Text = "RFC (F7):";
            // 
            // CURPLabel
            // 
            this.CURPLabel.Location = new System.Drawing.Point(182, 136);
            this.CURPLabel.Name = "CURPLabel";
            this.CURPLabel.Size = new System.Drawing.Size(58, 18);
            this.CURPLabel.TabIndex = 64;
            this.CURPLabel.Text = "CURP (F7):";
            // 
            // ClaveLabel
            // 
            this.ClaveLabel.Location = new System.Drawing.Point(106, 20);
            this.ClaveLabel.Name = "ClaveLabel";
            this.ClaveLabel.Size = new System.Drawing.Size(35, 18);
            this.ClaveLabel.TabIndex = 63;
            this.ClaveLabel.Text = "Clave:";
            // 
            // NumeroLabel
            // 
            this.NumeroLabel.Location = new System.Drawing.Point(6, 20);
            this.NumeroLabel.Name = "NumeroLabel";
            this.NumeroLabel.Size = new System.Drawing.Size(36, 18);
            this.NumeroLabel.TabIndex = 62;
            this.NumeroLabel.Text = "Núm.:";
            // 
            // TxbSegundoApellido
            // 
            this.TxbSegundoApellido.Location = new System.Drawing.Point(335, 68);
            this.TxbSegundoApellido.Name = "TxbSegundoApellido";
            this.TxbSegundoApellido.Size = new System.Drawing.Size(162, 20);
            this.TxbSegundoApellido.TabIndex = 58;
            // 
            // PrimerApellido
            // 
            this.PrimerApellido.Location = new System.Drawing.Point(172, 68);
            this.PrimerApellido.Name = "PrimerApellido";
            this.PrimerApellido.Size = new System.Drawing.Size(157, 20);
            this.PrimerApellido.TabIndex = 57;
            // 
            // Nombre
            // 
            this.Nombre.Location = new System.Drawing.Point(9, 68);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(157, 20);
            this.Nombre.TabIndex = 56;
            // 
            // TxbRFC
            // 
            this.TxbRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbRFC.Location = new System.Drawing.Point(12, 154);
            this.TxbRFC.MaxLength = 16;
            this.TxbRFC.Name = "TxbRFC";
            this.TxbRFC.Size = new System.Drawing.Size(125, 20);
            this.TxbRFC.TabIndex = 55;
            // 
            // TxbCURP
            // 
            this.TxbCURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbCURP.Location = new System.Drawing.Point(185, 154);
            this.TxbCURP.MaxLength = 18;
            this.TxbCURP.Name = "TxbCURP";
            this.TxbCURP.Size = new System.Drawing.Size(132, 20);
            this.TxbCURP.TabIndex = 54;
            // 
            // TxbClave
            // 
            this.TxbClave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TxbClave.Location = new System.Drawing.Point(149, 19);
            this.TxbClave.Name = "TxbClave";
            this.TxbClave.Size = new System.Drawing.Size(64, 20);
            this.TxbClave.TabIndex = 53;
            // 
            // TxbNum
            // 
            this.TxbNum.Location = new System.Drawing.Point(47, 19);
            this.TxbNum.Mask = "d6";
            this.TxbNum.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbNum.Name = "TxbNum";
            this.TxbNum.NullText = "000000";
            this.TxbNum.ReadOnly = true;
            this.TxbNum.Size = new System.Drawing.Size(53, 20);
            this.TxbNum.TabIndex = 52;
            this.TxbNum.TabStop = false;
            this.TxbNum.Text = "000000";
            this.TxbNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(46, 12);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(195, 18);
            this.label25.TabIndex = 104;
            this.label25.Text = "Información del Empleado Registrado";
            // 
            // groupBox
            // 
            this.groupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBox.Controls.Add(this.pictureBox1);
            this.groupBox.Controls.Add(this.switchActivo);
            this.groupBox.Controls.Add(this.NumeroLabel);
            this.groupBox.Controls.Add(this.TxbNum);
            this.groupBox.Controls.Add(this.TxbClave);
            this.groupBox.Controls.Add(this.Nombre);
            this.groupBox.Controls.Add(this.CiudadNacimientoLabel);
            this.groupBox.Controls.Add(this.PrimerApellido);
            this.groupBox.Controls.Add(this.CiudadNacimiento);
            this.groupBox.Controls.Add(this.TxbSegundoApellido);
            this.groupBox.Controls.Add(this.EntidadFedNacimientoLabel);
            this.groupBox.Controls.Add(this.ClaveLabel);
            this.groupBox.Controls.Add(this.EntidadFedNacimiento);
            this.groupBox.Controls.Add(this.NombresLabel);
            this.groupBox.Controls.Add(this.PrimerApellidoLabel);
            this.groupBox.Controls.Add(this.TxbCURP);
            this.groupBox.Controls.Add(this.SegundoApellidoLabel);
            this.groupBox.Controls.Add(this.GeneroLabel);
            this.groupBox.Controls.Add(this.FechaNacimientoLabel);
            this.groupBox.Controls.Add(this.RFCLabel);
            this.groupBox.Controls.Add(this.FechaNacimiento);
            this.groupBox.Controls.Add(this.ButtonRFC);
            this.groupBox.Controls.Add(this.CURPLabel);
            this.groupBox.Controls.Add(this.Genero);
            this.groupBox.Controls.Add(this.TxbRFC);
            this.groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox.HeaderText = "";
            this.groupBox.Location = new System.Drawing.Point(0, 46);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(674, 184);
            this.groupBox.TabIndex = 106;
            this.groupBox.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(524, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(122, 118);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 129;
            this.pictureBox1.TabStop = false;
            // 
            // switchActivo
            // 
            this.switchActivo.Location = new System.Drawing.Point(447, 19);
            this.switchActivo.Name = "switchActivo";
            this.switchActivo.Size = new System.Drawing.Size(50, 20);
            this.switchActivo.TabIndex = 110;
            // 
            // CiudadNacimientoLabel
            // 
            this.CiudadNacimientoLabel.Location = new System.Drawing.Point(323, 136);
            this.CiudadNacimientoLabel.Name = "CiudadNacimientoLabel";
            this.CiudadNacimientoLabel.Size = new System.Drawing.Size(118, 18);
            this.CiudadNacimientoLabel.TabIndex = 128;
            this.CiudadNacimientoLabel.Text = "Ciudad de Nacimiento";
            // 
            // CiudadNacimiento
            // 
            this.CiudadNacimiento.Location = new System.Drawing.Point(323, 154);
            this.CiudadNacimiento.Name = "CiudadNacimiento";
            this.CiudadNacimiento.Size = new System.Drawing.Size(174, 20);
            this.CiudadNacimiento.TabIndex = 127;
            // 
            // EntidadFedNacimientoLabel
            // 
            this.EntidadFedNacimientoLabel.Location = new System.Drawing.Point(323, 94);
            this.EntidadFedNacimientoLabel.Name = "EntidadFedNacimientoLabel";
            this.EntidadFedNacimientoLabel.Size = new System.Drawing.Size(144, 18);
            this.EntidadFedNacimientoLabel.TabIndex = 126;
            this.EntidadFedNacimientoLabel.Text = "Entidad Fed. de Nacimiento";
            // 
            // EntidadFedNacimiento
            // 
            this.EntidadFedNacimiento.Location = new System.Drawing.Point(323, 112);
            this.EntidadFedNacimiento.Name = "EntidadFedNacimiento";
            this.EntidadFedNacimiento.Size = new System.Drawing.Size(176, 20);
            this.EntidadFedNacimiento.TabIndex = 125;
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(674, 46);
            this.Encabezado.TabIndex = 102;
            this.Encabezado.TabStop = false;
            // 
            // Cancelar
            // 
            this.Cancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancelar.Location = new System.Drawing.Point(552, 233);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(110, 24);
            this.Cancelar.TabIndex = 126;
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.Click += new System.EventHandler(this.TEmpleado_Cancelar_Click);
            // 
            // Guardar
            // 
            this.Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Guardar.Location = new System.Drawing.Point(436, 233);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(110, 24);
            this.Guardar.TabIndex = 126;
            this.Guardar.Text = "Guardar";
            this.Guardar.Click += new System.EventHandler(this.TEmpleado_Guardar_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // EmpleadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancelar;
            this.ClientSize = new System.Drawing.Size(674, 269);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.Cancelar);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.Encabezado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmpleadoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Empleado";
            this.Load += new System.EventHandler(this.EmpleadoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaNacimientoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GeneroLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Genero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SegundoApellidoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimerApellidoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombresLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURPLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbSegundoApellido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimerApellido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox)).EndInit();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CiudadNacimientoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CiudadNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntidadFedNacimientoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EntidadFedNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton ButtonRFC;
        private Telerik.WinControls.UI.RadLabel FechaNacimientoLabel;
        private Telerik.WinControls.UI.RadLabel GeneroLabel;
        private Telerik.WinControls.UI.RadDropDownList Genero;
        private Telerik.WinControls.UI.RadDateTimePicker FechaNacimiento;
        private Telerik.WinControls.UI.RadLabel SegundoApellidoLabel;
        private Telerik.WinControls.UI.RadLabel PrimerApellidoLabel;
        private Telerik.WinControls.UI.RadLabel NombresLabel;
        private Telerik.WinControls.UI.RadLabel RFCLabel;
        private Telerik.WinControls.UI.RadLabel CURPLabel;
        private Telerik.WinControls.UI.RadLabel ClaveLabel;
        private Telerik.WinControls.UI.RadLabel NumeroLabel;
        private Telerik.WinControls.UI.RadTextBox TxbSegundoApellido;
        private Telerik.WinControls.UI.RadTextBox PrimerApellido;
        private Telerik.WinControls.UI.RadTextBox Nombre;
        private Telerik.WinControls.UI.RadTextBox TxbRFC;
        private Telerik.WinControls.UI.RadTextBox TxbCURP;
        private Telerik.WinControls.UI.RadTextBox TxbClave;
        private Telerik.WinControls.UI.RadMaskedEditBox TxbNum;
        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadLabel label25;
        private Telerik.WinControls.UI.RadGroupBox groupBox;
        private Telerik.WinControls.UI.RadLabel EntidadFedNacimientoLabel;
        private Telerik.WinControls.UI.RadDropDownList EntidadFedNacimiento;
        private Telerik.WinControls.UI.RadLabel CiudadNacimientoLabel;
        private Telerik.WinControls.UI.RadTextBox CiudadNacimiento;
        private Telerik.WinControls.UI.RadToggleSwitch switchActivo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadButton Cancelar;
        private Telerik.WinControls.UI.RadButton Guardar;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}