﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.CP.Builder;

namespace Jaeger.UI.CP.Forms.Produccion {
    internal class EmpleadoGridControl : Common.Forms.GridStandarControl {
        public GridViewTemplate gridContrato = new GridViewTemplate();
        public GridViewTemplate gridBanco = new GridViewTemplate();

        public EmpleadoGridControl() : base() {

        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            using (IEmpleadoGridViewBuilder build = new EmpleadoGridViewBuilder()) {
                this.GridData.Columns.AddRange(build.Templetes().Master().Build());
            }
        }
    }
}
