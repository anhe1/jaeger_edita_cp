﻿using Jaeger.Aplication.Cotizador.Services;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// Formulario para registro de avance de produccion
    /// </summary>
    public class DepartamentoProduccionForm : UI.Cotizador.Forms.Produccion.DepartamentoProduccionForm {
        public DepartamentoProduccionForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.TDepartamento.Service = new ProduccionService();
        }
    }
}
