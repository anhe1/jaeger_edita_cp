﻿using System;

namespace Jaeger.UI.CP.Forms.Produccion {
    /// <summary>
    /// Certificado de Calidad
    /// </summary>
    public partial class OrdenProduccionCertificadoForm : UI.CCalidad.Forms.CertificadoForm {
        public OrdenProduccionCertificadoForm(Domain.CCalidad.Entities.OrdenProduccionModel ordenProduccion) : base(ordenProduccion) {
            this.Load += OrdenProduccionCertificadoForm_Load;
        }

        private void OrdenProduccionCertificadoForm_Load(object sender, EventArgs e) {

        }
    }
}
