﻿namespace Jaeger.UI.CP.Forms.Produccion {
    partial class DepartamentoEvaluacion2Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries1 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries2 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea2 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis2 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis2 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries3 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries4 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea3 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis3 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis3 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries5 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries6 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries7 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea4 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis4 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis4 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries8 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries9 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.ChartTiempo = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.ChartCentro = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel6 = new Telerik.WinControls.UI.SplitPanel();
            this.ChartSalarios = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.ChartOficial = new Telerik.WinControls.UI.RadChartView();
            this.toolBarStandarControl1 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TPeriodo = new Jaeger.UI.Common.Forms.TbPeriodoControl();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartTiempo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            this.splitPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartCentro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).BeginInit();
            this.splitPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSalarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartOficial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1441, 692);
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radSplitContainer2);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1441, 348);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.005464471F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 5);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Controls.Add(this.splitPanel5);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1441, 348);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.ChartTiempo);
            this.splitPanel4.Location = new System.Drawing.Point(0, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(718, 348);
            this.splitPanel4.TabIndex = 0;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // ChartTiempo
            // 
            this.ChartTiempo.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            categoricalAxis1.LabelFitMode = Telerik.Charting.AxisLabelFitMode.MultiLine;
            categoricalAxis1.LabelRotationAngle = 300D;
            categoricalAxis1.Title = "";
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.LabelFitMode = Telerik.Charting.AxisLabelFitMode.MultiLine;
            linearAxis1.LabelFormat = "{0:n0}";
            linearAxis1.LabelRotationAngle = 300D;
            linearAxis1.TickOrigin = null;
            linearAxis1.Title = "Horas";
            this.ChartTiempo.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.ChartTiempo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartTiempo.LegendTitle = "Tie. Hrs.";
            this.ChartTiempo.Location = new System.Drawing.Point(0, 0);
            this.ChartTiempo.Name = "ChartTiempo";
            barSeries1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(202)))), ((int)(((byte)(238)))));
            barSeries1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(202)))), ((int)(((byte)(238)))));
            barSeries1.HorizontalAxis = categoricalAxis1;
            barSeries1.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries1.LegendTitle = "Tie. Est.";
            barSeries1.ShowLabels = true;
            barSeries1.VerticalAxis = linearAxis1;
            barSeries2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(169)))), ((int)(((byte)(215)))));
            barSeries2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(169)))), ((int)(((byte)(215)))));
            barSeries2.HorizontalAxis = categoricalAxis1;
            barSeries2.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries2.LegendTitle = "Tie. Real";
            barSeries2.ShowLabels = true;
            barSeries2.VerticalAxis = linearAxis1;
            this.ChartTiempo.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries1,
            barSeries2});
            this.ChartTiempo.ShowGrid = false;
            this.ChartTiempo.ShowLegend = true;
            this.ChartTiempo.ShowTitle = true;
            this.ChartTiempo.ShowToolTip = true;
            this.ChartTiempo.Size = new System.Drawing.Size(718, 348);
            this.ChartTiempo.TabIndex = 0;
            this.ChartTiempo.Title = "Producción en Tiempo Estimado vs Real";
            this.ChartTiempo.LabelFormatting += new Telerik.WinControls.UI.ChartViewLabelFormattingEventHandler(this.ChartView_LabelFormatting);
            ((Telerik.WinControls.UI.RadChartElement)(this.ChartTiempo.GetChildAt(0))).CustomFontSize = 12F;
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartTiempo.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción en Tiempo Estimado vs Real";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartTiempo.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            ((Telerik.WinControls.UI.ChartLegendElement)(this.ChartTiempo.GetChildAt(0).GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Segoe UI", 9F);
            ((Telerik.WinControls.UI.ChartLegendElement)(this.ChartTiempo.GetChildAt(0).GetChildAt(0).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitPanel5
            // 
            this.splitPanel5.Controls.Add(this.ChartCentro);
            this.splitPanel5.Location = new System.Drawing.Point(722, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel5.Size = new System.Drawing.Size(719, 348);
            this.splitPanel5.TabIndex = 1;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // ChartCentro
            // 
            this.ChartCentro.AreaDesign = cartesianArea2;
            categoricalAxis2.IsPrimary = true;
            categoricalAxis2.LabelRotationAngle = 300D;
            categoricalAxis2.Title = "";
            linearAxis2.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis2.IsPrimary = true;
            linearAxis2.LabelFormat = "{0:n0}";
            linearAxis2.LabelRotationAngle = 300D;
            linearAxis2.TickOrigin = null;
            linearAxis2.Title = "Cantidad Producida";
            this.ChartCentro.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis2,
            linearAxis2});
            this.ChartCentro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartCentro.LegendTitle = "Cantidad";
            this.ChartCentro.Location = new System.Drawing.Point(0, 0);
            this.ChartCentro.Name = "ChartCentro";
            barSeries3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            barSeries3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            barSeries3.HorizontalAxis = categoricalAxis2;
            barSeries3.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries3.LegendTitle = "Cant. Est.";
            barSeries3.ShowLabels = true;
            barSeries3.VerticalAxis = linearAxis2;
            barSeries4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(135)))), ((int)(((byte)(90)))));
            barSeries4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(135)))), ((int)(((byte)(90)))));
            barSeries4.HorizontalAxis = categoricalAxis2;
            barSeries4.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries4.LegendTitle = "Cant. Real";
            barSeries4.VerticalAxis = linearAxis2;
            this.ChartCentro.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries3,
            barSeries4});
            this.ChartCentro.ShowGrid = false;
            this.ChartCentro.ShowLegend = true;
            this.ChartCentro.ShowTitle = true;
            this.ChartCentro.ShowToolTip = true;
            this.ChartCentro.Size = new System.Drawing.Size(719, 348);
            this.ChartCentro.TabIndex = 1;
            this.ChartCentro.Title = "Producción por Centro (Cant. Est. vs Real)";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartCentro.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción por Centro (Cant. Est. vs Real)";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartCentro.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer3);
            this.splitPanel2.Location = new System.Drawing.Point(0, 352);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1441, 340);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.005464491F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -5);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel6);
            this.radSplitContainer3.Controls.Add(this.splitPanel3);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer3.Size = new System.Drawing.Size(1441, 340);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            // 
            // splitPanel6
            // 
            this.splitPanel6.Controls.Add(this.ChartSalarios);
            this.splitPanel6.Location = new System.Drawing.Point(0, 0);
            this.splitPanel6.Name = "splitPanel6";
            // 
            // 
            // 
            this.splitPanel6.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel6.Size = new System.Drawing.Size(718, 340);
            this.splitPanel6.TabIndex = 0;
            this.splitPanel6.TabStop = false;
            this.splitPanel6.Text = "splitPanel6";
            // 
            // ChartSalarios
            // 
            this.ChartSalarios.AreaDesign = cartesianArea3;
            categoricalAxis3.IsPrimary = true;
            categoricalAxis3.LabelRotationAngle = 300D;
            categoricalAxis3.Title = "";
            linearAxis3.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis3.IsPrimary = true;
            linearAxis3.LabelFormat = "{0:n0}";
            linearAxis3.LabelRotationAngle = 300D;
            linearAxis3.TickOrigin = null;
            linearAxis3.Title = "Horas";
            this.ChartSalarios.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis3,
            linearAxis3});
            this.ChartSalarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartSalarios.LegendTitle = "Tie. Hrs.";
            this.ChartSalarios.Location = new System.Drawing.Point(0, 0);
            this.ChartSalarios.Name = "ChartSalarios";
            barSeries5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(202)))), ((int)(((byte)(238)))));
            barSeries5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(202)))), ((int)(((byte)(238)))));
            barSeries5.HorizontalAxis = categoricalAxis3;
            barSeries5.LabelFormat = "{0:n0}";
            barSeries5.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries5.LegendTitle = "Tie. Est.";
            barSeries5.ShowLabels = true;
            barSeries5.VerticalAxis = linearAxis3;
            barSeries6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(169)))), ((int)(((byte)(215)))));
            barSeries6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(169)))), ((int)(((byte)(215)))));
            barSeries6.HorizontalAxis = categoricalAxis3;
            barSeries6.LabelFormat = "{0:n0}";
            barSeries6.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries6.LegendTitle = "Tie. Real";
            barSeries6.ShowLabels = true;
            barSeries6.VerticalAxis = linearAxis3;
            barSeries7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(123)))), ((int)(((byte)(169)))));
            barSeries7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(123)))), ((int)(((byte)(169)))));
            barSeries7.HorizontalAxis = categoricalAxis3;
            barSeries7.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries7.LegendTitle = "Hrs. Reloj";
            barSeries7.ShowLabels = true;
            barSeries7.VerticalAxis = linearAxis3;
            this.ChartSalarios.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries5,
            barSeries6,
            barSeries7});
            this.ChartSalarios.ShowGrid = false;
            this.ChartSalarios.ShowLegend = true;
            this.ChartSalarios.ShowTitle = true;
            this.ChartSalarios.ShowToolTip = true;
            this.ChartSalarios.ShowTrackBall = true;
            this.ChartSalarios.Size = new System.Drawing.Size(718, 340);
            this.ChartSalarios.TabIndex = 1;
            this.ChartSalarios.Title = "Producción por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartSalarios.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartSalarios.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.ChartOficial);
            this.splitPanel3.Location = new System.Drawing.Point(722, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(719, 340);
            this.splitPanel3.TabIndex = 2;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // ChartOficial
            // 
            this.ChartOficial.AreaDesign = cartesianArea4;
            categoricalAxis4.IsPrimary = true;
            categoricalAxis4.LabelRotationAngle = 300D;
            categoricalAxis4.Title = "";
            linearAxis4.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis4.IsPrimary = true;
            linearAxis4.LabelFormat = "{0:n0}";
            linearAxis4.LabelRotationAngle = 300D;
            linearAxis4.TickOrigin = null;
            linearAxis4.Title = "Cant. Producida";
            this.ChartOficial.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis4,
            linearAxis4});
            this.ChartOficial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartOficial.LegendTitle = "Cantidad.";
            this.ChartOficial.Location = new System.Drawing.Point(0, 0);
            this.ChartOficial.Name = "ChartOficial";
            barSeries8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            barSeries8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            barSeries8.HorizontalAxis = categoricalAxis4;
            barSeries8.LabelFormat = "{0:n0}";
            barSeries8.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries8.LegendTitle = "Est.";
            barSeries8.VerticalAxis = linearAxis4;
            barSeries9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(135)))), ((int)(((byte)(90)))));
            barSeries9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(135)))), ((int)(((byte)(90)))));
            barSeries9.HorizontalAxis = categoricalAxis4;
            barSeries9.LabelFormat = "{0:n0}";
            barSeries9.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries9.LegendTitle = "Real";
            barSeries9.VerticalAxis = linearAxis4;
            this.ChartOficial.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries8,
            barSeries9});
            this.ChartOficial.ShowGrid = false;
            this.ChartOficial.ShowLegend = true;
            this.ChartOficial.ShowTitle = true;
            this.ChartOficial.Size = new System.Drawing.Size(719, 340);
            this.ChartOficial.TabIndex = 1;
            this.ChartOficial.Title = "Producción por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartOficial.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartOficial.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // toolBarStandarControl1
            // 
            this.toolBarStandarControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl1.Etiqueta = "";
            this.toolBarStandarControl1.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl1.Name = "toolBarStandarControl1";
            this.toolBarStandarControl1.ReadOnly = false;
            this.toolBarStandarControl1.ShowActualizar = true;
            this.toolBarStandarControl1.ShowAutorizar = false;
            this.toolBarStandarControl1.ShowCerrar = true;
            this.toolBarStandarControl1.ShowEditar = true;
            this.toolBarStandarControl1.ShowExportarExcel = false;
            this.toolBarStandarControl1.ShowFiltro = false;
            this.toolBarStandarControl1.ShowGuardar = false;
            this.toolBarStandarControl1.ShowHerramientas = false;
            this.toolBarStandarControl1.ShowImagen = false;
            this.toolBarStandarControl1.ShowImprimir = true;
            this.toolBarStandarControl1.ShowNuevo = true;
            this.toolBarStandarControl1.ShowRemover = true;
            this.toolBarStandarControl1.Size = new System.Drawing.Size(1441, 30);
            this.toolBarStandarControl1.TabIndex = 0;
            // 
            // TPeriodo
            // 
            this.TPeriodo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPeriodo.FFinalCaption = " al:";
            this.TPeriodo.FInicialCaption = "Período del:";
            this.TPeriodo.Location = new System.Drawing.Point(0, 0);
            this.TPeriodo.Name = "TPeriodo";
            this.TPeriodo.ShowActualizar = true;
            this.TPeriodo.ShowCerrar = true;
            this.TPeriodo.ShowDepartamento = true;
            this.TPeriodo.ShowFFinal = true;
            this.TPeriodo.ShowFInicial = true;
            this.TPeriodo.ShowImprimir = true;
            this.TPeriodo.Size = new System.Drawing.Size(1462, 30);
            this.TPeriodo.TabIndex = 1;
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.DefaultPage = this.radPageViewPage1;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 30);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(1462, 740);
            this.radPageView1.TabIndex = 1;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.radSplitContainer1);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(69F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(1441, 692);
            this.radPageViewPage1.Text = "Evaluación";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.gridData);
            this.radPageViewPage2.Controls.Add(this.toolBarStandarControl1);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(45F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(1441, 692);
            this.radPageViewPage2.Text = "Datos";
            // 
            // gridData
            // 
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridData.MasterTemplate.AllowAddNewRow = false;
            this.gridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridData.Name = "gridData";
            this.gridData.ShowGroupPanel = false;
            this.gridData.Size = new System.Drawing.Size(1441, 662);
            this.gridData.TabIndex = 3;
            // 
            // DepartamentoEvaluacionPForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1462, 770);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.TPeriodo);
            this.Name = "DepartamentoEvaluacionPForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Producción: Evaluación por Oficial";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DepartamentoEvaluacion2Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartTiempo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            this.splitPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartCentro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).EndInit();
            this.splitPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartSalarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartOficial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.radPageViewPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadChartView ChartTiempo;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.RadChartView ChartCentro;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel6;
        private Telerik.WinControls.UI.RadChartView ChartSalarios;
        private Common.Forms.TbPeriodoControl TPeriodo;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        
        
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadGridView gridData;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl1;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadChartView ChartOficial;
    }
}
