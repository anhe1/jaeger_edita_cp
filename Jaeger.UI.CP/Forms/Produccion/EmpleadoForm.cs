﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.CP.Forms.Produccion {
    public partial class EmpleadoForm : RadForm {
        #region declaraciones
        protected EmpleadosService Service;
        protected EmpleadoModel Empleado;
        protected bool IsNuevo = false;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="service">EmpleadoService</param>
        /// <param name="model">EmpleadoModel</param>
        public EmpleadoForm(EmpleadosService service, EmpleadoModel model = null) {
            InitializeComponent();
            this.Service = service;
            this.Empleado = model;
        }

        private void EmpleadoForm_Load(object sender, EventArgs e) {
            if (this.Empleado == null) {
                this.Empleado = new EmpleadoModel();
                this.IsNuevo = true;
            }
            this.CreateBinding();
        }

        #region botones
        private void TEmpleado_Guardar_Click(object sender, EventArgs eventArgs) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, "Existen errores de captura!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            using (var espera = new Waiting1Form(this.Almacenar)) {
                espera.ShowDialog(this);
            }
            if (!string.IsNullOrEmpty((string)this.Tag)) {
                RadMessageBox.Show(this, (string)this.Tag, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
            this.Close();
        }
        
        private void TEmpleado_Cancelar_Click(object sender, EventArgs eventArgs) {
            this.Close();
        }
        #endregion

        #region metodos
        private void CreateBinding() {
            this.TxbNum.DataBindings.Clear();
            this.TxbNum.DataBindings.Add("Text", this.Empleado, "IdEmpleado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbClave.DataBindings.Clear();
            this.TxbClave.DataBindings.Add("Text", this.Empleado, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbRFC.DataBindings.Clear();
            this.TxbRFC.DataBindings.Add("Text", this.Empleado, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbCURP.DataBindings.Clear();
            this.TxbCURP.DataBindings.Add("Text", this.Empleado, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Nombre.DataBindings.Clear();
            this.Nombre.DataBindings.Add("Text", this.Empleado, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PrimerApellido.DataBindings.Clear();
            this.PrimerApellido.DataBindings.Add("Text", this.Empleado, "ApellidoPaterno", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbSegundoApellido.DataBindings.Clear();
            this.TxbSegundoApellido.DataBindings.Add("Text", this.Empleado, "ApellidoMaterno", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaNacimiento.DataBindings.Clear();
            this.FechaNacimiento.DataBindings.Add("Value", this.Empleado, "FecNacimiento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Genero.DataBindings.Clear();
            this.Genero.DataBindings.Add("SelectedValue", this.Empleado, "IdGenero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.EntidadFedNacimiento.DataBindings.Clear();
            this.EntidadFedNacimiento.DataBindings.Add("SelectedValue", this.Empleado, "IdEntidadFed", true, DataSourceUpdateMode.OnPropertyChanged);

            this.switchActivo.DataBindings.Clear();
            this.switchActivo.DataBindings.Add("Value", this.Empleado, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbClave.Enabled = !(this.Empleado.IdEmpleado > 0);
        }

        private bool Validar() {
            if (string.IsNullOrEmpty(this.TxbClave.Text)) {
                this.errorProvider1.SetError(this.TxbClave, "Agrega una clave válida de máximo 5 carácteres.");
                return false;
            }

            if (string.IsNullOrEmpty(this.Nombre.Text)) {
                this.errorProvider1.SetError(this.Nombre, "Es necesario un nombre");
                return false;
            }

            if (string.IsNullOrEmpty(this.PrimerApellido.Text)) {
                this.errorProvider1.SetError(this.PrimerApellido, "Agrega el apellido paterno");
                return false;
            }

            if (string.IsNullOrEmpty(this.TxbSegundoApellido.Text)) {
                this.errorProvider1.SetError(this.TxbSegundoApellido, "Agrega el apellido materno");
                return false;
            }
            return true;
        }

        private void Almacenar() {
            if (this.IsNuevo) {
                if (this.Service.Exists(this.Empleado)) {
                    this.Tag = "Esta clave ya se encuentra registrada.";
                    return;
                }
            }
            this.Empleado = this.Service.Save(this.Empleado);
            this.IsNuevo = false;
            this.Tag = "";
        }
        #endregion
    }
}
