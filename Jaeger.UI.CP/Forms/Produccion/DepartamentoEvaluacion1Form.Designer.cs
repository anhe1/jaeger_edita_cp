﻿namespace Jaeger.UI.CP.Forms.Produccion {
    partial class DepartamentoEvaluacion1Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries1 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries2 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea2 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis2 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis2 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries3 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries4 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea3 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis3 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis3 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries5 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries6 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea4 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis4 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis4 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries7 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries8 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.ChartTiempo = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.ChartCentro = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel6 = new Telerik.WinControls.UI.SplitPanel();
            this.ChartOficial = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel7 = new Telerik.WinControls.UI.SplitPanel();
            this.ChartSalarios = new Telerik.WinControls.UI.RadChartView();
            this.gridData = new Telerik.WinControls.UI.RadGridView();
            this.radCommandBar2 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.TPeriodo = new Jaeger.UI.Common.Forms.TbPeriodoControl();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartTiempo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            this.splitPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartCentro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).BeginInit();
            this.splitPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartOficial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel7)).BeginInit();
            this.splitPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSalarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1372, 520);
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radSplitContainer2);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1372, 261);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.005464471F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 5);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Controls.Add(this.splitPanel5);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1372, 261);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.ChartTiempo);
            this.splitPanel4.Location = new System.Drawing.Point(0, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(684, 261);
            this.splitPanel4.TabIndex = 0;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // ChartTiempo
            // 
            this.ChartTiempo.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            categoricalAxis1.LabelFitMode = Telerik.Charting.AxisLabelFitMode.MultiLine;
            categoricalAxis1.LabelRotationAngle = 300D;
            categoricalAxis1.Title = "Centro";
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.LabelFitMode = Telerik.Charting.AxisLabelFitMode.MultiLine;
            linearAxis1.LabelFormat = "{0:n0}";
            linearAxis1.LabelRotationAngle = 300D;
            linearAxis1.TickOrigin = null;
            linearAxis1.Title = "Hrs.";
            this.ChartTiempo.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.ChartTiempo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartTiempo.Location = new System.Drawing.Point(0, 0);
            this.ChartTiempo.Name = "ChartTiempo";
            barSeries1.HorizontalAxis = categoricalAxis1;
            barSeries1.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries1.LegendTitle = "Est.";
            barSeries1.VerticalAxis = linearAxis1;
            barSeries2.HorizontalAxis = categoricalAxis1;
            barSeries2.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries2.LegendTitle = "Real";
            barSeries2.VerticalAxis = linearAxis1;
            this.ChartTiempo.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries1,
            barSeries2});
            this.ChartTiempo.ShowGrid = false;
            this.ChartTiempo.ShowLegend = true;
            this.ChartTiempo.ShowTitle = true;
            this.ChartTiempo.ShowToolTip = true;
            this.ChartTiempo.Size = new System.Drawing.Size(684, 261);
            this.ChartTiempo.TabIndex = 0;
            this.ChartTiempo.Title = "Producción en Tiempo Estimado vs Real";
            this.ChartTiempo.LabelFormatting += new Telerik.WinControls.UI.ChartViewLabelFormattingEventHandler(this.ChartView_LabelFormatting);
            ((Telerik.WinControls.UI.RadChartElement)(this.ChartTiempo.GetChildAt(0))).CustomFontSize = 12F;
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartTiempo.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción en Tiempo Estimado vs Real";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartTiempo.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            ((Telerik.WinControls.UI.ChartLegendElement)(this.ChartTiempo.GetChildAt(0).GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Segoe UI", 9F);
            ((Telerik.WinControls.UI.ChartLegendElement)(this.ChartTiempo.GetChildAt(0).GetChildAt(0).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitPanel5
            // 
            this.splitPanel5.Controls.Add(this.ChartCentro);
            this.splitPanel5.Location = new System.Drawing.Point(688, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel5.Size = new System.Drawing.Size(684, 261);
            this.splitPanel5.TabIndex = 1;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // ChartCentro
            // 
            this.ChartCentro.AreaDesign = cartesianArea2;
            categoricalAxis2.IsPrimary = true;
            categoricalAxis2.LabelRotationAngle = 300D;
            categoricalAxis2.Title = "Centro";
            linearAxis2.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis2.IsPrimary = true;
            linearAxis2.LabelFormat = "{0:n0}";
            linearAxis2.LabelRotationAngle = 300D;
            linearAxis2.TickOrigin = null;
            linearAxis2.Title = "Cantidad Producida";
            this.ChartCentro.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis2,
            linearAxis2});
            this.ChartCentro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartCentro.LegendTitle = "Cantidad";
            this.ChartCentro.Location = new System.Drawing.Point(0, 0);
            this.ChartCentro.Name = "ChartCentro";
            barSeries3.HorizontalAxis = categoricalAxis2;
            barSeries3.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries3.LegendTitle = "Cant. Est.";
            barSeries3.ShowLabels = true;
            barSeries3.VerticalAxis = linearAxis2;
            barSeries4.HorizontalAxis = categoricalAxis2;
            barSeries4.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries4.LegendTitle = "Cant. Real";
            barSeries4.ShowLabels = true;
            barSeries4.VerticalAxis = linearAxis2;
            this.ChartCentro.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries3,
            barSeries4});
            this.ChartCentro.ShowGrid = false;
            this.ChartCentro.ShowLegend = true;
            this.ChartCentro.ShowTitle = true;
            this.ChartCentro.ShowToolTip = true;
            this.ChartCentro.Size = new System.Drawing.Size(684, 261);
            this.ChartCentro.TabIndex = 1;
            this.ChartCentro.Title = "Producción por Centro (Cant. Est. vs Real)";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartCentro.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción por Centro (Cant. Est. vs Real)";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartCentro.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer3);
            this.splitPanel2.Location = new System.Drawing.Point(0, 265);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1372, 255);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.005464491F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -5);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel6);
            this.radSplitContainer3.Controls.Add(this.splitPanel7);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer3.Size = new System.Drawing.Size(1372, 255);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            // 
            // splitPanel6
            // 
            this.splitPanel6.Controls.Add(this.ChartOficial);
            this.splitPanel6.Location = new System.Drawing.Point(0, 0);
            this.splitPanel6.Name = "splitPanel6";
            // 
            // 
            // 
            this.splitPanel6.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel6.Size = new System.Drawing.Size(684, 255);
            this.splitPanel6.TabIndex = 0;
            this.splitPanel6.TabStop = false;
            this.splitPanel6.Text = "splitPanel6";
            // 
            // ChartOficial
            // 
            this.ChartOficial.AreaDesign = cartesianArea3;
            categoricalAxis3.IsPrimary = true;
            categoricalAxis3.LabelRotationAngle = 300D;
            categoricalAxis3.Title = "Oficial";
            linearAxis3.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis3.IsPrimary = true;
            linearAxis3.LabelFormat = "{0:n0}";
            linearAxis3.LabelRotationAngle = 300D;
            linearAxis3.TickOrigin = null;
            linearAxis3.Title = "Cant. Producida";
            this.ChartOficial.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis3,
            linearAxis3});
            this.ChartOficial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartOficial.LegendTitle = "Tie. Min.";
            this.ChartOficial.Location = new System.Drawing.Point(0, 0);
            this.ChartOficial.Name = "ChartOficial";
            barSeries5.HorizontalAxis = categoricalAxis3;
            barSeries5.LabelFormat = "{0:n0}";
            barSeries5.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries5.LegendTitle = "Est.";
            barSeries5.ShowLabels = true;
            barSeries5.VerticalAxis = linearAxis3;
            barSeries6.HorizontalAxis = categoricalAxis3;
            barSeries6.LabelFormat = "{0:n0}";
            barSeries6.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries6.LegendTitle = "Real";
            barSeries6.ShowLabels = true;
            barSeries6.VerticalAxis = linearAxis3;
            this.ChartOficial.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries5,
            barSeries6});
            this.ChartOficial.ShowGrid = false;
            this.ChartOficial.ShowLegend = true;
            this.ChartOficial.ShowTitle = true;
            this.ChartOficial.Size = new System.Drawing.Size(684, 255);
            this.ChartOficial.TabIndex = 1;
            this.ChartOficial.Title = "Producción por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartOficial.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Producción por Oficial";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartOficial.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // splitPanel7
            // 
            this.splitPanel7.Controls.Add(this.ChartSalarios);
            this.splitPanel7.Location = new System.Drawing.Point(688, 0);
            this.splitPanel7.Name = "splitPanel7";
            // 
            // 
            // 
            this.splitPanel7.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel7.Size = new System.Drawing.Size(684, 255);
            this.splitPanel7.TabIndex = 1;
            this.splitPanel7.TabStop = false;
            this.splitPanel7.Text = "splitPanel7";
            // 
            // ChartSalarios
            // 
            this.ChartSalarios.AreaDesign = cartesianArea4;
            categoricalAxis4.IsPrimary = true;
            categoricalAxis4.LabelRotationAngle = 300D;
            categoricalAxis4.Title = "Oficial";
            linearAxis4.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis4.IsPrimary = true;
            linearAxis4.LabelFormat = "{0:n0}";
            linearAxis4.LabelRotationAngle = 300D;
            linearAxis4.TickOrigin = null;
            linearAxis4.Title = "Salario Diario";
            this.ChartSalarios.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis4,
            linearAxis4});
            this.ChartSalarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChartSalarios.Location = new System.Drawing.Point(0, 0);
            this.ChartSalarios.Name = "ChartSalarios";
            barSeries7.HorizontalAxis = categoricalAxis4;
            barSeries7.LabelFormat = "{0:n0}";
            barSeries7.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries7.LegendTitle = "$ Est.";
            barSeries7.VerticalAxis = linearAxis4;
            barSeries8.HorizontalAxis = categoricalAxis4;
            barSeries8.LabelFormat = "{0:n0}";
            barSeries8.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries8.LegendTitle = "$ Real";
            barSeries8.VerticalAxis = linearAxis4;
            this.ChartSalarios.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries7,
            barSeries8});
            this.ChartSalarios.ShowGrid = false;
            this.ChartSalarios.ShowLegend = true;
            this.ChartSalarios.ShowTitle = true;
            this.ChartSalarios.Size = new System.Drawing.Size(684, 255);
            this.ChartSalarios.TabIndex = 1;
            this.ChartSalarios.Title = "Salarios";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartSalarios.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Text = "Salarios";
            ((Telerik.WinControls.UI.ChartTitleElement)(this.ChartSalarios.GetChildAt(0).GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Segoe UI", 12F);
            // 
            // gridData
            // 
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridData.MasterTemplate.AllowAddNewRow = false;
            this.gridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridData.Name = "gridData";
            this.gridData.ShowGroupPanel = false;
            this.gridData.Size = new System.Drawing.Size(1370, 142);
            this.gridData.TabIndex = 2;
            // 
            // radCommandBar2
            // 
            this.radCommandBar2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar2.Name = "radCommandBar2";
            this.radCommandBar2.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2});
            this.radCommandBar2.Size = new System.Drawing.Size(1370, 30);
            this.radCommandBar2.TabIndex = 3;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Name = "commandBarRowElement2";
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2});
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.DisplayName = "commandBarStripElement2";
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarButtonFiltro});
            this.commandBarStripElement2.Name = "commandBarStripElement2";
            this.commandBarStripElement2.StretchHorizontally = true;
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.CP.Properties.Resources.filter_16px;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Up;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 550);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 749, 1462, 200);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.gridData);
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radCommandBar2);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(1370, 172);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(1372, 200);
            this.radCollapsiblePanel1.TabIndex = 2;
            // 
            // TPeriodo
            // 
            this.TPeriodo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TPeriodo.FFinalCaption = " al:";
            this.TPeriodo.FInicialCaption = "Período del:";
            this.TPeriodo.Location = new System.Drawing.Point(0, 0);
            this.TPeriodo.Name = "TPeriodo";
            this.TPeriodo.ShowActualizar = true;
            this.TPeriodo.ShowCerrar = true;
            this.TPeriodo.ShowDepartamento = true;
            this.TPeriodo.ShowFFinal = true;
            this.TPeriodo.ShowFInicial = true;
            this.TPeriodo.ShowImprimir = true;
            this.TPeriodo.Size = new System.Drawing.Size(1372, 30);
            this.TPeriodo.TabIndex = 1;
            // 
            // DepartamentoEvaluacion1Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1372, 750);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.Controls.Add(this.TPeriodo);
            this.Name = "DepartamentoEvaluacion1Form";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Producción: Evaluación por Departamento";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DepartamentoEvaluacion1Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartTiempo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            this.splitPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartCentro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).EndInit();
            this.splitPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartOficial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel7)).EndInit();
            this.splitPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChartSalarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar2)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadChartView ChartTiempo;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.RadChartView ChartCentro;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel6;
        private Telerik.WinControls.UI.RadChartView ChartOficial;
        private Telerik.WinControls.UI.SplitPanel splitPanel7;
        private Telerik.WinControls.UI.RadChartView ChartSalarios;
        private Telerik.WinControls.UI.RadGridView gridData;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar2;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Common.Forms.TbPeriodoControl TPeriodo;
    }
}
