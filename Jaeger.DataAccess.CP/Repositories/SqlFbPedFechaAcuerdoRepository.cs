﻿using System.Linq;
using System.Collections.Generic;
using System;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de cambios de fechas de acuerdo (PEDFEC)
    /// </summary>
    public class SqlFbPedFechaAcuerdoRepository : RepositoryMaster<PedFechaAcuerdoModel>, ISqlPedFechaAcuerdoRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public SqlFbPedFechaAcuerdoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE PEDFEC SET PEDFEC_A = 0 WHERE PEDFEC_ID = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(PedFechaAcuerdoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO PEDFEC (PEDFEC_ID, PEDFEC_A, PEDFEC_PEDPRD_ID, PEDFEC_FECL_ID, PEDFEC_CEN_ID, PEDFEC_DIR_ID, PEDFEC_FEC, PEDFEC_OBS, PEDFEC_USU_N, PEDFEC_USU_F, PEDFEC_USU_FM, PEDFEC_FM) 
                                           VALUES (@PEDFEC_ID,@PEDFEC_A,@PEDFEC_PEDPRD_ID,@PEDFEC_FECL_ID,@PEDFEC_CEN_ID,@PEDFEC_DIR_ID,@PEDFEC_FEC,@PEDFEC_OBS,@PEDFEC_USU_N,@PEDFEC_USU_F,@PEDFEC_USU_FM,@PEDFEC_FM)"
            };
            item.IdPedFec = this.Max("PEDFEC_ID");
            sqlCommand.Parameters.AddWithValue("@pedfec_id", item.IdPedFec);
            sqlCommand.Parameters.AddWithValue("@pedfec_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedfec_pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@pedfec_fecl_id", item.PEDFEC_FECL_ID);
            sqlCommand.Parameters.AddWithValue("@pedfec_cen_id", item.PEDFEC_CEN_ID);
            sqlCommand.Parameters.AddWithValue("@pedfec_dir_id", item.PEDFEC_DIR_ID);
            sqlCommand.Parameters.AddWithValue("@pedfec_fec", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@pedfec_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedfec_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@pedfec_usu_f", item.PEDFEC_USU_F);
            sqlCommand.Parameters.AddWithValue("@pedfec_usu_fm", item.PEDFEC_USU_FM);
            sqlCommand.Parameters.AddWithValue("@pedfec_fm", item.FechaModifica);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdPedFec;
            return 0;
        }

        public int Update(PedFechaAcuerdoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedfec 
                            SET pedfec_a = @pedfec_a, pedfec_pedprd_id = @pedfec_pedprd_id, pedfec_fecl_id = @pedfec_fecl_id, pedfec_cen_id = @pedfec_cen_id, pedfec_dir_id = @pedfec_dir_id, pedfec_fec = @pedfec_fec, pedfec_obs = @pedfec_obs, pedfec_usu_n = @pedfec_usu_n, pedfec_usu_f = @pedfec_usu_f, pedfec_usu_fm = @pedfec_usu_fm, pedfec_fm = @pedfec_fm 
                            WHERE pedfec_id = @pedfec_id;"
            };
            sqlCommand.Parameters.AddWithValue("@pedfec_id", item.IdPedFec);
            sqlCommand.Parameters.AddWithValue("@pedfec_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedfec_pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@pedfec_fecl_id", item.PEDFEC_FECL_ID);
            sqlCommand.Parameters.AddWithValue("@pedfec_cen_id", item.PEDFEC_CEN_ID);
            sqlCommand.Parameters.AddWithValue("@pedfec_dir_id", item.PEDFEC_DIR_ID);
            sqlCommand.Parameters.AddWithValue("@pedfec_fec", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@pedfec_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedfec_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@pedfec_usu_f", item.PEDFEC_USU_F);
            sqlCommand.Parameters.AddWithValue("@pedfec_usu_fm", item.PEDFEC_USU_FM);
            sqlCommand.Parameters.AddWithValue("@pedfec_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public PedFechaAcuerdoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM PEDFEC WHERE PEDFEC_ID = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.GetMapper<PedFechaAcuerdoModel>(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<PedFechaAcuerdoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM PEDFEC WHERE PEDFEC_ID = @id"
            };
            return this.GetMapper<PedFechaAcuerdoModel>(sqlCommand).ToList();
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM PEDFEC LEFT JOIN PEDFECL ON PEDFEC_FECL_ID = PEDFECL_ID WHERE PEDFEC_A > 0 @condiciones ORDER BY PEDFEC_FEC DESC"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public PedFechaAcuerdoDetailModel Save(PedFechaAcuerdoDetailModel model) {
            if (model.IdPedFec == 0) {
                model.Creo = this.User;
                model.PEDFEC_USU_F = DateTime.Now;
                model.IdPedFec = this.Insert(model);
            } else {
                model.Creo = this.User;
                model.PEDFEC_USU_F = DateTime.Now;
                this.Update(model);
            }
            return model;
        }
    }
}
