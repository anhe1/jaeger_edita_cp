﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.DataAccess.CP.Repositories {
    ///<summary>
    /// repositorio de SubProcesos - Puestos (EDP4PUE)
    ///</summary>
    public class SqlFbEdp4PueRepository : RepositoryMaster<Edp4PueModel>, ISqlEdp4PueRepository {
        public SqlFbEdp4PueRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp4pue SET edp4pue_a = 0 WHERE edp4pue_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public Edp4PueModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM edp4pue WHERE edp4pue_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Edp4PueModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<Edp4PueModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM edp4pue WHERE edp4pue_A > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Edp4PueModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener lista de puestos y funciones por indices de relacion de departamentos
        /// </summary>
        public IEnumerable<Edp4PueModel> GetList(int[] index) {
            var sqlLinea = new FbCommand {
                CommandText = @"select * from EDP4PUE where EDP4PUE_A > 0 and EDP4PUE_EDP4_ID in (@index)"
            };

            sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", index.ToArray()));
            var tabla = this.ExecuteReader(sqlLinea);
            var mapper = new DataNamesMapper<Edp4PueModel>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(Edp4PueModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO edp4pue (edp4pue_id, edp4pue_id3, edp4pue_a, edp4pue_edp4_id, edp4pue_pue_id4, edp4pue_pue_id, edp4pue_usu_fm, edp4pue_fm) 
                			VALUES (@edp4pue_id, @edp4pue_id3, @edp4pue_a, @edp4pue_edp4_id, @edp4pue_pue_id4, @edp4pue_pue_id, @edp4pue_usu_fm, @edp4pue_fm)"
            };
            item.IdEp4Pue = this.Max("EDP4PUE_ID");
            sqlCommand.Parameters.AddWithValue("@edp4pue_id", item.IdEp4Pue);
            sqlCommand.Parameters.AddWithValue("@edp4pue_id3", item.EDP4PUE_ID3);
            sqlCommand.Parameters.AddWithValue("@edp4pue_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp4pue_edp4_id", item.EDP4PUE_EDP4_ID);
            sqlCommand.Parameters.AddWithValue("@edp4pue_pue_id4", item.EDP4PUE_PUE_ID4);
            sqlCommand.Parameters.AddWithValue("@edp4pue_pue_id", item.EDP4PUE_PUE_ID);
            sqlCommand.Parameters.AddWithValue("@edp4pue_usu_fm", item.EDP4PUE_USU_FM);
            sqlCommand.Parameters.AddWithValue("@edp4pue_fm", item.EDP4PUE_FM);
            if (this.ExecuteScalar(sqlCommand) > 0)
                return item.IdEp4Pue;
            return 0;
        }

        public int Update(Edp4PueModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp4pue 
                			SET edp4pue_id3 = @edp4pue_id3, edp4pue_a = @edp4pue_a, edp4pue_edp4_id = @edp4pue_edp4_id, edp4pue_pue_id4 = @edp4pue_pue_id4, edp4pue_pue_id = @edp4pue_pue_id, edp4pue_usu_fm = @edp4pue_usu_fm, edp4pue_fm = @edp4pue_fm 
                			WHERE edp4pue_id = @edp4pue_id;"
            };
            sqlCommand.Parameters.AddWithValue("@edp4pue_id", item.IdEp4Pue);
            sqlCommand.Parameters.AddWithValue("@edp4pue_id3", item.EDP4PUE_ID3);
            sqlCommand.Parameters.AddWithValue("@edp4pue_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp4pue_edp4_id", item.EDP4PUE_EDP4_ID);
            sqlCommand.Parameters.AddWithValue("@edp4pue_pue_id4", item.EDP4PUE_PUE_ID4);
            sqlCommand.Parameters.AddWithValue("@edp4pue_pue_id", item.EDP4PUE_PUE_ID);
            sqlCommand.Parameters.AddWithValue("@edp4pue_usu_fm", item.EDP4PUE_USU_FM);
            sqlCommand.Parameters.AddWithValue("@edp4pue_fm", item.EDP4PUE_FM);
            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
