﻿/// purpose: repositorio de remisiones (REM)
/// develop: ANHE1 27092020 0121
using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Remisionado.Contracts;
using Jaeger.Domain.CP.Remisionado.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de remisiones (REM)
    /// </summary>
    public class SqlFbRemRepository : RepositoryMaster<RemisionModel>, ISqlRemisionRepository {
        protected ISqlRemisionPedRepository remisionPedRepository;

        public SqlFbRemRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.remisionPedRepository = new SqlFbRemPedRepository(configuracion);
        }

        public int Insert(RemisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO rem ( rem_id, rem_a, rem_st_id, rem_dir_id, rem_dircon_id, rem_fec, rem_ven, rem_emb, rem_$, rem_usu_n, rem_$pag, rem_vence, rem_obs) 
                    			         VALUES (@rem_id,@rem_a,@rem_st_id,@rem_dir_id,@rem_dircon_id,@rem_fec,@rem_ven,@rem_emb,@rem_$,@rem_usu_n,@rem_$pag,@rem_vence,@rem_obs)"
            };
            item.IdRemision = this.Max("REM_ID");
            sqlCommand.Parameters.AddWithValue("@rem_id", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@rem_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@rem_st_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@rem_dir_id", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@rem_dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@rem_fec", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@rem_ven", item.REM_VEN);
            sqlCommand.Parameters.AddWithValue("@rem_emb", item.Embarque);
            sqlCommand.Parameters.AddWithValue("@rem_$", item.Total);
            sqlCommand.Parameters.AddWithValue("@rem_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@rem_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@rem_$pag", item.Acumulado);
            sqlCommand.Parameters.AddWithValue("@rem_vence", item.FechaVence);
            sqlCommand.Parameters.AddWithValue("@rem_obs", item.Nota);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdRemision;
            return 0;
        }

        public int Update(RemisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE rem 
                    			SET rem_a = @rem_a, rem_st_id = @rem_st_id, rem_dir_id = @rem_dir_id, rem_dircon_id = @rem_dircon_id, rem_fec = @rem_fec, rem_ven = @rem_ven, rem_emb = @rem_emb, rem_$ = @rem_$, rem_usu_n = @rem_usu_n, rem_fm = @rem_fm, rem_$pag = @rem_$pag, rem_vence = @rem_vence, rem_$deu = @rem_$deu, rem_vencedias = @rem_vencedias, rem_obs = @rem_obs, rem_usu_m = @rem_usu_m 
                    			WHERE rem_id = @rem_id;"
            };
            sqlCommand.Parameters.AddWithValue("@rem_id", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@rem_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@rem_st_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@rem_dir_id", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@rem_dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@rem_fec", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@rem_ven", item.REM_VEN);
            sqlCommand.Parameters.AddWithValue("@rem_emb", item.Embarque);
            sqlCommand.Parameters.AddWithValue("@rem_$", item.Total);
            sqlCommand.Parameters.AddWithValue("@rem_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@rem_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@rem_$pag", item.Acumulado);
            sqlCommand.Parameters.AddWithValue("@rem_vence", item.FechaVence);
            sqlCommand.Parameters.AddWithValue("@rem_$deu", item.Saldo);
            sqlCommand.Parameters.AddWithValue("@rem_vencedias", item.VenceDias);
            sqlCommand.Parameters.AddWithValue("@rem_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@rem_usu_m", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE rem SET rem_a = 0 WHERE rem_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public RemisionModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "rem", "rem")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        /// <summary>
        /// Remision
        /// </summary>
        public RemisionDetailModelView GetById(int id, bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = @"select Rem.*,
                                RemST_ID, RemST_A, RemST_Sta, RemST_Sec, RemST_Sum,
                                DIR.*,
                                DIRCON_ID, DIRCON_A, DIRCON_con
                                from Rem, RemST, Dir, DirCon
                                where Rem_ST_ID = RemST_ID and Rem_DIR_ID = DIR_ID
                                    and DIR_DIRCLA_ID = 2 and Rem_DIRCON_ID = DIRCON_ID
                                    and Rem_Id = @index
                                order by REM_ID desc"
            };

            sqlCommand.Parameters.AddWithValue("@index", id);
            var result = this.GetMapper<RemisionDetailModelView>(sqlCommand).Single();
            var partidas = this.remisionPedRepository.GetList(new int[] { id }, onlyActive);
            result.Partidas = new System.ComponentModel.BindingList<RemisionPedModelView>(partidas.Where(it => it.IdRemision == result.IdRemision).ToList());

            return result;
        }

        /// <summary>
        /// almacenar una remision
        /// </summary>
        public RemisionDetailModelView Save(RemisionDetailModelView remision) {
            if (remision.IdRemision == 0) {
                remision.IdRemision = this.Max("rem_id");
                remision.FechaEmision = DateTime.Now;
                remision.IdRemision = this.Insert(remision);
            } else {
                this.Update(remision);
            }
            // partidas
            for (int i = 0; i < remision.Partidas.Count; i++) {
                if (remision.Partidas[i].IdRemPed == 0) {
                    remision.Partidas[i].IdRemPed = this.Max("remped_id");
                    remision.Partidas[i].IdRemision = remision.IdRemision;
                    remision.Partidas[i].IdRemPed = this.remisionPedRepository.Insert(remision.Partidas[i]);
                } else {
                    this.remisionPedRepository.Update(remision.Partidas[i]);
                }
            }
            return remision;
        }

        public IEnumerable<RemisionModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "rem", "rem")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener vista de remisiones por un periodo de fechas y status
        /// </summary>
        /// <param name="starDate">rango inicial de fecha, si el endDate en nulo entonces se considera por año y mes</param>
        /// <param name="endDate">fin del rango de fecha</param>
        /// <param name="startStatus">rango inicial de status, si endStatus es nulo entonces se conisdera el status especifico</param>
        /// <param name="endStatus">fin del rango de status</param>
        public IEnumerable<RemisionModelView> GetList(DateTime startDate, DateTime? endDate, int? startStatus, int? endStatus) {
            var sqlCommand = new FbCommand {
                CommandText = @"select Rem.*,
                                RemST_ID, RemST_A, RemST_Sta, RemST_Sec, RemST_Sum,
                                DIR_ID, DIR_A, DIR_DIRCLA_ID, DIR_cla, DIR_Nom,
                                DIRCON_ID, DIRCON_A, DIRCON_con
                                from Rem, RemST, Dir, DirCon
                                where Rem_ST_ID = RemST_ID and Rem_DIR_ID = DIR_ID
                                    and DIR_DIRCLA_ID = 2 and Rem_DIRCON_ID = DIRCON_ID
                                    @date
                                order by REM_ID desc"
            };

            if (endDate == null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and extract(year from Rem_Fec) = @year and extract(month from Rem_Fec) = @month");
                sqlCommand.Parameters.AddWithValue("@year", startDate.Year);
                sqlCommand.Parameters.AddWithValue("@month", startDate.Month);
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and Rem_Fec >= @startDate and Rem_Fec <= @endDate");
                sqlCommand.Parameters.AddWithValue("@startDate", startDate);
                sqlCommand.Parameters.AddWithValue("@endDate", endDate);
            }

            if (startStatus != null) {
                if (endStatus != null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and Rem_ST_ID >= @startStatus and Rem_ST_ID <= @endStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                    sqlCommand.Parameters.AddWithValue("@endStatus", endStatus);
                }
                else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and Rem_ST_ID = @startStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                }
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "");
            }

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionModelView>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener vista de remisiones por un periodo de fechas y status
        /// </summary>
        /// <param name="starDate">rango inicial de fecha, si el endDate en nulo entonces se considera por año y mes</param>
        /// <param name="endDate">fin del rango de fecha</param>
        /// <param name="startStatus">rango inicial de status, si endStatus es nulo entonces se conisdera el status especifico</param>
        /// <param name="endStatus">fin del rango de status</param>
        public IEnumerable<RemisionDetailModelView> GetList(DateTime startDate, DateTime? endDate, int? startStatus, int? endStatus, bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = @"select Rem.*,
                                RemST_ID, RemST_A, RemST_Sta, RemST_Sec, RemST_Sum,
                                DIR_ID, DIR_A, DIR_DIRCLA_ID, DIR_cla, DIR_Nom,
                                DIRCON_ID, DIRCON_A, DIRCON_con
                                from Rem, RemST, Dir, DirCon
                                where Rem_ST_ID = RemST_ID and Rem_DIR_ID = DIR_ID
                                    and DIR_DIRCLA_ID = 2 and Rem_DIRCON_ID = DIRCON_ID
                                    @date
                                order by REM_ID desc"
            };

            if (endDate == null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and extract(year from Rem_Fec) = @year and extract(month from Rem_Fec) = @month");
                sqlCommand.Parameters.AddWithValue("@year", startDate.Year);
                sqlCommand.Parameters.AddWithValue("@month", startDate.Month);
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and Rem_Fec >= @startDate and Rem_Fec <= @endDate");
                sqlCommand.Parameters.AddWithValue("@startDate", startDate);
                sqlCommand.Parameters.AddWithValue("@endDate", endDate);
            }

            if (startStatus != null) {
                if (endStatus != null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and Rem_ST_ID >= @startStatus and Rem_ST_ID <= @endStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                    sqlCommand.Parameters.AddWithValue("@endStatus", endStatus);
                }
                else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and Rem_ST_ID = @startStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                }
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "");
            }

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionDetailModelView>();
            var result = mapper.Map(tabla).ToList();
            var ids = result.Select(it => it.IdRemision).ToArray();
            var partidas = this.remisionPedRepository.GetList(ids, onlyActive);

            for (int i = 0; i < result.Count(); i++) {
                result[i].Partidas = new System.ComponentModel.BindingList<RemisionPedModelView>(partidas.Where(it => it.IdRemision == result[i].IdRemision).ToList());
            }

            return result;
        }

        /// <summary>
        /// obtener vista de remisiones por condicionales
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<RemisionPedDetailModelView> GetList(List<Conditional> conditionals, bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = @"select Rem.*, RemPed.*,
                                RemST_ID, RemST_A, RemST_Sta, RemST_Sec, RemST_Sum,
                                DIR_ID, DIR_A, DIR_DIRCLA_ID, DIR_cla, DIR_Nom,
                                DIRCON_ID, DIRCON_A, DIRCON_con,
                                PEDPRD_ID, PEDPRD_A, PedST_ID, Dir_Nom, PEDPRD_Nom, PedST_Sta, PedST_Sec, PedPrd_Uni, PEDPRD_$Uni
                                from Rem, RemST, Dir, DirCon, PedPrd, PedST, RemPed
                                where Rem_ST_ID = RemST_ID 
                                    and Rem_DIR_ID = DIR_ID
                                    and DIR_DIRCLA_ID = 2 
                                    and Rem_DIRCON_ID = DIRCON_ID
                                    and RemPed_PEDPRD_ID = PEDPRD_ID 
                                    and PedPrd_St_ID = PedST_ID 
                                    and RemPed_Rem_ID = Rem_ID
                                    @condiciones
                                order by REM_ID desc"
            };

            conditionals.Add(new Conditional() { ConditionalType = ConditionalTypeEnum.Like, FieldName = "RemPed_a", FieldValue = "1" });
            var d = FireBird.Services.ExpressionTool.ConditionalModelToSql(conditionals);
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "and" + d.Key);
            sqlCommand.Parameters.AddRange(d.Value);

            var result = this.GetMapper<RemisionPedDetailModelView>(sqlCommand);
           
            return result;
        }

        /// <summary>
        /// obtener lista de remisiones por el nombre del receptor
        /// </summary>
        /// <param name="receptor">descripcion del receptor</param>
        /// <param name="startStatus">status inicial</param>
        /// <param name="endStatus">status final</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <returns></returns>
        public IEnumerable<RemisionDetailModelView> GetList(string receptor, int? startStatus, int? endStatus, bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = @"select Rem.*,
                                RemST_ID, RemST_A, RemST_Sta, RemST_Sec, RemST_Sum,
                                DIR_ID, DIR_A, DIR_DIRCLA_ID, DIR_cla, DIR_Nom,
                                DIRCON_ID, DIRCON_A, DIRCON_con
                                from Rem, RemST, Dir, DirCon
                                where Rem_ST_ID = RemST_ID and Rem_DIR_ID = DIR_ID
                                    and DIR_DIRCLA_ID = 2 and Rem_DIRCON_ID = DIRCON_ID and DIR_Nom like '%@receptor%'
                                order by REM_ID desc"
            };

            if (startStatus != null) {
                if (endStatus != null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and Rem_ST_ID >= @startStatus and Rem_ST_ID <= @endStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                    sqlCommand.Parameters.AddWithValue("@endStatus", endStatus);
                } else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and Rem_ST_ID = @startStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                }
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "");
            }

            sqlCommand.Parameters.AddWithValue("@receptor", receptor);

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionDetailModelView>();
            var result = mapper.Map(tabla).ToList();
            var ids = result.Select(it => it.IdRemision).ToArray();
            var partidas = this.remisionPedRepository.GetList(ids, onlyActive);

            for (int i = 0; i < result.Count(); i++) {
                result[i].Partidas = new System.ComponentModel.BindingList<RemisionPedModelView>(partidas.Where(it => it.IdRemision == result[i].IdRemision).ToList());
            }

            return result;
        }

        /// <summary>
        /// obtener vista de remisiones por un periodo de fechas y status, incluye detalles de la partida
        /// </summary>
        /// <param name="startDate">rango inicial de fecha, si el endDate en nulo entonces se considera por año y mes</param>
        /// <param name="endDate">fin del rango de fecha</param>
        /// <param name="startStatus">rango inicial de status, si endStatus es nulo entonces se conisdera el status especifico</param>
        /// <param name="endStatus">fin del rango de status</param>
        /// <param name="startPedPrd">inicio del rango PedPrd, si el parametro endPedPrd es nulo se considera un solo numero de orden</param>
        /// <param name="endPedPrd"></param>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<RemisionPedDetailModelView> GetList(DateTime? startDate, DateTime? endDate, int? startStatus, int? endStatus, int? startPedPrd, int? endPedPrd, bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = @"select Rem.*, RemPed.*,
                                RemST_ID, RemST_A, RemST_Sta, RemST_Sec, RemST_Sum,
                                DIR_ID, DIR_A, DIR_DIRCLA_ID, DIR_cla, DIR_Nom,
                                DIRCON_ID, DIRCON_A, DIRCON_con,
                                PEDPRD_ID, PEDPRD_A, PedST_ID, Dir_Nom, PEDPRD_Nom, PedST_Sta, PedST_Sec, PedPrd_Uni, PEDPRD_$Uni
                                from Rem, RemST, Dir, DirCon, PedPrd, PedST, RemPed
                                where Rem_ST_ID = RemST_ID 
                                    and Rem_DIR_ID = DIR_ID
                                    and DIR_DIRCLA_ID = 2 
                                    and Rem_DIRCON_ID = DIRCON_ID
                                    and RemPed_PEDPRD_ID = PEDPRD_ID 
                                    and PedPrd_St_ID = PedST_ID 
                                    and RemPed_Rem_ID = Rem_ID
                                    @date @pedido
                                order by REM_ID desc"
            };

            if (startDate != null) {
                if (endDate == null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and extract(year from Rem_Fec) = @year and extract(month from Rem_Fec) = @month");
                    sqlCommand.Parameters.AddWithValue("@year", startDate.Value.Year);
                    sqlCommand.Parameters.AddWithValue("@month", startDate.Value.Month);
                }
                else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and Rem_Fec >= @startDate and Rem_Fec <= @endDate");
                    sqlCommand.Parameters.AddWithValue("@startDate", startDate);
                    sqlCommand.Parameters.AddWithValue("@endDate", endDate);
                }
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "");
            }

            if (startStatus != null) {
                if (endStatus != null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and Rem_ST_ID >= @startStatus and Rem_ST_ID <= @endStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                    sqlCommand.Parameters.AddWithValue("@endStatus", endStatus);
                }
                else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and Rem_ST_ID = @startStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                }
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "");
            }

            if (startPedPrd != null) {
                if (endPedPrd != null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@pedido", "and PedPrd_Id >= @startPedPrd and PedPrd_Id <= @endPedPrd");
                    sqlCommand.Parameters.AddWithValue("@startPedPrd", startPedPrd);
                    sqlCommand.Parameters.AddWithValue("@endPedPrd", endPedPrd);
                }
                else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@pedido", "and PedPrd_Id = @startPedPrd");
                    sqlCommand.Parameters.AddWithValue("@startPedPrd", startPedPrd);
                }
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@pedido", "");
            }

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionPedDetailModelView>();
            var result = mapper.Map(tabla).ToList();

            return result;
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"select Rem.*, RemPed.*,
                                RemST_ID, RemST_A, RemST_Sta, RemST_Sec, RemST_Sum,
                                DIR_ID, DIR_A, DIR_DIRCLA_ID, DIR_cla, DIR_Nom,
                                DIRCON_ID, DIRCON_A, DIRCON_con,
                                PEDPRD_ID, PEDPRD_A, PedST_ID, Dir_Nom, PEDPRD_Nom, PedST_Sta, PedST_Sec, PedPrd_Uni, PEDPRD_$Uni
                                from Rem, RemST, Dir, DirCon, PedPrd, PedST, RemPed
                                where Rem_ST_ID = RemST_ID 
                                    and Rem_DIR_ID = DIR_ID
                                    and DIR_DIRCLA_ID = 2 
                                    and Rem_DIRCON_ID = DIRCON_ID
                                    and RemPed_PEDPRD_ID = PEDPRD_ID 
                                    and PedPrd_St_ID = PedST_ID 
                                    and RemPed_Rem_ID = Rem_ID
                                    @condiciones
                                order by REM_ID desc"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }
    }
}
