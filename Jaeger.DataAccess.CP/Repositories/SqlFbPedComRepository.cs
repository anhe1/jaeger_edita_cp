﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// clase componentes / ensambles de la orden de produccion (PedCom)
    /// </summary>
    public class SqlFbPedComRepository : RepositoryMaster<ComponenteModel>, ISqlPedComRepository {
        protected ISqlPedProRepository pedproRepository;
        public SqlFbPedComRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.pedproRepository = new SqlFbPedProRepository(configuracion, user);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedcom SET pedcom_a = 0 WHERE pedcom_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ComponenteModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "pedcom", "pedcom")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComponenteModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ComponenteModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from pedcom where pedcom_a > 0 "
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComponenteModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener lista de componentes asociados a una orden (PedCom_PedPrd_ID=PedPrd_ID)
        /// </summary>
        public IEnumerable<ComponenteDetailModel> GetList(int idOrden) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from pedcom where pedcom_a > 0 and PedCom_PedPrd_ID = @index order by PedCom_Sec;"
            };

            sqlCommand.Parameters.AddWithValue("@index", idOrden);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComponenteDetailModel>();
            var result = mapper.Map(tabla).ToList();
            var linea = this.pedproRepository.GetList(result.Select(it => it.IdComponente).ToArray());
            if (linea != null) {
                for (int i = 0; i < result.Count; i++) {
                    result[i].LineaProduccion = new System.ComponentModel.BindingList<LineaProduccionModel>(linea.Where(it => it.IdComponente == result[i].IdComponente).ToList());
                }
            }
            return result;
        }

        /// <summary>
        /// obtener lista de componentes asociados a una orden (PedCom_PedPrd_ID=PedPrd_ID)
        /// </summary>
        public IEnumerable<ComponenteDetailModel> GetList(int[] idOrden) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from pedcom where pedcom_a > 0 and PedCom_PedPrd_ID in (@index) order by PedCom_Sec;"
            };

            //sqlCommand.Parameters.AddWithValue("@index", idOrden);
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", string.Join(",", idOrden));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComponenteDetailModel>();
            var result = mapper.Map(tabla).ToList();
            var linea = this.pedproRepository.GetList(result.Select(it => it.IdComponente).ToArray());
            if (linea != null) {
                for (int i = 0; i < result.Count; i++) {
                    result[i].LineaProduccion = new System.ComponentModel.BindingList<LineaProduccionModel>(linea.Where(it => it.IdComponente == result[i].IdComponente).ToList());
                }
            }
            return result;
        }

        /// <summary>
        /// obtener lista de componentes asociados a una orden (PedCom_PedPrd_ID=PedPrd_ID)
        /// </summary>
        public IEnumerable<ComponenteView> GetListView(int[] idOrden) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from pedcom where pedcom_a > 0 and PedCom_PedPrd_ID in (@index) order by PedCom_Sec;"
            };

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", string.Join(",", idOrden));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComponenteView>();
            var result = mapper.Map(tabla).ToList();
            var linea = this.pedproRepository.GetListView(result.Select(it => it.IdComponente).ToArray());
            if (linea != null) {
                for (int i = 0; i < result.Count; i++) {
                    result[i].LineaProduccion = new System.ComponentModel.BindingList<LineaProduccionDeptoCentroView>(linea.Where(it => it.IdComponente == result[i].IdComponente).ToList());
                }
            }
            return result;
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM PEDCOM @wcondiciones ORDER BY PEDCOM_SEC;"
            };

            sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
            return this.GetMapper<T1>(sqlCommand).ToList();
        }

        public int Insert(ComponenteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO pedcom (pedcom_id, pedcom_a, pedcom_cotcom_id, pedcom_pedprd_id, pedcom_cla, pedcom_nom, pedcom_num, pedcom_can_com, pedcom_sec, pedcom_t_max, pedcom_minr, pedcom_fec_min, pedcom_fec_max, pedcom_fec_ini, pedcom_fec_fin, pedcom_usu_fm, pedcom_usu_m, pedcom_fm) 
                            VALUES (@pedcom_id, @pedcom_a, @pedcom_cotcom_id, @pedcom_pedprd_id, @pedcom_cla, @pedcom_nom, @pedcom_num, @pedcom_can_com, @pedcom_sec, @pedcom_t_max, @pedcom_minr, @pedcom_fec_min, @pedcom_fec_max, @pedcom_fec_ini, @pedcom_fec_fin, @pedcom_usu_fm, @pedcom_usu_m, @pedcom_fm)"
            };
            item.IdComponente = this.Max("PEDCOM_ID");
            sqlCommand.Parameters.AddWithValue("@pedcom_id", item.IdComponente);
            sqlCommand.Parameters.AddWithValue("@pedcom_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedcom_cotcom_id", item.PEDCOM_COTCOM_ID);
            sqlCommand.Parameters.AddWithValue("@pedcom_pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@pedcom_cla", item.Clave);
            sqlCommand.Parameters.AddWithValue("@pedcom_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@pedcom_num", item.PEDCOM_NUM);
            sqlCommand.Parameters.AddWithValue("@pedcom_can_com", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@pedcom_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@pedcom_t_max", item.TiempoEstimado);
            sqlCommand.Parameters.AddWithValue("@pedcom_minr", item.TiempoReal);
            sqlCommand.Parameters.AddWithValue("@pedcom_fec_min", item.FechaMinima);
            sqlCommand.Parameters.AddWithValue("@pedcom_fec_max", item.FechaMaxima);
            sqlCommand.Parameters.AddWithValue("@pedcom_fec_ini", item.FechaInicial);
            sqlCommand.Parameters.AddWithValue("@pedcom_fec_fin", item.FechaFinal);
            sqlCommand.Parameters.AddWithValue("@pedcom_usu_fm", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@pedcom_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@pedcom_fm", item.FechaModifica);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdComponente;
            return 0;
        }

        public int Update(ComponenteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedcom 
                            SET pedcom_a = @pedcom_a, pedcom_cotcom_id = @pedcom_cotcom_id, pedcom_pedprd_id = @pedcom_pedprd_id, pedcom_cla = @pedcom_cla, pedcom_nom = @pedcom_nom, pedcom_num = @pedcom_num, pedcom_can_com = @pedcom_can_com, pedcom_sec = @pedcom_sec, pedcom_t_max = @pedcom_t_max, pedcom_minr = @pedcom_minr, pedcom_fec_min = @pedcom_fec_min, pedcom_fec_max = @pedcom_fec_max, pedcom_fec_ini = @pedcom_fec_ini, pedcom_fec_fin = @pedcom_fec_fin, pedcom_usu_fm = @pedcom_usu_fm, pedcom_usu_m = @pedcom_usu_m, pedcom_fm = @pedcom_fm 
                            WHERE pedcom_id = @pedcom_id;"
            };
            sqlCommand.Parameters.AddWithValue("@pedcom_id", item.IdComponente);
            sqlCommand.Parameters.AddWithValue("@pedcom_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedcom_cotcom_id", item.PEDCOM_COTCOM_ID);
            sqlCommand.Parameters.AddWithValue("@pedcom_pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@pedcom_cla", item.Clave);
            sqlCommand.Parameters.AddWithValue("@pedcom_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@pedcom_num", item.PEDCOM_NUM);
            sqlCommand.Parameters.AddWithValue("@pedcom_can_com", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@pedcom_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@pedcom_t_max", item.TiempoEstimado);
            sqlCommand.Parameters.AddWithValue("@pedcom_minr", item.TiempoReal);
            sqlCommand.Parameters.AddWithValue("@pedcom_fec_min", item.FechaMinima);
            sqlCommand.Parameters.AddWithValue("@pedcom_fec_max", item.FechaMaxima);
            sqlCommand.Parameters.AddWithValue("@pedcom_fec_ini", item.FechaInicial);
            sqlCommand.Parameters.AddWithValue("@pedcom_fec_fin", item.FechaFinal);
            sqlCommand.Parameters.AddWithValue("@pedcom_usu_fm", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@pedcom_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@pedcom_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
