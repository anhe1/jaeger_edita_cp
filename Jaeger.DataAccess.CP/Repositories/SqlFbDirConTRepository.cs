﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Contribuyentes.Contracts;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de telefonos del contacto del directorio (DIRCONT)
    /// </summary>
    public class SqlFbDirConTRepository : RepositoryMaster<ContribuyenteContactoTelefono>, ISqlDirConTRepository {
        /// <summary>
        /// repositorio de telefonos del contacto del directorio (DIRCONT)
        /// </summary>
        public SqlFbDirConTRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Insert(ContribuyenteContactoTelefono item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO dircont (dircont_id, dircont_a, dircont_tel_id, dircont_dircon_id, dircont_num, dircont_fm) 
                			VALUES (@dircont_id, @dircont_a, @dircont_tel_id, @dircont_dircon_id, @dircont_num, @dircont_fm)"
            };
            item.IdDirConT = this.Max("DIRCONT_ID");
            sqlCommand.Parameters.AddWithValue("@dircont_id", item.IdDirConT);
            sqlCommand.Parameters.AddWithValue("@dircont_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@dircont_tel_id", item.IdTelefonia);
            sqlCommand.Parameters.AddWithValue("@dircont_dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@dircont_num", item.Numero);
            sqlCommand.Parameters.AddWithValue("@dircont_fm", item.FechaModifica);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdDirConT;
            return 0;
        }

        public int Update(ContribuyenteContactoTelefono item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE dircont 
                			SET dircont_a = @dircont_a, dircont_tel_id = @dircont_tel_id, dircont_dircon_id = @dircont_dircon_id, dircont_num = @dircont_num, dircont_fm = @dircont_fm 
                			WHERE dircont_id = @dircont_id;"
            };
            sqlCommand.Parameters.AddWithValue("@dircont_id", item.IdDirConT);
            sqlCommand.Parameters.AddWithValue("@dircont_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@dircont_tel_id", item.IdTelefonia);
            sqlCommand.Parameters.AddWithValue("@dircont_dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@dircont_num", item.Numero);
            sqlCommand.Parameters.AddWithValue("@dircont_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE dircont SET dircont_a = 0 WHERE dircont_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ContribuyenteContactoTelefono GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "dircont", "dircont")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ContribuyenteContactoTelefono>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ContribuyenteContactoTelefono> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "dircont", "dircont")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ContribuyenteContactoTelefono>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de telefonia de los indices de los contactos
        /// </summary>
        /// <param name="indexs">array de indices DirCon_ID</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <returns></returns>
        public IEnumerable<ContribuyenteContactoTelefono> GetList(int[] indexs, bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from DirConT where DIRCONT_DIRCON_ID in (@indexs) and DirCONT_A > 0"
            };

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", string.Join(",", indexs));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ContribuyenteContactoTelefono>();
            return mapper.Map(tabla).ToList();
        }
    }
}
