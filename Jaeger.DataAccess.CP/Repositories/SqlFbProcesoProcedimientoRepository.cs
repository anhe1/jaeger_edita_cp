﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// interface del repositorio para procedimientos en los procesos (PRO3)
    /// </summary>
    public class SqlFbProcesoProcedimientoRepository : RepositoryMaster<ProcesoProcedimientoModel>, ISqlProcesoProcedimientoRepository {
        public SqlFbProcesoProcedimientoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pro3 SET pro3_a = 0 WHERE pro3_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ProcesoProcedimientoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from pro3 where pro3_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoProcedimientoModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ProcesoProcedimientoModel> GetList() {
            var sqlCheckList = new FbCommand {
                CommandText = @"select * from PRO3, PUE where PRO3_A > 0 and Pro3_Pue_ID = Pue_ID"
            };
            var tabla = this.ExecuteReader(sqlCheckList);
            var mapper = new DataNamesMapper<ProcesoProcedimientoModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de procedimiento, solo activos y array de indices EDP4_ID
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        /// <param name="indexs">array de indices EPD4_ID</param>
        /// <returns></returns>
        public IEnumerable<ProcesoProcedimientoModel> GetList(bool onlyActive, int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = @"select Pro3.* 
                                from PRO3, PUE 
                                where PRO3_A > 0 
                                and Pro3_Pue_ID = Pue_ID
                                and Pro3_Pro2_ID in (@indexs)"
            };

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", string.Join(",", indexs));
            if (onlyActive) {
                sqlCommand.Parameters.AddWithValue("@activo", 1);
            }

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoProcedimientoModel>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(ProcesoProcedimientoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO pro3 (pro3_id, pro3_a, pro3_nom, pro3_pue_id, pro3_cal, pro3_eval, pro3_fm, pro3_pro2_id) 
                			VALUES (@pro3_id, @pro3_a, @pro3_nom, @pro3_pue_id, @pro3_cal, @pro3_eval, @pro3_fm, @pro3_pro2_id)"
            };

            item.IdProcedimiento = this.Max("PRO3_ID");
            sqlCommand.Parameters.AddWithValue("@pro3_id", item.IdProcedimiento);
            sqlCommand.Parameters.AddWithValue("@pro3_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pro3_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@pro3_pue_id", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@pro3_cal", item.Calidad);
            sqlCommand.Parameters.AddWithValue("@pro3_eval", item.Evaluacion);
            sqlCommand.Parameters.AddWithValue("@pro3_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@pro3_pro2_id", item.PRO3_PRO2_ID);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdProcedimiento;
            return 0;
        }

        public int Update(ProcesoProcedimientoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pro3 
                			SET pro3_a = @pro3_a, pro3_nom = @pro3_nom, pro3_pue_id = @pro3_pue_id, pro3_cal = @pro3_cal, pro3_eval = @pro3_eval, pro3_fm = @pro3_fm, pro3_pro2_id = @pro3_pro2_id 
                			WHERE pro3_id = @pro3_id;"
            };

            sqlCommand.Parameters.AddWithValue("@pro3_id", item.IdProcedimiento);
            sqlCommand.Parameters.AddWithValue("@pro3_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pro3_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@pro3_pue_id", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@pro3_cal", item.Calidad);
            sqlCommand.Parameters.AddWithValue("@pro3_eval", item.Evaluacion);
            sqlCommand.Parameters.AddWithValue("@pro3_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@pro3_pro2_id", item.PRO3_PRO2_ID);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
