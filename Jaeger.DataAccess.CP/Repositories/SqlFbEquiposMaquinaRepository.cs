﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// interface del repositorio de Equipos y Maquinaria (EDP2)
    /// </summary>
    public class SqlFbEquiposMaquinaRepository : RepositoryMaster<CentroModel>, ISqlEquiposMaquinaRepository {
        protected ISqlProcesoRepository procesoRepository;

        public SqlFbEquiposMaquinaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.procesoRepository = new SqlFbProcesoRepository(configuracion);
        }

        #region CRUD
        public int Insert(CentroModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO edp2 ( edp2_id, edp2_a, edp2_sec, edp2_nom, edp2_edp1_id, edp2_pro, edp2_mar, edp2_mod, edp2_cla, edp2_num_ser, edp2_cal, edp2_ano, edp2_val, edp2_dia, edp2_hor, edp2_tm, edp2_val_min, edp2_usu_fm, edp2_fm) 
                			              VALUES (@edp2_id,@edp2_a,@edp2_sec,@edp2_nom,@edp2_edp1_id,@edp2_pro,@edp2_mar,@edp2_mod,@edp2_cla,@edp2_num_ser,@edp2_cal,@edp2_ano,@edp2_val,@edp2_dia,@edp2_hor,@edp2_tm,@edp2_val_min,@edp2_usu_fm,@edp2_fm);"
            };
            item.IdCentro = this.Max("EDP2_ID");
            sqlCommand.Parameters.AddWithValue("@edp2_id", item.IdCentro);
            sqlCommand.Parameters.AddWithValue("@edp2_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp2_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@edp2_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@edp2_edp1_id", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@edp2_pro", item.EDP2_PRO);
            sqlCommand.Parameters.AddWithValue("@edp2_mar", item.Marca);
            sqlCommand.Parameters.AddWithValue("@edp2_mod", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@edp2_cla", item.Clave);
            sqlCommand.Parameters.AddWithValue("@edp2_num_ser", item.NoSerie);
            sqlCommand.Parameters.AddWithValue("@edp2_cal", item.EDP2_CAL);
            sqlCommand.Parameters.AddWithValue("@edp2_ano", item.AnioAmortizacion);
            sqlCommand.Parameters.AddWithValue("@edp2_val", item.Valor);
            sqlCommand.Parameters.AddWithValue("@edp2_dia", item.DiasXAnio);
            sqlCommand.Parameters.AddWithValue("@edp2_hor", item.HoraXDia);
            sqlCommand.Parameters.AddWithValue("@edp2_tm", item.Uso);
            sqlCommand.Parameters.AddWithValue("@edp2_val_min", item.ValorXMinuto);
            sqlCommand.Parameters.AddWithValue("@edp2_usu_fm", item.EDP2_USU_FM);
            sqlCommand.Parameters.AddWithValue("@edp2_fm", item.FechaModifica);

            if (this.ExecuteScalar(sqlCommand) > 0)
                return item.IdCentro;
            return 0;
        }

        public int Update(CentroModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp2 
                			SET edp2_a = @edp2_a, edp2_sec = @edp2_sec, edp2_nom = @edp2_nom, edp2_edp1_id = @edp2_edp1_id, edp2_pro = @edp2_pro, edp2_mar = @edp2_mar, edp2_mod = @edp2_mod, edp2_cla = @edp2_cla, edp2_num_ser = @edp2_num_ser, edp2_cal = @edp2_cal, edp2_ano = @edp2_ano, edp2_val = @edp2_val, edp2_dia = @edp2_dia, edp2_hor = @edp2_hor, edp2_tm = @edp2_tm, edp2_val_min = @edp2_val_min, edp2_usu_fm = @edp2_usu_fm, edp2_fm = @edp2_fm 
                			WHERE edp2_id = @edp2_id;"
            };

            sqlCommand.Parameters.AddWithValue("@edp2_id", item.IdCentro);
            sqlCommand.Parameters.AddWithValue("@edp2_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp2_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@edp2_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@edp2_edp1_id", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@edp2_pro", item.EDP2_PRO);
            sqlCommand.Parameters.AddWithValue("@edp2_mar", item.Marca);
            sqlCommand.Parameters.AddWithValue("@edp2_mod", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@edp2_cla", item.Clave);
            sqlCommand.Parameters.AddWithValue("@edp2_num_ser", item.NoSerie);
            sqlCommand.Parameters.AddWithValue("@edp2_cal", item.EDP2_CAL);
            sqlCommand.Parameters.AddWithValue("@edp2_ano", item.AnioAmortizacion);
            sqlCommand.Parameters.AddWithValue("@edp2_val", item.Valor);
            sqlCommand.Parameters.AddWithValue("@edp2_dia", item.DiasXAnio);
            sqlCommand.Parameters.AddWithValue("@edp2_hor", item.HoraXDia);
            sqlCommand.Parameters.AddWithValue("@edp2_tm", item.Uso);
            sqlCommand.Parameters.AddWithValue("@edp2_val_min", item.ValorXMinuto);
            sqlCommand.Parameters.AddWithValue("@edp2_usu_fm", item.EDP2_USU_FM);
            sqlCommand.Parameters.AddWithValue("@edp2_fm", item.FechaModifica);

            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp2 SET edp2_a = 0 WHERE edp2_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CentroModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "edp2", "edp2")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CentroDetailModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<CentroModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from edp2 where edp2_a = 1 order by edp2_nom"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CentroModel>();
            return mapper.Map(tabla).ToList();
        }
        #endregion

        public IEnumerable<CentroDetailModel> GetList(int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from edp2 where edp2_a = 1 order by edp2_nom"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CentroDetailModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de centros productivos
        /// </summary>
        /// <param name="idDepartamento">indice del departamento</param>
        public IEnumerable<CentroModel> GetList(int idDepartamento) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from edp2 where edp2_a = 1 and edp2_edp1_id = @index order by edp2_nom"
            };

            sqlCommand.Parameters.AddWithValue("@index", idDepartamento);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CentroModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// listado de secciones y centros (EDP2)
        /// </summary>
        public IEnumerable<SeccionCentroDetailModel> GetList(bool onlyActive, int[] idDeptos) {
            var sqlCommand = new FbCommand {
                CommandText = @"select edp2.* 
                                from edp2 
                                where @index @active
                                order by edp2_nom"
            };

            if (idDeptos.Count() > 0) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", "EDP2_EDP1_ID in (@index)");
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", "");
            }

            if (onlyActive) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@active", "and edp2_a > 0");
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@active", "");
            }

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", string.Join(",", idDeptos));

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<SeccionCentroDetailModel>();
            var centros = mapper.Map(tabla).ToList();
            var ids = centros.Select(it => it.IdSeccion).ToArray();
            var procesos = this.procesoRepository.GetList(true, ids);
            var equipos = this.GetList(new int[] { 0 });

            for (int i = 0; i < centros.Count; i++) {
                centros[i].Proceso = new System.ComponentModel.BindingList<ProcesoDetailModel>(procesos.Where(it => it.IdCentro == centros[i].IdSeccion).ToList());
                centros[i].EquipoMaquinarias = new System.ComponentModel.BindingList<CentroDetailModel>(equipos.Where(it => it.IdCentro == centros[i].IdSeccion).ToList());
            }

            return centros;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM EDP2, EDP1 WHERE EDP2_EDP1_ID = EDP1_ID @condiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals));
        }
    }
}
