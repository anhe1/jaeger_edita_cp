﻿using System.Linq;
using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbUIMenuRepository : RepositoryMaster<UIMenuModel>, ISqlUIMenuRepository {
        public SqlFbUIMenuRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM UIMENU WHERE UIMENU_ID = @index;"
            };

            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public UIMenuModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 * FROM UIMENU WHERE IUMENU_ID = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<UIMenuElement> GetCPBeta() {
            throw new NotImplementedException();
        }

        public IEnumerable<UIMenuElement> GetCPLite() {
            throw new NotImplementedException();
        }

        public IEnumerable<UIMenuModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM UIMENU"
            };
            return this.GetMapper(sqlCommand);
        }

        public IEnumerable<UIMenuElement> GetMenus() {
            var response = new List<UIMenuElement> {
                new UIMenuElement { Id = 81000, ParentId = 0, Name = "TCPEmpresa", Label = "Empresa" },
                new UIMenuElement { Id = 81100, ParentId = 81000, Name = "cpemp_grp_directorio", Label = "Directorio" },
                new UIMenuElement { Id = 81110, ParentId = 81100, Name = "cpemp_gdir_directorio", Label = "Directorio", Form = "Forms.Empresa.ContribuyentesForm", Assembly = "Jaeger.UI.CP.Beta", 
                    LPermisos = new List<UIActionEnum>{ UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Autorizar } },
                new UIMenuElement { Id = 81200, ParentId = 81100, Name = "cpemp_grp_producto", Label = "Productos y Servicios" },
                new UIMenuElement { Id = 81210, ParentId = 81200, Name = "cpemp_gprd_clasificacion", Label = "Clasificación", Form = "Forms.Empresa.ClasificacionCatalogoForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 81220, ParentId = 81200, Name = "cpemp_gprd_catalogo", Label = "Productos\r\nServicios", Form = "Forms.Cotizacion.ProductoServicioCatalogoForm", Assembly = "Jaeger.UI.CP.Beta" },

                new UIMenuElement { Id = 84000, ParentId = 0, Name = "TCPCotizacion", Label = "Cotizaciones"},
                new UIMenuElement { Id = 84100, ParentId = 84000, Name = "cpcot_grp_cotizacion", Label = "Productos"},
                new UIMenuElement { Id = 84110, ParentId = 84100, Name = "cpcot_gcot_productos", Label = "Productos"},
                new UIMenuElement { Id = 84111, ParentId = 84200, Name = "cpcot_bprd_subproducto", Label = "Sub\r\nProductos"},
                new UIMenuElement { Id = 84200, ParentId = 84100, Name = "cpcot_gcot_presupuesto", Label = "Presupuestos", Form = "", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 84300, ParentId = 84100, Name = "cpcot_gcot_cotizaciones", Label = "Cotizaciones", Form = "Forms.Cotizacion.CotizacionCatalogoForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 84400, ParentId = 84100, Name = "cpcot_gcot_cliente", Label = "Clientes", Form = "Forms.Cotizacion.ClientesForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum>{ UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Remover, UIActionEnum.Autorizar } },
                new UIMenuElement { Id = 84500, ParentId = 84100, Name = "cpcot_gcot_conf", Label = "Configuración"},

                new UIMenuElement { Id = 85000, ParentId = 0, Name = "TCPProduccion", Label = "Producción" },
                new UIMenuElement { Id = 85100, ParentId = 85000, Name = "cppro_grp_produccion", Label = "Producción" },
                new UIMenuElement { Id = 85110, ParentId = 85100, Name = "cppro_gpro_orden", Label = "Ord. Producción" },
                new UIMenuElement { Id = 85111, ParentId = 85110, Name = "cppro_bord_historial", Label = "Historial", Form = "Forms.Produccion.OrdenProduccionCatalogoForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Exportar } },
                new UIMenuElement { Id = 85112, ParentId = 85110, Name = "cppro_bord_proceso", Label = "En Proceso", Form = "Forms.Produccion.OrdenEnProcesoForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Exportar }},
                new UIMenuElement { Id = 85113, ParentId = 85100, Name = "cppro_gpro_reqcompra", Label = "Requerimiento\r\nde Compra", Form = "Forms.Adquisiciones.ReqsCompraForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Autorizar } },

                new UIMenuElement { Id = 85120, ParentId = 85100, Name = "cppro_gpro_planea", Label = "Planeación", Form = "Forms.Produccion.CalendarioEntregaForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85136, ParentId = 85120, Name = "cppro_gpro_calendario", Label = "Calendario de Entregas", Form = "Forms.Produccion.CalendarioEntregaForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85137, ParentId = 85120, Name = "cppro_gpro_gantt", Label = "Gantt de Entregas", Form = "Forms.Produccion.Planeacion3Form", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85138, ParentId = 85120, Name = "cppro_gpro_plan", Label = "Departamento", Form = "Forms.Produccion.Planeacion2Form", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85139, ParentId = 85120, Name = "cppro_gpro_tieadd", Label = "Tiempo Extra", Form = "Forms.Produccion.TiempoExtraForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85145, ParentId = 85120, Name = "cppro_gpro_plan4", Label = "Planeación 04", Form = "Forms.Produccion.Planeacion7Form", Assembly = "Jaeger.UI.CP.Beta" },
                
                new UIMenuElement { Id = 85130, ParentId = 85100, Name = "cppro_gpro_sgrupo", Label = "Sub Grupo Producción" },
                new UIMenuElement { Id = 85131, ParentId = 85130, Name = "cppro_sgrp_depto", Label = "Departamento", Form = "Forms.Produccion.DepartamentoCalendarioEntregaForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85132, ParentId = 85130, Name = "cppro_sgrp_avance", Label = "Avance", Form = "Forms.Produccion.DepartamentoProduccionForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85133, ParentId = 85130, Name = "cppro_sgrp_evaluacion", Label = "Evaluación"},
                new UIMenuElement { Id = 85134, ParentId = 85133, Name = "cppro_beva_eproduccion", Label = "Producción", Form = "Forms.Produccion.EvaluacionDepartamentoForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85135, ParentId = 85133, Name = "cppro_beva_eperiodo", Label = "Período", Form = "Forms.Produccion.ProduccionEvaluacion1Form", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 85144, ParentId = 85133, Name = "cppro_beva_eprodof", Label = "Por Oficial", Form = "Forms.Produccion.DepartamentoEvaluacionPForm", Assembly = "Jaeger.UI.CP.Beta" },
                

                new UIMenuElement { Id = 85140, ParentId = 85100, Name = "cppro_gpro_remision", Label = "Remisionado" },
                new UIMenuElement { Id = 85141, ParentId = 85140, Name = "cppro_brem_emitido", Label = "Emitido", Form = "Forms.Almacen.DP.RemisionadoForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status } },
                new UIMenuElement { Id = 85142, ParentId = 85140, Name = "tcppro_brem_epartida", Label = "Recibido", Form = "Forms.Produccion.RemisionadoPartidaForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Exportar }},
                new UIMenuElement { Id = 85143, ParentId = 85140, Name = "tcppro_brem_recibido", Label = "Recibido", Form = "Forms.Produccion.RemisionesForm", Assembly = "Jaeger.UI.CP.Beta" },

                new UIMenuElement { Id = 86000, ParentId = 0, Name = "TCPVentas", Label = "Ventas"},
                new UIMenuElement { Id = 86100, ParentId = 86000, Name = "cpvnt_grp_ventas", Label = "Ventas"},
                new UIMenuElement { Id = 86110, ParentId = 86000, Name = "cpvnt_gven_vendedor", Label = "Vendedores", Form = "Forms.Ventas.VendedoresCatalogoForm", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 86120, ParentId = 86100, Name = "cpvnt_gven_prodvend", Label = "Productos \r\nVendidos", Form = "Forms.Ventas.ProductoVendidoCatalogoForm", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 86130, ParentId = 86000, Name = "cpvnt_gven_group1", Label = "Sub grupo de ventas"},
                new UIMenuElement { Id = 86131, ParentId = 86000, Name = "cpvnt_gven_pedidos", Label = "Pedidos", Form = "Forms.Ventas.OrdenProduccionCatalogoForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 86132, ParentId = 86000, Name = "cpvnt_gven_comision", Label = "Comisiones"},
                new UIMenuElement { Id = 86133, ParentId = 86000, Name = "cpvnt_gven_ventcosto", Label = "Venta vs Costo", Form = "Forms.Ventas.ProductoVendidoCatalogoCForm", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 86140, ParentId = 86100, Name = "cpvnt_gven_remisionado", Label = "Remisionado" },
                new UIMenuElement { Id = 86141, ParentId = 86140, Name = "cpvnt_brem_historial", Label = "Historial", Form = "Forms.Ventas.RemisionadoForm", Assembly = "Jaeger.UI.CP.Beta",
                    LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status } },
                new UIMenuElement { Id = 86142, ParentId = 86140, Name = "adm_gcli_remxcobrar", Label = "Por Cobrar", Form = "Forms.Almacen.PT.RemisionadoPorCobrar", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 86143, ParentId = 86140, Name = "adm_gcli_remxpartida", Label = "Remisión Partidas", Form = "Forms.Almacen.PT.RemisionadoPartidaForm", Assembly = "Jaeger.UI.CP.Beta", LPermisos = new List<UIActionEnum>{ UIActionEnum.Exportar, UIActionEnum.Imprimir } },
                new UIMenuElement { Id = 86144, ParentId = 86140, Name = "adm_gcli_porcliente", Label = "Por Cliente", Form = "Forms.Almacen.PT.RemisionadoClienteForm", Assembly = "Jaeger.UI.CP.Beta", LPermisos = new List<UIActionEnum>{ UIActionEnum.Exportar, UIActionEnum.Imprimir } },

                new UIMenuElement { Id = 88000, ParentId = 0, Name = "TCCalidad", Label = "C. Calidad"},
                new UIMenuElement { Id = 88100, ParentId = 88000, Name = "ccal_grp_nconf", Label = "No Conformidades"},
                new UIMenuElement { Id = 88101, ParentId = 88100, Name = "ccal_gnco_new", Label = "No Conformidad"},
                new UIMenuElement { Id = 88102, ParentId = 88101, Name = "ccal_gnco_todo", Label = "Historial", Form = "Forms.CCalidad.NoConformidadesForm", Assembly = "Jaeger.UI.CP.Beta",
                     LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }},
                new UIMenuElement { Id = 88103, ParentId = 88101, Name = "ccal_gnco_proceso", Label = "En Proceso", Form = "Forms.CCalidad.NoConformidadProcesoForm", Assembly = "Jaeger.UI.CP.Beta",
                     LPermisos = new List<UIActionEnum> { UIActionEnum.Agregar, UIActionEnum.Editar, UIActionEnum.Cancelar, UIActionEnum.Exportar, UIActionEnum.Status }},

                new UIMenuElement { Id = 88104, ParentId = 88100, Name = "ccal_gnco_reporte", Label = "Reportes"},
                new UIMenuElement { Id = 88105, ParentId = 88104, Name = "ccal_gnco_reporte1", Label = "Reporte Anual", Form = "Forms.CCalidad.NoConformidadEvaluarReporteForm", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 88106, ParentId = 88104, Name = "ccal_gnco_reporte2", Label = "Reporte 2", Form = "Forms.CCalidad.CCalidadEvaluarReporteForm", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 88107, ParentId = 88104, Name = "ccal_gnco_reporte3", Label = "Reporte 3", Form = "Forms.CCalidad.RemisionesFiscalesForm", Assembly = "Jaeger.UI.CP.Beta"},

                new UIMenuElement { Id = 88108, ParentId = 88000, Name = "ccal_grp_prod", Label = "Producción"},
                new UIMenuElement { Id = 88109, ParentId = 88108, Name = "ccal_gprd_oprod", Label = "Ordenes", Form = "Forms.RemisionesFiscalesForm", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 88110, ParentId = 88108, Name = "ccal_gprd_facue", Label = "F. Acuerdo", Form = "Forms.RemisionesFiscalesForm", Assembly = "Jaeger.UI.CP.Beta"},
                new UIMenuElement { Id = 88111, ParentId = 88108, Name = "ccal_gprd_remis", Label = "Remisionado", Form = "Forms.RemisionesFiscalesForm", Assembly = "Jaeger.UI.CP.Beta"},

                new UIMenuElement { Id = 80001, ParentId = 0, Name = "TCPTools", Label = "Herramientas", Default = true, Rol = "*"},
                new UIMenuElement { Id = 80002, ParentId = 80001, Name = "cph_grp_configura", Label = "Configuración" },
                new UIMenuElement { Id = 80003, ParentId = 80002, Name = "cph_gConfigura_Empresa", Label = "Ajustes", Form = "Forms.Empresa.EmpresaForm" },
                new UIMenuElement { Id = 80004, ParentId = 80002, Name = "cph_gConfigura_avanzado", Label = "Avanzado" , Form = "Forms.Empresa.ConfiguracionForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 80005, ParentId = 80002, Name = "cph_gConfigura_usuarios", Label = "Usuarios", Form = "Forms.Parametros.UsuariosForm", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 80006, ParentId = 80002, Name = "cph_gConfigura_menu", Label = "Empresa", Form = "Forms.Empresa.Configuracion", Assembly = "Jaeger.UI.CP.Beta" },
                new UIMenuElement { Id = 80007, ParentId = 80002, Name = "cph_gConfigura_perfil", Label = "Perfiles", Form = "Forms.Profile.ProfileActiveForm", Assembly = "Jaeger.UI.CP.Beta" },

                new UIMenuElement { Id = 89991, ParentId = 80001, Name = "cpl_grp_ayuda", Label = "Ayuda", Default = true, Rol = "*", IsAvailable = true },
                new UIMenuElement { Id = 89992, ParentId = 80001, Name = "cpl_gayuda_acercace", Label = "Acerca de ...", Form = "Forms.AboutBoxForm", Assembly = "Jaeger.UI.CPLite", Default = true, Rol = "*", IsAvailable = true },

                new UIMenuElement { Id = 89993, ParentId = 80001, Name = "dsk_grp_theme", Label = "Tema", Default = true, Rol = "*" },
                new UIMenuElement { Id = 89994, ParentId = 10022, Name = "dsk_gtheme_2010Black", Label = "Office 2010 Black", Form = "Office2010Black", Default = true, Rol = "*" },
                new UIMenuElement { Id = 89995, ParentId = 10022, Name = "dsk_gtheme_2010Blue", Label = "Office 2010 Blue", Form = "Office2010Blue", Default = true, Rol = "*" },
                new UIMenuElement { Id = 89996, ParentId = 10022, Name = "dsk_gtheme_2010Silver", Label = "Office 2010 Silver", Form = "Office2010Silver", Default = true, Rol = "*" }
            };
            return response;
        }

        public IEnumerable<UIMenuElement> GetNominaBeta() {
            throw new NotImplementedException();
        }

        public int Insert(UIMenuModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO UIMENU (UIMENU_ID, UIMENU_SBID, UIMENU_KEY, UIMENU_LABEL, UIMENU_TOOLTIP, UIMENU_FORM, UIMENU_ACTION) 
                                            VALUES(@UIMENU_ID,@UIMENU_SBID,@UIMENU_KEY,@UIMENU_LABEL,@UIMENU_TOOLTIP,@UIMENU_FORM,@UIMENU_ACTION) Returning UIMENU_ID"
            };
            sqlCommand.Parameters.AddWithValue("@UIMENU_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@UIMENU_SBID", item.ParentId);
            sqlCommand.Parameters.AddWithValue("@UIMENU_KEY", item.Name);
            sqlCommand.Parameters.AddWithValue("@UIMENU_LABEL", item.Label);
            sqlCommand.Parameters.AddWithValue("@UIMENU_TOOLTIP", item.ToolTipText);
            sqlCommand.Parameters.AddWithValue("@UIMENU_FORM", item.Form);
            sqlCommand.Parameters.AddWithValue("@UIMENU_ACTION", item.Permisos);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public UIMenuModel Save(UIMenuModel model) {
            if (model.Id >= 1000) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }

        public int Update(UIMenuModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE UIMENU SET UIMENU_SBID = @UIMENU_SBID, UIMENU_KEY = @UIMENU_KEY, UIMENU_LABEL = @UIMENU_LABEL, UIMENU_TOOLTIP = @UIMENU_TOOLTIP, UIMENU_FORM = @UIMENU_FORM, UIMENU_ACTION = @UIMENU_ACTION WHERE UIMENU_ID = @UIMENU_ID"
            };
            sqlCommand.Parameters.AddWithValue("@UIMENU_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@UIMENU_SBID", item.ParentId);
            sqlCommand.Parameters.AddWithValue("@UIMENU_KEY", item.Name);
            sqlCommand.Parameters.AddWithValue("@UIMENU_LABEL", item.Label);
            sqlCommand.Parameters.AddWithValue("@UIMENU_TOOLTIP", item.ToolTipText);
            sqlCommand.Parameters.AddWithValue("@UIMENU_FORM", item.Form);
            sqlCommand.Parameters.AddWithValue("@UIMENU_ACTION", item.Permisos);
            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
