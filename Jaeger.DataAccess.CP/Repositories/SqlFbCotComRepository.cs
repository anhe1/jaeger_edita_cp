﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbCotComRepository : RepositoryMaster<CotComModel>, ISqlCotComRepository {
        protected ISqlCotProRepository cotProRepository;
        public SqlFbCotComRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.cotProRepository = new SqlFbCotProRepository(configuracion);
        }

        public int Insert(CotComModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO cotcom (cotcom_id, cotcom_a, cotcom_cotprd_id, cotcom_prdcom_id, cotcom_lc_id, cotcom_cla, cotcom_nom, cotcom_can_ini, cotcom_can, cotcom_sec, cotcom_usu_fm, cotcom_can_prd, cotcom_can_com, cotcom_$_cen, cotcom_$_pro, cotcom_$_mp, cotcom_$_cns, cotcom_t_max, cotcom_fm, cotcom_com_id) 
                			VALUES (@cotcom_id, @cotcom_a, @cotcom_cotprd_id, @cotcom_prdcom_id, @cotcom_lc_id, @cotcom_cla, @cotcom_nom, @cotcom_can_ini, @cotcom_can, @cotcom_sec, @cotcom_usu_fm, @cotcom_can_prd, @cotcom_can_com, @cotcom_$_cen, @cotcom_$_pro, @cotcom_$_mp, @cotcom_$_cns, @cotcom_t_max, @cotcom_fm, @cotcom_com_id)"
            };
            item.IdCotCom = this.Max("COTCOM_ID");
            sqlCommand.Parameters.AddWithValue("@cotcom_id", item.IdCotCom);
            sqlCommand.Parameters.AddWithValue("@cotcom_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotcom_cotprd_id", item.COTCOM_COTPRD_ID);
            sqlCommand.Parameters.AddWithValue("@cotcom_prdcom_id", item.COTCOM_PRDCOM_ID);
            sqlCommand.Parameters.AddWithValue("@cotcom_lc_id", item.COTCOM_LC_ID);
            sqlCommand.Parameters.AddWithValue("@cotcom_cla", item.COTCOM_CLA);
            sqlCommand.Parameters.AddWithValue("@cotcom_nom", item.Denominacion);
            sqlCommand.Parameters.AddWithValue("@cotcom_can_ini", item.CantidadInicial);
            sqlCommand.Parameters.AddWithValue("@cotcom_can", item.CantidadP);
            sqlCommand.Parameters.AddWithValue("@cotcom_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@cotcom_usu_fm", item.COTCOM_USU_FM);
            sqlCommand.Parameters.AddWithValue("@cotcom_can_prd", item.COTCOM_CAN_PRD);
            sqlCommand.Parameters.AddWithValue("@cotcom_can_com", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@cotcom_$_cen", item.Centro);
            sqlCommand.Parameters.AddWithValue("@cotcom_$_pro", item.ManoObra);
            sqlCommand.Parameters.AddWithValue("@cotcom_$_mp", item.MateriaPrima);
            sqlCommand.Parameters.AddWithValue("@cotcom_$_cns", item.Consumibles);
            sqlCommand.Parameters.AddWithValue("@cotcom_t_max", item.Minutos);
            sqlCommand.Parameters.AddWithValue("@cotcom_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@cotcom_com_id", item.COTCOM_COM_ID);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdCotCom;
            return 0;
        }

        public int Update(CotComModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotcom 
                			SET cotcom_a, cotcom_cotprd_id = @cotcom_cotprd_id, cotcom_prdcom_id = @cotcom_prdcom_id, cotcom_lc_id = @cotcom_lc_id, cotcom_cla = @cotcom_cla, cotcom_nom = @cotcom_nom, cotcom_can_ini = @cotcom_can_ini, cotcom_can = @cotcom_can, cotcom_sec = @cotcom_sec, cotcom_usu_fm = @cotcom_usu_fm, cotcom_can_prd = @cotcom_can_prd, cotcom_can_com = @cotcom_can_com, cotcom_$_cen = @cotcom_$_cen, cotcom_$_pro = @cotcom_$_pro, cotcom_$_mp = @cotcom_$_mp, cotcom_$_cns = @cotcom_$_cns, cotcom_t_max = @cotcom_t_max, cotcom_fm = @cotcom_fm, cotcom_com_id = @cotcom_com_id
                			WHERE cotcom_id = @cotcom_id;"
            };
            sqlCommand.Parameters.AddWithValue("@cotcom_id", item.IdCotCom);
            sqlCommand.Parameters.AddWithValue("@cotcom_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotcom_cotprd_id", item.COTCOM_COTPRD_ID);
            sqlCommand.Parameters.AddWithValue("@cotcom_prdcom_id", item.COTCOM_PRDCOM_ID);
            sqlCommand.Parameters.AddWithValue("@cotcom_lc_id", item.COTCOM_LC_ID);
            sqlCommand.Parameters.AddWithValue("@cotcom_cla", item.COTCOM_CLA);
            sqlCommand.Parameters.AddWithValue("@cotcom_nom", item.Denominacion);
            sqlCommand.Parameters.AddWithValue("@cotcom_can_ini", item.CantidadInicial);
            sqlCommand.Parameters.AddWithValue("@cotcom_can", item.CantidadP);
            sqlCommand.Parameters.AddWithValue("@cotcom_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@cotcom_usu_fm", item.COTCOM_USU_FM);
            sqlCommand.Parameters.AddWithValue("@cotcom_can_prd", item.COTCOM_CAN_PRD);
            sqlCommand.Parameters.AddWithValue("@cotcom_can_com", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@cotcom_$_cen", item.Centro);
            sqlCommand.Parameters.AddWithValue("@cotcom_$_pro", item.ManoObra);
            sqlCommand.Parameters.AddWithValue("@cotcom_$_mp", item.MateriaPrima);
            sqlCommand.Parameters.AddWithValue("@cotcom_$_cns", item.Consumibles);
            sqlCommand.Parameters.AddWithValue("@cotcom_t_max", item.Minutos);
            sqlCommand.Parameters.AddWithValue("@cotcom_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@cotcom_com_id", item.COTCOM_COM_ID);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotcom SET cotcom_a = 0 WHERE cotcom_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CotComModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "cotcom", "cotcom")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotComModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<CotComModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "cotcom", "cotcom")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotComModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<CotComDetailModel> GetList(int[] index) {
            if (index.Length > 0) {
                var sqlLinea = new FbCommand {
                    CommandText = @"select * from COTCOM where COTCOM_A > 0 and COTCOM_COTPRD_ID in (@index) order by COTCOM_sec"
                };

                sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", index.ToArray()));
                var tabla = this.ExecuteReader(sqlLinea);
                var mapper = new DataNamesMapper<CotComDetailModel>();
                var result = mapper.Map(tabla).ToList();
                var allids = result.Select(it => it.IdCotCom).ToArray();
                var lineaProduccion = this.cotProRepository.GetListBy(allids);

                for (int i = 0; i < result.Count; i++) {
                    result[i].LineaProduccion = new BindingList<CotProDetailModel>(lineaProduccion.Where(it => it.COTPRO_COTCOM_ID == result[i].IdCotCom).ToList());
                }

                return result;
            }
            return new List<CotComDetailModel>();
        }

        public IEnumerable<CotComDetailModelView> GetList1(int[] index) {
            if (index.Length > 0) {
                var sqlLinea = new FbCommand {
                    CommandText = @"select CotCom.*,
                                    Lc1_Nom
                                    from COTCOM , LC1
                                    where COTCOM_A > 0 
                                    and CotCom_Com_ID = LC1_ID
                                    and COTCOM_COTPRD_ID in (@index) 
                                    order by COTCOM_sec"
                };

                sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", index.ToArray()));
                var tabla = this.ExecuteReader(sqlLinea);
                var mapper = new DataNamesMapper<CotComDetailModelView>();
                var result = mapper.Map(tabla).ToList();
                var allids = result.Select(it => it.IdCotCom).ToArray();
                var lineaProduccion = this.cotProRepository.GetList1(allids);

                for (int i = 0; i < result.Count; i++) {
                    result[i].LineaProduccion = new BindingList<CotProDetailModelView>(lineaProduccion.Where(it => it.COTPRO_COTCOM_ID == result[i].IdCotCom).ToList());
                }

                return result;
            }
            return new List<CotComDetailModelView>();
        }
    }
}
