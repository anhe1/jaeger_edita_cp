﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// Materia prima (PRDMP)
    /// </summary>
    public class SqlFbPrdMpRepository : RepositoryMaster<PrdMpModel>, ISqlPrdMpRepository {
        public SqlFbPrdMpRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE prdmp SET prdmp_a = 0 WHERE prdmp_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PrdMpModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from prdmp where prdmp_id =@id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PrdMpModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<PrdMpModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "select * from prdmp where prdmp_a > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PrdMpModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// Materia Prima (PRDMP )
        /// </summary>
        public IEnumerable<PrdMpDetailModel> GetList(int[] index) {
            var sqlLinea = new FbCommand {
                CommandText = @"select * from PRDMP where PRDMP_A > 0 and PRDMP_PRDPRO_ID in (@index) order by PRDMP_ID"
            };

            sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", index.ToArray()));
            var tabla = this.ExecuteReader(sqlLinea);
            var mapper = new DataNamesMapper<PrdMpDetailModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// Materia Prima (PRDMP )
        /// </summary>
        public IEnumerable<PrdMpDetailModelView> GetList1(int[] index) {
            var sqlLinea = new FbCommand {
                CommandText = @"select PRDMP.*,
                                Com3_Cod, Com3_N1, Com3_N2, Com3_N3,
                                ComMod_Mod, ComMod_nom, ComMod_Mar, ComMod_UniNom, ComMod_$Uni, ComMod_Mul, ComMod_Com_ID
                                from PRDMP , Com3, ComMod
                                where PRDMP_A > 0 
                                and PRDMP_Com_ID = Com3_ID
                                and PRDMP_MP4_ID = ComMod_ID
                                and PRDMP_PRDPRO_ID in (@index) 
                                order by PRDMP_ID"
            };

            sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", index.ToArray()));
            var tabla = this.ExecuteReader(sqlLinea);
            var mapper = new DataNamesMapper<PrdMpDetailModelView>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(PrdMpModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO prdmp (prdmp_id, prdmp_a, prdmp_prdpro_id, prdmp_mp_id, prdmp_mp4_id, prdmp_can, prdmp_mul, prdmp_lib, prdmp_fyv, prdmp_tra, prdmp_trb, prdmp_trc, prdmp_tpa, prdmp_tpb, prdmp_tpc, prdmp_mra, prdmp_mrb, prdmp_mpa, prdmp_mpb, prdmp_usu_fm, prdmp_fm, prdmp_cot_id, prdmp_com_id) 
                			VALUES (@prdmp_id, @prdmp_a, @prdmp_prdpro_id, @prdmp_mp_id, @prdmp_mp4_id, @prdmp_can, @prdmp_mul, @prdmp_lib, @prdmp_fyv, @prdmp_tra, @prdmp_trb, @prdmp_trc, @prdmp_tpa, @prdmp_tpb, @prdmp_tpc, @prdmp_mra, @prdmp_mrb, @prdmp_mpa, @prdmp_mpb, @prdmp_usu_fm, @prdmp_fm, @prdmp_cot_id, @prdmp_com_id)"
            };

            item.IdPrdMP = this.Max("PRDMP_ID");
            sqlCommand.Parameters.AddWithValue("@prdmp_id", item.IdPrdMP);
            sqlCommand.Parameters.AddWithValue("@prdmp_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@prdmp_prdpro_id", item.IdPrdPro);
            sqlCommand.Parameters.AddWithValue("@prdmp_mp_id", item.PRDMP_MP_ID);
            sqlCommand.Parameters.AddWithValue("@prdmp_mp4_id", item.PRDMP_MP4_ID);
            sqlCommand.Parameters.AddWithValue("@prdmp_can", item.Factor);
            sqlCommand.Parameters.AddWithValue("@prdmp_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@prdmp_lib", item.PRDMP_LIB);
            sqlCommand.Parameters.AddWithValue("@prdmp_fyv", item.PRDMP_FYV);
            sqlCommand.Parameters.AddWithValue("@prdmp_tra", item.PRDMP_TRA);
            sqlCommand.Parameters.AddWithValue("@prdmp_trb", item.PRDMP_TRB);
            sqlCommand.Parameters.AddWithValue("@prdmp_trc", item.PRDMP_TRC);
            sqlCommand.Parameters.AddWithValue("@prdmp_tpa", item.PRDMP_TPA);
            sqlCommand.Parameters.AddWithValue("@prdmp_tpb", item.PRDMP_TPB);
            sqlCommand.Parameters.AddWithValue("@prdmp_tpc", item.PRDMP_TPC);
            sqlCommand.Parameters.AddWithValue("@prdmp_mra", item.PRDMP_MRA);
            sqlCommand.Parameters.AddWithValue("@prdmp_mrb", item.PRDMP_MRB);
            sqlCommand.Parameters.AddWithValue("@prdmp_mpa", item.PRDMP_MPA);
            sqlCommand.Parameters.AddWithValue("@prdmp_mpb", item.PRDMP_MPB);
            sqlCommand.Parameters.AddWithValue("@prdmp_usu_fm", item.PRDMP_USU_FM);
            sqlCommand.Parameters.AddWithValue("@prdmp_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@prdmp_cot_id", item.PRDMP_COT_ID);
            sqlCommand.Parameters.AddWithValue("@prdmp_com_id", item.PRDMP_COM_ID);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdPrdMP;
            return 0;
        }

        public int Update(PrdMpModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE prdmp 
                			SET prdmp_a = @prdmp_a, prdmp_prdpro_id = @prdmp_prdpro_id, prdmp_mp_id = @prdmp_mp_id, prdmp_mp4_id = @prdmp_mp4_id, prdmp_can = @prdmp_can, prdmp_mul = @prdmp_mul, prdmp_lib = @prdmp_lib, prdmp_fyv = @prdmp_fyv, prdmp_tra = @prdmp_tra, prdmp_trb = @prdmp_trb, prdmp_trc = @prdmp_trc, prdmp_tpa = @prdmp_tpa, prdmp_tpb = @prdmp_tpb, prdmp_tpc = @prdmp_tpc, prdmp_mra = @prdmp_mra, prdmp_mrb = @prdmp_mrb, prdmp_mpa = @prdmp_mpa, prdmp_mpb = @prdmp_mpb, prdmp_usu_fm = @prdmp_usu_fm, prdmp_fm = @prdmp_fm, prdmp_cot_id = @prdmp_cot_id, prdmp_com_id = @prdmp_com_id 
                			WHERE prdmp_id = @prdmp_id;"
            };
            sqlCommand.Parameters.AddWithValue("@prdmp_id", item.IdPrdMP);
            sqlCommand.Parameters.AddWithValue("@prdmp_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@prdmp_prdpro_id", item.IdPrdPro);
            sqlCommand.Parameters.AddWithValue("@prdmp_mp_id", item.PRDMP_MP_ID);
            sqlCommand.Parameters.AddWithValue("@prdmp_mp4_id", item.PRDMP_MP4_ID);
            sqlCommand.Parameters.AddWithValue("@prdmp_can", item.Factor);
            sqlCommand.Parameters.AddWithValue("@prdmp_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@prdmp_lib", item.PRDMP_LIB);
            sqlCommand.Parameters.AddWithValue("@prdmp_fyv", item.PRDMP_FYV);
            sqlCommand.Parameters.AddWithValue("@prdmp_tra", item.PRDMP_TRA);
            sqlCommand.Parameters.AddWithValue("@prdmp_trb", item.PRDMP_TRB);
            sqlCommand.Parameters.AddWithValue("@prdmp_trc", item.PRDMP_TRC);
            sqlCommand.Parameters.AddWithValue("@prdmp_tpa", item.PRDMP_TPA);
            sqlCommand.Parameters.AddWithValue("@prdmp_tpb", item.PRDMP_TPB);
            sqlCommand.Parameters.AddWithValue("@prdmp_tpc", item.PRDMP_TPC);
            sqlCommand.Parameters.AddWithValue("@prdmp_mra", item.PRDMP_MRA);
            sqlCommand.Parameters.AddWithValue("@prdmp_mrb", item.PRDMP_MRB);
            sqlCommand.Parameters.AddWithValue("@prdmp_mpa", item.PRDMP_MPA);
            sqlCommand.Parameters.AddWithValue("@prdmp_mpb", item.PRDMP_MPB);
            sqlCommand.Parameters.AddWithValue("@prdmp_usu_fm", item.PRDMP_USU_FM);
            sqlCommand.Parameters.AddWithValue("@prdmp_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@prdmp_cot_id", item.PRDMP_COT_ID);
            sqlCommand.Parameters.AddWithValue("@prdmp_com_id", item.PRDMP_COM_ID);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
