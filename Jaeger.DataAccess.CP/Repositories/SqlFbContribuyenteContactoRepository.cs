﻿/// purpose: directorio de contactos del directorio
/// develop: ANHE1 28092020 11:14
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Contribuyentes.Contracts;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de contactos del contribuyente (tabla DIRCON)
    /// </summary>
    public class SqlFbContribuyenteContactoRepository : RepositoryMaster<ContribuyenteContactoModel>, ISqlContribuyenteContactoRepository {
        protected ISqlDirConTRepository dirConTRepository;

        public SqlFbContribuyenteContactoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.dirConTRepository = new SqlFbDirConTRepository(configuracion);
        }

        public int Insert(ContribuyenteContactoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO dircon (dircon_id, dircon_a, dircon_con, dircon_dep, dircon_pue, dircon_obs, dircon_dir_id, dircon_fm) 
                			VALUES (@dircon_id, @dircon_a, @dircon_con, @dircon_dep, @dircon_pue, @dircon_obs, @dircon_dir_id, @dircon_fm)"
            };

            item.IdContacto = this.Max("DIRCON_ID");
            sqlCommand.Parameters.AddWithValue("@dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@dircon_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@dircon_con", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@dircon_dep", item.Departamento);
            sqlCommand.Parameters.AddWithValue("@dircon_pue", item.Puesto);
            sqlCommand.Parameters.AddWithValue("@dircon_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@dircon_dir_id", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@dircon_fm", item.FechaModifica);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdContacto;
            return 0;
        }

        public int Update(ContribuyenteContactoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE dircon 
                			SET dircon_a = @dircon_a, dircon_con = @dircon_con, dircon_dep = @dircon_dep, dircon_pue = @dircon_pue, dircon_obs = @dircon_obs, dircon_dir_id = @dircon_dir_id, dircon_fm = @dircon_fm 
                			WHERE dircon_id = @dircon_id;"
            };

            sqlCommand.Parameters.AddWithValue("@dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@dircon_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@dircon_con", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@dircon_dep", item.Departamento);
            sqlCommand.Parameters.AddWithValue("@dircon_pue", item.Puesto);
            sqlCommand.Parameters.AddWithValue("@dircon_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@dircon_dir_id", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@dircon_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE dircon SET dircon_a = 0 WHERE dircon_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ContribuyenteContactoModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "dircon", "dircon")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ContribuyenteContactoModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ContribuyenteContactoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "dircon", "dircon")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ContribuyenteContactoModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de contacto relacionados a un indice (dircon_dir_id)
        /// </summary>
        /// <param name="index">indice de relacion del directorio (dircon_dir_id)</param>
        /// <param name="onlyActive">solo registros activos</param>
        //public IEnumerable<ContribuyenteContactoModel> GetList(int index, bool onlyActive) {
        //    return this.GetList(new int[] { index }, onlyActive);
        //}

        /// <summary>
        /// obtener listado de contacto relacionados a un indice (dircon_dir_id)
        /// </summary>
        /// <param name="index">indice de relacion del directorio (dircon_dir_id)</param>
        /// <param name="onlyActive">solo registros activos</param>
        //public IEnumerable<ContribuyenteContactoDetailModel> GetList(int[] indexs, bool onlyActive) {
        //    var sqlCommand = new FbCommand {
        //        CommandText = "select dircon.* from dircon where @onlyActive dircon_dir_id in (@indexs);"
        //    };

        //    if (indexs.Length > 0) {
        //        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", string.Join(",", indexs));
                
        //        if (onlyActive) {
        //            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@onlyActive", "dircon_a > 0 and");
        //        }
        //        else {
        //            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@onlyActive", "");
        //        }

        //        var tabla = this.ExecuteReader(sqlCommand);
        //        var mapper = new DataNamesMapper<ContribuyenteContactoDetailModel>();
        //        var result = mapper.Map(tabla).ToList();
        //        var ids = result.Select(it => it.IdContacto).ToArray();
        //        var telefonos = this.dirConTRepository.GetList(ids, true);

        //        for (int i = 0; i < result.Count; i++) {
        //            result[i].Telefonos = new System.ComponentModel.BindingList<ContribuyenteContactoTelefono>(telefonos.Where(it => it.IdContacto == result[i].IdContacto).ToList());
        //        }
        //        return result;
        //    }
        //    return new List<ContribuyenteContactoDetailModel>();
        //}

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT DIRCON.* FROM DIRCON @wcondiciones;"
            };

            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals));
        }
    }
}
