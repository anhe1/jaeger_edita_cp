﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbProcesoRepository : RepositoryMaster<ProcesoModel>, ISqlProcesoRepository {
        protected ISqlProcesoEtapaRepository procesoEtapaRepository;
        protected ISqlCheckListRepository checkListrepository;
        protected ISqlProMoRepository proMoRepository;
        protected ISqlProMpRepository proMpRepository;

        public SqlFbProcesoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.procesoEtapaRepository = new SqlFbProcesoEtapaRepository(configuracion);
            this.checkListrepository = new SqlFbCheckListRepository(configuracion);
            this.proMoRepository = new SqlFbProMoRepository(configuracion);
            this.proMpRepository = new SqlFbProMpRepository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp3 SET edp3_a = 0 WHERE edp3_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ProcesoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from edp3 where edp3_id =@id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ProcesoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from EDP3 where EDP3_A > 0 order by EDP3_nom"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de procesos relacionados a un centro, incluye etapas, materia prima, mano de obra
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        /// <param name="idCentros">array de indices</param>
        public IEnumerable<ProcesoDetailModel> GetList(bool onlyActive, int[] idCentros) {
            var sqlCommand = new FbCommand {
                CommandText = @"select Edp3.* 
                                from EDP3 
                                where EDP3_A > 0 
                                order by EDP3_sec"
            };

            if (idCentros.Count() > 0) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", "EDP3_EDP2_ID in (@index)");
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", "");
            }

            if (onlyActive) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@active", "and EDP3_A > 0");
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@active", "");
            }

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", string.Join(",", idCentros));

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoDetailModel>();
            var result = mapper.Map(tabla).ToList();
            var allids = result.Select(it => it.IdProceso).ToArray();
            
            var etapas = this.procesoEtapaRepository.GetList(onlyActive, allids);
            var checkList = this.checkListrepository.GetList(new int[] { 0 });
            var recursosMO = this.proMoRepository.GetList();
            var recursosMP = this.proMpRepository.GetList(true, allids);

            for (int i = 0; i < result.Count; i++) {
                result[i].Etapas = new BindingList<ProcesoEtapaDetailModel>(etapas.Where(it => it.IdEdp3 == result[i].IdProceso).ToList());
                result[i].CheckList = new BindingList<CheckListDetailModel>(checkList.Where(it => it.IdProceso == result[i].IdProceso).ToList());
                result[i].RecursosMO = new BindingList<ProcesoManoObraModel>(recursosMO.Where(it => it.IdProceso == result[i].IdProceso).ToList());
                result[i].RecursosMP = new BindingList<ProMpDetailModelView>(recursosMP.Where(it => it.PROMP_PRO_ID == result[i].IdProceso).ToList());
            }

            return result;
        }

        /// <summary>
        /// obtener listado de procesos, incluye Area, Departamento, Seccion, Centro, Proceso
        /// </summary>
        public IEnumerable<ProcesoModelView> GetViewList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select EDP3_ID, EDP3_A, EDP1_are, EDP1_nom, EDP2_sec, EDP2_nom, EDP3_nom, EDP2_val_min 
                                from edp3, edp1, edp2
                                where Edp3_Edp2_ID = Edp2_ID and Edp2_Edp1_ID = Edp1_ID and EDP3_A > 0
                                order by EDP1_nom, EDP2_sec, EDP2_nom, EDP3_nom"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoModelView>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(ProcesoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO edp3 (edp3_id, edp3_a, edp3_edp2_id, edp3_nom, edp3_sec, edp3_nt, edp3_mo, edp3_cen, edp3_usu_fm, edp3_fm, edp3_canini, edp3_canext) 
                			VALUES (@edp3_id, @edp3_a, @edp3_edp2_id, @edp3_nom, @edp3_sec, @edp3_nt, @edp3_mo, @edp3_cen, @edp3_usu_fm, @edp3_fm, @edp3_canini, @edp3_canext)"
            };
            item.IdProceso = this.Max("EDP3_ID");
            sqlCommand.Parameters.AddWithValue("@edp3_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@edp3_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp3_edp2_id", item.IdCentro);
            sqlCommand.Parameters.AddWithValue("@edp3_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@edp3_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@edp3_nt", item.Edp3Nt);
            sqlCommand.Parameters.AddWithValue("@edp3_mo", item.ManoObra);
            sqlCommand.Parameters.AddWithValue("@edp3_cen", item.ValorHora);
            sqlCommand.Parameters.AddWithValue("@edp3_usu_fm", item.Edp3UsuFm);
            sqlCommand.Parameters.AddWithValue("@edp3_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@edp3_canini", item.CantidadInicial);
            sqlCommand.Parameters.AddWithValue("@edp3_canext", item.CantidadExt);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdProceso;
            return 0;
        }

        public int Update(ProcesoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp3 
                			SET edp3_a = @edp3_a, edp3_edp2_id = @edp3_edp2_id, edp3_nom = @edp3_nom, edp3_sec = @edp3_sec, edp3_nt = @edp3_nt, edp3_mo = @edp3_mo, edp3_cen = @edp3_cen, edp3_usu_fm = @edp3_usu_fm, edp3_fm = @edp3_fm, edp3_canini = @edp3_canini, edp3_canext = @edp3_canext 
                			WHERE edp3_id = @edp3_id;"
            };

            sqlCommand.Parameters.AddWithValue("@edp3_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@edp3_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp3_edp2_id", item.IdCentro);
            sqlCommand.Parameters.AddWithValue("@edp3_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@edp3_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@edp3_nt", item.Edp3Nt);
            sqlCommand.Parameters.AddWithValue("@edp3_mo", item.ManoObra);
            sqlCommand.Parameters.AddWithValue("@edp3_cen", item.ValorHora);
            sqlCommand.Parameters.AddWithValue("@edp3_usu_fm", item.Edp3UsuFm);
            sqlCommand.Parameters.AddWithValue("@edp3_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@edp3_canini", item.CantidadInicial);
            sqlCommand.Parameters.AddWithValue("@edp3_canext", item.CantidadExt);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
