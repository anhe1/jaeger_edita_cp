﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// Costos (COTPRO)
    /// </summary>
    public class SqlFbCotProRepository : RepositoryMaster<CotProModel>, ISqlCotProRepository {
        protected ISqlCotMatRepository cotMatRepository;
        protected ISqlEdp4PueRepository edp4PueRepository;
        protected ISqlProcesoEtapaRepository procesoEtapaRepository;
        public SqlFbCotProRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.cotMatRepository = new SqlFbCotMatRepository(configuracion);
            this.edp4PueRepository = new SqlFbEdp4PueRepository(configuracion);
            this.procesoEtapaRepository = new SqlFbProcesoEtapaRepository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotpro SET cotpro_a = 0 WHERE cotpro_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CotProModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM cotpro WHERE cotpro_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotProModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<CotProModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM cotpro WHERE cotpro_a > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotProModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// Linea de produccion
        /// </summary>
        /// <param name="index">indices COTPRO_COTCOM_ID</param>
        public IEnumerable<CotProDetailModel> GetListBy(int[] index) {
            if (index.Length > 0) {
                var sqlLinea = new FbCommand {
                    CommandText = @"select * from COTPRO where COTPRO_A > 0 and COTPRO_COTCOM_ID in (@index) order by COTPRO_Sec"
                };

                sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", index.ToArray()));
                var tabla = this.ExecuteReader(sqlLinea);
                var mapper = new DataNamesMapper<CotProDetailModel>();
                var result = mapper.Map(tabla).ToList();
                var allids = result.Select(it => it.COTPRO_ID).ToArray();
                var mp = this.cotMatRepository.GetList(allids);
                var allids2 = result.Select(it => it.COTPRO_PRO_ID).ToArray();
                var subprocesos = this.procesoEtapaRepository.GetList(true, allids2);//this.LineaProduccionSubProcesos(allids2);
                for (int i = 0; i < result.Count; i++) {
                    result[i].MateriaPrima = new BindingList<CotMatDetailModel>(mp.Where(it => it.COTMAT_COTPRO_ID == result[i].COTPRO_ID).ToList());
                    result[i].SubProcesos = new BindingList<ProcesoEtapaDetailModel>(subprocesos.Where(it => it.IdEdp3 == result[i].COTPRO_PRO_ID).ToList());
                }

                return result;
            }
            return new List<CotProDetailModel>();
        }

        /// <summary>
        /// Linea de produccion
        /// </summary>
        /// <param name="index">indices COTPRO_COTCOM_ID</param>
        public IEnumerable<CotProDetailModelView> GetList1(int[] index) {
            if (index.Length > 0) {
                var sqlLinea = new FbCommand {
                    CommandText = @"select CotPro.* ,
                                    EDP1_nom, EDP2_sec, EDP2_nom, EDP3_nom, EDP2_val_min
                                    from COTPRO , Edp3, Edp2, Edp1
                                    where COTPRO_A > 0 
                                    and Edp3_Edp2_ID = Edp2_ID
                                    and Edp2_Edp1_ID = Edp1_ID
                                    and EDP3_ID = COTPRO_PRO_ID
                                    and COTPRO_COTCOM_ID in (@index) 
                                    order by COTPRO_Sec"
                };

                sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", index.ToArray()));
                var tabla = this.ExecuteReader(sqlLinea);
                var mapper = new DataNamesMapper<CotProDetailModelView>();
                var result = mapper.Map(tabla).ToList();
                var allids = result.Select(it => it.COTPRO_ID).ToArray();
                var mp = this.cotMatRepository.GetList1(allids);
                var allids2 = result.Select(it => it.COTPRO_PRO_ID).ToArray();
                var subprocesos = this.procesoEtapaRepository.GetList(true, allids2);  //this.LineaProduccionSubProcesos(allids2);
                for (int i = 0; i < result.Count; i++) {
                    result[i].MateriaPrima = new BindingList<CotMatDetailModelView>(mp.Where(it => it.COTMAT_COTPRO_ID == result[i].COTPRO_ID).ToList());
                    result[i].SubProcesos = new BindingList<ProcesoEtapaDetailModel>(subprocesos.Where(it => it.IdEdp3 == result[i].COTPRO_PRO_ID).ToList());
                }

                return result;
            }
            return new List<CotProDetailModelView>();
        }

        public int Insert(CotProModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO cotpro (cotpro_id, cotpro_a, cotpro_prdpro_id, cotpro_cotprd_id, cotpro_pro_id, cotpro_cotcom_id, cotpro_sec, cotpro_can_vp, cotpro_can_cp, cotpro_can_com, cotpro_can_pro, cotpro_can_pros, cotpro_can_prot, cotpro_t_max, cotpro_nt, cotpro_$_cen, cotpro_$_pro, cotpro_$_mat, cotpro_$_cns, cotpro_usu_fm, cotpro_fm) 
                			VALUES (@cotpro_id, @cotpro_a, @cotpro_prdpro_id, @cotpro_cotprd_id, @cotpro_pro_id, @cotpro_cotcom_id, @cotpro_sec, @cotpro_can_vp, @cotpro_can_cp, @cotpro_can_com, @cotpro_can_pro, @cotpro_can_pros, @cotpro_can_prot, @cotpro_t_max, @cotpro_nt, @cotpro_$_cen, @cotpro_$_pro, @cotpro_$_mat, @cotpro_$_cns, @cotpro_usu_fm, @cotpro_fm)"
            };
            item.COTPRO_ID = this.Max("COTPRO_ID");
            sqlCommand.Parameters.AddWithValue("@cotpro_id", item.COTPRO_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_a", item.COTPRO_A);
            sqlCommand.Parameters.AddWithValue("@cotpro_prdpro_id", item.COTPRO_PRDPRO_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_cotprd_id", item.COTPRO_COTPRD_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_pro_id", item.COTPRO_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_cotcom_id", item.COTPRO_COTCOM_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_vp", item.COTPRO_CAN_VP);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_cp", item.CantidadProceso);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_com", item.COTPRO_CAN_COM);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_pro", item.COTPRO_CAN_PRO);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_pros", item.COTPRO_CAN_PROS);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_prot", item.CantidadTotal);
            sqlCommand.Parameters.AddWithValue("@cotpro_t_max", item.Minutos);
            sqlCommand.Parameters.AddWithValue("@cotpro_nt", item.OperadoresEstimados);
            sqlCommand.Parameters.AddWithValue("@cotpro_$_cen", item.Centro1);
            sqlCommand.Parameters.AddWithValue("@cotpro_$_pro", item.ManoObra);
            sqlCommand.Parameters.AddWithValue("@cotpro_$_mat", item.MateriaPrima1);
            sqlCommand.Parameters.AddWithValue("@cotpro_$_cns", item.Consumibles);
            sqlCommand.Parameters.AddWithValue("@cotpro_usu_fm", item.COTPRO_USU_FM);
            sqlCommand.Parameters.AddWithValue("@cotpro_fm", item.COTPRO_FM);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.COTPRO_ID;
            return 0;
        }

        public int Update(CotProModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotpro 
                			SET cotpro_a = @cotpro_a, cotpro_prdpro_id = @cotpro_prdpro_id, cotpro_cotprd_id = @cotpro_cotprd_id, cotpro_pro_id = @cotpro_pro_id, cotpro_cotcom_id = @cotpro_cotcom_id, cotpro_sec = @cotpro_sec, cotpro_can_vp = @cotpro_can_vp, cotpro_can_cp = @cotpro_can_cp, cotpro_can_com = @cotpro_can_com, cotpro_can_pro = @cotpro_can_pro, cotpro_can_pros = @cotpro_can_pros, cotpro_can_prot = @cotpro_can_prot, cotpro_t_max = @cotpro_t_max, cotpro_nt = @cotpro_nt, cotpro_$_cen = @cotpro_$_cen, cotpro_$_pro = @cotpro_$_pro, cotpro_$_mat = @cotpro_$_mat, cotpro_$_cns = @cotpro_$_cns, cotpro_usu_fm = @cotpro_usu_fm, cotpro_fm = @cotpro_fm 
                			WHERE cotpro_id = @cotpro_id;"
            };
            sqlCommand.Parameters.AddWithValue("@cotpro_id", item.COTPRO_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_a", item.COTPRO_A);
            sqlCommand.Parameters.AddWithValue("@cotpro_prdpro_id", item.COTPRO_PRDPRO_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_cotprd_id", item.COTPRO_COTPRD_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_pro_id", item.COTPRO_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_cotcom_id", item.COTPRO_COTCOM_ID);
            sqlCommand.Parameters.AddWithValue("@cotpro_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_vp", item.COTPRO_CAN_VP);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_cp", item.CantidadProceso);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_com", item.COTPRO_CAN_COM);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_pro", item.COTPRO_CAN_PRO);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_pros", item.COTPRO_CAN_PROS);
            sqlCommand.Parameters.AddWithValue("@cotpro_can_prot", item.CantidadTotal);
            sqlCommand.Parameters.AddWithValue("@cotpro_t_max", item.Minutos);
            sqlCommand.Parameters.AddWithValue("@cotpro_nt", item.OperadoresEstimados);
            sqlCommand.Parameters.AddWithValue("@cotpro_$_cen", item.Centro1);
            sqlCommand.Parameters.AddWithValue("@cotpro_$_pro", item.ManoObra);
            sqlCommand.Parameters.AddWithValue("@cotpro_$_mat", item.MateriaPrima1);
            sqlCommand.Parameters.AddWithValue("@cotpro_$_cns", item.Consumibles);
            sqlCommand.Parameters.AddWithValue("@cotpro_usu_fm", item.COTPRO_USU_FM);
            sqlCommand.Parameters.AddWithValue("@cotpro_fm", item.COTPRO_FM);
            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
