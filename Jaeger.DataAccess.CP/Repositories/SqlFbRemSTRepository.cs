﻿/// purpose: repositorio de status de remision (REMST)
/// develop: ANHE1 27092020 0121
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Remisionado.Contracts;
using Jaeger.Domain.CP.Remisionado.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using System.Data.SqlClient;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de status de remision (REMST)
    /// </summary>
    public class SqlFbRemSTRepository : RepositoryMaster<RemisionSTModel>, ISqlRemisionSTRepository {

        public SqlFbRemSTRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Insert(RemisionSTModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO remst (remst_id, remst_a, remst_sta, remst_sec, remst_sum, remst_fm) 
                    			VALUES (@remst_id, @remst_a, @remst_sta, @remst_sec, @remst_sum, @remst_fm)"
            };
            item.IdRemST = this.Max("REMST_ID");
            sqlCommand.Parameters.AddWithValue("@remst_id", item.IdRemST);
            sqlCommand.Parameters.AddWithValue("@remst_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@remst_sta", item.Status);
            sqlCommand.Parameters.AddWithValue("@remst_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@remst_sum", item.REMST_SUM);
            sqlCommand.Parameters.AddWithValue("@remst_fm", item.FechaModifica);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdRemST;
            return 0;
        }

        public int Update(RemisionSTModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE remst 
                    			SET remst_a = @remst_a, remst_sta = @remst_sta, remst_sec = @remst_sec, remst_sum = @remst_sum, remst_fm = @remst_fm 
                    			WHERE remst_id = @remst_id;"
            };
            sqlCommand.Parameters.AddWithValue("@remst_id", item.IdRemST);
            sqlCommand.Parameters.AddWithValue("@remst_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@remst_sta", item.Status);
            sqlCommand.Parameters.AddWithValue("@remst_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@remst_sum", item.REMST_SUM);
            sqlCommand.Parameters.AddWithValue("@remst_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE remst SET remst_a = 0 WHERE remst_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public RemisionSTModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "remst", "remst")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.GetMapper(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<RemisionSTModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "remst", "remst")
            };
            return this.GetMapper(sqlCommand);
        }
    }
}
