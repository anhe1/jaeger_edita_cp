﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Contribuyentes.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de control del directorio (tabla DIR)
    /// </summary>
    public class SqlFbContribuyenteRepository : RepositoryMaster<ContribuyenteModel>, ISqlContribuyenteRepository {

        public SqlFbContribuyenteRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public int Insert(ContribuyenteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO dir (dir_id, dir_a, dir_dircla_id, dir_diresp_id, dir_cla, dir_nom, dir_tel, dir_dir, dir_col, dir_cp, dir_del_mun, dir_est, dir_pai, dir_rfc, dir_fec_pag, dir_obs, dir_des, dir_lim_cre, dir_web, dir_usu_n, dir_fm, dir_aut, dir_user, dir_pass, dir_pue, dir_dep, dir_tiprem_id) 
                			VALUES (@dir_id, dir_a, @dir_dircla_id, @dir_diresp_id, @dir_cla, @dir_nom, @dir_tel, @dir_dir, @dir_col, @dir_cp, @dir_del_mun, @dir_est, @dir_pai, @dir_rfc, @dir_fec_pag, @dir_obs, @dir_des, @dir_lim_cre, @dir_web, @dir_usu_n, @dir_fm, @dir_aut, @dir_user, @dir_pass, @dir_pue, @dir_dep, @dir_tiprem_id)"
            };
            item.IdDirectorio = this.Max("DIR_ID");
            sqlCommand.Parameters.AddWithValue("@dir_id", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@dir_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@dir_dircla_id", item.IdClase);
            sqlCommand.Parameters.AddWithValue("@dir_diresp_id", item.IdEspecialidad);
            sqlCommand.Parameters.AddWithValue("@dir_cla", item.Clave);
            sqlCommand.Parameters.AddWithValue("@dir_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@dir_tel", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@dir_dir", item.Direccion);
            sqlCommand.Parameters.AddWithValue("@dir_col", item.Colonia);
            sqlCommand.Parameters.AddWithValue("@dir_cp", item.CodigoPostal);
            sqlCommand.Parameters.AddWithValue("@dir_del_mun", item.Delegacion);
            sqlCommand.Parameters.AddWithValue("@dir_est", item.Estado);
            sqlCommand.Parameters.AddWithValue("@dir_pai", item.Pais);
            sqlCommand.Parameters.AddWithValue("@dir_rfc", item.RFC);
            sqlCommand.Parameters.AddWithValue("@dir_fec_pag", item.DiasCredito);
            sqlCommand.Parameters.AddWithValue("@dir_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@dir_des", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@dir_lim_cre", item.Credito);
            sqlCommand.Parameters.AddWithValue("@dir_web", item.Correo);
            sqlCommand.Parameters.AddWithValue("@dir_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@dir_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@dir_aut", item.DIR_AUT);
            sqlCommand.Parameters.AddWithValue("@dir_user", item.DIR_USER);
            sqlCommand.Parameters.AddWithValue("@dir_pass", item.DIR_PASS);
            sqlCommand.Parameters.AddWithValue("@dir_pue", item.DIR_PUE);
            sqlCommand.Parameters.AddWithValue("@dir_dep", item.DIR_DEP);
            sqlCommand.Parameters.AddWithValue("@dir_tiprem_id", item.IdTipoRemision);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdDirectorio;
            return 0;
        }

        public int Update(ContribuyenteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE dir 
                			SET dir_a = @dir_a, dir_dircla_id = @dir_dircla_id, dir_diresp_id = @dir_diresp_id, dir_cla = @dir_cla, dir_nom = @dir_nom, dir_tel = @dir_tel, dir_dir = @dir_dir, dir_col = @dir_col, dir_cp = @dir_cp, dir_del_mun = @dir_del_mun, dir_est = @dir_est, dir_pai = @dir_pai, dir_rfc = @dir_rfc, dir_fec_pag = @dir_fec_pag, dir_obs = @dir_obs, dir_des = @dir_des, dir_lim_cre = @dir_lim_cre, dir_web = @dir_web, dir_usu_m = @dir_usu_m, dir_fm = @dir_fm, dir_aut = @dir_aut, dir_user = @dir_user, dir_pass = @dir_pass, dir_pue = @dir_pue, dir_dep = @dir_dep, dir_tiprem_id = @dir_tiprem_id 
                			WHERE dir_id = @dir_id;"
            };
            sqlCommand.Parameters.AddWithValue("@dir_id", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@dir_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@dir_dircla_id", item.IdClase);
            sqlCommand.Parameters.AddWithValue("@dir_diresp_id", item.IdEspecialidad);
            sqlCommand.Parameters.AddWithValue("@dir_cla", item.Clave);
            sqlCommand.Parameters.AddWithValue("@dir_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@dir_tel", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@dir_dir", item.Direccion);
            sqlCommand.Parameters.AddWithValue("@dir_col", item.Colonia);
            sqlCommand.Parameters.AddWithValue("@dir_cp", item.CodigoPostal);
            sqlCommand.Parameters.AddWithValue("@dir_del_mun", item.Delegacion);
            sqlCommand.Parameters.AddWithValue("@dir_est", item.Estado);
            sqlCommand.Parameters.AddWithValue("@dir_pai", item.Pais);
            sqlCommand.Parameters.AddWithValue("@dir_rfc", item.RFC);
            sqlCommand.Parameters.AddWithValue("@dir_fec_pag", item.DiasCredito);
            sqlCommand.Parameters.AddWithValue("@dir_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@dir_des", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@dir_lim_cre", item.Credito);
            sqlCommand.Parameters.AddWithValue("@dir_web", item.Correo);
            sqlCommand.Parameters.AddWithValue("@dir_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@dir_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@dir_aut", item.DIR_AUT);
            sqlCommand.Parameters.AddWithValue("@dir_user", item.DIR_USER);
            sqlCommand.Parameters.AddWithValue("@dir_pass", item.DIR_PASS);
            sqlCommand.Parameters.AddWithValue("@dir_pue", item.DIR_PUE);
            sqlCommand.Parameters.AddWithValue("@dir_dep", item.DIR_DEP);
            sqlCommand.Parameters.AddWithValue("@dir_tiprem_id", item.IdTipoRemision);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE dir SET dir_a = 0 WHERE dir_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ContribuyenteModel GetById(int id) {
            return this.GetById<ContribuyenteModel>(id); 
        }

        public T1 GetById<T1>(int id) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM dir WHERE dir_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.GetMapper<T1>(sqlCommand).SingleOrDefault();
        }

        public ContribuyenteDetailModel Save(ContribuyenteDetailModel model) {
            if (model.IdDirectorio == 0) {
                model.FechaModifica = System.DateTime.Now;
                model.Creo = this.User;
                model.IdDirectorio = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = System.DateTime.Now;
                this.Update(model);
            }
            return model;
        }

        public IEnumerable<ContribuyenteModel> GetList() {
            var sqlCommand = new FbCommand() {
                CommandText = @"SELECT DIR_ID,DIR_A,DIR_CLA,DIR_NOM,DIR_DIRCLA_ID,DIRCLA_ID,DIRCLA_A,DIRCLA_C,DIRCLA_CLA,DIR_DIRESP_ID,DIRESP_ID,DIRESP_A,DIRESP_ESP,DIR_TEL,DIR_DIR,DIR_COL,DIR_CP,DIR_DEL_MUN,DIR_PAI,DIR_EST,DIR_RFC,DIR_WEB,DIR_OBS,DIR_LIM_CRE,DIR_FEC_PAG,DIR_DES,DIR_USU_N,DIR_USU_M,DIR_TIPREM_ID,TIPREM_ID,TIPREM_A,TIPREM_NOM,DIR_FM 
                                FROM DIR , DIRCLA , DIRESP , TIPREM  
                                WHERE DIR_A=1 AND DIR_DIRCLA_ID = DIRCLA_ID AND DIR_DIRESP_ID = DIRESP_ID AND TIPREM_ID = DIR_TIPREM_ID AND DIRCLA_C <>'0' AND DIR_DIRCLA_ID = 2
                                ORDER BY DIR_NOM "
            };

            return this.GetMapper<ContribuyenteModel>(sqlCommand);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DIR.*, DIRCLA.*, DIRESP.*, TIPREM.*
FROM DIR
LEFT JOIN DIRCLA ON DIR_DIRCLA_ID = DIRCLA_ID 
LEFT JOIN DIRESP ON DIR_DIRESP_ID = DIRESP_ID 
LEFT JOIN TIPREM ON DIR_TIPREM_ID = TIPREM_ID
WHERE DIRCLA_C <>'0' AND DIRCLA_C <> '0' @condiciones
ORDER BY DIR_NOM "
            };

            var d1 = conditionals.Where(it => it.FieldName.ToLower() == "@search").FirstOrDefault();
            if (d1 != null) {
                if (d1.FieldValue != string.Empty) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "AND LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DIR_NOM, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u')) || ',' || LOWER(DIR_CLA) || ','|| LOWER(DIR_RFC) LIKE @SEARCH @condiciones ");
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@SEARCH", "'%" + d1.FieldValue.ToLower() + "%'");
                }
                conditionals.Remove(d1);
            }
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals));
        }
    }
}
