﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbLcProRepository : RepositoryMaster<Domain.CP.Presupuesto.Entities.LineaProduccionModel>, ISqlLcProRepository {
        protected ISqlLcMpRepository lcMpRepository;
        protected ISqlProMpRepository proMpRepository;
        protected ISqlProcesoEtapaRepository procesoEtapaRepository;

        public SqlFbLcProRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.lcMpRepository = new SqlFbLcMpRepository(configuracion);
            this.proMpRepository = new SqlFbProMpRepository(configuracion);
            this.procesoEtapaRepository = new SqlFbProcesoEtapaRepository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE lcpro SET lcpro_A = 0 WHERE lcpro_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public Domain.CP.Presupuesto.Entities.LineaProduccionModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from lcpro where lcpro_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Domain.CP.Presupuesto.Entities.LineaProduccionModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<Domain.CP.Presupuesto.Entities.LineaProduccionModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from LcPro where LcPro_A = 1 order by LcPro_Sec"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Domain.CP.Presupuesto.Entities.LineaProduccionModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<LineaProduccionDetailModel> GetList(int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from LcPro where LcPro_A = 1 order by LcPro_Sec"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionDetailModel>();
            var result = mapper.Map(tabla).ToList();

            var ids = result.Select(it => it.IdProceso).ToArray();
            var materiaPrima = this.lcMpRepository.GetList(new int[] { 0 });
            var consumibles = this.proMpRepository.GetList(ids);
            var subProcesos = this.procesoEtapaRepository.GetList(true, ids);

            for (int i = 0; i < result.Count; i++) {
                result[i].MateriaPrima = new System.ComponentModel.BindingList<LcMpDetailModel>(materiaPrima.Where(it => it.LCMP_PRO_ID == result[i].IdLinea).ToList());
                result[i].Consumibles = new System.ComponentModel.BindingList<ProMpDetailModel>(consumibles.Where(it => it.PROMP_PRO_ID == result[i].IdProceso).ToList());
                result[i].SubProcesos = new System.ComponentModel.BindingList<ProcesoEtapaDetailModel>(subProcesos.Where(it => it.IdEdp3 == result[i].IdProceso).ToList());
            }
            return result;
        }

        public IEnumerable<Domain.CP.Presupuesto.Entities.LineaProduccionView> GetListView(int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = @"select LcPro.* ,
                                EDP3_ID, EDP3_A, EDP1_are, EDP1_nom, EDP2_sec, EDP2_nom, EDP3_nom, EDP2_val_min
                                from LcPro, Edp3, Edp2, Edp1 
                                where LcPro_A = 1 and Edp3_Edp2_ID = Edp2_ID and Edp2_Edp1_ID = Edp1_ID and EDP3_ID = LcPro_PRO_ID 
                                order by LcPro_Sec, EDP1_nom, EDP2_sec,EDP2_nom,EDP3_nom"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Domain.CP.Presupuesto.Entities.LineaProduccionView>();
            var result = mapper.Map(tabla).ToList();

            var ids = result.Select(it => it.IdProceso).ToArray();
            var materiaPrima = this.lcMpRepository.GetListView(new int[] { 0 });
            var consumibles = this.proMpRepository.GetList(true, ids);
            var subProcesos = this.procesoEtapaRepository.GetList(true, ids);

            for (int i = 0; i < result.Count; i++) {
                result[i].MateriaPrima = new System.ComponentModel.BindingList<LcMpModelView>(materiaPrima.Where(it => it.LCMP_PRO_ID == result[i].IdLinea).ToList());
                result[i].Consumibles = new System.ComponentModel.BindingList<ProMpDetailModelView>(consumibles.Where(it => it.PROMP_PRO_ID == result[i].IdProceso).ToList());
                result[i].SubProcesos = new System.ComponentModel.BindingList<ProcesoEtapaDetailModel>(subProcesos.Where(it => it.IdEdp3 == result[i].IdProceso).ToList());
            }
            return result;
        }

        public int Insert(Domain.CP.Presupuesto.Entities.LineaProduccionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO lcpro (lcpro_id, lcpro_a, lcpro_com_id, lcpro_pro_id, lcpro_sec, lcpro_can_cp, lcpro_can_vp, lcpro_usu_fm, lcpro_fm, lcpro_com2_id) 
                			VALUES (@lcpro_id, @lcpro_a, @lcpro_com_id, @lcpro_pro_id, @lcpro_sec, @lcpro_can_cp, @lcpro_can_vp, @lcpro_usu_fm, @lcpro_fm, @lcpro_com2_id)"
            };
            item.IdLinea = this.Max("LCPRO_ID");
            sqlCommand.Parameters.AddWithValue("@lcpro_id", item.IdLinea);
            sqlCommand.Parameters.AddWithValue("@lcpro_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@lcpro_com_id", item.IdCom);
            sqlCommand.Parameters.AddWithValue("@lcpro_pro_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@lcpro_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@lcpro_can_cp", item.CantidadProceso);
            sqlCommand.Parameters.AddWithValue("@lcpro_can_vp", item.VecesProceso);
            sqlCommand.Parameters.AddWithValue("@lcpro_usu_fm", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@lcpro_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@lcpro_com2_id", item.IdCom2);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdLinea;
            return 0;
        }

        public int Update(Domain.CP.Presupuesto.Entities.LineaProduccionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE lcpro 
                			SET lcpro_a = @lcpro_a, lcpro_com_id = @lcpro_com_id, lcpro_pro_id = @lcpro_pro_id, lcpro_sec = @lcpro_sec, lcpro_can_cp = @lcpro_can_cp, lcpro_can_vp = @lcpro_can_vp, lcpro_usu_fm = @lcpro_usu_fm, lcpro_fm = @lcpro_fm, lcpro_com2_id = @lcpro_com2_id 
                			WHERE lcpro_id = @lcpro_id;"
            };

            sqlCommand.Parameters.AddWithValue("@lcpro_id", item.IdLinea);
            sqlCommand.Parameters.AddWithValue("@lcpro_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@lcpro_com_id", item.IdCom);
            sqlCommand.Parameters.AddWithValue("@lcpro_pro_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@lcpro_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@lcpro_can_cp", item.CantidadProceso);
            sqlCommand.Parameters.AddWithValue("@lcpro_can_vp", item.VecesProceso);
            sqlCommand.Parameters.AddWithValue("@lcpro_usu_fm", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@lcpro_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@lcpro_com2_id", item.IdCom2);
            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
