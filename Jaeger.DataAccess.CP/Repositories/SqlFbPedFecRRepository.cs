﻿using System.Linq;
using System.Collections.Generic;
using System;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.CP.Repositories {
    ///<summary>
    /// repositorio de cambios de status de la orden de produccion (PEDFECR)
    ///</summary>
    public class SqlFbPedFecRRepository : RepositoryMaster<PedFecRModel>, ISqlPedFecRRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public SqlFbPedFecRRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) { this.User = user; }

        public int Insert(PedFecRModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO pedfecr (pedfecr_id, pedfecr_a, pedfecr_pedprd_id, pedfecr_fecl_id, pedfecr_st_id, pedfecr_fec, pedfecr_obs, pedfecr_usu_n, pedfecr_usu_f, pedfecr_usu_fm, pedfecr_fm) 
                            VALUES (@pedfecr_id, @pedfecr_a, @pedfecr_pedprd_id, @pedfecr_fecl_id, @pedfecr_st_id, @pedfecr_fec, @pedfecr_obs, @pedfecr_usu_n, @pedfecr_usu_f, @pedfecr_usu_fm, @pedfecr_fm)"
            };
            item.IdPedFecR = this.Max("PEDFECR_ID");
            sqlCommand.Parameters.AddWithValue("@pedfecr_id", item.IdPedFecR);
            sqlCommand.Parameters.AddWithValue("@pedfecr_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedfecr_pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@pedfecr_fecl_id", item.PEDFECR_FECL_ID);
            sqlCommand.Parameters.AddWithValue("@pedfecr_st_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@pedfecr_fec", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@pedfecr_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedfecr_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@pedfecr_usu_f", item.PEDFECR_USU_F);
            sqlCommand.Parameters.AddWithValue("@pedfecr_usu_fm", item.PEDFECR_USU_FM);
            sqlCommand.Parameters.AddWithValue("@pedfecr_fm", item.FechaModifica);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdPedFecR;
            return 0;
        }

        public int Update(PedFecRModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedfecr 
                            SET pedfecr_a = @pedfecr_a, pedfecr_pedprd_id = @pedfecr_pedprd_id, pedfecr_fecl_id = @pedfecr_fecl_id, pedfecr_st_id = @pedfecr_st_id, pedfecr_fec = @pedfecr_fec, pedfecr_obs = @pedfecr_obs, pedfecr_usu_n = @pedfecr_usu_n, pedfecr_usu_f = @pedfecr_usu_f, pedfecr_usu_fm = @pedfecr_usu_fm, pedfecr_fm = @pedfecr_fm 
                            WHERE pedfecr_id = @pedfecr_id;"
            };
            sqlCommand.Parameters.AddWithValue("@pedfecr_id", item.IdPedFecR);
            sqlCommand.Parameters.AddWithValue("@pedfecr_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedfecr_pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@pedfecr_fecl_id", item.PEDFECR_FECL_ID);
            sqlCommand.Parameters.AddWithValue("@pedfecr_st_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@pedfecr_fec", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@pedfecr_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedfecr_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@pedfecr_usu_f", item.PEDFECR_USU_F);
            sqlCommand.Parameters.AddWithValue("@pedfecr_usu_fm", item.PEDFECR_USU_FM);
            sqlCommand.Parameters.AddWithValue("@pedfecr_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedfecr SET pedfecr_a = 0 WHERE pedfecr_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public bool Cancelar(PedFecRDetailModel item) {
            return this.Insert(item) > 0;
        }

        public bool Save(PedFecRDetailModel model) {
            if (model.IdPedFecR == 0) {
                model.PEDFECR_USU_F = DateTime.Now;
                model.Creo = this.User;
                return this.Insert(model) > 0;
            } else {
                model.FechaModifica = DateTime.Now;
                model.Creo = this.User;
                return this.Update(model) > 0;
            }
        }

        public PedFecRModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "pedfecr", "pedfecr")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PedFecRModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<PedFecRModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "pedfecr", "pedfecr")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PedFecRModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de cambios de fechas de acuerdo de un grupo de ordenes
        /// </summary>
        public IEnumerable<PedFecRDetailModelView> GetList(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT PEDFECR.*, PEDST_STA, PEDST_SEC FROM PEDFECR, PEDST WHERE PEDFECR_ST_ID = PEDST_ID @condiciones"
            };
            sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
            var result = this.GetMapper<PedFecRDetailModelView>(sqlCommand).ToList();
            return result;
        }

        /// <summary>
        /// obtener listado de cambios de fechas de acuerdo de un grupo de ordenes
        /// </summary>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT PEDFECR.*, PEDST_STA, PEDST_SEC FROM PEDFECR, PEDST WHERE PEDFECR_ST_ID = PEDST_ID @condiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals).ToList(); 
        }
    }
}
