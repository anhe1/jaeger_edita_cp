﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbPrdProRepository : RepositoryMaster<PrdProModel>, ISqlPrdProRepository {
        protected ISqlPrdMpRepository prdMpRepository;
        protected ISqlProMpRepository proMpRepository;
        protected ISqlProcesoEtapaRepository procesoEtapaRepository;

        public SqlFbPrdProRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.prdMpRepository = new SqlFbPrdMpRepository(configuracion);
            this.proMpRepository = new SqlFbProMpRepository(configuracion);
            this.procesoEtapaRepository = new SqlFbProcesoEtapaRepository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE prdpro SET prdpro_a = 0 WHERE prdpro_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PrdProModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from prdpro where prdpro_id=@id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PrdProModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<PrdProModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "select * from prdpro where prdpro_a > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PrdProModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// linea de produccion (PRDPRO)
        /// </summary>
        public IEnumerable<PrdProDetailModel> GetList(int[] indexs) {
            var sqlLinea = new FbCommand {
                CommandText = @"select * from PRDPRO where PRDPRO_A > 0 and PRDPRO_PRDCOM_ID in (@index) order by PRDPRO_Sec"
            };

            sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", indexs.ToArray()));
            var tabla = this.ExecuteReader(sqlLinea);
            var mapper = new DataNamesMapper<PrdProDetailModel>();
            var result = mapper.Map(tabla).ToList();
            var allids = result.Select(it => it.PRDPRO_ID).ToList();
            var mp = this.prdMpRepository.GetList(allids.ToArray());
            var allids2 = result.Select(it => it.PRDPRO_PRO_ID).ToArray();
            var consumibles = this.proMpRepository.GetList(allids2.ToArray());
            var subProcesos = this.procesoEtapaRepository.GetList(true, allids2.ToArray());

            for (int i = 0; i < result.Count; i++) {
                result[i].MateriaPrima = new BindingList<PrdMpDetailModel>(mp.Where(it => it.IdPrdPro == result[i].PRDPRO_ID).ToList());
                result[i].Consumibles = new BindingList<ProMpDetailModel>(consumibles.Where(it => it.PROMP_PRO_ID == result[i].PRDPRO_PRO_ID).ToList());
                result[i].SubProcesos = new BindingList<ProcesoEtapaDetailModel>(subProcesos.Where(it => it.IdEdp3 == result[i].PRDPRO_PRO_ID).ToList());
            }

            return result;
        }

        public IEnumerable<PrdProDetailModelView> GetList(int[] indexs, bool active) {
            var sqlCommand = new FbCommand {
                CommandText = @"select prdpro.*, edp3.*, edp2.*, edp1.*
                                from prdpro, edp3, edp2, edp1
                                where prdpro_a > 0 and EDP3_ID = PRDPRO_PRO_ID and Edp3_Edp2_ID = Edp2_ID and Edp2_Edp1_ID = Edp1_ID and PRDPRO_PRDCOM_ID in (@index)
                                order by PRDPRO_Sec"
            };
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", string.Join(",", indexs.ToArray()));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PrdProDetailModelView>();
            var result = mapper.Map(tabla).ToList();

            var allids = result.Select(it => it.PRDPRO_ID).ToList();
            var mp = this.prdMpRepository.GetList1(allids.ToArray());
            var allids2 = result.Select(it => it.PRDPRO_PRO_ID).ToArray();
            var consumibles = this.proMpRepository.GetList(true, allids2.ToArray());
            var subProcesos = this.procesoEtapaRepository.GetList(true, allids2.ToArray());

            Console.WriteLine(active);

            for (int i = 0; i < result.Count; i++) {
                result[i].MateriaPrima = new BindingList<PrdMpDetailModelView>(mp.Where(it => it.IdPrdPro == result[i].PRDPRO_ID).ToList());
                result[i].Consumibles = new BindingList<ProMpDetailModelView>(consumibles.Where(it => it.PROMP_PRO_ID == result[i].PRDPRO_PRO_ID).ToList());
                result[i].SubProcesos = new BindingList<ProcesoEtapaDetailModel>(subProcesos.Where(it => it.IdEdp3 == result[i].PRDPRO_PRO_ID).ToList());
            }

            return result;
        }

        public int Insert(PrdProModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO prdpro (prdpro_id, prdpro_a, prdpro_prdcom_id, prdpro_pro_id, prdpro_sec, prdpro_can_vp, prdpro_can_cp, prdpro_usu_fm, prdpro_fm, prdpro_cot_id) 
                			VALUES (@prdpro_id, @prdpro_a, @prdpro_prdcom_id, @prdpro_pro_id, @prdpro_sec, @prdpro_can_vp, @prdpro_can_cp, @prdpro_usu_fm, @prdpro_fm, @prdpro_cot_id)"
            };
            item.PRDPRO_ID = this.Max("PRDPRO_ID");
            sqlCommand.Parameters.AddWithValue("@prdpro_id", item.PRDPRO_ID);
            sqlCommand.Parameters.AddWithValue("@prdpro_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@prdpro_prdcom_id", item.PRDPRO_PRDCOM_ID);
            sqlCommand.Parameters.AddWithValue("@prdpro_pro_id", item.PRDPRO_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@prdpro_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@prdpro_can_vp", item.PRDPRO_CAN_VP);
            sqlCommand.Parameters.AddWithValue("@prdpro_can_cp", item.PRDPRO_CAN_CP);
            sqlCommand.Parameters.AddWithValue("@prdpro_usu_fm", item.PRDPRO_USU_FM);
            sqlCommand.Parameters.AddWithValue("@prdpro_fm", item.PRDPRO_FM);
            sqlCommand.Parameters.AddWithValue("@prdpro_cot_id", item.PRDPRO_COT_ID);
            if (this.ExecuteScalar(sqlCommand) > 0)
                return item.PRDPRO_ID;
            return 0;
        }

        public int Update(PrdProModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE prdpro 
                			SET prdpro_a = @prdpro_a, prdpro_prdcom_id = @prdpro_prdcom_id, prdpro_pro_id = @prdpro_pro_id, prdpro_sec = @prdpro_sec, prdpro_can_vp = @prdpro_can_vp, prdpro_can_cp = @prdpro_can_cp, prdpro_usu_fm = @prdpro_usu_fm, prdpro_fm = @prdpro_fm, prdpro_cot_id = @prdpro_cot_id 
                			WHERE prdpro_id = @prdpro_id;"
            };
            sqlCommand.Parameters.AddWithValue("@prdpro_id", item.PRDPRO_ID);
            sqlCommand.Parameters.AddWithValue("@prdpro_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@prdpro_prdcom_id", item.PRDPRO_PRDCOM_ID);
            sqlCommand.Parameters.AddWithValue("@prdpro_pro_id", item.PRDPRO_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@prdpro_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@prdpro_can_vp", item.PRDPRO_CAN_VP);
            sqlCommand.Parameters.AddWithValue("@prdpro_can_cp", item.PRDPRO_CAN_CP);
            sqlCommand.Parameters.AddWithValue("@prdpro_usu_fm", item.PRDPRO_USU_FM);
            sqlCommand.Parameters.AddWithValue("@prdpro_fm", item.PRDPRO_FM);
            sqlCommand.Parameters.AddWithValue("@prdpro_cot_id", item.PRDPRO_COT_ID);
            return this.ExecuteTransaction(sqlCommand);
        }

        public PrdProDetailModelView Save(PrdProDetailModelView item) {
            if (item.PRDPRO_ID == 0) {
                item.PRDPRO_ID = this.Insert(item);
            } else {
                this.Update(item);
            }
            return item;
        }
    }
}
