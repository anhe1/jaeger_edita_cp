﻿using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio presupuesto o cotizacion (COT)
    /// </summary>
    public class SqlFbCotizacionRepository : RepositoryMaster<CotizacionModel>, ISqlCotizacionRepository {
        protected ISqlCotizacionCantidadRepository cantidadRepository;
        protected ISqlPrdComRepository prdComRepository;
        public SqlFbCotizacionRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.cantidadRepository = new SqlFbCotizacionCantidadRepository(configuracion);
            this.prdComRepository = new SqlFbPrdComRepository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "update cot set cot_a = 0 where cot_id = @index"
            };

            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(CotizacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO cot (cot_id, cot_a, cot_lp_id, cot_dir_id, cot_dircon_id, cot_ven_id, cot_cotsta_id, cot_prd_id, cot_prd, cot_con, cot_con_pag, cot_con_ent, cot_obs, cot_fec_cad, cot_fec_sol, cot_fec_req, cot_fec_cot, cot_usu_n, cot_usu_m, cot_fm, cot_com_id) 
                			VALUES (@cot_id, @cot_a, @cot_lp_id, @cot_dir_id, @cot_dircon_id, @cot_ven_id, @cot_cotsta_id, @cot_prd_id, @cot_prd, @cot_con, @cot_con_pag, @cot_con_ent, @cot_obs, @cot_fec_cad, @cot_fec_sol, @cot_fec_req, @cot_fec_cot, @cot_usu_n, @cot_usu_m, @cot_fm, @cot_com_id)"
            };
            item.IdCotizacion = this.Max("COT_ID");
            sqlCommand.Parameters.AddWithValue("@cot_id", item.IdCotizacion);
            sqlCommand.Parameters.AddWithValue("@cot_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cot_lp_id", item.COT_LP_ID);
            sqlCommand.Parameters.AddWithValue("@cot_dir_id", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@cot_dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@cot_ven_id", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@cot_cotsta_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@cot_prd_id", item.COT_PRD_ID);
            sqlCommand.Parameters.AddWithValue("@cot_prd", item.COT_PRD);
            sqlCommand.Parameters.AddWithValue("@cot_con", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@cot_con_pag", item.CondicionPago);
            sqlCommand.Parameters.AddWithValue("@cot_con_ent", item.CondicionEntrega);
            sqlCommand.Parameters.AddWithValue("@cot_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@cot_fec_cad", item.COT_FEC_CAD);
            sqlCommand.Parameters.AddWithValue("@cot_fec_sol", item.FechaSolicitud);
            sqlCommand.Parameters.AddWithValue("@cot_fec_req", item.FechaRequerido);
            sqlCommand.Parameters.AddWithValue("@cot_fec_cot", item.FechaCotizacion);
            sqlCommand.Parameters.AddWithValue("@cot_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@cot_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@cot_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@cot_com_id", item.IdCom);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdCotizacion;
            return 0;
        }

        public int Update(CotizacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cot 
                			SET cot_a = @cot_a, cot_lp_id = @cot_lp_id, cot_dir_id = @cot_dir_id, cot_dircon_id = @cot_dircon_id, cot_ven_id = @cot_ven_id, cot_cotsta_id = @cot_cotsta_id, cot_prd_id = @cot_prd_id, cot_prd = @cot_prd, cot_con = @cot_con, cot_con_pag = @cot_con_pag, cot_con_ent = @cot_con_ent, cot_obs = @cot_obs, cot_fec_cad = @cot_fec_cad, cot_fec_sol = @cot_fec_sol, cot_fec_req = @cot_fec_req, cot_fec_cot = @cot_fec_cot, cot_usu_n = @cot_usu_n, cot_usu_m = @cot_usu_m, cot_fm = @cot_fm, cot_com_id = @cot_com_id 
                			WHERE cot_id = @cot_id;"
            };
            sqlCommand.Parameters.AddWithValue("@cot_id", item.IdCotizacion);
            sqlCommand.Parameters.AddWithValue("@cot_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cot_lp_id", item.COT_LP_ID);
            sqlCommand.Parameters.AddWithValue("@cot_dir_id", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@cot_dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@cot_ven_id", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@cot_cotsta_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@cot_prd_id", item.COT_PRD_ID);
            sqlCommand.Parameters.AddWithValue("@cot_prd", item.COT_PRD);
            sqlCommand.Parameters.AddWithValue("@cot_con", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@cot_con_pag", item.CondicionPago);
            sqlCommand.Parameters.AddWithValue("@cot_con_ent", item.CondicionEntrega);
            sqlCommand.Parameters.AddWithValue("@cot_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@cot_fec_cad", item.COT_FEC_CAD);
            sqlCommand.Parameters.AddWithValue("@cot_fec_sol", item.FechaSolicitud);
            sqlCommand.Parameters.AddWithValue("@cot_fec_req", item.FechaRequerido);
            sqlCommand.Parameters.AddWithValue("@cot_fec_cot", item.FechaCotizacion);
            sqlCommand.Parameters.AddWithValue("@cot_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@cot_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@cot_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@cot_com_id", item.IdCom);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdCotizacion;
            return 0;
        }

        public CotizacionModel GetById(int index) {
            var sqlCotizacion = new FbCommand {
                CommandText = @"select * from cot where COT_ID = @index"
            };
            sqlCotizacion.Parameters.AddWithValue("@index", index);
            //var tabla = this.ExecuteReader(sqlCotizacion);
            //var mapper = new DataNamesMapper<CotizacionDetailModel>();
            //var result = mapper.Map(tabla).Single();
            //return result;
            return this.GetMapper(sqlCotizacion).Single();
        }

        public CotizacionDetailModelView GetCotizacion(int index) {
            var sqlCotizacion = new FbCommand {
                CommandText = @"select * from cot where COT_ID = @index"
            };
            sqlCotizacion.Parameters.AddWithValue("@index", index);
            var result = this.GetMapper<CotizacionDetailModelView>(sqlCotizacion).Single();
            result.CantidadPrecios = new BindingList<CotizacionCantidadPrecioDetailModel>(this.cantidadRepository.GetList(index).ToList());
            result.Estructura = new BindingList<PrdComDetailModelView>(this.prdComRepository.GetList(index, false).ToList());
            return result;
        }

        public IEnumerable<CotizacionModel> GetList() {
            var sqlCotizacion = new FbCommand {
                CommandText = @"select * from cot where COT_A > 0"
            };

            //var tabla = this.ExecuteReader(sqlCotizacion);
            //var mapper = new DataNamesMapper<CotizacionDetailModel>();
            //var result = mapper.Map(tabla).ToList();
            //return result;
            return this.GetMapper(sqlCotizacion);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"select cot.*, 
                                dir.DIR_nom, 
                                dircon.DIRCON_con, 
                                lp1.LP1_Tex, 
                                ven.VEN_cla, 
                                cotsta.COTSTA_nom
                                from cot, dir, dircon, lp1, ven, cotsta
                                where cot_dir_id = dir_id and cot_dircon_id = dircon_id 
                                and cot_com_id = lp1_id 
                                and cot_ven_id = ven_id
                                and cot_cotsta_id = cotsta_id 
                                @condiciones
                                order by cot_id desc"
            };

            sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
            var result = this.GetMapper<T1>(sqlCommand).ToList();
            return result;
        }

        /// <summary>
        /// obtener listado de la vista de presupuestos y cotizaciones por rangos de fecha o periodo
        /// </summary>
        /// <param name="startDate">rango inicial, si el segundo parametro es nulo se considera como un periodo</param>
        /// <param name="endDate">fin del rango</param>
        /// <param name="startStatus">rango inicial del status, si el siguiente parametro es nulo entonces se cosidera un solo status</param>
        /// <param name="endStatus">fin del rango de status</param>
        /// <returns>Vista de cotizacion o presupuestos</returns>
        //public IEnumerable<CotizacionModelView> GetList(DateTime startDate, DateTime? endDate = null, int? startStatus = null, int? endStatus = null) {
        //    var sqlCommand = new FbCommand {
        //        CommandText = @"select cot.*, 
        //                        dir.DIR_nom, 
        //                        dircon.DIRCON_con, 
        //                        lp1.LP1_Tex, 
        //                        ven.VEN_cla, 
        //                        cotsta.COTSTA_nom
        //                        from cot, dir, dircon, lp1, ven, cotsta
        //                        where cot_dir_id = dir_id and cot_dircon_id = dircon_id 
        //                        and cot_com_id = lp1_id 
        //                        and cot_ven_id = ven_id
        //                        and cot_cotsta_id = cotsta_id 
        //                        @status 
        //                        @date
        //                        order by cot_id desc"
        //    };

        //    if (endDate == null) {
        //        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and extract(year from COT_fec_sol) = @year and extract(month from COT_fec_sol) = @month");
        //        sqlCommand.Parameters.AddWithValue("@year", startDate.Year);
        //        sqlCommand.Parameters.AddWithValue("@month", startDate.Month);
        //    } else {
        //        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and COT_fec_sol >= @startDate and COT_fec_sol <= @endDate");
        //        sqlCommand.Parameters.AddWithValue("@startDate", startDate);
        //        sqlCommand.Parameters.AddWithValue("@endDate", endDate);
        //    }

        //    if (startStatus != null) {
        //        if (endStatus != null) {
        //            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and cot_cotsta_id >= @startStatus and cot_cotsta_id <= @endStatus");
        //            sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
        //            sqlCommand.Parameters.AddWithValue("@endStatus", endStatus);
        //        } else {
        //            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and cot_cotsta_id = @startStatus");
        //            sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
        //        }
        //    } else {
        //        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "");
        //    }

        //    //var tabla = this.ExecuteReader(sqlCommand);
        //    //var mapper = new DataNamesMapper<CotizacionModelView>();
        //    //return mapper.Map(tabla).ToList();
        //    return this.GetMapper<CotizacionModelView>(sqlCommand);
        //}

        /// <summary>
        /// almacenar cotizacion
        /// </summary>
        public CotizacionDetailModelView Save(CotizacionDetailModelView item) {
            if (item.IdCotizacion == 0) {
                item.IdCotizacion = this.Insert(item);
            } else {
                this.Update(item);
            }

            if (item.IdCotizacion > 0) {
                // cantidades y precios
                for (int i = 0; i < item.CantidadPrecios.Count; i++) {
                    item.CantidadPrecios[i].IdCotizacion = item.IdCotizacion;
                    item.CantidadPrecios[i] = this.cantidadRepository.Save(item.CantidadPrecios[i]);
                }
                // lineas de produccion
                for (int i = 0; i < item.Estructura.Count; i++) {
                    item.Estructura[i].IdCotizacion = item.IdCotizacion;
                    item.Estructura[i] = this.prdComRepository.Save(item.Estructura[i]);
                }
            }
            return item;
        }

        public CotizacionDetailModelView Calcular(CotizacionDetailModelView item) {
            for (int i = 0; i < item.CantidadPrecios.Count; i++) {
                var sqlCommand = new FbCommand {
                    CommandText = @"Execute Procedure Cot_Calcula (@index)"
                };
                sqlCommand.Parameters.AddWithValue("@index", item.CantidadPrecios[i].IdCotPrd);
                this.ExecuteNoQuery(sqlCommand);
            }
            return item;
        }

        public void Crear(CotizacionDetailModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"Execute Procedure CotN2 (@Cot_IDX,@Cot_Com_IDX,@Max_Com,@Max_LC,@Max_Pro,@Max_MP)"
            };
            sqlCommand.Parameters.AddWithValue("@Cot_IDX", item.IdCotizacion);
            sqlCommand.Parameters.AddWithValue("@Cot_Com_IDX", item.IdCom);
            sqlCommand.Parameters.AddWithValue("@Max_Com", this.Max("PrdCom_ID"));
            sqlCommand.Parameters.AddWithValue("@Max_LC", this.Max("CotLC2_ID"));
            sqlCommand.Parameters.AddWithValue("@Max_Pro", this.Max("PrdPro_ID"));
            sqlCommand.Parameters.AddWithValue("@Max_MP", this.Max("PrdMP_ID"));
            this.ExecuteNoQuery(sqlCommand);
        }
    }
}
