﻿/// purpose: repositorio de status de remision (REMST)
/// develop: ANHE1 27092020 0121
using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Domain.CP.Empresa.Contracts;
using Jaeger.DataAccess.FireBird.Services;
using System.Linq;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbReqCompraPartidaRepository : RepositoryMaster<ReqCompraPartidaModel>, ISqlReqCompraPartidaRepository {
        public SqlFbReqCompraPartidaRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM OCDPP WHERE ((OCDPP_ID = @p1))" };
            throw new System.NotImplementedException();
        }

        public ReqCompraPartidaModel GetById(int index) {
            var response = this.GetList<ReqCompraPartidaModel>(new List<Conditional> {
                new Conditional("OCDPP_ID", index.ToString()) 
            }).FirstOrDefault();
            return response;
        }

        public IEnumerable<ReqCompraPartidaModel> GetList() {
            return this.GetList<ReqCompraPartidaModel>(new List<Conditional>());
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT OCDPP.* FROM OCDPP @wcondiciones ORDER BY OCDPP_ID ASC"
            };

            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals));
        }

        public int Insert(ReqCompraPartidaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO OCDPP ( OCDPP_ID, OCDPP_A, OCDPP_OCDP_ID, OCDPP_CTPRD_ID, OCDPP_CTMDL_ID, OCDPP_CTDP_ID, OCDPP_PEDPRD, OCDPP_CANT, OCDPP_UNDN, OCDPP_NOM, OCDPP_MRC, OCDPP_ESPC, OCDPP_NOTA, OCDPP_UNT, OCDPP_SBTTL, OCDPP_DESC, OCDPP_IMG_URL, OCDPP_USR_N, OCDPP_FN) 
                                           VALUES (@OCDPP_ID,@OCDPP_A,@OCDPP_OCDP_ID,@OCDPP_CTPRD_ID,@OCDPP_CTMDL_ID,@OCDPP_CTDP_ID,@OCDPP_PEDPRD,@OCDPP_CANT,@OCDPP_UNDN,@OCDPP_NOM,@OCDPP_MRC,@OCDPP_ESPC,@OCDPP_NOTA,@OCDPP_UNT,@OCDPP_SBTTL,@OCDPP_DESC,@OCDPP_IMG_URL,@OCDPP_USR_N,@OCDPP_FN) RETURNING OCDPP_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@OCDPP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@OCDPP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@OCDPP_OCDP_ID", item.Folio);
            sqlCommand.Parameters.AddWithValue("@OCDPP_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@OCDPP_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@OCDPP_CTDP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@OCDPP_PEDPRD", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@OCDPP_CANT", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@OCDPP_UNDN", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@OCDPP_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@OCDPP_MRC", item.Marca);
            sqlCommand.Parameters.AddWithValue("@OCDPP_ESPC", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@OCDPP_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@OCDPP_UNT", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@OCDPP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@OCDPP_DESC", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@OCDPP_IMG_URL", item.ImagenURL);
            sqlCommand.Parameters.AddWithValue("@OCDPP_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@OCDPP_FN", item.FechaNuevo);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdConcepto;
            return 0;
        }

        public int Update(ReqCompraPartidaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OCDPP SET OCDPP_A = @OCDPP_A, OCDPP_OCDP_ID = @OCDPP_OCDP_ID, OCDPP_CTPRD_ID = @OCDPP_CTPRD_ID, OCDPP_CTMDL_ID = @OCDPP_CTMDL_ID, OCDPP_CTDP_ID = @OCDPP_CTDP_ID, OCDPP_PEDPRD = @OCDPP_PEDPRD, OCDPP_CANT = @OCDPP_CANT, OCDPP_UNDN = @OCDPP_UNDN, 
OCDPP_NOM = @OCDPP_NOM, OCDPP_MRC = @OCDPP_MRC, OCDPP_ESPC = @OCDPP_ESPC, OCDPP_NOTA = @OCDPP_NOTA, OCDPP_UNT = @OCDPP_UNT, OCDPP_SBTTL = @OCDPP_SBTTL, OCDPP_DESC = @OCDPP_DESC, OCDPP_IMG_URL = @OCDPP_IMG_URL, OCDPP_USR_M = @OCDPP_USR_M, OCDPP_FM = @OCDPP_FM WHERE (OCDPP_ID = @OCDPP_ID)"
            };
            sqlCommand.Parameters.AddWithValue("@OCDPP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@OCDPP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@OCDPP_OCDP_ID", item.Folio);
            sqlCommand.Parameters.AddWithValue("@OCDPP_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@OCDPP_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@OCDPP_CTDP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@OCDPP_PEDPRD", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@OCDPP_CANT", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@OCDPP_UNDN", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@OCDPP_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@OCDPP_MRC", item.Marca);
            sqlCommand.Parameters.AddWithValue("@OCDPP_ESPC", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@OCDPP_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@OCDPP_UNT", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@OCDPP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@OCDPP_DESC", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@OCDPP_IMG_URL", item.ImagenURL);
            sqlCommand.Parameters.AddWithValue("@OCDPP_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@OCDPP_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public ReqCompraPartidaDetailModel Save(ReqCompraPartidaDetailModel model) {
            if (model.IdConcepto == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdConcepto = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            return model;
        }
    }
}
