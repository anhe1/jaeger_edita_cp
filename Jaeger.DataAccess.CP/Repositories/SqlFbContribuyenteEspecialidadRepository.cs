﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Contribuyentes.Contracts;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de especialidades del directorio (tabla DIRESP)
    /// </summary>
    public class SqlFbContribuyenteEspecialidadRepository : RepositoryMaster<ContribuyenteEspecialidadModel>, ISqlContribuyenteEspecialidadRepository {

        public SqlFbContribuyenteEspecialidadRepository(DataBaseConfiguracion configuracion) : base(configuracion) { }

        public int Insert(ContribuyenteEspecialidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO diresp (diresp_id, diresp_a, diresp_esp, diresp_fm, diresp_bps) 
                			VALUES (@diresp_id, @diresp_a, @diresp_esp, @diresp_fm, @diresp_bps)"
            };
            item.IdDirEsp = this.Max("DIRESP_ID");
            sqlCommand.Parameters.AddWithValue("@diresp_id", item.IdDirEsp);
            sqlCommand.Parameters.AddWithValue("@diresp_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@diresp_esp", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@diresp_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@diresp_bps", item.DIRESP_BPS);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdDirEsp;
            return 0;
        }

        public int Update(ContribuyenteEspecialidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE diresp 
                			SET diresp_a = @diresp_a, diresp_esp = @diresp_esp, diresp_fm = @diresp_fm, diresp_bps = @diresp_bps 
                			WHERE diresp_id = @diresp_id;"
            };
            sqlCommand.Parameters.AddWithValue("@diresp_id", item.IdDirEsp);
            sqlCommand.Parameters.AddWithValue("@diresp_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@diresp_esp", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@diresp_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@diresp_bps", item.DIRESP_BPS);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE diresp SET diresp_a = 0 WHERE diresp_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ContribuyenteEspecialidadModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "diresp", "diresp")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ContribuyenteEspecialidadModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ContribuyenteEspecialidadModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "diresp", "diresp")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ContribuyenteEspecialidadModel>();
            return mapper.Map(tabla).ToList();
        }

    }
}
