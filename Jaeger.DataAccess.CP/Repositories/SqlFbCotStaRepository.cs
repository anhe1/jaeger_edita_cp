﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using System;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Aplication.CP.Repositories {
    public class SqlFbCotStaRepository : RepositoryMaster<CotizacionStatusModel>, ISqlCotStatusRepository {

        public SqlFbCotStaRepository(DataBaseConfiguracion configuracion) : base(configuracion) { }

        public int Insert(CotizacionStatusModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO cotsta (cotsta_id, cotsta_a, cotsta_nom, cotsta_sec, cotsta_usu_fm, cotsta_fm) 
                			VALUES (@cotsta_id, @cotsta_a, @cotsta_nom, @cotsta_sec, @cotsta_usu_fm, @cotsta_fm)"
            };
            item.IdCotStatus = this.Max("COTSTA_ID");
            sqlCommand.Parameters.AddWithValue("@cotsta_id", item.IdCotStatus);
            sqlCommand.Parameters.AddWithValue("@cotsta_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotsta_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@cotsta_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@cotsta_usu_fm", item.COTSTA_USU_FM);
            sqlCommand.Parameters.AddWithValue("@cotsta_fm", item.FechaModifica);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdCotStatus;
            return 0;
        }

        public int Update(CotizacionStatusModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotsta 
                			SET cotsta_a = @cotsta_a, cotsta_nom = @cotsta_nom, cotsta_sec = @cotsta_sec, cotsta_usu_fm = @cotsta_usu_fm, cotsta_fm = @cotsta_fm 
                			WHERE cotsta_id = @cotsta_id;"
            };
            sqlCommand.Parameters.AddWithValue("@cotsta_id", item.IdCotStatus);
            sqlCommand.Parameters.AddWithValue("@cotsta_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotsta_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@cotsta_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@cotsta_usu_fm", item.COTSTA_USU_FM);
            sqlCommand.Parameters.AddWithValue("@cotsta_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotsta SET cotsta_a = 0 WHERE cotsta_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CotizacionStatusModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "cotsta", "cotsta")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotizacionStatusModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<CotizacionStatusModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "cotsta", "cotsta")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotizacionStatusModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT COTSTA.* FROM COTSTA @wcondiciones ORDER BY COTSTA_SEC ASC"
            };

            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }
    }
}
