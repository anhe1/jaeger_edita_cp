﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Contribuyentes.Contracts;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de clases del directorio
    /// </summary>
    public class SqlFbContribuyenteClaseRepository : RepositoryMaster<ContribuyenteClaseModel>, ISqlContribuyenteClaseRepository {
        public SqlFbContribuyenteClaseRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Insert(ContribuyenteClaseModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO dircla ( dircla_id, dircla_a, dircla_cla, dircla_c, dircla_fm, dircla_dep_id) 
                			                VALUES (@dircla_id,@dircla_a,@dircla_cla,@dircla_c,@dircla_fm,@dircla_dep_id);"
            };
            item.IdClase = this.Max("dircla_id");
            sqlCommand.Parameters.AddWithValue("@dircla_id", item.IdClase);
            sqlCommand.Parameters.AddWithValue("@dircla_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@dircla_cla", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@dircla_c", item.Clase);
            sqlCommand.Parameters.AddWithValue("@dircla_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@dircla_dep_id", item.IdDepto);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdClase;
            return 0;
        }

        public int Update(ContribuyenteClaseModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE dircla 
                			SET dircla_a = @dircla_a, dircla_cla = @dircla_cla, dircla_c = @dircla_c, dircla_fm = @dircla_fm, dircla_dep_id = @dircla_dep_id
                			WHERE dircla_id = @dircla_id;"
            };
            sqlCommand.Parameters.AddWithValue("@dircla_id", item.IdClase);
            sqlCommand.Parameters.AddWithValue("@dircla_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@dircla_cla", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@dircla_c", item.Clase);
            sqlCommand.Parameters.AddWithValue("@dircla_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@dircla_dep_id", item.IdDepto);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE dircla SET dircla_a = 0 WHERE dircla_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ContribuyenteClaseModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM dircla WHERE dircla_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.GetMapper(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<ContribuyenteClaseModel> GetList() {
            FbCommand sqlCommand = new FbCommand() {
                CommandText = "select * from dircla where dircla_a = 1 order by dircla_cla"
            };

            return this.GetMapper(sqlCommand);
        }
    }
}
