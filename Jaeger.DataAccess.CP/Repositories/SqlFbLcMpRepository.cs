﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbLcMpRepository : RepositoryMaster<LcMpModel>, ISqlLcMpRepository {
        public SqlFbLcMpRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Insert(LcMpModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO lcmp (lcmp_id, lcmp_a, lcmp_pro_id, lcmp_mp_id, lcmp_mp4_id, lcmp_can, lcmp_mul, lcmp_lib, lcmp_fyv, lcmp_tra, lcmp_trb, lcmp_trc, lcmp_tpa, lcmp_tpb, lcmp_tpc, lcmp_mra, lcmp_mrb, lcmp_mpa, lcmp_mpb, lcmp_usu_fm, lcmp_fm, lcmp_com_id) 
                			VALUES (@lcmp_id, @lcmp_a, @lcmp_pro_id, @lcmp_mp_id, @lcmp_mp4_id, @lcmp_can, @lcmp_mul, @lcmp_lib, @lcmp_fyv, @lcmp_tra, @lcmp_trb, @lcmp_trc, @lcmp_tpa, @lcmp_tpb, @lcmp_tpc, @lcmp_mra, @lcmp_mrb, @lcmp_mpa, @lcmp_mpb, @lcmp_usu_fm, @lcmp_fm, @lcmp_com_id)"
            };
            item.IdLCMP = this.Max("LCMP_ID");
            sqlCommand.Parameters.AddWithValue("@lcmp_id", item.IdLCMP);
            sqlCommand.Parameters.AddWithValue("@lcmp_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@lcmp_pro_id", item.LCMP_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@lcmp_mp_id", item.LCMP_MP_ID);
            sqlCommand.Parameters.AddWithValue("@lcmp_mp4_id", item.LCMP_MP4_ID);
            sqlCommand.Parameters.AddWithValue("@lcmp_can", item.Factor);
            sqlCommand.Parameters.AddWithValue("@lcmp_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@lcmp_lib", item.LCMP_LIB);
            sqlCommand.Parameters.AddWithValue("@lcmp_fyv", item.LCMP_FYV);
            sqlCommand.Parameters.AddWithValue("@lcmp_tra", item.LCMP_TRA);
            sqlCommand.Parameters.AddWithValue("@lcmp_trb", item.LCMP_TRB);
            sqlCommand.Parameters.AddWithValue("@lcmp_trc", item.LCMP_TRC);
            sqlCommand.Parameters.AddWithValue("@lcmp_tpa", item.LCMP_TPA);
            sqlCommand.Parameters.AddWithValue("@lcmp_tpb", item.LCMP_TPB);
            sqlCommand.Parameters.AddWithValue("@lcmp_tpc", item.LCMP_TPC);
            sqlCommand.Parameters.AddWithValue("@lcmp_mra", item.LCMP_MRA);
            sqlCommand.Parameters.AddWithValue("@lcmp_mrb", item.LCMP_MRB);
            sqlCommand.Parameters.AddWithValue("@lcmp_mpa", item.LCMP_MPA);
            sqlCommand.Parameters.AddWithValue("@lcmp_mpb", item.LCMP_MPB);
            sqlCommand.Parameters.AddWithValue("@lcmp_usu_fm", item.LCMP_USU_FM);
            sqlCommand.Parameters.AddWithValue("@lcmp_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@lcmp_com_id", item.LCMP_COM_ID);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdLCMP;
            return 0;
        }

        public int Update(LcMpModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE lcmp 
                			SET lcmp_a = @lcmp_a, lcmp_pro_id = @lcmp_pro_id, lcmp_mp_id = @lcmp_mp_id, lcmp_mp4_id = @lcmp_mp4_id, lcmp_can = @lcmp_can, lcmp_mul = @lcmp_mul, lcmp_lib = @lcmp_lib, lcmp_fyv = @lcmp_fyv, lcmp_tra = @lcmp_tra, lcmp_trb = @lcmp_trb, lcmp_trc = @lcmp_trc, lcmp_tpa = @lcmp_tpa, lcmp_tpb = @lcmp_tpb, lcmp_tpc = @lcmp_tpc, lcmp_mra = @lcmp_mra, lcmp_mrb = @lcmp_mrb, lcmp_mpa = @lcmp_mpa, lcmp_mpb = @lcmp_mpb, lcmp_usu_fm = @lcmp_usu_fm, lcmp_fm = @lcmp_fm, lcmp_com_id = @lcmp_com_id 
                			WHERE lcmp_id = @lcmp_id;"
            };
            sqlCommand.Parameters.AddWithValue("@lcmp_id", item.IdLCMP);
            sqlCommand.Parameters.AddWithValue("@lcmp_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@lcmp_pro_id", item.LCMP_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@lcmp_mp_id", item.LCMP_MP_ID);
            sqlCommand.Parameters.AddWithValue("@lcmp_mp4_id", item.LCMP_MP4_ID);
            sqlCommand.Parameters.AddWithValue("@lcmp_can", item.Factor);
            sqlCommand.Parameters.AddWithValue("@lcmp_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@lcmp_lib", item.LCMP_LIB);
            sqlCommand.Parameters.AddWithValue("@lcmp_fyv", item.LCMP_FYV);
            sqlCommand.Parameters.AddWithValue("@lcmp_tra", item.LCMP_TRA);
            sqlCommand.Parameters.AddWithValue("@lcmp_trb", item.LCMP_TRB);
            sqlCommand.Parameters.AddWithValue("@lcmp_trc", item.LCMP_TRC);
            sqlCommand.Parameters.AddWithValue("@lcmp_tpa", item.LCMP_TPA);
            sqlCommand.Parameters.AddWithValue("@lcmp_tpb", item.LCMP_TPB);
            sqlCommand.Parameters.AddWithValue("@lcmp_tpc", item.LCMP_TPC);
            sqlCommand.Parameters.AddWithValue("@lcmp_mra", item.LCMP_MRA);
            sqlCommand.Parameters.AddWithValue("@lcmp_mrb", item.LCMP_MRB);
            sqlCommand.Parameters.AddWithValue("@lcmp_mpa", item.LCMP_MPA);
            sqlCommand.Parameters.AddWithValue("@lcmp_mpb", item.LCMP_MPB);
            sqlCommand.Parameters.AddWithValue("@lcmp_usu_fm", item.LCMP_USU_FM);
            sqlCommand.Parameters.AddWithValue("@lcmp_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@lcmp_com_id", item.LCMP_COM_ID);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE lcmp SET lcmp_a > 0 WHERE lcmp_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public LcMpModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "lcmp", "lcmp")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LcMpModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<LcMpModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "lcmp", "lcmp")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LcMpModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<LcMpDetailModel> GetList(int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "lcmp", "lcmp")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LcMpDetailModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<LcMpModelView> GetListView(int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from LcMp, Com3, ComMod where LcMp_a > 0 and LcMp_Com_ID = Com3_ID and LcMp_MP4_ID = ComMod_ID order by LcMp_ID, Com3_Cod, LcMp_ID"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LcMpModelView>();
            return mapper.Map(tabla).ToList();
        }
    }
}
