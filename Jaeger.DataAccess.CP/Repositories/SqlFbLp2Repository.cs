﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbLp2Repository : RepositoryMaster<LP2Model>, ISqlLp2Repository {
        protected ISqlLc2Repository lc2Repository;
        protected ISqlLcProRepository lcProRepository;

        public SqlFbLp2Repository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.lc2Repository = new SqlFbLc2Repository(configuracion);
            this.lcProRepository = new SqlFbLcProRepository(configuracion);
        }

        public int Insert(LP2Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO lp2 (lp2_id, lp2_a, lp2_lp1_id, lp2_lc1_id, lp2_fm, lp2_com_id, lp2_com1_id) 
                			VALUES (@lp2_id, lp2_a, lp2_lp1_id, lp2_lc1_id, lp2_fm, lp2_com_id, lp2_com1_id)"
            };
            item.IdLP2 = this.Max("LP2_ID");
            sqlCommand.Parameters.AddWithValue("@lp2_id", item.IdLP2);
            sqlCommand.Parameters.AddWithValue("@lp2_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@lp2_lp1_id", item.LP2_LP1_ID);
            sqlCommand.Parameters.AddWithValue("@lp2_lc1_id", item.LP2_LC1_ID);
            sqlCommand.Parameters.AddWithValue("@lp2_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@lp2_com_id", item.LP2_COM_ID);
            sqlCommand.Parameters.AddWithValue("@lp2_com1_id", item.LP2_COM1_ID);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdLP2;
            return 0;
        }

        public int Update(LP2Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE lp2 
                			SET lp2_a = @lp2_a, lp2_lp1_id = @lp2_lp1_id, lp2_lc1_id = @lp2_lc1_id, lp2_fm = @lp2_fm, lp2_com_id = @lp2_com_id, lp2_com1_id = @lp2_com1_id 
                			WHERE lp2_id = @lp2_id;"
            };

            sqlCommand.Parameters.AddWithValue("@lp2_id", item.IdLP2);
            sqlCommand.Parameters.AddWithValue("@lp2_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@lp2_lp1_id", item.LP2_LP1_ID);
            sqlCommand.Parameters.AddWithValue("@lp2_lc1_id", item.LP2_LC1_ID);
            sqlCommand.Parameters.AddWithValue("@lp2_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@lp2_com_id", item.LP2_COM_ID);
            sqlCommand.Parameters.AddWithValue("@lp2_com1_id", item.LP2_COM1_ID);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE lp2 SET lp2_a = 0 WHERE lp2_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public LP2Model GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from lp2 where lp2_id = @id"
            };

            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LP2Model>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<LP2Model> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "select * from lp2 where lp2 _a > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LP2Model>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de subproductos por indice de relacion (LP2_Com1_ID)
        /// </summary>
        /// <param name="indexs">array de indices LP2_Com1_ID</param>
        public IEnumerable<LP2DetailModel> GetList(int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from lp2 where lp2_a > 0 and LP2_Com1_ID in (@indexs)"
            };

            if (indexs.Length > 0) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", string.Join(",", indexs));
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<LP2DetailModel>();
                var result = mapper.Map(tabla).ToList();
                var requisitos = this.lc2Repository.GetList(new int[] { 0 }).ToList();
                var ids = requisitos.Select(it => it.IdLC1).ToArray();
                var lproduccion = this.lcProRepository.GetList(ids).ToList();

                for (int i = 0; i < result.Count; i++) {
                    result[i].Requisitos = new System.ComponentModel.BindingList<Lc2DetailModel>(requisitos.Where(it => it.IdCom == result[i].LP2_COM_ID).ToList());
                    result[i].LineaProduccion = new System.ComponentModel.BindingList<LineaProduccionDetailModel>(lproduccion.Where(it => it.IdCom2 == result[i].LP2_COM_ID).ToList());
                }

                return result;
            }

            return null;
        }

        /// <summary>
        /// obtener vista de subproductos por indice de relacion (LP2_Com1_ID)
        /// </summary>
        /// <param name="indexs">array de indices LP2_Com1_ID</param>
        public IEnumerable<LP2ModelView> GetListView(int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from lp2 where lp2_a > 0 and LP2_Com1_ID in (@indexs)"
            };

            if (indexs.Length > 0) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", string.Join(",", indexs));
                var tabla = this.ExecuteReader(sqlCommand);
                var mapper = new DataNamesMapper<LP2ModelView>();
                var result = mapper.Map(tabla).ToList();
                var requisitos = this.lc2Repository.GetList(new int[] { 0 }).ToList();
                var ids = requisitos.Select(it => it.IdLC1).ToArray();
                var lproduccion = this.lcProRepository.GetListView(ids).ToList();

                for (int i = 0; i < result.Count; i++) {
                    result[i].Especificaciones = new System.ComponentModel.BindingList<Lc2DetailModel>(requisitos.Where(it => it.IdCom == result[i].LP2_COM_ID).ToList());
                    result[i].LineaProduccion = new System.ComponentModel.BindingList<LineaProduccionView>(lproduccion.Where(it => it.IdCom2 == result[i].LP2_COM_ID).ToList());
                }

                return result;
            }

            return null;
        }

        public IEnumerable<LP2DetailModel> Prueba(int idCom) {
            var sqlCommand = new FbCommand {
                CommandText = @"Select LP2_ID,1,0,Com_ID,''
                                From Com
                                join LP2 on LP2_Com_ID = Com_ID
                                Where Com_A > 0 And LP2_A > 0
                                AND LP2_Com1_ID = @index;"
            };

            sqlCommand.Parameters.AddWithValue("@index", idCom);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LP2DetailModel>();
            return mapper.Map(tabla).ToList();
        }
    }
}
