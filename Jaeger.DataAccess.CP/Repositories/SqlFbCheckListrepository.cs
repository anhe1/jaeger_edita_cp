﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de lista de verificacion del proceso en CP (Chklst)
    /// </summary>
    public class SqlFbCheckListRepository : RepositoryMaster<CheckListModel>, ISqlCheckListRepository {
        protected ISqlSchklstRepository schklstRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuracion"></param>
        public SqlFbCheckListRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.schklstRepository = new SqlFbSchklstRepository(configuracion);
        }

        /// <summary>
        /// insertar registro
        /// </summary>
        /// <param name="item"></param>
        /// <returns>retorna el nuevo indice creado</returns>
        public int Insert(CheckListModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CHKLST (CHKLST_A, CHKLST_PROC_ID, CHKLST_SEC, CHKLST_DESC, CHKLST_FN, CHKLST_USU_N)
                                           VALUES (@CHKLST_A,@CHKLST_PROC_ID,@CHKLST_SEC,@CHKLST_DESC,@CHKLST_FN,@CHKLST_USU_N)"
            };
            item.IdCheckList = this.Max("ChkLst_A");
            sqlCommand.Parameters.AddWithValue("@ChkLst_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@ChkLst_Proc_Id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@ChkLst_Sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@ChkLst_Desc", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@ChkLst_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@ChkLst_USU_N", item.Creo);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdCheckList;
            return 0;
        }

        /// <summary>
        /// actualizar registro
        /// </summary>
        /// <param name="item"></param>
        /// <returns>devuelve la cantidad de filas actualizadas</returns>
        public int Update(CheckListModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CHKLST SET CHKLST_A = @CHKLST_A, CHKLST_PROC_ID = @CHKLST_PROC_ID, CHKLST_SEC = @CHKLST_SEC, CHKLST_DESC = @CHKLST_DESC, CHKLST_USU_M = @CHKLST_USU_M, CHKLST_FM = @CHKLST_FM WHERE CHKLST_ID = @CHKLST_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@chklst_id", item.IdCheckList);
            sqlCommand.Parameters.AddWithValue("@chklst_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@chklst_proc_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@chklst_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@chklst_desc", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@chklst_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@chklst_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CHKLST SET CHKLST_A = 0 WHERE CHKLST_ID = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CheckListModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM CHKLST WHERE CHKLST_ID = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CheckListModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        /// <summary>
        /// listado de lista de verificacion simple
        /// </summary>
        public IEnumerable<CheckListModel> GetList() {
            var sqlCheckList = new FbCommand {
                CommandText = @"SELECT * FROM CHKLST WHERE CHKLST_A > 0"
            };
            var tabla = this.ExecuteReader(sqlCheckList);
            var mapperCheckList = new DataNamesMapper<CheckListModel>();
            return mapperCheckList.Map(tabla).ToList();
        }

        /// <summary>
        /// listado detail de las lista de verificacion con lista de posibles fallas
        /// </summary>
        public IEnumerable<CheckListDetailModel> GetList(int[] indexs) {
            var sqlCheckList = new FbCommand {
                CommandText = @"SELECT * FROM CHKLST WHERE CHKLST_A > 0"
            };

            var tabla = this.ExecuteReader(sqlCheckList);
            var mapperCheckList = new DataNamesMapper<CheckListDetailModel>();
            var checkList = mapperCheckList.Map(tabla).ToList();

            var checkSub = this.schklstRepository.GetList();

            for (int i = 0; i < checkList.Count; i++) {
                checkList[i].PosibleFalla = new BindingList<ChekListFallaModel>(checkSub.Where(it => it.IdProceso == checkList[i].IdCheckList).ToList());
            }

            return checkList;
        }
    }
}
