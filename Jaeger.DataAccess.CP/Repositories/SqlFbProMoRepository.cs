﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// Proceso Mano de Obra (PROMO)
    /// </summary>
    public class SqlFbProMoRepository : RepositoryMaster<ProcesoManoObraModel>, ISqlProMoRepository {
        public SqlFbProMoRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE promo SET promo_a = 0 WHERE promo_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ProcesoManoObraModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from promo where promo_id=@id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoManoObraModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ProcesoManoObraModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "select * from promo where promo_a > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoManoObraModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<ProcesoManoObraModel> GetListBy() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from PROMO where PROMO_A > 0"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoManoObraModel>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(ProcesoManoObraModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO promo (promo_id, promo_a, promo_pro_id, promo_pue_id, promo_fm) 
                			VALUES (@values)"
            };
            item.IdPromo = this.Max("PROMO_IDPROMO_ID");
            sqlCommand.Parameters.AddWithValue("@promo_id", item.IdPromo);
            sqlCommand.Parameters.AddWithValue("@promo_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@promo_pro_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@promo_pue_id", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@promo_fm", item.FechaModifica);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdPromo;
            return 0;
        }

        public int Update(ProcesoManoObraModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE promo 
                			SET promo_a = @promo_a, promo_pro_id = @promo_pro_id, promo_pue_id = @promo_pue_id, promo_fm = @promo_fm 
                			WHERE promo_id = @promo_id;"
            };

            sqlCommand.Parameters.AddWithValue("@promo_id", item.IdPromo);
            sqlCommand.Parameters.AddWithValue("@promo_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@promo_pro_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@promo_pue_id", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@promo_fm", item.FechaModifica);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
