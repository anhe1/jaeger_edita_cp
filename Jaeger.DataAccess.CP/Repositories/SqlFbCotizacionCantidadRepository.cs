﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using System.ComponentModel;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// cantidades y precios (COTPRD)
    /// </summary>
    public class SqlFbCotizacionCantidadRepository : RepositoryMaster<CotizacionCantidadPrecioModel>, ISqlCotizacionCantidadRepository {
        protected ISqlCotComRepository cotComRepository;
        public SqlFbCotizacionCantidadRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.cotComRepository = new SqlFbCotComRepository(configuracion);
        }

        public int Insert(CotizacionCantidadPrecioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"insert into COTPRD (cotprd_id, cotprd_a, cotprd_cot_id, cotprd_can, CotPrd_Uni, COTPRD_Uti) values (@cotprd_id, 1, @cotprd_cot_id, @cotprd_can, @CotPrd_Uni, @COTPRD_Uti)"
            };
            item.IdCotPrd = this.Max("cotprd_id");
            sqlCommand.Parameters.AddWithValue("@cotprd_id", item.IdCotPrd);
            sqlCommand.Parameters.AddWithValue("@cotprd_cot_id", item.IdCotizacion);
            sqlCommand.Parameters.AddWithValue("@cotprd_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@CotPrd_uni", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@COTPRD_uti", item.Utilidad);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdCotPrd;
            return 0;
        }

        public int Update(CotizacionCantidadPrecioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"update COTPRD set cotprd_a = @cotprd_a, cotprd_cot_id=@cotprd_cot_id,cotprd_prd_id=@cotprd_prd_id,cotprd_nom=@cotprd_nom,cotprd_can=@cotprd_can, COTPRD_Uti=@COTPRD_Uti where cotprd_id=@index"
            };

            sqlCommand.Parameters.AddWithValue("@cotprd_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotprd_cot_id", item.IdCotizacion);
            sqlCommand.Parameters.AddWithValue("@cotprd_prd_id", item.COTPRD_PRD_ID);
            sqlCommand.Parameters.AddWithValue("@cotprd_nom", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@cotprd_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@COTPRD_uti", item.Utilidad);
            sqlCommand.Parameters.AddWithValue("@index", item.IdCotPrd);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "update cotprd set cotprd_a = 0 where cotprd_id = @index;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CotizacionCantidadPrecioModel GetById(int index) {
            var sqlCantidadPrecios = new FbCommand {
                CommandText = @"select * from COTPRD where COTPRD_A > 0 and COTPRD_COT_ID = @index order by COTPRD_Can"
            };
            sqlCantidadPrecios.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCantidadPrecios);
            var mapper = new DataNamesMapper<CotizacionCantidadPrecioModel>();
            var result = mapper.Map(tabla).SingleOrDefault();
            return result;
        }

        public IEnumerable<CotizacionCantidadPrecioModel> GetList() {
            var sqlCantidadPrecios = new FbCommand {
                CommandText = @"select * from COTPRD where COTPRD_A > 0 order by COTPRD_Can"
            };
            var tabla = this.ExecuteReader(sqlCantidadPrecios);
            var mapper = new DataNamesMapper<CotizacionCantidadPrecioModel>();
            var result = mapper.Map(tabla).ToList();
            return result;
        }

        /// <summary>
        /// obtener listado de detalle de cantidades y precios de una cotizacion o presupuesto
        /// </summary>
        /// <param name="index">indice de la catizacion o presupuesto</param>
        /// <returns>IEnumerable<CotizacionCantidadPrecioDetailModel></returns>
        public IEnumerable<CotizacionCantidadPrecioDetailModel> GetList(int index) {
            var sqlCantidadPrecios = new FbCommand {
                CommandText = @"select * from COTPRD where COTPRD_A > 0 and COTPRD_COT_ID = @index order by COTPRD_Can"
            };
            sqlCantidadPrecios.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCantidadPrecios);
            var mapper = new DataNamesMapper<CotizacionCantidadPrecioDetailModel>();
            var result2 = mapper.Map(tabla).ToList();

            if (result2 != null) {
                var allids = result2.Select(it => it.IdCotPrd).ToArray();
                var costos = this.cotComRepository.GetList1(allids);

                for (int i = 0; i < result2.Count; i++) {
                    result2[i].Costos = new BindingList<CotComDetailModelView>(costos.Where(it => it.COTCOM_COTPRD_ID == result2[i].IdCotPrd).ToList());
                }
            }

            return result2;
        }

        public CotizacionCantidadPrecioDetailModel Save(CotizacionCantidadPrecioDetailModel item) {
            if (item.IdCotPrd == 0) {
                item.IdCotPrd = this.Insert(item);
            } else {
                this.Update(item);
            }
            return item;
        }
    }
}
