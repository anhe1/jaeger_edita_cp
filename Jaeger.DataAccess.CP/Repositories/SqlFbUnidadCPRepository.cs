﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Empresa.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// Catálogo Unidades Almacén
    /// </summary>
    public class SqlFbUnidadCPRepository : RepositoryMaster<UnidadModel>, ISqlUnidadRepository {
        public SqlFbUnidadCPRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE mpuni SET mpuni_a = 0 WHERE mpuni_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public UnidadModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select mpuni.* from mpuni where mpuni_id =@id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<UnidadModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<UnidadModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from MPUNI where MPUNI_A = 1"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<UnidadModel>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(UnidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO mpuni (mpuni_id, mpuni_a, mpuni_nom, mpuni_usu_fm, mpuni_fm) 
                			VALUES (@mpuni_id, @mpuni_a, @mpuni_nom, @mpuni_usu_fm, @mpuni_fm)"
            };
            item.IdUnidad = this.Max("ID");
            sqlCommand.Parameters.AddWithValue("@mpuni_id", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@mpuni_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@mpuni_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@mpuni_usu_fm", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@mpuni_fm", item.FechaModifica);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdUnidad;
            return 0;
        }

        public int Update(UnidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE mpuni 
                			SET mpuni_a = @mpuni_a, mpuni_nom = @mpuni_nom, mpuni_usu_fm = @mpuni_usu_fm, mpuni_fm = @mpuni_fm 
                			WHERE mpuni_id = @mpuni_id;"
            };

            sqlCommand.Parameters.AddWithValue("@mpuni_id", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@mpuni_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@mpuni_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@mpuni_usu_fm", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@mpuni_fm", item.FechaModifica);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
