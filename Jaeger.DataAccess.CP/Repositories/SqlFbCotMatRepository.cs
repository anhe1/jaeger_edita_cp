﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbCotMatRepository : RepositoryMaster<CotMatModel>, ISqlCotMatRepository {
        public SqlFbCotMatRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Insert(CotMatModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO cotmat (cotmat_id, cotmat_a, cotmat_mp_id, cotmat_com_id, cotmat_mp4_id, cotmat_prdmat_id3, cotmat_prdmat_id, cotmat_cotprd_id, cotmat_cotpro_id, cotmat_can, cotmat_mul, cotmat_can_mat, cotmat_$_mp, cotmat_$_cns, cotmat_usu_fm, cotmat_lib, cotmat_fyv, cotmat_tra, cotmat_trb, cotmat_trc, cotmat_tpa, cotmat_tpb, cotmat_tpc, cotmat_mra, cotmat_mrb, cotmat_mpa, cotmat_mpb, cotmat_$uni, cotmat_fm)
                			VALUES (@cotmat_id, @cotmat_a, @cotmat_mp_id, @cotmat_com_id, @cotmat_mp4_id, @cotmat_prdmat_id3, @cotmat_prdmat_id, @cotmat_cotprd_id, @cotmat_cotpro_id, @cotmat_can, @cotmat_mul, @cotmat_can_mat, @cotmat_$_mp, @cotmat_$_cns, @cotmat_usu_fm, @cotmat_lib, @cotmat_fyv, @cotmat_tra, @cotmat_trb, @cotmat_trc, @cotmat_tpa, @cotmat_tpb, @cotmat_tpc, @cotmat_mra, @cotmat_mrb, @cotmat_mpa, @cotmat_mpb, @cotmat_$uni, @cotmat_fm)"
            };
            item.IdCotMat = this.Max("COTMAT_ID");
            sqlCommand.Parameters.AddWithValue("@cotmat_id", item.IdCotMat);
            sqlCommand.Parameters.AddWithValue("@cotmat_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotmat_mp_id", item.COTMAT_MP_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_com_id", item.COTMAT_COM_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_mp4_id", item.COTMAT_MP4_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_prdmat_id3", item.COTMAT_PRDMAT_ID3);
            sqlCommand.Parameters.AddWithValue("@cotmat_prdmat_id", item.COTMAT_PRDMAT_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_cotprd_id", item.COTMAT_COTPRD_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_cotpro_id", item.COTMAT_COTPRO_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_can", item.FactorConversion);
            sqlCommand.Parameters.AddWithValue("@cotmat_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@cotmat_can_mat", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@cotmat_$_mp", item.MateriaPrima);
            sqlCommand.Parameters.AddWithValue("@cotmat_$_cns", item.Consumibles);
            sqlCommand.Parameters.AddWithValue("@cotmat_usu_fm", item.COTMAT_USU_FM);
            sqlCommand.Parameters.AddWithValue("@cotmat_lib", item.COTMAT_LIB);
            sqlCommand.Parameters.AddWithValue("@cotmat_fyv", item.COTMAT_FYV);
            sqlCommand.Parameters.AddWithValue("@cotmat_tra", item.COTMAT_TRA);
            sqlCommand.Parameters.AddWithValue("@cotmat_trb", item.COTMAT_TRB);
            sqlCommand.Parameters.AddWithValue("@cotmat_trc", item.COTMAT_TRC);
            sqlCommand.Parameters.AddWithValue("@cotmat_tpa", item.COTMAT_TPA);
            sqlCommand.Parameters.AddWithValue("@cotmat_tpb", item.COTMAT_TPB);
            sqlCommand.Parameters.AddWithValue("@cotmat_tpc", item.COTMAT_TPC);
            sqlCommand.Parameters.AddWithValue("@cotmat_mra", item.COTMAT_MRA);
            sqlCommand.Parameters.AddWithValue("@cotmat_mrb", item.COTMAT_MRB);
            sqlCommand.Parameters.AddWithValue("@cotmat_mpa", item.COTMAT_MPA);
            sqlCommand.Parameters.AddWithValue("@cotmat_mpb", item.COTMAT_MPB);
            sqlCommand.Parameters.AddWithValue("@cotmat_$uni", item.COTMAT_UNI);
            sqlCommand.Parameters.AddWithValue("@cotmat_fm", item.FechaModifica);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdCotMat;
            return 0;
        }

        public int Update(CotMatModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotmat 
                			SET cotmat_a = @cotmat_a, cotmat_mp_id = @cotmat_mp_id, cotmat_com_id = @cotmat_com_id, cotmat_mp4_id = @cotmat_mp4_id, cotmat_prdmat_id3 = @cotmat_prdmat_id3, cotmat_prdmat_id = @cotmat_prdmat_id, cotmat_cotprd_id = @cotmat_cotprd_id, cotmat_cotpro_id = @cotmat_cotpro_id, cotmat_can = @cotmat_can, cotmat_mul = @cotmat_mul, cotmat_can_mat = @cotmat_can_mat, cotmat_$_mp = @cotmat_$_mp, cotmat_$_cns = @cotmat_$_cns, cotmat_usu_fm = @cotmat_usu_fm, cotmat_lib = @cotmat_lib, cotmat_fyv = @cotmat_fyv, cotmat_tra = @cotmat_tra, cotmat_trb = @cotmat_trb, cotmat_trc = @cotmat_trc, cotmat_tpa = @cotmat_tpa, cotmat_tpb = @cotmat_tpb, cotmat_tpc = @cotmat_tpc, cotmat_mra = @cotmat_mra, cotmat_mrb = @cotmat_mrb, cotmat_mpa = @cotmat_mpa, cotmat_mpb = @cotmat_mpb, cotmat_$uni = @cotmat_$uni, cotmat_fm = @cotmat_fm 
                			WHERE cotmat_id = @cotmat_id;"
            };
            sqlCommand.Parameters.AddWithValue("@cotmat_id", item.IdCotMat);
            sqlCommand.Parameters.AddWithValue("@cotmat_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotmat_mp_id", item.COTMAT_MP_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_com_id", item.COTMAT_COM_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_mp4_id", item.COTMAT_MP4_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_prdmat_id3", item.COTMAT_PRDMAT_ID3);
            sqlCommand.Parameters.AddWithValue("@cotmat_prdmat_id", item.COTMAT_PRDMAT_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_cotprd_id", item.COTMAT_COTPRD_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_cotpro_id", item.COTMAT_COTPRO_ID);
            sqlCommand.Parameters.AddWithValue("@cotmat_can", item.FactorConversion);
            sqlCommand.Parameters.AddWithValue("@cotmat_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@cotmat_can_mat", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@cotmat_$_mp", item.MateriaPrima);
            sqlCommand.Parameters.AddWithValue("@cotmat_$_cns", item.Consumibles);
            sqlCommand.Parameters.AddWithValue("@cotmat_usu_fm", item.COTMAT_USU_FM);
            sqlCommand.Parameters.AddWithValue("@cotmat_lib", item.COTMAT_LIB);
            sqlCommand.Parameters.AddWithValue("@cotmat_fyv", item.COTMAT_FYV);
            sqlCommand.Parameters.AddWithValue("@cotmat_tra", item.COTMAT_TRA);
            sqlCommand.Parameters.AddWithValue("@cotmat_trb", item.COTMAT_TRB);
            sqlCommand.Parameters.AddWithValue("@cotmat_trc", item.COTMAT_TRC);
            sqlCommand.Parameters.AddWithValue("@cotmat_tpa", item.COTMAT_TPA);
            sqlCommand.Parameters.AddWithValue("@cotmat_tpb", item.COTMAT_TPB);
            sqlCommand.Parameters.AddWithValue("@cotmat_tpc", item.COTMAT_TPC);
            sqlCommand.Parameters.AddWithValue("@cotmat_mra", item.COTMAT_MRA);
            sqlCommand.Parameters.AddWithValue("@cotmat_mrb", item.COTMAT_MRB);
            sqlCommand.Parameters.AddWithValue("@cotmat_mpa", item.COTMAT_MPA);
            sqlCommand.Parameters.AddWithValue("@cotmat_mpb", item.COTMAT_MPB);
            sqlCommand.Parameters.AddWithValue("@cotmat_$uni", item.COTMAT_UNI);
            sqlCommand.Parameters.AddWithValue("@cotmat_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotmat SET cotmat_a = 0 WHERE cotmat_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CotMatModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "cotmat", "cotmat")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotMatModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<CotMatModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "cotmat", "cotmat")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotMatModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<CotMatDetailModel> GetList(int[] index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM cotmat WHERE cotmat_a > 0 and COTMAT_COTPRO_ID in (@indexs)"
            };

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", string.Join(",", index.ToArray()));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotMatDetailModel>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<CotMatDetailModelView> GetList1(int[] index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT cotmat.*,
                                Com3_Cod, Com3_N1, Com3_N2, Com3_N3,
                                ComMod_Mod, ComMod_nom, ComMod_Mar, ComMod_UniNom, ComMod_$Uni, ComMod_Mul
                                FROM cotmat, Com3, ComMod
                                WHERE cotmat_a > 0 
                                and COTMAT_Com_ID = Com3_ID
                                and COTMAT_MP4_ID = ComMod_ID
                                and COTMAT_COTPRO_ID in (@indexs)"
            };

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", string.Join(",", index.ToArray()));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotMatDetailModelView>();
            return mapper.Map(tabla).ToList();
        }
    }
}
