﻿using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de areas, departamentos EDP1
    /// </summary>
    public class SqlFbDepartamentosRepository : RepositoryMaster<DepartamentoModel>, ISqlDepartamentosRepository {
        protected ISqlEquiposMaquinaRepository equiposMaquinaRepository;

        public SqlFbDepartamentosRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.equiposMaquinaRepository = new SqlFbEquiposMaquinaRepository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp1 SET edp1_a = 0 WHERE edp1_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public DepartamentoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from edp1 where edp1_id = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<DepartamentoModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        /// <summary>
        /// listado simple de areas y departamentos
        /// </summary>
        public IEnumerable<DepartamentoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from edp1 where edp1_a > 0 order by edp1_nom"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<DepartamentoModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// lisatado de departamentos, incluye centros productivos
        /// </summary>
        /// <param name="onlyActive">solor registros activos</param>
        public IEnumerable<DepartamentoDetailModel> GetList(bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from edp1 where edp1_a = 1 order by edp1_nom"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<DepartamentoDetailModel>();
            var result = mapper.Map(tabla).ToList();
            var ids = result.Select(it => it.IdDepto).ToArray();
            var centros = this.equiposMaquinaRepository.GetList(true, ids);

            for (int i = 0; i < result.Count; i++) {
                result[i].Centros = new BindingList<SeccionCentroDetailModel>(centros.Where(it => it.IdDepto == result[i].IdDepto).ToList());
            }

            return result;
        }

        public int Insert(DepartamentoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO edp1 (edp1_id, edp1_a, edp1_nom, edp1_are, edp1_sec, edp1_usu_fm, edp1_fm) 
                			VALUES (@edp1_id, @edp1_a, @edp1_nom, @edp1_are, @edp1_sec, @edp1_usu_fm, @edp1_fm)"
            };
            item.IdDepto = this.Max("IEDP1_ID");
            sqlCommand.Parameters.AddWithValue("@edp1_id", item.IdDepto);
            sqlCommand.Parameters.AddWithValue("@edp1_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp1_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@edp1_are", item.Area);
            sqlCommand.Parameters.AddWithValue("@edp1_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@edp1_usu_fm", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@edp1_fm", item.FechaModifica);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdDepto;
            return 0;
        }

        public int Update(DepartamentoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp1 
                			SET edp1_a = @edp1_a, edp1_nom = @edp1_nom, edp1_are = @edp1_are, edp1_sec = @edp1_sec, edp1_usu_fm = @edp1_usu_fm, edp1_fm = @edp1_fm 
                			WHERE edp1_id = @edp1_id;"
            };

            sqlCommand.Parameters.AddWithValue("@edp1_id", item.IdDepto);
            sqlCommand.Parameters.AddWithValue("@edp1_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp1_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@edp1_are", item.Area);
            sqlCommand.Parameters.AddWithValue("@edp1_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@edp1_usu_fm", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@edp1_fm", item.FechaModifica);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
