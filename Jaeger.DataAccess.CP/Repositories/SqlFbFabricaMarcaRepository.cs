﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Empresa.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio del Catálogo Fábricas y Marcas
    /// </summary>
    public class SqlFbFabricaMarcaRepository : RepositoryMaster<FabricaMarcaModel>, ISqlFabricaMarcaRepository {
        public SqlFbFabricaMarcaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Insert(FabricaMarcaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO MPMAR (MPMAR_ID, MPMAR_A, MPMAR_NOM, MPMAR_USU_FM, MPMAR_FM) values (@MPMAR_ID, @MPMAR_A, @MPMAR_NOM, @MPMAR_USU_FM, @MPMAR_FM)"
            };
            item.IdMarca = this.Max("mpmar_id");
            sqlCommand.Parameters.AddWithValue("@mpmar_id", item.IdMarca);
            sqlCommand.Parameters.AddWithValue("@MPMAR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@MPMAR_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@MPMAR_USU_FM", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@MPMAR_FM", DateTime.Now);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdMarca;
            return 0;
        }

        public int Update(FabricaMarcaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE mpmar 
                			SET mpmar_a = @mpmar_a, mpmar_nom = @mpmar_nom, mpmar_usu_fm = @mpmar_usu_fm, mpmar_fm = @mpmar_fm 
                			WHERE mpmar_id = @mpmar_id;"
            };
            sqlCommand.Parameters.AddWithValue("@mpmar_id", item.IdMarca);
            sqlCommand.Parameters.AddWithValue("@MPMAR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@MPMAR_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@MPMAR_USU_FM", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@MPMAR_FM", DateTime.Now);
            return this.ExecuteTransaction(sqlCommand);
        }
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"update mpmar set mpmar_a = 0 where mpmar_id = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteScalar(sqlCommand) > 0;
        }

        public FabricaMarcaModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM mpmar WHERE mpmar_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<FabricaMarcaModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<FabricaMarcaModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from MPMAR where MPMAR_A = 1"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<FabricaMarcaModel>();
            return mapper.Map(tabla).ToList();
        }
    }
}
