﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Empresa.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de productos y servicios (tabla MP4)
    /// </summary>
    public class SqlFbProductosServiciosRepository : RepositoryMaster<MateriaPrima4Model>, ISqlProductosServiciosRepository {
        public SqlFbProductosServiciosRepository(DataBaseConfiguracion configuracion) : base(configuracion) {

        }

        public int Insert(MateriaPrima4Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO MP4 ( MP4_ID, MP4_A, MP4_MOD, MP4_NOM, MP4_TIE_EST, MP4_SEC, MP4_MES, MP4_DIA, MP4_TCA, MP4_TCB, MP4_TCC, MP4_VAL, MP4_CAN, MP4_REN, MP4_MPMAR_ID, MP4_MP3_ID, MP4_MIN, MP4_MED, MP4_MAX, MP4_FOT, MP4_USU_FM, MP4_FM, MP4_COM_ID, MP4_MUL, MP4_UNI_ID, MP4_PROD) 
                                          VALUES(@MP4_ID,@MP4_A,@MP4_MOD,@MP4_NOM,@MP4_TIE_EST,@MP4_SEC,@MP4_MES,@MP4_DIA,@MP4_TCA,@MP4_TCB,@MP4_TCC,@MP4_VAL,@MP4_CAN,@MP4_REN,@MP4_MPMAR_ID,@MP4_MP3_ID,@MP4_MIN,@MP4_MED,@MP4_MAX,@MP4_FOT,@MP4_USU_FM,@MP4_FM,@MP4_COM_ID,@MP4_MUL,@MP4_UNI_ID,@MP4_PROD);"
            };
            item.IdModelo = this.Max("MP4_ID");
            sqlCommand.Parameters.AddWithValue("@mp4_id", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@MP4_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@MP4_MOD", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@MP4_NOM", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@MP4_TIE_EST", item.Mp4TieEst);
            sqlCommand.Parameters.AddWithValue("@MP4_SEC", item.Mp4Sec);
            sqlCommand.Parameters.AddWithValue("@MP4_MES", item.Mp4Mes);
            sqlCommand.Parameters.AddWithValue("@MP4_DIA", item.Mp4Dia);
            sqlCommand.Parameters.AddWithValue("@MP4_TCA", item.Largo);
            sqlCommand.Parameters.AddWithValue("@MP4_TCB", item.Ancho);
            sqlCommand.Parameters.AddWithValue("@MP4_TCC", item.Calibre);
            sqlCommand.Parameters.AddWithValue("@MP4_VAL", item.Valor);
            sqlCommand.Parameters.AddWithValue("@MP4_CAN", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@MP4_REN", item.Rendimiento);
            sqlCommand.Parameters.AddWithValue("@MP4_MPMAR_ID", item.IdMarca);
            sqlCommand.Parameters.AddWithValue("@MP4_MP3_ID", item.Mp4Mp3Id);
            sqlCommand.Parameters.AddWithValue("@MP4_MIN", item.Minimo);
            sqlCommand.Parameters.AddWithValue("@MP4_MED", item.Reorden);
            sqlCommand.Parameters.AddWithValue("@MP4_MAX", item.Maximo);
            sqlCommand.Parameters.AddWithValue("@MP4_FOT", item.Foto);
            sqlCommand.Parameters.AddWithValue("@MP4_USU_FM", item.Mp4UsuFm);
            sqlCommand.Parameters.AddWithValue("@MP4_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@MP4_COM_ID", item.Mp4ComId);
            sqlCommand.Parameters.AddWithValue("@MP4_MUL", item.Mul);
            sqlCommand.Parameters.AddWithValue("@MP4_UNI_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@MP4_PROD", item.Mp4Prod);
            this.ExecuteTransaction(sqlCommand);
            return item.IdModelo;
        }

        public int Update(MateriaPrima4Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @" UPDATE MP4 SET MP4_A=@MP4_A,MP4_MOD=@MP4_MOD,MP4_NOM=@MP4_NOM,MP4_TIE_EST=@MP4_TIE_EST,MP4_SEC=@MP4_SEC,MP4_MES=@MP4_MES,MP4_DIA=@MP4_DIA,MP4_TCA=@MP4_TCA,MP4_TCB=@MP4_TCB,MP4_TCC=@MP4_TCC,MP4_VAL=@MP4_VAL,MP4_CAN=@MP4_CAN,MP4_REN=@MP4_REN,MP4_MPMAR_ID=@MP4_MPMAR_ID,MP4_MP3_ID=@MP4_MP3_ID,MP4_MIN=@MP4_MIN,MP4_MED=@MP4_MED,MP4_MAX=@MP4_MAX,MP4_FOT=@MP4_FOT,MP4_USU_FM=@MP4_USU_FM,MP4_FM=@MP4_FM,MP4_COM_ID=@MP4_COM_ID,MP4_MUL=@MP4_MUL,MP4_UNI_ID=@MP4_UNI_ID,MP4_PROD=@MP4_PROD where mp4_id = @index;"
            };

            sqlCommand.Parameters.AddWithValue("@index", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@MP4_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@MP4_MOD", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@MP4_NOM", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@MP4_TIE_EST", item.Mp4TieEst);
            sqlCommand.Parameters.AddWithValue("@MP4_SEC", item.Mp4Sec);
            sqlCommand.Parameters.AddWithValue("@MP4_MES", item.Mp4Mes);
            sqlCommand.Parameters.AddWithValue("@MP4_DIA", item.Mp4Dia);
            sqlCommand.Parameters.AddWithValue("@MP4_TCA", item.Largo);
            sqlCommand.Parameters.AddWithValue("@MP4_TCB", item.Ancho);
            sqlCommand.Parameters.AddWithValue("@MP4_TCC", item.Calibre);
            sqlCommand.Parameters.AddWithValue("@MP4_VAL", item.Valor);
            sqlCommand.Parameters.AddWithValue("@MP4_CAN", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@MP4_REN", item.Rendimiento);
            sqlCommand.Parameters.AddWithValue("@MP4_MPMAR_ID", item.IdMarca);
            sqlCommand.Parameters.AddWithValue("@MP4_MP3_ID", item.Mp4Mp3Id);
            sqlCommand.Parameters.AddWithValue("@MP4_MIN", item.Minimo);
            sqlCommand.Parameters.AddWithValue("@MP4_MED", item.Reorden);
            sqlCommand.Parameters.AddWithValue("@MP4_MAX", item.Maximo);
            sqlCommand.Parameters.AddWithValue("@MP4_FOT", item.Foto);
            sqlCommand.Parameters.AddWithValue("@MP4_USU_FM", item.Mp4UsuFm);
            sqlCommand.Parameters.AddWithValue("@MP4_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@MP4_COM_ID", item.Mp4ComId);
            sqlCommand.Parameters.AddWithValue("@MP4_MUL", item.Mul);
            sqlCommand.Parameters.AddWithValue("@MP4_UNI_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@MP4_PROD", item.Mp4Prod);

            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"update mp4 set mp4_a = 0 where mp4_id = @index;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);

            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public MateriaPrima4Model GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from mp4 where mp4_id = @index;"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<MateriaPrima4Model>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<MateriaPrima4Model> GetList() {
            var sqlCommand = new FbCommand() {
                CommandText = @"select * from MP4 where MP4_a > 0 order by MP4_NOM"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<MateriaPrima4Model>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de modelos de materia prima por indices de relacion a productos (MP4_COM_ID)
        /// </summary>
        /// <param name="indexs">MP4_COM_ID</param>
        public IEnumerable<MateriaPrima4DetailModel> GetList(int[] indexs) {
            var sqlComman1 = new FbCommand {
                CommandText = @"select * from mp4 where mp4_a > 0 and MP4_COM_ID in (@indices)"
            };

            sqlComman1.CommandText = sqlComman1.CommandText.Replace("@indices", string.Join(",", indexs));
            var tablaModelos = this.ExecuteReader(sqlComman1);
            var mapperModelos = new DataNamesMapper<MateriaPrima4DetailModel>();
            return mapperModelos.Map(tablaModelos).ToList();
        }

        /// <summary>
        /// obtener listado de clase para vista de comerio + marca + unidad + modelo (COM3, MP4, MPMAR, MPPUNI)
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<MarcaModeloModelView> GetList(bool onlyActive) {
            var sqlComman1 = new FbCommand {
                CommandText = @"select mp4.* ,
                                MPMAR_nom, 
                                MPUNI_nom,
                                Com3_id, com3_n1, com3_n2, com3_n3
                                from mp4, MpMar, MpUni, Com3
                                where mp4_a > 0 
                                and MP4_MPMAR_ID = MPMAR_ID 
                                and MP4_Uni_ID = MPUni_ID and MP4_Com_ID = Com3_Id
                                order by MP4_NOM"
            };

            if (onlyActive) {

            }

            var tablaModelos = this.ExecuteReader(sqlComman1);
            var mapperModelos = new DataNamesMapper<MarcaModeloModelView>();
            return mapperModelos.Map(tablaModelos).ToList();
        }
    }
}
