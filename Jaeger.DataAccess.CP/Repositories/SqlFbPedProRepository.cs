﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// Línea de Producción (PedPro)
    /// </summary>
    public class SqlFbPedProRepository : RepositoryMaster<LineaProduccionModel>, ISqlPedProRepository {
        public SqlFbPedProRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) { this.User = user; }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedpro SET pedpro_a = 0 WHERE pedpro_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public LineaProduccionModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM pedpro WHERE pedpro_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public int Insert(LineaProduccionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO pedpro (pedpro_id, pedpro_a, pedpro_pedcom_id, pedpro_cotprd_id, pedpro_cotpro_id, pedpro_cen_id, pedpro_pro_id, pedpro_sec, pedpro_can_prot, pedpro_t_max, pedpro_minr, pedpro_nt, pedpro_fec_min, pedpro_fec_max, pedpro_fec_ini, pedpro_fec_fin, pedpro_fpe, pedpro_fpp, pedpro_fps, pedpro_fre, pedpro_frs, pedpro_usu_fm, pedpro_usu_m, pedpro_fm, pedpro_cen_ord, pedpro_can_pror, pedpro_ope, pedpro_obs, pedpro_ntr) 
                            VALUES (@pedpro_id, @pedpro_a, @pedpro_pedcom_id, @pedpro_cotprd_id, @pedpro_cotpro_id, @pedpro_cen_id, @pedpro_pro_id, @pedpro_sec, @pedpro_can_prot, @pedpro_t_max, @pedpro_minr, @pedpro_nt, @pedpro_fec_min, @pedpro_fec_max, @pedpro_fec_ini, @pedpro_fec_fin, @pedpro_fpe, @pedpro_fpp, @pedpro_fps, @pedpro_fre, @pedpro_frs, @pedpro_usu_fm, @pedpro_usu_m, @pedpro_fm, @pedpro_cen_ord, @pedpro_can_pror, @pedpro_ope, @pedpro_obs, @pedpro_ntr)"
            };
            item.IdPedPro = this.Max("PEDPRO_ID");
            sqlCommand.Parameters.AddWithValue("@pedpro_id", item.IdPedPro);
            sqlCommand.Parameters.AddWithValue("@pedpro_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedpro_pedcom_id", item.IdComponente);
            sqlCommand.Parameters.AddWithValue("@pedpro_cotprd_id", item.PEDPRO_COTPRD_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_cotpro_id", item.PEDPRO_COTPRO_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_cen_id", item.PEDPRO_CEN_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_pro_id", item.PEDPRO_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@pedpro_can_prot", item.CantidadEstimada);
            sqlCommand.Parameters.AddWithValue("@pedpro_t_max", item.TiempoEstimado);
            sqlCommand.Parameters.AddWithValue("@pedpro_minr", item.TiempoReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_nt", item.NumeroTrabajadoresEstimado);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_min", item.FechaMinima);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_max", item.FechaMaxima);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_ini", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_fin", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@pedpro_fpe", item.PEDPRO_FPE);
            sqlCommand.Parameters.AddWithValue("@pedpro_fpp", item.PEDPRO_FPP);
            sqlCommand.Parameters.AddWithValue("@pedpro_fps", item.PEDPRO_FPS);
            sqlCommand.Parameters.AddWithValue("@pedpro_fre", item.PEDPRO_FRE);
            sqlCommand.Parameters.AddWithValue("@pedpro_frs", item.PEDPRO_FRS);
            sqlCommand.Parameters.AddWithValue("@pedpro_usu_fm", item.PEDPRO_USU_FM);
            sqlCommand.Parameters.AddWithValue("@pedpro_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@pedpro_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@pedpro_cen_ord", item.PEDPRO_CEN_ORD);
            sqlCommand.Parameters.AddWithValue("@pedpro_can_pror", item.CantidadReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_ope", item.Oficial);
            sqlCommand.Parameters.AddWithValue("@pedpro_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedpro_ntr", item.NumeroTrabajdoresReal);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdPedPro;
            return 0;
        }

        public int Update(LineaProduccionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedpro 
                            SET pedpro_a = @pedpro_a, pedpro_pedcom_id = @pedpro_pedcom_id, pedpro_cotprd_id = @pedpro_cotprd_id, pedpro_cotpro_id = @pedpro_cotpro_id, pedpro_cen_id = @pedpro_cen_id, pedpro_pro_id = @pedpro_pro_id, pedpro_sec = @pedpro_sec, pedpro_can_prot = @pedpro_can_prot, pedpro_t_max = @pedpro_t_max, pedpro_minr = @pedpro_minr, pedpro_nt = @pedpro_nt, pedpro_fec_min = @pedpro_fec_min, pedpro_fec_max = @pedpro_fec_max, pedpro_fec_ini = @pedpro_fec_ini, pedpro_fec_fin = @pedpro_fec_fin, pedpro_fpe = @pedpro_fpe, pedpro_fpp = @pedpro_fpp, pedpro_fps = @pedpro_fps, pedpro_fre = @pedpro_fre, pedpro_frs = @pedpro_frs, pedpro_usu_fm = @pedpro_usu_fm, pedpro_usu_m = @pedpro_usu_m, pedpro_fm = @pedpro_fm, pedpro_cen_ord = @pedpro_cen_ord, pedpro_can_pror = @pedpro_can_pror, pedpro_ope = @pedpro_ope, pedpro_obs = @pedpro_obs, pedpro_ntr = @pedpro_ntr 
                            WHERE pedpro_id = @pedpro_id;"
            };
            sqlCommand.Parameters.AddWithValue("@pedpro_id", item.IdPedPro);
            sqlCommand.Parameters.AddWithValue("@pedpro_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedpro_pedcom_id", item.IdComponente);
            sqlCommand.Parameters.AddWithValue("@pedpro_cotprd_id", item.PEDPRO_COTPRD_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_cotpro_id", item.PEDPRO_COTPRO_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_cen_id", item.PEDPRO_CEN_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_pro_id", item.PEDPRO_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@pedpro_can_prot", item.CantidadEstimada);
            sqlCommand.Parameters.AddWithValue("@pedpro_t_max", item.TiempoEstimado);
            sqlCommand.Parameters.AddWithValue("@pedpro_minr", item.TiempoReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_nt", item.NumeroTrabajadoresEstimado);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_min", item.FechaMinima);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_max", item.FechaMaxima);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_ini", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_fin", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@pedpro_fpe", item.PEDPRO_FPE);
            sqlCommand.Parameters.AddWithValue("@pedpro_fpp", item.PEDPRO_FPP);
            sqlCommand.Parameters.AddWithValue("@pedpro_fps", item.PEDPRO_FPS);
            sqlCommand.Parameters.AddWithValue("@pedpro_fre", item.PEDPRO_FRE);
            sqlCommand.Parameters.AddWithValue("@pedpro_frs", item.PEDPRO_FRS);
            sqlCommand.Parameters.AddWithValue("@pedpro_usu_fm", item.PEDPRO_USU_FM);
            sqlCommand.Parameters.AddWithValue("@pedpro_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@pedpro_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@pedpro_cen_ord", item.PEDPRO_CEN_ORD);
            sqlCommand.Parameters.AddWithValue("@pedpro_can_pror", item.CantidadReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_ope", item.Oficial);
            sqlCommand.Parameters.AddWithValue("@pedpro_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedpro_ntr", item.NumeroTrabajdoresReal);
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<LineaProduccionModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM pedpro WHERE pedpro_A > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionModel>();
            return mapper.Map(tabla).ToList();
        }
        #endregion

        /// <summary>
        /// obtener lista PedPro_PedCom_ID
        /// </summary>
        public IEnumerable<LineaProduccionModel> GetList(int[] ProdID) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM PEDPRO WHERE PEDPRO_A > 0 AND PEDPRO_PEDCOM_ID IN (@ProID) ORDER BY PEDPRO_SEC;"
            };
            //sqlCommand.Parameters.AddWithValue("@ProID", ProdID);
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@ProID", string.Join(",", ProdID));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener lista PedPro_PedCom_ID (solo vista)
        /// </summary>
        public IEnumerable<LineaProduccionDeptoCentroView> GetListView(int[] ProdID) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM PEDPRO, EDP3, EDP2, EDP1 WHERE PEDPRO_A > 0 AND PEDPRO_PEDCOM_ID IN (@ProID) AND EDP3_ID = PEDPRO_PRO_ID AND EDP3_EDP2_ID = EDP2_ID AND EDP2_EDP1_ID = EDP1_ID ORDER BY PEDPRO_SEC;"
            };
            //sqlCommand.Parameters.AddWithValue("@ProID", ProdID);
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@ProID", string.Join(",", ProdID));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionDeptoCentroView>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener vista de componente y linea de produccion
        /// </summary>
        /// <param name="startDateTime">fecha de inicio</param>
        /// <param name="endDateTime">fecha final del rango, null si debe obtener todo el mes</param>
        /// <param name="startStatus">inicio de status, puede ser null si debe ser ignorado</param>
        /// <param name="endStatus">fin de status, puede ser null si debe ser ignorado</param>
        /// <param name="idDepartamento">indice del departamento</param>
        /// <param name="fieldDate">0 = fecha de pedido, 1 = fecha final de la captura de produccion</param>
        /// <returns>lista de componentes y linea de produccion</returns>
        public IEnumerable<ComponenteLineaProduccionView> GetList(DateTime startDateTime, DateTime? endDateTime, int? startStatus, int? endStatus, int idDepartamento, int fieldDate = 0, bool onlyYear = false, bool onlyActive = false) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PEDPRD_ID, PEDPRD_ST_ID, DIR_NOM, PEDPRD_NOM, PEDPRD_CAN, PEDPRD_FEC_PED, PEDPRD_FEC_OP,PEDPRD_FEC_ALM2, PEDPRD_FEC_REQ, PEDPRD_FEC_ACO, PEDPRD_FEC_VOBOC, PEDPRD_JER, PEDPRD_OBS, PEDPRD_USU_N,PEDPRD_USU_M, PEDPRD_FM,
                                PEDST_STA, PEDST_SEC, 
                                PEDCOM_ID, PEDCOM_SEC, PEDCOM_CLA, PEDCOM_NOM, PEDCOM_CAN_COM, PEDCOM_T_MAX, PEDCOM_MINR, PEDCOM_FEC_MIN, PEDCOM_FEC_MAX, PEDCOM_FEC_INI, PEDCOM_FEC_FIN,
                                PEDPRO.*,
                                EDP3_ID, EDP1_ARE, EDP1_ID, EDP1_NOM, EDP2_SEC, EDP2_NOM, EDP3_NOM, EDP2_VAL_MIN
                                FROM PEDPRD, PEDST, DIR, PEDCOM, PEDPRO, EDP3, EDP2, EDP1
                                WHERE PEDPRD_ST_ID = PEDST_ID
                                AND DIR_ID = PEDPRD_DIR_ID
                                AND PEDCOM_PEDPRD_ID = PEDPRD_ID
                                AND PEDPRO_PEDCOM_ID = PEDCOM_ID
                                AND EDP3_ID = PEDPRO_PRO_ID
                                AND EDP3_EDP2_ID = EDP2_ID
                                AND EDP2_EDP1_ID = EDP1_ID
                                @dateTime @status
                                AND EDP1_ID = @idDepartamento
                                ORDER BY PEDPRO_SEC, PEDCOM_SEC, PEDPRD_ID, EDP1_NOM, EDP2_SEC,EDP2_NOM,EDP3_NOM"
            };
            // por mes y año
            if (endDateTime == null) {
                if (onlyYear) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@dateTime", "and extract(year from @fieldDate) = @year");
                    sqlCommand.Parameters.AddWithValue("@year", startDateTime.Year);
                } else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@dateTime", "and extract(year from @fieldDate) = @year and extract(month from @fieldDate) = @month");
                    sqlCommand.Parameters.AddWithValue("@year", startDateTime.Year);
                    sqlCommand.Parameters.AddWithValue("@month", startDateTime.Month);
                }
            } else { // por rango de fechas
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@dateTime", "and @fieldDate >= @startDateTime and @fieldDate <= @endDateTime");
                sqlCommand.Parameters.AddWithValue("@startDateTime", startDateTime);
                sqlCommand.Parameters.AddWithValue("@endDateTime", endDateTime);
            }

            if (startStatus == null | endStatus == null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "");
            } else { // por rango de status
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and PedST_Sec >= @startStatus and PedST_Sec<=@endStatus");
                sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                sqlCommand.Parameters.AddWithValue("@endStatus", endStatus);
            }

            // fecha final de produccion
            if (fieldDate == 1) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@fieldDate", "pedpro_fec_fin");
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@fieldDate", "pedprd_fec_ped");
            }

            if (onlyActive) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@idDepartamento", "@idDepartamento and pedpro_fec_fin is null");
            }

            sqlCommand.Parameters.AddWithValue("@idDepartamento", idDepartamento);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComponenteLineaProduccionView>();
            return mapper.Map(tabla).ToList();
        }

        public bool Update(IRegistroProduccion item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE PEDPRO 
                            SET PEDPRO_MINR = @PEDPRO_MINR, PEDPRO_USU_M = @PEDPRO_USU_M, PEDPRO_FM = @PEDPRO_FM, PEDPRO_CAN_PROR = @PEDPRO_CAN_PROR, PEDPRO_OPE=@PEDPRO_OPE, PEDPRO_OBS = @PEDPRO_OBS, PEDPRO_NTR = @PEDPRO_NTR, PEDPRO_FEC_FIN = @PEDPRO_FEC_FIN, PEDPRO_CCAL_ID = @PEDPRO_CCAL_ID
                            WHERE PEDPRO_ID = @PEDPRO_ID;"
            };
            item.Modifica = this.User;
            item.FechaModifica = DateTime.Now;
            sqlCommand.Parameters.AddWithValue("@PEDPRO_ID", item.IdPedPro);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_MINR", item.TiempoReal);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_USU_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_FEC_FIN", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_CAN_PROR", item.CantidadReal);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_OPE", item.Oficial);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_OBS", item.Nota);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_NTR", item.NumeroTrabajdoresReal);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_CCAL_ID", item.Id5Clasificacion);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        /// <summary>
        /// actualizar la fecha planeada del proceso
        /// </summary>
        /// <param name="idPedPro">PedPro_ID</param>
        /// <param name="startDate">fecha de inicio</param>
        /// <param name="endDate">fecha final</param>
        /// <param name="user">clave del usuario</param>
        /// <returns>verdadero si actualiza la fecha</returns>
        public bool Update(int idPedPro, DateTime startDate, DateTime endDate, string user) {
            var sqlCommand = new FbCommand {
                CommandText = "update PEDPRO set PedPro_Fec_Min=@PedPro_Fec_Min, PedPro_Fec_Max=@PedPro_Fec_Max, PEDPRO_USU_M=@PEDPRO_USU_M where PedPro_ID=@index"
            };
            sqlCommand.Parameters.AddWithValue("@PedPro_Fec_Min", startDate);
            sqlCommand.Parameters.AddWithValue("@PedPro_Fec_Max", endDate);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_USU_M", user);
            sqlCommand.Parameters.AddWithValue("@index", idPedPro);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        #region
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(ComponenteLineaProduccion)) {
                return this.GetList1<T1>(conditionals);
            } else if (typeof(T1) == typeof(ComponenteLineaProduccionView)) {
                return this.GetList1<T1>(conditionals);
            }

            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PEDPRO.*, EDP3.*, EDP1.*, EDP2.* FROM PEDPRO, EDP3, EDP2, EDP1 WHERE EDP3_ID = PEDPRO_PRO_ID AND EDP3_EDP2_ID = EDP2_ID AND EDP2_EDP1_ID = EDP1_ID @condiciones ORDER BY EDP1_NOM, EDP2_SEC,EDP2_NOM,EDP3_NOM"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        //public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
        //    if (typeof(T1) == typeof(ComponenteLineaProduccion)) {
        //        return this.GetList1<T1>(conditionals); 
        //    } else if (typeof(T1) == typeof(ComponenteLineaProduccionView)) {
        //        return this.GetList1<T1>(conditionals);
        //    }

        //    var sqlCommand = new FbCommand {
        //        CommandText = @"SELECT PEDPRO.*, EDP3.*, EDP1.*, EDP2.* FROM PEDPRO, EDP3, EDP2, EDP1 WHERE EDP3_ID = PEDPRO_PRO_ID AND EDP3_EDP2_ID = EDP2_ID AND EDP2_EDP1_ID = EDP1_ID @condiciones ORDER BY EDP1_NOM, EDP2_SEC,EDP2_NOM,EDP3_NOM"
        //    };
        //    return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        //}

        //private IEnumerable<T1> GetList1<T1>(List<Conditional> conditionals) where T1 : class, new() {
        //    var sqlCommand = new FbCommand {
        //        CommandText = @"SELECT PEDPRD_ID, PEDPRD_ST_ID, DIR_NOM, PEDPRD_NOM, PEDPRD_CAN, PEDPRD_FEC_PED, PEDPRD_FEC_OP,PEDPRD_FEC_ALM2, PEDPRD_FEC_REQ, PEDPRD_FEC_ACO, PEDPRD_FEC_VOBOC, PEDPRD_JER, PEDPRD_OBS, PEDPRD_USU_N,PEDPRD_USU_M, PEDPRD_FM,
        //                        PEDST_STA, PEDST_SEC, 
        //                        PEDCOM_ID, PEDCOM_SEC, PEDCOM_CLA, PEDCOM_NOM, PEDCOM_CAN_COM, PEDCOM_T_MAX, PEDCOM_MINR, PEDCOM_FEC_MIN, PEDCOM_FEC_MAX, PEDCOM_FEC_INI, PEDCOM_FEC_FIN,
        //                        PEDPRO.*,
        //                        EDP3_ID, EDP1_ARE, EDP1_ID, EDP1_NOM, EDP2_SEC, EDP2_NOM, EDP3_NOM, EDP2_VAL_MIN
        //                        FROM PEDPRD, PEDST, DIR, PEDCOM, PEDPRO, EDP3, EDP2, EDP1
        //                        WHERE PEDPRD_ST_ID = PEDST_ID
        //                        AND DIR_ID = PEDPRD_DIR_ID
        //                        AND PEDCOM_PEDPRD_ID = PEDPRD_ID
        //                        AND PEDPRO_PEDCOM_ID = PEDCOM_ID
        //                        AND EDP3_ID = PEDPRO_PRO_ID
        //                        AND EDP3_EDP2_ID = EDP2_ID
        //                        AND EDP2_EDP1_ID = EDP1_ID
        //                        @condiciones
        //                        ORDER BY PEDPRO_SEC, PEDCOM_SEC, PEDPRD_ID, EDP1_NOM, EDP2_SEC,EDP2_NOM,EDP3_NOM"
        //    };
        //    return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        //}

        private IEnumerable<T1> GetList1<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PEDPRD_ID, PEDPRD_ST_ID, DIR_NOM, PEDPRD_NOM, PEDPRD_CAN, PEDPRD_FEC_PED, PEDPRD_FEC_OP,PEDPRD_FEC_ALM2, PEDPRD_FEC_REQ, PEDPRD_FEC_ACO, PEDPRD_FEC_VOBOC, PEDPRD_JER, PEDPRD_OBS, PEDPRD_USU_N,PEDPRD_USU_M, PEDPRD_FM,
                                PEDST_STA, PEDST_SEC, 
                                PEDCOM_ID, PEDCOM_SEC, PEDCOM_CLA, PEDCOM_NOM, PEDCOM_CAN_COM, PEDCOM_T_MAX, PEDCOM_MINR, PEDCOM_FEC_MIN, PEDCOM_FEC_MAX, PEDCOM_FEC_INI, PEDCOM_FEC_FIN,
                                PEDPRO.*,
                                EDP3_ID, EDP1_ARE, EDP1_ID, EDP1_NOM, EDP2_SEC, EDP2_NOM, EDP3_NOM, EDP2_VAL_MIN
                                FROM PEDPRD, PEDST, DIR, PEDCOM, PEDPRO, EDP3, EDP2, EDP1
                                WHERE PEDPRD_ST_ID = PEDST_ID
                                AND DIR_ID = PEDPRD_DIR_ID
                                AND PEDCOM_PEDPRD_ID = PEDPRD_ID
                                AND PEDPRO_PEDCOM_ID = PEDCOM_ID
                                AND EDP3_ID = PEDPRO_PRO_ID
                                AND EDP3_EDP2_ID = EDP2_ID
                                AND EDP2_EDP1_ID = EDP1_ID
                                @condiciones
                                ORDER BY PEDPRO_SEC, PEDCOM_SEC, PEDPRD_ID, EDP1_NOM, EDP2_SEC,EDP2_NOM,EDP3_NOM"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }
        #endregion
    }
}
