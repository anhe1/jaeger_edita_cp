﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de puestos (tabla PUE)
    /// </summary>
    public class SqlFbPueRepository : RepositoryMaster<PuestoModel>, ISqlPueRepository {

        public SqlFbPueRepository(DataBaseConfiguracion configuracion) : base(configuracion) { }

        public int Insert(PuestoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO pue (pue_id, pue_a, pue_edp1_id, pue_pue, pue_sal, pue_prepro_fij, pue_prepro_fac, pue_pue_id, pue_fm) 
                    			VALUES (@pue_id, @pue_a, @pue_edp1_id, @pue_pue, @pue_sal, @pue_prepro_fij, @pue_prepro_fac, @pue_pue_id, @pue_fm)"
            };

            item.IdPuesto = this.Max("PUE_ID");
            sqlCommand.Parameters.AddWithValue("@pue_id", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@pue_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pue_edp1_id", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@pue_pue", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@pue_sal", item.Salario);
            sqlCommand.Parameters.AddWithValue("@pue_prepro_fij", item.PremioProduccionFijo);
            sqlCommand.Parameters.AddWithValue("@pue_prepro_fac", item.PremioProduccionFactor);
            sqlCommand.Parameters.AddWithValue("@pue_pue_id", item.PUE_PUE_ID);
            //sqlCommand.Parameters.AddWithValue("@pue_usu_fm", item);
            sqlCommand.Parameters.AddWithValue("@pue_fm", item.FechaModifica);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdPuesto;
            return 0;
        }

        public int Update(PuestoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pue 
                    			SET pue_a = @pue_a, pue_edp1_id = @pue_edp1_id, pue_pue = @pue_pue, pue_sal = @pue_sal, pue_prepro_fij = @pue_prepro_fij, pue_prepro_fac = @pue_prepro_fac, pue_pue_id = @pue_pue_id, pue_fm  = @pue_fm 
                    			WHERE pue_id = @pue_id;"
            };

            sqlCommand.Parameters.AddWithValue("@pue_id", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@pue_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pue_edp1_id", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@pue_pue", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@pue_sal", item.Salario);
            sqlCommand.Parameters.AddWithValue("@pue_prepro_fij", item.PremioProduccionFijo);
            sqlCommand.Parameters.AddWithValue("@pue_prepro_fac", item.PremioProduccionFactor);
            sqlCommand.Parameters.AddWithValue("@pue_pue_id", item.PUE_PUE_ID);
            //sqlCommand.Parameters.AddWithValue("@pue_usu_fm", item);
            sqlCommand.Parameters.AddWithValue("@pue_fm", item.FechaModifica);

            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pue SET pue_a = 0 WHERE pue_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PuestoModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "pue", "pue")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PuestoModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<PuestoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "pue", "pue")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PuestoModel>();
            return mapper.Map(tabla).ToList();
        }
    }
}
