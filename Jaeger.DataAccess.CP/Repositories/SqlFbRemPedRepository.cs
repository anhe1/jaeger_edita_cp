﻿/// purpose: repositorio de partidas o conceptos de remision (REMPED)
/// develop: ANHE1 270920200122
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Remisionado.Contracts;
using Jaeger.Domain.CP.Remisionado.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de partidas o conceptos de remision (REMPED)
    /// </summary>
    public class SqlFbRemPedRepository : RepositoryMaster<RemisionPedModel>, ISqlRemisionPedRepository {

        public SqlFbRemPedRepository(DataBaseConfiguracion configuracion) : base(configuracion) { }

        public int Insert(RemisionPedModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO remped ( remped_id, remped_a, remped_rem_id, remped_pedprd_id, remped_can, remped_uni, remped_$_uni, remped_obs, remped_fm, remped_usu_n, remped_usu_fm, remped_almx_id, remped_prd_cuenta) 
                    			            VALUES (@remped_id,@remped_a,@remped_rem_id,@remped_pedprd_id,@remped_can,@remped_uni,@remped_$_uni,@remped_obs,@remped_fm,@remped_usu_n,@remped_usu_fm, @remped_almx_id, @remped_prd_cuenta)"
            };
            item.IdRemPed = this.Max("REMPED_ID");
            sqlCommand.Parameters.AddWithValue("@remped_id", item.IdRemPed);
            sqlCommand.Parameters.AddWithValue("@remped_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@remped_rem_id", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@remped_pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@remped_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@remped_uni", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@remped_$_uni", item.Unitario);
            
            sqlCommand.Parameters.AddWithValue("@remped_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@remped_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@remped_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@remped_usu_fm", item.REMPED_USU_FM);
            sqlCommand.Parameters.AddWithValue("@remped_almx_id", item.REMPED_ALMX_ID);
            sqlCommand.Parameters.AddWithValue("@remped_prd_cuenta", item.REMPED_PRD_CUENTA);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdRemPed;
            return 0;
        }

        public int Update(RemisionPedModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE remped 
                    			SET remped_a = @remped_a, remped_rem_id = @remped_rem_id, remped_pedprd_id = @remped_pedprd_id, remped_can = @remped_can, remped_uni = @remped_uni, remped_$_uni = @remped_$_uni, remped_$ = @remped_$, remped_$PAG = @remped_$PAG, remped_obs = @remped_obs, remped_fm = @remped_fm, remped_usu_n = @remped_usu_n, remped_usu_m = @remped_usu_m, remped_usu_fm = @remped_usu_fm, remped_almx_id = @remped_almx_id, remped_prd_cuenta = @remped_prd_cuenta 
                    			WHERE remped_id = @remped_id;"
            };
            sqlCommand.Parameters.AddWithValue("@remped_id", item.IdRemPed);
            sqlCommand.Parameters.AddWithValue("@remped_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@remped_rem_id", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@remped_pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@remped_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@remped_uni", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@remped_$_uni", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@remped_$", item.Importe);
            sqlCommand.Parameters.AddWithValue("@remped_$PAG", item.Acumulado);
            sqlCommand.Parameters.AddWithValue("@remped_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@remped_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@remped_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@remped_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@remped_usu_fm", item.REMPED_USU_FM);
            sqlCommand.Parameters.AddWithValue("@remped_almx_id", item.REMPED_ALMX_ID);
            sqlCommand.Parameters.AddWithValue("@remped_prd_cuenta", item.REMPED_PRD_CUENTA);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE remped SET remped_a = 0 WHERE remped_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public RemisionPedModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "remped", "remped")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionPedModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<RemisionPedModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "remped", "remped")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionPedModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de partidas por el indice de la remision
        /// </summary>
        /// <param name="idRemision">indice de la remision REM_ID</param>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<RemisionPedModel> GetList(int idRemision, bool onlyActive) {
            return this.GetList(new int[] { idRemision }, onlyActive);
        }

        /// <summary>
        /// obtener listado de partidas por un array de indices de remisiones
        /// </summary>
        /// <param name="idRemision">array de indices REM_ID</param>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<RemisionPedModelView> GetList(int[] idRemision, bool onlyActive) {
            // en caso que el arreglo este vacio
            if (idRemision.Length <= 0)
                return new List<RemisionPedModelView>();

            var sqlCommand = new FbCommand {
                CommandText = @"select RemPed.*,
                                PEDPRD_ID, PEDPRD_A, PedST_ID, Dir_Nom, PEDPRD_Nom, PedST_Sta, PedST_Sec, PedPrd_Uni, PEDPRD_$Uni
                                from RemPed, PedPrd, PedST, Dir
                                where RemPed_PEDPRD_ID = PEDPRD_ID
                                and PedPrd_St_ID = PedST_ID
                                and DIR_ID = PedPrd_Dir_ID
                                and RemPed_Rem_ID in (@indexs)
                                @active"
            };
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", string.Join(",", idRemision));

            if (onlyActive) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@active", "and RemPed_A > 0");
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@active", "");
            }

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionPedModelView>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de las partidas de remisiones por la descripcion de los conceptos
        /// </summary>
        /// <param name="descripcion">descipcion del concepto</param>
        /// <param name="Orden">a partir del numero de orden especifico</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <returns></returns>
        public IEnumerable<RemisionPedModelView> GetList(string descripcion, int Orden, bool onlyActive = true) {
            var sqlCommand = new FbCommand {
                CommandText = @"select RemPed.*,
                                PEDPRD_ID, PEDPRD_A, PedST_ID, Dir_Nom, PEDPRD_Nom, PedST_Sta, PedST_Sec, PedPrd_Uni, PEDPRD_$Uni
                                from RemPed, PedPrd, PedST, Dir
                                where RemPed_PEDPRD_ID = PEDPRD_ID
                                and PedPrd_St_ID = PedST_ID
                                and DIR_ID = PedPrd_Dir_ID
                                and PEDPRD_Nom like '%@indexs%' and PEDPRD_ID >= @orden
                                @active"
            };
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@indexs", descripcion);
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@orden", Orden.ToString());

            if (onlyActive) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@active", "and RemPed_A > 0");
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@active", "");
            }

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionPedModelView>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<RemisionPedModelView> GetList(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"select RemPed.*,
                                PEDPRD_ID, PEDPRD_A, PedST_ID, Dir_Nom, PEDPRD_Nom, PedST_Sta, PedST_Sec, PedPrd_Uni, PEDPRD_$Uni
                                from RemPed, PedPrd, PedST, Dir
                                where RemPed_PEDPRD_ID = PEDPRD_ID
                                and PedPrd_St_ID = PedST_ID
                                and DIR_ID = PedPrd_Dir_ID
                                and RemPed_Rem_ID in (@indexs)
                                @condiciones"
            };
            

            

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RemisionPedModelView>();
            return mapper.Map(tabla).ToList();
        }
    }
}
