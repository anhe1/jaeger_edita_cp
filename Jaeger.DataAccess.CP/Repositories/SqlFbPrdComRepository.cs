﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de componentes y ensambles (estructura en cotizaciones) (PedCom)
    /// </summary>
    public class SqlFbPrdComRepository : RepositoryMaster<PrdComModel>, ISqlPrdComRepository {
        protected ISqlCotizacionEspecificacionRepository especificacionRepository;
        protected ISqlPrdProRepository prdProRepository;
        public SqlFbPrdComRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.especificacionRepository = new SqlFbCotizacionEspecificacionRepository(configuracion);
            this.prdProRepository = new SqlFbPrdProRepository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE prdcom SET prdcom_a = 0 WHERE prdcom_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PrdComModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from prdcom where prdcom_a > 0 and prdcom_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.GetMapper(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<PrdComModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "select * from prdcom where prdcom_a > 0"
            };
            return this.GetMapper(sqlCommand);
        }

        /// <summary>
        /// obtener listado de componentes o esambles de una cotizacion o presupuesto
        /// </summary>
        /// <param name="index">indice de relacion del presupuesto o cotizacion</param>
        /// <returns>IEnumerable<PrdComDetailModel></returns>
        public IEnumerable<PrdComDetailModel> GetList(int index) {
            var sqlPrdCom = new FbCommand {
                CommandText = @"select * from PRDCOM where PRDCOM_a > 0 and PRDCOM_Cot_ID = @index order by PrdCom_Sec"
            };

            sqlPrdCom.Parameters.AddWithValue("@index", index);
            var result = this.GetMapper<PrdComDetailModel>(sqlPrdCom).ToList();

            if (result != null) {
                if (result.Count > 0) {
                    var allids = result.Select(it => it.Id).ToList();
                    var espec = this.especificacionRepository.GetList(allids.ToArray());
                    var lineaProduccion = this.prdProRepository.GetList(allids.ToArray());

                    for (int i = 0; i < result.Count; i++) {
                        result[i].Especificaciones = new BindingList<CotLC2DetailModel>(espec.Where(it => it.COTLC2_PRDCOM_ID == result[i].Id).ToList());
                        result[i].LineaProduccion = new BindingList<PrdProDetailModel>(lineaProduccion.Where(it => it.PRDPRO_PRDCOM_ID == result[i].Id).ToList());
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// obtener listado de componentes o esambles de una cotizacion o presupuesto
        /// </summary>
        /// <param name="index">indice de relacion del presupuesto o cotizacion</param>
        /// <returns>IEnumerable<PrdComDetailModel></returns>
        public IEnumerable<PrdComDetailModelView> GetList(int index, bool active) {
            var sqlPrdCom = new FbCommand {
                CommandText = @"select * from PRDCOM where PRDCOM_a > 0 and PRDCOM_Cot_ID = @index order by PrdCom_Sec"
            };

            sqlPrdCom.Parameters.AddWithValue("@index", index);
            var result = this.GetMapper<PrdComDetailModelView>(sqlPrdCom).ToList();

            if (result != null) {
                if (result.Count > 0) {
                    var allids = result.Select(it => it.Id).ToList();
                    var espec = this.especificacionRepository.GetList(allids.ToArray());
                    var lineaProduccion = this.prdProRepository.GetList(allids.ToArray(), false);

                    for (int i = 0; i < result.Count; i++) {
                        result[i].Especificaciones = new BindingList<CotLC2DetailModel>(espec.Where(it => it.COTLC2_PRDCOM_ID == result[i].Id).ToList());
                        result[i].LineaProduccion = new BindingList<PrdProDetailModelView>(lineaProduccion.Where(it => it.PRDPRO_PRDCOM_ID == result[i].Id).ToList());
                    }
                }
            }

            return result;
        }

        public PrdComDetailModelView Save(PrdComDetailModelView item) {
            if (item.Id == 0) {
                item.Id = this.Insert(item);
            } else {
                this.Update(item);
            }

            if (item.Id > 0) {
                for (int i = 0; i < item.Especificaciones.Count; i++) {
                    item.Especificaciones[i].COTLC2_PRDCOM_ID = item.Id;
                    item.Especificaciones[i] = this.especificacionRepository.Save(item.Especificaciones[i]);
                }
                // linea de produccion
                for (int i = 0; i < item.LineaProduccion.Count; i++) {
                    item.LineaProduccion[i].PRDPRO_PRDCOM_ID = item.Id;
                    item.LineaProduccion[i] = this.prdProRepository.Save(item.LineaProduccion[i]);
                }
            }

            return item;
        }

        public int Insert(PrdComModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO prdcom (prdcom_id, prdcom_a, prdcom_cot_id, prdcom_prd_id, prdcom_lc_id, prdcom_nom, prdcom_can_ini, prdcom_can, prdcom_sec, prdcom_usu_fm, prdcom_fm, prdcom_com_id) 
                			VALUES (@prdcom_id, @prdcom_a, @prdcom_cot_id, @prdcom_prd_id, @prdcom_lc_id, @prdcom_nom, @prdcom_can_ini, @prdcom_can, @prdcom_sec, @prdcom_usu_fm, @prdcom_fm, @prdcom_com_id)"
            };
            item.Id = this.Max("PRDCOM_ID");
            sqlCommand.Parameters.AddWithValue("@prdcom_id", item.Id);
            sqlCommand.Parameters.AddWithValue("@prdcom_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@prdcom_cot_id", item.IdCotizacion);
            sqlCommand.Parameters.AddWithValue("@prdcom_prd_id", item.PRDCOM_PRD_ID);
            sqlCommand.Parameters.AddWithValue("@prdcom_lc_id", item.PRDCOM_LC_ID);
            sqlCommand.Parameters.AddWithValue("@prdcom_nom", item.Denominacion);
            sqlCommand.Parameters.AddWithValue("@prdcom_can_ini", item.CantidadInicial);
            sqlCommand.Parameters.AddWithValue("@prdcom_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@prdcom_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@prdcom_usu_fm", item.PRDCOM_USU_FM);
            sqlCommand.Parameters.AddWithValue("@prdcom_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@prdcom_com_id", item.PRDCOM_COM_ID);
            if (this.ExecuteTransaction(sqlCommand) == 0) {
                item.Id = 0;
            }
            return item.Id;
        }

        public int Update(PrdComModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE prdcom 
                			SET prdcom_a = @prdcom_a, prdcom_cot_id = @prdcom_cot_id, prdcom_prd_id = @prdcom_prd_id, prdcom_lc_id = @prdcom_lc_id, prdcom_nom = @prdcom_nom, prdcom_can_ini = @prdcom_can_ini, prdcom_can = @prdcom_can, prdcom_sec = @prdcom_sec, prdcom_usu_fm = @prdcom_usu_fm, prdcom_fm = @prdcom_fm, prdcom_com_id = @prdcom_com_id 
                			WHERE prdcom_id = @prdcom_id;"
            };
            sqlCommand.Parameters.AddWithValue("@prdcom_id", item.Id);
            sqlCommand.Parameters.AddWithValue("@prdcom_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@prdcom_cot_id", item.IdCotizacion);
            sqlCommand.Parameters.AddWithValue("@prdcom_prd_id", item.PRDCOM_PRD_ID);
            sqlCommand.Parameters.AddWithValue("@prdcom_lc_id", item.PRDCOM_LC_ID);
            sqlCommand.Parameters.AddWithValue("@prdcom_nom", item.Denominacion);
            sqlCommand.Parameters.AddWithValue("@prdcom_can_ini", item.CantidadInicial);
            sqlCommand.Parameters.AddWithValue("@prdcom_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@prdcom_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@prdcom_usu_fm", item.PRDCOM_USU_FM);
            sqlCommand.Parameters.AddWithValue("@prdcom_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@prdcom_com_id", item.PRDCOM_COM_ID);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
