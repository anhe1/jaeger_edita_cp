﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbCotizacionEspecificacionRepository : RepositoryMaster<CotLC2Model>, ISqlCotizacionEspecificacionRepository {
        public SqlFbCotizacionEspecificacionRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotlc2 SET cotlc2_a = 0 WHERE cotlc2_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CotLC2Model GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "cotlc2", "cotlc2")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotLC2Model>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<CotLC2Model> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "cotlc2", "cotlc2")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<CotLC2Model>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener lista de especificaciones 
        /// </summary>
        /// <param name="indexs">lista de indices del componente</param>
        public IEnumerable<CotLC2DetailModel> GetList(int[] indexs) {
            var sqlEspec = new FbCommand {
                CommandText = @"select * from CotLC2 where CotLC2_A > 0 and CotLC2_PrdCom_ID in (@indices) order by CotLC2_ID asc"
            };

            if (indexs.Length > 0) {
                sqlEspec.CommandText = sqlEspec.CommandText.Replace("@indices", string.Join(",", indexs));
                var tablaEspec = this.ExecuteReader(sqlEspec);
                var mapperEspec = new DataNamesMapper<CotLC2DetailModel>();
                return mapperEspec.Map(tablaEspec).ToList();
            }
            return null;
        }

        public int Insert(CotLC2Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO cotlc2 (cotlc2_id, cotlc2_a, cotlc2_lc2_id, cotlc2_prdcom_id, cotlc2_tex, cotlc2_usu_m, cotlc2_fm, cotlc2_cot_id, cotlc2_com_id) 
                			VALUES (@cotlc2_id, @cotlc2_a, @cotlc2_lc2_id, @cotlc2_prdcom_id, @cotlc2_tex, @cotlc2_usu_m, @cotlc2_fm, @cotlc2_cot_id, @cotlc2_com_id)"
            };
            item.Id = this.Max("COTLC2_ID");
            sqlCommand.Parameters.AddWithValue("@cotlc2_id", item.Id);
            sqlCommand.Parameters.AddWithValue("@cotlc2_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotlc2_lc2_id", item.COTLC2_LC2_ID);
            sqlCommand.Parameters.AddWithValue("@cotlc2_prdcom_id", item.COTLC2_PRDCOM_ID);
            sqlCommand.Parameters.AddWithValue("@cotlc2_tex", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@cotlc2_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@cotlc2_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@cotlc2_cot_id", item.COTLC2_COT_ID);
            sqlCommand.Parameters.AddWithValue("@cotlc2_com_id", item.COTLC2_COM_ID);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.Id;
            return 0;
        }

        public int Update(CotLC2Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE cotlc2 
                			SET cotlc2_a = @cotlc2_a, cotlc2_lc2_id = @cotlc2_lc2_id, cotlc2_prdcom_id = @cotlc2_prdcom_id, cotlc2_tex = @cotlc2_tex, cotlc2_usu_m = @cotlc2_usu_m, cotlc2_fm = @cotlc2_fm, cotlc2_cot_id = @cotlc2_cot_id, cotlc2_com_id = @cotlc2_com_id 
                			WHERE cotlc2_id = @cotlc2_id;"
            };
            sqlCommand.Parameters.AddWithValue("@cotlc2_id", item.Id);
            sqlCommand.Parameters.AddWithValue("@cotlc2_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@cotlc2_lc2_id", item.COTLC2_LC2_ID);
            sqlCommand.Parameters.AddWithValue("@cotlc2_prdcom_id", item.COTLC2_PRDCOM_ID);
            sqlCommand.Parameters.AddWithValue("@cotlc2_tex", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@cotlc2_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@cotlc2_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@cotlc2_cot_id", item.COTLC2_COT_ID);
            sqlCommand.Parameters.AddWithValue("@cotlc2_com_id", item.COTLC2_COM_ID);
            return this.ExecuteTransaction(sqlCommand);
        }

        public CotLC2DetailModel Save(CotLC2DetailModel item) {
            if (item.Id == 0) {
                item.Id = this.Insert(item);
            } else {
                this.Update(item);
            }
            return item;
        }
    }
}
