﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// registros de calidad (tabla PEDCAL)
    /// </summary>
    public class SqlFbOrdenProduccionCalidadRepository : RepositoryMaster<OrdenProduccionCalidadModel>, ISqlFbOrdenProduccionCalidadRepository {
        public SqlFbOrdenProduccionCalidadRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = ""
            };
            throw new System.NotImplementedException();
        }

        public OrdenProduccionCalidadModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT PEDCAL.* FROM PEDCAL WHERE PEDCAL_ID = @INDEX"
            };
            sqlCommand.Parameters.AddWithValue("@INDEX", index.ToString());
            throw new System.NotImplementedException();
        }

        public int Insert(OrdenProduccionCalidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO PEDCAL ( PEDCAL_ID, PEDCAL_A, PEDCAL_PEDPRD_ID, PEDCAL_FECL_ID, PEDCAL_CEN_ID, PEDCAL_DIR_ID, PEDCAL_FEC, PEDCAL_OBS, PEDCAL_USU_N, PEDCAL_USU_F, PEDCAL_USU_FM, PEDCAL_FM)
                                            VALUES (@PEDCAL_ID,@PEDCAL_A,@PEDCAL_PEDPRD_ID,@PEDCAL_FECL_ID,@PEDCAL_CEN_ID,@PEDCAL_DIR_ID,@PEDCAL_FEC,@PEDCAL_OBS,@PEDCAL_USU_N,@PEDCAL_USU_F,@PEDCAL_USU_FM,@PEDCAL_FM) RETURNING PEDCAL_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@PEDCAL_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_PEDPRD_ID", item.IdOrden);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_FECL_ID", item.IdMotivo);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_CEN_ID", item.IdCentro);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_DIR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_OBS", item.Nota);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_USU_F", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_USU_FM", item.algo);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_FM", item.FechaModifica);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(OrdenProduccionCalidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE PEDCAL SET PEDCAL_A = @PEDCAL_A, PEDCAL_PEDPRD_ID = @PEDCAL_PEDPRD_ID, PEDCAL_FECL_ID = @PEDCAL_FECL_ID, PEDCAL_CEN_ID = @PEDCAL_CEN_ID, PEDCAL_DIR_ID = @PEDCAL_DIR_ID, PEDCAL_FEC = @PEDCAL_FEC, 
PEDCAL_OBS = @PEDCAL_OBS, PEDCAL_USU_N = @PEDCAL_USU_N, PEDCAL_USU_F = @PEDCAL_USU_F, PEDCAL_USU_FM = @PEDCAL_USU_FM, PEDCAL_FM = @PEDCAL_FM WHERE PEDCAL_ID = @PEDCAL_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@PEDCAL_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_PEDPRD_ID", item.IdOrden);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_FECL_ID", item.IdMotivo);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_CEN_ID", item.IdCentro);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_DIR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_OBS", item.Nota);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_USU_F", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_USU_FM", item.algo);
            sqlCommand.Parameters.AddWithValue("@PEDCAL_FM", item.FechaModifica);

            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<OrdenProduccionCalidadModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = ""
            };
            throw new System.NotImplementedException();
        }
        #endregion

        public OrdenProduccionCalidadDetail Salveable(OrdenProduccionCalidadDetail model) {
            model.FechaModifica = DateTime.Now;
            if (model.Id == 0) {
                model.Creo = this.User;
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand() {
                CommandText = @"SELECT * FROM PEDCAL @condiciones"
            };

            if (typeof(T1) == typeof(OrdenProduccionCalidadModel)) {
                sqlCommand.CommandText = @"SELECT * FROM PEDCAL @wcondiciones";
            } else if (typeof(T1) == typeof(OrdenProduccionCalidadDetail)) {
                // el directorio se filtra por relacion de empleado que es 9
                sqlCommand.CommandText = @"SELECT PEDCAL.*, DIR.DIR_NOM, PEDFECL.PEDFECL_TEX, EDP2_NOM FROM PEDCAL 
                LEFT JOIN EDP2 ON PEDCAL_CEN_ID = EDP2_ID
                LEFT JOIN DIR ON PEDCAL_DIR_ID = DIR_ID AND DIR_DIRCLA_ID = 9
                LEFT JOIN PEDFECL ON PEDCAL_FECL_ID = PEDFECL_ID @wcondiciones";
            }
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}
