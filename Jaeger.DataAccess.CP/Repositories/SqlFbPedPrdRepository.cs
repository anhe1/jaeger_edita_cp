﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// Orden de Produccion (PedPrd)
    /// </summary>
    public class SqlFbPedPrdRepository : RepositoryMaster<OrdenProduccionModel>, ISqlPedPrdRepository {
        #region declaraciones
        protected ISqlPedComRepository pedComRepository;
        protected ISqlPedFechaAcuerdoRepository pedFecRepository;
        protected ISqlPedFecRRepository pedFecRRepository;
        #endregion

        public SqlFbPedPrdRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.pedComRepository = new SqlFbPedComRepository(configuracion, user);
            this.pedFecRepository = new SqlFbPedFechaAcuerdoRepository(configuracion, this.User);
            this.pedFecRRepository = new SqlFbPedFecRRepository(configuracion, this.User);
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedprd SET pedprd_a = 0 WHERE pedprd_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteScalar(sqlCommand) > 0;
        }

        public int Insert(OrdenProduccionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO pedprd (pedprd_id, pedprd_a, pedprd_cla1, pedprd_cla2, pedprd_dir_id, pedprd_dircon_id, pedprd_ped_id, pedprd_cotprd_id, pedprd_ven_id, pedprd_nom, pedprd_can, pedprd_uni, pedprd_$uni, pedprd_$ped, pedprd_$cot, pedprd_$uti, pedprd_fec_ped, pedprd_fec_req, pedprd_fec_aco, pedprd_fec_fps, pedprd_fec_rec, pedprd_fec_op, pedprd_obs, pedprd_sec, pedprd_st_id, pedprd_jer, pedprd_usu_n, pedprd_usu_m, pedprd_fm, pedprd_fac, pedprd_$fac, pedprd_$facp, pedprd_rem, pedprd_$rem, pedprd_$remp, pedprd_lp_id, pedprd_fec_vobo, pedprd_fec_rem1, pedprd_fec_rem2, pedprd_com_id, pedprd_tafrm_id, pedprd_fec_alm2, pedprd_fec_voboc) 
                             VALUES (@pedprd_id, @pedprd_a, @pedprd_cla1, @pedprd_cla2, @pedprd_dir_id, @pedprd_dircon_id, @pedprd_ped_id, @pedprd_cotprd_id, @pedprd_ven_id, @pedprd_nom, @pedprd_can, @pedprd_uni, @pedprd_$uni, @pedprd_$ped, @pedprd_$cot, @pedprd_$uti, @pedprd_fec_ped, @pedprd_fec_req, @pedprd_fec_aco, @pedprd_fec_fps, @pedprd_fec_rec, @pedprd_fec_op, @pedprd_obs, @pedprd_sec, @pedprd_st_id, @pedprd_jer, @pedprd_usu_n, @pedprd_usu_m, @pedprd_fm, @pedprd_fac, @pedprd_$fac, @pedprd_$facp, @pedprd_rem, @pedprd_$rem, @pedprd_$remp, @pedprd_lp_id, @pedprd_fec_vobo, @pedprd_fec_rem1, @pedprd_fec_rem2, @pedprd_com_id, @pedprd_tafrm_id, @pedprd_fec_alm2, @pedprd_fec_voboc)"
            };
            item.IdPedPrd = this.Max("PEDPRD_ID");
            sqlCommand.Parameters.AddWithValue("@pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@pedprd_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedprd_cla1", item.PEDPRD_CLA1);
            sqlCommand.Parameters.AddWithValue("@pedprd_cla2", item.PEDPRD_CLA2);
            sqlCommand.Parameters.AddWithValue("@pedprd_dir_id", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@pedprd_dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@pedprd_ped_id", item.PEDPRD_PED_ID);
            sqlCommand.Parameters.AddWithValue("@pedprd_cotprd_id", item.Cotizacion);
            sqlCommand.Parameters.AddWithValue("@pedprd_ven_id", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@pedprd_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@pedprd_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@pedprd_uni", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@pedprd_$uni", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@pedprd_$ped", item.TotalVenta);
            sqlCommand.Parameters.AddWithValue("@pedprd_$cot", item.TotalCosto);
            sqlCommand.Parameters.AddWithValue("@pedprd_$uti", item.Utilidad);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_ped", item.FechaPedido);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_req", item.FechaCliente);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_aco", item.FechaAcuerdo);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_fps", item.PEDPRD_FEC_FPS);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_rec", item.FechaReCompra);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_op", item.FechaOrden);
            sqlCommand.Parameters.AddWithValue("@pedprd_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedprd_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@pedprd_st_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@pedprd_jer", item.Jerarquia);
            sqlCommand.Parameters.AddWithValue("@pedprd_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@pedprd_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@pedprd_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@pedprd_fac", item.PEDPRD_FAC);
            sqlCommand.Parameters.AddWithValue("@pedprd_$fac", item.PEDPRD_FAC2);
            sqlCommand.Parameters.AddWithValue("@pedprd_$facp", item.PEDPRD_FACP);
            sqlCommand.Parameters.AddWithValue("@pedprd_rem", item.Remisionado);
            sqlCommand.Parameters.AddWithValue("@pedprd_$rem", item.TotalRemisionado);
            sqlCommand.Parameters.AddWithValue("@pedprd_$remp", item.PEDPRD_REMP);
            sqlCommand.Parameters.AddWithValue("@pedprd_lp_id", item.PEDPRD_LP_ID);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_vobo", item.PEDPRD_FEC_VOBO);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_rem1", item.FechaRemision1);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_rem2", item.FechaRemision2);
            sqlCommand.Parameters.AddWithValue("@pedprd_com_id", item.PEDPRD_COM_ID);
            sqlCommand.Parameters.AddWithValue("@pedprd_tafrm_id", item.IdVerificado);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_alm2", item.FechaLiberacion);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_voboc", item.FechaVobo);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdPedPrd;
            return 0;
        }

        public int Update(OrdenProduccionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedprd 
                            SET pedprd_a = @pedprd_a, pedprd_cla1 = @pedprd_cla1, pedprd_cla2 = @pedprd_cla2, pedprd_dir_id = @pedprd_dir_id, pedprd_dircon_id = @pedprd_dircon_id, pedprd_ped_id = @pedprd_ped_id, pedprd_cotprd_id = @pedprd_cotprd_id, pedprd_ven_id = @pedprd_ven_id, pedprd_nom = @pedprd_nom, pedprd_can = @pedprd_can, pedprd_uni = @pedprd_uni, pedprd_$uni = @pedprd_$uni, pedprd_$ped = @pedprd_$ped, pedprd_$cot = @pedprd_$cot, pedprd_$uti = @pedprd_$uti, pedprd_fec_ped = @pedprd_fec_ped, pedprd_fec_req = @pedprd_fec_req, pedprd_fec_aco = @pedprd_fec_aco, pedprd_fec_fps = @pedprd_fec_fps, pedprd_fec_rec = @pedprd_fec_rec, pedprd_fec_op = @pedprd_fec_op, pedprd_obs = @pedprd_obs, pedprd_sec = @pedprd_sec, pedprd_st_id = @pedprd_st_id, pedprd_jer = @pedprd_jer, pedprd_usu_n = @pedprd_usu_n, pedprd_usu_m = @pedprd_usu_m, pedprd_fm = @pedprd_fm, pedprd_fac = @pedprd_fac, pedprd_$fac = @pedprd_$fac, pedprd_$facp = @pedprd_$facp, pedprd_rem = @pedprd_rem, pedprd_$rem = @pedprd_$rem, pedprd_$remp = @pedprd_$remp, pedprd_lp_id = @pedprd_lp_id, pedprd_fec_vobo = @pedprd_fec_vobo, pedprd_fec_rem1 = @pedprd_fec_rem1, pedprd_fec_rem2 = @pedprd_fec_rem2, pedprd_com_id = @pedprd_com_id, pedprd_tafrm_id = @pedprd_tafrm_id, pedprd_fec_alm2 = @pedprd_fec_alm2, pedprd_fec_voboc = @pedprd_fec_voboc 
                            WHERE pedprd_id = @pedprd_id;"
            };

            sqlCommand.Parameters.AddWithValue("@pedprd_id", item.IdPedPrd);
            sqlCommand.Parameters.AddWithValue("@pedprd_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedprd_cla1", item.PEDPRD_CLA1);
            sqlCommand.Parameters.AddWithValue("@pedprd_cla2", item.PEDPRD_CLA2);
            sqlCommand.Parameters.AddWithValue("@pedprd_dir_id", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@pedprd_dircon_id", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@pedprd_ped_id", item.PEDPRD_PED_ID);
            sqlCommand.Parameters.AddWithValue("@pedprd_cotprd_id", item.Cotizacion);
            sqlCommand.Parameters.AddWithValue("@pedprd_ven_id", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@pedprd_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@pedprd_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@pedprd_uni", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@pedprd_$uni", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@pedprd_$ped", item.TotalVenta);
            sqlCommand.Parameters.AddWithValue("@pedprd_$cot", item.TotalCosto);
            sqlCommand.Parameters.AddWithValue("@pedprd_$uti", item.Utilidad);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_ped", item.FechaPedido);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_req", item.FechaCliente);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_aco", item.FechaAcuerdo);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_fps", item.PEDPRD_FEC_FPS);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_rec", item.FechaReCompra);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_op", item.FechaOrden);
            sqlCommand.Parameters.AddWithValue("@pedprd_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedprd_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@pedprd_st_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@pedprd_jer", item.Jerarquia);
            sqlCommand.Parameters.AddWithValue("@pedprd_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@pedprd_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@pedprd_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@pedprd_fac", item.PEDPRD_FAC);
            sqlCommand.Parameters.AddWithValue("@pedprd_$fac", item.PEDPRD_FAC2);
            sqlCommand.Parameters.AddWithValue("@pedprd_$facp", item.PEDPRD_FACP);
            sqlCommand.Parameters.AddWithValue("@pedprd_rem", item.Remisionado);
            sqlCommand.Parameters.AddWithValue("@pedprd_$rem", item.TotalRemisionado);
            sqlCommand.Parameters.AddWithValue("@pedprd_$remp", item.PEDPRD_REMP);
            sqlCommand.Parameters.AddWithValue("@pedprd_lp_id", item.PEDPRD_LP_ID);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_vobo", item.PEDPRD_FEC_VOBO);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_rem1", item.FechaRemision1);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_rem2", item.FechaRemision2);
            sqlCommand.Parameters.AddWithValue("@pedprd_com_id", item.PEDPRD_COM_ID);
            sqlCommand.Parameters.AddWithValue("@pedprd_tafrm_id", item.IdVerificado);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_alm2", item.FechaLiberacion);
            sqlCommand.Parameters.AddWithValue("@pedprd_fec_voboc", item.FechaVobo);
            return this.ExecuteTransaction(sqlCommand);
        }

        public OrdenProduccionModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "pedprd", "pedprd")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<OrdenProduccionModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public OrdenProduccion GetBy(int index) {
            var orden = this.GetList<OrdenProduccion>(new List<IConditional> {
                new Conditional("PEDPRD_ID", index.ToString())
            }).FirstOrDefault();

            if (orden != null) {
                var componentes = this.pedComRepository.GetList<Componente>(new List<Conditional> {
                    new Conditional("PEDCOM_PEDPRD_ID", index.ToString()),
                    new Conditional("PEDCOM_A", "0", ConditionalTypeEnum.GreaterThan),
                }).ToList();
                if (componentes != null) {
                    orden.Componentes = new System.ComponentModel.BindingList<Componente>(componentes);
                }
            }
            return orden;
        }

        /// <summary>
        /// lista simple de ordenes de produccion
        /// </summary>
        public IEnumerable<OrdenProduccionModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "select * from pedprd where pedprd_a > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<OrdenProduccionModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// actualizar el estado de la verificacion de la orden de produccion
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="user">clave del usuario que autoriza</param>
        /// <param name="value">bandera: 0 - No Verificado, 1 - Verificado</param>
        /// <returns>verdadero si se actualiza correctamente</returns>
        public bool Update(int index, string user, int value = 1) {
            var sqlcommand = new FbCommand() {
                CommandText = @"update pedprd set pedprd_tafrm_id = @activo, pedprd_fm = @fecha, pedprd_usu_m = @modifica where pedprd_id = @index;"
            };

            sqlcommand.Parameters.AddWithValue("@index", index);
            sqlcommand.Parameters.AddWithValue("@activo", value);
            sqlcommand.Parameters.AddWithValue("@fecha", DateTime.Now);
            sqlcommand.Parameters.AddWithValue("@modifica", user);
            return this.ExecuteTransaction(sqlcommand) > 0;
        }
        #endregion

        public bool Update(IOrdenProduccionC model) {
            var sqlCommand = new FbCommand() {
                CommandText = "UPDATE PEDPRD SET PEDPRD_CAN=@PEDPRD_CAN, PEDPRD_$UNI=@PEDPRD_$UNI, PEDPRD_$COT=@PEDPRD_$COT, PEDPRD_VEN_ID=@PEDPRD_VEN_ID, PEDPRD_NOM=@PEDPRD_NOM WHERE PEDPRD_ID=@INDEX"
            };
            sqlCommand.Parameters.AddWithValue("@PEDPRD_CAN", model.Cantidad);
            sqlCommand.Parameters.AddWithValue("@PEDPRD_$UNI", model.Unitario);
            //sqlCommand.Parameters.AddWithValue("@PEDPRD_$PED", model.TotalVenta);
            sqlCommand.Parameters.AddWithValue("@PEDPRD_$COT", model.TotalCosto);
            //sqlCommand.Parameters.AddWithValue("@PEDPRD_$UTI", model.Unitario);
            sqlCommand.Parameters.AddWithValue("@PEDPRD_VEN_ID", model.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@PEDPRD_NOM", model.Descripcion);
            sqlCommand.Parameters.AddWithValue("@INDEX", model.IdPedPrd);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PEDPRD.*, PEDST_STA, PEDST_SEC, DIR_NOM, VEN_CLA FROM PEDPRD LEFT JOIN PEDST ON PEDPRD_ST_ID = PEDST_ID LEFT JOIN DIR ON PEDPRD_DIR_ID = DIR_ID  LEFT JOIN VEN ON PEDPRD_VEN_ID = VEN_ID @condiciones ORDER BY PEDPRD_ID DESC"
            };

            if (typeof(T1) == typeof(OrdenProduccionModel)) {

            } else if (typeof(T1) == typeof(OrdenProduccionModel)) {
                sqlCommand.CommandText = @"SELECT PEDPRD.* FROM PEDFEC @condiciones ORDER BY PEDPRD_ID DESC";
            } else if (typeof(T1) == typeof(PedPrdModelView)) {
                sqlCommand.CommandText = @"SELECT PEDPRD.*, PEDST_STA, PEDST_SEC, DIR_NOM, VEN_CLA
                            FROM PEDPRD, PEDST, DIR, VEN WHERE PEDPRD_ST_ID = PEDST_ID AND DIR_ID = PEDPRD_DIR_ID AND PEDPRD_VEN_ID = VEN_ID @condiciones ORDER BY PEDPRD_ID DESC";
            } else if (typeof(T1) == typeof(OrdenProduccionReporte)) {
                sqlCommand.CommandText =
                    @"SELECT PEDPRD.*, PEDST_STA, PEDST_SEC, DIR_NOM, VEN_CLA, (SELECT first 1 PEDFEC_OBS FROM PEDFEC WHERE PEDFEC_PEDPRD_ID=PEDPRD_ID ORDER BY PEDFEC_ID DESC) FROM PEDPRD LEFT JOIN PEDST ON PEDPRD_ST_ID = PEDST_ID LEFT JOIN DIR ON PEDPRD_DIR_ID = DIR_ID  LEFT JOIN VEN ON PEDPRD_VEN_ID = VEN_ID @condiciones ORDER BY PEDPRD_ID DESC";
            } else if (typeof(T1) == typeof(PedPrdPedidoModelView)) {
                sqlCommand.CommandText = @"SELECT PEDPRD.*,
                                PEDST_STA, PEDST_SEC,
                                COTPRDS_ID, COTPRDS_A, COTPRDS_DIR_ID, COTPRDS_DIRCON_ID, COTPRDS_COT_ID, COTPRDS_CLI, COTPRDS_LPTEX, COTPRDS_COM_ID, COTPRDS_NOM, COTPRDS_$_ST2, COTPRDS_CAN, COTPRDS_UNI, COTPRDS_$UNI, COTPRDS_$, COTPRDS_UTI, COTPRDS_VEN_ID,
                                DIR_NOM, DIRCON_CON, VEN_CLA
                                FROM PEDPRD, PEDST, COTPRDS, DIR, DIRCON, VEN
                                WHERE PEDPRD_ST_ID = PEDST_ID AND PEDPRD_COTPRD_ID = COTPRDS_ID AND PEDPRD_DIR_ID = DIR_ID AND PEDPRD_DIRCON_ID = DIRCON_ID AND PEDPRD_VEN_ID = VEN_ID
                                @condiciones
                                ORDER BY PEDPRD_ID DESC, COTPRDS_ID DESC";
            } else if (typeof(T1) == typeof(OrdenProduccionEvaluacion1Item)) {
                sqlCommand.CommandText =
                    @"SELECT PEDPRD.*, PEDST_STA, PEDST_SEC, DIR_NOM, VEN_CLA, (SELECT first 1 PEDFEC_OBS FROM PEDFEC WHERE PEDFEC_PEDPRD_ID=PEDPRD_ID ORDER BY PEDFEC_ID DESC), RMSPED.RMSPED_FECEMS, RMSPED.RMSPED_REM
FROM PEDPRD 
LEFT JOIN PEDST ON PEDPRD_ST_ID = PEDST_ID 
LEFT JOIN DIR ON PEDPRD_DIR_ID = DIR_ID  
LEFT JOIN VEN ON PEDPRD_VEN_ID = VEN_ID  
LEFT JOIN RMSPED ON RMSPED.RMSPED_PEDPRD_ID = PEDPRD.PEDPRD_ID
@wcondiciones
ORDER BY PEDPRD_ID DESC";
            } else if (typeof(T1) == typeof(OrdenProduccionCostosModel)) {
                sqlCommand.CommandText = @"SELECT PEDPRD_ID, PEDPRD_COTPRD_ID, DIR_NOM, PEDPRD.PEDPRD_NOM, SUM (COTCOM_$_CEN) AS CEN, SUM(COTCOM_$_PRO) AS MO,SUM(COTCOM_$_MP) AS MP, SUM(COTCOM_$_CNS) AS CNS
                            FROM PEDPRD
                            LEFT JOIN DIR ON PEDPRD.PEDPRD_DIR_ID = DIR.DIR_ID
                            LEFT JOIN COTCOM ON PEDPRD_COTPRD_ID = COTCOM_COTPRD_ID AND PEDPRD_A > 0 AND COTCOM_A > 0
                            @wcondiciones
                            GROUP BY PEDPRD_ID, DIR_NOM, PEDPRD_NOM, PEDPRD_COTPRD_ID
                            ORDER BY PEDPRD_ID DESC";
            }
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        /// <summary>
        /// obtener listado de productos vendidos por periodos
        /// </summary>
        /// <param name="startDate">periodo inicial, si el valor endDate es nulo se toma por año y mes</param>
        /// <param name="endDate">fin del periodo</param>
        /// <param name="startStatus">status inicial en el caso de que endStatus es nulo entonces no es un rango sino status especifico</param>
        /// <param name="endStatus">fin del rango de status</param>
        public IEnumerable<PedPrdPedidoModelView> GetList(DateTime startDate, DateTime? endDate = null, int? startStatus = null, int? endStatus = null) {
            var sqlCommand = new FbCommand {
                CommandText = @"select PedPrd.*,
                                PedST_Sta, PedST_Sec,
                                CotPrds_ID, CotPrds_A, CotPrds_DIR_ID, CotPrds_DIRCON_ID, CotPrds_COT_ID, CotPrds_Cli, CotPrds_LPTex, COTPRDs_Com_ID, CotPrds_Nom, CotPrds_$_ST2, CotPrds_Can, CotPrds_Uni, CotPrds_$Uni, CotPrds_$, CotPrds_Uti, CotPrds_Ven_ID,
                                DIR_nom,
                                DIRCON_con,
                                Ven_Cla
                                from PedPrd, PedSt, COTPRDs, Dir, DirCon, Ven
                                where PedPrd_St_ID = PedSt_ID
                                and PEDPRD_COTPRD_ID = COTPRDs_ID
                                and PEDPRD_DIR_ID = DIR_ID
                                and PEDPRD_DIRCON_ID = DIRCON_ID
                                and PedPrd_Ven_ID = Ven_ID
                                @date
                                @status
                                order by PEDPRD_ID desc, COTPRDs_ID desc"
            };

            if (endDate == null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and extract(year from PedPrd_Fec_Ped) = @year and extract(month from PedPrd_Fec_Ped) = @month");
                sqlCommand.Parameters.AddWithValue("@year", startDate.Year);
                sqlCommand.Parameters.AddWithValue("@month", startDate.Month);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@date", "and PedPrd_Fec_Ped >= @startDate and PedPrd_Fec_Ped <= @endDate");
                sqlCommand.Parameters.AddWithValue("@startDate", startDate);
                sqlCommand.Parameters.AddWithValue("@endDate", endDate);
            }

            if (startStatus != null) {
                if (endStatus != null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and PedST_Sec >= @startStatus and PedST_Sec <= @endStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                    sqlCommand.Parameters.AddWithValue("@endStatus", endStatus);
                } else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and PedST_Sec = @startStatus");
                    sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                }
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "");
            }

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<PedPrdPedidoModelView>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<PedPrdPedidoModelView> GetListC(int year) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PEDPRD_ID,PedPrd_St_ID,DIR_NOM,PEDPRD_NOM, PEDPRD_CAN, PEDPRD_$UNI, PEDPRD_$PED, PEDPRD_$COT, PEDPRD_$UTI, PEDPRD_FEC_PED, PEDPRD_FEC_ACO, PEDPRD_REM,PEDPRD_$REM
                                COTPRD_UTI, COTPRD_$_IND, COTPRD_$_ADM, COTPRD_$_FIN, COTPRD_$_VEN, COTPRD_$_ST1, 
                                COTPRD_$_CEN, COTPRD_$_PRO, COTPRD_$_MP, COTPRD_$_CNS, 
                                sum(COTCOM_$_CEN), sum(COTCOM_$_PRO), sum(COTCOM_$_MP), sum(COTCOM_$_CNS), sum(COTCOM_$_TOT)
                                From PEDPRD,CotPrd, Dir, DIRCON , COTCOM
                                Where PedPrd_CotPrd_ID = CotPrd_ID AND PedPrd_DIR_ID = DIR_ID And PedPrd_DIRCON_ID = DIRCON_ID and extract(year from PedPrd_Fec_Ped) = @year
                                and COTCOM_A > 0 AND COTCOM_COTPRD_ID = pedprd_cotprd_id and PedPrd_St_ID > 3 and PedPrd_St_ID<9
                                group by PEDPRD_ID,PedPrd_St_ID,DIR_NOM,PEDPRD_NOM, PEDPRD_CAN, PEDPRD_$UNI, PEDPRD_$PED, PEDPRD_$COT, PEDPRD_$UTI, PEDPRD_FEC_PED, 
                                PEDPRD_FEC_ACO, PEDPRD_REM,PEDPRD_$REM, COTPRD_UTI, COTPRD_$_IND, COTPRD_$_ADM, COTPRD_$_FIN, COTPRD_$_VEN, COTPRD_$_ST1, 
                                COTPRD_$_CEN, COTPRD_$_PRO, COTPRD_$_MP, COTPRD_$_CNS
                                order by PEDPRD_ID desc"
            };
            sqlCommand.Parameters.AddWithValue("@year", year);
            return this.GetMapper<PedPrdPedidoModelView>(sqlCommand);
        }

        public bool SetFechaVoBo(int idPedPro, DateTime fechaVoBo) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE PEDPRD SET PEDPRD_FEC_VOBOC=@PEDPRD_FEC_VOBOC, PEDPRD_USU_M=@PEDPRD_USU_M WHERE PEDPRD_ID=@index"
            };
            sqlCommand.Parameters.AddWithValue("@PEDPRD_FEC_VOBOC", fechaVoBo);
            sqlCommand.Parameters.AddWithValue("@PEDPRD_USU_M", this.User);
            sqlCommand.Parameters.AddWithValue("@index", idPedPro);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public bool SetFechaLibera(int idPedPro, DateTime fechaLibera) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE PEDPRD SET PEDPRD_FEC_ALM2=@PEDPRD_FEC_ALM2, PEDPRD_USU_M=@PEDPRD_USU_M WHERE PEDPRD_ID=@index"
            };
            sqlCommand.Parameters.AddWithValue("@PEDPRD_FEC_ALM2", fechaLibera);
            sqlCommand.Parameters.AddWithValue("@PEDPRD_USU_M", this.User);
            sqlCommand.Parameters.AddWithValue("@index", idPedPro);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }
    }
}
