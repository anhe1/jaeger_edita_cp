﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Contribuyentes.Contracts;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbVendedorRepository : RepositoryMaster<VendedorModel>, ISqlVendedorRepository {
        public SqlFbVendedorRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE ven SET ven_a = 0 WHERE ven_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public VendedorModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "ven", "ven")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.GetMapper(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<VendedorModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "ven", "ven")
            };
            return this.GetMapper(sqlCommand);
        }

        /// <summary>
        /// catalogo de vendedores
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<VendedorDetailModel> GetList(bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "ven", "ven")
            };
            return this.GetMapper<VendedorDetailModel>(sqlCommand);
        }

        /// <summary>
        /// catalogo de vendedores
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT VEN.* FROM VEN @wcondiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public int Insert(VendedorModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO ven (ven_id, ven_a, ven_cla, ven_nom, ven_com, ven_fm) 
                    			VALUES (@ven_id, @ven_a, @ven_cla, @ven_nom, @ven_com, @ven_fm)"
            };
            item.IdVendedor = this.Max("VEN_ID");
            sqlCommand.Parameters.AddWithValue("@ven_id", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@ven_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@ven_cla", item.Clave);
            sqlCommand.Parameters.AddWithValue("@ven_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@ven_com", item.Comision);
            sqlCommand.Parameters.AddWithValue("@ven_fm", item.FechaModifica);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(VendedorModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE ven 
                    			SET ven_a = @ven_a, ven_cla = @ven_cla, ven_nom = @ven_nom, ven_com = @ven_com, ven_fm = @ven_fm 
                    			WHERE ven_id = @ven_id;"
            };
            sqlCommand.Parameters.AddWithValue("@ven_id", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@ven_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@ven_cla", item.Clave);
            sqlCommand.Parameters.AddWithValue("@ven_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@ven_com", item.Comision);
            sqlCommand.Parameters.AddWithValue("@ven_fm", item.FechaModifica);
            return this.ExecuteScalar(sqlCommand);
        }
    }
}
