﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de control de fallas (schklst)
    /// </summary>
    public class SqlFbSchklstRepository : RepositoryMaster<ChekListFallaModel>, ISqlSchklstRepository {

        public SqlFbSchklstRepository(DataBaseConfiguracion configuracion) : base(configuracion) { 
        }

        public int Insert(ChekListFallaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO schklst (schklst_id, schklst_a, schklst_chklst_id, schklst_proc_id, schklst_sec, schklst_desc, schklst_usu_n, schklst_usu_m, schklst_fn, schklst_fm) 
                    			VALUES (@schklst_id, @schklst_a, @schklst_chklst_id, @schklst_proc_id, @schklst_sec, @schklst_desc, @schklst_usu_n, @schklst_usu_m, @schklst_fn, @schklst_fm)"
            };
            item.IdFalla = this.Max("SCHKLST_ID");
            sqlCommand.Parameters.AddWithValue("@schklst_id", item.IdFalla);
            sqlCommand.Parameters.AddWithValue("@schklst_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@schklst_chklst_id", item.IdCheckList);
            sqlCommand.Parameters.AddWithValue("@schklst_proc_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@schklst_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@schklst_desc", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@schklst_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@schklst_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@schklst_fn", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@schklst_fm", item.FechaModifica);
            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdFalla;
            return 0;
        }

        public int Update(ChekListFallaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE schklst 
                    			SET schklst_a = @schklst_a, schklst_chklst_id = @schklst_chklst_id, schklst_proc_id = @schklst_proc_id, schklst_sec = @schklst_sec, schklst_desc = @schklst_desc, schklst_usu_n = @schklst_usu_n, schklst_usu_m = @schklst_usu_m, schklst_fn = @schklst_fn, schklst_fm = @schklst_fm 
                    			WHERE schklst_id = @schklst_id;"
            };
            sqlCommand.Parameters.AddWithValue("@schklst_id", item.IdFalla);
            sqlCommand.Parameters.AddWithValue("@schklst_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@schklst_chklst_id", item.IdCheckList);
            sqlCommand.Parameters.AddWithValue("@schklst_proc_id", item.IdProceso);
            sqlCommand.Parameters.AddWithValue("@schklst_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@schklst_desc", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@schklst_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@schklst_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@schklst_fn", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@schklst_fm", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE schklst SET schklst_a = 0 WHERE schklst_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ChekListFallaModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "schklst", "schklst")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.GetMapper(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<ChekListFallaModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "schklst", "schklst")
            };

            return this.GetMapper(sqlCommand);
        }
    }
}
