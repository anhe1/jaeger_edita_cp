﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// Requisitos (LC2)
    /// </summary>
    public class SqlFbLc2Repository : RepositoryMaster<Lc2Model>, ISqlLc2Repository {
        public SqlFbLc2Repository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE lc2 SET lc2_a = 0 WHERE lc2_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public Lc2Model GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM lc2 WHERE lc2_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Lc2Model>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        /// <summary>
        /// lista de especificaciones LC2
        /// </summary>
        public IEnumerable<Lc2Model> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from LC2 where LC2_a = 1 order by LC2_Sec"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Lc2Model>();
            return mapper.Map(tabla).ToList();
        }

        public IEnumerable<Lc2DetailModel> GetList(int[] indexs) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from LC2 where LC2_a = 1 order by LC2_Sec"
            };
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<Lc2DetailModel>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(Lc2Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO lc2 ( lc2_id, lc2_a, lc2_lc1_id, lc2_tex, lc2_sec, lc2_fm, lc2_com_id) 
                			             VALUES (@lc2_id,@lc2_a,@lc2_lc1_id,@lc2_tex,@lc2_sec,@lc2_fm,@lc2_com_id);"
            };
            item.Id = this.Max("LC2_ID");
            sqlCommand.Parameters.AddWithValue("@lc2_id", item.Id);
            sqlCommand.Parameters.AddWithValue("@lc2_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@lc2_lc1_id", item.IdLC1);
            sqlCommand.Parameters.AddWithValue("@lc2_tex", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@lc2_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@lc2_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@lc2_com_id", item.IdCom);

            if (this.ExecuteScalar(sqlCommand) > 0)
                return item.Id;
            return 0;
        }

        public int Update(Lc2Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE lc2 
                			SET lc2_a = @lc2_a, lc2_lc1_id = @lc2_lc1_id, lc2_tex = @lc2_tex, lc2_sec = @lc2_sec, lc2_fm = @lc2_fm, lc2_com_id = @lc2_com_id 
                			WHERE lc2_id = @lc2_id;"
            };
            sqlCommand.Parameters.AddWithValue("@lc2_id", item.Id);
            sqlCommand.Parameters.AddWithValue("@lc2_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@lc2_lc1_id", item.IdLC1);
            sqlCommand.Parameters.AddWithValue("@lc2_tex", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@lc2_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@lc2_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@lc2_com_id", item.IdCom);
            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
