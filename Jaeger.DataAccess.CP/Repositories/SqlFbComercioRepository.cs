﻿using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Empresa.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Domain.CP.Presupuesto.Entities;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbComercioRepository : RepositoryMaster<ComercioModel>, ISqlComercioRepository {
        protected ISqlProductosServiciosRepository productosServiciosRepository;
        protected ISqlLc2Repository lc2Repository;
        protected ISqlLcProRepository lcProRepository;
        protected ISqlLp2Repository sqlLp2Repository;

        public SqlFbComercioRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.productosServiciosRepository = new SqlFbProductosServiciosRepository(configuracion);
            this.lc2Repository = new SqlFbLc2Repository(configuracion);
            this.lcProRepository = new SqlFbLcProRepository(configuracion);
            this.sqlLp2Repository = new SqlFbLp2Repository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE com SET com_a = 0 WHERE com_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ComercioModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM com WHERE com_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.GetMapper(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<ComercioModel> GetList() {
            var sqlCommand = new FbCommand() {
                CommandText = @"select * from com where com_a = 1 order by com_cod"
            };

            return this.GetMapper(sqlCommand);
        }

        /// <summary>
        /// obtener claisficacion de productos, bienes y servicios con todos lo desendientes
        /// </summary>
        /// <param name="nivel">el nivel es 1</param>
        public IEnumerable<ComercioDetailModel> GetList(int nivel) {
            // obtener los tres niveles de la clasificacion
            var nivel1 = new List<ComercioDetailModel>(this.GetListDetail(1).ToList());
            var nivel2 = this.GetListDetail(2);
            var nivel3 = this.GetListDetail(3);
            // dividir en los niveles 1, 2, 3
            for (int i = 0; i < nivel1.Count(); i++) {
                nivel1[i].Lista = new BindingList<ComercioDetailModel>(nivel2.Where(it => it.IdIdr == nivel1[i].IdCom).ToList());
                for (int i2 = 0; i2 < nivel1[i].Lista.Count; i2++) {
                    nivel1[i].Lista[i2].Lista = new BindingList<ComercioDetailModel>(nivel3.Where(it => it.IdIdr == nivel1[i].Lista[i2].IdCom).ToList());
                }
            }

            return nivel1;
        }

        /// <summary>
        /// obtener vista COM3
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public IEnumerable<Comercio3ModelView> GetList(bool onlyActive) {
            var sqlCommand = new FbCommand() {
                CommandText = @"select * from com3 where com3_a > 0 order by com3_cod"
            };

            if (onlyActive == false) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("where com3_a > 0", "");
            }

            return this.GetMapper<Comercio3ModelView>(sqlCommand);
        }

        /// <summary>
        /// vista de clasificacion, productos y servicios incluye los modelos, marcas y unidades
        /// </summary>
        public IEnumerable<Comercio3DetailModelView> GetList(bool onlyActive, string search = null) {
            var sqlCommand = new FbCommand() {
                CommandText = @"select * from com3 where com3_a > 0 order by com3_cod"
            };

            if (search != null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("order", " and com3_n3 like '%@search%' order");
            }

            if (onlyActive == false) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("where com3_a > 0", "");
            }

            sqlCommand.Parameters.AddWithValue("@search", search);

            var result = this.GetMapper<Comercio3DetailModelView>(sqlCommand).ToList();
            var allIds = result.Select(it => it.IdCom).ToArray();

            var modelos = this.productosServiciosRepository.GetList(allIds);

            for (int i = 0; i < result.Count; i++) {
                result[i].Modelos = new BindingList<MateriaPrima4DetailModel>(modelos.Where(it => it.Mp4ComId == result[i].IdCom).ToList());
            }

            return result;
        }

        /// <summary>
        /// obtener listado de sub productos (2191)
        /// </summary>
        /// <param name="comIDR">21738</param>
        /// <param name="lc1">1</param>
        /// <returns></returns>
        public IEnumerable<SubProductoDetailModel> GetList(int comIDR = 21738, int lc1 = 1) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from Com where Com_IDR = @comidr and Com_LC1 = @lc1 and com_a = 1 order by com_ord"
            };

            sqlCommand.Parameters.AddWithValue("@comidr", comIDR);
            sqlCommand.Parameters.AddWithValue("@lc1", lc1);

            var result = this.GetMapper<SubProductoDetailModel>(sqlCommand).ToList();

            var requisitos = this.lc2Repository.GetList();
            var linea = lcProRepository.GetListView(new int[] { 0 });

            for (int i = 0; i < result.Count; i++) {
                result[i].Especificaciones = new BindingList<Lc2Model>(requisitos.Where(it => it.IdCom == result[i].IdCom).ToList());
                result[i].LineaProduccion = new BindingList<LineaProduccionView>(linea.Where(it => it.IdCom2 == result[i].IdCom).ToList());
            }

            return result;
        }

        /// <summary>
        /// lista de productos de venta
        /// </summary>
        /// <param name="comLP1">com_lp1 = 1</param>
        /// <param name="all">false</param>
        public IEnumerable<ComProductoModelView> GetList(int comLP1 = 1, bool all = false) {
            var sqlCommand = new FbCommand() {
                CommandText = @"select * from com where com_a > 0 and Com_LP1 = 1 order by Com_Ord"
            };

            sqlCommand.Parameters.AddWithValue("@comlp1", comLP1);
            sqlCommand.Parameters.AddWithValue("@activo", all);

            var result = this.GetMapper<ComProductoModelView>(sqlCommand).ToList();

            var ids = result.Select(it => it.IdCom).ToArray();
            var subproductos = this.sqlLp2Repository.GetListView(ids);

            for (int i = 0; i < result.Count(); i++) {
                result[i].SubProductos = new BindingList<LP2ModelView>(subproductos.Where(it => it.LP2_COM1_ID == result[i].IdCom).ToList());
            }

            return result;
        }

        /// <summary>
        /// Lista simple de productos de cotizaciones
        /// </summary>
        public IEnumerable<ComercioModel> GetListProductos() {
            var sqlCommand = new FbCommand() {
                CommandText = @"select * from com where com_a > 0 and Com_LP1 = 1 order by Com_Ord"
            };

            return this.GetMapper<ComercioModel>(sqlCommand).ToList();
        }

        /// <summary>
        /// obtener Lista Linea Componentes (vista LC1)
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ComercioModel> GetList(int comLC1, bool onlyActive, bool componente) {
            var sqlCommand = new FbCommand {
                CommandText = @"Select Com_ID,Com_A,Com_Nom,Com_Ord,Com_FM
                                From COM
                                Where Com_LC1 = @index order by Com_Ord;"
            };

            sqlCommand.Parameters.AddWithValue("@index", comLC1);
            sqlCommand.Parameters.AddWithValue("@activo", onlyActive);
            sqlCommand.Parameters.AddWithValue("@componente", componente);

            return this.GetMapper(sqlCommand);
        }

        public int Insert(ComercioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO com (com_id, com_idr, com_cod, com_fm, com_a, com_niv, com_sec, com_nom, com_idv, com_autosol, com_lc1, com_mp, com_ord, com_lp1, com_uti, com_tee, com_usu_n, com_usu_m, com_bps) 
                			VALUES (@com_id, @com_idr, @com_cod, @com_fm, @com_a, @com_niv, @com_sec, @com_nom, @com_idv, @com_autosol, @com_lc1, @com_mp, @com_ord, @com_lp1, @com_uti, @com_tee, @com_usu_n, @com_usu_m, @com_bps)"
            };
            item.IdCom = this.Max("COM_ID");
            sqlCommand.Parameters.AddWithValue("@com_id", item.IdCom);
            sqlCommand.Parameters.AddWithValue("@com_idr", item.IdIdr);
            sqlCommand.Parameters.AddWithValue("@com_cod", item.Codigo);
            sqlCommand.Parameters.AddWithValue("@com_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@com_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@com_niv", item.Nivel);
            sqlCommand.Parameters.AddWithValue("@com_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@com_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@com_idv", item.Idv);
            sqlCommand.Parameters.AddWithValue("@com_autosol", item.AutoSol);
            sqlCommand.Parameters.AddWithValue("@com_lc1", item.LC1);
            sqlCommand.Parameters.AddWithValue("@com_mp", item.MP);
            sqlCommand.Parameters.AddWithValue("@com_ord", item.Orden);
            sqlCommand.Parameters.AddWithValue("@com_lp1", item.LP1);
            sqlCommand.Parameters.AddWithValue("@com_uti", item.Uti);
            sqlCommand.Parameters.AddWithValue("@com_tee", item.TEE);
            sqlCommand.Parameters.AddWithValue("@com_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@com_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@com_bps", item.BPS);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdCom;
            return 0;
        }

        public int Update(ComercioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE com 
                			SET com_idr = @_idr, com_cod = @com_cod, com_fm = @com_fm, com_a = @com_a, com_niv = @com_niv, com_sec = @com_sec, com_nom = @com_nom, com_idv = @com_idv, com_autosol = @com_autosol, com_lc1 = @com_lc1, com_mp = @com_mp, com_ord = @com_ord, com_lp1 = @com_lp1, com_uti = @com_uti, com_tee = @com_tee, com_usu_n = @com_usu_n, com_usu_m = @com_usu_m, com_bps = @com_bps 
                			WHERE com_id = @com_id;"
            };
            sqlCommand.Parameters.AddWithValue("@com_id", item.IdCom);
            sqlCommand.Parameters.AddWithValue("@com_idr", item.IdIdr);
            sqlCommand.Parameters.AddWithValue("@com_cod", item.Codigo);
            sqlCommand.Parameters.AddWithValue("@com_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@com_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@com_niv", item.Nivel);
            sqlCommand.Parameters.AddWithValue("@com_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@com_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@com_idv", item.Idv);
            sqlCommand.Parameters.AddWithValue("@com_autosol", item.AutoSol);
            sqlCommand.Parameters.AddWithValue("@com_lc1", item.LC1);
            sqlCommand.Parameters.AddWithValue("@com_mp", item.MP);
            sqlCommand.Parameters.AddWithValue("@com_ord", item.Orden);
            sqlCommand.Parameters.AddWithValue("@com_lp1", item.LP1);
            sqlCommand.Parameters.AddWithValue("@com_uti", item.Uti);
            sqlCommand.Parameters.AddWithValue("@com_tee", item.TEE);
            sqlCommand.Parameters.AddWithValue("@com_usu_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@com_usu_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@com_bps", item.BPS);
            return this.ExecuteTransaction(sqlCommand);
        }

        /// <summary>
        /// obtener clasificacion de productos, bienes y servicios del nivel especificado
        /// </summary>
        private IEnumerable<ComercioDetailModel> GetListDetail(int nivel) {
            var sqlCommand = new FbCommand() {
                CommandText = @"select * from com where com_niv = @nivel and com_a = 1 order by Com_Cod"
            };
            sqlCommand.Parameters.AddWithValue("@nivel", nivel);
            return this.GetMapper<ComercioDetailModel>(sqlCommand).ToList();
        }
    }
}
