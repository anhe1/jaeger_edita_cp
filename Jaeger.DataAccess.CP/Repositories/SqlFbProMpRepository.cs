﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// repositorio de proceso materia prima (promp)
    /// </summary>
    public class SqlFbProMpRepository : RepositoryMaster<ProMpModel>, ISqlProMpRepository {
        public SqlFbProMpRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE promp SET promp_a = 0 WHERE promp_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ProMpModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_ID = @id", "promp", "promp")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProMpModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ProMpModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM {0} WHERE {1}_A > 0", "promp", "promp")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProMpModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener listado de objetos Detail relacionado al proceso
        /// </summary>
        /// <param name="index">indice de relacion con el proceso</param>
        public IEnumerable<ProMpDetailModel> GetList(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from PROMP where PROMP_A > 0"
            };
            //sqlCommand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProMpDetailModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// consumibles (PROMP)
        /// </summary>
        public IEnumerable<ProMpDetailModel> GetList(int[] index) {
            var sqlLinea = new FbCommand {
                CommandText = @"select * from PROMP where PROMP_A > 0 and PROMP_PRO_ID in (@index) order by PROMP_ID"
            };

            sqlLinea.CommandText = sqlLinea.CommandText.Replace("@index", string.Join(",", index.ToArray()));
            var tabla = this.ExecuteReader(sqlLinea);
            var mapper = new DataNamesMapper<ProMpDetailModel>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener vista materia prima o consumibles indices PROMP_PRO_ID (PROMP)
        /// </summary>
        public IEnumerable<ProMpDetailModelView> GetList(bool onlyActive, int[] index) {
            var sqlCommand = new FbCommand {
                CommandText = @"select ProMP.*,
                                Com3_ID, Com3_A, Com3_Cod, Com3_N1, Com3_N2, Com3_N3,
                                ComMod_ID, ComMod_A, ComMod_Mod, ComMod_nom, ComMod_Mar, ComMod_UniNom, ComMod_$Uni, ComMod_Mul, ComMod_Com_ID
                                from PROMP , Com3, ComMod
                                where PROMP_A > 0 
                                and PROMP_Com_ID = Com3_ID
                                and PROMP_MP4_ID = ComMod_ID
                                and PROMP_PRO_ID in (@index) 
                                order by PROMP_ID"
            };

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", string.Join(",", index.ToArray()));

            if (onlyActive)
                sqlCommand.Parameters.AddWithValue("@activo", 1);

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProMpDetailModelView>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(ProMpModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO promp (promp_id, promp_a, promp_pro_id, promp_mp_id, promp_mp4_id, promp_can, promp_mpmar_id, promp_can_ent, promp_$_tot, promp_sec, promp_lib, promp_fyv, promp_tra, promp_trb, promp_trc, promp_tpa, promp_tpb, promp_tpc, promp_mra, promp_mrb, promp_mpa, promp_mpb, promp_can_pro, promp_mul, promp_usu_fm, promp_fm, promp_com_id) 
                			VALUES (@promp_id, @promp_a, @promp_pro_id, @promp_mp_id, @promp_mp4_id, @promp_can, @promp_mpmar_id, @promp_can_ent, @promp_$_tot, @promp_sec, @promp_lib, @promp_fyv, @promp_tra, @promp_trb, @promp_trc, @promp_tpa, @promp_tpb, @promp_tpc, @promp_mra, @promp_mrb, @promp_mpa, @promp_mpb, @promp_can_pro, @promp_mul, @promp_usu_fm, @promp_fm, @promp_com_id)"
            };
            item.IdProMP = this.Max("PROMP_ID");
            sqlCommand.Parameters.AddWithValue("@promp_id", item.IdProMP);
            sqlCommand.Parameters.AddWithValue("@promp_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@promp_pro_id", item.PROMP_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@promp_mp_id", item.PROMP_MP_ID);
            sqlCommand.Parameters.AddWithValue("@promp_mp4_id", item.PROMP_MP4_ID);
            sqlCommand.Parameters.AddWithValue("@promp_can", item.CantidadSalida);
            sqlCommand.Parameters.AddWithValue("@promp_mpmar_id", item.PROMP_MPMAR_ID);
            sqlCommand.Parameters.AddWithValue("@promp_can_ent", item.CantidadEntrada);
            sqlCommand.Parameters.AddWithValue("@promp_$_tot", item.PROMP__TOT);
            sqlCommand.Parameters.AddWithValue("@promp_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@promp_lib", item.PROMP_LIB);
            sqlCommand.Parameters.AddWithValue("@promp_fyv", item.PROMP_FYV);
            sqlCommand.Parameters.AddWithValue("@promp_tra", item.PROMP_TRA);
            sqlCommand.Parameters.AddWithValue("@promp_trb", item.PROMP_TRB);
            sqlCommand.Parameters.AddWithValue("@promp_trc", item.PROMP_TRC);
            sqlCommand.Parameters.AddWithValue("@promp_tpa", item.PROMP_TPA);
            sqlCommand.Parameters.AddWithValue("@promp_tpb", item.PROMP_TPB);
            sqlCommand.Parameters.AddWithValue("@promp_tpc", item.PROMP_TPC);
            sqlCommand.Parameters.AddWithValue("@promp_mra", item.PROMP_MRA);
            sqlCommand.Parameters.AddWithValue("@promp_mrb", item.PROMP_MRB);
            sqlCommand.Parameters.AddWithValue("@promp_mpa", item.PROMP_MPA);
            sqlCommand.Parameters.AddWithValue("@promp_mpb", item.PROMP_MPB);
            sqlCommand.Parameters.AddWithValue("@promp_can_pro", item.CantidadProduccion);
            sqlCommand.Parameters.AddWithValue("@promp_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@promp_usu_fm", item.PROMP_USU_FM);
            sqlCommand.Parameters.AddWithValue("@promp_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@promp_com_id", item.IdCom);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdProMP;
            return 0;
        }

        public int Update(ProMpModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE promp 
                			SET promp_a = @promp_a, promp_pro_id = @promp_pro_id, promp_mp_id = @promp_mp_id, promp_mp4_id = @promp_mp4_id, promp_can = @promp_can, promp_mpmar_id = @promp_mpmar_id, promp_can_ent = @promp_can_ent, promp_$_tot = @promp_$_tot, promp_sec = @promp_sec, promp_lib = @promp_lib, promp_fyv = @promp_fyv, promp_tra = @promp_tra, promp_trb = @promp_trb, promp_trc = @promp_trc, promp_tpa = @promp_tpa, promp_tpb = @promp_tpb, promp_tpc = @promp_tpc, promp_mra = @promp_mra, promp_mrb = @promp_mrb, promp_mpa = @promp_mpa, promp_mpb = @promp_mpb, promp_can_pro = @promp_can_pro, promp_mul = @promp_mul, promp_usu_fm = @promp_usu_fm, promp_fm = @promp_fm, promp_com_id = @promp_com_id 
                			WHERE id = @id;"
            };

            sqlCommand.Parameters.AddWithValue("@promp_id", item.IdProMP);
            sqlCommand.Parameters.AddWithValue("@promp_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@promp_pro_id", item.PROMP_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@promp_mp_id", item.PROMP_MP_ID);
            sqlCommand.Parameters.AddWithValue("@promp_mp4_id", item.PROMP_MP4_ID);
            sqlCommand.Parameters.AddWithValue("@promp_can", item.CantidadSalida);
            sqlCommand.Parameters.AddWithValue("@promp_mpmar_id", item.PROMP_MPMAR_ID);
            sqlCommand.Parameters.AddWithValue("@promp_can_ent", item.CantidadEntrada);
            sqlCommand.Parameters.AddWithValue("@promp_$_tot", item.PROMP__TOT);
            sqlCommand.Parameters.AddWithValue("@promp_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@promp_lib", item.PROMP_LIB);
            sqlCommand.Parameters.AddWithValue("@promp_fyv", item.PROMP_FYV);
            sqlCommand.Parameters.AddWithValue("@promp_tra", item.PROMP_TRA);
            sqlCommand.Parameters.AddWithValue("@promp_trb", item.PROMP_TRB);
            sqlCommand.Parameters.AddWithValue("@promp_trc", item.PROMP_TRC);
            sqlCommand.Parameters.AddWithValue("@promp_tpa", item.PROMP_TPA);
            sqlCommand.Parameters.AddWithValue("@promp_tpb", item.PROMP_TPB);
            sqlCommand.Parameters.AddWithValue("@promp_tpc", item.PROMP_TPC);
            sqlCommand.Parameters.AddWithValue("@promp_mra", item.PROMP_MRA);
            sqlCommand.Parameters.AddWithValue("@promp_mrb", item.PROMP_MRB);
            sqlCommand.Parameters.AddWithValue("@promp_mpa", item.PROMP_MPA);
            sqlCommand.Parameters.AddWithValue("@promp_mpb", item.PROMP_MPB);
            sqlCommand.Parameters.AddWithValue("@promp_can_pro", item.CantidadProduccion);
            sqlCommand.Parameters.AddWithValue("@promp_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@promp_usu_fm", item.PROMP_USU_FM);
            sqlCommand.Parameters.AddWithValue("@promp_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@promp_com_id", item.IdCom);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
