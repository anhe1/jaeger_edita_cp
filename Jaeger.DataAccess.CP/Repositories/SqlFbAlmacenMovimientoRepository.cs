﻿using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Almacen.Contracts;
using Jaeger.Domain.CP.Almacen.Entities;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbAlmacenMovimientoRepository : RepositoryMaster<AlmacenMovModel>, ISqlAlmacenMovimientoRepository {
        public SqlFbAlmacenMovimientoRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            throw new System.NotImplementedException();
        }

        public AlmacenMovModel GetById(int index) {
            throw new System.NotImplementedException();
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT ALMMOV.* FROM ALMMOV "
            };
            if (typeof(T1) == typeof(MaterialOPModel)) {
                sqlCommand.CommandText = @"SELECT MAX(ALMMOV_ID) AS ALMMOV_ID, ALMMOV_A, ALMMOV_MP_ID, ALMMOV_OP_ID, SUM(ALMMOV_PED) AS ALMMOV_PED, SUM(ALMMOV_SOL) AS ALMMOV_SOL, SUM(ALMMOV_ENT) AS ALMMOV_ENT, SUM(ALMMOV_SAL) AS ALMMOV_SAL, 
SUM(ALMMOV_ENT-ALMMOV_SAL) AS MATOP_EXI, SUM(ALMMOV_PED-ALMMOV_SOL+ALMMOV_ENT-ALMMOV_SAL) AS MATOP_EXC FROM ALMMOV @wcondiciones GROUP BY ALMMOV_A, ALMMOV_MP_ID, ALMMOV_OP_ID HAVING ALMMOV_A>0;";
            }

            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals));
        }

        public IEnumerable<AlmacenMovModel> GetList() {
            throw new System.NotImplementedException();
        }

        public int Insert(AlmacenMovModel item) {
            throw new System.NotImplementedException();
        }

        public int Update(AlmacenMovModel item) {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
