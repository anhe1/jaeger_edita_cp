﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// Línea de Producción (PedPro)
    /// </summary>
    public class SqlFbPedProERepository : RepositoryMaster<LineaProduccionModelE>, ISqlPedProERepository {
        public SqlFbPedProERepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedproe SET pedproe_a = 0 WHERE pedproe_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public LineaProduccionModelE GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM pedproe WHERE pedproe_id = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionModelE>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<LineaProduccionModelE> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM pedproe WHERE pedproe_A > 0"
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionModelE>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener lista PedPro_PedCom_ID
        /// </summary>
        public IEnumerable<LineaProduccionModelE> GetList(int[] ProdID) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from PedProe where PedProe_a > 0 and PedPro_PedCom_ID in (@ProID) order by PedProe_Sec;"
            };
            //sqlCommand.Parameters.AddWithValue("@ProID", ProdID);
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@ProID", string.Join(",", ProdID));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionModelE>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener lista PedPro_PedCom_ID (solo vista)
        /// </summary>
        public IEnumerable<LineaProduccionDeptoCentroView> GetListView(int[] ProdID) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from PedPro, Edp3, Edp2, Edp1 where PedPro_a > 0 and PedPro_PedCom_ID in (@ProID) and Edp3_ID = PedPro_Pro_ID and Edp3_Edp2_ID = Edp2_ID and Edp2_Edp1_ID = Edp1_ID order by PedPro_Sec;"
            };
            //sqlCommand.Parameters.AddWithValue("@ProID", ProdID);
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@ProID", string.Join(",", ProdID));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<LineaProduccionDeptoCentroView>();
            return mapper.Map(tabla).ToList();
        }

        /// <summary>
        /// obtener vista de componente y linea de produccion
        /// </summary>
        /// <param name="startDateTime">fecha de inicio</param>
        /// <param name="endDateTime">fecha final del rango, null si debe obtener todo el mes</param>
        /// <param name="startStatus">inicio de status, puede ser null si debe ser ignorado</param>
        /// <param name="endStatus">fin de status, puede ser null si debe ser ignorado</param>
        /// <param name="idDepartamento">indice del departamento</param>
        /// <param name="fieldDate">0 = fecha de pedido, 1 = fecha final de la captura de produccion</param>
        /// <returns>lista de componentes y linea de produccion</returns>
        public IEnumerable<ComponenteLineaProduccionView> GetList(DateTime startDateTime, DateTime? endDateTime, int? startStatus, int? endStatus, int idDepartamento, int fieldDate = 0, bool onlyYear = false, bool onlyActive = false) {
            var sqlCommand = new FbCommand {
                CommandText = @"select PEDPRD_ID, PedPrd_St_ID, DIR_Nom, PEDPRD_Nom, PEDPRD_Can, PedPrd_Fec_Ped, PEDPRD_Fec_OP,PEDPRD_FEC_ALM2, PEDPRD_Fec_Req, PEDPRD_Fec_Aco, PEDPRD_FEC_VOBOC, PedPrd_Jer, PEDPRD_Obs, PedPrd_Usu_N,PedPrd_Usu_M, PedPrd_FM,
                                PedST_Sta, PedST_Sec, 
                                PedCom_ID, PedCom_sec, PedCom_Cla, PedCom_Nom, PedCom_Can_Com, PedCom_T_Max, PedCom_MinR, PedCom_Fec_Min, PedCom_Fec_Max, PedCom_Fec_Ini, PedCom_Fec_Fin,
                                PedPro.*,
                                EDP3_ID, EDP1_are, EDP1_ID, EDP1_nom, EDP2_sec, EDP2_nom, EDP3_nom, EDP2_val_min
                                from PEDPRD, PedST, Dir, PedCom, PedPro, Edp3, Edp2, Edp1
                                where PedPrd_St_ID = PedSt_ID
                                and DIR_ID = PedPrd_DIR_ID
                                and PedCom_PedPrd_ID = PedPrd_Id
                                and PedPro_PedCom_ID = PedCom_ID
                                and Edp3_ID = PedPro_Pro_ID
                                and Edp3_Edp2_ID = Edp2_ID
                                and Edp2_Edp1_ID = Edp1_ID
                                @dateTime @status
                                and EDP1_ID = @idDepartamento
                                order by PedPro_Sec, PedCom_Sec, PedPrd_ID, EDP1_nom, EDP2_sec,EDP2_nom,EDP3_nom"
            };
            // por mes y año
            if (endDateTime == null) {
                if (onlyYear) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@dateTime", "and extract(year from @fieldDate) = @year");
                    sqlCommand.Parameters.AddWithValue("@year", startDateTime.Year);
                }
                else {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@dateTime", "and extract(year from @fieldDate) = @year and extract(month from @fieldDate) = @month");
                    sqlCommand.Parameters.AddWithValue("@year", startDateTime.Year);
                    sqlCommand.Parameters.AddWithValue("@month", startDateTime.Month);
                }
            }
            else { // por rango de fechas
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@dateTime", "and @fieldDate >= @startDateTime and @fieldDate <= @endDateTime");
                sqlCommand.Parameters.AddWithValue("@startDateTime", startDateTime);
                sqlCommand.Parameters.AddWithValue("@endDateTime", endDateTime);
            }

            if (startStatus == null | endStatus == null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "");
            }
            else { // por rango de status
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@status", "and PedST_Sec >= @startStatus and PedST_Sec<=@endStatus");
                sqlCommand.Parameters.AddWithValue("@startStatus", startStatus);
                sqlCommand.Parameters.AddWithValue("@endStatus", endStatus);
            }

            // fecha final de produccion
            if (fieldDate == 1) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@fieldDate", "pedpro_fec_fin");
            }
            else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@fieldDate", "pedprd_fec_ped");
            }

            if (onlyActive) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@idDepartamento", "@idDepartamento and pedpro_fec_fin is null");
            }

            sqlCommand.Parameters.AddWithValue("@idDepartamento", idDepartamento);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ComponenteLineaProduccionView>();
            return mapper.Map(tabla).ToList();
        }

        public int Insert(LineaProduccionModelE item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO pedpro (pedpro_id, pedpro_a, pedpro_pedcom_id, pedpro_cotprd_id, pedpro_cotpro_id, pedpro_cen_id, pedpro_pro_id, pedpro_sec, pedpro_can_prot, pedpro_t_max, pedpro_minr, pedpro_nt, pedpro_fec_min, pedpro_fec_max, pedpro_fec_ini, pedpro_fec_fin, pedpro_fpe, pedpro_fpp, pedpro_fps, pedpro_fre, pedpro_frs, pedpro_usu_fm, pedpro_usu_m, pedpro_fm, pedpro_cen_ord, pedpro_can_pror, pedpro_ope, pedpro_obs, pedpro_ntr) 
                            VALUES (@pedpro_id, @pedpro_a, @pedpro_pedcom_id, @pedpro_cotprd_id, @pedpro_cotpro_id, @pedpro_cen_id, @pedpro_pro_id, @pedpro_sec, @pedpro_can_prot, @pedpro_t_max, @pedpro_minr, @pedpro_nt, @pedpro_fec_min, @pedpro_fec_max, @pedpro_fec_ini, @pedpro_fec_fin, @pedpro_fpe, @pedpro_fpp, @pedpro_fps, @pedpro_fre, @pedpro_frs, @pedpro_usu_fm, @pedpro_usu_m, @pedpro_fm, @pedpro_cen_ord, @pedpro_can_pror, @pedpro_ope, @pedpro_obs, @pedpro_ntr)"
            };
            item.IdPedPro = this.Max("PEDPRO_ID");
            sqlCommand.Parameters.AddWithValue("@pedpro_id", item.IdPedPro);
            sqlCommand.Parameters.AddWithValue("@pedpro_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedpro_pedcom_id", item.IdComponente);
            sqlCommand.Parameters.AddWithValue("@pedpro_pro_id", item.PEDPRO_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_minr", item.TiempoReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_fin", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@pedpro_can_pror", item.CantidadReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_ope", item.Oficial);
            sqlCommand.Parameters.AddWithValue("@pedpro_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedpro_ntr", item.NumeroTrabajdoresReal);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdPedPro;
            return 0;
        }

        public int Update(LineaProduccionModelE item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedpro 
                            SET pedpro_a = @pedpro_a, pedpro_pedcom_id = @pedpro_pedcom_id, pedpro_cotprd_id = @pedpro_cotprd_id, pedpro_cotpro_id = @pedpro_cotpro_id, pedpro_cen_id = @pedpro_cen_id, pedpro_pro_id = @pedpro_pro_id, pedpro_sec = @pedpro_sec, pedpro_can_prot = @pedpro_can_prot, pedpro_t_max = @pedpro_t_max, pedpro_minr = @pedpro_minr, pedpro_nt = @pedpro_nt, pedpro_fec_min = @pedpro_fec_min, pedpro_fec_max = @pedpro_fec_max, pedpro_fec_ini = @pedpro_fec_ini, pedpro_fec_fin = @pedpro_fec_fin, pedpro_fpe = @pedpro_fpe, pedpro_fpp = @pedpro_fpp, pedpro_fps = @pedpro_fps, pedpro_fre = @pedpro_fre, pedpro_frs = @pedpro_frs, pedpro_usu_fm = @pedpro_usu_fm, pedpro_usu_m = @pedpro_usu_m, pedpro_fm = @pedpro_fm, pedpro_cen_ord = @pedpro_cen_ord, pedpro_can_pror = @pedpro_can_pror, pedpro_ope = @pedpro_ope, pedpro_obs = @pedpro_obs, pedpro_ntr = @pedpro_ntr 
                            WHERE pedpro_id = @pedpro_id;"
            };
            sqlCommand.Parameters.AddWithValue("@pedpro_id", item.IdPedPro);
            sqlCommand.Parameters.AddWithValue("@pedpro_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@pedpro_pedcom_id", item.IdComponente);
            sqlCommand.Parameters.AddWithValue("@pedpro_pro_id", item.PEDPRO_PRO_ID);
            sqlCommand.Parameters.AddWithValue("@pedpro_minr", item.TiempoReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_fec_fin", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@pedpro_can_pror", item.CantidadReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_ope", item.Oficial);
            sqlCommand.Parameters.AddWithValue("@pedpro_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedpro_ntr", item.NumeroTrabajdoresReal);
            return this.ExecuteTransaction(sqlCommand);
        }

        /// <summary>
        /// actualizar avance de produccion
        /// </summary>
        public bool UpdateAvance(LineaProduccionModelE item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE pedpro 
                            SET pedpro_minr = @pedpro_minr, pedpro_usu_m = @pedpro_usu_m, pedpro_fm = @pedpro_fm, pedpro_can_pror = @pedpro_can_pror, pedpro_ope=@pedpro_ope, pedpro_obs = @pedpro_obs, pedpro_ntr = @pedpro_ntr
                            WHERE pedpro_id = @pedpro_id;"
            };
            sqlCommand.Parameters.AddWithValue("@pedpro_id", item.IdPedPro);
            //sqlCommand.Parameters.AddWithValue("@pedpro_a", item.Activo);
            //sqlCommand.Parameters.AddWithValue("@pedpro_pedcom_id", item.IdComponente);
            //sqlCommand.Parameters.AddWithValue("@pedpro_cotprd_id", item.PEDPRO_COTPRD_ID);
            //sqlCommand.Parameters.AddWithValue("@pedpro_cotpro_id", item.PEDPRO_COTPRO_ID);
            //sqlCommand.Parameters.AddWithValue("@pedpro_cen_id", item.PEDPRO_CEN_ID);
            //sqlCommand.Parameters.AddWithValue("@pedpro_pro_id", item.PEDPRO_PRO_ID);
            //sqlCommand.Parameters.AddWithValue("@pedpro_sec", item.Secuencia);
            //sqlCommand.Parameters.AddWithValue("@pedpro_can_prot", item.CantidadEstimada);
            //sqlCommand.Parameters.AddWithValue("@pedpro_t_max", item.TiempoEstimado);
            sqlCommand.Parameters.AddWithValue("@pedpro_minr", item.TiempoReal);
            //sqlCommand.Parameters.AddWithValue("@pedpro_nt", item.NumeroTrabajadoresEstimado);
            //sqlCommand.Parameters.AddWithValue("@pedpro_fec_min", item.FechaMinima);
            //sqlCommand.Parameters.AddWithValue("@pedpro_fec_max", item.FechaMaxima);
            //sqlCommand.Parameters.AddWithValue("@pedpro_fec_ini", item.FechaInicio);
            //sqlCommand.Parameters.AddWithValue("@pedpro_fec_fin", item.FechaFin);
            //sqlCommand.Parameters.AddWithValue("@pedpro_fpe", item.PEDPRO_FPE);
            //sqlCommand.Parameters.AddWithValue("@pedpro_fpp", item.PEDPRO_FPP);
            //sqlCommand.Parameters.AddWithValue("@pedpro_fps", item.PEDPRO_FPS);
            //sqlCommand.Parameters.AddWithValue("@pedpro_fre", item.PEDPRO_FRE);
            //sqlCommand.Parameters.AddWithValue("@pedpro_frs", item.PEDPRO_FRS);
            //sqlCommand.Parameters.AddWithValue("@pedpro_usu_fm", item.PEDPRO_USU_FM);
            sqlCommand.Parameters.AddWithValue("@pedpro_fm", DateTime.Now);
            //sqlCommand.Parameters.AddWithValue("@pedpro_cen_ord", item.PEDPRO_CEN_ORD);
            sqlCommand.Parameters.AddWithValue("@pedpro_can_pror", item.CantidadReal);
            sqlCommand.Parameters.AddWithValue("@pedpro_ope", item.Oficial);
            sqlCommand.Parameters.AddWithValue("@pedpro_obs", item.Nota);
            sqlCommand.Parameters.AddWithValue("@pedpro_ntr", item.NumeroTrabajdoresReal);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        /// <summary>
        /// actualizar la fecha planeada del proceso
        /// </summary>
        /// <param name="idPedPro">PedPro_ID</param>
        /// <param name="startDate">fecha de inicio</param>
        /// <param name="endDate">fecha final</param>
        /// <param name="user">clave del usuario</param>
        /// <returns>verdadero si actualiza la fecha</returns>
        public bool Update(int idPedPro, DateTime startDate, DateTime endDate, string user) {
            var sqlCommand = new FbCommand {
                CommandText = "update PEDPRO set PedPro_Fec_Min=@PedPro_Fec_Min, PedPro_Fec_Max=@PedPro_Fec_Max, PEDPRO_USU_M=@PEDPRO_USU_M where PedPro_ID=@index"
            };
            sqlCommand.Parameters.AddWithValue("@PedPro_Fec_Min", startDate);
            sqlCommand.Parameters.AddWithValue("@PedPro_Fec_Max", endDate);
            sqlCommand.Parameters.AddWithValue("@PEDPRO_USU_M", user);
            sqlCommand.Parameters.AddWithValue("@index", idPedPro);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }
    }
}
