﻿/// purpose: repositorio de status de remision (REMST)
/// develop: ANHE1 27092020 0121
using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Domain.CP.Empresa.Contracts;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.CP.Repositories {
    public class SqlFbRequerimientoCompraRepository : RepositoryMaster<ReqCompraModel>, ISqlRequerimientoCompraRepository {
        public SqlFbRequerimientoCompraRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM OCDP WHERE ((OCDP_ID = @p1))" };
            throw new System.NotImplementedException();
        }

        public ReqCompraModel GetById(int index) {
            return this.GetList<ReqCompraModel>(new List<Conditional> { 
                new Conditional("OCDP_ID", index.ToString()) 
            }).FirstOrDefault();
        }

        public IEnumerable<ReqCompraModel> GetList() {
            return this.GetList<ReqCompraModel>(new List<Conditional>());
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT OCDP.* FROM OCDP @wcondiciones ORDER BY OCDP_ID DESC"
            };

            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals));
        }

        public int Insert(ReqCompraModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO OCDP (OCDP_ID, OCDP_VER, OCDP_STTS_ID, OCDP_DRCTR_ID, OCDP_CTDP_ID, OCDP_PDCL_ID, OCDP_TIPO_ID, OCDP_NOMR, OCDP_CNTC, OCDP_NOTA, OCDP_CMAIL, OCDP_FECACO, OCDP_TEL, OCDP_SBTTL, OCDP_DESC, OCDP_TOTAL, OCDP_USR_A, OCDP_FEC_A, OCDP_FCCNCL, OCDP_USR_C, OCDP_CVCAN, OCDP_CLMTV, OCDP_CLNTA, OCDP_FN, OCDP_USR_N) 
VALUES (@OCDP_ID, @OCDP_VER, @OCDP_STTS_ID, @OCDP_DRCTR_ID, @OCDP_CTDP_ID, @OCDP_PDCL_ID, @OCDP_TIPO_ID, @OCDP_NOMR, @OCDP_CNTC, @OCDP_NOTA, @OCDP_CMAIL, @OCDP_FECACO, @OCDP_TEL, @OCDP_SBTTL, @OCDP_DESC, @OCDP_TOTAL, @OCDP_USR_A, @OCDP_FEC_A, @OCDP_FCCNCL, @OCDP_USR_C, @OCDP_CVCAN, @OCDP_CLMTV, @OCDP_CLNTA, @OCDP_FN, @OCDP_USR_N) RETURNING OCDP_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@OCDP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@OCDP_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@OCDP_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@OCDP_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@OCDP_CTDP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@OCDP_PDCL_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@OCDP_TIPO_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@OCDP_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@OCDP_CNTC", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@OCDP_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@OCDP_CMAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@OCDP_FECACO", item.FechaAcuerdo);
            sqlCommand.Parameters.AddWithValue("@OCDP_TEL", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@OCDP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@OCDP_DESC", item.TotalDescuento);
            sqlCommand.Parameters.AddWithValue("@OCDP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@OCDP_USR_A", item.Autoriza);
            sqlCommand.Parameters.AddWithValue("@OCDP_FEC_A", item.FechaAutoriza);
            sqlCommand.Parameters.AddWithValue("@OCDP_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@OCDP_USR_C", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@OCDP_CVCAN", item.ClaveCancela);
            sqlCommand.Parameters.AddWithValue("@OCDP_CLMTV", item.ClaveCancelacion);
            sqlCommand.Parameters.AddWithValue("@OCDP_CLNTA", item.NotaCancelacion);
            sqlCommand.Parameters.AddWithValue("@OCDP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@OCDP_USR_N", item.Creo);

            item.Folio = this.ExecuteScalar(sqlCommand);
            if (item.Folio > 0)
                return item.Folio;
            return 0;
        }

        public int Update(ReqCompraModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OCDP SET OCDP_VER = @OCDP_VER, OCDP_STTS_ID = @OCDP_STTS_ID, OCDP_DRCTR_ID = @OCDP_DRCTR_ID, OCDP_CTDP_ID = @OCDP_CTDP_ID, OCDP_PDCL_ID = @OCDP_PDCL_ID, OCDP_TIPO_ID = @OCDP_TIPO_ID, OCDP_NOMR = @OCDP_NOMR, OCDP_CNTC = @OCDP_CNTC, OCDP_NOTA = @OCDP_NOTA, OCDP_CMAIL = @OCDP_CMAIL, OCDP_FECACO = @OCDP_FECACO, OCDP_TEL = @OCDP_TEL, OCDP_SBTTL = @OCDP_SBTTL, OCDP_DESC = @OCDP_DESC, OCDP_TOTAL = @OCDP_TOTAL, OCDP_USR_A = @OCDP_USR_A, OCDP_FEC_A = @OCDP_FEC_A, OCDP_FCCNCL = @OCDP_FCCNCL, OCDP_USR_C = @OCDP_USR_C, OCDP_CVCAN = @OCDP_CVCAN, OCDP_CLMTV = @OCDP_CLMTV, OCDP_CLNTA = @OCDP_CLNTA, OCDP_FM = @OCDP_FM, OCDP_USR_M = @OCDP_USR_M WHERE (OCDP_ID = @OCDP_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@OCDP_ID", item.Folio);
            sqlCommand.Parameters.AddWithValue("@OCDP_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@OCDP_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@OCDP_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@OCDP_CTDP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@OCDP_PDCL_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@OCDP_TIPO_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@OCDP_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@OCDP_CNTC", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@OCDP_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@OCDP_CMAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@OCDP_FECACO", item.FechaAcuerdo);
            sqlCommand.Parameters.AddWithValue("@OCDP_TEL", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@OCDP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@OCDP_DESC", item.TotalDescuento);
            sqlCommand.Parameters.AddWithValue("@OCDP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@OCDP_USR_A", item.Autoriza);
            sqlCommand.Parameters.AddWithValue("@OCDP_FEC_A", item.FechaAutoriza);
            sqlCommand.Parameters.AddWithValue("@OCDP_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@OCDP_USR_C", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@OCDP_CVCAN", item.ClaveCancela);
            sqlCommand.Parameters.AddWithValue("@OCDP_CLMTV", item.ClaveCancelacion);
            sqlCommand.Parameters.AddWithValue("@OCDP_CLNTA", item.NotaCancelacion);
            sqlCommand.Parameters.AddWithValue("@OCDP_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@OCDP_USR_M", item.Modifica);

            return this.ExecuteTransaction(sqlCommand);
        }

        public ReqCompraDetailModel Save(ReqCompraDetailModel model) {
            if (model.Folio == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.Folio = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            return model;
        }
    }
}
