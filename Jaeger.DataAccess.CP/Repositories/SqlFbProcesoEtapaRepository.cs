﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.CP.Repositories {
    /// <summary>
    /// interface del repositorio para procedimientos en los procesos
    /// </summary>
    public class SqlFbProcesoEtapaRepository : RepositoryMaster<ProcesoEtapaModel>, ISqlProcesoEtapaRepository {
        protected ISqlProcesoProcedimientoRepository procedimientoRepository;
        public SqlFbProcesoEtapaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
            this.procedimientoRepository = new SqlFbProcesoProcedimientoRepository(configuracion);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp4 SET edp4_a = 0 WHERE edp4_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ProcesoEtapaModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select * from edp4 where edp4_id=@id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoEtapaModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ProcesoEtapaModel> GetList() {
            var sqlCommand3 = new FbCommand {
                CommandText = @"select * from edp4 where edp4_a = 1"
            };
            var tabla3 = this.ExecuteReader(sqlCommand3);
            var mapper3 = new DataNamesMapper<ProcesoEtapaModel>();
            var etapas = mapper3.Map(tabla3).ToList();
            return etapas;
        }

        /// <summary>
        /// estapas de proceso
        /// </summary>
        /// <param name="index">si el indice es 0 devuelve toda la lista</param>
        /// <returns></returns>
        public IEnumerable<ProcesoEtapaDetailModel> GetList(int index) {
            var sqlCommand3 = new FbCommand {
                CommandText = @"select * from edp4 where edp4_a > 0 order by EDP4_Sec"
            };

            if (index > 0)
                sqlCommand3.CommandText = @"select * from edp4 where edp4_a > 0 and EDP4_EDP3_ID = @index order by EDP4_Sec";
            sqlCommand3.Parameters.AddWithValue("@index", index);
            var tabla3 = this.ExecuteReader(sqlCommand3);
            var mapper3 = new DataNamesMapper<ProcesoEtapaDetailModel>();
            var etapas = mapper3.Map(tabla3).ToList();
            return etapas;
        }

        /// <summary>
        /// obtener listado de etapas de proceso por indice EDP4_EDP3_ID
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        /// <param name="index">array de indices EDP4_EDP3_ID</param>
        /// <returns></returns>
        public IEnumerable<ProcesoEtapaDetailModel> GetList(bool onlyActive, int[] index) {
            var sqlCommand = new FbCommand {
                CommandText = @"select * from EDP4 where EDP4_A > 0 and EDP4_EDP3_ID in (@index) order by EDP4_sec"
            };

            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@index", string.Join(",", index.ToArray()));
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ProcesoEtapaDetailModel>();
            var result = mapper.Map(tabla).ToList();
            var allids = result.Select(it => it.IdEdp4).ToArray();
            var procedimiento = this.procedimientoRepository.GetList(true, allids);

            for (int i = 0; i < result.Count; i++) {
                result[i].Procedimiento = new BindingList<ProcesoProcedimientoModel>(procedimiento.Where(it => it.PRO3_PRO2_ID == result[i].IdEdp4).ToList());
            }

            return result;
        }

        public int Insert(ProcesoEtapaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO edp4 (edp4_id, edp4_a, edp4_nom, edp4_tie_est, edp4_sec, edp4_uni, edp4_tie_can, edp4_can, edp4_mes, edp4_dia, edp4_edp3_id, edp4_mul, edp4_usu_fm, edp4_fm, edp4_pue_id) 
                			VALUES (@edp4_id, @edp4_a, @edp4_nom, @edp4_tie_est, @edp4_sec, @edp4_uni, @edp4_tie_can, @edp4_can, @edp4_mes, @edp4_dia, @edp4_edp3_id, @edp4_mul, @edp4_usu_fm, @edp4_fm, @edp4_pue_id)"
            };
            item.IdEdp4 = this.Max("EDP4_ID");
            sqlCommand.Parameters.AddWithValue("@edp4_id", item.IdEdp4);
            sqlCommand.Parameters.AddWithValue("@edp4_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp4_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@edp4_tie_est", item.UnitarioTE);
            sqlCommand.Parameters.AddWithValue("@edp4_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@edp4_uni", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@edp4_tie_can", item.TiempoEstimado);
            sqlCommand.Parameters.AddWithValue("@edp4_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@edp4_mes", item.EDP4_MES);
            sqlCommand.Parameters.AddWithValue("@edp4_dia", item.Dia);
            sqlCommand.Parameters.AddWithValue("@edp4_edp3_id", item.IdEdp3);
            sqlCommand.Parameters.AddWithValue("@edp4_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@edp4_usu_fm", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@edp4_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@edp4_pue_id", item.IdPuesto);

            if (this.ExecuteTransaction(sqlCommand) > 0)
                return item.IdEdp4;
            return 0;
        }

        public int Update(ProcesoEtapaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE edp4 
                			SET edp4_a = @edp4_a, edp4_nom = @edp4_nom, edp4_tie_est = @edp4_tie_est, edp4_sec = @edp4_sec, edp4_uni = @edp4_uni, edp4_tie_can = @edp4_tie_can, edp4_can = @edp4_can, edp4_mes = @edp4_mes, edp4_dia = @edp4_dia, edp4_edp3_id = @edp4_edp3_id, edp4_mul = @edp4_mul, edp4_usu_fm = @edp4_usu_fm, edp4_fm = @edp4_fm, edp4_pue_id = @edp4_pue_id 
                			WHERE edp4_id = @edp4_id;"
            };

            sqlCommand.Parameters.AddWithValue("@edp4_id", item.IdEdp4);
            sqlCommand.Parameters.AddWithValue("@edp4_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@edp4_nom", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@edp4_tie_est", item.UnitarioTE);
            sqlCommand.Parameters.AddWithValue("@edp4_sec", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@edp4_uni", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@edp4_tie_can", item.TiempoEstimado);
            sqlCommand.Parameters.AddWithValue("@edp4_can", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@edp4_mes", item.EDP4_MES);
            sqlCommand.Parameters.AddWithValue("@edp4_dia", item.Dia);
            sqlCommand.Parameters.AddWithValue("@edp4_edp3_id", item.IdEdp3);
            sqlCommand.Parameters.AddWithValue("@edp4_mul", item.Repetir);
            sqlCommand.Parameters.AddWithValue("@edp4_usu_fm", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@edp4_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@edp4_pue_id", item.IdPuesto);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
