﻿using System.ComponentModel;

namespace Jaeger.Domain.CP.Empresa.ValueObjects {
    public enum ReqCompraStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Requerido")]
        Requerida = 1,
        [Description("Rechazado")]
        Rechazado = 2,
        [Description("Autorizado")]
        Autorizado = 3,
        [Description("Completado")]
        Completado = 4,
    }
}
