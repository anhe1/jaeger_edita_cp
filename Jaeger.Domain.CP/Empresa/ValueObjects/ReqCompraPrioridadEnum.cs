﻿using System.ComponentModel;

namespace Jaeger.Domain.CP.Empresa.ValueObjects {
    public enum ReqCompraPrioridadEnum {
        [Description("Normal")]
        Normal = 0,
        [Description("Urgente")]
        Urgente = 1,
    }
}
