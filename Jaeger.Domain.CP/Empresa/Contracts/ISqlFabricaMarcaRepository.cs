﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    /// <summary>
    /// repositorio del Catálogo Fábricas y Marcas
    /// </summary>
    public interface ISqlFabricaMarcaRepository : IGenericRepository<FabricaMarcaModel> {
    }
}
