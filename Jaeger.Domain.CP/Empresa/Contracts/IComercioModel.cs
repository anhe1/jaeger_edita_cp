﻿using System;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    public interface IComercioModel {
        bool Activo { get; set; }
        int AutoSol { get; set; }
        int BPS { get; set; }
        int Codigo { get; set; }
        string Creo { get; set; }
        string Descripcion { get; set; }
        DateTime? FechaModifica { get; set; }
        int IdCom { get; set; }
        int IdIdr { get; set; }
        int Idv { get; set; }
        int LC1 { get; set; }
        int LP1 { get; set; }
        string Modifica { get; set; }
        int MP { get; set; }
        int Nivel { get; set; }
        int Orden { get; set; }
        int Secuencia { get; set; }
        int TEE { get; set; }
        int Uti { get; set; }
    }
}
