﻿// vista de comercio tabla COM
using System;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    public interface IFabricaMarcaModel {
        bool Activo { get; set; }
        string Descripcion { get; set; }
        DateTime? FechaModifica { get; set; }
        int IdMarca { get; set; }
        string Modifica { get; set; }
    }
}