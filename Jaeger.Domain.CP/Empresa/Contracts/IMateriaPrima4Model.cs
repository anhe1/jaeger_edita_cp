﻿using System;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    /// <summary>
    /// clase marca / modelo (mp4)
    /// </summary>
    public interface IMateriaPrima4Model {
        bool Activo { get; set; }
        decimal Ancho { get; set; }
        int Bps { get; set; }
        decimal Calibre { get; set; }
        decimal Cantidad { get; set; }
        string Especificacion { get; set; }
        DateTime? FechaModifica { get; set; }
        string Foto { get; set; }
        int IdMarca { get; set; }

        /// <sumary>
        /// obtener o establecer (MP4_ID)
        /// </sumary>
        int IdModelo { get; set; }
        int IdUnidad { get; set; }
        decimal Largo { get; set; }
        decimal Maximo { get; set; }
        decimal Minimo { get; set; }
        string Modelo { get; set; }

        /// <sumary>
        /// obtener o establecer (MP4_COM_ID)
        /// </sumary>
        int Mp4ComId { get; set; }
        int Mp4Dia { get; set; }
        int Mp4Mes { get; set; }
        int Mp4Mp3Id { get; set; }
        string Mp4Prod { get; set; }
        int Mp4Sec { get; set; }
        decimal Mp4TieEst { get; set; }
        int Mp4UsuFm { get; set; }
        int Mul { get; set; }
        decimal Rendimiento { get; set; }
        decimal Reorden { get; set; }
        decimal Valor { get; set; }
    }
}
