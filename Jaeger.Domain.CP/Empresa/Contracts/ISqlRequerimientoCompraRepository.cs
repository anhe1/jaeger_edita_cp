﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    public interface ISqlRequerimientoCompraRepository : IGenericRepository<ReqCompraModel> {
        ReqCompraDetailModel Save(ReqCompraDetailModel model);
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
    }
}
