﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    /// <summary>
    /// Catálogo Unidades Almacén
    /// </summary>
    public interface ISqlUnidadRepository : IGenericRepository<UnidadModel> {
    }
}
