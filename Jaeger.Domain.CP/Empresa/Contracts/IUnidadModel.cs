﻿using System;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    /// <summary>
    /// Catálogo Unidades Almacén (tabla MPUNI)
    /// </summary>
    public interface IUnidadModel {
        bool Activo { get; set; }
        string Descripcion { get; set; }
        DateTime? FechaModifica { get; set; }
        int IdUnidad { get; set; }
        string Modifica { get; set; }
    }
}