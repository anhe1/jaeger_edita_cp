﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    /// <summary>
    /// repositorio de productos y servicios (Materia Prima)
    /// </summary>
    public interface ISqlProductosServiciosRepository : IGenericRepository<MateriaPrima4Model> {
        /// <summary>
        /// obtener listado de modelos de materia prima por indices de relacion a productos (MP4_COM_ID)
        /// </summary>
        /// <param name="indexs">MP4_COM_ID</param>
        IEnumerable<MateriaPrima4DetailModel> GetList(int[] indexs);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="onlyActive"></param>
        /// <returns></returns>
        IEnumerable<MarcaModeloModelView> GetList(bool onlyActive);
    }
}
