﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    public interface ISqlReqCompraPartidaRepository : IGenericRepository<ReqCompraPartidaModel> {
        ReqCompraPartidaDetailModel Save(ReqCompraPartidaDetailModel model);
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
    }
}
