﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;
using Jaeger.Domain.CP.Presupuesto.Entities;

namespace Jaeger.Domain.CP.Empresa.Contracts {
    public interface ISqlComercioRepository : IGenericRepository<ComercioModel> {
        /// <summary>
        /// Lista simple de productos de cotizaciones
        /// </summary>
        IEnumerable<ComercioModel> GetListProductos();

        /// <summary>
        /// obtener claisficacion de productos, bienes y servicios con todos lo desendientes
        /// </summary>
        /// <param name="nivel">el nivel es 1</param>
        IEnumerable<ComercioDetailModel> GetList(int nivel);

        /// <summary>
        /// obtener vista COM3
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<Comercio3ModelView> GetList(bool onlyActive);

        /// <summary>
        /// vista de clasificacion, productos y servicios
        /// </summary>
        IEnumerable<Comercio3DetailModelView> GetList(bool onlyActive, string search = null);

        /// <summary>
        /// obtener listado de sub productos (2191)
        /// </summary>
        /// <param name="comIDR">21738</param>
        /// <param name="lc1">1</param>
        /// <returns></returns>
        IEnumerable<SubProductoDetailModel> GetList(int comIDR = 21738, int lc1 = 1);

        /// <summary>
        /// lista de productos de venta
        /// </summary>
        /// <param name="comLP1">com_lp1 = 1</param>
        /// <param name="all">false</param>
        IEnumerable<ComProductoModelView> GetList(int comLP1 = 1, bool all = false);

        /// <summary>
        /// obtener Lista Linea Componentes (vista LC1)
        /// </summary>
        /// <returns></returns>
        IEnumerable<ComercioModel> GetList(int comLC1, bool onlyActive, bool componente);
    }
}
