﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Empresa.Entities {
    public class ReqCompraPartidaDetailModel : ReqCompraPartidaModel {
        public ReqCompraPartidaDetailModel() : base() {

        }

        [DataNames("OCDPP_CANT")]
        public new decimal Cantidad {
            get { return base.Cantidad; } 
            set { base.Cantidad = value;
                this.Unitario = this.Unitario;
                this.OnPropertyChanged();
            } 
        }

        [DataNames("OCDPP_UNT")]
        public new decimal Unitario {
            get { return base.Unitario; }
            set { base.Unitario = value;
                base.SubTotal = this.Cantidad * this.Unitario;
                this.OnPropertyChanged();
            }
        }

        public decimal Importe {
            get { return (base.SubTotal - base.Descuento); }
        }

        public new object Clone() {
            return this.MemberwiseClone();
        }
    }
}
