﻿namespace Jaeger.Domain.CP.Empresa.Entities {
    public class ReqCompraPrioridadModel : Base.Abstractions.BaseSingleModel {
        public ReqCompraPrioridadModel() { }

        public ReqCompraPrioridadModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
