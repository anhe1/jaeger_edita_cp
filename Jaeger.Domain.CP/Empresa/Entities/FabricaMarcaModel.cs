﻿// vista de comercio tabla COM
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Empresa.Contracts;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// Catálogo Fábricas y Marcas
    /// </summary>
    [SugarTable("MPMAR", "Catalogo Fabricas y Marcas")]
    public class FabricaMarcaModel : BasePropertyChangeImplementation, IFabricaMarcaModel {
        private int idMarca;
        private bool activo;
        private string descripcion;
        private string usuarioModifica;
        private DateTime? fechaModifica;

        public FabricaMarcaModel() {
            fechaModifica = DateTime.Now;
            activo = true;
        }

        /// <summary>
        /// obtener o establecer el indice de la marca o fabricante
        /// </summary>
        [DataNames("MPMAR_ID")]
        [SugarColumn(ColumnName = "MPMAR_ID", ColumnDescription = "indice de la tabla", IsNullable = false, IsIdentity = true, IsPrimaryKey = true)]
        public int IdMarca {
            get { return idMarca; }
            set {
                idMarca = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("MPMAR_A")]
        [SugarColumn(ColumnName = "MPMAR_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la marca o fabricante (MPMAR_NOM)
        /// </summary>
        [DataNames("MPMAR_NOM")]
        [SugarColumn(ColumnName = "MPMAR_NOM", ColumnDescription = "descripcion de la marca o fabricante", Length = 50)]
        public string Descripcion {
            get { return descripcion; }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el usuario que modifica el regitro
        /// </summary>
        [DataNames("MPMAR_USU_FM")]
        [SugarColumn(ColumnName = "MPMAR_USU_FM", ColumnDescription = "clave del ultimo usuario que modifica el registro")]
        public string Modifica {
            get { return usuarioModifica; }
            set {
                usuarioModifica = value;
                OnPropertyChanged();
            }

        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("MPMAR_FM")]
        [SugarColumn(ColumnName = "MPMAR_FM", ColumnDescription = "ultima fecha de modificacion del registro", DefaultValue = "CURRENT_TIMESTAMP", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
