﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.CP.Empresa.ValueObjects;

namespace Jaeger.Domain.CP.Empresa.Entities {
    public class ReqCompraDetailModel : ReqCompraModel {
        private BindingList<ReqCompraPartidaDetailModel> _Conceptos;
        public ReqCompraDetailModel() : base() {
            this._Conceptos = new BindingList<ReqCompraPartidaDetailModel>() { RaiseListChangedEvents = true };
            this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
            this.FechaAcuerdo = this.FechaNuevo.AddDays(3);
        }

        public ReqCompraStatusEnum Status {
            get { return (ReqCompraStatusEnum)this.IdStatus; }
            set { this.IdStatus = (int)value;
                this.OnPropertyChanged();
            } 
        }

        public string StatusText {
            get { return EnumerationExtension.GetEnumDescription((ReqCompraStatusEnum)this.IdStatus); }
        }

        public string PrioridadText {
            get { return EnumerationExtension.GetEnumDescription((ReqCompraPrioridadEnum)this.IdTipo); }
        }

        public bool IsEditable {
            get { return this.Folio == 0; }
        }

        public BindingList<ReqCompraPartidaDetailModel> Conceptos {
            get { return this._Conceptos; }
            set {
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this._Conceptos = value;
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.Conceptos.Where((ReqCompraPartidaDetailModel p) => p.Activo == true).Sum((ReqCompraPartidaDetailModel p) => p.SubTotal);
            this.TotalDescuento = this.Conceptos.Where((ReqCompraPartidaDetailModel p) => p.Activo == true).Sum((ReqCompraPartidaDetailModel p) => p.Descuento);
            this.Total = (this.SubTotal - this.TotalDescuento);
        }
    }
}
