﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Empresa.Contracts;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// clase marca / modelo (mp4)
    /// </summary>
    [SugarTable("MP4", "Clase marca /modelo")]
    public class MateriaPrima4Model : BasePropertyChangeImplementation, IMateriaPrima4Model {
        private int idModelo;
        private bool mp4a;
        private string mp4mod;
        private string especificacion;
        private decimal mp4tieest;
        private int mp4sec;
        private int mp4mes;
        private int mp4dia;
        private decimal largo;
        private decimal ancho;
        private decimal calibre;
        private decimal mp4val;
        private decimal cantidad;
        private decimal rendimiento;
        private int idMarca;
        private int mp4mp3id;
        private decimal mp4min;
        private decimal reorden;
        private decimal mp4medx;
        private string mp4fot;
        private int mp4usufm;
        private DateTime? fechaModifica;
        private int mp4comid;
        private int mp4mul;
        private int mp4uniid;
        private int bps;
        private string mp4prod;

        public MateriaPrima4Model() {

        }

        /// <sumary>
        /// obtener o establecer indice de la tabla (MP4_ID)
        /// </sumary>
        [DataNames("MP4_ID")]
        [SugarColumn(ColumnName = "MP4_ID", ColumnDescription = "IdModel", IsPrimaryKey = true, IsIdentity = true)]
        public int IdModelo {
            get { return idModelo; }
            set {
                idModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_A)
        /// </sumary>
        [DataNames("MP4_A")]
        [SugarColumn(ColumnName = "MP4_A", ColumnDescription = "Activo")]
        public bool Activo {
            get { return mp4a; }
            set {
                mp4a = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_MOD)
        /// </sumary>
        [DataNames("MP4_MOD")]
        [SugarColumn(ColumnName = "MP4_MOD", ColumnDescription = "Modelo", Length = 50)]
        public string Modelo {
            get { return mp4mod; }
            set {
                mp4mod = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_NOM)
        /// </sumary>
        [DataNames("MP4_NOM")]
        [SugarColumn(ColumnName = "MP4_NOM", ColumnDescription = "Especificacion", Length = 255)]
        public string Especificacion {
            get { return especificacion; }
            set {
                especificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_TIE_EST)
        /// </sumary>
        [DataNames("MP4_TIE_EST")]
        [SugarColumn(ColumnName = "MP4_TIE_EST", ColumnDescription = "Mp4TieEst", Length = 18, DecimalDigits = 4)]
        public decimal Mp4TieEst {
            get { return mp4tieest; }
            set {
                mp4tieest = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_SEC)
        /// </sumary>
        [DataNames("MP4_SEC")]
        [SugarColumn(ColumnName = "MP4_SEC", ColumnDescription = "Mp4Sec")]
        public int Mp4Sec {
            get { return mp4sec; }
            set {
                mp4sec = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_MES)
        /// </sumary>
        [DataNames("MP4_MES")]
        [SugarColumn(ColumnName = "MP4_MES", ColumnDescription = "Mp4Mes")]
        public int Mp4Mes {
            get { return mp4mes; }
            set {
                mp4mes = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_DIA)
        /// </sumary>
        [DataNames("MP4_DIA")]
        [SugarColumn(ColumnName = "MP4_DIA", ColumnDescription = "Mp4Dia")]
        public int Mp4Dia {
            get { return mp4dia; }
            set {
                mp4dia = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_TCA)
        /// </sumary>
        [DataNames("MP4_TCA")]
        [SugarColumn(ColumnName = "MP4_TCA", ColumnDescription = "Largo", Length = 18, DecimalDigits = 4)]
        public decimal Largo {
            get { return largo; }
            set {
                largo = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_TCB)
        /// </sumary>
        [DataNames("MP4_TCB")]
        [SugarColumn(ColumnName = "MP4_TCB", ColumnDescription = "Ancho", Length = 18, DecimalDigits = 4)]
        public decimal Ancho {
            get { return ancho; }
            set {
                ancho = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_TCC)
        /// </sumary>
        [DataNames("MP4_TCC")]
        [SugarColumn(ColumnName = "MP4_TCC", ColumnDescription = "Calibre", Length = 18, DecimalDigits = 4)]
        public decimal Calibre {
            get { return calibre; }
            set {
                calibre = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_VAL)
        /// </sumary>
        [DataNames("MP4_VAL")]
        [SugarColumn(ColumnName = "MP4_VAL", ColumnDescription = "Valor", Length = 18, DecimalDigits = 4)]
        public decimal Valor {
            get { return mp4val; }
            set {
                mp4val = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_CAN)
        /// </sumary>
        [DataNames("MP4_CAN")]
        [SugarColumn(ColumnName = "MP4_CAN", ColumnDescription = "Cantidad", Length = 18, DecimalDigits = 4)]
        public decimal Cantidad {
            get { return cantidad; }
            set {
                cantidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_REN)
        /// </sumary>
        [DataNames("MP4_REN")]
        [SugarColumn(ColumnName = "MP4_REN", ColumnDescription = "Rendimiento", Length = 18, DecimalDigits = 4)]
        public decimal Rendimiento {
            get { return rendimiento; }
            set {
                rendimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_MPMAR_ID)
        /// </sumary>
        [DataNames("MP4_MPMAR_ID")]
        [SugarColumn(ColumnName = "MP4_MPMAR_ID", ColumnDescription = "IdMarca")]
        public int IdMarca {
            get { return idMarca; }
            set {
                idMarca = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_MP3_ID)
        /// </sumary>
        [DataNames("MP4_MP3_ID")]
        [SugarColumn(ColumnName = "MP4_MP3_ID", ColumnDescription = "Mp4Mp3Id")]
        public int Mp4Mp3Id {
            get { return mp4mp3id; }
            set {
                mp4mp3id = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_MIN) 
        /// </sumary>
        [DataNames("MP4_MIN")]
        [SugarColumn(ColumnName = "MP4_MIN", ColumnDescription = "Mp4Min", Length = 18, DecimalDigits = 4)]
        public decimal Minimo {
            get { return mp4min; }
            set {
                mp4min = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_MED)
        /// </sumary>
        [DataNames("MP4_MED")]
        [SugarColumn(ColumnName = "MP4_MED", ColumnDescription = "Reorden", Length = 18, DecimalDigits = 4)]
        public decimal Reorden {
            get { return reorden; }
            set {
                reorden = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_MEDX)
        /// </sumary>
        [DataNames("MP4_MAX")]
        [SugarColumn(ColumnName = "MP4_MAX", ColumnDescription = "Mp4Medx", Length = 18, DecimalDigits = 4)]
        public decimal Maximo {
            get { return mp4medx; }
            set {
                mp4medx = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_FOT)
        /// </sumary>
        [DataNames("MP4_FOT")]
        [SugarColumn(ColumnName = "MP4_FOT", ColumnDescription = "Mp4Fot", Length = 50)]
        public string Foto {
            get { return mp4fot; }
            set {
                mp4fot = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_USU_FM)
        /// </sumary>
        [DataNames("MP4_USU_FM")]
        [SugarColumn(ColumnName = "MP4_USU_FM", ColumnDescription = "Mp4UsuFm")]
        public int Mp4UsuFm {
            get { return mp4usufm; }
            set {
                mp4usufm = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_FM)
        /// </sumary>
        [DataNames("MP4_FM")]
        [SugarColumn(ColumnName = "MP4_FM", ColumnDescription = "Mp4Fm")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_COM_ID)
        /// </sumary>
        [DataNames("MP4_COM_ID")]
        [SugarColumn(ColumnName = "MP4_COM_ID", ColumnDescription = "Mp4ComId")]
        public int Mp4ComId {
            get { return mp4comid; }
            set {
                mp4comid = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_MUL)
        /// </sumary>
        [DataNames("MP4_MUL")]
        [SugarColumn(ColumnName = "MP4_MUL", ColumnDescription = "")]
        public int Mul {
            get { return mp4mul; }
            set {
                mp4mul = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_UNI_ID)
        /// </sumary>
        [DataNames("MP4_UNI_ID")]
        [SugarColumn(ColumnName = "MP4_UNI_ID", ColumnDescription = "IdUnidad")]
        public int IdUnidad {
            get { return mp4uniid; }
            set {
                mp4uniid = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (BPS)
        /// </sumary>
        [DataNames("BPS")]
        [SugarColumn(ColumnName = "BPS", ColumnDescription = "Bps")]
        public int Bps {
            get { return bps; }
            set {
                bps = value;
                this.OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (MP4_PROD)
        /// </sumary>
        [DataNames("MP4_PROD")]
        [SugarColumn(ColumnName = "MP4_PROD", ColumnDescription = "Mp4Prod", Length = 50)]
        public string Mp4Prod {
            get { return mp4prod; }
            set {
                mp4prod = value;
                this.OnPropertyChanged();
            }
        }
    }
}
