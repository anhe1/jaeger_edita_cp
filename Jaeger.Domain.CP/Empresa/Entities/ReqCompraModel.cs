﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Empresa.Entities {
    public class ReqCompraModel : BasePropertyChangeImplementation, Base.Contracts.IEntityBase, Base.Contracts.ICancelacion {
        #region declaraciones
        private int _IdPedido;
        private int _Folio;
        private string _Version;
        private int _IdStatus;
        private int _IdCliente;
        private int _IdDepartamento;
        private int _IdTipo;
        private string _Receptor;
        private string _Contacto;
        private string _Onservaciones;
        private string _Correo;
        private DateTime? _FechaAcuerdo;
        private string _Telefono;
        private decimal _Total;
        private string _Autoriza;
        private DateTime? _FechaAutoriza;
        private DateTime _FechaNuevo;
        private string _Creo;
        private DateTime? _FechaModifica;
        private string _Modifica;
        private decimal _SubTotal;
        private decimal _Descuento;
        private string _Cancela;
        private string _NotaCancelacion;
        private DateTime? fechaCancela;
        private int _ClaveCancela;
        private string _MotivoCancelacion;
        private string _Serie;
        private string _Compras;
        private DateTime? _FechaCompras;
        private int _OrdenCompra;
        private string _NotaAdq;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ReqCompraModel() {
            this._FechaNuevo = DateTime.Now;
            this.IdStatus = 1;
        }

        #region datos generales del comprobante
        /// <summary>
        /// obtener o establecer indice o folio (OCDP_ID)
        /// </summary>
        [DataNames("OCDP_ID")]
        public int Folio {
            get { return _Folio; }
            set {
                _Folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_VER")]
        public string Version {
            get { return _Version; }
            set {
                this._Version = value;
                this.OnPropertyChanged();
            }
        }

        public string Serie {
            get { return this._Serie; }
            set { this._Serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer IdStatus
        /// </summary>
        [DataNames("OCDP_STTS_ID")]
        public int IdStatus {
            get { return this._IdStatus; }
            set {
                this._IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_DRCTR_ID")]
        public int IdCliente {
            get { return this._IdCliente; }
            set {
                this._IdCliente = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_CTDP_ID")]
        public int IdDepartamento {
            get { return this._IdDepartamento; }
            set {
                this._IdDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_PDCL_ID")]
        public int IdPedido {
            get { return this._IdPedido; }
            set {
                this._IdPedido = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_TIPO_ID")]
        public int IdTipo {
            get { return this._IdTipo; }
            set {
                this._IdTipo = value;
                this.OnPropertyChanged();
            }
        }
        [DataNames("OCDP_NOMR")]
        public string ReceptorNombre {
            get { return this._Receptor; }
            set {
                this._Receptor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_CNTC")]
        public string Contacto {
            get { return this._Contacto; }
            set {
                this._Contacto = value;
                this.OnPropertyChanged();
            }
        }
        [DataNames("OCDP_NOTA")]
        public string Nota {
            get { return this._Onservaciones; }
            set {
                this._Onservaciones = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_CMAIL")]
        public string Correo {
            get { return this._Correo; }
            set {
                this._Correo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_FECACO")]
        public DateTime? FechaAcuerdo {
            get { return this._FechaAcuerdo; }
            set { this._FechaAcuerdo = value; }
        }
        
        [DataNames("OCDP_TEL")]
        public string Telefono {
            get { return this._Telefono; }
            set {
                this._Telefono = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_SBTTL")]
        public decimal SubTotal {
            get { return this._SubTotal; }
            set { this._SubTotal = value; }
        }

        [DataNames("OCDP_DESC")]
        public decimal TotalDescuento {
            get { return this._Descuento; }
            set { this._Descuento = value; }
        }

        [DataNames("OCDP_TOTAL")]
        public decimal Total {
            get { return this._Total; }
            set {
                this._Total = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region autorizacion
        [DataNames("OCDP_USR_A")]
        public string Autoriza {
            get { return this._Autoriza; }
            set {
                this._Autoriza = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("OCDP_FEC_A")]
        public DateTime? FechaAutoriza {
            get {
                if (this._FechaAutoriza >= new DateTime(1900, 1, 1))
                    return this._FechaAutoriza;
                return null;
            }
            set {
                this._FechaAutoriza = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region aceptacion compras
        /// <summary>
        /// obtener o establecer clave de usuario de adquisiciones
        /// </summary>
        [DataNames("OCDP_USR_ADQ")]
        public string Adquisiciones {
            get { return this._Compras; }
            set {
                this._Compras = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de orden de compra
        /// </summary>
        [DataNames("OCDP_ORDCOM")]
        public int OrdenCompra {
            get { return this._OrdenCompra; }
            set { this._OrdenCompra = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de recepcion de compras
        /// </summary>
        [DataNames("OCDP_FECADQ")]
        public DateTime? FechaCompras {
            get {
                if (this._FechaCompras >= new DateTime(1900, 1, 1))
                    return this._FechaCompras;
                return null;
            }
            set {
                this._FechaCompras = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota del departamento de adquisiciones
        /// </summary>
        [DataNames("OCDP_ADQNOT")]
        public string NotaAdq {
            get { return this._NotaAdq; }
            set { this._NotaAdq = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region cancelacion
        [DataNames("OCDP_FCCNCL")]
        public DateTime? FechaCancela {
            get {
                if (this.fechaCancela >= new DateTime(1900, 1, 1))
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_USR_C")]
        public string Cancela {
            get { return this._Cancela; }
            set {
                this._Cancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// OCDP_CVCAN
        /// </summary>
        [DataNames("OCDP_CVCAN")]
        public int ClaveCancela {
            get { return this._ClaveCancela; }
            set {
                this._ClaveCancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_CLMTV")]
        public string ClaveCancelacion {
            get => this._MotivoCancelacion;
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_CLNTA")]
        public string NotaCancelacion {
            get { return this._NotaCancelacion; }
            set {
                this._NotaCancelacion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region creacion
        [DataNames("OCDP_FN")]
        public DateTime FechaNuevo {
            get { return this._FechaNuevo; }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_USR_N")]
        public string Creo {
            get { return this._Creo; }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._FechaModifica >= new DateTime(1900, 1, 1))
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_USR_M")]
        public string Modifica {
            get { return this._Modifica; }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        /// <summary>
        /// obtener o establecer objeto de cancelacion
        /// </summary>
        public ReqCompraCancelacionModel Cancelacion {
            get {
                return new ReqCompraCancelacionModel {
                    Folio = this.Folio,
                    Cancela = this.Cancela,
                    ClaveCancela = this.ClaveCancela,
                    ClaveCancelacion = this.ClaveCancelacion,
                    FechaCancela = this.FechaCancela,
                    IdStatus = this.IdStatus,
                    NotaCancelacion = this.NotaCancelacion
                };
            }
            set {
                if (value != null) {
                    Cancela = value.Cancela;
                    ClaveCancela = value.ClaveCancela;
                    ClaveCancelacion = value.ClaveCancelacion;
                    FechaCancela = value.FechaCancela;
                    IdStatus = value.IdStatus;
                    NotaCancelacion = value.NotaCancelacion;
                }
            }
        }
    }
}
