﻿using System.ComponentModel;
using Jaeger.Domain.CP.Empresa.Contracts;
using SqlSugar;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// Comercio, incluye: Productos, Bienes y Servicios, modelos etc. tabla (COM)
    /// </summary>
    [SugarTable("com", "Comercio, incluye: Productos, Bienes y Servicios, modelos etc. tabla (COM)")]
    public class ComercioDetailModel : ComercioModel, IComercioModel {

        private BindingList<ComercioDetailModel> subListaComercio;

        public ComercioDetailModel() {
            this.subListaComercio = new BindingList<ComercioDetailModel>();
        }

        /// <summary>
        /// obtener o establecer el siguiente nivel de la clasificacion de productos, bienes y Servicios
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ComercioDetailModel> Lista {
            get { return this.subListaComercio; }
            set {
                this.subListaComercio = value;
                this.OnPropertyChanged();
            }
        }
    }
}
