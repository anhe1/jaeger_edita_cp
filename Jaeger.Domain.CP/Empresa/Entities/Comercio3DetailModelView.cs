﻿using System.ComponentModel;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// vista de productos y servicios (COM3 y MP4)
    /// </summary>
    public class Comercio3DetailModelView : Comercio3ModelView {
        private BindingList<MateriaPrima4DetailModel> modelos;

        /// <summary>
        /// constructor
        /// </summary>
        public Comercio3DetailModelView() {
            this.modelos = new BindingList<MateriaPrima4DetailModel>();
        }

        /// <summary>
        /// listado de modelos
        /// </summary>
        public BindingList<MateriaPrima4DetailModel> Modelos {
            get { return this.modelos; }
            set {
                this.modelos = value;
            }
        }
    }
}
