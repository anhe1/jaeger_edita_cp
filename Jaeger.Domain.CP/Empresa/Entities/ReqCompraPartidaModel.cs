﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Empresa.Entities {
    public class ReqCompraPartidaModel : BasePropertyChangeImplementation, Base.Contracts.IEntityBase {
        #region declaraciones
        private int _IdConcepto;
        private bool _Activo;
        private int _Folio;
        private int _IdProducto;
        private int _IdModelo;
        private int _IdDepartamento;
        private decimal _Cantidad;
        private string _Unidad;
        private string _Descripcion;
        private string _Marca;
        private string _Especificacion;
        private string _Nota;
        private decimal _Unitario;
        private decimal _SubTotal;
        private decimal _Descuento;
        private string _ImagenURL;
        private string _Creo;
        private DateTime _FechaNuevo;
        private string _Modifica;
        private DateTime? _FechaModifica;
        private int _NumOrden;
        private int _IdPedidoC;
        #endregion

        public ReqCompraPartidaModel() {
            this.FechaNuevo = DateTime.Now;
            this.Activo = true;
        }

        [DataNames("OCDPP_ID")]
        public int IdConcepto {
            get { return this._IdConcepto; }
            set {
                this._IdConcepto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_A")]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_OCDP_ID")]
        public int Folio {
            get { return this._Folio; }
            set {
                this._Folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_CTPRD_ID")]
        public int IdProducto {
            get { return this._IdProducto; }
            set {
                this._IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_CTMDL_ID")]
        public int IdModelo {
            get { return this._IdModelo; }
            set {
                this._IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_CTDP_ID")]
        public int IdDepartamento {
            get { return this._IdDepartamento; }
            set {
                this._IdDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de orden de produccion
        /// </summary>
        [DataNames("OCDPP_PEDPRD")]
        public int IdPedPrd {
            get { return this._NumOrden; }
            set { this._NumOrden = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del numero de pedido del cliente
        /// </summary>
        [DataNames("OCDPP_PDCL_ID")]
        public int IdPedidoC {
            get { return this._IdPedidoC; }
            set { this._IdPedidoC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_CANT")]
        public decimal Cantidad {
            get { return this._Cantidad; }
            set {
                this._Cantidad = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_UNDN")]
        public string Unidad {
            get { return this._Unidad; }
            set {
                this._Unidad = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_NOM")]
        public string Descripcion {
            get { return this._Descripcion; }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_MRC")]
        public string Marca {
            get { return this._Marca; }
            set {
                this._Marca = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_ESPC")]
        public string Especificacion {
            get { return this._Especificacion; }
            set {
                this._Especificacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_NOTA")]
        public string Nota {
            get { return this._Nota; }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_UNT")]
        public decimal Unitario {
            get { return this._Unitario; }
            set {
                this._Unitario = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_SBTTL")]
        public decimal SubTotal {
            get { return this._SubTotal; }
            set {
                this._SubTotal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_DESC")]
        public decimal Descuento {
            get { return this._Descuento; }
            set {
                this._Descuento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_IMG_URL")]
        public string ImagenURL {
            get { return this._ImagenURL; }
            set {
                this._ImagenURL = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_USR_N")]
        public string Creo {
            get { return this._Creo; }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_FN")]
        public DateTime FechaNuevo {
            get { return this._FechaNuevo; }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_USR_M")]
        public string Modifica {
            get { return this._Modifica; }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDPP_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._FechaModifica >= new DateTime(1900, 1, 1))
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
