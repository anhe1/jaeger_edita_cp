﻿using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.CP.Empresa.Entities {
    public class ReqCompraPrinter : ReqCompraDetailModel {
        public ReqCompraPrinter() : base() { 
        }
        public ReqCompraPrinter(ReqCompraDetailModel source) :base() {
            MapperClassExtensions.MatchAndMap<ReqCompraDetailModel, ReqCompraDetailModel>(source, this);
            this.Conceptos = source.Conceptos;
        }

        public string Departamento {
            get; set;
        }

        public string[] QrText {
            get {
                return new string[] { "&identificador=", this.Folio.ToString("000000"),
                    (this.Folio.ToString("000000") == null ? "" : "&folio=" + this.Folio.ToString()),
                    "&em=", this.IdDepartamento.ToString("00"), "&rec=", this.ReceptorNombre, "&fecha=", this.FechaNuevo.ToShortDateString(), "&total=", this.Total.ToString("0.00") };
            }
        }
    }
}
