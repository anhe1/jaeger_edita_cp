﻿// vista de comercio tabla COM
// CREATE VIEW COM3 (COM3_ID, COM3_A, COM3_COD, COM3_N1, COM3_N2, COM3_N3, COM3_FM, COM3_USU_N, COM3_USU_M) AS 
// Select C3.Com_ID, C3.Com_A* C2.Com_A* C1.Com_A, C3.Com_Cod,
// Case
//     When (C1.Com_Nom = '' And C2.Com_Nom = '') then C3.Com_Nom
//     When (C1.Com_Nom = '') then C2.Com_Nom
//     Else C1.Com_Nom
// end,
// Case
//    When C2.Com_Nom = '' then C3.Com_Nom
//     Else C2.Com_Nom
// end,
// C3.Com_Nom,
// C3.Com_FM,
// C3.Com_USU_N,
// C3.Com_USU_M
// From Com C1
// join Com C2 on C2.Com_IDR = C1.Com_ID
// join Com C3 on C3.Com_IDR = C2.Com_ID

using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// vista de productos (COM3)
    /// </summary>
    public class Comercio3ModelView {

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("com3_id", "Id")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com3_id")]
        public int IdCom { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("com3_cod", "Codigo")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com3_cod")]
        public int Codigo { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("com3_a", "Activo")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com3_a")]
        public bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("com3_n1", "Nivel1")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_n1")]
        public string Nivel1 { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("com3_n2", "Nivel2")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_n2")]
        public string Nivel2 { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("com3_n3", "Nivel3")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com3_n3")]
        public string Nivel3 { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("com3_usu_n", "Creo")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com3_usu_n", Length = 10)]
        public string Creo { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("com3_usu_m", "Modifica")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com3_usu_m", Length = 10)]
        public string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string CodigoHex {
            get {
                var cadena = this.Codigo.ToString("x").ToUpper();
                if (cadena.Length < 8)
                    cadena = cadena + "".PadLeft(8 - cadena.Length, '0');
                return string.Format("{0}:{1}:{2}:{3}", cadena.Substring(0, 2), cadena.Substring(2, 2), cadena.Substring(4, 2), cadena.Substring(6, 2));
            }
        }
    }
}
