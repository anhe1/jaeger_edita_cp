﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Empresa.Contracts;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// Catálogo Unidades Almacén
    /// </summary>
    [SugarTable("MPUNI", "catalogo de unidades del almacen")]
    public class UnidadModel : BasePropertyChangeImplementation, IUnidadModel {
        private int idUnidad;
        private bool activo;
        private string descripcion;
        private string usuarioModifica;
        private DateTime? fechaModifica;

        public UnidadModel() {
            this.fechaModifica = DateTime.Now;
            this.activo = true;
        }

        /// <summary>
        /// obtener o establecer el indice de la marca o fabricante
        /// </summary>
        [DataNames("MPUNI_ID")]
        [SugarColumn(ColumnName = "MPUNI_ID", ColumnDescription = "indice de la tabla", IsNullable = false, IsIdentity = true, IsPrimaryKey = true)]
        public int IdUnidad {
            get { return this.idUnidad; }
            set {
                this.idUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("MPUNI_A")]
        [SugarColumn(ColumnName = "MPUNI_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la marca o fabricante
        /// </summary>
        [DataNames("MPUNI_NOM")]
        [SugarColumn(ColumnName = "MPUNI_NOM", ColumnDescription = "descripcion de la marca o fabricante", Length = 50)]
        public string Descripcion {
            get { return this.descripcion; }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el usuario que modifica el regitro
        /// </summary>
        [DataNames("MPUNI_USU_FM")]
        [SugarColumn(ColumnName = "MPUNI_USU_FM", ColumnDescription = "clave del ultimo usuario que modifica el registro")]
        public string Modifica {
            get { return this.usuarioModifica; }
            set {
                this.usuarioModifica = value;
                this.OnPropertyChanged();
            }

        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("MPUNI_FM")]
        [SugarColumn(ColumnName = "MPUNI_FM", ColumnDescription = "ultima fecha de modificacion del registro", DefaultValue = "CURRENT_TIMESTAMP", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
