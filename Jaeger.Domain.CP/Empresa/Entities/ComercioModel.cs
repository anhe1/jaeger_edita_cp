﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Empresa.Contracts;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// Comercio, incluye: Productos, Bienes y Servicios, modelos etc. tabla (COM)
    /// </summary>
    [SugarTable("com", "Comercio, incluye: Productos, Bienes y Servicios")]
    public class ComercioModel : BasePropertyChangeImplementation, IComercioModel {
        private int index;
        private int ididr;
        private DateTime? fechaModifica;
        private int codigo;

        public ComercioModel() {

        }

        /// <summary>
        /// com_id
        /// </summary>
        [DataNames("com_id")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_id", IsPrimaryKey = true, IsIdentity = true)]
        public int IdCom {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// com_a
        /// </summary>
        [DataNames("com_a")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_a")]
        public bool Activo { get; set; }

        /// <summary>
        /// com_idr
        /// </summary>
        [DataNames("com_idr")]
        [SugarColumn(ColumnDescription = "parent", ColumnName = "com_idr")]
        public int IdIdr {
            get { return this.ididr; }
            set {
                this.ididr = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// com_cod
        /// </summary>
        [DataNames("com_cod")]
        [SugarColumn(ColumnDescription = "codigo hexadecimal", ColumnName = "com_cod")]
        public int Codigo {
            get { return this.codigo; }
            set {
                this.codigo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// com_niv
        /// </summary>
        [DataNames("com_niv")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_niv")]
        public int Nivel { get; set; }

        /// <summary>
        /// com_sec
        /// </summary>
        [DataNames("com_sec")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_sec")]
        public int Secuencia { get; set; }

        /// <summary>
        /// com_idv
        /// </summary>
        [DataNames("com_idv")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_idv")]
        public int Idv { get; set; }

        /// <summary>
        /// com_autosol
        /// </summary>
        [DataNames("com_autosol")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_autosol")]
        public int AutoSol { get; set; }

        /// <summary>
        /// com_lc1
        /// </summary>
        [DataNames("com_lc1")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_lc1")]
        public int LC1 { get; set; }

        /// <summary>
        /// com_mp
        /// </summary>
        [DataNames("com_mp")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_mp")]
        public int MP { get; set; }

        /// <summary>
        /// COM_ORD
        /// </summary>
        [DataNames("com_ord")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_ord")]
        public int Orden { get; set; }

        /// <summary>
        /// com_lp1
        /// </summary>
        [DataNames("com_lp1")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_lp1")]
        public int LP1 { get; set; }

        /// <summary>
        /// com_uti
        /// </summary>
        [DataNames("com_uti")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_uti")]
        public int Uti { get; set; }

        /// <summary>
        /// com_tee
        /// </summary>
        [DataNames("com_tee")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_tee")]
        public int TEE { get; set; }

        /// <summary>
        /// com_bps
        /// </summary>
        [DataNames("com_bps")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_bps")]
        public int BPS { get; set; }

        /// <summary>
        /// com_nom
        /// </summary>
        [DataNames("com_nom")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_nom", Length = 50)]
        public string Descripcion { get; set; }

        /// <summary>
        /// com_usu_n
        /// </summary>
        [DataNames("com_usu_n")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_usu_n", Length = 10)]
        public string Creo { get; set; }

        /// <summary>
        /// com_usu_m
        /// </summary>
        [DataNames("com_usu_m")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_usu_m", Length = 10)]
        public string Modifica { get; set; }

        /// <summary>
        /// com_fm
        /// </summary>
        [DataNames("com_fm")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_fm", IsNullable = true, IsIgnore = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener codigo hexadecimal
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string CodigoHex {
            get {
                var cadena = this.Codigo.ToString("x").ToUpper();
                if (cadena.Length < 8)
                    cadena = cadena + "".PadLeft(8 - cadena.Length, '0');
                return string.Format("{0}:{1}:{2}:{3}", cadena.Substring(0, 2), cadena.Substring(2, 2), cadena.Substring(4, 2), cadena.Substring(6, 2));
            }
        }
    }
}
