﻿using SqlSugar;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// clase marca / modelo (mp4)
    /// </summary>
    [SugarTable("MP4", "Clase marca /modelo")]
    public class MateriaPrima4DetailModel : MateriaPrima4Model {
        public MateriaPrima4DetailModel() {

        }

        public string NoIdentificacion {
            get {
                return "CP" + this.Mp4ComId.ToString("#00000") + "-" + this.IdModelo.ToString("#00000") + "-" + this.IdUnidad.ToString("#00");
            }
        }
    }
}
