﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Empresa.Entities {
    /// <summary>
    /// clase para vista de comerio + marca + unidad + modelo (COM3, MP4, MPMAR, MPPUNI)
    /// </summary>
    public class MarcaModeloModelView : MateriaPrima4Model {
        private string descripcion;
        private string unidad;

        #region comercio

        /// <summary>
        /// obtener o establecer indice de la vista de comercio (com3_id)
        /// </summary>
        [DataNames("Com3_id")]
        public int IdComercio { get; set; }

        /// <summary>
        /// obtener o establecer com3_n1
        /// </summary>
        [DataNames("com3_n1", "Nivel1")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_n1")]
        public string Nivel1 { get; set; }

        /// <summary>
        /// obtener o establecer com3_n2
        /// </summary>
        [DataNames("com3_n2", "Nivel2")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com_n2")]
        public string Nivel2 { get; set; }

        /// <summary>
        /// obtener o establecer com3_n3
        /// </summary>
        [DataNames("com3_n3", "Nivel3")]
        [SugarColumn(ColumnDescription = "", ColumnName = "com3_n3")]
        public string Nivel3 { get; set; }

        #endregion

        #region marca modelo 

        /// <summary>
        /// obtener o establecer la descripcion de la marca o fabricante (MPMAR_NOM)
        /// </summary>
        [DataNames("MPMAR_NOM")]
        [SugarColumn(ColumnName = "MPMAR_NOM", ColumnDescription = "descripcion de la marca o fabricante", Length = 50)]
        public string FabricanteModelo {
            get { return descripcion; }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descripcion de la marca o fabricante (MPUNI_NOM)
        /// </summary>
        [DataNames("MPUNI_NOM")]
        [SugarColumn(ColumnName = "MPUNI_NOM", ColumnDescription = "descripcion de la marca o fabricante", Length = 50)]
        public string Unidad {
            get { return this.unidad; }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        public string NoIdentificacion {
            get {
                return "CP" + this.IdComercio.ToString("#00000") + "-" + this.IdModelo.ToString("#00000") + "-" + this.IdUnidad.ToString("#00");
            }
        }
    }
}
