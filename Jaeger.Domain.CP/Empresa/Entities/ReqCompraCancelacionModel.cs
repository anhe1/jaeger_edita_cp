﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Empresa.Entities {
    public class ReqCompraCancelacionModel : BasePropertyChangeImplementation, ICancelacion {
        #region declaraciones
        private int _Folio;
        private int _IdStatus;
        private DateTime? fechaCancela;
        private string _Cancela;
        private int _ClaveCancela;
        private string _MotivoCancelacion;
        private string _NotaCancelacion;
#endregion

        public ReqCompraCancelacionModel() {

        }

        #region cancelacion

        [DataNames("OCDP_ID")]
        public int Folio {
            get { return _Folio; }
            set {
                _Folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_STTS_ID")]
        public int IdStatus {
            get { return this._IdStatus; }
            set {
                this._IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_FCCNCL")]
        public DateTime? FechaCancela {
            get {
                if (this.fechaCancela >= new DateTime(1900, 1, 1))
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_USR_C")]
        public string Cancela {
            get { return this._Cancela; }
            set {
                this._Cancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// OCDP_CVCAN
        /// </summary>
        [DataNames("OCDP_CVCAN")]
        public int ClaveCancela {
            get { return this._ClaveCancela; }
            set {
                this._ClaveCancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_CLMTV")]
        public string ClaveCancelacion {
            get => this._MotivoCancelacion;
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("OCDP_CLNTA")]
        public string NotaCancelacion {
            get { return this._NotaCancelacion; }
            set {
                this._NotaCancelacion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
