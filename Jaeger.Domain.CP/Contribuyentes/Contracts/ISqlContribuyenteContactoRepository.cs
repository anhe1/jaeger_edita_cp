﻿/// purpose: directorio de contactos del directorio
/// develop: ANHE1 28092020 11:14
using System.Collections.Generic;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    /// <summary>
    /// repositorio de contactos del contribuyente (tabla DIRCON)
    /// </summary>
    public interface ISqlContribuyenteContactoRepository : IGenericRepository<ContribuyenteContactoModel> {
        /// <summary>
        /// obtener listado de contacto relacionados a un indice (dircon_dir_id)
        /// </summary>
        /// <param name="index">indice de relacion del directorio (dircon_dir_id)</param>
        /// <param name="onlyActive">solo registros activos</param>
        //IEnumerable<ContribuyenteContactoDetailModel> GetList(int[] indexs, bool onlyActive);

        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
    }
}
