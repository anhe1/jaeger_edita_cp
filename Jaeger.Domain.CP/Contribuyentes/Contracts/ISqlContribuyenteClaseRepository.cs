﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    /// <summary>
    /// repositorio de clases del directorio
    /// </summary>
    public interface ISqlContribuyenteClaseRepository : IGenericRepository<ContribuyenteClaseModel> {
    }
}
