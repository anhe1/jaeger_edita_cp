﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    ///<summary>
    /// telefono del contacto  (DIRCONT)
    ///</summary>
    public interface IContribuyenteContactoTelefonoModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla (DIRCONT_ID)
        /// </summary>
        int IdDirConT { get; set; }

        /// <summary>
        /// obtener o establecer (DIRCONT_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o estalecer (DIRCONT_DIRCON_ID)
        /// </summary>
        int IdContacto { get; set; }

        /// <summary>
        /// obtener o establecer (DIRCONT_TEL_ID)
        /// </summary>
        int IdTelefonia { get; set; }

        /// <summary>
        /// obtener o establecer el numero telefonico (DIRCONT_NUM)
        /// </summary>
        string Numero { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro (DIRCONT_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}