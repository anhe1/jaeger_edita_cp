﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    /// <summary>
    /// repositorio de especialidades del directorio (tabla DIRESP)
    /// </summary>
    public interface ISqlContribuyenteEspecialidadRepository : IGenericRepository<ContribuyenteEspecialidadModel> {

    }
}
