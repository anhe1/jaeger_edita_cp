﻿using System;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    ///<summary>
    /// especialidad (tabla DIRESP)
    ///</summary>
    public interface IContribuyenteEspecialidadModel {
        /// <summary>
        /// obtener o establecer indice de la tabla (DIRESP_ID)
        /// </summary>
        int IdDirEsp { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (DIRESP_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de bienes productos y servicios (DIRESP_BPS)
        /// </summary>
        int DIRESP_BPS { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la especialidad (DIRESP_ESP)
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro (DIRESP_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}