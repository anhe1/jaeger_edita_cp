﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    /// <summary>
    /// Contacto del directorio (DIRCON)
    /// </summary>
    public interface IContribuyenteContactoModel {
        /// <summary>
        /// obtener o establecer indice de la tabla (DIRCON_ID)
        /// </summary>
        int IdContacto { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (DIRCON_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer (DIRCON_DIR_ID)
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer nombre del contacto (DIRCON_CON)
        /// </summary>
        string Contacto { get; set; }

        /// <summary>
        /// obtener o establecer departamento (DIRCON_DEP)
        /// </summary>
        string Departamento { get; set; }

        /// <summary>
        /// obtener o establecer puesto (DIRCON_PUE)
        /// </summary>
        string Puesto { get; set; }

        /// <summary>
        /// obtener o establecer observaciones (DIRCON_OBS)
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer fecha de modificacion (DIRCON_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}