﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System.Collections.Generic;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    /// <summary>
    /// repositorio de control del directorio (tabla DIR)
    /// </summary>
    public interface ISqlContribuyenteRepository : IGenericRepository<ContribuyenteModel> {

        //new ContribuyenteDetailModel GetById(int id);

        T1 GetById<T1>(int id) where T1 : class, new();

        ContribuyenteDetailModel Save(ContribuyenteDetailModel model);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
