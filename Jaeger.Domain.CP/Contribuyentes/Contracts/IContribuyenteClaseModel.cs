﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    /// <summary>
    /// clase de contribuyentes (tabla DIRCLA)
    /// </summary>
    public interface IContribuyenteClaseModel {
        /// <summary>
        /// obtener o establecer indice de la tabla (DIRCLA_ID)
        /// </summary>
        int IdClase { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del departamento (DIRCLA_DEP_ID)
        /// </summary>
        int IdDepto { get; set; }

        /// <summary>
        /// obtener o establecer DIRCLA_C
        /// </summary>
        string Clase { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la clase (DIRCLA_CLA)
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (DIRCLA_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}