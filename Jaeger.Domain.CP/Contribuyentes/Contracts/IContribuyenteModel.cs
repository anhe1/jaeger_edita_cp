﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    /// <summary>
    /// clase para el directorio de CP
    /// </summary>
    public interface IContribuyenteModel {
        /// <summary>
        /// obtener o establecer indice de la tabla (DIR_ID)
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (DIR_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer DIR_DIRCLA_ID
        /// </summary>
        int IdClase { get; set; }
        
        /// <summary>
        /// obtener o establecer id de especialidad (DIR_DIRESP_ID)
        /// </summary>
        int IdEspecialidad { get; set; }

        /// <summary>
        /// obtener o establecer DIR_AUT
        /// </summary>
        int DIR_AUT { get; set; }

        /// <summary>
        /// obtener o establecer DIR_DEP
        /// </summary>
        int DIR_DEP { get; set; }

        /// <summary>
        /// obtener o establecer DIR_PUE
        /// </summary>
        int DIR_PUE { get; set; }

        /// <summary>
        /// obtener o establecer DIR_FEC_PAG
        /// </summary>
        int DiasCredito { get; set; }

        /// <summary>
        /// obtener o establecer DIR_LIM_CRE
        /// </summary>
        int Credito { get; set; }

        /// <summary>
        /// obtener o establecer DIR_TIPREM_ID
        /// </summary>
        int IdTipoRemision { get; set; }

        /// <summary>
        /// obtener o establecer clave unica del directorio (DIR_CLA)
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer DIR_USER
        /// </summary>
        string DIR_USER { get; set; }

        /// <summary>
        /// obtener o establecer DIR_RFC
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer clave del país de residencia para efectos fiscales del receptor del comprobante
        /// </summary>
        string ResidenciaFiscal {
            get; set;
        }

        /// <summary>
        /// obtener o establecer uso de CFDI predeterminado
        /// </summary>
        string ClaveUsoCFDI { get; set; }

        string DomicilioFiscal { get; set; }

        /// <summary>
        /// obtener o establecer DIR_CP
        /// </summary>
        string CodigoPostal { get; set; }

        /// <summary>
        /// obtener o establecer DIR_COL
        /// </summary>
        string Colonia { get; set; }

        /// <summary>
        /// obtener o establecer DIR_DEL_MUN
        /// </summary>
        string Delegacion { get; set; }

        /// <summary>
        /// obtener o establecer DIR_DES
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer DIR_PASS
        /// </summary>
        string DIR_PASS { get; set; }

        /// <summary>
        /// obtener o establecer DIR_DIR
        /// </summary>
        string Direccion { get; set; }

        /// <summary>
        /// obtener o establecer DIR_EST
        /// </summary>
        string Estado { get; set; }

        /// <summary>
        /// obtener o establecer DIR_NOM
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer DIR_OBS
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer DIR_PAI
        /// </summary>
        string Pais { get; set; }

        /// <summary>
        /// obtener o establecer DIR_TEL
        /// </summary>
        string Telefono { get; set; }

        /// <summary>
        /// obtener o establecer DIR_WEB
        /// </summary>
        string Correo { get; set; }

        /// <summary>
        /// obtener o establecer DIR_USU_N
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer DIR_USU_M
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer DIR_FM
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}