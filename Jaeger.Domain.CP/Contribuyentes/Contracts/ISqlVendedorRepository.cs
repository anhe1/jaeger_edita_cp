﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Contribuyentes.Entities;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    public interface ISqlVendedorRepository : IGenericRepository<VendedorModel> {
        /// <summary>
        /// catalogo de vendedores
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<VendedorDetailModel> GetList(bool onlyActive);

        /// <summary>
        /// catalogo de vendedores
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
    }
}
