﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System.Collections.Generic;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.CP.Contribuyentes.Contracts {
    /// <summary>
    /// repositorio de telefonos del contacto del directorio (DIRCONT)
    /// </summary>
    public interface ISqlDirConTRepository : IGenericRepository<ContribuyenteContactoTelefono> {
        /// <summary>
        /// obtener listado de telefonia de los indices de los contactos
        /// </summary>
        /// <param name="indexs">array de indices DirCon_ID</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <returns></returns>
        IEnumerable<ContribuyenteContactoTelefono> GetList(int[] indexs, bool onlyActive);
    }
}
