﻿using System.ComponentModel;

namespace Jaeger.Domain.CP.Contribuyentes.ValueObjects {
    public enum TelefoniaEnum {
        [Description("Directo")]
        Directo = 0,
        [Description("Directo 2")]
        Directo2 = 1,
        [Description("Particular")]
        Particular = 2,
        [Description("Fax")]
        Fax = 3,
        [Description("Ext. Conmutador")]
        Ext_Conmutador = 4,
        [Description("Beeper")]
        Beeper = 5,
        [Description("Mobil")]
        Mobil = 6,
        [Description("Email")]
        Email = 8,
        [Description("Conmutador")]
        Conmutador = 9
    }
}
