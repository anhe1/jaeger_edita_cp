﻿using System.ComponentModel;

namespace Jaeger.Domain.CP.Contribuyentes.ValueObjects {
    public enum TipoRemisionEnum {
        [Description("S/Remisión")]
        SinRemision = 0,
        /// <summary>
        /// remision sin valores y sin pagare
        /// </summary>
        [Description("S/Pagaré")]
        SinPagare = 1,
        /// <summary>
        /// remision con valores e incluye el pagare
        /// </summary>
        [Description("C/Pagaré")]
        ConPagare = 2,
        /// <summary>
        /// remision con valores e incluye el pagare y con la leyenda Pago contra entrega
        /// </summary>
        [Description("Contado")]
        Contado = 3
    }
}
