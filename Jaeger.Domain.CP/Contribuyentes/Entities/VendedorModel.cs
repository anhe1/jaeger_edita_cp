﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("ven")]
    public partial class VendedorModel : BasePropertyChangeImplementation {
        private int index;
        private int activo;
        private string clave;
        private string nombre;
        private int _VEN_COM;
        private DateTime? fechaModifica;

        public VendedorModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer (ven_id)
        /// </summary>
        [DataNames("VEN_ID")]
        [SugarColumn(ColumnName = "ven_id", ColumnDescription = "VEN_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdVendedor {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer (ven_a)
        /// </summary>
        [DataNames("VEN_A")]
        [SugarColumn(ColumnName = "ven_a", ColumnDescription = "VEN_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer (ven_cla)
        /// </summary>
        [DataNames("VEN_CLA")]
        [SugarColumn(ColumnName = "ven_cla", ColumnDescription = "VEN_CLA", Length = 7)]
        public string Clave {
            get { return this.clave; }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (ven_nom)
        /// </summary>
        [DataNames("VEN_NOM")]
        [SugarColumn(ColumnName = "ven_nom", ColumnDescription = "VEN_NOM", Length = 50)]
        public string Nombre {
            get { return this.nombre; }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (ven_com)
        /// </summary>
        [DataNames("VEN_COM")]
        [SugarColumn(ColumnName = "ven_com", ColumnDescription = "VEN_COM")]
        public int Comision {
            get { return this._VEN_COM; }
            set {
                this._VEN_COM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (ven_fm)
        /// </summary>
        [DataNames("VEN_FM")]
        [SugarColumn(ColumnName = "ven_fm", ColumnDescription = "VEN_FM")]
        public DateTime? FechaModifica {
            get { return this.fechaModifica; }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Descriptor {
            get { return string.Format("{0}: {1}", this.IdVendedor.ToString("00"), this.Nombre); }
        }
    }
}