﻿namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    public class ContribuyenteTipoRemisionModel : Base.Abstractions.BaseSingleModel {
        public ContribuyenteTipoRemisionModel() {
        }

        public ContribuyenteTipoRemisionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
