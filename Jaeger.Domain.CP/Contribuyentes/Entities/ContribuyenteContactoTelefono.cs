﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Contribuyentes.Contracts;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    ///<summary>
    /// telefono del contacto  (DIRCONT)
    ///</summary>
    [SugarTable("dircont")]
    public class ContribuyenteContactoTelefono : BasePropertyChangeImplementation, IContribuyenteContactoTelefonoModel {

        private int index;
        private int activo;
        private int idtelefonia;
        private int idContacto;
        private string numero;
        private DateTime? fechaModifica;

        /// <summary>
        /// obtener o establecer el indice de la tabla (DIRCONT_ID)
        /// </summary>
        [DataNames("DIRCONT_ID")]
        [SugarColumn(ColumnName = "dircont_id", ColumnDescription = "DIRCONT_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDirConT {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (DIRCONT_A)
        /// </summary>
        [DataNames("DIRCONT_A")]
        [SugarColumn(ColumnName = "dircont_a", ColumnDescription = "DIRCONT_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estalecer (DIRCONT_DIRCON_ID)
        /// </summary>
        [DataNames("DIRCONT_DIRCON_ID")]
        [SugarColumn(ColumnName = "dircont_dircon_id", ColumnDescription = "DIRCONT_DIRCON_ID")]
        public int IdContacto {
            get { return this.idContacto; }
            set {
                this.idContacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (DIRCONT_TEL_ID)
        /// </summary>
        [DataNames("DIRCONT_TEL_ID")]
        [SugarColumn(ColumnName = "dircont_tel_id", ColumnDescription = "DIRCONT_TEL_ID")]
        public int IdTelefonia {
            get { return this.idtelefonia; }
            set {
                this.idtelefonia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero telefonico (DIRCONT_NUM)
        /// </summary>
        [DataNames("DIRCONT_NUM")]
        [SugarColumn(ColumnName = "dircont_num", ColumnDescription = "DIRCONT_NUM", Length = 50)]
        public string Numero {
            get { return this.numero; }
            set {
                this.numero = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro (DIRCONT_FM)
        /// </summary>
        [DataNames("DIRCONT_FM")]
        [SugarColumn(ColumnName = "dircont_fm", ColumnDescription = "DIRCONT_FM")]
        public DateTime? FechaModifica {
            get { return this.fechaModifica; }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}