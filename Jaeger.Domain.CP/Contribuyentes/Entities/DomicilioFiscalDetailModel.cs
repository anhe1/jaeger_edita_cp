﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;
using Jaeger.Domain.Base.ValueObjects;
using SqlSugar;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    public class DomicilioFiscalDetailModel : DomicilioFiscalModel {
        public DomicilioFiscalDetailModel() : base() {

        }

        /// <summary>
        /// obtener o establecer el tipo de domicilio registrado este campo es utilizado en modo texto en edita
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public TipoDomicilioEnum TipoEnum {
            get {
                return (TipoDomicilioEnum)(Enum.Parse(typeof(TipoDomicilioEnum), this.Tipo));
            }
            set {
                this.Tipo = Enum.GetName(typeof(TipoDomicilioEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de domicilio registrado, esta campo modifica modo numerico utilizado en CPLite
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public TipoDomicilioEnum TipoDomicilio {
            get { return (TipoDomicilioEnum)base.IdTipoDomicilio; }
            set {
                this.IdTipoDomicilio = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para el boton de autorizacion de la direccion 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string AutorizadoText {
            get {
                if (this.IdAutorizado == 1)
                    return "Autorizado";
                return "Pendiente";
            }
        }
    }
}