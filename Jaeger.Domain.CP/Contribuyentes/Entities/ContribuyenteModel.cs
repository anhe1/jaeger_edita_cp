﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    /// <summary>
    /// clase para el directorio de CP
    /// </summary>
    [SugarTable("dir", "directorio de clientes/proveedores/")]
    public class ContribuyenteModel : Persona, Contracts.IContribuyenteModel {
        #region declaraciones
        private int index;
        private int activo;
        private int _DIR_DIRCLA_ID;
        private int _DIR_DIRESP_ID;
        private string _DIR_CLA;
        private string _DIR_TEL;
        private string _DIR_DIR;
        private string _DIR_COL;
        private string _DIR_CP;
        private string _DIR_DEL_MUN;
        private string _DIR_EST;
        private string _DIR_PAI;
        private int _DIR_FEC_PAG;
        private string _DIR_OBS;
        private string _DIR_DES;
        private int _DIR_LIM_CRE;
        private string _DIR_WEB;
        private string _DIR_USU_N;
        private string _DIR_USU_M;
        private DateTime? _DIR_FM;
        private int _DIR_AUT;
        private string _DIR_USER;
        private string _DIR_PASS;
        private int _DIR_PUE;
        private int _DIR_DEP;
        private int _DIR_TIPREM_ID;
        private string residenciaFiscal;
        private string claveUsoCFDI;
        #endregion

        public ContribuyenteModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (DIR_ID)
        /// </summary>
        [DataNames("DIR_ID")]
        [SugarColumn(ColumnName = "dir_id", ColumnDescription = "DIR_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDirectorio {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (DIR_A)
        /// </summary>
        [DataNames("DIR_A")]
        [SugarColumn(ColumnName = "dir_a", ColumnDescription = "DIR_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_DIRCLA_ID
        /// </summary>
        [DataNames("DIR_DIRCLA_ID")]
        [SugarColumn(ColumnName = "dir_dircla_id", ColumnDescription = "DIR_DIRCLA_ID", IsNullable = false)]
        public int IdClase {
            get { return this._DIR_DIRCLA_ID; }
            set {
                this._DIR_DIRCLA_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer id de especialidad (DIR_DIRESP_ID)
        /// </summary>
        [DataNames("DIR_DIRESP_ID")]
        [SugarColumn(ColumnName = "dir_diresp_id", ColumnDescription = "DIR_DIRESP_ID", IsNullable = false)]
        public int IdEspecialidad {
            get { return this._DIR_DIRESP_ID; }
            set {
                this._DIR_DIRESP_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_AUT
        /// </summary>
        [DataNames("DIR_AUT")]
        [SugarColumn(ColumnName = "dir_aut", ColumnDescription = "DIR_AUT")]
        public int DIR_AUT {
            get { return this._DIR_AUT; }
            set {
                this._DIR_AUT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_DEP
        /// </summary>
        [DataNames("DIR_DEP")]
        [SugarColumn(ColumnName = "dir_dep", ColumnDescription = "DIR_DEP", IsNullable = false)]
        public int DIR_DEP {
            get { return this._DIR_DEP; }
            set {
                this._DIR_DEP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_PUE
        /// </summary>
        [DataNames("DIR_PUE")]
        [SugarColumn(ColumnName = "dir_pue", ColumnDescription = "DIR_PUE", IsNullable = false)]
        public int DIR_PUE {
            get { return this._DIR_PUE; }
            set {
                this._DIR_PUE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_FEC_PAG
        /// </summary>
        [DataNames("DIR_FEC_PAG")]
        [SugarColumn(ColumnName = "dir_fec_pag", ColumnDescription = "DIR_FEC_PAG")]
        public int DiasCredito {
            get { return this._DIR_FEC_PAG; }
            set {
                this._DIR_FEC_PAG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_LIM_CRE
        /// </summary>
        [DataNames("DIR_LIM_CRE")]
        [SugarColumn(ColumnName = "dir_lim_cre", ColumnDescription = "DIR_LIM_CRE")]
        public int Credito {
            get { return this._DIR_LIM_CRE; }
            set {
                this._DIR_LIM_CRE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de remision aplicable (DIR_TIPREM_ID)
        /// </summary>
        [DataNames("DIR_TIPREM_ID")]
        [SugarColumn(ColumnName = "dir_tiprem_id", ColumnDescription = "DIR_TIPREM_ID")]
        public int IdTipoRemision {
            get { return this._DIR_TIPREM_ID; }
            set {
                this._DIR_TIPREM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave unica del directorio (DIR_CLA)
        /// </summary>
        [DataNames("DIR_CLA")]
        [SugarColumn(ColumnName = "dir_cla", ColumnDescription = "DIR_CLA", Length = 10)]
        public string Clave {
            get { return this._DIR_CLA; }
            set {
                this._DIR_CLA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_USER
        /// </summary>
        [DataNames("DIR_USER")]
        [SugarColumn(ColumnName = "dir_user", ColumnDescription = "DIR_USER", Length = 5)]
        public string DIR_USER {
            get { return this._DIR_USER; }
            set {
                this._DIR_USER = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_RFC
        /// </summary>
        [DataNames("DIR_RFC")]
        [SugarColumn(ColumnName = "dir_rfc", ColumnDescription = "DIR_RFC", Length = 15)]
        public new string RFC {
            get { return base.RFC; }
            set { if (!string.IsNullOrEmpty(value)) {
                    base.RFC = value.CleanRFC();
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer DIR_CP
        /// </summary>
        [DataNames("DIR_CP")]
        [SugarColumn(ColumnName = "dir_cp", ColumnDescription = "DIR_CP", Length = 10)]
        public string CodigoPostal {
            get { return this._DIR_CP; }
            set {
                this._DIR_CP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_COL
        /// </summary>
        [DataNames("DIR_COL")]
        [SugarColumn(ColumnName = "dir_col", ColumnDescription = "DIR_COL", Length = 50)]
        public string Colonia {
            get { return this._DIR_COL; }
            set {
                this._DIR_COL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer delegacion o municipio (DIR_DEL_MUN)
        /// </summary>
        [DataNames("DIR_DEL_MUN")]
        [SugarColumn(ColumnName = "dir_del_mun", ColumnDescription = "DIR_DEL_MUN", Length = 50)]
        public string Delegacion {
            get { return this._DIR_DEL_MUN; }
            set {
                this._DIR_DEL_MUN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_DES
        /// </summary>
        [DataNames("DIR_DES")]
        [SugarColumn(ColumnName = "dir_des", ColumnDescription = "DIR_DES", Length = 50)]
        public string Descripcion {
            get { return this._DIR_DES; }
            set {
                this._DIR_DES = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_PASS
        /// </summary>
        [DataNames("DIR_PASS")]
        [SugarColumn(ColumnName = "dir_pass", ColumnDescription = "DIR_PASS", Length = 24)]
        public string DIR_PASS {
            get { return this._DIR_PASS; }
            set {
                this._DIR_PASS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_DIR
        /// </summary>
        [DataNames("DIR_DIR")]
        [SugarColumn(ColumnName = "dir_dir", ColumnDescription = "DIR_DIR", Length = 100)]
        public string Direccion {
            get { return this._DIR_DIR; }
            set {
                this._DIR_DIR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_EST Pais-Estado
        /// </summary>
        [DataNames("DIR_EST")]
        [SugarColumn(ColumnName = "dir_est", ColumnDescription = "DIR_EST", Length = 25)]
        public string Estado {
            get { return this._DIR_EST; }
            set {
                this._DIR_EST = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_NOM
        /// </summary>
        [DataNames("DIR_NOM")]
        [SugarColumn(ColumnName = "dir_nom", ColumnDescription = "DIR_NOM", Length = 100)]
        public new string Nombre {
            get { return base.Nombre; }
            set {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_OBS
        /// </summary>
        [DataNames("DIR_OBS")]
        [SugarColumn(ColumnName = "dir_obs", ColumnDescription = "DIR_OBS", Length = 255)]
        public string Nota {
            get { return this._DIR_OBS; }
            set {
                this._DIR_OBS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_PAI en el directorio esta como ciudad
        /// </summary>
        [DataNames("DIR_PAI")]
        [SugarColumn(ColumnName = "dir_pai", ColumnDescription = "DIR_PAI", Length = 25)]
        public string Pais {
            get { return this._DIR_PAI; }
            set {
                this._DIR_PAI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_TEL
        /// </summary>
        [DataNames("DIR_TEL")]
        [SugarColumn(ColumnName = "dir_tel", ColumnDescription = "DIR_TEL", Length = 50)]
        public string Telefono {
            get { return this._DIR_TEL; }
            set {
                this._DIR_TEL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_WEB
        /// </summary>
        [DataNames("DIR_WEB")]
        [SugarColumn(ColumnName = "dir_web", ColumnDescription = "DIR_WEB", Length = 50)]
        public string Correo {
            get { return this._DIR_WEB; }
            set {
                this._DIR_WEB = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_USU_N
        /// </summary>
        [DataNames("DIR_USU_N")]
        [SugarColumn(ColumnName = "dir_usu_n", ColumnDescription = "DIR_USU_N", Length = 7)]
        public string Creo {
            get { return this._DIR_USU_N; }
            set {
                this._DIR_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_USU_M
        /// </summary>
        [DataNames("DIR_USU_M")]
        [SugarColumn(ColumnName = "dir_usu_m", ColumnDescription = "DIR_USU_M", Length = 7)]
        public string Modifica {
            get { return this._DIR_USU_M; }
            set {
                this._DIR_USU_M = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_FM
        /// </summary>
        [DataNames("DIR_FM")]
        [SugarColumn(ColumnName = "dir_fm", ColumnDescription = "DIR_FM")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this._DIR_FM >= firstGoodDate)
                    return this._DIR_FM;
                return null;
            }
            set {
                this._DIR_FM = value;
                this.OnPropertyChanged();
            }
        }

        public string ResidenciaFiscal {
            get { return this.residenciaFiscal; }
            set {
                this.residenciaFiscal = value;
                this.OnPropertyChanged();
            }
        }

        public new string DomicilioFiscal {
            get { return base.DomicilioFiscal; }
            set {
                base.DomicilioFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer uso de CFDI predeterminado
        /// </summary>
        public string ClaveUsoCFDI {
            get { return this.claveUsoCFDI; }
            set {
                this.claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }
    }
}