﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Contribuyentes.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    /// <summary>
    /// clase de contribuyentes (tabla DIRCLA)
    /// </summary>
    public class ContribuyenteClaseModel : BasePropertyChangeImplementation, IContribuyenteClaseModel {
        private int index;
        private bool activo;
        private string nombre;
        private string clase;
        private DateTime? fechaModifica;
        private int idDepto;

        /// <summary>
        /// obtener o establecer indice de la tabla (DIRCLA_ID)
        /// </summary>
        [DataNames("DIRCLA_ID")]
        [SugarColumn(ColumnName = "dircla_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdClase {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("DIRCLA_A")]
        [SugarColumn(ColumnName = "dircla_a", ColumnDescription = "DIRCLA_A")]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del departamento (DIRCLA_DEP_ID)
        /// </summary>
        [DataNames("DIRCLA_DEP_ID")]
        [SugarColumn(ColumnName = "dircla_dep_id", ColumnDescription = "DIRCLA_DEP_ID")]
        public int IdDepto {
            get { return this.idDepto; }
            set {
                this.idDepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la clase (DIRCLA_CLA)
        /// </summary>
        [DataNames("DIRCLA_CLA")]
        [SugarColumn(ColumnName = "dircla_cla", ColumnDescription = "DIRCLA_CLA", Length = 50)]
        public string Nombre {
            get { return this.nombre; }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIRCLA_C
        /// </summary>
        [DataNames("DIRCLA_C")]
        [SugarColumn(ColumnName = "dircla_c", ColumnDescription = "DIRCLA_C", Length = 1)]
        public string Clase {
            get { return this.clase; }
            set {
                this.clase = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (DIRCLA_FM)
        /// </summary>
        [DataNames("DIRCLA_FM")]
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica >= new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
