﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.CP.Contribuyentes.Contracts;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    /// <summary>
    /// clase para el directorio de CP
    /// </summary>
    public class ContribuyenteDetailModel : ContribuyenteModel, IContribuyenteModel {
        private BindingList<ContribuyenteContactoDetailModel> contactos;

        /// <summary>
        /// constructor
        /// </summary>
        public ContribuyenteDetailModel() {
            this.contactos = new BindingList<ContribuyenteContactoDetailModel>();
        }

        /// <summary>
        /// lista de contactos asociado al contribuyente
        /// </summary>
        public BindingList<ContribuyenteContactoDetailModel> Contactos {
            get { return this.contactos; }
            set { this.contactos = value;
                this.OnPropertyChanged();
            }
        }

        public RemisionFormatoEnum Remisionado {
            get { return (RemisionFormatoEnum)this.IdTipoRemision; }
            set { base.IdTipoRemision = (int)value;
                this.OnPropertyChanged();
            }
        }

        public string Descriptor {
            get { return string.Format("{0}: {1}", this.Clave, this.Nombre); }
        }

        public string DomicilioToString {
            get {
                string direccion = string.Empty;

                if (!(string.IsNullOrEmpty(this.Direccion))) {
                    if (this.Direccion.ToLower().StartsWith("calle")) {
                        direccion = this.Direccion;
                    } else {
                        direccion = string.Concat("Calle ", this.Direccion);
                    }
                }

                //if (!(string.IsNullOrEmpty(this.NoExterior)))
                //    direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);

                //if (!(string.IsNullOrEmpty(this.NoInterior)))
                //    direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);

                if (!(string.IsNullOrEmpty(this.Colonia)))
                    direccion = string.Concat(direccion, " Col. ", this.Colonia);

                if (!(string.IsNullOrEmpty(this.CodigoPostal)))
                    direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);

                if (!(string.IsNullOrEmpty(this.Delegacion)))
                    direccion = string.Concat(direccion, " ", this.Delegacion);

                if (!(string.IsNullOrEmpty(this.Estado)))
                    direccion = string.Concat(direccion, ", ", this.Estado);

                //if (!(string.IsNullOrEmpty(this.Referencia)))
                //    direccion = string.Concat(direccion, ", Ref: ", this.Referencia);

                //if (!(string.IsNullOrEmpty(this.Localidad)))
                //    direccion = string.Concat(direccion, ", Loc: ", this.Localidad);

                return direccion;
            }
        }

        public DomicilioFiscalDetailModel GetDomicilio() {
            return new DomicilioFiscalDetailModel() {
                Activo = true,
                Colonia = this.Colonia,
                CodigoPostal = this.CodigoPostal,
                Calle = this.Direccion,
                Pais = this.Pais,
                Estado = this.Estado,
            };
        }
    }
}