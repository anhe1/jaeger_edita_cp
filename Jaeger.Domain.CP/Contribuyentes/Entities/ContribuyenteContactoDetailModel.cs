﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using Jaeger.Domain.CP.Contribuyentes.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    public class ContribuyenteContactoDetailModel : ContribuyenteContactoModel, IContribuyenteContactoModel {
        private BindingList<ContribuyenteContactoTelefono> telefonos;

        public ContribuyenteContactoDetailModel() {
            this.telefonos = new BindingList<ContribuyenteContactoTelefono>();
        }

        /// <summary>
        /// obtener o establecer listado de telefonos
        /// </summary>
        public BindingList<ContribuyenteContactoTelefono> Telefonos {
            get { return this.telefonos; }
            set { this.telefonos = value;
                this.OnPropertyChanged();
            }
        }
    }
}