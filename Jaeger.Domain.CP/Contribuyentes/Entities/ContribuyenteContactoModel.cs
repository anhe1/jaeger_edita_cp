﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Contribuyentes.Contracts;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    /// <summary>
    /// Contacto del directorio (DIRCON)
    /// </summary>
    public class ContribuyenteContactoModel : BasePropertyChangeImplementation, IContribuyenteContactoModel {
        #region declaraciones
        private int index;
        private int activo;
        private string contacto;
        private string departamento;
        private string puesto;
        private string observaciones;
        private int iddirectorio;
        private DateTime? fechaModifica;
        #endregion

        public ContribuyenteContactoModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (DIRCON_ID)
        /// </summary>
        [DataNames("DIRCON_ID")]
        [SugarColumn(ColumnName = "dircon_id", ColumnDescription = "DIRCON_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdContacto {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (DIRCON_A)
        /// </summary>
        [DataNames("DIRCON_A")]
        [SugarColumn(ColumnName = "dircon_a", ColumnDescription = "DIRCON_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (DIRCON_DIR_ID)
        /// </summary>
        [DataNames("DIRCON_DIR_ID")]
        [SugarColumn(ColumnName = "dircon_dir_id", ColumnDescription = "DIRCON_DIR_ID")]
        public int IdDirectorio {
            get { return this.iddirectorio; }
            set {
                this.iddirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto (DIRCON_CON)
        /// </summary>
        [DataNames("DIRCON_CON")]
        [SugarColumn(ColumnName = "dircon_con", ColumnDescription = "DIRCON_CON", Length = 50)]
        public string Contacto {
            get { return this.contacto; }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer departamento (DIRCON_DEP)
        /// </summary>
        [DataNames("DIRCON_DEP")]
        [SugarColumn(ColumnName = "dircon_dep", ColumnDescription = "DIRCON_DEP", Length = 50)]
        public string Departamento {
            get { return this.departamento; }
            set {
                this.departamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer puesto (DIRCON_PUE)
        /// </summary>
        [DataNames("DIRCON_PUE")]
        [SugarColumn(ColumnName = "dircon_pue", ColumnDescription = "DIRCON_PUE", Length = 50)]
        public string Puesto {
            get { return this.puesto; }
            set {
                this.puesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones (DIRCON_OBS)
        /// </summary>
        [DataNames("DIRCON_OBS")]
        [SugarColumn(ColumnName = "dircon_obs", ColumnDescription = "DIRCON_OBS", Length = 50)]
        public string Nota {
            get { return this.observaciones; }
            set {
                this.observaciones = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de modificacion (DIRCON_FM)
        /// </summary>
        [DataNames("DIRCON_FM")]
        [SugarColumn(ColumnName = "dircon_fm", ColumnDescription = "DIRCON_FM")]
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica >= new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}