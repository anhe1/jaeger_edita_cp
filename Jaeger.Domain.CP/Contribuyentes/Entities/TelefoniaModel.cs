﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    ///<summary>
    /// Tipo de telefonia (TEL)
    ///</summary>
    [SugarTable("tel")]
    public partial class TelefoniaModel : BasePropertyChangeImplementation {
        private int index;
        private int activo;
        private string tipo;
        private DateTime? fechaModifica;

        public TelefoniaModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (TEL_ID)
        /// </summary>
        [DataNames("TEL_ID")]
        [SugarColumn(ColumnName = "tel_id", ColumnDescription = "TEL_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdTelefonia {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (TEL_A)
        /// </summary>
        [DataNames("TEL_A")]
        [SugarColumn(ColumnName = "tel_a", ColumnDescription = "TEL_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (TEL_TEL)
        /// </summary>
        [DataNames("TEL_TEL")]
        [SugarColumn(ColumnName = "tel_tel", ColumnDescription = "TEL_TEL", Length = 15)]
        public string Tipo {
            get { return this.tipo; }
            set {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (TEL_FM)
        /// </summary>
        [DataNames("TEL_FM")]
        [SugarColumn(ColumnName = "tel_fm", ColumnDescription = "TEL_FM")]
        public DateTime? FechaModifica {
            get { return this.fechaModifica; }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}