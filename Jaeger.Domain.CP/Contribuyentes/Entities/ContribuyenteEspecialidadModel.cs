﻿/// purpose:
/// develop: ANHE1 28092020 11:14
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Contribuyentes.Contracts;

namespace Jaeger.Domain.CP.Contribuyentes.Entities {
    ///<summary>
    /// especialidad (tabla DIRESP)
    ///</summary>
    [SugarTable("diresp")]
    public class ContribuyenteEspecialidadModel : BasePropertyChangeImplementation, IContribuyenteEspecialidadModel {

        private int _DIRESP_ID;
        private int activo;
        private string _DIRESP_ESP;
        private int _DIRESP_BPS;
        private DateTime? _DIRESP_FM;

        public ContribuyenteEspecialidadModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (DIRESP_ID)
        /// </summary>
        [DataNames("DIRESP_ID")]
        [SugarColumn(ColumnName = "diresp_id", ColumnDescription = "DIRESP_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDirEsp {
            get { return this._DIRESP_ID; }
            set {
                this._DIRESP_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (DIRESP_A)
        /// </summary>
        [DataNames("DIRESP_A")]
        [SugarColumn(ColumnName = "diresp_a", ColumnDescription = "DIRESP_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de bienes productos y servicios (DIRESP_BPS)
        /// </summary>
        [DataNames("DIRESP_BPS")]
        [SugarColumn(ColumnName = "diresp_bps", ColumnDescription = "DIRESP_BPS")]
        public int DIRESP_BPS {
            get { return this._DIRESP_BPS; }
            set {
                this._DIRESP_BPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la especialidad (DIRESP_ESP)
        /// </summary>
        [DataNames("DIRESP_ESP")]
        [SugarColumn(ColumnName = "diresp_esp", ColumnDescription = "DIRESP_ESP", Length = 50)]
        public string Nombre {
            get { return this._DIRESP_ESP; }
            set {
                this._DIRESP_ESP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (DIRESP_FM)
        /// </summary>
        [DataNames("DIRESP_FM")]
        [SugarColumn(ColumnName = "diresp_fm", ColumnDescription = "DIRESP_FM")]
        public DateTime? FechaModifica {
            get { return this._DIRESP_FM; }
            set {
                this._DIRESP_FM = value;
                this.OnPropertyChanged();
            }
        }
    }
}