﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.CP.Contribuyentes.Builder {
    /// <summary>
    /// Consulta del directorio
    /// </summary>
    public interface IDirectorioQueryBuilder : IConditionalBuilder {
        IConditionalBuilder ById(int id);

        /// <summary>
        /// filtro por clase 2=Cliente, 9=Empleado
        /// </summary>
        /// <param name="idClase">indice de relacion</param>
        IDirectorioIdClaseBuilder ByIdClase(int idClase);
        IDirectorioIdClaseBuilder Search(string search);
    }

    public interface IDirectorioIdClaseBuilder : IConditionalBuilder {
        IDirectorioActivoBuilder OnlyActive(bool active = true);
        IDirectorioIdClaseBuilder Search(string search);
    }

    public interface IDirectorioActivoBuilder : IConditionalBuilder {

    }
}
