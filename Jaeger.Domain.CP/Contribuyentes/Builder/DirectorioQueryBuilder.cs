﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.CP.Contribuyentes.Builder {
    /// <summary>
    /// Consulta del directorio
    /// </summary>
    public class DirectorioQueryBuilder : ConditionalBuilder, IConditionalBuilder, IDirectorioQueryBuilder, IDirectorioIdClaseBuilder, IDirectorioActivoBuilder {
        public DirectorioQueryBuilder() : base() {

        }

        public IConditionalBuilder ById(int id) {
            this._Conditionals.Clear();
            this._Conditionals.Add(new Conditional("DIR_ID", id.ToString()));
            return this;
        }

        public IDirectorioIdClaseBuilder ByIdClase(int idClase) {
            this._Conditionals.Add(new Conditional("DIR_DIRCLA_ID", idClase.ToString()));
            return this;
        }

        public IDirectorioActivoBuilder OnlyActive(bool active = true) {
            this._Conditionals.Add(new Conditional("DIR_A", "1"));
            return this;
        }

        public IDirectorioIdClaseBuilder Search(string search) {
            this._Conditionals.Add(new Conditional("@SEARCH", search, ConditionalTypeEnum.Like));
            return this;
        }
    }
}
