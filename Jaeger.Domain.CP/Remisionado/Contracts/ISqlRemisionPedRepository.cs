﻿/// purpose: repositorio de partidas o conceptos de remision (REMPED)
/// develop: ANHE1 27092020 0119
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Remisionado.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Remisionado.Contracts {
    /// <summary>
    /// repositorio de partidas o conceptos de remision (REMPED)
    /// </summary>
    public interface ISqlRemisionPedRepository : IGenericRepository<RemisionPedModel> {
        /// <summary>
        /// obtener listado de partidas por el indice de la remision
        /// </summary>
        /// <param name="idRemision">indice de la remision REM_ID</param>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<RemisionPedModel> GetList(int idRemision, bool onlyActive);

        /// <summary>
        /// obtener listado de partidas por un array de indices de remisiones
        /// </summary>
        /// <param name="idRemision">array de indices REM_ID</param>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<RemisionPedModelView> GetList(int[] idRemision, bool onlyActive);

        /// <summary>
        /// obtener listado de las partidas de remisiones por la descripcion de los conceptos
        /// </summary>
        /// <param name="descripcion">descipcion del concepto</param>
        /// <param name="Orden">a partir del numero de orden especifico</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <returns></returns>
        IEnumerable<RemisionPedModelView> GetList(string descripcion, int Orden, bool onlyActive = true);
    }
}
