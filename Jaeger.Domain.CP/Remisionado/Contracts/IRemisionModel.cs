﻿/// purpose: modelo Remision (tabla REM)
/// develop: ANHE1 27092020 0121
using System;

namespace Jaeger.Domain.CP.Remisionado.Contracts {
    ///<summary>
    /// modelo Remision (tabla REM)
    ///</summary>
    public interface IRemisionModel {
        /// <summary>
        /// obtener o establecer (REM_ID)
        /// </summary>
        int IdRemision { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (REM_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer REM_ST_ID
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer (REM_DIR_ID)
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer (REM_DIRCON_ID)
        /// </summary>
        int IdContacto { get; set; }

        /// <summary>
        /// obtener o establecer (REM_VENCEDIAS)
        /// </summary>
        int VenceDias { get; set; }

        /// <summary>
        /// obtener o establecer el lugar de embarque (REM_EMB)
        /// </summary>
        string Embarque { get; set; }

        /// <summary>
        /// obtener o establecer REM_OBS
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer Total de la remision (REM_$)
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer el monto total cobrado (REM_$PAG)
        /// </summary>
        decimal Acumulado { get; set; }

        /// <summary>
        /// obtener o establecer (REM_$DEU)
        /// </summary>
        decimal Saldo { get; set; }

        /// <summary>
        /// obtener o establecer (REM_VEN)
        /// </summary>
        DateTime? REM_VEN { get; set; }

        /// <summary>
        /// obtener o establecer fecha de vencimiento (REM_VENCE)
        /// </summary>
        DateTime? FechaVence { get; set; }

        /// <summary>
        /// obtener o establecer REM_FEC
        /// </summary>
        DateTime? FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (REM_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro (REM_USU_N)
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro (REM_USU_M)
        /// </summary>
        string Modifica { get; set; }
    }
}