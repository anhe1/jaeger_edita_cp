﻿/// purpose: modelo de partida o concepto de remision (REMPED)
/// develop: ANHE1 270920200120
using System;

namespace Jaeger.Domain.CP.Remisionado.Contracts {
    ///<summary>
    /// modelo de partida o concepto de remision (REMPED)
    ///</summary>
    public interface IRemisionPedModel {
        /// <summary>
        /// obtener o establecer REMPED_ID
        /// </summary>
        int IdRemPed { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (REMPED_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con remision (REMPED_REM_ID)
        /// </summary>
        int IdRemision { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_ALMX_ID
        /// </summary>
        int REMPED_ALMX_ID { get; set; }

        /// <summary>
        /// obtener o establecer numero de orden de produccion (REMPED_PEDPRD_ID)
        /// </summary>
        int IdPedPrd { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_PRD_CUENTA
        /// </summary>
        int REMPED_PRD_CUENTA { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_USU_FM
        /// </summary>
        int REMPED_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_UNI
        /// </summary>
        int Unidad { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_CAN
        /// </summary>
        decimal Cantidad { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_$_UNI
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_$
        /// </summary>
        decimal Importe { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_$PAG
        /// </summary>
        decimal Acumulado { get; set; }

        /// <summary>
        /// obtener o establecer REMPED_OBS
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (REMPED_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro REMPED_USU_N
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro REMPED_USU_M
        /// </summary>
        string Modifica { get; set; }
    }
}