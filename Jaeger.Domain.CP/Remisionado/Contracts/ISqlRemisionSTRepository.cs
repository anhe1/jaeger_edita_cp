﻿/// purpose: repositorio de status de remision (REMST)
/// develop: ANHE1 27092020 0118
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Remisionado.Entities;

namespace Jaeger.Domain.CP.Remisionado.Contracts {
    /// <summary>
    /// repositorio de status de remision (REMST)
    /// </summary>
    public interface ISqlRemisionSTRepository : IGenericRepository<RemisionSTModel> {

    }
}
