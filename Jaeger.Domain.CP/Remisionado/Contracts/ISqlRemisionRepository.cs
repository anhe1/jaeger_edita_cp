﻿/// purpose: repositorio de remisiones (REM)
/// develop: ANHE1 27092020 0118
using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Remisionado.Entities;

namespace Jaeger.Domain.CP.Remisionado.Contracts {
    /// <summary>
    /// repositorio de remisiones (REM)
    /// </summary>
    public interface ISqlRemisionRepository : IGenericRepository<RemisionModel> {
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();

        /// <summary>
        /// Remision
        /// </summary>
        RemisionDetailModelView GetById(int id, bool onlyActive);
        /// <summary>
        /// almacenar una remision
        /// </summary>
        RemisionDetailModelView Save(RemisionDetailModelView remision);

        /// <summary>
        /// obtener vista de remisiones por un periodo de fechas y status
        /// </summary>
        /// <param name="starDate">rango inicial de fecha, si el endDate en nulo entonces se considera por año y mes</param>
        /// <param name="endDate">fin del rango de fecha</param>
        /// <param name="startStatus">rango inicial de status, si endStatus es nulo entonces se conisdera el status especifico</param>
        /// <param name="endStatus">fin del rango de status</param>
        IEnumerable<RemisionModelView> GetList(DateTime startDate, DateTime? endDate, int? startStatus, int? endStatus);

        /// <summary>
        /// obtener vista de remisiones por un periodo de fechas y status
        /// </summary>
        /// <param name="starDate">rango inicial de fecha, si el endDate en nulo entonces se considera por año y mes</param>
        /// <param name="endDate">fin del rango de fecha</param>
        /// <param name="startStatus">rango inicial de status, si endStatus es nulo entonces se conisdera el status especifico</param>
        /// <param name="endStatus">fin del rango de status</param>
        IEnumerable<RemisionDetailModelView> GetList(DateTime startDate, DateTime? endDate, int? startStatus, int? endStatus, bool onlyActive);

        /// <summary>
        /// obtener vista de remisiones por un periodo de fechas y status
        /// </summary>
        /// <param name="startDate">rango inicial de fecha, si el endDate en nulo entonces se considera por año y mes</param>
        /// <param name="endDate">fin del rango de fecha</param>
        /// <param name="startStatus">rango inicial de status, si endStatus es nulo entonces se conisdera el status especifico</param>
        /// <param name="endStatus">fin del rango de status</param>
        /// <param name="startPedPrd">inicio del rango PedPrd, si el parametro endPedPrd es nulo se considera un solo numero de orden</param>
        /// <param name="endPedPrd"></param>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<RemisionPedDetailModelView> GetList(DateTime? startDate, DateTime? endDate, int? startStatus, int? endStatus, int? startPedPrd, int? endPedPrd, bool onlyActive);

        /// <summary>
        /// obtener vista de remisiones por condicionales
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<RemisionPedDetailModelView> GetList(List<Conditional> conditionals, bool onlyActive);

        /// <summary>
        /// obtener lista de remisiones por el nombre del receptor
        /// </summary>
        /// <param name="receptor">descripcion del receptor</param>
        /// <param name="startStatus">status inicial</param>
        /// <param name="endStatus">status final</param>
        /// <param name="onlyActive">solo registros activos</param>
        /// <returns></returns>
        IEnumerable<RemisionDetailModelView> GetList(string receptor, int? startStatus, int? endStatus, bool onlyActive);
    }
}
