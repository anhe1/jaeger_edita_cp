﻿/// purpose: modelo de Status de remision (REMST)
/// develop: 27092020 01:20
using System;

namespace Jaeger.Domain.CP.Remisionado.Entities {
    /// <summary>
    /// modelo de Status de remision (REMST)
    /// </summary>
    public interface IRemisionSTModel {

        /// <summary>
        /// obtener o establecer el indice de la tabla (REMST_ID)
        /// </summary>
        int IdRemST { get; set; }

        /// <summary>
        /// obtener o establecer el registro activo (REMST_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia (REMST_SEC)
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer (REMST_SUM)
        /// </summary>
        int REMST_SUM { get; set; }

        /// <summary>
        /// obtener o estableccer la descripcion del status (REMST_STA)
        /// </summary>
        string Status { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion (REMST_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}