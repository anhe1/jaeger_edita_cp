﻿
namespace Jaeger.Domain.CP.Remisionado.ValueObjects {
    public enum RemisionStatusEnum {
        Pendiente = 1,
        Entregada = 5,
        Pagada = 10,
        Cancelada = 20,
        Anticipada = 3,
        Cobrar = 21
    }
}
