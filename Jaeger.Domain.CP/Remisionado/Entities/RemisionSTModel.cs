﻿/// purpose: modelo de status de remision
/// develop: ANHE1 270920200115
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Remisionado.Entities {
    /// <summary>
    /// modelo de Status de remision (REMST)
    /// </summary>
    [SugarTable("remst")]
    public partial class RemisionSTModel : BasePropertyChangeImplementation, IRemisionSTModel {
        private int _REMST_ID;
        private int _REMST_A;
        private string _REMST_STA;
        private int _REMST_SEC;
        private int _REMST_SUM;
        private DateTime? _REMST_FM;
        public RemisionSTModel() {

        }

        /// <summary>
        /// obtener o establecer el indice de la tabla (REMST_ID)
        /// </summary>
        [DataNames("REMST_ID")]
        [SugarColumn(ColumnName = "remst_id", ColumnDescription = "REMST_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemST {
            get { return this._REMST_ID; }
            set {
                this._REMST_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo (REMST_A)
        /// </summary>
        [DataNames("REMST_A")]
        [SugarColumn(ColumnName = "remst_a", ColumnDescription = "REMST_A")]
        public int Activo {
            get { return this._REMST_A; }
            set {
                this._REMST_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia (REMST_SEC)
        /// </summary>
        [DataNames("REMST_SEC")]
        [SugarColumn(ColumnName = "remst_sec", ColumnDescription = "REMST_SEC")]
        public int Secuencia {
            get { return this._REMST_SEC; }
            set {
                this._REMST_SEC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (REMST_SUM)
        /// </summary>
        [DataNames("REMST_SUM")]
        [SugarColumn(ColumnName = "remst_sum", ColumnDescription = "REMST_SUM")]
        public int REMST_SUM {
            get { return this._REMST_SUM; }
            set {
                this._REMST_SUM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estableccer la descripcion del status (REMST_STA)
        /// </summary>
        [DataNames("REMST_STA")]
        [SugarColumn(ColumnName = "remst_sta", ColumnDescription = "REMST_STA", Length = 50)]
        public string Status {
            get { return this._REMST_STA; }
            set {
                this._REMST_STA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion (REMST_FM)
        /// </summary>
        [DataNames("REMST_FM")]
        [SugarColumn(ColumnName = "remst_fm", ColumnDescription = "REMST_FM")]
        public DateTime? FechaModifica {
            get { return this._REMST_FM; }
            set {
                this._REMST_FM = value;
                this.OnPropertyChanged();
            }
        }
    }
}
