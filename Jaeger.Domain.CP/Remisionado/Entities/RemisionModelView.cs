﻿/// purpose: vista de remision (REM)
/// develop: ANHE1 27/09/20200117
using Jaeger.Domain.CP.Remisionado.Contracts;
using Jaeger.Domain.CP.Remisionado.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Remisionado.Entities {
    /// <summary>
    /// vista de remision (REM)
    /// </summary>
    public class RemisionModelView : RemisionModel, IRemisionModel {
        private string _DIR_TEL;
        private string _DIR_CP;
        private string _DIR_COL;
        private string _DIR_DEL_MUN;
        private string _DIR_DIR;
        private string _DIR_EST;
        private string _DIR_PAI;

        public RemisionModelView() {
        }

        #region directorio 

        [DataNames("DIR_cla")]
        public string Clave { get; set; }

        [DataNames("DIR_Nom")]
        public string Cliente { get; set; }

        [DataNames("DIRCON_con")]
        public string Contacto { get; set; }

        /// <summary>
        /// obtener o establecer DIR_CP
        /// </summary>
        [DataNames("DIR_CP")]
        public string CodigoPostal {
            get {
                return this._DIR_CP;
            }
            set {
                this._DIR_CP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_COL
        /// </summary>
        [DataNames("DIR_COL")]
        public string Colonia {
            get {
                return this._DIR_COL;
            }
            set {
                this._DIR_COL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_DEL_MUN
        /// </summary>
        [DataNames("DIR_DEL_MUN")]
        public string Delegacion {
            get {
                return this._DIR_DEL_MUN;
            }
            set {
                this._DIR_DEL_MUN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_DIR
        /// </summary>
        [DataNames("DIR_DIR")]
        public string Direccion {
            get {
                return this._DIR_DIR;
            }
            set {
                this._DIR_DIR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_EST
        /// </summary>
        [DataNames("DIR_EST")]
        public string Estado {
            get {
                return this._DIR_EST;
            }
            set {
                this._DIR_EST = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_PAI
        /// </summary>
        [DataNames("DIR_PAI")]
        public string Pais {
            get {
                return this._DIR_PAI;
            }
            set {
                this._DIR_PAI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_TEL
        /// </summary>
        [DataNames("DIR_TEL")]
        public string Telefono {
            get {
                return this._DIR_TEL;
            }
            set {
                this._DIR_TEL = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        public bool Editable {
            get {
                return this.IdStatus == (int)RemisionStatusEnum.Pendiente;
            }
        }
    }
}
