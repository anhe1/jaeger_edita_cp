﻿/// purpose: modelo de vista de partidas de remisiones
/// develop: ANHE1 270920200115
using Jaeger.Domain.CP.Remisionado.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Remisionado.Entities {
    /// <summary>
    /// modelo vista de partidas de remisiones
    /// </summary>
    public class RemisionPedModelView : RemisionPedModel, IRemisionPedModel {

        public RemisionPedModelView() {

        }

        /// <summary>
        /// PEDPRD_ID
        /// </summary>
        //[DataNames("PEDPRD_ID")]
        //public int IdPedPrd { get; set; }

        /// <summary>
        /// Dir_Nom
        /// </summary>
        [DataNames("Dir_Nom")]
        public string Cliente { get; set; }

        /// <summary>
        /// PEDPRD_Nom
        /// </summary>
        [DataNames("PEDPRD_Nom")]
        public string Producto { get; set; }

        [DataNames("PedST_Sta")]
        public string Status { get; set; }

        [DataNames("PedPrd_Uni")]
        public new int Unidad { get; set; }

        [DataNames("PEDPRD_$Uni")]
        public new decimal Unitario { get; set; }
    }
}
