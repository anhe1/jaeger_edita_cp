﻿/// purpose: modelo Remision (tabla REM)
/// develop: ANHE1 27092020 0118
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Remisionado.Contracts;
using Jaeger.Domain.CP.Remisionado.ValueObjects;

namespace Jaeger.Domain.CP.Remisionado.Entities {
    ///<summary>
    /// modelo Remision (tabla REM)
    ///</summary>
    [SugarTable("rem")]
    public class RemisionModel : BasePropertyChangeImplementation, IRemisionModel {
        #region declaraciones
        private int _REM_ID;
        private int _REM_A;
        private int _REM_ST_ID;
        private int _REM_DIR_ID;
        private int _REM_DIRCON_ID;
        private DateTime? _REM_FEC;
        private DateTime? _REM_VEN;
        private string _REM_EMB;
        private decimal _REM_;
        private string _REM_USU_N;
        private DateTime? _REM_FM;
        private decimal _REM_PAG;
        private DateTime? _REM_VENCE;
        private decimal _REM_DEU;
        private int _REM_VENCEDIAS;
        private string _REM_OBS;
        private string _REM_USU_M;
        #endregion

        public RemisionModel() {
            this._REM_A = 1;
            this._REM_ST_ID = (int)RemisionStatusEnum.Pendiente;
            this._REM_FEC = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer (REM_ID)
        /// </summary>
        [DataNames("REM_ID")]
        [SugarColumn(ColumnName = "rem_id", ColumnDescription = "REM_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemision {
            get { return this._REM_ID; }
            set {
                this._REM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (REM_A)
        /// </summary>
        [DataNames("REM_A")]
        [SugarColumn(ColumnName = "rem_a", ColumnDescription = "REM_A")]
        public int Activo {
            get { return this._REM_A; }
            set {
                this._REM_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REM_ST_ID
        /// </summary>
        [DataNames("REM_ST_ID")]
        [SugarColumn(ColumnName = "rem_st_id", ColumnDescription = "REM_ST_ID")]
        public int IdStatus {
            get { return this._REM_ST_ID; }
            set {
                this._REM_ST_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (REM_DIR_ID)
        /// </summary>
        [DataNames("REM_DIR_ID")]
        [SugarColumn(ColumnName = "rem_dir_id", ColumnDescription = "REM_DIR_ID")]
        public int IdDirectorio {
            get { return this._REM_DIR_ID; }
            set {
                this._REM_DIR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (REM_DIRCON_ID)
        /// </summary>
        [DataNames("REM_DIRCON_ID")]
        [SugarColumn(ColumnName = "rem_dircon_id", ColumnDescription = "REM_DIRCON_ID")]
        public int IdContacto {
            get { return this._REM_DIRCON_ID; }
            set {
                this._REM_DIRCON_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (REM_VENCEDIAS)
        /// </summary>
        [DataNames("REM_VENCEDIAS")]
        [SugarColumn(ColumnName = "rem_vencedias", ColumnDescription = "REM_VENCEDIAS")]
        public int VenceDias {
            get { return this._REM_VENCEDIAS; }
            set {
                this._REM_VENCEDIAS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el lugar de embarque (REM_EMB)
        /// </summary>
        [DataNames("REM_EMB")]
        [SugarColumn(ColumnName = "rem_emb", ColumnDescription = "REM_EMB", Length = 100)]
        public string Embarque {
            get { return this._REM_EMB; }
            set {
                this._REM_EMB = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REM_OBS
        /// </summary>
        [DataNames("REM_OBS")]
        [SugarColumn(ColumnName = "rem_obs", ColumnDescription = "REM_OBS", Length = 100)]
        public string Nota {
            get { return this._REM_OBS; }
            set {
                this._REM_OBS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Total de la remision (REM_$)
        /// </summary>
        [DataNames("REM_$")]
        [SugarColumn(ColumnName = "rem_$", ColumnDescription = "REM_$", Length = 18, DecimalDigits = 4)]
        public decimal Total {
            get { return this._REM_; }
            set {
                this._REM_ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el monto total cobrado (REM_$PAG)
        /// </summary>
        [DataNames("REM_$PAG")]
        [SugarColumn(ColumnName = "rem_$pag", ColumnDescription = "REM_$PAG", Length = 18, DecimalDigits = 4)]
        public decimal Acumulado {
            get { return this._REM_PAG; }
            set {
                this._REM_PAG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (REM_$DEU)
        /// </summary>
        [DataNames("REM_$DEU")]
        [SugarColumn(ColumnName = "rem_$deu", ColumnDescription = "REM_$DEU")]
        public decimal Saldo {
            get { return this._REM_DEU; }
            set {
                this._REM_DEU = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (REM_VEN)
        /// </summary>
        [DataNames("REM_VEN")]
        [SugarColumn(ColumnName = "rem_ven", ColumnDescription = "REM_VEN")]
        public DateTime? REM_VEN {
            get { return this._REM_VEN; }
            set {
                this._REM_VEN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de vencimiento (REM_VENCE)
        /// </summary>
        [DataNames("REM_VENCE")]
        [SugarColumn(ColumnName = "rem_vence", ColumnDescription = "REM_VENCE")]
        public DateTime? FechaVence {
            get { 
                if (this._REM_VENCE >= new DateTime(1900, 1, 1))
                    return this._REM_VENCE;
                return null;
            }
            set {
                this._REM_VENCE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REM_FEC
        /// </summary>
        [DataNames("REM_FEC")]
        [SugarColumn(ColumnName = "rem_fec", ColumnDescription = "REM_FEC")]
        public DateTime? FechaEmision {
            get { if (this._REM_FEC >= new DateTime(1900, 1, 1))
                    return this._REM_FEC;
                return null;
            }
            set {
                this._REM_FEC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (REM_FM)
        /// </summary>
        [DataNames("REM_FM")]
        [SugarColumn(ColumnName = "rem_fm", ColumnDescription = "REM_FM")]
        public DateTime? FechaModifica {
            get { if (this._REM_FM >= new DateTime(1900, 1, 1))
                    return this._REM_FM;
                return null;
            }
            set {
                this._REM_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro (REM_USU_N)
        /// </summary>
        [DataNames("REM_USU_N")]
        [SugarColumn(ColumnName = "rem_usu_n", ColumnDescription = "REM_USU_N", Length = 7)]
        public string Creo {
            get { return this._REM_USU_N; }
            set {
                this._REM_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro (REM_USU_M)
        /// </summary>
        [DataNames("REM_USU_M")]
        [SugarColumn(ColumnName = "rem_usu_m", ColumnDescription = "REM_USU_M", Length = 7)]
        public string Modifica {
            get { return this._REM_USU_M; }
            set {
                this._REM_USU_M = value;
                this.OnPropertyChanged();
            }
        }
    }
}
