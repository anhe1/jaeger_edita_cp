﻿/// purpose: modelo de vista de remision con partidas (REM-REMPED)
/// develop: ANHE1 27092020 0118
using Jaeger.Domain.CP.Remisionado.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Remisionado.Entities {
    /// <summary>
    /// modelo de vista de remision con partidas (REM-REMPED)
    /// </summary>
    public class RemisionDetailModelView : RemisionModelView, IRemisionModel {
        private BindingList<RemisionPedModelView> remisionPeds;

        public RemisionDetailModelView() {
            this.remisionPeds = new BindingList<RemisionPedModelView>();
        }

        public BindingList<RemisionPedModelView> Partidas {
            get { return this.remisionPeds; }
            set { this.remisionPeds = value;
                this.OnPropertyChanged();
            }
        }
    }
}
