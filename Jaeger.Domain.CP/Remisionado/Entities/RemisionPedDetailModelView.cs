﻿/// purpose: vista de remision-partidas
/// develop: ANHE1 27/09/20200117
using Jaeger.Domain.CP.Remisionado.Contracts;
using Jaeger.Domain.CP.Remisionado.ValueObjects;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Jaeger.Domain.CP.Remisionado.Entities {
    /// <summary>
    /// vista de remision-partidas
    /// </summary>
    public class RemisionPedDetailModelView : RemisionPedModel, IRemisionPedModel, IRemisionModel {
        #region directorio 

        [DataNames("DIR_cla")]
        public string Clave { get; set; }

        [DataNames("DIR_Nom")]
        public string Cliente { get; set; }

        [DataNames("DIRCON_con")]
        public string Contacto { get; set; }

        #endregion

        #region datos generales de la remision

        /// <summary>
        /// obtener o establecer REM_$
        /// </summary>
        [DataNames("REM_$")]
        [SugarColumn(ColumnName = "rem_$", ColumnDescription = "REM_$", Length = 18, DecimalDigits = 4)]
        public decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer el monto total cobrado (REM_$PAG)
        /// </summary>
        [DataNames("REM_$PAG")]
        [SugarColumn(ColumnName = "rem_$pag", ColumnDescription = "REM_$PAG", Length = 18, DecimalDigits = 4)]
        public new decimal Acumulado { get; set; }

        /// <summary>
        /// obtener o establecer REM_$DEU
        /// </summary>
        [DataNames("REM_$DEU")]
        [SugarColumn(ColumnName = "rem_$deu", ColumnDescription = "REM_$DEU")]
        public decimal Saldo { get; set; }

        /// <summary>
        /// obtener o establecer REM_DIR_ID
        /// </summary>
        [DataNames("REM_DIR_ID")]
        [SugarColumn(ColumnName = "rem_dir_id", ColumnDescription = "REM_DIR_ID")]
        public int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer REM_DIRCON_ID
        /// </summary>
        [DataNames("REM_DIRCON_ID")]
        [SugarColumn(ColumnName = "rem_dircon_id", ColumnDescription = "REM_DIRCON_ID")]
        public int IdContacto { get; set; }

        /// <summary>
        /// obtener o establecer REM_EMB
        /// </summary>
        [DataNames("REM_EMB")]
        [SugarColumn(ColumnName = "rem_emb", ColumnDescription = "REM_EMB", Length = 100)]
        public string Embarque { get; set; }

        /// <summary>
        /// obtener o establecer REM_FEC
        /// </summary>
        [DataNames("REM_FEC")]
        [SugarColumn(ColumnName = "rem_fec", ColumnDescription = "REM_FEC")]
        public DateTime? FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer REM_ST_ID
        /// </summary>
        [DataNames("REM_ST_ID")]
        [SugarColumn(ColumnName = "rem_st_id", ColumnDescription = "REM_ST_ID")]
        public int IdStatus { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string StatusRemision {
            get {
                return Enum.GetName(typeof(RemisionStatusEnum), this.IdStatus);
            }
        }

        /// <summary>
        /// obtener o establecer REM_VEN
        /// </summary>
        [DataNames("REM_VEN")]
        [SugarColumn(ColumnName = "rem_ven", ColumnDescription = "REM_VEN")]
        public DateTime? REM_VEN { get; set; }

        /// <summary>
        /// obtener o establecer REM_FEC
        /// </summary>
        [DataNames("REM_FEC")]
        [SugarColumn(ColumnName = "rem_fec", ColumnDescription = "REM_FEC")]
        public DateTime? FechaVence { get; set; }

        /// <summary>
        /// obtener o establecer REM_VENCEDIAS
        /// </summary>
        [DataNames("REM_VENCEDIAS")]
        [SugarColumn(ColumnName = "rem_vencedias", ColumnDescription = "REM_VENCEDIAS")]

        public int VenceDias { get; set; }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("REM_OBS")]
        [SugarColumn(ColumnName = "rem_obs", ColumnDescription = "REM_OBS", Length = 100)]
        public string RemNota { get; set; }
        
        #endregion

        #region datos de la orden de produccion

        /// <summary>
        /// PEDPRD_Nom
        /// </summary>
        [DataNames("PEDPRD_Nom")]
        public string Producto { get; set; }

        /// <summary>
        /// obtener o establecer el status de la orden de produccion
        /// </summary>
        [DataNames("PedST_Sta")]
        public string Status { get; set; }

        public string PO {
            get {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(this.Producto.ToLower());
                string str1 = Regex.Replace(stringBuilder.ToString(), "[\r\n\t]", "");
                Match match = Regex.Match(str1, @"(pedido.*?no|no|ped|pedido)[.:\s]+\b(\d{9,11})\b", RegexOptions.IgnoreCase);
                var uuidFound = (!match.Success || !(match.Value != "") ? "" : match.Value.ToString().ToUpper());
                Match match1 = Regex.Match(uuidFound, @"\d+"); 
                return match1.Value.ToString();
            }
        }

        #endregion
    }
}
