﻿/// purpose: modelo de partida o concepto de remision (REMPED)
/// develop: ANHE1 270920200115
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Remisionado.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Remisionado.Entities {
    ///<summary>
    /// modelo de partida o concepto de remision (REMPED)
    ///</summary>
    [SugarTable("REMPED")]
    public class RemisionPedModel : BasePropertyChangeImplementation, IRemisionPedModel {
        #region declaraciones
        private int index;
        private int activo;
        private int idRemision;
        private int idPedPrd;
        private decimal cantidad;
        private int unidad;
        private decimal unitario;
        private decimal _REMPED_;
        private decimal _REMPED_PAG;
        private string notas;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private int _REMPED_USU_FM;
        private int _REMPED_ALMX_ID;
        private int _REMPED_PRD_CUENTA;
        #endregion

        /// <summary>
        /// obtener o establecer REMPED_ID (indice de la tabla)
        /// </summary>
        [DataNames("REMPED_ID")]
        [SugarColumn(ColumnName = "remped_id", ColumnDescription = "REMPED_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemPed {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (REMPED_A)
        /// </summary>
        [DataNames("REMPED_A")]
        [SugarColumn(ColumnName = "remped_a", ColumnDescription = "REMPED_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con remision (REMPED_REM_ID)
        /// </summary>
        [DataNames("REMPED_REM_ID")]
        [SugarColumn(ColumnName = "remped_rem_id", ColumnDescription = "REMPED_REM_ID")]
        public int IdRemision {
            get { return this.idRemision; }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REMPED_ALMX_ID
        /// </summary>
        [DataNames("REMPED_ALMX_ID")]
        [SugarColumn(ColumnName = "remped_almx_id", ColumnDescription = "REMPED_ALMX_ID")]
        public int REMPED_ALMX_ID {
            get { return this._REMPED_ALMX_ID; }
            set {
                this._REMPED_ALMX_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de orden de produccion (REMPED_PEDPRD_ID)
        /// </summary>
        [DataNames("REMPED_PEDPRD_ID")]
        [SugarColumn(ColumnName = "remped_pedprd_id", ColumnDescription = "REMPED_PEDPRD_ID")]
        public int IdPedPrd {
            get { return this.idPedPrd; }
            set {
                this.idPedPrd = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REMPED_PRD_CUENTA
        /// </summary>
        [DataNames("REMPED_PRD_CUENTA")]
        [SugarColumn(ColumnName = "remped_prd_cuenta", ColumnDescription = "REMPED_PRD_CUENTA")]
        public int REMPED_PRD_CUENTA {
            get { return this._REMPED_PRD_CUENTA; }
            set {
                this._REMPED_PRD_CUENTA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REMPED_USU_FM
        /// </summary>
        [DataNames("REMPED_USU_FM")]
        [SugarColumn(ColumnName = "remped_usu_fm", ColumnDescription = "REMPED_USU_FM")]
        public int REMPED_USU_FM {
            get { return this._REMPED_USU_FM; }
            set {
                this._REMPED_USU_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REMPED_UNI
        /// </summary>
        [DataNames("REMPED_UNI")]
        [SugarColumn(ColumnName = "remped_uni", ColumnDescription = "REMPED_UNI")]
        public int Unidad {
            get { return this.unidad; }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REMPED_CAN
        /// </summary>
        [DataNames("REMPED_CAN")]
        [SugarColumn(ColumnName = "remped_can", ColumnDescription = "REMPED_CAN", Length = 18, DecimalDigits = 4)]
        public decimal Cantidad {
            get { return this.cantidad; }
            set {
                this.cantidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REMPED_$_UNI
        /// </summary>
        [DataNames("REMPED_$_UNI")]
        [SugarColumn(ColumnName = "remped_$_uni", ColumnDescription = "REMPED_$_UNI", Length = 18, DecimalDigits = 4)]
        public decimal Unitario {
            get { return this.unitario; }
            set {
                this.unitario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REMPED_$
        /// </summary>
        [DataNames("REMPED_$")]
        [SugarColumn(ColumnName = "remped_$", ColumnDescription = "REMPED_$", Length = 18, DecimalDigits = 4)]
        public decimal Importe {
            get { return this._REMPED_; }
            set {
                this._REMPED_ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe acumulado de pagos (REMPED_$PAG)
        /// </summary>
        [DataNames("REMPED_$PAG")]
        [SugarColumn(ColumnName = "REMPED_$PAG", ColumnDescription = "REMPED_$PAG", Length = 18, DecimalDigits = 4)]
        public decimal Acumulado {
            get { return this._REMPED_PAG; }
            set {
                this._REMPED_PAG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer REMPED_OBS
        /// </summary>
        [DataNames("REMPED_OBS")]
        [SugarColumn(ColumnName = "remped_obs", ColumnDescription = "REMPED_OBS", Length = 50)]
        public string Nota {
            get { return this.notas; }
            set {
                this.notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (REMPED_FM)
        /// </summary>
        [DataNames("REMPED_FM")]
        [SugarColumn(ColumnName = "remped_fm", ColumnDescription = "REMPED_FM")]
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica >= new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro REMPED_USU_N
        /// </summary>
        [DataNames("REMPED_USU_N")]
        [SugarColumn(ColumnName = "remped_usu_n", ColumnDescription = "REMPED_USU_N", Length = 7)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro REMPED_USU_M
        /// </summary>
        [DataNames("REMPED_USU_M")]
        [SugarColumn(ColumnName = "remped_usu_m", ColumnDescription = "REMPED_USU_M", Length = 7)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
