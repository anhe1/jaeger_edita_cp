﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Almacen.Entities;

namespace Jaeger.Domain.CP.Almacen.Contracts {
    public interface ISqlAlmacenMovimientoRepository : IGenericRepository<AlmacenMovModel> {
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
    }
}
