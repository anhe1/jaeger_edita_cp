﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Almacen.Entities {
    /// <summary>
    /// movimiento de almacen de materia prima
    /// </summary>
    public class AlmacenMovModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _ALMMOV_ID;
        private int _ALMMOV_A;
        private int _ALMMOV_OC_ID;
        private int _ALMMOV_ENT_ID;
        private int _ALMMOV_OCC_ID;
        private int _ALMMOV_ALM_ID_OLD;
        private int _ALMMOV_OP_ID;
        private int _ALMMOV_MP_ID;
        private int _ALMMOV_CEN_ID;
        private int _ALMMOV_ALMX_ID;
        private decimal _ALMMOV_UNI;
        private decimal _ALMMOV_SOL;
        private decimal _ALMMOV_PED;
        private decimal _ALMMOV_ENT;
        private decimal _ALMMOV_SAL;
        private int _ALMMOV_DOC;
        private string _ALMMOV_USU_REC;
        private int _ALMMOV_DIR_ID;
        private int _ALMMOV_ST;
        private DateTime _ALMMOV_FREQ;
        private DateTime _ALMMOV_USU_F;
        private string _ALMMOV_USU_N;
        private string _ALMMOV_USU_M;
        private DateTime _ALMMOV_FM;
        private int _ALMMOV_AUT_ID;
        private decimal _ALMMOV_EXI;
        private decimal _ALMMOV_EXIF;
        #endregion

        #region propiedades
        [DataNames("_ALMMOV_ID")]
        public int ALMMOV_ID {
            get { return this._ALMMOV_ID; }
            set {
                this._ALMMOV_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_A")]
        public int Activo {
            get { return this._ALMMOV_A; }
            set {
                this._ALMMOV_A = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ALMMOV_OC_ID")]
        public int ALMMOV_OC_ID {
            get { return this._ALMMOV_OC_ID; }
            set {
                this._ALMMOV_OC_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_ENT_ID")]
        public int ALMMOV_ENT_ID {
            get { return this._ALMMOV_ENT_ID; }
            set {
                this._ALMMOV_ENT_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_OCC_ID")]
        public int ALMMOV_OCC_ID {
            get { return this._ALMMOV_OCC_ID; }
            set {
                this._ALMMOV_OCC_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_ALM_ID")]
        public int ALMMOV_ALM_ID_OLD {
            get { return this._ALMMOV_ALM_ID_OLD; }
            set {
                this._ALMMOV_ALM_ID_OLD = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_OP_ID")]
        public int ALMMOV_OP_ID {
            get { return this._ALMMOV_OP_ID; }
            set {
                this._ALMMOV_OP_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_MP_ID ")]
        public int ALMMOV_MP_ID {
            get { return this._ALMMOV_MP_ID; }
            set {
                this._ALMMOV_MP_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_CEN_ID")]
        public int ALMMOV_CEN_ID {
            get { return this._ALMMOV_CEN_ID; }
            set {
                this._ALMMOV_CEN_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_ALMX_ID")]
        public int ALMMOV_ALMX_ID {
            get { return this._ALMMOV_ALMX_ID; }
            set {
                this._ALMMOV_ALMX_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_UNI")]
        public decimal ALMMOV_UNI {
            get { return this._ALMMOV_UNI; }
            set {
                this._ALMMOV_UNI = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_SOL")]
        public decimal ALMMOV_SOL {
            get { return this._ALMMOV_SOL; }
            set {
                this._ALMMOV_SOL = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_PED")]
        public decimal ALMMOV_PED {
            get { return this._ALMMOV_PED; }
            set {
                this._ALMMOV_PED = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_ENT")]
        public decimal ALMMOV_ENT {
            get { return this._ALMMOV_ENT; }
            set {
                this._ALMMOV_ENT = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_SAL")]
        public decimal ALMMOV_SAL {
            get { return this._ALMMOV_SAL; }
            set {
                this._ALMMOV_SAL = value;
                this.OnPropertyChanged();
            }
        }


        [DataNames("ALMMOV_DOC")]
        public int ALMMOV_DOC {
            get { return this._ALMMOV_DOC; }
            set {
                this._ALMMOV_DOC = value; ;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_USU_REC")]
        public string ALMMOV_USU_REC {
            get { return this._ALMMOV_USU_REC; }
            set {
                this._ALMMOV_USU_REC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_DIR_ID")]
        public int ALMMOV_DIR_ID {
            get { return this._ALMMOV_DIR_ID; }
            set {
                this._ALMMOV_DIR_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_ST")]
        public int ALMMOV_ST {
            get { return this._ALMMOV_ST; }
            set {
                this._ALMMOV_ST = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_FREQ")]
        public DateTime ALMMOV_FREQ {
            get { return this._ALMMOV_FREQ; }
            set {
                this._ALMMOV_FREQ = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_USU_F")]
        public DateTime ALMMOV_USU_F {
            get { return this._ALMMOV_USU_F; }
            set {
                this._ALMMOV_USU_F = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_USU_N")]
        public string ALMMOV_USU_N {
            get { return this._ALMMOV_USU_N; }
            set {
                this._ALMMOV_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_USU_M")]
        public string ALMMOV_USU_M {
            get { return this._ALMMOV_USU_M; }
            set {
                this._ALMMOV_USU_M = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_FM")]
        public DateTime ALMMOV_FM {
            get { return this._ALMMOV_FM; }
            set {
                this._ALMMOV_FM = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_AUT_ID")]
        public int ALMMOV_AUT_ID {
            get { return this._ALMMOV_AUT_ID; }
            set {
                this._ALMMOV_AUT_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_EXI")]
        public decimal ALMMOV_EXI {
            get { return this._ALMMOV_EXI; }
            set {
                this._ALMMOV_EXI = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_EXIF")]
        public decimal ALMMOV_REQ {
            get { return this._ALMMOV_EXIF; }
            set {
                this._ALMMOV_EXIF = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMMOV_EXIF")]
        public decimal ALMMOV_EXIF {
            get { return this._ALMMOV_EXIF; }
            set {
                this._ALMMOV_EXIF = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
