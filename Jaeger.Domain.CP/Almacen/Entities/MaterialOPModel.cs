﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Almacen.Entities {
    public class MaterialOPModel : AlmacenMovModel {
        #region declaraciones
        private int _MatOP_ID;
        private int _MatOP_A;
        private decimal _MatOP_Sol;
        private string _MPUNI_NOM;
        private string _Com3_N1;
        private string _Com3_N2;
        private string _Com3_N3;
        private string _MPMar_Nom;
        private string _MP4_Mod;
        private string _MP4_nom;
        private decimal _MP4_TCA;
        private decimal _MP4_TCB;
        private decimal _MP4_TCC;
        private decimal _MP4_val;
        #endregion

        public MaterialOPModel() {

        }

        [DataNames("MATOP_ID")]
        public int IdOrden {
            get { return this._MatOP_ID; }
            set {
                this._MatOP_ID = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MATOP_A")]
        public new int Activo {
            get { return this._MatOP_A; }
            set {
                this._MatOP_A = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MATOP_SOL")]
        public decimal MatOP_Sol {
            get { return this._MatOP_Sol; }
            set {
                this._MatOP_Sol = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MPUNI_NOM")]
        public string Unidad {
            get { return this._MPUNI_NOM; }
            set {
                this._MPUNI_NOM = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("COM3_N1")]
        public string Cla1 {
            get { return this._Com3_N1; }
            set {
                this._Com3_N1 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("COM3_N2")]
        public string Cla2 {
            get { return this._Com3_N2; }
            set {
                this._Com3_N2 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("COM3_N3")]
        public string Cla3 {
            get { return this._Com3_N3; }
            set {
                this._Com3_N3 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MPMAR_NOM")]
        public string Marca {
            get { return this._MPMar_Nom; }
            set {
                this._MPMar_Nom = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MP4_MOD")]
        public string Modelo {
            get { return this._MP4_Mod; }
            set {
                this._MP4_Mod = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MP4_NOM")]
        public string Especificacion {
            get { return this._MP4_nom; }
            set {
                this._MP4_nom = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MP4_TCA")]
        public decimal Largo {
            get { return this._MP4_TCA; }
            set {
                this._MP4_TCA = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MP4_TCB")]
        public decimal Ancho {
            get { return this._MP4_TCB; }
            set {
                this._MP4_TCB = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MP4_TCC")]
        public decimal Calibre {
            get { return this._MP4_TCC; }
            set {
                this._MP4_TCC = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MP4_VAL")]
        public decimal Unitario {
            get { return this._MP4_val; }
            set {
                this._MP4_val = value;
                this.OnPropertyChanged();
            }
        }
    }
}
