﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.CP.Presupuesto.ValueObjects;

namespace Jaeger.Domain.CP.Presupuesto.Builder {
    public class CotizacionQueryBuilder : ConditionalBuilder, IConditionalBuilder, ICotizacionQueryBuilder, ICotizacionYearQueryBuilder, ICotizacionMonthQueryBuilder, ICotizacionStatusQueryBuilder {
        public CotizacionQueryBuilder() : base() {

        }

        public ICotizacionYearQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("EXTRACT(YEAR FROM COT_FEC_SOL)", year.ToString()));
            return this;
        }

        public ICotizacionMonthQueryBuilder Month(int month) {
            if (month > 0) this._Conditionals.Add(new Conditional("EXTRACT(MONTH FROM COT_FEC_SOL)", month.ToString()));
            return this;
        }

        public ICotizacionStatusQueryBuilder ByStatus(CotizacionStatusEnum status) {
            var idStatus = (int)status;
            this._Conditionals.Add(new Conditional("COT_COTSTA_ID", idStatus.ToString()));
            return this;
        }
    }
}
