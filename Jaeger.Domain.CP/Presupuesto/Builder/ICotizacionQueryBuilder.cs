﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.CP.Presupuesto.ValueObjects;

namespace Jaeger.Domain.CP.Presupuesto.Builder {
    public interface ICotizacionQueryBuilder : IConditionalBuilder {
        ICotizacionYearQueryBuilder Year(int year);
    }

    public interface ICotizacionYearQueryBuilder : IConditionalBuilder {
        ICotizacionMonthQueryBuilder Month(int month);
    }

    public interface ICotizacionMonthQueryBuilder : IConditionalBuilder {
        ICotizacionStatusQueryBuilder ByStatus(CotizacionStatusEnum status);
    }

    public interface ICotizacionStatusQueryBuilder : IConditionalBuilder {

    }
}
