﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface IPrdMpModel {
        int Activo {
            get; set;
        }
        DateTime? FechaModifica {
            get; set;
        }
        int IdPrdMP {
            get; set;
        }
        decimal Factor {
            get; set;
        }
        int PRDMP_COM_ID {
            get; set;
        }
        int PRDMP_COT_ID {
            get; set;
        }
        int PRDMP_FYV {
            get; set;
        }
        int PRDMP_LIB {
            get; set;
        }
        int PRDMP_MP_ID {
            get; set;
        }
        int PRDMP_MP4_ID {
            get; set;
        }
        decimal PRDMP_MPA {
            get; set;
        }
        decimal PRDMP_MPB {
            get; set;
        }
        decimal PRDMP_MRA {
            get; set;
        }
        decimal PRDMP_MRB {
            get; set;
        }
        int Repetir {
            get; set;
        }
        int IdPrdPro {
            get; set;
        }
        decimal PRDMP_TPA {
            get; set;
        }
        decimal PRDMP_TPB {
            get; set;
        }
        decimal PRDMP_TPC {
            get; set;
        }
        decimal PRDMP_TRA {
            get; set;
        }
        decimal PRDMP_TRB {
            get; set;
        }
        decimal PRDMP_TRC {
            get; set;
        }
        int PRDMP_USU_FM {
            get; set;
        }
    }
}
