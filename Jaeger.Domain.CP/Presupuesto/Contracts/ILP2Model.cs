﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ILP2Model {
        int Activo { get; set; }
        DateTime? FechaModifica { get; set; }
        int IdLP2 { get; set; }
        int LP2_COM_ID { get; set; }
        int LP2_COM1_ID { get; set; }
        int LP2_LC1_ID { get; set; }
        int LP2_LP1_ID { get; set; }
    }
}
