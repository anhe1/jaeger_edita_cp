﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// 
    /// </summary>
    public interface ISqlCotizacionRepository : IGenericRepository<CotizacionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener cotizacion o presupuesto completo
        /// </summary>
        //CotizacionDetailModel GetCotizacion(int index);
        CotizacionDetailModelView GetCotizacion(int index);

        /// <summary>
        /// obtener listado de la vista de presupuestos y cotizaciones por rangos de fecha o periodo
        /// </summary>
        /// <param name="startDate">rango inicial, si el segundo parametro es nulo se considera como un periodo</param>
        /// <param name="endDate">fin del rango</param>
        /// <param name="startStatus">rango inicial del status, si el siguiente parametro es nulo entonces se cosidera un solo status</param>
        /// <param name="endStatus">fin del rango de status</param>
        /// <returns>Vista de cotizacion o presupuestos</returns>
        //IEnumerable<CotizacionModelView> GetList(DateTime startDate, DateTime? endDate = null, int? startStatus = null, int? endStatus = null);

        void Crear(CotizacionDetailModel item);

        /// <summary>
        /// almacenar cotizacion
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        CotizacionDetailModelView Save(CotizacionDetailModelView item);

        CotizacionDetailModelView Calcular(CotizacionDetailModelView item);
    }
}