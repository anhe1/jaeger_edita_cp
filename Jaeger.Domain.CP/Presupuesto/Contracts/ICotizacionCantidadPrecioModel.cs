﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ICotizacionCantidadPrecioModel {
        int Activo {
            get; set;
        }
        int Cantidad {
            get; set;
        }
        decimal CostoDirecto {
            get; set;
        }
        decimal CostoIndirecto {
            get; set;
        }
        decimal COTPRD_CEN {
            get; set;
        }
        decimal COTPRD_CNS {
            get; set;
        }
        decimal COTPRD_MP {
            get; set;
        }
        int? COTPRD_PRD_ID {
            get; set;
        }
        decimal COTPRD_PRO {
            get; set;
        }
        decimal TiempoEstimado {
            get; set;
        }
        string Descripcion {
            get; set;
        }
        DateTime? FechaModifica {
            get; set;
        }
        decimal Fijo {
            get; set;
        }
        decimal GastoAdmon {
            get; set;
        }
        decimal GastoFin {
            get; set;
        }
        decimal GastoVenta {
            get; set;
        }
        int IdCotPrd {
            get; set;
        }
        int IdCotizacion {
            get; set;
        }
        string Modelo {
            get; set;
        }
        int Secuencia {
            get; set;
        }
        decimal SubTotal2 {
            get; set;
        }
        decimal Total {
            get; set;
        }
        decimal Unidad {
            get; set;
        }
        decimal Unitario {
            get; set;
        }
        decimal Utilidad {
            get; set;
        }
    }
}