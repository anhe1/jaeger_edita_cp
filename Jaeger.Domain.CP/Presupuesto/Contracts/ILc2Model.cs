﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ILc2Model {
        int Id { get; set; }
        bool Activo { get; set; }
        int IdCom { get; set; }
        int IdLC1 { get; set; }
        int Secuencia { get; set; }
        string Descripcion { get; set; }
        DateTime? FechaModifica { get; set; }
    }
}
