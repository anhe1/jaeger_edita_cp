﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ICotMatModel {
        int Activo {
            get; set;
        }
        decimal Consumibles {
            get; set;
        }
        decimal MateriaPrima {
            get; set;
        }
        decimal FactorConversion {
            get; set;
        }
        decimal Cantidad {
            get; set;
        }
        int COTMAT_COM_ID {
            get; set;
        }
        int COTMAT_COTPRD_ID {
            get; set;
        }
        int COTMAT_COTPRO_ID {
            get; set;
        }
        DateTime? FechaModifica {
            get; set;
        }
        int COTMAT_FYV {
            get; set;
        }
        int IdCotMat {
            get; set;
        }
        int COTMAT_LIB {
            get; set;
        }
        int COTMAT_MP_ID {
            get; set;
        }
        int COTMAT_MP4_ID {
            get; set;
        }
        decimal COTMAT_MPA {
            get; set;
        }
        decimal COTMAT_MPB {
            get; set;
        }
        decimal COTMAT_MRA {
            get; set;
        }
        decimal COTMAT_MRB {
            get; set;
        }
        int Repetir {
            get; set;
        }
        int COTMAT_PRDMAT_ID {
            get; set;
        }
        int COTMAT_PRDMAT_ID3 {
            get; set;
        }
        decimal COTMAT_TPA {
            get; set;
        }
        decimal COTMAT_TPB {
            get; set;
        }
        decimal COTMAT_TPC {
            get; set;
        }
        decimal COTMAT_TRA {
            get; set;
        }
        decimal COTMAT_TRB {
            get; set;
        }
        decimal COTMAT_TRC {
            get; set;
        }
        decimal COTMAT_UNI {
            get; set;
        }
        int COTMAT_USU_FM {
            get; set;
        }
    }
}