﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ILineaProduccionModel {
        int IdCom { get; set; }
        bool Activo { get; set; }
        decimal CantidadProceso { get; set; }
        DateTime? FechaModifica { get; set; }
        int IdCom2 { get; set; }
        int IdLinea { get; set; }
        int IdProceso { get; set; }
        int Modifica { get; set; }
        int Secuencia { get; set; }
        decimal VecesProceso { get; set; }
    }
}
