﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    ///<summary>
    /// repositorio de SubProcesos - Puestos (EDP4PUE)
    ///</summary>
    public interface ISqlEdp4PueRepository : IGenericRepository<Edp4PueModel> {

    }
}