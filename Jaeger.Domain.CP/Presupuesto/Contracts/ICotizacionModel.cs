﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// presupuesto o cotizacion (COT)
    /// </summary>
    public interface ICotizacionModel {
        bool Activo {
            get; set;
        }
        string CondicionEntrega {
            get; set;
        }
        string CondicionPago {
            get; set;
        }
        DateTime? COT_FEC_CAD {
            get; set;
        }
        DateTime? FechaCotizacion {
            get; set;
        }
        DateTime? FechaRequerido {
            get; set;
        }
        int COT_LP_ID {
            get; set;
        }
        string COT_PRD {
            get; set;
        }
        int COT_PRD_ID {
            get; set;
        }
        string Creo {
            get; set;
        }
        DateTime? FechaModifica {
            get; set;
        }
        DateTime FechaSolicitud {
            get; set;
        }
        int IdCliente {
            get; set;
        }
        int IdCom {
            get; set;
        }
        int IdContacto {
            get; set;
        }
        int IdCotizacion {
            get; set;
        }
        int IdStatus {
            get; set;
        }
        int IdVendedor {
            get; set;
        }
        string Modelo {
            get; set;
        }
        string Modifica {
            get; set;
        }
        string Nota {
            get; set;
        }
    }
}