﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// Requisitos (LC2)
    /// </summary>
    public interface ISqlLc2Repository : IGenericRepository<Lc2Model> {
        IEnumerable<Lc2DetailModel> GetList(int[] indexs);
    }
}
