﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// repositorio materia prima en linea de componente (LCMP)
    /// </summary>
    public interface ISqlLcMpRepository : IGenericRepository<LcMpModel> {
        IEnumerable<LcMpDetailModel> GetList(int[] indexs);

        IEnumerable<LcMpModelView> GetListView(int[] indexs);
    }
}
