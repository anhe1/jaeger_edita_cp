﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ICotProModel {
        decimal Centro1 {
            get; set;
        }
        decimal Consumibles {
            get; set;
        }
        decimal MateriaPrima1 {
            get; set;
        }
        decimal ManoObra {
            get; set;
        }
        int COTPRO_A {
            get; set;
        }
        int COTPRO_CAN_COM {
            get; set;
        }
        decimal CantidadProceso {
            get; set;
        }
        int COTPRO_CAN_PRO {
            get; set;
        }
        int COTPRO_CAN_PROS {
            get; set;
        }
        int CantidadTotal {
            get; set;
        }
        decimal COTPRO_CAN_VP {
            get; set;
        }
        int COTPRO_COTCOM_ID {
            get; set;
        }
        int COTPRO_COTPRD_ID {
            get; set;
        }
        DateTime? COTPRO_FM {
            get; set;
        }
        int COTPRO_ID {
            get; set;
        }
        int OperadoresEstimados {
            get; set;
        }
        int COTPRO_PRDPRO_ID {
            get; set;
        }
        int COTPRO_PRO_ID {
            get; set;
        }
        int Secuencia {
            get; set;
        }
        decimal COTPRO_ST {
            get; set;
        }
        int Minutos {
            get; set;
        }
        int COTPRO_USU_FM {
            get; set;
        }
    }
}