﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ILcMpModel {
        int Activo { get; set; }
        DateTime? FechaModifica { get; set; }
        decimal Factor { get; set; }
        int LCMP_COM_ID { get; set; }
        int LCMP_FYV { get; set; }
        int IdLCMP { get; set; }
        int LCMP_LIB { get; set; }
        int LCMP_MP_ID { get; set; }
        int LCMP_MP4_ID { get; set; }
        decimal LCMP_MPA { get; set; }
        decimal LCMP_MPB { get; set; }
        decimal LCMP_MRA { get; set; }
        decimal LCMP_MRB { get; set; }
        int Repetir { get; set; }
        int LCMP_PRO_ID { get; set; }
        decimal LCMP_TPA { get; set; }
        decimal LCMP_TPB { get; set; }
        decimal LCMP_TPC { get; set; }
        decimal LCMP_TRA { get; set; }
        decimal LCMP_TRB { get; set; }
        decimal LCMP_TRC { get; set; }
        int LCMP_USU_FM { get; set; }
    }
}
