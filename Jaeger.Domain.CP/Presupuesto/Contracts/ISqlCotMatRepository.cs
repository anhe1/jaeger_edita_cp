﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ISqlCotMatRepository : IGenericRepository<CotMatModel> {
        IEnumerable<CotMatDetailModel> GetList(int[] index);

        IEnumerable<CotMatDetailModelView> GetList1(int[] index);
    }
}