﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ISqlPrdProRepository : IGenericRepository<PrdProModel> {
        IEnumerable<PrdProDetailModel> GetList(int[] indexs);

        IEnumerable<PrdProDetailModelView> GetList(int[] indexs, bool active);

        PrdProDetailModelView Save(PrdProDetailModelView item);
    }
}