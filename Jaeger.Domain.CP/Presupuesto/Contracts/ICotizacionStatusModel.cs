﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ICotizacionStatusModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla (COTSTA_ID)
        /// </summary>
        int IdCotStatus { get; set; }

        /// <summary>
        /// obtener o establecer el registro activo (COTSTA_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion o nombre del status (COTSTA_NOM)
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia del status (COTSTA_SEC)
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer el usuario que modifica (COTSTA_USU_FM)
        /// </summary>
        int COTSTA_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}