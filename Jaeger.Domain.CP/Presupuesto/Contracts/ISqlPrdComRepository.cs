﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// repositorio de componentes y ensambles
    /// </summary>
    public interface ISqlPrdComRepository : IGenericRepository<PrdComModel> {
        /// <summary>
        /// obtener listado de componentes o esambles de una cotizacion o presupuesto
        /// </summary>
        /// <param name="index">indice de relacion del presupuesto o cotizacion</param>
        /// <returns>IEnumerable<PrdComDetailModel></returns>
        IEnumerable<PrdComDetailModel> GetList(int index);

        IEnumerable<PrdComDetailModelView> GetList(int index, bool active);

        /// <summary>
        /// linea de produccion
        /// </summary>
        PrdComDetailModelView Save(PrdComDetailModelView item);
    }
}