﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    ///<summary>
    /// Línea de Producción (prdpro)
    ///</summary>
    public interface IPrdProModel {
        int Activo {
            get; set;
        }
        decimal PRDPRO_CAN_CP {
            get; set;
        }
        decimal PRDPRO_CAN_VP {
            get; set;
        }
        int PRDPRO_COT_ID {
            get; set;
        }
        DateTime? PRDPRO_FM {
            get; set;
        }
        int PRDPRO_ID {
            get; set;
        }
        int PRDPRO_PRDCOM_ID {
            get; set;
        }
        int PRDPRO_PRO_ID {
            get; set;
        }
        int PRDPRO_USU_FM {
            get; set;
        }
        int Secuencia {
            get; set;
        }
    }
}