﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ISqlPrdMpRepository : IGenericRepository<PrdMpModel> {
        /// <summary>
        /// Materia Prima (PRDMP )
        /// </summary>
        IEnumerable<PrdMpDetailModel> GetList(int[] index);

        IEnumerable<PrdMpDetailModelView> GetList1(int[] index);
    }
}