﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// cantidades y precios (COTPRD)
    /// </summary>
    public interface ISqlCotizacionCantidadRepository : IGenericRepository<CotizacionCantidadPrecioModel> {
        /// <summary>
        /// obtener listado de detalle de cantidades y precios de una cotizacion o presupuesto
        /// </summary>
        /// <param name="index">indice de la catizacion o presupuesto</param>
        /// <returns>IEnumerable<CotizacionCantidadPrecioDetailModel></returns>
        IEnumerable<CotizacionCantidadPrecioDetailModel> GetList(int index);

        CotizacionCantidadPrecioDetailModel Save(CotizacionCantidadPrecioDetailModel item);
    }
}