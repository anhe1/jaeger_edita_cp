﻿using System;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface IPrdComModel {
        int Activo {
            get; set;
        }
        decimal Cantidad {
            get; set;
        }
        int CantidadInicial {
            get; set;
        }
        string Denominacion {
            get; set;
        }
        DateTime? FechaModifica {
            get; set;
        }
        int Id {
            get; set;
        }
        int IdCotizacion {
            get; set;
        }
        int PRDCOM_COM_ID {
            get; set;
        }
        int PRDCOM_LC_ID {
            get; set;
        }
        int PRDCOM_PRD_ID {
            get; set;
        }
        int PRDCOM_USU_FM {
            get; set;
        }
        int Secuencia {
            get; set;
        }
    }
}