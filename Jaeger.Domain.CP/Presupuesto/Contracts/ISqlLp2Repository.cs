﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ISqlLp2Repository : IGenericRepository<LP2Model> {
        /// <summary>
        /// obtener listado de subproductos por indice de relacion (LP2_Com1_ID)
        /// </summary>
        /// <param name="indexs">array de indices LP2_Com1_ID</param>
        IEnumerable<LP2DetailModel> GetList(int[] indexs);

        /// <summary>
        /// obtener vista de subproductos por indice de relacion (LP2_Com1_ID)
        /// </summary>
        /// <param name="indexs">array de indices LP2_Com1_ID</param>
        IEnumerable<LP2ModelView> GetListView(int[] indexs);
    }
}
