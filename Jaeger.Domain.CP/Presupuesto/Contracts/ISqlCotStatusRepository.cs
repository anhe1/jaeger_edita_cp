﻿using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// COTSTA
    /// </summary>
    public interface ISqlCotStatusRepository : IGenericRepository<CotizacionStatusModel> {
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
    }
}
