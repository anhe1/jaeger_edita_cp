﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// costos linea de produccion (COTPRO)
    /// </summary>
    public interface ISqlCotProRepository : IGenericRepository<CotProModel> {

        IEnumerable<CotProDetailModel> GetListBy(int[] index);

        IEnumerable<CotProDetailModelView> GetList1(int[] index);
    }
}