﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    public interface ISqlLcProRepository : IGenericRepository<LineaProduccionModel> {
        IEnumerable<LineaProduccionDetailModel> GetList(int[] indexs);

        IEnumerable<LineaProduccionView> GetListView(int[] indexs);
    }
}
