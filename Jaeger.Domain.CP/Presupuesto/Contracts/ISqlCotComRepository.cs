﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    /// <summary>
    /// Costos (CotCom)
    /// </summary>
    public interface ISqlCotComRepository : IGenericRepository<CotComModel> {
        IEnumerable<CotComDetailModel> GetList(int[] index);

        IEnumerable<CotComDetailModelView> GetList1(int[] index);
    }
}