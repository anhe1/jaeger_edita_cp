﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Presupuesto.Entities;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Presupuesto.Contracts {
    ///<summary>
    /// Cotización / Especificaciones de componentes / ensambles (COTLC2)
    ///</summary>
    public interface ISqlCotizacionEspecificacionRepository : IGenericRepository<CotLC2Model> {
        /// <summary>
        /// obtener lista de especificaciones 
        /// </summary>
        /// <param name="indexs">lista de indices del componente</param>
        IEnumerable<CotLC2DetailModel> GetList(int[] indexs);

        CotLC2DetailModel Save(CotLC2DetailModel item);
    }
}