﻿using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.ValueObjects {
    public enum CotizacionStatusEnum {
        [Description("Presupuesto")]
        Presupuesto = 1,
        [Description("Cotiz. Entregada")]
        Cotiz_Entregada = 2,
        [Description("Pedido_Autorizado")]
        Pedido_Autorizado = 3,
        [Description("Prod_Proceso")]
        Prod_Proceso = 4,
        [Description("Terminado_Auditado")]
        Terminado_Auditado = 5,
        [Description("Entregado")]
        Entregado = 6,
        [Description("Cobrado")]
        Cobrado = 7,
        [Description("Solicitado")]
        Solicitado = 8,
        [Description("Catalogo")]
        Catalogo = 9,
    }
}
