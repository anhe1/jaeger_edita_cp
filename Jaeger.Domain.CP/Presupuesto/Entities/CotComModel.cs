﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Costos (COTCOM)
    ///</summary>
    [SugarTable("cotcom")]
    public partial class CotComModel : BasePropertyChangeImplementation {
        private int index;
        private int activo;
        private int _COTCOM_COTPRD_ID;
        private int _COTCOM_PRDCOM_ID;
        private int _COTCOM_LC_ID;
        private string _COTCOM_CLA;
        private string _COTCOM_NOM;
        private int _COTCOM_CAN_INI;
        private decimal _COTCOM_CAN;
        private int _COTCOM_SEC;
        private int _COTCOM_USU_FM;
        private decimal _COTCOM_CAN_PRD;
        private int _COTCOM_CAN_COM;
        private decimal _COTCOM__CEN;
        private decimal _COTCOM__PRO;
        private decimal _COTCOM__MP;
        private decimal _COTCOM__CNS;
        private decimal _COTCOM_T_MAX;
        private DateTime? fechaModifica;
        private int _COTCOM_COM_ID;
        private decimal _COTCOM__TOT;

        public CotComModel() {
            this.activo = 1;
        }
        /// <summary>
        /// obtener o establecer COTCOM_ID
        /// </summary>
        [DataNames("COTCOM_ID")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "cotcom_id", ColumnDescription = "")]
        public int IdCotCom {
            get {
                return index;
            }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (COTCOM_A)
        /// </summary>
        [DataNames("COTCOM_A")]
        [SugarColumn(ColumnName = "cotcom_a", ColumnDescription = "")]
        public int Activo {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (COTCOM_COTPRD_ID)
        /// </summary>
        [DataNames("COTCOM_COTPRD_ID")]
        [SugarColumn(ColumnName = "cotcom_cotprd_id", ColumnDescription = "")]
        public int COTCOM_COTPRD_ID {
            get {
                return _COTCOM_COTPRD_ID;
            }
            set {
                _COTCOM_COTPRD_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_PRDCOM_ID")]
        [SugarColumn(ColumnName = "cotcom_prdcom_id", ColumnDescription = "")]
        public int COTCOM_PRDCOM_ID {
            get {
                return _COTCOM_PRDCOM_ID;
            }
            set {
                _COTCOM_PRDCOM_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_LC_ID")]
        [SugarColumn(ColumnName = "cotcom_lc_id", ColumnDescription = "")]
        public int COTCOM_LC_ID {
            get {
                return _COTCOM_LC_ID;
            }
            set {
                _COTCOM_LC_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_CLA")]
        [SugarColumn(ColumnName = "cotcom_cla", ColumnDescription = "")]
        public string COTCOM_CLA {
            get {
                return _COTCOM_CLA;
            }
            set {
                _COTCOM_CLA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o denominacion (COTCOM_NOM)
        /// </summary>
        [DataNames("COTCOM_NOM")]
        [SugarColumn(ColumnName = "cotcom_nom", ColumnDescription = "")]
        public string Denominacion {
            get {
                return _COTCOM_NOM;
            }
            set {
                _COTCOM_NOM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (COTCOM_CAN_INI)
        /// </summary>
        [DataNames("COTCOM_CAN_INI")]
        [SugarColumn(ColumnName = "cotcom_can_ini", ColumnDescription = "")]
        public int CantidadInicial {
            get {
                return _COTCOM_CAN_INI;
            }
            set {
                _COTCOM_CAN_INI = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer % Componente (COTCOM_CAN)
        /// </summary>
        [DataNames("COTCOM_CAN")]
        [SugarColumn(ColumnName = "cotcom_can", ColumnDescription = "")]
        public decimal CantidadP {
            get {
                return _COTCOM_CAN;
            }
            set {
                _COTCOM_CAN = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_SEC")]
        [SugarColumn(ColumnName = "cotcom_sec", ColumnDescription = "")]
        public int Secuencia {
            get {
                return _COTCOM_SEC;
            }
            set {
                _COTCOM_SEC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:0 Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_USU_FM")]
        [SugarColumn(ColumnName = "cotcom_usu_fm", ColumnDescription = "")]
        public int COTCOM_USU_FM {
            get {
                return _COTCOM_USU_FM;
            }
            set {
                _COTCOM_USU_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_CAN_PRD")]
        [SugarColumn(ColumnName = "cotcom_can_prd", ColumnDescription = "")]
        public decimal COTCOM_CAN_PRD {
            get {
                return _COTCOM_CAN_PRD;
            }
            set {
                _COTCOM_CAN_PRD = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer COTCOM_CAN_COM
        /// </summary>
        [DataNames("COTCOM_CAN_COM")]
        [SugarColumn(ColumnName = "cotcom_can_com", ColumnDescription = "")]
        public int Cantidad {
            get {
                return _COTCOM_CAN_COM;
            }
            set {
                _COTCOM_CAN_COM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (COTCOM_$_CEN)
        /// </summary>
        [DataNames("COTCOM_$_CEN")]
        [SugarColumn(ColumnName = "cotcom_$_cen", ColumnDescription = "")]
        public decimal Centro {
            get {
                return _COTCOM__CEN;
            }
            set {
                _COTCOM__CEN = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (COTCOM_$_pro)
        /// </summary>
        [DataNames("COTCOM_$_PRO")]
        [SugarColumn(ColumnName = "cotcom_$_pro", ColumnDescription = "")]
        public decimal ManoObra {
            get {
                return _COTCOM__PRO;
            }
            set {
                _COTCOM__PRO = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (COTCOM_$_MP)
        /// </summary>
        [DataNames("COTCOM_$_MP")]
        [SugarColumn(ColumnName = "cotcom_$_mp", ColumnDescription = "")]
        public decimal MateriaPrima {
            get {
                return _COTCOM__MP;
            }
            set {
                _COTCOM__MP = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (COTCOM_$_CNS)
        /// </summary>
        [DataNames("COTCOM_$_CNS")]
        [SugarColumn(ColumnName = "cotcom_$_cns", ColumnDescription = "")]
        public decimal Consumibles {
            get {
                return _COTCOM__CNS;
            }
            set {
                _COTCOM__CNS = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_T_MAX")]
        [SugarColumn(ColumnName = "cotcom_t_max", ColumnDescription = "")]
        public decimal Minutos {
            get {
                return _COTCOM_T_MAX;
            }
            set {
                _COTCOM_T_MAX = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("COTCOM_FM")]
        [SugarColumn(ColumnName = "cotcom_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_COM_ID")]
        [SugarColumn(ColumnName = "cotcom_com_id", ColumnDescription = "")]
        public int COTCOM_COM_ID {
            get {
                return _COTCOM_COM_ID;
            }
            set {
                _COTCOM_COM_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTCOM_$_TOT")]
        [SugarColumn(ColumnName = "cotcom_$_tot", ColumnDescription = "")]
        public decimal COTCOM__TOT {
            get {
                return _COTCOM__TOT;
            }
            set {
                _COTCOM__TOT = value;
                OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal SubTotal {
            get {
                return this.ManoObra + this.MateriaPrima + this.Centro + this.Consumibles;
            }
        }
    }
}
