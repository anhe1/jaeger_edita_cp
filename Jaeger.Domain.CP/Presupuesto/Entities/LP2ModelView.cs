﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// vista de linea de productos (Tabla LP2)
    /// </summary>
    public class LP2ModelView : LP2Model, ILP2Model {
        private BindingList<Lc2DetailModel> requisitos;
        private BindingList<LineaProduccionView> lineaProduccion;

        public LP2ModelView() {
            this.requisitos = new BindingList<Lc2DetailModel>();
            this.lineaProduccion = new BindingList<LineaProduccionView>();
        }

        /// <summary>
        /// obtener o establecer la lista de especificaciones (tabla LC2)
        /// </summary>
        public BindingList<Lc2DetailModel> Especificaciones {
            get { return this.requisitos; }
            set {
                this.requisitos = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// linea de produccion
        /// </summary>
        public BindingList<LineaProduccionView> LineaProduccion {
            get { return this.lineaProduccion; }
            set {
                this.lineaProduccion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
