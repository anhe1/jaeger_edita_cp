﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Costos Materiales (COTMAT)
    ///</summary>
    [SugarTable("cotmat")]
    public class CotMatModel : BasePropertyChangeImplementation, ICotMatModel {
        private int _COTMAT_ID;
        private int activo;
        private int _COTMAT_MP_ID;
        private int _COTMAT_COM_ID;
        private int _COTMAT_MP4_ID;
        private int _COTMAT_PRDMAT_ID3;
        private int _COTMAT_PRDMAT_ID;
        private int _COTMAT_COTPRD_ID;
        private int _COTMAT_COTPRO_ID;
        private decimal _COTMAT_CAN;
        private int _COTMAT_MUL;
        private decimal _COTMAT_CAN_MAT;
        private decimal _COTMAT__MP;
        private decimal _COTMAT__CNS;
        private int _COTMAT_USU_FM;
        private int _COTMAT_LIB;
        private int _COTMAT_FYV;
        private decimal _COTMAT_TRA;
        private decimal _COTMAT_TRB;
        private decimal _COTMAT_TRC;
        private decimal _COTMAT_TPA;
        private decimal _COTMAT_TPB;
        private decimal _COTMAT_TPC;
        private decimal _COTMAT_MRA;
        private decimal _COTMAT_MRB;
        private decimal _COTMAT_MPA;
        private decimal _COTMAT_MPB;
        private decimal _COTMAT_UNI;
        private DateTime? _COTMAT_FM;

        public CotMatModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("COTMAT_ID")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "cotmat_id", ColumnDescription = "")]
        public int IdCotMat {
            get {
                return _COTMAT_ID;
            }
            set {
                _COTMAT_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_A")]
        [SugarColumn(ColumnName = "cotmat_a", ColumnDescription = "")]
        public int Activo {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_MP_ID")]
        [SugarColumn(ColumnName = "cotmat_mp_id", ColumnDescription = "")]
        public int COTMAT_MP_ID {
            get {
                return _COTMAT_MP_ID;
            }
            set {
                _COTMAT_MP_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_COM_ID")]
        [SugarColumn(ColumnName = "cotmat_com_id", ColumnDescription = "")]
        public int COTMAT_COM_ID {
            get {
                return _COTMAT_COM_ID;
            }
            set {
                _COTMAT_COM_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_MP4_ID")]
        [SugarColumn(ColumnName = "cotmat_mp4_id", ColumnDescription = "")]
        public int COTMAT_MP4_ID {
            get {
                return _COTMAT_MP4_ID;
            }
            set {
                _COTMAT_MP4_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_PRDMAT_ID3")]
        [SugarColumn(ColumnName = "cotmat_prdmat_id3", ColumnDescription = "")]
        public int COTMAT_PRDMAT_ID3 {
            get {
                return _COTMAT_PRDMAT_ID3;
            }
            set {
                _COTMAT_PRDMAT_ID3 = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_PRDMAT_ID")]
        [SugarColumn(ColumnName = "cotmat_prdmat_id", ColumnDescription = "")]
        public int COTMAT_PRDMAT_ID {
            get {
                return _COTMAT_PRDMAT_ID;
            }
            set {
                _COTMAT_PRDMAT_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_COTPRD_ID")]
        [SugarColumn(ColumnName = "cotmat_cotprd_id", ColumnDescription = "")]
        public int COTMAT_COTPRD_ID {
            get {
                return _COTMAT_COTPRD_ID;
            }
            set {
                _COTMAT_COTPRD_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_COTPRO_ID")]
        [SugarColumn(ColumnName = "cotmat_cotpro_id", ColumnDescription = "")]
        public int COTMAT_COTPRO_ID {
            get {
                return _COTMAT_COTPRO_ID;
            }
            set {
                _COTMAT_COTPRO_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Fac Conv (COTMAT_CAN)
        /// </summary>
        [DataNames("COTMAT_CAN")]
        [SugarColumn(ColumnName = "cotmat_can", ColumnDescription = "")]
        public decimal FactorConversion {
            get {
                return _COTMAT_CAN;
            }
            set {
                _COTMAT_CAN = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_MUL")]
        [SugarColumn(ColumnName = "cotmat_mul", ColumnDescription = "")]
        public int Repetir {
            get {
                return _COTMAT_MUL;
            }
            set {
                _COTMAT_MUL = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_CAN_MAT")]
        [SugarColumn(ColumnName = "cotmat_can_mat", ColumnDescription = "")]
        public decimal Cantidad {
            get {
                return _COTMAT_CAN_MAT;
            }
            set {
                _COTMAT_CAN_MAT = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_$_MP")]
        [SugarColumn(ColumnName = "cotmat_$_mp", ColumnDescription = "")]
        public decimal MateriaPrima {
            get {
                return _COTMAT__MP;
            }
            set {
                _COTMAT__MP = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_$_CNS")]
        [SugarColumn(ColumnName = "cotmat_$_cns", ColumnDescription = "")]
        public decimal Consumibles {
            get {
                return _COTMAT__CNS;
            }
            set {
                _COTMAT__CNS = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_USU_FM")]
        [SugarColumn(ColumnName = "cotmat_usu_fm", ColumnDescription = "")]
        public int COTMAT_USU_FM {
            get {
                return _COTMAT_USU_FM;
            }
            set {
                _COTMAT_USU_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_LIB")]
        [SugarColumn(ColumnName = "cotmat_lib", ColumnDescription = "")]
        public int COTMAT_LIB {
            get {
                return _COTMAT_LIB;
            }
            set {
                _COTMAT_LIB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_FYV")]
        [SugarColumn(ColumnName = "cotmat_fyv", ColumnDescription = "")]
        public int COTMAT_FYV {
            get {
                return _COTMAT_FYV;
            }
            set {
                _COTMAT_FYV = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_TRA")]
        [SugarColumn(ColumnName = "cotmat_tra", ColumnDescription = "")]
        public decimal COTMAT_TRA {
            get {
                return _COTMAT_TRA;
            }
            set {
                _COTMAT_TRA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_TRB")]
        [SugarColumn(ColumnName = "cotmat_trb", ColumnDescription = "")]
        public decimal COTMAT_TRB {
            get {
                return _COTMAT_TRB;
            }
            set {
                _COTMAT_TRB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_TRC")]
        [SugarColumn(ColumnName = "cotmat_trc", ColumnDescription = "")]
        public decimal COTMAT_TRC {
            get {
                return _COTMAT_TRC;
            }
            set {
                _COTMAT_TRC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_TPA")]
        [SugarColumn(ColumnName = "cotmat_tpa", ColumnDescription = "")]
        public decimal COTMAT_TPA {
            get {
                return _COTMAT_TPA;
            }
            set {
                _COTMAT_TPA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_TPB")]
        [SugarColumn(ColumnName = "cotmat_tpb", ColumnDescription = "")]
        public decimal COTMAT_TPB {
            get {
                return _COTMAT_TPB;
            }
            set {
                _COTMAT_TPB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_TPC")]
        [SugarColumn(ColumnName = "cotmat_tpc", ColumnDescription = "")]
        public decimal COTMAT_TPC {
            get {
                return _COTMAT_TPC;
            }
            set {
                _COTMAT_TPC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_MRA")]
        [SugarColumn(ColumnName = "cotmat_mra", ColumnDescription = "")]
        public decimal COTMAT_MRA {
            get {
                return _COTMAT_MRA;
            }
            set {
                _COTMAT_MRA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_MRB")]
        [SugarColumn(ColumnName = "cotmat_mrb", ColumnDescription = "")]
        public decimal COTMAT_MRB {
            get {
                return _COTMAT_MRB;
            }
            set {
                _COTMAT_MRB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_MPA")]
        [SugarColumn(ColumnName = "cotmat_mpa", ColumnDescription = "")]
        public decimal COTMAT_MPA {
            get {
                return _COTMAT_MPA;
            }
            set {
                _COTMAT_MPA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_MPB")]
        [SugarColumn(ColumnName = "cotmat_mpb", ColumnDescription = "")]
        public decimal COTMAT_MPB {
            get {
                return _COTMAT_MPB;
            }
            set {
                _COTMAT_MPB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_$UNI")]
        [SugarColumn(ColumnName = "cotmat_$uni", ColumnDescription = "")]
        public decimal COTMAT_UNI {
            get {
                return _COTMAT_UNI;
            }
            set {
                _COTMAT_UNI = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTMAT_FM")]
        [SugarColumn(ColumnName = "cotmat_fm", ColumnDescription = "")]
        public DateTime? FechaModifica {
            get {
                if (_COTMAT_FM >= new DateTime(1900, 1, 1))
                    return _COTMAT_FM;
                return null;
            }
            set {
                _COTMAT_FM = value;
                OnPropertyChanged();
            }
        }
    }
}
