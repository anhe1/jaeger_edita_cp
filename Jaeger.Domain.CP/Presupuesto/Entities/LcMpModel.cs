﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.Domain.CP.Presupuesto.Entities { 
    ///<summary>
    ///T LC Materia Prima (LCMP)
    ///</summary>
    [SugarTable("lcmp")]
    public partial class LcMpModel : BasePropertyChangeImplementation, ILcMpModel {
        private int index;
        private int activo;
        private int _LCMP_PRO_ID;
        private int _LCMP_MP_ID;
        private int _LCMP_MP4_ID;
        private decimal _LCMP_CAN;
        private int _LCMP_MUL;
        private int _LCMP_LIB;
        private int _LCMP_FYV;
        private decimal _LCMP_TRA;
        private decimal _LCMP_TRB;
        private decimal _LCMP_TRC;
        private decimal _LCMP_TPA;
        private decimal _LCMP_TPB;
        private decimal _LCMP_TPC;
        private decimal _LCMP_MRA;
        private decimal _LCMP_MRB;
        private decimal _LCMP_MPA;
        private decimal _LCMP_MPB;
        private int _LCMP_USU_FM;
        private DateTime? fechaModifica;
        private int _LCMP_COM_ID;

        public LcMpModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer LCMP_ID
        /// </summary>
        [DataNames("LCMP_ID")]
        [SugarColumn(ColumnName = "lcmp_id", ColumnDescription = "LCMP_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdLCMP {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_A
        /// </summary>
        [DataNames("LCMP_A")]
        [SugarColumn(ColumnName = "lcmp_a", ColumnDescription = "LCMP_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_PRO_ID
        /// </summary>
        [DataNames("LCMP_PRO_ID")]
        [SugarColumn(ColumnName = "lcmp_pro_id", ColumnDescription = "LCMP_PRO_ID")]
        public int LCMP_PRO_ID {
            get { return this._LCMP_PRO_ID; }
            set {
                this._LCMP_PRO_ID = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_MP_ID
        /// </summary>
        [DataNames("LCMP_MP_ID")]
        [SugarColumn(ColumnName = "lcmp_mp_id", ColumnDescription = "LCMP_MP_ID")]
        public int LCMP_MP_ID {
            get { return this._LCMP_MP_ID; }
            set {
                this._LCMP_MP_ID = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_MP4_ID
        /// </summary>
        [DataNames("LCMP_MP4_ID")]
        [SugarColumn(ColumnName = "lcmp_mp4_id", ColumnDescription = "LCMP_MP4_ID")]
        public int LCMP_MP4_ID {
            get { return this._LCMP_MP4_ID; }
            set {
                this._LCMP_MP4_ID = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_CAN
        /// </summary>
        [DataNames("LCMP_CAN")]
        [SugarColumn(ColumnName = "lcmp_can", ColumnDescription = "LCMP_CAN", Length = 18, DecimalDigits = 4)]
        public decimal Factor {
            get { return this._LCMP_CAN; }
            set {
                this._LCMP_CAN = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_MUL
        /// </summary>
        [DataNames("LCMP_MUL")]
        [SugarColumn(ColumnName = "lcmp_mul", ColumnDescription = "LCMP_MUL")]
        public int Repetir {
            get { return this._LCMP_MUL; }
            set {
                this._LCMP_MUL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_LIB
        /// </summary>
        [DataNames("LCMP_LIB")]
        [SugarColumn(ColumnName = "lcmp_lib", ColumnDescription = "LCMP_LIB")]
        public int LCMP_LIB {
            get { return this._LCMP_LIB; }
            set {
                this._LCMP_LIB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_FYV
        /// </summary>
        [DataNames("LCMP_FYV")]
        [SugarColumn(ColumnName = "lcmp_fyv", ColumnDescription = "LCMP_FYV")]
        public int LCMP_FYV {
            get { return this._LCMP_FYV; }
            set {
                this._LCMP_FYV = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_TRA
        /// </summary>
        [DataNames("LCMP_TRA")]
        [SugarColumn(ColumnName = "lcmp_tra", ColumnDescription = "LCMP_TRA", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_TRA {
            get { return this._LCMP_TRA; }
            set {
                this._LCMP_TRA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_TRB
        /// </summary>
        [DataNames("LCMP_TRB")]
        [SugarColumn(ColumnName = "lcmp_trb", ColumnDescription = "LCMP_TRB", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_TRB {
            get { return this._LCMP_TRB; }
            set {
                this._LCMP_TRB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_TRC
        /// </summary>
        [DataNames("LCMP_TRC")]
        [SugarColumn(ColumnName = "lcmp_trc", ColumnDescription = "LCMP_TRC", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_TRC {
            get { return this._LCMP_TRC; }
            set {
                this._LCMP_TRC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_TPA
        /// </summary>
        [DataNames("LCMP_TPA")]
        [SugarColumn(ColumnName = "lcmp_tpa", ColumnDescription = "LCMP_TPA", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_TPA {
            get { return this._LCMP_TPA; }
            set {
                this._LCMP_TPA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_TPB
        /// </summary>
        [DataNames("LCMP_TPB")]
        [SugarColumn(ColumnName = "lcmp_tpb", ColumnDescription = "LCMP_TPB", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_TPB {
            get { return this._LCMP_TPB; }
            set {
                this._LCMP_TPB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_TPC
        /// </summary>
        [DataNames("LCMP_TPC")]
        [SugarColumn(ColumnName = "lcmp_tpc", ColumnDescription = "LCMP_TPC", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_TPC {
            get { return this._LCMP_TPC; }
            set {
                this._LCMP_TPC = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_MRA
        /// </summary>
        [DataNames("LCMP_MRA")]
        [SugarColumn(ColumnName = "lcmp_mra", ColumnDescription = "LCMP_MRA", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_MRA {
            get { return this._LCMP_MRA; }
            set {
                this._LCMP_MRA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_MRB
        /// </summary>
        [DataNames("LCMP_MRB")]
        [SugarColumn(ColumnName = "lcmp_mrb", ColumnDescription = "LCMP_MRB", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_MRB {
            get { return this._LCMP_MRB; }
            set {
                this._LCMP_MRB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_MPA
        /// </summary>
        [DataNames("LCMP_MPA")]
        [SugarColumn(ColumnName = "lcmp_mpa", ColumnDescription = "LCMP_MPA", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_MPA {
            get { return this._LCMP_MPA; }
            set {
                this._LCMP_MPA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_MPB
        /// </summary>
        [DataNames("LCMP_MPB")]
        [SugarColumn(ColumnName = "lcmp_mpb", ColumnDescription = "LCMP_MPB", Length = 18, DecimalDigits = 4)]
        public decimal LCMP_MPB {
            get { return this._LCMP_MPB; }
            set {
                this._LCMP_MPB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_USU_FM
        /// </summary>
        [DataNames("LCMP_USU_FM")]
        [SugarColumn(ColumnName = "lcmp_usu_fm", ColumnDescription = "LCMP_USU_FM")]
        public int LCMP_USU_FM {
            get { return this._LCMP_USU_FM; }
            set {
                this._LCMP_USU_FM = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_FM
        /// </summary>
        [DataNames("LCMP_FM")]
        [SugarColumn(ColumnName = "lcmp_fm", ColumnDescription = "LCMP_FM")]
        public DateTime? FechaModifica {
            get { return this.fechaModifica; }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LCMP_COM_ID
        /// </summary>
        [DataNames("LCMP_COM_ID")]
        [SugarColumn(ColumnName = "lcmp_com_id", ColumnDescription = "LCMP_COM_ID")]
        public int LCMP_COM_ID {
            get { return this._LCMP_COM_ID; }
            set {
                this._LCMP_COM_ID = value;
                this.OnPropertyChanged();
            }
        }
    }
}
