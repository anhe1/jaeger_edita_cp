﻿using System.ComponentModel;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    public class CotComDetailModelView : CotComModel {
        private BindingList<CotProDetailModelView> lineaProduccion;

        public CotComDetailModelView() {
            lineaProduccion = new BindingList<CotProDetailModelView>();
        }

        /// <summary>
        /// obtener o establcer la descripcion del componente / ensamble
        /// </summary>
        [DataNames("LC1_Nom")]
        public string Componente {
            get; set;
        }

        /// <summary>
        /// costos de la linea de produccion
        /// </summary>
        public BindingList<CotProDetailModelView> LineaProduccion {
            get {
                return lineaProduccion;
            }
            set {
                lineaProduccion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
