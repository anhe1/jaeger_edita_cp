﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// clase utilizada para las especificaciones
    /// </summary>
    [SugarTable("LC2", "Especificaciones")]
    public class Lc2Model : BasePropertyChangeImplementation, ILc2Model {
        private int index;
        private bool activo;
        private int idLc1;
        private int idCom;
        private string descripcion;
        private int secuencia;
        private DateTime? fechaModifica;

        public Lc2Model() {
            this.activo = true;
        }

        /// <summary>
        /// LC2_ID
        /// </summary>
        [DataNames("LC2_ID")]
        [SugarColumn(ColumnName = "LC2_ID", ColumnDescription = "Id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LC2_A")]
        [SugarColumn(ColumnName = "LC2_A", ColumnDescription = "Activo")]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// LC2_LC1_ID
        /// </summary>
        [DataNames("LC2_LC1_ID")]
        [SugarColumn(ColumnName = "LC2_LC1_ID", ColumnDescription = "IdLC1")]
        public int IdLC1 {
            get { return this.idLc1; }
            set {
                this.idLc1 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LC2_Sec")]
        [SugarColumn(ColumnName = "LC2_Sec", ColumnDescription = "Secuencia")]
        public int Secuencia {
            get { return this.secuencia; }
            set {
                this.secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// LC2_Com_ID
        /// </summary>
        [DataNames("LC2_Com_ID")]
        [SugarColumn(ColumnName = "LC2_Com_ID", ColumnDescription = "IdCom")]
        public int IdCom {
            get { return this.idCom; }
            set {
                this.idCom = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LC2_Tex")]
        [SugarColumn(ColumnName = "LC2_Tex", ColumnDescription = "Descripcion", Length = 50)]
        public string Descripcion {
            get { return this.descripcion; }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LC2_FM")]
        [SugarColumn(ColumnName = "LC2_FM", ColumnDescription = "FechaModifica", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
