﻿using System.ComponentModel;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// CotPro
    /// </summary>
    public class CotProDetailModel : CotProModel, ICotProModel {
        private BindingList<CotMatDetailModel> cotMatDetailModel;
        private BindingList<ProcesoEtapaDetailModel> subProcesos;

        public CotProDetailModel() {
            cotMatDetailModel = new BindingList<CotMatDetailModel>();
            subProcesos = new BindingList<ProcesoEtapaDetailModel>();
        }

        /// <summary>
        /// materia prima (COTMAT)
        /// </summary>
        public BindingList<CotMatDetailModel> MateriaPrima {
            get {
                return cotMatDetailModel;
            }
            set {
                cotMatDetailModel = value;
            }
        }

        /// <summary>
        /// sub procesos
        /// </summary>
        public BindingList<ProcesoEtapaDetailModel> SubProcesos {
            get {
                return subProcesos;
            }
            set {
                subProcesos = value;
            }
        }
    }
}
