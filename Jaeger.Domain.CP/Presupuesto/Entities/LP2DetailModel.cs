﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    public class LP2DetailModel : LP2Model, ILP2Model {
        private BindingList<Lc2DetailModel> requisitos;
        private BindingList<LineaProduccionDetailModel> lineaProduccion;

        public LP2DetailModel() {
            this.requisitos = new BindingList<Lc2DetailModel>();
            this.lineaProduccion = new BindingList<LineaProduccionDetailModel>();
        }

        public BindingList<Lc2DetailModel> Requisitos {
            get { return this.requisitos; }
            set {
                this.requisitos = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<LineaProduccionDetailModel> LineaProduccion {
            get { return this.lineaProduccion; }
            set {
                this.lineaProduccion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
