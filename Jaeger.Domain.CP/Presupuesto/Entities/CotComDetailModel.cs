﻿using System.ComponentModel;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// Costos (COTCOM_ID)
    /// </summary>
    public class CotComDetailModel : CotComModel {
        private BindingList<CotProDetailModel> lineaProduccion;

        public CotComDetailModel() {
            lineaProduccion = new BindingList<CotProDetailModel>();
        }

        [DataNames("LC1_Nom")]
        public string Componente {
            get; set;
        }

        /// <summary>
        /// costos de la linea de produccion
        /// </summary>
        public BindingList<CotProDetailModel> LineaProduccion {
            get {
                return lineaProduccion;
            }
            set {
                lineaProduccion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
