﻿using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Materia Prima (PRDMP)
    ///</summary>
    public class PrdMpDetailModel : PrdMpModel, IPrdMpModel {

    }
}
