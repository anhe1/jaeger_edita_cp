﻿using System.ComponentModel;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// Vista de Linea de produccion (LCPRO)
    /// </summary>
    [SugarTable("LCPRO", "Linea de producccion")]
    public class LineaProduccionView : LineaProduccionModel, ILineaProduccionModel {
        private BindingList<LcMpModelView> materiaPrima;
        private BindingList<ProMpDetailModelView> procesoConsumibles;
        private BindingList<ProcesoEtapaDetailModel> subProcesos;

        public LineaProduccionView() {
            this.materiaPrima = new BindingList<LcMpModelView>();
            this.procesoConsumibles = new BindingList<ProMpDetailModelView>();
            this.subProcesos = new BindingList<ProcesoEtapaDetailModel>();
        }

        [DataNames("EDP1_nom")]
        public string Departamento { get; set; }

        [DataNames("EDP2_sec")]
        public string Seccion { get; set; }

        [DataNames("EDP2_nom")]
        public string Centro { get; set; }

        [DataNames("EDP3_nom")]
        public string Proceso { get; set; }

        public BindingList<LcMpModelView> MateriaPrima {
            get { return this.materiaPrima; }
            set {
                this.materiaPrima = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<ProMpDetailModelView> Consumibles {
            get { return this.procesoConsumibles; }
            set {
                this.procesoConsumibles = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// EDP4
        /// </summary>
        public BindingList<ProcesoEtapaDetailModel> SubProcesos {
            get { return this.subProcesos; }
            set {
                this.subProcesos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
