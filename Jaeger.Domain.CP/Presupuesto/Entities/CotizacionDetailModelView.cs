﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Presupuesto.ValueObjects;
using System;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// cotizacion o presupuesto (COT)
    /// </summary>
    public class CotizacionDetailModelView : CotizacionModelView, ICloneable, ICotizacionModel {
        private BindingList<CotizacionCantidadPrecioDetailModel> cantidadPrecios;
        private BindingList<PrdComDetailModelView> prdCom;

        /// <summary>
        /// constructor
        /// </summary>
        public CotizacionDetailModelView() {
            this.cantidadPrecios = new BindingList<CotizacionCantidadPrecioDetailModel>();
            this.prdCom = new BindingList<PrdComDetailModelView>() { RaiseListChangedEvents = true };
            this.prdCom.AddingNew += new AddingNewEventHandler(this.Estructura_AddingNew);
            this.prdCom.ListChanged += new ListChangedEventHandler(this.Estructura_ListChanged);
        }

        /// <summary>
        /// obtenero establecer el estado de la cotizacion o presupuesto
        /// </summary>
        public CotizacionStatusEnum StatusEnum {
            get {
                return (CotizacionStatusEnum)this.IdStatus;
            }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cantidades y precios (tabla COTPRD)
        /// </summary>
        public BindingList<CotizacionCantidadPrecioDetailModel> CantidadPrecios {
            get {
                return cantidadPrecios;
            }
            set {
                cantidadPrecios = value;
                OnPropertyChanged();
            }
        }

        ///<summary>
        /// Estructura - Componente / ensamble, (prdcom)
        ///</summary>
        public BindingList<PrdComDetailModelView> Estructura {
            get {
                return prdCom;
            }
            set {
                if (this.prdCom != null) {
                    this.prdCom.AddingNew -= new AddingNewEventHandler(this.Estructura_AddingNew);
                    this.prdCom.ListChanged -= new ListChangedEventHandler(this.Estructura_ListChanged);
                }
                prdCom = value;
                if (this.prdCom != null) {
                    this.prdCom.AddingNew += new AddingNewEventHandler(this.Estructura_AddingNew);
                    this.prdCom.ListChanged += new ListChangedEventHandler(this.Estructura_ListChanged);
                }
                OnPropertyChanged();
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }

        private void Estructura_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new PrdComDetailModel { IdCotizacion = this.IdCotizacion };
            Console.WriteLine(this.IdCotizacion);
        }

        private void Estructura_ListChanged(object sender, ListChangedEventArgs e) {

        }
    }
}
