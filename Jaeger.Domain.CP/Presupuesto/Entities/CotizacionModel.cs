﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;
using System;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// presupuesto o cotizacion (COT)
    /// </summary>
    [SugarTable("COT", "Presupuesto / cotizacion")]
    public class CotizacionModel : BasePropertyChangeImplementation, ICotizacionModel {
        private int idCotizacion;
        private bool activo;
        private int idCliente;
        private int idContacto;
        private int idCom;
        private string modelo;
        private string observaciones;
        private int idVendedor;
        private string condicionPago;
        private string condicionEntrega;
        private DateTime fechaSolicitud;
        private int idStatus;
        private string creo;
        private string modifica;
        private DateTime? fechaModifica;
        private DateTime? fechaCotizacion;
        private DateTime? fechaRequerido;

        public CotizacionModel() {
            this.activo = true;
            this.fechaSolicitud = DateTime.Now;
            this.idStatus = 1;
        }

        /// <summary>
        /// obtener o establecer el numero de la cotizacion o presupuesto (COT_ID)
        /// </summary>
        [DataNames("COT_ID")]
        [SugarColumn(ColumnName = "COT_ID", ColumnDescription = "indice de cotizacion", IsPrimaryKey = true, IsIdentity = true)]
        public int IdCotizacion {
            get {
                return idCotizacion;
            }
            set {
                idCotizacion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (COT_A)
        /// </summary>
        [DataNames("COT_A")]
        [SugarColumn(ColumnName = "COT_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer COT_LP_ID
        /// </summary>
        [DataNames("COT_LP_ID")]
        [SugarColumn(ColumnName = "COT_LP_ID", ColumnDescription = "")]
        public int COT_LP_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer indice de relacion del status del documento COT_COTSTA_ID
        /// </summary>
        [DataNames("COT_COTSTA_ID")]
        [SugarColumn(ColumnName = "COT_COTSTA_ID", ColumnDescription = "IdStatus")]
        public int IdStatus {
            get {
                return idStatus;
            }
            set {
                idStatus = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio (COT_DIR_ID)
        /// </summary>
        [DataNames("COT_DIR_ID")]
        [SugarColumn(ColumnName = "COT_DIR_ID", ColumnDescription = "IdCliente")]
        public int IdCliente {
            get {
                return idCliente;
            }
            set {
                idCliente = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la tabla de los contactos (COT_DIRCON_ID)
        /// </summary>
        [DataNames("COT_DIRCON_ID")]
        [SugarColumn(ColumnName = "COT_DIRCON_ID", ColumnDescription = "IdContacto")]
        public int IdContacto {
            get {
                return idContacto;
            }
            set {
                idContacto = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la tabla de vendedores COT_VEN_ID
        /// </summary>
        [DataNames("COT_VEN_ID")]
        [SugarColumn(ColumnName = "COT_VEN_ID", ColumnDescription = "IdVendedor")]
        public int IdVendedor {
            get {
                return idVendedor;
            }
            set {
                idVendedor = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el catalogo de productos de venta (COT_COM_ID)->(LP1_ID)
        /// </summary>
        [DataNames("COT_Com_ID")]
        [SugarColumn(ColumnName = "COT_Com_ID", ColumnDescription = "IdCom")]
        public int IdCom {
            get {
                return idCom;
            }
            set {
                idCom = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// COT_PRD_ID
        /// </summary>
        [DataNames("COT_PRD_ID")]
        [SugarColumn(ColumnName = "COT_PRD_ID", ColumnDescription = "COT_PRD_ID")]
        public int COT_PRD_ID {
            get; set;
        }

        /// <summary>
        /// COT_PRD
        /// </summary>
        [DataNames("COT_PRD")]
        [SugarColumn(ColumnName = "COT_PRD", ColumnDescription = "", Length = 50)]
        public string COT_PRD {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el modelo de la cotizacion (COT_Con)
        /// </summary>
        [DataNames("COT_Con")]
        [SugarColumn(ColumnName = "COT_Con", ColumnDescription = "Modelo", Length = 100)]
        public string Modelo {
            get {
                return modelo;
            }
            set {
                modelo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la condiciones de pago (COT_Con_Pag)
        /// </summary>
        [DataNames("COT_Con_Pag")]
        [SugarColumn(ColumnName = "COT_Con_Pag", ColumnDescription = "condiciones de pago", Length = 50)]
        public string CondicionPago {
            get {
                return condicionPago;
            }
            set {
                condicionPago = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// COT_Con_Ent
        /// </summary>
        [DataNames("COT_Con_Ent")]
        [SugarColumn(ColumnName = "COT_Con_Ent", ColumnDescription = "condiciones de entrega", Length = 50)]
        public string CondicionEntrega {
            get {
                return condicionEntrega;
            }
            set {
                condicionEntrega = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// COT_Obs
        /// </summary>
        [DataNames("COT_Obs")]
        [SugarColumn(ColumnName = "COT_Obs", ColumnDescription = "Nota", Length = 500)]
        public string Nota {
            get {
                return observaciones;
            }
            set {
                observaciones = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// COT_FEC_CAD
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public DateTime? COT_FEC_CAD {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de solicitud (COT_fec_sol)
        /// </summary>
        [DataNames("COT_fec_sol")]
        [SugarColumn(ColumnName = "COT_fec_sol", ColumnDescription = "fecha de solicitud")]
        public DateTime FechaSolicitud {
            get {
                return fechaSolicitud;
            }
            set {
                fechaSolicitud = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha requerida (COT_FEC_REQ)
        /// </summary>
        [DataNames("COT_FEC_REQ")]
        [SugarColumn(ColumnName = "COT_FEC_REQ", ColumnDescription = "fecha requerida", IsNullable = true)]
        public DateTime? FechaRequerido {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaRequerido >= firstGoodDate)
                    return fechaRequerido;
                return null;
            }
            set {
                this.fechaRequerido = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cotizacion (COT_FEC_COT)
        /// </summary>
        [DataNames("COT_FEC_COT")]
        [SugarColumn(ColumnName = "COT_FEC_COT", ColumnDescription = "fecha de cotizacion", IsNullable = true)]
        public DateTime? FechaCotizacion {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaCotizacion >= firstGoodDate)
                    return fechaCotizacion;
                return null;
            }
            set {
                this.fechaCotizacion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// COT_USU_N
        /// </summary>
        [DataNames("COT_USU_N")]
        [SugarColumn(ColumnName = "COT_USU_N", ColumnDescription = "Creo", Length = 10)]
        public string Creo {
            get {
                return creo;
            }
            set {
                creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// clave del ultimo usuario que modifica el registro (COT_USU_M)
        /// </summary>
        [DataNames("COT_USU_M")]
        [SugarColumn(ColumnName = "COT_USU_M", ColumnDescription = "clave del utlimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return modifica;
            }
            set {
                modifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("COT_FM")]
        [SugarColumn(ColumnName = "COT_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

        public long Diferencia {
            get {
                if (this.fechaCotizacion != null) {
                    TimeSpan? days = (this.FechaCotizacion - this.FechaSolicitud);
                    return days.Value.Days;
                }
                return 0;
            }
        }
    }
}
