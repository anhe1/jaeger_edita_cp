﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using System;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Vista Estructura - Componente / Ensamble, (prdcom)
    ///</summary>
    public class PrdComDetailModelView : PrdComModel, ICloneable, IPrdComModel {
        private BindingList<CotLC2DetailModel> especificaciones;
        private BindingList<PrdProDetailModelView> prdProDetailModels;

        public PrdComDetailModelView() {
            this.Cantidad = new decimal(1.1); // %Com
            this.especificaciones = new BindingList<CotLC2DetailModel>();
            prdProDetailModels = new BindingList<PrdProDetailModelView>();
        }

        /// <summary>
        /// especificaciones (COTLC2)
        /// </summary>
        public BindingList<CotLC2DetailModel> Especificaciones {
            get {
                return especificaciones;
            }
            set {
                especificaciones = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// linea de produccion (prdpro)
        /// </summary>
        public BindingList<PrdProDetailModelView> LineaProduccion {
            get {
                return prdProDetailModels;
            }
            set {
                prdProDetailModels = value;
                this.OnPropertyChanged();
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
