﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Cotización / Especificaciones de componentes / ensambles (COTLC2)
    ///</summary>
    [SugarTable("cotlc2")]
    public class CotLC2Model : BasePropertyChangeImplementation {
        private int index;
        private int activo;
        private DateTime? fechaModifica;
        private string modifica;

        public CotLC2Model() {
            this.activo = 1;
        }
        /// <summary>
        /// obtener o establecer (cotlc2_id)
        /// </summary>           
        [DataNames("cotlc2_id")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "cotlc2_id", ColumnDescription = "", IsIdentity = true)]
        public int Id {
            get {
                return index;
            }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        [DataNames("cotlc2_a")]
        [SugarColumn(ColumnName = "cotlc2_a", ColumnDescription = "registro activo")]
        public int Activo {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (COTLC2_LC2_ID)
        /// </summary>           
        [DataNames("cotlc2_lc2_id")]
        [SugarColumn(ColumnName = "cotlc2_lc2_id", ColumnDescription = "")]
        public int? COTLC2_LC2_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer  cotlc2_prdcom_id
        /// </summary>           
        [DataNames("cotlc2_prdcom_id")]
        [SugarColumn(ColumnName = "cotlc2_prdcom_id", ColumnDescription = "")]
        public int COTLC2_PRDCOM_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la descripcion de la especificacion (COTLC2_TEX)
        /// </summary>           
        [DataNames("cotlc2_tex")]
        [SugarColumn(ColumnName = "cotlc2_tex", ColumnDescription = "", Length = 100)]
        public string Descripcion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del usuario que modifica el registro
        /// </summary>           
        [DataNames("cotlc2_usu_m")]
        [SugarColumn(ColumnName = "cotlc2_usu_m", ColumnDescription = "clave del usuario que modifica el registro", Length = 10)]
        public string Modifica {
            get {
                return modifica;
            }
            set {
                modifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>           
        [DataNames("cotlc2_cot_id")]
        [SugarColumn(ColumnName = "cotlc2_cot_id", ColumnDescription = "")]
        public int? COTLC2_COT_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>           
        [DataNames("cotlc2_com_id")]
        [SugarColumn(ColumnName = "cotlc2_com_id", ColumnDescription = "")]
        public int? COTLC2_COM_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("cotlc2_fm")]
        [SugarColumn(ColumnName = "cotlc2_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

    }
}
