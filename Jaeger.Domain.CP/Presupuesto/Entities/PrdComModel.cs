﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Estructura - Componente / Ensamble, (prdcom)
    ///</summary>
    [SugarTable("prdcom", "componente - ensamble")]
    public class PrdComModel : BasePropertyChangeImplementation, IPrdComModel {
        private int index;
        private int activo;
        private int secuencia;
        private string denominacion;
        private int cantidadInicial;
        private decimal cantidad;
        private DateTime? fechaModifica;

        public PrdComModel() {
            this.activo = 1;
        }
        /// <summary>
        /// obtener o establecer prdcom_id
        /// </summary>           
        [DataNames("prdcom_id")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "prdcom_id", ColumnDescription = "", IsIdentity = true, IsNullable = false)]
        public int Id {
            get {
                return index;
            }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        [DataNames("prdcom_a")]
        [SugarColumn(ColumnName = "prdcom_a", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public int Activo {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer prdcom_cot_id
        /// </summary>           
        [DataNames("prdcom_cot_id")]
        [SugarColumn(ColumnName = "prdcom_cot_id", ColumnDescription = "")]
        public int IdCotizacion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer prdcom_prd_id
        /// </summary>           
        [DataNames("prdcom_prd_id")]
        [SugarColumn(ColumnName = "prdcom_prd_id", ColumnDescription = "")]
        public int PRDCOM_PRD_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer prdcom_lc_id
        /// </summary>           
        [DataNames("prdcom_lc_id")]
        [SugarColumn(ColumnName = "prdcom_lc_id", ColumnDescription = "")]
        public int PRDCOM_LC_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer indice de la Linea Componentes (PRDCOM_COM_ID)
        /// </summary>           
        [DataNames("prdcom_com_id")]
        [SugarColumn(ColumnName = "prdcom_com_id", ColumnDescription = "")]
        public int PRDCOM_COM_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer denominacion del componente (prdcom_nom)
        /// </summary>           
        [DataNames("prdcom_nom")]
        [SugarColumn(ColumnName = "prdcom_nom", ColumnDescription = "denominacion del compronente", Length = 50)]
        public string Denominacion {
            get {
                return denominacion;
            }
            set {
                denominacion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cantidad inicial (prdcom_can_ini)
        /// </summary>           
        [DataNames("prdcom_can_ini")]
        [SugarColumn(ColumnName = "prdcom_can_ini", ColumnDescription = "cantidad inicial")]
        public int CantidadInicial {
            get {
                return cantidadInicial;
            }
            set {
                cantidadInicial = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer % Com (prdcom_can)
        /// </summary>           
        [DataNames("prdcom_can")]
        [SugarColumn(ColumnName = "prdcom_can", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal Cantidad {
            get {
                return cantidad;
            }
            set {
                cantidad = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer secuencia (cotprd_sec)
        /// </summary>           
        [DataNames("prdcom_sec")]
        [SugarColumn(ColumnName = "prdcom_sec", ColumnDescription = "secuencia")]
        public int Secuencia {
            get {
                return secuencia;
            }
            set {
                secuencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>           
        [DataNames("prdcom_usu_fm")]
        [SugarColumn(ColumnName = "prdcom_usu_fm")]
        public int PRDCOM_USU_FM {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro (prdcom_fm)
        /// </summary>
        [DataNames("prdcom_fm")]
        [SugarColumn(ColumnName = "prdcom_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
