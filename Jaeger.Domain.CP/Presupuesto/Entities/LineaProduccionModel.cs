﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// Linea de produccion (LCPRO)
    /// </summary>
    [SugarTable("LCPRO", "Linea de producccion")]
    public class LineaProduccionModel : BasePropertyChangeImplementation, ILineaProduccionModel {
        private int index;
        private bool activo;
        private int idCom;
        private int idProceso;
        private int secuencia;
        private decimal cantidadProceso;
        private decimal vecesProceso;
        private int modifica;
        private DateTime? fechaModifica;
        private int idcom2;

        public LineaProduccionModel() {
            this.activo = true;
        }

        /// <summary>
        /// LCPRO_ID
        /// </summary>
        [DataNames("LCPRO_ID")]
        [SugarColumn(ColumnName = "LCPRO_ID", ColumnDescription = "IdLinea", IsPrimaryKey = true, IsIdentity = true)]
        public int IdLinea {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LCPRO_A")]
        [SugarColumn(ColumnName = "LCPRO_A", ColumnDescription = "Activo")]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LCPRO_COM_ID")]
        [SugarColumn(ColumnName = "LCPRO_COM_ID", ColumnDescription = "IdCom")]
        public int IdCom {
            get { return this.idCom; }
            set {
                this.idCom = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// LCPRO_PRO_ID
        /// </summary>
        [DataNames("LCPRO_PRO_ID")]
        [SugarColumn(ColumnName = "LCPRO_PRO_ID", ColumnDescription = "IdProceso")]
        public int IdProceso {
            get { return this.idProceso; }
            set {
                this.idProceso = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LCPRO_SEC")]
        [SugarColumn(ColumnName = "LCPRO_SEC", ColumnDescription = "Secuencia")]
        public int Secuencia {
            get { return this.secuencia; }
            set {
                this.secuencia = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LCPRO_CAN_CP")]
        [SugarColumn(ColumnName = "LCPRO_CAN_CP", ColumnDescription = "CantidadProceso", Length = 18, DecimalDigits = 4)]
        public decimal CantidadProceso {
            get { return this.cantidadProceso; }
            set {
                this.cantidadProceso = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LCPRO_CAN_VP")]
        [SugarColumn(ColumnName = "LCPRO_CAN_VP", ColumnDescription = "VecesProceso", Length = 18, DecimalDigits = 4)]
        public decimal VecesProceso {
            get { return this.vecesProceso; }
            set {
                this.vecesProceso = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LCPRO_USU_FM")]
        [SugarColumn(ColumnName = "LCPRO_USU_FM", ColumnDescription = "Modifica")]
        public int Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("LCPRO_FM")]
        [SugarColumn(ColumnName = "LCPRO_FM", ColumnDescription = "FechaModifica", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// LcPro_Com2_ID
        /// </summary>
        [DataNames("LCPRO_COM2_ID")]
        [SugarColumn(ColumnName = "LCPRO_COM2_ID", ColumnDescription = "IdCom2")]
        public int IdCom2 {
            get { return this.idcom2; }
            set {
                this.idcom2 = value;
                this.OnPropertyChanged();
            }
        }
    }
}
