﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    public class LcMpModelView : LcMpModel, ILcMpModel {
        [DataNames("Com3_N1")]
        public string Nivel1 { get; set; }
        [DataNames("Com3_N2")]
        public string Nivel2 { get; set; }
        [DataNames("Com3_N3")]
        public string Nivel3 { get; set; }
        [DataNames("ComMod_Mod")]
        public string Modelo { get; set; }
        [DataNames("ComMod_nom")]
        public string Especificacion { get; set; }
        [DataNames("ComMod_Mar")]
        public string Marca { get; set; }
        [DataNames("ComMod_UniNom")]
        public string Unidad { get; set; }
        [DataNames("ComMod_$Uni")]
        public decimal Unitario { get; set; }
    }
}
