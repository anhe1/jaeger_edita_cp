﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;
using System;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Materia Prima (PRDMP)
    ///</summary>
    [SugarTable("prdmp")]
    public class PrdMpModel : BasePropertyChangeImplementation, IPrdMpModel {
        private int index;
        private int activo;
        private int _PRDMP_PRDPRO_ID;
        private int _PRDMP_MP_ID;
        private int _PRDMP_MP4_ID;
        private decimal _PRDMP_CAN;
        private int _PRDMP_MUL;
        private int _PRDMP_LIB;
        private int _PRDMP_FYV;
        private decimal _PRDMP_TRA;
        private decimal _PRDMP_TRB;
        private decimal _PRDMP_TRC;
        private decimal _PRDMP_TPA;
        private decimal _PRDMP_TPB;
        private decimal _PRDMP_TPC;
        private decimal _PRDMP_MRA;
        private decimal _PRDMP_MRB;
        private decimal _PRDMP_MPA;
        private decimal _PRDMP_MPB;
        private int _PRDMP_USU_FM;
        private DateTime? fechaModifica;
        private int _PRDMP_COT_ID;
        private int _PRDMP_COM_ID;

        public PrdMpModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (PRDMP_ID)
        /// </summary>
        [DataNames("PRDMP_ID")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "prdmp_id", ColumnDescription = "")]
        public int IdPrdMP {
            get {
                return index;
            }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_A")]
        [SugarColumn(ColumnName = "prdmp_a", ColumnDescription = "")]
        public int Activo {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PRDMP_PRDPRO_ID)
        /// </summary>
        [DataNames("PRDMP_PRDPRO_ID")]
        [SugarColumn(ColumnName = "prdmp_prdpro_id", ColumnDescription = "")]
        public int IdPrdPro {
            get {
                return _PRDMP_PRDPRO_ID;
            }
            set {
                _PRDMP_PRDPRO_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_MP_ID")]
        [SugarColumn(ColumnName = "prdmp_mp_id", ColumnDescription = "")]
        public int PRDMP_MP_ID {
            get {
                return _PRDMP_MP_ID;
            }
            set {
                _PRDMP_MP_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_MP4_ID")]
        [SugarColumn(ColumnName = "prdmp_mp4_id", ColumnDescription = "")]
        public int PRDMP_MP4_ID {
            get {
                return _PRDMP_MP4_ID;
            }
            set {
                _PRDMP_MP4_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor de conversion (PRDMP_CAN)
        /// </summary>
        [DataNames("PRDMP_CAN")]
        [SugarColumn(ColumnName = "prdmp_can", ColumnDescription = "")]
        public decimal Factor {
            get {
                return _PRDMP_CAN;
            }
            set {
                _PRDMP_CAN = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PRDMP_MUL
        /// </summary>
        [DataNames("PRDMP_MUL")]
        [SugarColumn(ColumnName = "prdmp_mul", ColumnDescription = "")]
        public int Repetir {
            get {
                return _PRDMP_MUL;
            }
            set {
                _PRDMP_MUL = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_LIB")]
        [SugarColumn(ColumnName = "prdmp_lib", ColumnDescription = "")]
        public int PRDMP_LIB {
            get {
                return _PRDMP_LIB;
            }
            set {
                _PRDMP_LIB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_FYV")]
        [SugarColumn(ColumnName = "prdmp_fyv", ColumnDescription = "")]
        public int PRDMP_FYV {
            get {
                return _PRDMP_FYV;
            }
            set {
                _PRDMP_FYV = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_TRA")]
        [SugarColumn(ColumnName = "prdmp_tra", ColumnDescription = "")]
        public decimal PRDMP_TRA {
            get {
                return _PRDMP_TRA;
            }
            set {
                _PRDMP_TRA = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_TRB")]
        [SugarColumn(ColumnName = "prdmp_trb", ColumnDescription = "")]
        public decimal PRDMP_TRB {
            get {
                return _PRDMP_TRB;
            }
            set {
                _PRDMP_TRB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_TRC")]
        [SugarColumn(ColumnName = "prdmp_trc", ColumnDescription = "")]
        public decimal PRDMP_TRC {
            get {
                return _PRDMP_TRC;
            }
            set {
                _PRDMP_TRC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_TPA")]
        [SugarColumn(ColumnName = "prdmp_tpa", ColumnDescription = "")]
        public decimal PRDMP_TPA {
            get {
                return _PRDMP_TPA;
            }
            set {
                _PRDMP_TPA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_TPB")]
        [SugarColumn(ColumnName = "prdmp_tpb", ColumnDescription = "")]
        public decimal PRDMP_TPB {
            get {
                return _PRDMP_TPB;
            }
            set {
                _PRDMP_TPB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_TPC")]
        [SugarColumn(ColumnName = "prdmp_tpc", ColumnDescription = "")]
        public decimal PRDMP_TPC {
            get {
                return _PRDMP_TPC;
            }
            set {
                _PRDMP_TPC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_MRA")]
        [SugarColumn(ColumnName = "prdmp_mra", ColumnDescription = "")]
        public decimal PRDMP_MRA {
            get {
                return _PRDMP_MRA;
            }
            set {
                _PRDMP_MRA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_MRB")]
        [SugarColumn(ColumnName = "prdmp_mrb", ColumnDescription = "")]
        public decimal PRDMP_MRB {
            get {
                return _PRDMP_MRB;
            }
            set {
                _PRDMP_MRB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_MPA")]
        [SugarColumn(ColumnName = "prdmp_mpa", ColumnDescription = "")]
        public decimal PRDMP_MPA {
            get {
                return _PRDMP_MPA;
            }
            set {
                _PRDMP_MPA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_MPB")]
        [SugarColumn(ColumnName = "prdmp_mpb", ColumnDescription = "")]
        public decimal PRDMP_MPB {
            get {
                return _PRDMP_MPB;
            }
            set {
                _PRDMP_MPB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_USU_FM")]
        [SugarColumn(ColumnName = "prdmp_usu_fm", ColumnDescription = "")]
        public int PRDMP_USU_FM {
            get {
                return _PRDMP_USU_FM;
            }
            set {
                _PRDMP_USU_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("PRDMP_FM")]
        [SugarColumn(ColumnName = "prdmp_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_COT_ID")]
        [SugarColumn(ColumnName = "prdmp_cot_id", ColumnDescription = "")]
        public int PRDMP_COT_ID {
            get {
                return _PRDMP_COT_ID;
            }
            set {
                _PRDMP_COT_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDMP_COM_ID")]
        [SugarColumn(ColumnName = "prdmp_com_id", ColumnDescription = "")]
        public int PRDMP_COM_ID {
            get {
                return _PRDMP_COM_ID;
            }
            set {
                _PRDMP_COM_ID = value;
                OnPropertyChanged();
            }
        }
    }
}
