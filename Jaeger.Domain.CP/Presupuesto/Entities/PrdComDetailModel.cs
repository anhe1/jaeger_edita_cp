﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using System;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Estructura - Componente / Ensamble, (prdcom)
    ///</summary>
    public class PrdComDetailModel : PrdComModel, ICloneable, IPrdComModel {
        private BindingList<CotLC2DetailModel> especificaciones;
        private BindingList<PrdProDetailModel> prdProDetailModels;

        public PrdComDetailModel() {
            especificaciones = new BindingList<CotLC2DetailModel>();
            prdProDetailModels = new BindingList<PrdProDetailModel>();
        }

        /// <summary>
        /// especificaciones (COTLC2)
        /// </summary>
        public BindingList<CotLC2DetailModel> Especificaciones {
            get {
                return especificaciones;
            }
            set {
                especificaciones = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// linea de produccion (prdpro)
        /// </summary>
        public BindingList<PrdProDetailModel> LineaProduccion {
            get {
                return prdProDetailModels;
            }
            set {
                prdProDetailModels = value;
                this.OnPropertyChanged();
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
