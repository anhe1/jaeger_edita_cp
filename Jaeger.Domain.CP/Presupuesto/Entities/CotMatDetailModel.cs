﻿using Jaeger.Domain.CP.Presupuesto.Contracts;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Costos Materiales (COTMAT)
    ///</summary>
    public class CotMatDetailModel : CotMatModel, ICotMatModel {
        public CotMatDetailModel() {

        }
    }
}
