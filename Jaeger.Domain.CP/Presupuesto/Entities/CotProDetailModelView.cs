﻿using System.ComponentModel;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// CotPro
    /// </summary>
    public class CotProDetailModelView : CotProModel, ICotProModel {
        private BindingList<CotMatDetailModelView> cotMatDetailModel;
        private BindingList<ProcesoEtapaDetailModel> subProcesos;

        public CotProDetailModelView() {
            cotMatDetailModel = new BindingList<CotMatDetailModelView>();
            subProcesos = new BindingList<ProcesoEtapaDetailModel>();
        }

        #region departamento

        [DataNames("EDP1_nom")]
        public string Departamento {
            get; set;
        }

        [DataNames("EDP2_sec")]
        public string Seccion {
            get; set;
        }

        [DataNames("EDP2_nom")]
        public string Centro {
            get; set;
        }

        [DataNames("EDP3_nom")]
        public string Proceso {
            get; set;
        }

        [DataNames("EDP2_val_min")]
        public decimal ValorMinuto {
            get; set;
        }

        #endregion

        /// <summary>
        /// materia prima (COTMAT)
        /// </summary>
        public BindingList<CotMatDetailModelView> MateriaPrima {
            get {
                return cotMatDetailModel;
            }
            set {
                cotMatDetailModel = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// sub procesos
        /// </summary>
        public BindingList<ProcesoEtapaDetailModel> SubProcesos {
            get {
                return subProcesos;
            }
            set {
                subProcesos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
