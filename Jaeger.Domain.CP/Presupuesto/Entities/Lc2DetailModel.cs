﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// clase utilizada para las especificaciones
    /// </summary>
    [SugarTable("LC2", "Especificaciones")]
    public class Lc2DetailModel : Lc2Model, ILc2Model {

    }
}
