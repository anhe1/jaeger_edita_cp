﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// vista de cotizacion o presupuesto, contiene informacion del directorio y vendedores
    /// </summary>
    public class CotizacionModelView : CotizacionModel, ICotizacionModel {

        public CotizacionModelView() {

        }

        #region datos del directorio

        /// <summary>
        /// obtener o establecer nombre del cliente (DIR_nom)
        /// </summary>
        [DataNames("DIR_nom")]
        public string Cliente {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre del contacto (DIRCON_con)
        /// </summary>
        [DataNames("DIRCON_con")]
        public string Contacto {
            get; set;
        }

        #endregion

        #region catalogo de productos

        /// <summary>
        /// obtener o establecer Catálogo Productos (LP1_Tex)
        /// </summary>
        [DataNames("LP1_Tex")]
        public string Producto {
            get; set;
        }

        #endregion

        #region catalogo de vendedores

        /// <summary>
        /// obtener o establecer clave del vendedor (VEN_cla)
        /// </summary>
        [DataNames("VEN_cla")]
        public string Vendedor {
            get; set;
        }

        #endregion
    }
}
