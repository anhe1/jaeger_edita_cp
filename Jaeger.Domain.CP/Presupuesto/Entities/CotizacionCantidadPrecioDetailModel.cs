﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using SqlSugar;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// Cantidades y Precios
    /// </summary>
    [SugarTable("COTPRD", "presupuesto: cantidades y precios")]
    public class CotizacionCantidadPrecioDetailModel : CotizacionCantidadPrecioModel, ICotizacionCantidadPrecioModel {
        private BindingList<CotComDetailModelView> costos;

        public CotizacionCantidadPrecioDetailModel() {
            costos = new BindingList<CotComDetailModelView>();
        }

        /// <summary>
        /// Especificaciones
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<CotComDetailModelView> Costos {
            get {
                return costos;
            }
            set {
                costos = value;
                OnPropertyChanged();
            }
        }
    }
}
