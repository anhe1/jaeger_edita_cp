﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;
using System;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    ///TRIAL
    ///</summary>
    [SugarTable("cotsta")]
    public class CotizacionStatusModel : BasePropertyChangeImplementation, ICotizacionStatusModel {
        #region declaraciones
        private int _COTSTA_ID;
        private int _COTSTA_A;
        private string _COTSTA_NOM;
        private int _COTSTA_SEC;
        private int _COTSTA_USU_FM;
        private DateTime? _COTSTA_FM;
        #endregion

        public CotizacionStatusModel() {

        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("COTSTA_ID")]
        [SugarColumn(ColumnName = "cotsta_id", ColumnDescription = "COTSTA_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdCotStatus {
            get { return this._COTSTA_ID; }
            set {
                this._COTSTA_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTSTA_A")]
        [SugarColumn(ColumnName = "cotsta_a", ColumnDescription = "COTSTA_A")]
        public int Activo {
            get { return this._COTSTA_A; }
            set {
                this._COTSTA_A = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTSTA_NOM")]
        [SugarColumn(ColumnName = "cotsta_nom", ColumnDescription = "COTSTA_NOM", Length = 50)]
        public string Descripcion {
            get { return this._COTSTA_NOM; }
            set {
                this._COTSTA_NOM = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTSTA_SEC")]
        [SugarColumn(ColumnName = "cotsta_sec", ColumnDescription = "COTSTA_SEC")]
        public int Secuencia {
            get { return this._COTSTA_SEC; }
            set {
                this._COTSTA_SEC = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTSTA_USU_FM")]
        [SugarColumn(ColumnName = "cotsta_usu_fm", ColumnDescription = "COTSTA_USU_FM")]
        public int COTSTA_USU_FM {
            get { return this._COTSTA_USU_FM; }
            set {
                this._COTSTA_USU_FM = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTSTA_FM")]
        [SugarColumn(ColumnName = "cotsta_fm", ColumnDescription = "COTSTA_FM")]
        public DateTime? FechaModifica {
            get { 
                if (this._COTSTA_FM >= new DateTime(1900, 1, 1))
                    return this._COTSTA_FM;
                return null;
            }
            set {
                this._COTSTA_FM = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Descriptor {
            get { return string.Format("{0}: {1}", this.IdCotStatus.ToString("00"), this.Descripcion); }
        }
    }
}
