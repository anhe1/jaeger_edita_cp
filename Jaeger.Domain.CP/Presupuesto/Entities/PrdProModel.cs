﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;
using System;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Línea de Producción (prdpro)
    ///</summary>
    [SugarTable("prdpro")]
    public partial class PrdProModel : BasePropertyChangeImplementation, IPrdProModel {
        private int index;
        private int activo;
        private int _PRDPRO_PRDCOM_ID;
        private int _PRDPRO_PRO_ID;
        private int _PRDPRO_SEC;
        private decimal _PRDPRO_CAN_VP;
        private decimal _PRDPRO_CAN_CP;
        private int _PRDPRO_USU_FM;
        private DateTime? _PRDPRO_FM;
        private int _PRDPRO_COT_ID;

        public PrdProModel() {
            this.activo = 1;
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New: Nullable_New:False
        /// </summary>
        [DataNames("PRDPRO_ID")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "prdpro_id", ColumnDescription = "")]
        public int PRDPRO_ID {
            get {
                return index;
            }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_A")]
        [SugarColumn(ColumnName = "prdpro_a", ColumnDescription = "")]
        public int Activo {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_PRDCOM_ID")]
        [SugarColumn(ColumnName = "prdpro_prdcom_id", ColumnDescription = "")]
        public int PRDPRO_PRDCOM_ID {
            get {
                return _PRDPRO_PRDCOM_ID;
            }
            set {
                _PRDPRO_PRDCOM_ID = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_PRO_ID")]
        [SugarColumn(ColumnName = "prdpro_pro_id", ColumnDescription = "")]
        public int PRDPRO_PRO_ID {
            get {
                return _PRDPRO_PRO_ID;
            }
            set {
                _PRDPRO_PRO_ID = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_SEC")]
        [SugarColumn(ColumnName = "prdpro_sec", ColumnDescription = "")]
        public int Secuencia {
            get {
                return _PRDPRO_SEC;
            }
            set {
                _PRDPRO_SEC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_CAN_VP")]
        [SugarColumn(ColumnName = "prdpro_can_vp", ColumnDescription = "")]
        public decimal PRDPRO_CAN_VP {
            get {
                return _PRDPRO_CAN_VP;
            }
            set {
                _PRDPRO_CAN_VP = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_CAN_CP")]
        [SugarColumn(ColumnName = "prdpro_can_cp", ColumnDescription = "")]
        public decimal PRDPRO_CAN_CP {
            get {
                return _PRDPRO_CAN_CP;
            }
            set {
                _PRDPRO_CAN_CP = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_USU_FM")]
        [SugarColumn(ColumnName = "prdpro_usu_fm", ColumnDescription = "")]
        public int PRDPRO_USU_FM {
            get {
                return _PRDPRO_USU_FM;
            }
            set {
                _PRDPRO_USU_FM = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_FM")]
        [SugarColumn(ColumnName = "prdpro_fm", ColumnDescription = "")]
        public DateTime? PRDPRO_FM {
            get {
                return _PRDPRO_FM;
            }
            set {
                _PRDPRO_FM = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PRDPRO_COT_ID")]
        [SugarColumn(ColumnName = "prdpro_cot_id", ColumnDescription = "")]
        public int PRDPRO_COT_ID {
            get {
                return _PRDPRO_COT_ID;
            }
            set {
                _PRDPRO_COT_ID = value;
                OnPropertyChanged();
            }
        }
    }
}
