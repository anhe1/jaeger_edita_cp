﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using Jaeger.Domain.Services.Mapping;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Línea de Producción (prdpro)
    ///</summary>
    public class PrdProDetailModelView : PrdProModel, IPrdProModel {
        private BindingList<PrdMpDetailModelView> materiaPrima;
        private BindingList<ProMpDetailModelView> consumibles;
        private BindingList<ProcesoEtapaDetailModel> subProcesos;

        public PrdProDetailModelView() {
            materiaPrima = new BindingList<PrdMpDetailModelView>();
            consumibles = new BindingList<ProMpDetailModelView>();
            subProcesos = new BindingList<ProcesoEtapaDetailModel>();
        }

        #region departamento

        [DataNames("EDP1_nom")]
        public string Departamento {
            get; set;
        }

        [DataNames("EDP2_sec")]
        public string Seccion {
            get; set;
        }

        [DataNames("EDP2_nom")]
        public string Centro {
            get; set;
        }

        [DataNames("EDP3_nom")]
        public string Proceso {
            get; set;
        }

        [DataNames("EDP2_val_min")]
        public decimal ValorMinuto {
            get; set;
        }

        #endregion

        /// <summary>
        /// Materia Prima
        /// </summary>
        public BindingList<PrdMpDetailModelView> MateriaPrima {
            get {
                return materiaPrima;
            }
            set {
                materiaPrima = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Materia Prima Consumibles
        /// </summary>
        public BindingList<ProMpDetailModelView> Consumibles {
            get {
                return consumibles;
            }
            set {
                consumibles = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// SubProcesos (EDP4)
        /// </summary>
        public BindingList<ProcesoEtapaDetailModel> SubProcesos {
            get {
                return subProcesos;
            }
            set {
                subProcesos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
