﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// COTPRO
    ///</summary>
    [SugarTable("cotpro")]
    public partial class CotProModel : BasePropertyChangeImplementation, ICotProModel {
        private int index;
        private int activo;
        private int _COTPRO_PRDPRO_ID;
        private int _COTPRO_COTPRD_ID;
        private int _COTPRO_PRO_ID;
        private int _COTPRO_COTCOM_ID;
        private int _COTPRO_SEC;
        private decimal _COTPRO_CAN_VP;
        private decimal _COTPRO_CAN_CP;
        private int _COTPRO_CAN_COM;
        private int _COTPRO_CAN_PRO;
        private int _COTPRO_CAN_PROS;
        private int _COTPRO_CAN_PROT;
        private int _COTPRO_T_MAX;
        private int _COTPRO_NT;
        private decimal _COTPRO__CEN;
        private decimal _COTPRO__PRO;
        private decimal _COTPRO__MAT;
        private decimal _COTPRO__CNS;
        private int _COTPRO_USU_FM;
        private DateTime? _COTPRO_FM;
        private decimal _COTPRO_ST;

        public CotProModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer (COTPRO_ID)
        /// </summary>
        [DataNames("COTPRO_ID")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "cotpro_id", ColumnDescription = "indice de la tabla")]
        public int COTPRO_ID {
            get {
                return index;
            }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (COTPRO_A)
        /// </summary>
        [DataNames("COTPRO_A")]
        [SugarColumn(ColumnName = "cotpro_a", ColumnDescription = "registro activo")]
        public int COTPRO_A {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_PRDPRO_ID")]
        [SugarColumn(ColumnName = "cotpro_prdpro_id", ColumnDescription = "")]
        public int COTPRO_PRDPRO_ID {
            get {
                return _COTPRO_PRDPRO_ID;
            }
            set {
                _COTPRO_PRDPRO_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_COTPRD_ID")]
        [SugarColumn(ColumnName = "cotpro_cotprd_id", ColumnDescription = "")]
        public int COTPRO_COTPRD_ID {
            get {
                return _COTPRO_COTPRD_ID;
            }
            set {
                _COTPRO_COTPRD_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_PRO_ID")]
        [SugarColumn(ColumnName = "cotpro_pro_id", ColumnDescription = "")]
        public int COTPRO_PRO_ID {
            get {
                return _COTPRO_PRO_ID;
            }
            set {
                _COTPRO_PRO_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_COTCOM_ID")]
        [SugarColumn(ColumnName = "cotpro_cotcom_id", ColumnDescription = "")]
        public int COTPRO_COTCOM_ID {
            get {
                return _COTPRO_COTCOM_ID;
            }
            set {
                _COTPRO_COTCOM_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_SEC")]
        [SugarColumn(ColumnName = "cotpro_sec", ColumnDescription = "")]
        public int Secuencia {
            get {
                return _COTPRO_SEC;
            }
            set {
                _COTPRO_SEC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_CAN_VP")]
        [SugarColumn(ColumnName = "cotpro_can_vp", ColumnDescription = "")]
        public decimal COTPRO_CAN_VP {
            get {
                return _COTPRO_CAN_VP;
            }
            set {
                _COTPRO_CAN_VP = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_CAN_CP")]
        [SugarColumn(ColumnName = "cotpro_can_cp", ColumnDescription = "")]
        public decimal CantidadProceso {
            get {
                return _COTPRO_CAN_CP;
            }
            set {
                _COTPRO_CAN_CP = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_CAN_COM")]
        [SugarColumn(ColumnName = "cotpro_can_com", ColumnDescription = "")]
        public int COTPRO_CAN_COM {
            get {
                return _COTPRO_CAN_COM;
            }
            set {
                _COTPRO_CAN_COM = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_CAN_PRO")]
        [SugarColumn(ColumnName = "cotpro_can_pro", ColumnDescription = "")]
        public int COTPRO_CAN_PRO {
            get {
                return _COTPRO_CAN_PRO;
            }
            set {
                _COTPRO_CAN_PRO = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_CAN_PROS")]
        [SugarColumn(ColumnName = "cotpro_can_pros", ColumnDescription = "")]
        public int COTPRO_CAN_PROS {
            get {
                return _COTPRO_CAN_PROS;
            }
            set {
                _COTPRO_CAN_PROS = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_CAN_PROT")]
        [SugarColumn(ColumnName = "cotpro_can_prot", ColumnDescription = "")]
        public int CantidadTotal {
            get {
                return _COTPRO_CAN_PROT;
            }
            set {
                _COTPRO_CAN_PROT = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_T_MAX")]
        [SugarColumn(ColumnName = "cotpro_t_max", ColumnDescription = "")]
        public int Minutos {
            get {
                return _COTPRO_T_MAX;
            }
            set {
                _COTPRO_T_MAX = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer COTPRO_NT
        /// </summary>
        [DataNames("COTPRO_NT")]
        [SugarColumn(ColumnName = "cotpro_nt", ColumnDescription = "")]
        public int OperadoresEstimados {
            get {
                return _COTPRO_NT;
            }
            set {
                _COTPRO_NT = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_$_CEN")]
        [SugarColumn(ColumnName = "cotpro_$_cen", ColumnDescription = "")]
        public decimal Centro1 {
            get {
                return _COTPRO__CEN;
            }
            set {
                _COTPRO__CEN = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_$_PRO")]
        [SugarColumn(ColumnName = "cotpro_$_pro", ColumnDescription = "")]
        public decimal ManoObra {
            get {
                return _COTPRO__PRO;
            }
            set {
                _COTPRO__PRO = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer COTPRO_$_MAT
        /// </summary>
        [DataNames("COTPRO_$_MAT")]
        [SugarColumn(ColumnName = "cotpro_$_mat", ColumnDescription = "")]
        public decimal MateriaPrima1 {
            get {
                return _COTPRO__MAT;
            }
            set {
                _COTPRO__MAT = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer COTPRO_$_CNS
        /// </summary>
        [DataNames("COTPRO_$_CNS")]
        [SugarColumn(ColumnName = "cotpro_$_cns", ColumnDescription = "")]
        public decimal Consumibles {
            get {
                return _COTPRO__CNS;
            }
            set {
                _COTPRO__CNS = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_USU_FM")]
        [SugarColumn(ColumnName = "cotpro_usu_fm", ColumnDescription = "")]
        public int COTPRO_USU_FM {
            get {
                return _COTPRO_USU_FM;
            }
            set {
                _COTPRO_USU_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_FM")]
        [SugarColumn(ColumnName = "cotpro_fm", ColumnDescription = "")]
        public DateTime? COTPRO_FM {
            get {
                return _COTPRO_FM;
            }
            set {
                _COTPRO_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("COTPRO_$ST")]
        [SugarColumn(ColumnName = "cotpro_$st", ColumnDescription = "")]
        public decimal COTPRO_ST {
            get {
                return _COTPRO_ST;
            }
            set {
                _COTPRO_ST = value;
                OnPropertyChanged();
            }
        }
    }
}
