﻿using System.ComponentModel;
using Jaeger.Domain.CP.Empresa.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// vista de productos de la tabla com
    /// </summary>
    public class ComProductoModelView : ComercioModel, IComercioModel {
        private BindingList<LP2ModelView> subproductos;

        public ComProductoModelView() {

        }

        public BindingList<LP2ModelView> SubProductos {
            get { return this.subproductos; }
            set {
                this.subproductos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
