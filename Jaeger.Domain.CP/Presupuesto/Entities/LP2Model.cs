﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// Tabla - LP2
    /// </summary>
    public class LP2Model : BasePropertyChangeImplementation, ILP2Model {
        private int _LP2_ID;
        private int _LP2_A;
        private int _LP2_LP1_ID;
        private int _LP2_LC1_ID;
        private DateTime? fechaModifica;
        private int _LP2_COM_ID;
        private int _LP2_COM1_ID;

        public LP2Model() {
            this._LP2_A = 1;
        }

        /// <summary>
        /// obtener o establecer (LP2_ID)
        /// </summary>
        [DataNames("LP2_ID")]
        [SugarColumn(ColumnName = "lp2_id", ColumnDescription = "LP2_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdLP2 {
            get { return this._LP2_ID; }
            set {
                this._LP2_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LP2_A)
        /// </summary>
        [DataNames("LP2_A")]
        [SugarColumn(ColumnName = "lp2_a", ColumnDescription = "LP2_A")]
        public int Activo {
            get { return this._LP2_A; }
            set {
                this._LP2_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LP2_LP1_ID)
        /// </summary>
        [DataNames("LP2_LP1_ID")]
        [SugarColumn(ColumnName = "lp2_lp1_id", ColumnDescription = "LP2_LP1_ID")]
        public int LP2_LP1_ID {
            get { return this._LP2_LP1_ID; }
            set {
                this._LP2_LP1_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LP2_LC1_ID)
        /// </summary>
        [DataNames("LP2_LC1_ID")]
        [SugarColumn(ColumnName = "lp2_lc1_id", ColumnDescription = "LP2_LC1_ID")]
        public int LP2_LC1_ID {
            get { return this._LP2_LC1_ID; }
            set {
                this._LP2_LC1_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LP2_FM)
        /// </summary>
        [DataNames("LP2_FM")]
        [SugarColumn(ColumnName = "lp2_fm", ColumnDescription = "LP2_FM")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LP2_COM_ID)
        /// </summary>
        [DataNames("LP2_COM_ID")]
        [SugarColumn(ColumnName = "lp2_com_id", ColumnDescription = "LP2_COM_ID")]
        public int LP2_COM_ID {
            get { return this._LP2_COM_ID; }
            set {
                this._LP2_COM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LP2_COM1_ID)
        /// </summary>
        [DataNames("LP2_COM1_ID")]
        [SugarColumn(ColumnName = "lp2_com1_id", ColumnDescription = "LP2_COM1_ID")]
        public int LP2_COM1_ID {
            get { return this._LP2_COM1_ID; }
            set {
                this._LP2_COM1_ID = value;
                this.OnPropertyChanged();
            }
        }
    }
}
