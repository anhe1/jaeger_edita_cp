﻿using System.ComponentModel;
using Jaeger.Domain.CP.Empresa.Contracts;
using Jaeger.Domain.CP.Empresa.Entities;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// clase de subproductos de venta, clase derivada de ComercioModel (tabla COM)
    /// </summary>
    public class SubProductoDetailModel : ComercioModel, IComercioModel {
        private BindingList<Lc2Model> lC2Models;
        private BindingList<LineaProduccionView> lineaProduccion;

        public SubProductoDetailModel() {
            this.Especificaciones = new BindingList<Lc2Model>();
            this.LineaProduccion = new BindingList<LineaProduccionView>();
        }

        public BindingList<Lc2Model> Especificaciones {
            get { return this.lC2Models; }
            set {
                this.lC2Models = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<LineaProduccionView> LineaProduccion {
            get { return this.lineaProduccion; }
            set {
                this.lineaProduccion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
