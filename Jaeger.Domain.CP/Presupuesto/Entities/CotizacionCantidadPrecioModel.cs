﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;
using System;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// Cantidades y Precios (COTPRD)
    /// </summary>
    [SugarTable("COTPRD", "presupuesto: cantidades y precios")]
    public class CotizacionCantidadPrecioModel : BasePropertyChangeImplementation, ICotizacionCantidadPrecioModel {
        private int index;
        private int activo;
        private int idPresupuesto;
        private string modelo;
        private string descripcion;
        private int cantidad;
        private int secuencia;
        private decimal unitario;
        private decimal unidad;
        private decimal total;
        private decimal utilidad;
        private decimal costoFijo;
        private decimal subTotal2;
        private decimal costoIndirecto;
        private decimal gastoAdministracion;
        private decimal gastoFinanciero;
        private decimal gastoVenta;
        private decimal costoDirecto;
        private DateTime? fechaModifica;

        public CotizacionCantidadPrecioModel() {
            this.activo = 1;
            this.fechaModifica = DateTime.Now;
            this.unitario = new decimal(1);
            this.unidad = new decimal(1);
            this.utilidad = new decimal(.30);
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (cotprd_id)
        /// </summary>           
        [DataNames("cotprd_id")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "cotprd_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsNullable = false)]
        public int IdCotPrd {
            get {
                return index;
            }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>           
        [DataNames("cotprd_a")]
        [SugarColumn(ColumnName = "cotprd_a", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public int Activo {
            get {
                return activo;
            }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla presupuestos (cotprd_cot_id)
        /// </summary>           
        [DataNames("cotprd_cot_id")]
        [SugarColumn(ColumnName = "cotprd_cot_id", ColumnDescription = "indice de relacion con la tabla presupuestos (COT_ID)")]
        public int IdCotizacion {
            get {
                return idPresupuesto;
            }
            set {
                idPresupuesto = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (cotprd_prd_id)
        /// </summary>           
        [SugarColumn(ColumnName = "cotprd_prd_id", ColumnDescription = "", IsNullable = true)]
        public int? COTPRD_PRD_ID {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el modelo (cotprd_nom)
        /// </summary>           
        [DataNames("cotprd_nom")]
        [SugarColumn(ColumnName = "cotprd_nom", ColumnDescription = "modelo", Length = 50, IsNullable = true)]
        public string Modelo {
            get {
                return modelo;
            }
            set {
                modelo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion (cotprd_des)
        /// </summary>           
        [DataNames("cotprd_des")]
        [SugarColumn(ColumnName = "cotprd_des", ColumnDescription = "descripcion", Length = 255)]
        public string Descripcion {
            get {
                return descripcion;
            }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad (cotprd_can)
        /// </summary>           
        [DataNames("cotprd_can")]
        [SugarColumn(ColumnName = "cotprd_can", ColumnDescription = "")]
        public int Cantidad {
            get {
                return cantidad;
            }
            set {
                cantidad = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer secuencia (cotprd_sec)
        /// </summary>           
        [DataNames("cotprd_sec")]
        [SugarColumn(ColumnName = "cotprd_sec", ColumnDescription = "")]
        public int Secuencia {
            get {
                return secuencia;
            }
            set {
                secuencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor unitario (cotprd_$_uni)
        /// </summary>           
        [DataNames("cotprd_$_uni")]
        [SugarColumn(ColumnName = "cotprd_untr", ColumnDescription = "valor unitario", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Unitario {
            get {
                return unitario;
            }
            set {
                unitario = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad (cotprd_uni)
        /// </summary>           
        [DataNames("cotprd_uni")]
        [SugarColumn(ColumnName = "cotprd_uni", ColumnDescription = "unidad", DefaultValue = "1", Length = 18, DecimalDigits = 4)]
        public decimal Unidad {
            get {
                return unidad;
            }
            set {
                unidad = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total (cotprd_$)
        /// </summary>           
        [DataNames("cotprd_$")]
        [SugarColumn(ColumnName = "cotprd_ttl", ColumnDescription = "Total", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Total {
            get {
                return total;
            }
            set {
                total = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cotprd_tie
        /// </summary>           
        [DataNames("cotprd_tie")]
        [SugarColumn(ColumnName = "cotprd_tie", ColumnDescription = "", Length = 18, DecimalDigits = 4, IsNullable = true)]
        public decimal TiempoEstimado {
            get; set;
        }

        /// <summary>
        /// obtener o establecer % de utilidad (cotprd_uti)
        /// </summary>           
        [DataNames("cotprd_uti")]
        [SugarColumn(ColumnName = "cotprd_uti", ColumnDescription = "utilidad", Length = 18, DecimalDigits = 4)]
        public decimal Utilidad {
            get {
                return utilidad;
            }
            set {
                utilidad = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costos fijos (cotprd_$_fij)
        /// </summary>           
        [DataNames("cotprd_$_fij")]
        [SugarColumn(ColumnName = "cotprd_fij", ColumnDescription = "", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Fijo {
            get {
                return costoFijo;
            }
            set {
                costoFijo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer $2º SubT (cotprd_$_st2)
        /// </summary>           
        [DataNames("cotprd_$_st2")]
        [SugarColumn(ColumnName = "cotprd_sbttl2", ColumnDescription = "", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal SubTotal2 {
            get {
                return subTotal2;
            }
            set {
                subTotal2 = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costos indirectos (cotprd_$_ind)
        /// </summary>           
        [DataNames("cotprd_$_ind")]
        [SugarColumn(ColumnName = "cotprd_indrct", ColumnDescription = "costo indirecto", Length = 18, DecimalDigits = 4, DefaultValue = "0", IsNullable = true)]
        public decimal CostoIndirecto {
            get {
                return costoIndirecto;
            }
            set {
                costoIndirecto = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer gastos administrativos (cotprd_$_adm)
        /// </summary>           
        [DataNames("cotprd_$_adm")]
        [SugarColumn(ColumnName = "cotprd_gstadm", ColumnDescription = "gastos administrativos", Length = 18, DecimalDigits = 4, DefaultValue = "0", IsNullable = true)]
        public decimal GastoAdmon {
            get {
                return gastoAdministracion;
            }
            set {
                gastoAdministracion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer gastos financieros (cotprd_$_fin)
        /// </summary>           
        [DataNames("cotprd_$_fin")]
        [SugarColumn(ColumnName = "cotprd_gstfin", ColumnDescription = "gastos financieros", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal GastoFin {
            get {
                return gastoFinanciero;
            }
            set {
                gastoFinanciero = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer gastos de venta (cotprd_$_ven)
        /// </summary>           
        [DataNames("cotprd_$_ven")]
        [SugarColumn(ColumnName = "cotprd_gstven", ColumnDescription = "gastos de venta", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal GastoVenta {
            get {
                return gastoVenta;
            }
            set {
                gastoVenta = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costos directos (cotprd_$_st1)
        /// </summary>           
        [DataNames("cotprd_$_st1")]
        [SugarColumn(ColumnName = "cotprd_cstdir", ColumnDescription = "costos directos", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal CostoDirecto {
            get {
                return costoDirecto;
            }
            set {
                costoDirecto = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (cotprd_$_cen)
        /// </summary>           
        [DataNames("cotprd_$_cen")]
        [SugarColumn(ColumnName = "cotprd_cen", ColumnDescription = "", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal COTPRD_CEN {
            get; set;
        }

        /// <summary>
        /// obtener o establecer cotprd_$_pro
        /// </summary>           
        [DataNames("cotprd_$_pro")]
        [SugarColumn(ColumnName = "cotprd_pro", ColumnDescription = "", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal COTPRD_PRO {
            get; set;
        }

        /// <summary>
        /// obtener o establecer cotprd_$_mp
        /// </summary>           
        [DataNames("cotprd_$_mp")]
        [SugarColumn(ColumnName = "cotprd_mp", ColumnDescription = "", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal COTPRD_MP {
            get; set;
        }

        /// <summary>
        /// obtener o establecer cotprd_$_cns
        /// </summary>           
        [DataNames("cotprd_$_cns")]
        [SugarColumn(ColumnName = "cotprd_cns", ColumnDescription = "", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal COTPRD_CNS {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("cotprd_fm")]
        [SugarColumn(ColumnName = "cotprd_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
