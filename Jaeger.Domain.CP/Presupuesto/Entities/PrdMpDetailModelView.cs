﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Materia Prima (PRDMP)
    ///</summary>
    public class PrdMpDetailModelView : PrdMpModel, IPrdMpModel {

        public PrdMpDetailModelView() {
            this.Activo = 1;
        }

        #region comercio lista

        [DataNames("Com3_Cod")]
        public int Codigo {
            get; set;
        }

        [DataNames("Com3_N1")]
        public string Cla1 {
            get; set;
        }

        [DataNames("Com3_N2")]
        public string Cla2 {
            get; set;
        }

        [DataNames("Com3_N3")]
        public string Cla3 {
            get; set;
        }

        #endregion

        #region modelo

        [DataNames("ComMod_Mod")]
        public string Modelo {
            get; set;
        }

        [DataNames("ComMod_nom")]
        public string Especificacion {
            get; set;
        }
        [DataNames("ComMod_Mar")]
        public string Marca {
            get; set;
        }
        [DataNames("ComMod_UniNom")]
        public string Unidad {
            get; set;
        }
        [DataNames("ComMod_$Uni")]
        public decimal Unitario {
            get; set;
        }
        [DataNames("ComMod_Mul")]
        public decimal Mul {
            get; set;
        }

        #endregion
    }
}
