﻿using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    ///<summary>
    /// Línea de Producción (prdpro)
    ///</summary>
    public class PrdProDetailModel : PrdProModel, IPrdProModel {
        private BindingList<PrdMpDetailModel> materiaPrima;
        private BindingList<ProMpDetailModel> consumibles;
        private BindingList<ProcesoEtapaDetailModel> subProcesos;

        public PrdProDetailModel() {
            materiaPrima = new BindingList<PrdMpDetailModel>();
            consumibles = new BindingList<ProMpDetailModel>();
            subProcesos = new BindingList<ProcesoEtapaDetailModel>();
        }

        /// <summary>
        /// Materia Prima
        /// </summary>
        public BindingList<PrdMpDetailModel> MateriaPrima {
            get {
                return materiaPrima;
            }
            set {
                materiaPrima = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Materia Prima Consumibles
        /// </summary>
        public BindingList<ProMpDetailModel> Consumibles {
            get {
                return consumibles;
            }
            set {
                consumibles = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// SubProcesos (EDP4)
        /// </summary>
        public BindingList<ProcesoEtapaDetailModel> SubProcesos {
            get {
                return subProcesos;
            }
            set {
                subProcesos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
