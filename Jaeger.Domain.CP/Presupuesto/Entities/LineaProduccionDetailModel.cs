﻿using System.ComponentModel;
using Jaeger.Domain.CP.Presupuesto.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using SqlSugar;

namespace Jaeger.Domain.CP.Presupuesto.Entities {
    /// <summary>
    /// Linea de produccion (LCPRO)
    /// </summary>
    [SugarTable("LCPRO", "Linea de producccion")]
    public class LineaProduccionDetailModel : LineaProduccionModel, ILineaProduccionModel {
        private BindingList<LcMpDetailModel> materiaPrima;
        private BindingList<ProMpDetailModel> procesoConsumibles;
        private BindingList<ProcesoEtapaDetailModel> subProcesos;

        public LineaProduccionDetailModel() {
            this.materiaPrima = new BindingList<LcMpDetailModel>();
            this.procesoConsumibles = new BindingList<ProMpDetailModel>();
            this.subProcesos = new BindingList<ProcesoEtapaDetailModel>();
        }

        public BindingList<LcMpDetailModel> MateriaPrima {
            get { return this.materiaPrima; }
            set {
                this.materiaPrima = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<ProMpDetailModel> Consumibles {
            get { return this.procesoConsumibles; }
            set {
                this.procesoConsumibles = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// EDP4
        /// </summary>
        public BindingList<ProcesoEtapaDetailModel> SubProcesos {
            get { return this.subProcesos; }
            set {
                this.subProcesos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
