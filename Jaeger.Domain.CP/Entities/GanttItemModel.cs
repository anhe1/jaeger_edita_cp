﻿using System;

namespace Jaeger.Domain.Firebird.Entities {
    /// <summary>
    /// item
    /// </summary>
    public class GanttItemModel {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Progress { get; set; }
    }

}
