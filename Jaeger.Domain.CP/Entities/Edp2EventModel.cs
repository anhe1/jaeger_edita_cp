﻿using System;
using System.Drawing;

namespace Jaeger.Domain.CP.Entities {
    public class Edp2EventModel {
        private string sCentro = string.Empty;
        private Color oColor = Color.AliceBlue;
        private DateTime oDay = DateTime.Now;
        private TimeSpan oTime = new TimeSpan(7, 0, 0);
        private long lEmployeeId = 0;
        private string sEmployee = string.Empty;
        private object mObject = new object();

        public int Id { get; set; }

        public long EmployeeId {
            get {
                return this.lEmployeeId;
            }
            set {
                this.lEmployeeId = value;
            }
        }
        public string Employee {
            get {
                return this.sEmployee;
            }
            set {
                this.sEmployee = value;
            }
        }
        public string Name {
            get {
                return this.sCentro;
            }
            set {
                this.sCentro = value;
            }
        }
        public Color Color {
            get {
                return this.oColor;
            }
            set {
                this.oColor = value;
            }
        }
        public DateTime Day {
            get {
                return this.oDay;
            }
            set {
                this.oDay = value;
            }
        }
        public object EventID {
            get {
                return this.mObject;
            }
            set {
                this.mObject = value;
            }
        }
        public TimeSpan Time {
            get {
                return this.oTime;
            }
            set {
                this.oTime = value;
            }
        }
    }
}
