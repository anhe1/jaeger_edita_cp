﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;

namespace Jaeger.Domain.CP.Produccion.ValueObjects {
    public enum PedFechaAcuerdoMotivoEnum {
        [Description("No Definido")]
        NoDefinido = 0,
        [Description("Cliente Acuerdo")]
        ClienteAcuerdo = 1,
        [Description("4.-Materiales")]
        Materiales = 3,
        [Description("Cliente Detiene")]
        ClienteDetiene = 4,
        [Description("6.-Método")]
        Metodo = 5,
        [Description("2.-Maquila")]
        Maquila = 7,
        [Description("3.-Mano de Obra")]
        ManoDeObra = 8,
        [Description("5.-Medio Ambiente")]
        MedioAmbiente = 9,
        [Description("Cliente Urgencia")]
        ClienteUrgencia = 11,
        [Description("1.-Maquinaria")]
        Maquinaria = 12,
        [Description("7.-Acción Preventiva")]
        AccionPreventiva = 15,
        [Description("8.-Acción Correctiva")]
        AccionCorrectiva = 16,
        [Description("0.-No Conformidad")]
        NoConformidad = 17
    }
}
