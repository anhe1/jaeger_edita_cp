﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;

namespace Jaeger.Domain.CP.Produccion.ValueObjects {
    /// <summary>
    /// fechas de orden de produccion
    /// </summary>
    public enum PedPrdFechaEnum {
        /// <summary>
        /// fecha de pedido
        /// </summary>
        [Description("Fecha Pedido")]
        Pedido = 1,

        /// <summary>
        /// fecha de orden de produccion
        /// </summary>
        [Description("Fecha OP")]
        Orden = 2,

        /// <summary>
        /// fecha de acuerdo
        /// </summary>
        [Description("Fecha Acuerdo")]
        Acuerdo = 3,

        /// <summary>
        /// fecha de visto bueno
        /// </summary>
        [Description("Fecha VoBo")]
        VoBo = 4,

        /// <summary>
        /// fecha de visto bueno de calidad
        /// </summary>
        [Description("Fecha VoBo Calidad")]
        VoBoC = 5
    }
}
