﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;

namespace Jaeger.Domain.CP.Produccion.ValueObjects {
    /// <summary>
    /// Status de ordenes de producción (PEDST)
    /// </summary>
    public enum PedPrdStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Pedido")]
        Pedido = 1,
        [Description("Detenido")]
        Detenido = 3,
        [Description("Pd")]
        Pd = 5,
        [Description("Terminado")]
        Terminado = 9,
        [Description("Remisionado")]
        Remisionado = 10,
        [Description("Facturado")]
        Facturado = 11,
        [Description("Cobrado")]
        Cobrado = 12,
        [Description("Remisionado2")]
        Remisionado2 = 13,
        [Description("PrePrensa")]
        PrePrensa = 6,
        [Description("Prensa")]
        Prensa = 7,
        [Description("PostPrensa")]
        PostPrensa = 8,
        [Description("Exc-Vender")]
        Exc_Vender = 17,
        [Description("Exc-Obsequio")]
        Exc_Obsequio = 18,
        [Description("Exc-Tirar")]
        Exc_Tirar = 19,
        [Description("Resguardo")]
        Resguardo = 20,
        [Description("Diseño")]
        Diseno = 23,
        [Description("Sin Mat. Prim.")]
        Sin_Mat_Prim = 24,
        [Description("Laminado")]
        Laminado = 21,
        [Description("Estampado")]
        Estampado = 22,
        [Description("Maquila")]
        Maquila = 25,
        [Description("Cancelado2")]
        Cancelado2 = 26,
        [Description("Bolsa")]
        Bolsa = 27
    }
}
