﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// clase componentes / ensambles de la orden de produccion (PedCom)
    /// </summary>
    public interface ISqlPedComRepository : IGenericRepository<ComponenteModel> {
        IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener lista de componentes asociados a una orden (PedCom_PedPrd_ID=PedPrd_ID)
        /// </summary>
        IEnumerable<ComponenteDetailModel> GetList(int idOrden);

        /// <summary>
        /// obtener lista de componentes asociados a una orden (PedCom_PedPrd_ID=PedPrd_ID)
        /// </summary>
        IEnumerable<ComponenteDetailModel> GetList(int[] idOrden);

        IEnumerable<ComponenteView> GetListView(int[] idOrden);
    }
}
