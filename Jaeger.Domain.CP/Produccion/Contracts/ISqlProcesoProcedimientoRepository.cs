﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// interface del repositorio para procedimientos en los procesos
    /// </summary>
    public interface ISqlProcesoProcedimientoRepository : IGenericRepository<ProcesoProcedimientoModel> {
        /// <summary>
        /// obtener listado de procedimiento, solo activos y array de indices EDP4_ID
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        /// <param name="indexs">array de indices EPD4_ID</param>
        /// <returns></returns>
        IEnumerable<ProcesoProcedimientoModel> GetList(bool onlyActive, int[] indexs);
    }
}
