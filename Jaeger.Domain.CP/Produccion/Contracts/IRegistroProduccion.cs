﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    public interface IRegistroProduccion {
        int IdPedPro { get; set; }

        int TiempoReal { get; set; }

        int CantidadReal { get; set; }

        int NumeroTrabajdoresReal { get; set; }

        string Oficial { get; set; }

        string Nota { get; set; }

        string Modifica { get; set; }

        DateTime? FechaFin { get; set; }

        DateTime? FechaModifica { get; set; }

        int Id5Clasificacion { get; set; }
    }
}
