﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Ordenes de Produccion (pedprd)
    /// </summary>
    public interface ISqlPedPrdRepository : IGenericRepository<OrdenProduccionModel> {
        OrdenProduccion GetBy(int index);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener listado de productos vendidos por periodos
        /// </summary>
        /// <param name="startDate">periodo inicial, si el valor endDate es nulo se toma por año y mes</param>
        /// <param name="endDate">fin del periodo</param>
        /// <param name="startStatus">status inicial en el caso de que endStatus es nulo entonces no es un rango sino status especifico</param>
        /// <param name="endStatus">fin del rango de status</param>
        IEnumerable<PedPrdPedidoModelView> GetList(DateTime startDate, DateTime? endDate = null, int? startStatus = null, int? endStatus = null);

        /// <summary>
        /// actualizar el estado de la verificacion de la orden de produccion
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="user">clave del usuario que autoriza</param>
        /// <param name="value">bandera: 0 - No Verificado, 1 - Verificado</param>
        /// <returns>verdadero si se actualiza correctamente</returns>
        bool Update(int index, string user, int value = 1);

        bool SetFechaVoBo(int idPedPro, DateTime fechaVoBo);

        bool SetFechaLibera(int idPedPro, DateTime fechaVoBo);

        IEnumerable<PedPrdPedidoModelView> GetListC(int year);

        bool Update(IOrdenProduccionC model);
    }
}
