﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Catalogo de lista de verificacion del proceso en cp (CHKLST)
    /// </summary>
    public interface ISqlCheckListRepository : IGenericRepository<CheckListModel> {
        /// <summary>
        /// listado detail de las lista de verificacion con lista de posibles fallas
        /// </summary>
        IEnumerable<CheckListDetailModel> GetList(int[] indexs);
    }
}
