﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Orden de Producción (pedprd)
    /// </summary>
    public interface IOrdenProduccionModel {
        /// <summary>
        /// obtener o establecer Orden de Produccion (PEDPRD_ID)
        /// </summary>
        int IdPedPrd { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_DIR_ID)
        /// </summary>
        int IdCliente { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_DIRCON_ID)
        /// </summary>
        int IdContacto { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_ST_ID)
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_VEN_ID)
        /// </summary>
        int IdVendedor { get; set; }

        /// <summary>
        /// obtener o establecer la jerarquia (pedprd_jer)
        /// </summary>
        int Jerarquia { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad total solicitada (PEDPRD_CAN)
        /// </summary>
        int Cantidad { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_CLA1)
        /// </summary>
        int PEDPRD_CLA1 { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_CLA2)
        /// </summary>
        int PEDPRD_CLA2 { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_COM_ID
        /// </summary>
        int PEDPRD_COM_ID { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_LP_ID
        /// </summary>
        int PEDPRD_LP_ID { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_PED_ID)
        /// </summary>
        int PEDPRD_PED_ID { get; set; }

        /// <summary>
        /// obtener o establecer  PEDPRD_SEC
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer numero de cotizacion (PEDPRD_COTPRD_ID)
        /// </summary>
        int Cotizacion { get; set; }

        /// <summary>
        /// obtener o establecer si el precio unitario esta verificado (PEDPRD_TAFRM_ID)
        /// </summary>
        int IdVerificado { get; set; }

        /// <summary>
        /// obtener o establecer el total de la venta de la orden de produccion o pedido (PEDPRD_$PED)
        /// </summary>
        decimal TotalVenta { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad acumulada remisionada (PEDPRD_REM)
        /// </summary>
        decimal Remisionado { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_$REM
        /// </summary>
        decimal TotalRemisionado { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_$REMP 
        /// </summary>
        decimal PEDPRD_REMP { get; set; }

        /// <summary>
        /// obtener o establecer la utilidad del pedido u orden de produccion (PEDPRD_$UTI)
        /// </summary>
        decimal Utilidad { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_UNI)
        /// </summary>
        decimal Unidad { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_$UNI
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer el total del costo (PEDPRD_$COT)
        /// </summary>
        decimal TotalCosto { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_FAC
        /// </summary>
        decimal PEDPRD_FAC { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_$FAC
        /// </summary>
        decimal PEDPRD_FAC2 { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_$FACP
        /// </summary>
        decimal PEDPRD_FACP { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del producto (PEDPRD_NOM)
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_OBS
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_USU_N
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_USU_M
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_FEC_PED)
        /// </summary>
        DateTime? FechaPedido { get; set; }

        /// <summary>
        /// obtener o establecer fecha de la orden de producción (PEDPRD_FEC_OP)
        /// </summary>
        DateTime? FechaOrden { get; set; }

        /// <summary>
        /// obtener o establecer fecha de solicitud del cliente (PEDPRD_FEC_REQ)
        /// </summary>
        DateTime? FechaCliente { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_FEC_ACO)
        /// </summary>
        DateTime? FechaAcuerdo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de emision de la primera remision PEDPRD_FEC_REM1
        /// </summary>
        DateTime? FechaRemision1 { get; set; }

        /// <summary>
        /// obtener o establecer establecer la fecha de emision de la ultima remision (PEDPRD_FEC_REM2)
        /// </summary>
        DateTime? FechaRemision2 { get; set; }

        /// <summary>
        /// obtener o establecer fecha de vobo de calidad (pedprd_fec_voboc)
        /// </summary>
        DateTime? FechaVobo { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_FEC_ALM2
        /// </summary>
        DateTime? FechaLiberacion { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_FEC_FPS
        /// </summary>
        DateTime? PEDPRD_FEC_FPS { get; set; }

        /// <summary>
        /// obtener o establecer fecha de recompra (pedprd_fec_rec)
        /// </summary>
        DateTime? FechaReCompra { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_FEC_VOBO
        /// </summary>
        DateTime? PEDPRD_FEC_VOBO { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_FM
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
