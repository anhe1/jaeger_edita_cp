﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// PUE
    /// </summary>
    public interface IPuestoModel {
        /// <summary>
        /// obtener o establecer el indce de la tabla (PUE_ID)
        /// </summary>
        int IdPuesto { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (PUE_A)
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// PUE_EDP1_ID
        /// </summary>
        int IdDepartamento { get; set; }

        /// <summary>
        /// PUE_SAL
        /// </summary>
        decimal Salario { get; set; }

        /// <summary>
        /// PUE_PREPRO_FAC
        /// </summary>
        decimal PremioProduccionFactor { get; set; }

        /// <summary>
        /// PUE_PREPRO_FIJ
        /// </summary>
        decimal PremioProduccionFijo { get; set; }

        /// <summary>
        /// PUE_PUE
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro PUE_FM
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
