﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Componentes/Ensambles (PedCom)
    /// </summary>
    public interface IPedComModel {
        /// <summary>
        /// obtener o establecer indice del Componentes o Ensambles (PEDCOM_ID)
        /// </summary>
        int IdComponente { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (pedcom_a)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_COTCOM_ID)
        /// </summary>
        int PEDCOM_COTCOM_ID { get; set; }

        /// <summary>
        /// obtener o establecer relacion con la orden de produccion (PedCom_PedPrd_ID)
        /// </summary>
        int IdPedPrd { get; set; }

        /// <summary>
        /// obtener o establecer PEDCOM_NUM
        /// </summary>
        int PEDCOM_NUM { get; set; }

        /// <summary>
        /// obtener o establecer secuencia del componente (PEDCOM_SEC)
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad del componente (PedCom_Can_Com)
        /// </summary>
        int Cantidad { get; set; }

        /// <summary>
        /// obtener o establecer el tiempo estimado en minutos (PedCom_T_Max)
        /// </summary>
        decimal TiempoEstimado { get; set; }

        /// <summary>
        /// obtener o establecer tiempo acumulado real (PEDCOM_MINR)
        /// </summary>
        int TiempoReal { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_USU_FM)
        /// </summary>
        int PEDCOM_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer clave (PedCom_Cla)
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del componente (PEDCOM_NOM)
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro (PEDCOM_USU_M)
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_MIN)
        /// </summary>
        DateTime? FechaMinima { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_MAX)
        /// </summary>
        DateTime? FechaMaxima { get; set; }

        /// <summary>
        /// obtener o establecer PEDCOM_FEC_INI
        /// </summary>
        DateTime? FechaInicial { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_FIN)
        /// </summary>
        DateTime? FechaFinal { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (PEDCOM_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
