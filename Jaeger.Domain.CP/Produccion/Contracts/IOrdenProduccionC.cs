﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    public interface IOrdenProduccionC {
        /// <summary>
        /// obtener o establecer Orden de Produccion (PEDPRD_ID)
        /// </summary>
        int IdPedPrd { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_DIR_ID)
        /// </summary>
        int IdCliente { get; set; }

        string Persona { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_DIRCON_ID)
        /// </summary>
        int IdContacto { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_ST_ID)
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_VEN_ID)
        /// </summary>
        int IdVendedor { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad total solicitada (PEDPRD_CAN)
        /// </summary>
        int Cantidad { get; set; }

        /// <summary>
        /// obtener o establecer si el precio unitario esta verificado (PEDPRD_TAFRM_ID)
        /// </summary>
        int IdVerificado { get; set; }

        /// <summary>
        /// obtener o establecer el total de la venta de la orden de produccion o pedido (PEDPRD_$PED)
        /// </summary>
        decimal TotalVenta { get; set; }

        /// <summary>
        /// obtener o establecer la utilidad del pedido u orden de produccion (PEDPRD_$UTI)
        /// </summary>
        decimal Utilidad { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_$UNI
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer el total del costo (PEDPRD_$COT)
        /// </summary>
        decimal TotalCosto { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del producto (PEDPRD_NOM)
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_OBS
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_USU_M
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_FEC_PED)
        /// </summary>
        DateTime? FechaPedido { get; set; }

        /// <summary>
        /// obtener o establecer fecha de solicitud del cliente (PEDPRD_FEC_REQ)
        /// </summary>
        DateTime? FechaCliente { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_FEC_ACO)
        /// </summary>
        DateTime? FechaAcuerdo { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRD_FM
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
