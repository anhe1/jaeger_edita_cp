﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    ///<summary>
    /// clase para el cambios de status de la orden de produccion (PEDFECR)
    ///</summary>
    public interface IPedFecRModel {
        /// <summary>
        /// obtener o establecer PEDFECR_ID
        /// </summary>
        int IdPedFecR { get; set; }

        /// <summary>
        /// obtener o establecer PEDFECR_A
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer PEDFECR_PEDPRD_ID
        /// </summary>
        int IdPedPrd { get; set; }

        /// <summary>
        /// obtener o establecer PEDFECR_ST_ID
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer PEDFECR_FECL_ID
        /// </summary>
        int PEDFECR_FECL_ID { get; set; }

        /// <summary>
        /// obtener o establecer PEDFECR_USU_FM
        /// </summary>
        int? PEDFECR_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer PEDFECR_OBS
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer PEDFECR_USU_N
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer (PEDFECR_FEC)
        /// </summary>
        DateTime Fecha { get; set; }

        /// <summary>
        /// obtener o establecer PEDFECR_USU_F
        /// </summary>
        DateTime? PEDFECR_USU_F { get; set; }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}