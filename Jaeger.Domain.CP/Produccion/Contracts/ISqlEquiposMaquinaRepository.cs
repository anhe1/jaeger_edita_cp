﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// interface del repositorio de Equipos y Maquinaria (EDP2)
    /// </summary>
    public interface ISqlEquiposMaquinaRepository : IGenericRepository<CentroModel> {
        /// <summary>
        /// listado de secciones y centros (EDP2)
        /// </summary>
        IEnumerable<SeccionCentroDetailModel> GetList(bool onlyActive, int[] idDeptos);

        /// <summary>
        /// obtener listado de centros productivos
        /// </summary>
        /// <param name="idDepartamento">indice del departamento</param>
        IEnumerable<CentroModel> GetList(int idDepartamento);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
