﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// repositorio de areas, departamentos EDP1
    /// </summary>
    public interface ISqlDepartamentosRepository : IGenericRepository<DepartamentoModel> {
        /// <summary>
        /// lisatado de departamentos, incluye centros productivos
        /// </summary>
        /// <param name="onlyActive">solor registros activos</param>
        IEnumerable<DepartamentoDetailModel> GetList(bool onlyActive);
    }
}
