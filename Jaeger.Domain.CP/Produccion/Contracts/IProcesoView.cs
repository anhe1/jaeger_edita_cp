﻿/// purpose:
/// develop: ANHE1 29092020 1713

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Vista de Línea de Producción con la combinacion de la vista del departamentos y centros (PedPro)
    /// </summary>
    public interface IProcesoView {
        /// <sumary>
        /// obtener o establecer (EDP3_ID)
        /// </sumary>
        int IdProceso { get; set; }
        
        int Activo { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP1_ARE)
        /// </sumary>
        string Area { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_NOM)
        /// </sumary>
        string Centro { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP1_NOM)
        /// </sumary>
        string Departamento { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_NOM)
        /// </sumary>
        string Proceso { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_SEC)
        /// </sumary>
        string Seccion { get; set; }

        string Completo { get; }
    }
}
