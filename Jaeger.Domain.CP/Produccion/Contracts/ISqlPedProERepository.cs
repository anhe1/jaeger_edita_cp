﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;
using System;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Línea de Producción (PedPro)
    /// </summary>
    public interface ISqlPedProERepository : IGenericRepository<LineaProduccionModelE> {
        /// <summary>
        /// obtener lista PedPro_Pro_Id
        /// </summary>
        IEnumerable<LineaProduccionModelE> GetList(int[] ProdID);

        /// <summary>
        /// obtener lista PedPro_PedCom_ID (solo vista)
        /// </summary>
        IEnumerable<LineaProduccionDeptoCentroView> GetListView(int[] ProdID);

        /// <summary>
        /// obtener listado de componente, linea de produccion
        /// </summary>
        IEnumerable<ComponenteLineaProduccionView> GetList(DateTime startDateTime, DateTime? endDateTime, int? startStatus, int? endStatus, int idDepartamento, int fieldDate = 0, bool onlyYear = false, bool onlyActive = false);

        /// <summary>
        /// actualizar avance de produccion
        /// </summary>
        bool UpdateAvance(LineaProduccionModelE item);

        /// <summary>
        /// actualizar la fecha planeada del proceso
        /// </summary>
        /// <param name="idPedPro">PedPro_ID</param>
        /// <param name="startDate">fecha de inicio</param>
        /// <param name="endDate">fecha final</param>
        /// <param name="user">clave del usuario</param>
        /// <returns>verdadero si actualiza la fecha</returns>
        bool Update(int idPedPro, DateTime startDate, DateTime endDate, string user);
    }
}
