﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Costos (COTPRO)
    /// </summary>
    public interface ISqlProcesoEtapaRepository : IGenericRepository<ProcesoEtapaModel> {
        /// <summary>
        /// obtener listado de etapas de proceso por indice EDP4_EDP3_ID
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        /// <param name="index">array de indices EDP4_EDP3_ID</param>
        /// <returns></returns>
        IEnumerable<ProcesoEtapaDetailModel> GetList(bool onlyActive, int[] index);
    }
}
