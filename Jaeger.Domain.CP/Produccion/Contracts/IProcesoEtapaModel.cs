﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// etapas del proceso (EDP4)
    /// </summary>
    public interface IProcesoEtapaModel {
        /// <summary>
        /// obtener o establecer el indice de la etapa de proceso (EDP4_ID)
        /// </summary>
        int IdEdp4 { get; set; }

        /// <summary>
        /// obtener o establecer el registro activo (EDP4_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// EDP4_EDP3_ID
        /// </summary>
        int IdEdp3 { get; set; }

        /// <summary>
        /// EDP4_PUE_ID
        /// </summary>
        int IdPuesto { get; set; }

        /// <summary>
        /// EDP4_DIA
        /// </summary>
        int Dia { get; set; }

        /// <summary>
        /// EDP4_MES
        /// </summary>
        int EDP4_MES { get; set; }

        /// <summary>
        /// EDP4_SEC
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// EDP4_USU_FM
        /// </summary>
        int Modifica { get; set; }

        /// <summary>
        /// EDP4_CAN
        /// </summary>
        decimal Cantidad { get; set; }

        /// <summary>
        /// EDP4_MUL
        /// </summary>
        decimal Repetir { get; set; }

        /// <summary>
        /// EDP4_TIE_CAN
        /// </summary>
        decimal TiempoEstimado { get; set; }

        /// <summary>
        /// EDP4_UNI
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// EDP4_TIE_EST
        /// </summary>
        decimal UnitarioTE { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion de la etapa del proceso (EDP4_NOM)
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// EDP4_FM
        /// </summary>
        DateTime FechaModifica { get; set; }
    }
}
