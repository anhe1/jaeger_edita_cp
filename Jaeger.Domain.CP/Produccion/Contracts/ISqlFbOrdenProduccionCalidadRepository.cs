﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// registros de calidad (tabla PEDCAL)
    /// </summary>
    public interface ISqlFbOrdenProduccionCalidadRepository : IGenericRepository<OrdenProduccionCalidadModel> {
        OrdenProduccionCalidadDetail Salveable(OrdenProduccionCalidadDetail model);
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
