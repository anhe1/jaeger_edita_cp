﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    ///<summary>
    /// repositorio de cambios de status de la orden de produccion (PEDFECR)
    ///</summary>
    public interface ISqlPedFecRRepository : IGenericRepository<PedFecRModel> {
        /// <summary>
        /// obtener listado de cambios de fechas de acuerdo de un grupo de ordenes
        /// </summary>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        bool Cancelar(PedFecRDetailModel item);

        bool Save(PedFecRDetailModel model);
    }
}
