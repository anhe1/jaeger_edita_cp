﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// posibles fallas (SCHKLST)
    /// </summary>
    public interface IChekListFallaModel {
        /// <summary>
        /// obtener o establecer el indce de la tabla (SCHKLST_ID)
        /// </summary>
        int IdFalla { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (SCHKLST_A)
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con la lista de verificacion (SCHKLST_CHKLST_ID)
        /// </summary>
        int IdCheckList { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con el proceso (SCHKLST_PROC_ID)
        /// </summary>
        int IdProceso { get; set; }
        
        /// <summary>
        /// obtener o establecer la secuencia a presentar (SCHKLST_SEC)
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion (SCHKLST_DESC)
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro (SCHKLST_USU_N)
        /// </summary>
        string Creo { get; set; }
        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro (SCHKLST_USU_M)
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer la fecha del nuevo registro (SCHKLST_FN)
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro (SCHKLST_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}