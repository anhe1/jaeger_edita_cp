﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    ///<summary>
    /// Cambio de fecha de acuerdo (PedFec)
    ///</summary>
    public interface IPedFechaAcuerdoModel {
        /// <summary>
        /// obtener o establecer PEDFEC_ID
        /// </summary>
        int IdPedFec { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_A
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_PEDPRD_ID
        /// </summary>
        int IdPedPrd { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_FECL_ID
        /// </summary>
        int PEDFEC_FECL_ID { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_CEN_ID
        /// </summary>
        int PEDFEC_CEN_ID { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_DIR_ID
        /// </summary>
        int PEDFEC_DIR_ID { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_USU_FM
        /// </summary>
        int? PEDFEC_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_FEC
        /// </summary>
        DateTime Fecha { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_OBS
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_USU_N
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_USU_F
        /// </summary>
        DateTime? PEDFEC_USU_F { get; set; }

        /// <summary>
        /// obtener o establecer PEDFEC_FM
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}