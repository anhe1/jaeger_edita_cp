﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// clase area y departamento (EDP1)
    /// </summary>
    public interface IDepartamentoModel {
        /// <summary>
        /// obtener o establecer el indice del departamento
        /// </summary>
        int IdDepto { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del area 
        /// </summary>
        string Area { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del departamento
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int Modifica { get; set; }

        /// <summary>
        /// obtener nombre o descripcion completa del Area + Departamento
        /// </summary>
        string AreaDepartamento { get; }
    }
}
