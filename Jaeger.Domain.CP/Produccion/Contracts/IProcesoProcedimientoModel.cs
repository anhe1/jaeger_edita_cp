﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// PRO3
    /// </summary>
    public interface IProcesoProcedimientoModel {
        /// <summary>
        /// obtener o establecer el indce de la tabla (PRO3_ID)
        /// </summary>
        int IdProcedimiento { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (PRO3_A9
        /// </summary>
        bool Activo { get; set; }
        /// <summary>
        /// PRO3_CAL
        /// </summary>
        string Calidad { get; set; }
        /// <summary>
        /// PRO3_NOM
        /// </summary>
        
        string Descripcion { get; set; }
        /// <summary>
        /// PRO3_EVAL
        /// </summary>
        int Evaluacion { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion el puesto (PRO3_PUE_ID)
        /// </summary>
        int IdPuesto { get; set; }
        
        /// <summary>
        /// PRO3_PRO2_ID
        /// </summary>
        int PRO3_PRO2_ID { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}