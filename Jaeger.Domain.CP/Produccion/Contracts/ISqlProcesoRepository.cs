﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// interface del repositorio de Equipos y Maquinaria (EDP2)
    /// </summary>
    public interface ISqlProcesoRepository : IGenericRepository<ProcesoModel> {
        /// <summary>
        /// obtener listado de procesos relacionados a un centro, incluye etapas, materia prima, mano de obra
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        /// <param name="idCentros">array de indices</param>
        IEnumerable<ProcesoDetailModel> GetList(bool onlyActive, int[] idCentros);

        /// <summary>
        /// obtener listado de procesos, incluye Area, Departamento, Seccion, Centro, Proceso
        /// </summary>
        IEnumerable<ProcesoModelView> GetViewList();
    }
}
