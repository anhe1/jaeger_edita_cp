﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// repositorio de cambios de fechas de acuerdo (PEDFEC)
    /// </summary>
    public interface ISqlPedFechaAcuerdoRepository : IGenericRepository<PedFechaAcuerdoModel> {
        /// <summary>
        /// fechas de acuerdo por condicionales
        /// </summary>
        /// <param name="conditionals">condicionales</param>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        PedFechaAcuerdoDetailModel Save(PedFechaAcuerdoDetailModel model);
    }
}
