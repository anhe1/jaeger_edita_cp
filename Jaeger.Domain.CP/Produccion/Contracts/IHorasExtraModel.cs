﻿using System;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    public interface IHorasExtraModel {
        /// <summary>
        /// obtener o establecer
        /// </summary>
        int IdControl { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        int IdDepartamento { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        DateTime FechaApliacion { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Supervisor { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        BindingList<IHoraExtraPersonalModel> Personal { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        BindingList<IHoraExtraOrdenModel> Ordenes { get; set; }
    }

    public interface IHoraExtraPersonalModel {
        /// <summary>
        /// obtener o establecer
        /// </summary>
        int IdControl { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        decimal Tiempo { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Nota { get; set; }
    }

    public interface IHoraExtraOrdenModel {
        /// <summary>
        /// obtener o establecer
        /// </summary>
        int IdControl { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        int IdOrden { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        int IdProceso { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        decimal Estimado { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        decimal Real { get; set; }
    }
}
