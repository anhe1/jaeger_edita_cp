﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Línea de Producción (PedPro)
    /// </summary>
    public interface IPedProModel {
        /// <summary>
        /// obtener o establecer (PEDPRO_ID)
        /// </summary>
        int IdPedPro { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (PEDPRO_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRO_SEC
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_CEN_ID)
        /// </summary>
        int PEDPRO_CEN_ID { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRO_CEN_ORD
        /// </summary>
        int PEDPRO_CEN_ORD { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_COTPRD_ID)
        /// </summary>
        int PEDPRO_COTPRD_ID { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_PRO_ID)
        /// </summary>
        int PEDPRO_COTPRO_ID { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_PEDCOM_ID)
        /// </summary>
        int IdComponente { get; set; }

        /// <summary>
        /// obtener o establecer PEDPRO_PRO_ID
        /// </summary>
        int PEDPRO_PRO_ID { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad estimada (PEDPRO_CAN_PROT)
        /// </summary>
        int CantidadEstimada { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad real producida (PEDPRO_CAN_PROR)
        /// </summary>
        int CantidadReal { get; set; }

        /// <summary>
        /// obtener o establecer numero de trabajadores estimados (PEDPRO_NT)
        /// </summary>
        int NumeroTrabajadoresEstimado { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_NTR)
        /// </summary>
        int NumeroTrabajdoresReal { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_T_MAX)
        /// </summary>
        int TiempoEstimado { get; set; }

        /// <summary>
        /// obtener o establecer el tiempo real ocupado, representado en minutos (PEDPRO_MINR)
        /// </summary>
        int TiempoReal { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_USU_FM)
        /// </summary>
        int PEDPRO_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_FEC_MIN)
        /// </summary>
        DateTime? FechaMinima { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_FEC_MAX)
        /// </summary>
        DateTime? FechaMaxima { get; set; }

        /// <summary>
        /// obtener o establecer fecha inicial de produccion (pedpro_fec_ini)
        /// </summary>
        DateTime? FechaInicio { get; set; }

        /// <summary>
        /// obtener o establecer fecha final de produccion (PEDPRO_FEC_FIN)
        /// </summary>
        DateTime? FechaFin { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_FPE)
        /// </summary>
        DateTime? PEDPRO_FPE { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_FPP)
        /// </summary>
        DateTime? PEDPRO_FPP { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_FPS)
        /// </summary>
        DateTime? PEDPRO_FPS { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_FRE)
        /// </summary>
        DateTime? PEDPRO_FRE { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_FRS)
        /// </summary>
        DateTime? PEDPRO_FRS { get; set; }

        /// <summary>
        /// obtener o establecer clave del operador u oficial (PEDPRO_OPE)
        /// </summary>
        string Oficial { get; set; }

        /// <summary>
        /// obtener o establecer clave deel ultimo usuario que modifica el registro (PEDPRO_USU_M)
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer observaciones (PEDPRO_OBS)
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRO_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
