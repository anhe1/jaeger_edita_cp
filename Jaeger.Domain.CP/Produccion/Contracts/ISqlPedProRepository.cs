﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Línea de Producción (PedPro)
    /// </summary>
    public interface ISqlPedProRepository : IGenericRepository<LineaProduccionModel> {
        //IEnumerable<ComponenteLineaProduccionView> GetList(List<Conditional> condiciones);

        /// <summary>
        /// obtener lista PedPro_Pro_Id
        /// </summary>
        IEnumerable<LineaProduccionModel> GetList(int[] ProdID);

        /// <summary>
        /// obtener lista PedPro_PedCom_ID (solo vista)
        /// </summary>
        IEnumerable<LineaProduccionDeptoCentroView> GetListView(int[] ProdID);

        /// <summary>
        /// obtener listado de componente, linea de produccion
        /// </summary>
        IEnumerable<ComponenteLineaProduccionView> GetList(DateTime startDateTime, DateTime? endDateTime, int? startStatus, int? endStatus, int idDepartamento, int fieldDate = 0, bool onlyYear = false, bool onlyActive = false);

        /// <summary>
        /// actualizar avance de produccion
        /// </summary>
        //bool UpdateAvance(PedProModel item);

        bool Update(IRegistroProduccion item);

        /// <summary>
        /// actualizar la fecha planeada del proceso
        /// </summary>
        /// <param name="idPedPro">PedPro_ID</param>
        /// <param name="startDate">fecha de inicio</param>
        /// <param name="endDate">fecha final</param>
        /// <param name="user">clave del usuario</param>
        /// <returns>verdadero si actualiza la fecha</returns>
        bool Update(int idPedPro, DateTime startDate, DateTime endDate, string user);

        //IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new();
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
