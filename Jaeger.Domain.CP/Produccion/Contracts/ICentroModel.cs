﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// interface Centro Modelo(EDP2)
    /// </summary>
    public interface ICentroModel {
        /// <sumary>
        /// obtener o establecer (EDP2_ID)
        /// </sumary>
        int IdCentro { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_A)
        /// </sumary>
        bool Activo { get; set; }

        /// <summary>
        /// EDP2_SEC
        /// </summary>
        string Secuencia { get; set; }

        /// <summary>
        /// EDP2_SEC
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer EDP2_EDP1_ID
        /// </summary>
        int IdDepartamento { get; set; }

        /// <summary>
        /// obtener o establecer EDP2_PRO
        /// </summary>
        string EDP2_PRO { get; set; }

        /// <sumary>
        /// obtener o establecer marca (EDP2_MAR)
        /// </sumary>
        string Marca { get; set; }

        /// <sumary>
        /// obtener o establecer modelo (EDP2_MOD)
        /// </sumary>
        string Modelo { get; set; }

        /// <sumary>
        /// obtener o establecer clave (EDP2_CLA)
        /// </sumary>
        string Clave { get; set; }

        /// <sumary>
        /// obtener o establecer numero de serie (EDP2_NUM_SER)
        /// </sumary>
        string NoSerie { get; set; }

        /// <summary>
        /// obtener o establecer (EDP2_CAL)
        /// </summary>
        string EDP2_CAL { get; set; }

        /// <sumary>
        /// obtener o establecer años de amortizacion (EDP2_ANO)
        /// </sumary>
        decimal AnioAmortizacion { get; set; }

        /// <sumary>
        /// obtener o establecer el valor de la adquisicion (EDP2_VAL)
        /// </sumary>
        decimal Valor { get; set; }

        /// <sumary>
        /// obtener o establecer dias por año (EDP2_DIA)
        /// </sumary>
        decimal DiasXAnio { get; set; }

        /// <sumary>
        /// obtener o establecer horas por dia (EDP2_HOR)
        /// </sumary>
        decimal HoraXDia { get; set; }

        /// <sumary>
        /// obtener o establecer % de uso (EDP2_TM)
        /// </sumary>
        decimal Uso { get; set; }

        /// <sumary>
        /// obtener o establecer valor por minuto (EDP2_VAL_MIN)
        /// </sumary>
        decimal ValorXMinuto { get; set; }

        /// <summary>
        /// obtener o establecer EDP2_USU_FM
        /// </summary>
        int? EDP2_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro EDP2_FM
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
