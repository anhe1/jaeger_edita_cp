﻿/// purpose:
/// develop: ANHE1 29092020 1713

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Seccion, Centro (EDP2)
    /// </summary>
    public interface ISeccionCentroModel {
        /// <sumary>
        /// obtener o establecer (EDP2_ID)
        /// </sumary>
        int IdSeccion { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_A)
        /// </sumary>
        bool Activo { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_EDP1_ID)
        /// </sumary>
        int IdDepto { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_SEC)
        /// </sumary>
        string Seccion { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_NOM)
        /// </sumary>
        string Centro { get; set; }
    }
}