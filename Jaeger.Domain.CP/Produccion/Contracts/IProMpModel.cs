﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    ///<summary>
    /// Proceso Materia Prima (PROMP)
    ///</summary>
    public interface IProMpModel {
        /// <summary>
        /// obtener o establecer indice de la tabla (PROMP_ID)
        /// </summary>
        int IdProMP { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (PROMP_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer secuencia (PROMP_SEC)
        /// </summary>
        int? Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_COM_ID)
        /// </summary>
        int? IdCom { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_FYV)
        /// </summary>
        int? PROMP_FYV { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_LIB)
        /// </summary>
        int? PROMP_LIB { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_MP_ID)
        /// </summary>
        int? PROMP_MP_ID { get; set; }

        /// <summary>
        /// obtener o establecer PROMP_MP4_ID
        /// </summary>
        int? PROMP_MP4_ID { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_MUL)
        /// </summary>
        int? Repetir { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_PRO_ID)
        /// </summary>
        int? PROMP_PRO_ID { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_MPMAR_ID)
        /// </summary>
        int? PROMP_MPMAR_ID { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_USU_FM)
        /// </summary>
        int? PROMP_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_$_TOT)
        /// </summary>
        decimal? PROMP__TOT { get; set; }

        /// <summary>
        /// obtener o establecer Unidad de Salida (PROMP_CAN)
        /// </summary>
        decimal CantidadSalida { get; set; }

        /// <summary>
        /// obtener o establecer cantidad entrante (PROMP_CAN_ENT)
        /// </summary>
        decimal? CantidadEntrada { get; set; }

        /// <summary>
        /// obtener o establecer cantidad produccion (PROMP_CAN_PRO)
        /// </summary>
        decimal? CantidadProduccion { get; set; }

        /// <summary>
        /// obtener o establecer PROMP_MPA
        /// </summary>
        decimal? PROMP_MPA { get; set; }

        /// <summary>
        /// obtener o establecer TRIAL PROMP_MPB
        /// </summary>
        decimal? PROMP_MPB { get; set; }

        /// <summary>
        /// obtener o establecer PROMP_MRA
        /// </summary>
        decimal? PROMP_MRA { get; set; }

        /// <summary>
        /// obtener o establecer PROMP_MRB
        /// </summary>
        decimal? PROMP_MRB { get; set; }

        /// <summary>
        /// obtener o establecer PROMP_TPA
        /// </summary>
        decimal? PROMP_TPA { get; set; }

        /// <summary>
        /// obtener o establecer PROMP_TPB
        /// </summary>
        decimal? PROMP_TPB { get; set; }

        /// <summary>
        /// obtener o establecer PROMP_TPC
        /// </summary>
        decimal? PROMP_TPC { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_TRA)
        /// </summary>
        decimal? PROMP_TRA { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_TRB)
        /// </summary>
        decimal? PROMP_TRB { get; set; }

        /// <summary>
        /// obtener o establecer (PROMP_TRC)
        /// </summary>
        decimal? PROMP_TRC { get; set; }

        /// <summary>
        /// obtener o establecer la utlima fecha de modificacion del registro (PROMP_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
