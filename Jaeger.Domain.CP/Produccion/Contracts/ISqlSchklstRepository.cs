﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// repositorio de control de fallas (schklst)
    /// </summary>
    public interface ISqlSchklstRepository : IGenericRepository<ChekListFallaModel> {

    }
}
