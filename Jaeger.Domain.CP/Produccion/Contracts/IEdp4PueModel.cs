﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    ///<summary>
    /// SubProcesos - Puestos (EDP4PUE)
    ///</summary>
    public interface IEdp4PueModel {
        /// <summary>
        /// obtener o establecer indice de la tabla (EDP4PUE_ID)
        /// </summary>
        int IdEp4Pue { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (EDP4PUE_A)
        /// </summary>
        int Activo { get; set; }

        /// <summary>
        /// obtener o establecer (EDP4PUE_ID3)
        /// </summary>
        int EDP4PUE_ID3 { get; set; }

        /// <summary>
        /// obtener o establecer (EDP4PUE_EDP4_ID)
        /// </summary>
        int EDP4PUE_EDP4_ID { get; set; }

        /// <summary>
        /// obtener o establecer (EDP4PUE_PUE_ID4)
        /// </summary>
        int EDP4PUE_PUE_ID4 { get; set; }

        /// <summary>
        /// obtener o establecer (EDP4PUE_PUE_ID)
        /// </summary>
        int EDP4PUE_PUE_ID { get; set; }

        /// <summary>
        /// obtener o establecer (EDP4PUE_USU_FM)
        /// </summary>
        int EDP4PUE_USU_FM { get; set; }

        /// <summary>
        /// obtener o establecer (EDP4PUE_FM)
        /// </summary>
        DateTime? EDP4PUE_FM { get; set; }
    }
}
