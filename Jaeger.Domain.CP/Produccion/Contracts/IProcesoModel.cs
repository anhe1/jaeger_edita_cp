﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Proceso (EDP3)
    /// </summary>
    public interface IProcesoModel {
        /// <sumary>
        /// obtener o establecer indice de la tabla(EDP3_ID)
        /// </sumary>
        int IdProceso { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_A)
        /// </sumary>
        int Activo { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_NT)
        /// </sumary>
        int Edp3Nt { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_EDP2_ID)
        /// </sumary>
        int IdCentro { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_SEC)
        /// </sumary>
        int Secuencia { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_USU_FM)
        /// </sumary>
        int Edp3UsuFm { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_CANINI)
        /// </sumary>
        int CantidadInicial { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_CEN)
        /// </sumary>
        decimal ValorHora { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_CANEXT)
        /// </sumary>
        decimal CantidadExt { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_MO)
        /// </sumary>
        decimal ManoObra { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_NOM)
        /// </sumary>
        string Nombre { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_FM)
        /// </sumary>
        DateTime FechaModifica { get; set; }
    }
}
