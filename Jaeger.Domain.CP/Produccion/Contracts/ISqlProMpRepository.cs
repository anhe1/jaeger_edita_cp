﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// repositorio de proceso materia prima (promp)
    /// </summary>
    public interface ISqlProMpRepository : IGenericRepository<ProMpModel> {
        /// <summary>
        /// consumibles (PROMP)
        /// </summary>
        IEnumerable<ProMpDetailModel> GetList(int[] index);

        /// <summary>
        /// obtener listado de objetos Detail relacionado al proceso
        /// </summary>
        /// <param name="index">indice de relacion con el proceso</param>
        IEnumerable<ProMpDetailModel> GetList(int index);

        /// <summary>
        /// materia prima o consumibles (PROMP)
        /// </summary>
        IEnumerable<ProMpDetailModelView> GetList(bool onlyActive, int[] index);
    }
}
