﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.CP.Produccion.Entities;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// Proceso Mano de Obra (PROMO)
    /// </summary>
    public interface ISqlProMoRepository : IGenericRepository<ProcesoManoObraModel> {
    }
}
