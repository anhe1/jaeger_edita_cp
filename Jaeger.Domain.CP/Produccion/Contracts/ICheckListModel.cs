﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;

namespace Jaeger.Domain.CP.Produccion.Contracts {
    /// <summary>
    /// lista de verificacion del proceso (CHKLST)
    /// </summary>
    public interface ICheckListModel {
        /// <summary>
        /// obtener o establecer el indce de la tabla (CHKLST_ID)
        /// </summary>
        int IdCheckList { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (CHKLST_A)
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con el proceso (CHKLST_PROC_ID)
        /// </summary>
        int IdProceso { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia a presentar (CHKLST_SEC)
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion (CHKLST_DESC)
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer la fecha del nuevo registro (CHKLST_FN)
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro (CHKLST_FM)
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro (CHKLST_USU_N)
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro (CHKLST_USU_M)
        /// </summary>
        string Modifica { get; set; }

    }
}