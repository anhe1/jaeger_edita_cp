﻿using System;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.CP.Produccion.ValueObjects;

namespace Jaeger.Domain.CP.Produccion.Builder {
    /// <summary>
    /// clase builder para consultas de ordenes de produccion
    /// </summary>
    public interface IOrdenProduccionQueryBuilder : IConditionalBuilder {
        IOrdenProduccionClienteQueryBuilder ByIdCliente(int idCliente);
        IOrdenProduccionVendedorQueryBuilder ByIdVendedor(int idVendedor);
        
        IConditionalBuilder WithIdOrden(int idOrden);
        IConditionalBuilder WithIdOrden(int[] idOrden);
        
        /// <summary>
        /// orden de produccion
        /// </summary>
        /// <param name="idOrden">indice de orden de produccion</param>
        IConditionalBuilder GetById(int idOrden);

        /// <summary>
        /// Por fechas
        /// </summary>
        
        IOrdenProduccionByDateQueryBuilder ByDate(PedPrdFechaEnum byDate);
    }

    public interface IOrdenProduccionByDateQueryBuilder : IConditionalBuilder {
        IOrdenProduccionYearQueryBuilder WithYear(int year);
        IOrdenProduccionFechaEndQueryBuilder StartDate(DateTime startDate);
    }

    public interface IOrdenProduccionClienteQueryBuilder : IConditionalBuilder {
        IOrdenProduccionYearQueryBuilder WithYear(int year);
    }

    public interface IOrdenProduccionYearQueryBuilder : IConditionalBuilder {
        IOrdenProduccionMonthQueryBuilder WithMonth(int month);
        IOrdenProduccionMonthQueryBuilder WithWeek(int month);
        IOrdenProduccionStatusQueryBuilder WithIdStatus(int idStatus);
        IOrdenProduccionStatusQueryBuilder WithIdStatus(int[] idStatus);
    }

    public interface IOrdenProduccionMonthQueryBuilder : IConditionalBuilder {
        IOrdenProduccionStatusQueryBuilder WithIdStatus(int idStatus);
        IOrdenProduccionStatusQueryBuilder WithIdStatus(int[] idStatus);
    }

    public interface IOrdenProduccionStatusQueryBuilder : IConditionalBuilder {
        IOrdenProduccionStatusQueryBuilder WithIdStatus(int idStatus);
        IOrdenProduccionStatusQueryBuilder WithIdStatus(int[] idStatus);
    }

    public interface IOrdenProduccionVendedorQueryBuilder : IConditionalBuilder {
        IOrdenProduccionYearQueryBuilder WithYear(int year);
    }

    public interface IOrdenProduccionFechaStartQueryBuilder {
        IOrdenProduccionFechaEndQueryBuilder StartDate(DateTime startDate);
    }

    public interface IOrdenProduccionFechaEndQueryBuilder {
        IOrdenProduccionStatusQueryBuilder EndDate(DateTime endDate);
    }
}
