﻿using System;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.CP.Produccion.ValueObjects;

namespace Jaeger.Domain.CP.Produccion.Builder {
    /// <summary>
    /// clase builder para consulta de componentes
    /// </summary>
    public class ComponenteQueryBuilder : ConditionalBuilder, IConditionalBuilder, IComponenteQueryBuilder, IComponenteByDateQueryBuilder, IComponenteDepartamentoQueryBuilder, IComprobanteCentroQueryBuilder,
        IComponenteByDepartamentoQueryBuilder, IOrdenProduccionByDateQueryBuilder, IOrdenProduccionYearQueryBuilder, IOrdenProduccionFechaEndQueryBuilder, IOrdenProduccionStatusQueryBuilder, IOrdenProduccionMonthQueryBuilder {

        private string _Fecha = "PEDPRD_FEC_PED";

        public ComponenteQueryBuilder() : base() { }

        public IOrdenProduccionByDateQueryBuilder ByDate(PedPrdFechaEnum byDate) {
            switch (byDate) {
                case PedPrdFechaEnum.Acuerdo:
                    this._Fecha = "PEDPRD_FEC_ACO";
                    break;
                case PedPrdFechaEnum.Orden:
                    this._Fecha = "PEDPRD_FEC_OP";
                    break;
                case PedPrdFechaEnum.Pedido:
                    this._Fecha = "PEDPRD_FEC_PED";
                    break;
                case PedPrdFechaEnum.VoBoC:
                    this._Fecha = "PEDPRD_FEC_VOBOC";
                    break;
                case PedPrdFechaEnum.VoBo:
                    this._Fecha = "PEDPRD_FEC_VOBO";
                    break;
            }
            return this;
        }

        public IComponenteByDepartamentoQueryBuilder ByDepartamento(int idDepto) {
            this._Conditionals.Add(new Conditional("EDP1_ID", idDepto.ToString()));
            return this;
        }

        public IOrdenProduccionStatusQueryBuilder EndDate(DateTime endDate) {
            this._Conditionals.Add(new Conditional(this._Fecha, endDate.ToString(), ConditionalTypeEnum.LessThanOrEqual));
            return this;
        }

        public IComponenteByDateQueryBuilder FechaProduccion() {
            this._Fecha = "PEDPRO_FEC_FIN";
            return this;
        }

        public IComprobanteCentroQueryBuilder IdCentro(int idCentro) {
            return this;
        }

        public IComponenteDepartamentoQueryBuilder IdDepartamento(int idDepartamento) {
            this._Conditionals.Add(new Conditional("EDP1_ID", idDepartamento.ToString()));
            return this;
        }

        public IComponenteByDateQueryBuilder Pendiente() {
            this._Conditionals.Add(new Conditional("PEDPRO_FEC_FIN", null, ConditionalTypeEnum.EqualNull));
            return this;
        }

        public IOrdenProduccionFechaEndQueryBuilder StartDate(DateTime startDate) {
            this._Conditionals.Add(new Conditional(this._Fecha, startDate.ToString(), ConditionalTypeEnum.GreaterThanOrEqual));
            return this;
        }

        public IOrdenProduccionStatusQueryBuilder WithIdStatus(int idStatus) {
            this._Conditionals.Add(new Conditional("PEDPRD_ST_ID", idStatus.ToString(), ConditionalTypeEnum.In));
            return this;
        }

        public IOrdenProduccionStatusQueryBuilder WithIdStatus(int[] idStatus) {
            var s0 = string.Join(",", idStatus);
            this._Conditionals.Add(new Conditional("PEDPRD_ST_ID", s0, ConditionalTypeEnum.In));
            return this;
        }

        public IOrdenProduccionMonthQueryBuilder WithMonth(int month) {
            if (month > 0) {
                this._Conditionals.Add(new Conditional(string.Format("EXTRACT(MONTH FROM {0})", this._Fecha), month.ToString()));
            }
            return this;
        }

        public IOrdenProduccionMonthQueryBuilder WithWeek(int week) {
            if (week > 0)this._Conditionals.Add(new Conditional(string.Format("EXTRACT(WEEK FROM {0})", this._Fecha), week.ToString()));
            return this;
        }

        public IOrdenProduccionYearQueryBuilder WithYear(int year) {
            this._Conditionals.Add(new Conditional(string.Format("EXTRACT(YEAR FROM {0})", this._Fecha), year.ToString()));
            return this;
        }
    }
}
