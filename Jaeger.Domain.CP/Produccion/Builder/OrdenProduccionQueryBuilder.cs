﻿using System;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.CP.Produccion.ValueObjects;

namespace Jaeger.Domain.CP.Produccion.Builder {
    public class OrdenProduccionQueryBuilder : ConditionalBuilder, IConditionalBuilder, IOrdenProduccionQueryBuilder, IOrdenProduccionYearQueryBuilder, IOrdenProduccionMonthQueryBuilder, IOrdenProduccionStatusQueryBuilder,
        IOrdenProduccionFechaStartQueryBuilder, IOrdenProduccionFechaEndQueryBuilder, IOrdenProduccionClienteQueryBuilder, IOrdenProduccionVendedorQueryBuilder, IOrdenProduccionByDateQueryBuilder {

        private string _Fecha = "PEDPRD_FEC_PED";

        public OrdenProduccionQueryBuilder() : base() { }

        /// <summary>
        /// por numero de orden
        /// </summary>
        /// <param name="idOrden">numero de orden de produccion</param>
        public IConditionalBuilder GetById(int idOrden) {
            this._Conditionals.Add(new Conditional("PEDPRD_ID", idOrden.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        public IOrdenProduccionByDateQueryBuilder ByDate(PedPrdFechaEnum byDate) {
            switch (byDate) {
                case PedPrdFechaEnum.Acuerdo:
                    this._Fecha = "PEDPRD_FEC_ACO";
                    break;
                case PedPrdFechaEnum.Orden:
                    this._Fecha = "PEDPRD_FEC_OP";
                    break;
                case PedPrdFechaEnum.Pedido:
                    this._Fecha = "PEDPRD_FEC_PED";
                    break;
                case PedPrdFechaEnum.VoBoC:
                    this._Fecha = "PEDPRD_FEC_VOBOC";
                    break;
                case PedPrdFechaEnum.VoBo:
                    this._Fecha = "PEDPRD_FEC_VOBO";
                    break;
            }
            return this;
        }

        public IOrdenProduccionByDateQueryBuilder ByDate() {
            return this;
        }

        public IOrdenProduccionFechaEndQueryBuilder StartDate(DateTime startDate) {
            this._Conditionals.Add(new Conditional(this._Fecha, startDate.ToString(), ConditionalTypeEnum.GreaterThanOrEqual));
            return this;
        }

        public IOrdenProduccionStatusQueryBuilder EndDate(DateTime endDate) {
            this._Conditionals.Add(new Conditional(this._Fecha, endDate.ToString(), ConditionalTypeEnum.LessThanOrEqual));
            return this;
        }

        public IOrdenProduccionYearQueryBuilder WithYear(int year) {
            this._Conditionals.Add(new Conditional(string.Format("EXTRACT(YEAR FROM {0})", this._Fecha), year.ToString()));
            return this;
        }

        public IOrdenProduccionMonthQueryBuilder WithMonth(int month) {
            if (month > 0) {
                this._Conditionals.Add(new Conditional(string.Format("EXTRACT(MONTH FROM {0})", this._Fecha), month.ToString()));
            }
            return this;
        }

        /// <summary>
        /// por nuemro de orden de produccion
        /// </summary>
        /// <param name="week">numero de semana</param>
        public IOrdenProduccionMonthQueryBuilder WithWeek(int week) {
            this._Conditionals.Add(new Conditional(string.Format("EXTRACT(WEEK FROM {0})", this._Fecha), week.ToString()));
            return this;
        }

        public IOrdenProduccionClienteQueryBuilder ByIdCliente(int idCliente) {
            this._Conditionals.Add(new Conditional("PEDPRD_DIR_ID", idCliente.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        public IOrdenProduccionVendedorQueryBuilder ByIdVendedor(int idVendedor) {
            this._Conditionals.Add(new Conditional("PEDPRD_VEN_ID", idVendedor.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// Orden de Produccion
        /// </summary>
        /// <param name="idOrden">indice de orden de produccion</param>
        public IConditionalBuilder WithIdOrden(int idOrden) {
            this._Conditionals.Add(new Conditional("PEDPRD_ID", idOrden.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// lista de ordenes de produccion
        /// </summary>
        /// <param name="idOrden">array o lista de ordenes de produccion</param>
        public IConditionalBuilder WithIdOrden(int[] idOrden) {
            this._Conditionals.Add(new Conditional("PEDPRD_ID", string.Join(",", idOrden), ConditionalTypeEnum.In));
            return this;
        }

        /// <summary>
        /// por indice de statis
        /// </summary>
        /// <param name="idStatus">indice de status</param>
        public IOrdenProduccionStatusQueryBuilder WithIdStatus(int idStatus) {
            this._Conditionals.Add(new Conditional("PEDPRD_ST_ID", idStatus.ToString(), ConditionalTypeEnum.In));
            return this;
        }

        public IOrdenProduccionStatusQueryBuilder WithIdStatus(int[] idStatus) {
            var s0 = string.Join(",", idStatus);
            this._Conditionals.Add(new Conditional("PEDPRD_ST_ID", s0, ConditionalTypeEnum.In));
            return this;
        }
    }
}
