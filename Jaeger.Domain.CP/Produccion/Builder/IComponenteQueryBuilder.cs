﻿using System;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.CP.Produccion.ValueObjects;

namespace Jaeger.Domain.CP.Produccion.Builder {
    /// <summary>
    /// clase builder para consulta de componentes
    /// </summary>
    public interface IComponenteQueryBuilder : IConditionalBuilder {
        IComponenteDepartamentoQueryBuilder IdDepartamento(int idDepartamento);
    }

    public interface IComponenteDepartamentoQueryBuilder {
        IComprobanteCentroQueryBuilder IdCentro(int idCentro);
        IOrdenProduccionYearQueryBuilder WithYear(int year);
        IComponenteByDateQueryBuilder Pendiente();
    }

    public interface IComprobanteCentroQueryBuilder {
        IComponenteByDateQueryBuilder FechaProduccion();
        IOrdenProduccionByDateQueryBuilder ByDate(PedPrdFechaEnum byDate);
    }

    public interface IComponenteByDateQueryBuilder {
        IOrdenProduccionYearQueryBuilder WithYear(int year);
        IComponenteByDepartamentoQueryBuilder ByDepartamento(int idDepto);
        IOrdenProduccionFechaEndQueryBuilder StartDate(DateTime startDate);
    }

    public interface IComponenteByDepartamentoQueryBuilder {
        IOrdenProduccionYearQueryBuilder WithYear(int year);
        IOrdenProduccionFechaEndQueryBuilder StartDate(DateTime startDate);
    }
}
