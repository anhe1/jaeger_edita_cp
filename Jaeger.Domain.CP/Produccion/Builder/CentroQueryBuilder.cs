﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.CP.Produccion.Builder {
    /// <summary>
    /// clase builder para consultas para centros productivos
    /// </summary>
    public class CentroQueryBuilder : ConditionalBuilder, IConditionalBuilder, ICentroQueryBuilder, ICentroDepartamentoQueryBuilder, ICentroOnlyActiveQueryBuilder {
        public CentroQueryBuilder() : base() { }

        /// <summary>
        /// por indice de departamentos
        /// </summary>
        public ICentroDepartamentoQueryBuilder IdDepartamento(int id) {
            this._Conditionals.Add(new Conditional("EDP2_EDP1_ID", id.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// por indices de departamentos
        /// </summary>
        public ICentroDepartamentoQueryBuilder IdDepartamento(int[] idDepartamento) {
            var d0 = string.Join(",", idDepartamento);
            this._Conditionals.Add(new Conditional("EDP2_EDP1_ID", d0, ConditionalTypeEnum.In));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public ICentroOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("EDP2_A", "1", ConditionalTypeEnum.In));
            return this;
        }
    }
}
