﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.CP.Produccion.Builder {
    public interface ICentroQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        ICentroOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true);
        /// <summary>
        /// por indice de departamentos
        /// </summary>
        ICentroDepartamentoQueryBuilder IdDepartamento(int idDepatamento);
        /// <summary>
        /// por indices de departamentos
        /// </summary>
        ICentroDepartamentoQueryBuilder IdDepartamento(int[] idDepartamento);
    }

    public interface ICentroDepartamentoQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        ICentroOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface ICentroOnlyActiveQueryBuilder : IConditionalBuilder {

    }
}
