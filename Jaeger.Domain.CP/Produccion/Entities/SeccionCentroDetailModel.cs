﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Seccion y Centros (EDP2)
    /// </summary>
    public class SeccionCentroDetailModel : SeccionCentroModel {
        private BindingList<ProcesoDetailModel> procesoDetailModels;
        private BindingList<CentroDetailModel> equipoMaquinarias;
        public SeccionCentroDetailModel() {
            procesoDetailModels = new BindingList<ProcesoDetailModel>();
            equipoMaquinarias = new BindingList<CentroDetailModel>();
        }

        public BindingList<ProcesoDetailModel> Proceso {
            get { return procesoDetailModels; }
            set {
                procesoDetailModels = value;
                OnPropertyChanged();
            }
        }

        public BindingList<CentroDetailModel> EquipoMaquinarias {
            get { return equipoMaquinarias; }
            set {
                equipoMaquinarias = value;
                OnPropertyChanged();
            }
        }
    }
}
