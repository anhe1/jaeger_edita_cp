﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Produccion.Entities {
    public class OrdenProduccionDiarioReporte {
        protected internal List<OrdenProduccionReporte> _OrdenProduccion;

        public OrdenProduccionDiarioReporte() {

        }

        public OrdenProduccionDiarioReporte(List<OrdenProduccionReporte> ordenProduccion) {
            this._OrdenProduccion = ordenProduccion;
        }

        public DateTime Fecha { get; set; }

        /// <summary>
        /// ordenes del periodo
        /// </summary>
        public List<OrdenProduccionReporte> DelDia {
            get { return this._OrdenProduccion.Where(it => it.FechaAcuerdo <= this.Fecha).ToList(); }
        }

        /// <summary>
        /// ordenes por vencer en el periodo
        /// </summary>
        public List<OrdenProduccionReporte> PorVencer {
            get { return this._OrdenProduccion.Where(it => it.FechaAcuerdo > this.Fecha).ToList(); }
        }

        public int TotalDiaAnterior {
            get { return this.DelDia.Where(it => it.FechaAcuerdo<this.Fecha).Count(); }
        }

        public int TotalDelDia {
            get { return this.DelDia.Where(it => it.FechaAcuerdo == this.Fecha).Count(); }
        }

        public int TotalEntregadas {
            get { return this.DelDia.Where(it => it.IdStatus == 9).Count(); }
        }

        public int TotalPorEntregar {
            get { return this.DelDia.Where(it => it.IdStatus != 9).Count(); }
        }

        /// <summary>
        /// ordenes que cumplen fechas de acuerdo
        /// </summary>
        public int TotalFechaAcuerdo {
            get { return this.DelDia.Where(it => it.CumpleFechaAcuerdo == true).Count(); }
        }

        public int TotalNoFechaAcuerdo {
            get { return this.DelDia.Where(it => it.CumpleFechaAcuerdo == false).Count(); }
        }

        public int TotalAEntregar {
            get { return this.TotalDelDia + this.TotalDiaAnterior; }
        }

        public int TotalSigPeriodo {
            get { return (this.TotalDelDia + this.TotalDiaAnterior) - this.TotalEntregadas; }
        }

        /// <summary>
        /// cumple con cantidad
        /// </summary>
        public int TotalCantidad {
            get { return this.DelDia.Where(it => it.CumpleCantidad == true).Count(); }
        }

        /// <summary>
        /// cumple con cantidad
        /// </summary>
        public int TotalNoCantidad {
            get { return this.DelDia.Where(it => it.CumpleCantidad == false).Count(); }
        }
    }
}
