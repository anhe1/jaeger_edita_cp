﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Línea de Producción (PEDPROE)
    /// </summary>
    [SugarTable("pedproe", "linea de produccion")]
    public class LineaProduccionModelE : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private int activo;
        private int _PEDPRO_PEDCOM_ID;
        private int _PEDPRO_PRO_ID;
        private int _PEDPRO_MINR;
        private DateTime? _PEDPRO_FEC_INI;
        private DateTime? _PEDPRO_FEC_FIN;
        private string _PEDPRO_USU_M;
        private int _PEDPRO_CAN_PROR;
        private string _PEDPRO_OPE;
        private string _PEDPRO_OBS;
        private int _PEDPRO_NTR;
        #endregion

        public LineaProduccionModelE() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_ID)
        /// </summary>
        [DataNames("PEDPRO_ID")]
        [SugarColumn(ColumnName = "pedpro_id", ColumnDescription = "PEDPRO_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPedPro {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (PEDPRO_A)
        /// </summary>
        [DataNames("PEDPRO_A")]
        [SugarColumn(ColumnName = "pedpro_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_PEDCOM_ID)
        /// </summary>
        [DataNames("PEDPRO_PEDCOM_ID")]
        [SugarColumn(ColumnName = "pedpro_pedcom_id", ColumnDescription = "PEDPRO_PEDCOM_ID")]
        public int IdComponente {
            get { return this._PEDPRO_PEDCOM_ID; }
            set {
                this._PEDPRO_PEDCOM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRO_PRO_ID
        /// </summary>
        [DataNames("PEDPRO_PRO_ID")]
        [SugarColumn(ColumnName = "pedpro_pro_id", ColumnDescription = "PEDPRO_PRO_ID")]
        public int PEDPRO_PRO_ID {
            get { return this._PEDPRO_PRO_ID; }
            set {
                this._PEDPRO_PRO_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad real producida (PEDPRO_CAN_PROR)
        /// </summary>
        [DataNames("PEDPRO_CAN_PROR")]
        [SugarColumn(ColumnName = "pedpro_can_pror", ColumnDescription = "PEDPRO_CAN_PROR")]
        public int CantidadReal {
            get { return this._PEDPRO_CAN_PROR; }
            set {
                this._PEDPRO_CAN_PROR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_NTR)
        /// </summary>
        [DataNames("PEDPRO_NTR")]
        [SugarColumn(ColumnName = "pedpro_ntr", ColumnDescription = "PEDPRO_NTR")]
        public int NumeroTrabajdoresReal {
            get { return this._PEDPRO_NTR; }
            set {
                this._PEDPRO_NTR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tiempo real ocupado, representado en minutos (PEDPRO_MINR)
        /// </summary>
        [DataNames("PEDPRO_MINR")]
        [SugarColumn(ColumnName = "pedpro_minr", ColumnDescription = "PEDPRO_MINR")]
        public int TiempoReal {
            get { return this._PEDPRO_MINR; }
            set {
                this._PEDPRO_MINR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del operador u oficial (PEDPRO_OPE)
        /// </summary>
        [DataNames("PEDPRO_OPE")]
        [SugarColumn(ColumnName = "pedpro_ope", ColumnDescription = "PEDPRO_OPE", Length = 7)]
        public string Oficial {
            get { return this._PEDPRO_OPE; }
            set {
                this._PEDPRO_OPE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha final de produccion (PEDPRO_FEC_FIN)
        /// </summary>
        [DataNames("PEDPRO_FEC_FIN")]
        [SugarColumn(ColumnName = "pedpro_fec_fin", ColumnDescription = "PEDPRO_FEC_FIN")]
        public DateTime? FechaFin {
            get {
                if (this._PEDPRO_FEC_FIN >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FEC_FIN;
                return null;
            }
            set {
                this._PEDPRO_FEC_FIN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones (PEDPRO_OBS)
        /// </summary>
        [DataNames("PEDPRO_OBS")]
        [SugarColumn(ColumnName = "pedpro_obs", ColumnDescription = "PEDPRO_OBS", Length = 100)]
        public string Nota {
            get { return this._PEDPRO_OBS; }
            set {
                this._PEDPRO_OBS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha inicial de produccion (pedpro_fec_ini)
        /// </summary>
        [DataNames("PEDPRO_FEC_INI")]
        [SugarColumn(ColumnName = "pedpro_fec_ini", ColumnDescription = "PEDPRO_FEC_INI")]
        public DateTime? FechaNuevo {
            get {
                if (this._PEDPRO_FEC_INI >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FEC_INI;
                return null;
            }
            set {
                this._PEDPRO_FEC_INI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave deel ultimo usuario que modifica el registro (PEDPRO_USU_M)
        /// </summary>
        [DataNames("PEDPRO_USU_M")]
        [SugarColumn(ColumnName = "pedpro_usu_m", ColumnDescription = "PEDPRO_USU_M", Length = 7)]
        public string Creo {
            get { return this._PEDPRO_USU_M; }
            set {
                this._PEDPRO_USU_M = value;
                this.OnPropertyChanged();
            }
        }
    }
}
