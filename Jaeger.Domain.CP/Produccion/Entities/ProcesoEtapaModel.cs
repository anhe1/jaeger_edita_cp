﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// etapas del proceso (EDP4)
    /// </summary>
    [SugarTable("EDP4")]
    public class ProcesoEtapaModel : BasePropertyChangeImplementation, IProcesoEtapaModel {
        private int indice;
        private int activo;
        private string etapa;
        private decimal unitarioTE;
        private int secuencia;
        private decimal unitario;

        public ProcesoEtapaModel() {
            activo = 1;
        }

        /// <summary>
        /// obtener o establecer el indice de la etapa de proceso (EDP4_ID)
        /// </summary>
        [DataNames("EDP4_ID")]
        [SugarColumn(ColumnName = "EDP4_ID", ColumnDescription = "Id", IsPrimaryKey = true, IsIdentity = true)]
        public int IdEdp4 {
            get { return indice; }
            set {
                indice = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo (EDP4_A)
        /// </summary>
        [DataNames("EDP4_A")]
        [SugarColumn(ColumnName = "EDP4_A", ColumnDescription = "Activo")]
        public int Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// EDP4_EDP3_ID
        /// </summary>
        [DataNames("EDP4_EDP3_ID")]
        [SugarColumn(ColumnName = "EDP4_EDP3_ID", ColumnDescription = "Relacion")]
        public int IdEdp3 { get; set; }

        /// <summary>
        /// EDP4_PUE_ID
        /// </summary>
        [DataNames("EDP4_PUE_ID")]
        [SugarColumn(ColumnName = "EDP4_PUE_ID", ColumnDescription = "IdPuesto")]
        public int IdPuesto { get; set; }

        /// <summary>
        /// EDP4_DIA
        /// </summary>
        [DataNames("EDP4_DIA")]
        [SugarColumn(ColumnName = "EDP4_DIA", ColumnDescription = "EDP4_DIA")]
        public int Dia { get; set; }

        /// <summary>
        /// EDP4_MES
        /// </summary>
        [DataNames("EDP4_MES")]
        [SugarColumn(ColumnName = "EDP4_MES", ColumnDescription = "EDP4_MES")]
        public int EDP4_MES { get; set; }

        /// <summary>
        /// EDP4_SEC
        /// </summary>
        [DataNames("EDP4_SEC")]
        [SugarColumn(ColumnName = "EDP4_SEC", ColumnDescription = "Secuencia")]
        public int Secuencia {
            get { return secuencia; }
            set {
                secuencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// EDP4_USU_FM
        /// </summary>
        [DataNames("EDP4_USU_FM")]
        [SugarColumn(ColumnName = "EDP4_USU_FM", ColumnDescription = "Modifica")]
        public int Modifica { get; set; }

        /// <summary>
        /// EDP4_CAN
        /// </summary>
        [DataNames("EDP4_CAN")]
        [SugarColumn(ColumnName = "EDP4_CAN", ColumnDescription = "Cantidad", Length = 18, DecimalDigits = 4)]
        public decimal Cantidad { get; set; }

        /// <summary>
        /// EDP4_MUL
        /// </summary>
        [DataNames("EDP4_MUL")]
        [SugarColumn(ColumnName = "EDP4_MUL", ColumnDescription = "Repetir")]
        public decimal Repetir { get; set; }

        /// <summary>
        /// EDP4_TIE_CAN
        /// </summary>
        [DataNames("EDP4_TIE_CAN")]
        [SugarColumn(ColumnName = "EDP4_TIE_CAN", ColumnDescription = "TiempoEstimado", Length = 18, DecimalDigits = 4)]
        public decimal TiempoEstimado { get; set; }

        /// <summary>
        /// EDP4_UNI
        /// </summary>
        [DataNames("EDP4_UNI")]
        [SugarColumn(ColumnName = "EDP4_UNI", ColumnDescription = "Unitario")]
        public decimal Unitario {
            get { return unitario; }
            set {
                unitario = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// EDP4_TIE_EST
        /// </summary>
        [DataNames("EDP4_TIE_EST")]
        [SugarColumn(ColumnName = "EDP4_TIE_EST", ColumnDescription = "UnitarioTE")]
        public decimal UnitarioTE {
            get { return unitarioTE; }
            set {
                unitarioTE = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la etapa del proceso (EDP4_NOM)
        /// </summary>
        [DataNames("EDP4_NOM")]
        [SugarColumn(ColumnName = "EDP4_NOM", ColumnDescription = "Nombre", Length = 255)]
        public string Nombre {
            get { return etapa; }
            set {
                etapa = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// EDP4_FM
        /// </summary>
        [DataNames("EDP4_FM")]
        [SugarColumn(ColumnName = "EDP4_FM", ColumnDescription = "FechaModifica")]
        public DateTime FechaModifica { get; set; }
    }
}
