﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Jaeger.Domain.CP.Produccion.Entities {
    public class FechasAcuerdoReportePrinter {
        protected internal List<OrdenProduccionReporte> _OrdenProduccion;

        public FechasAcuerdoReportePrinter(List<OrdenProduccionReporte> ordenProduccion) {
            this._OrdenProduccion= ordenProduccion;
        }

        public DateTime Start {
            get;set;
        }

        public DateTime End {
            get;set;
        }

        public int PeriodoAnterior {
            get {
                if (this._OrdenProduccion != null)
                    return this._OrdenProduccion.Where(it => it.IdStatus == 9).Where(it => it.FechaAcuerdo < this.Start).Count();
                return 0;
            }
        }

        public int PeriodoSiguiente {
            get {
                if (this._OrdenProduccion != null)
                    return this._OrdenProduccion.Where(it => it.IdStatus == 9).Where(it => this.Start > it.FechaAcuerdo).Count();
                return 0;
            }
        }

        public int Entregadas {
            get {
                if (this._OrdenProduccion != null)
                    return this._OrdenProduccion.Where(it => it.IdStatus == 9).Where(it => it.FechaOrden >= this.Start && it.FechaOrden <= this.End).Count();
                return 0;
            }
        }

        public int PorEntregar {
            get {
                if (this._OrdenProduccion != null) {
                    return this._OrdenProduccion.Where(it => it.IdStatus != 9).Where(it => it.FechaOrden >= this.Start && it.FechaOrden <= this.End).Count();
                }
                return 0;
            }
        }

        public int PorFechaAcuerdo {
            get {
                return this._OrdenProduccion.Where(it => it.IdStatus == 9).Where(it => it.FechaOrden >= this.Start && it.FechaOrden <= this.End)
                    .Where(it => it.FechaAcuerdo <= it.FechaRemision2)
                    .Count();
            }
        }

        public int Total {
            get { return this._OrdenProduccion.Count(); }
        }
    }
}
