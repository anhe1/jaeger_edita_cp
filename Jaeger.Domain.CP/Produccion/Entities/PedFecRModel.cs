﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    ///<summary>
    /// clase para el cambios de status de la orden de produccion (PEDFECR)
    ///</summary>
    [SugarTable("pedfecr", "OP: cambios de status")]
    public class PedFecRModel : BasePropertyChangeImplementation, IPedFecRModel {
        #region declaraciones
        private int _PEDFECR_ID;
        private int _PEDFECR_A;
        private int _PEDFECR_PEDPRD_ID;
        private int _PEDFECR_FECL_ID;
        private int _PEDFECR_ST_ID;
        private DateTime _PEDFECR_FEC;
        private string _PEDFECR_OBS;
        private string _PEDFECR_USU_N;
        private DateTime? _PEDFECR_USU_F;
        private int? _PEDFECR_USU_FM;
        private DateTime? _PEDFECR_FM;
        #endregion

        public PedFecRModel() {
            this.Activo = 1;
        }

        /// <summary>
        /// obtener o establecer PEDFECR_ID
        /// </summary>
        [DataNames("PedFecR_ID")]
        [SugarColumn(ColumnName = "PedFecR_ID", ColumnDescription = "PEDFECR_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPedFecR {
            get { return this._PEDFECR_ID; }
            set {
                this._PEDFECR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFECR_A
        /// </summary>
        [DataNames("PEDFECR_A")]
        [SugarColumn(ColumnName = "pedfecr_a", ColumnDescription = "PEDFECR_A")]
        public int Activo {
            get { return this._PEDFECR_A; }
            set {
                this._PEDFECR_A = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFECR_PEDPRD_ID
        /// </summary>
        [DataNames("PEDFECR_PEDPRD_ID")]
        [SugarColumn(ColumnName = "pedfecr_pedprd_id", ColumnDescription = "PEDFECR_PEDPRD_ID")]
        public int IdPedPrd {
            get { return this._PEDFECR_PEDPRD_ID; }
            set {
                this._PEDFECR_PEDPRD_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFECR_ST_ID (relacion con la tabla PEDST)
        /// </summary>
        [DataNames("PEDFECR_ST_ID")]
        [SugarColumn(ColumnName = "pedfecr_st_id", ColumnDescription = "PEDFECR_ST_ID")]
        public int IdStatus {
            get { return this._PEDFECR_ST_ID; }
            set {
                this._PEDFECR_ST_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFECR_FECL_ID
        /// </summary>
        [DataNames("PEDFECR_FECL_ID")]
        [SugarColumn(ColumnName = "pedfecr_fecl_id", ColumnDescription = "PEDFECR_FECL_ID")]
        public int PEDFECR_FECL_ID {
            get { return this._PEDFECR_FECL_ID; }
            set {
                this._PEDFECR_FECL_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFECR_USU_FM
        /// </summary>
        [DataNames("PEDFECR_USU_FM")]
        [SugarColumn(ColumnName = "pedfecr_usu_fm", ColumnDescription = "PEDFECR_USU_FM")]
        public int? PEDFECR_USU_FM {
            get { return this._PEDFECR_USU_FM; }
            set {
                this._PEDFECR_USU_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFECR_OBS
        /// </summary>
        [DataNames("PEDFECR_OBS")]
        [SugarColumn(ColumnName = "pedfecr_obs", ColumnDescription = "PEDFECR_OBS", Length = 50)]
        public string Nota {
            get { return this._PEDFECR_OBS; }
            set {
                this._PEDFECR_OBS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFECR_USU_N
        /// </summary>
        [DataNames("PEDFECR_USU_N")]
        [SugarColumn(ColumnName = "pedfecr_usu_n", ColumnDescription = "PEDFECR_USU_N", Length = 7)]
        public string Creo {
            get { return this._PEDFECR_USU_N; }
            set {
                this._PEDFECR_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDFECR_FEC)
        /// </summary>
        [DataNames("PEDFECR_FEC")]
        [SugarColumn(ColumnName = "pedfecr_fec", ColumnDescription = "PEDFECR_FEC")]
        public DateTime Fecha {
            get { return this._PEDFECR_FEC; }
            set {
                this._PEDFECR_FEC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFECR_USU_F
        /// </summary>
        [DataNames("PEDFECR_USU_F")]
        [SugarColumn(ColumnName = "pedfecr_usu_f", ColumnDescription = "PEDFECR_USU_F")]
        public DateTime? PEDFECR_USU_F {
            get { return this._PEDFECR_USU_F; }
            set {
                this._PEDFECR_USU_F = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL Default_New:NULL Nullable_New:True
        /// </summary>
        [DataNames("PEDFECR_FM")]
        [SugarColumn(ColumnName = "pedfecr_fm", ColumnDescription = "PEDFECR_FM")]
        public DateTime? FechaModifica {
            get { return this._PEDFECR_FM; }
            set {
                this._PEDFECR_FM = value;
                this.OnPropertyChanged();
            }
        }
    }
}
