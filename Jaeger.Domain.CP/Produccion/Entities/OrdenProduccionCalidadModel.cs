﻿using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// clase para informacion de calidad
    /// </summary>
    public class OrdenProduccionCalidadModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int _IdCalidad;
        private bool _IsActive;
        private int _IdOrden;
        private int _IdEfecto;
        private int _IdCentro;
        private int _IdDirectorio;
        private DateTime _Fecha;
        private string _Observaciones;
        private string _Creo;
        private string _PEDPRD_USU_M;
        private DateTime? _PEDPRD_FM;
        #endregion

        public OrdenProduccionCalidadModel() {
            this.IdMotivo = 0;
            this.Fecha = DateTime.Now;
            this.FechaModifica = DateTime.Now;
        }

        [DataNames("PEDCAL_ID")]
        public int Id {
            get { return _IdCalidad; }
            set { _IdCalidad = value; }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("PEDCAL_A")]
        public bool Activo {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        /// <summary>
        /// obtener o establecer numero de orden de produccion
        /// </summary>
        [DataNames("PEDCAL_PEDPRD_ID")]
        public int IdOrden {
            get { return _IdOrden; }
            set { _IdOrden = value; }
        }

        [DataNames("PEDCAL_FECL_ID")]
        public int IdMotivo {
            get { return _IdEfecto; }
            set { _IdEfecto = value; }
        }

        /// <summary>
        /// obtener o establecer indice del centro productivo
        /// </summary>
        [DataNames("PEDCAL_CEN_ID")]
        public int IdCentro {
            get { return this._IdCentro; }
            set {  this._IdCentro = value; }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("PEDCAL_DIR_ID")]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set { this._IdDirectorio = value; }
        }

        [DataNames("PEDCAL_FEC")]
        public DateTime Fecha {
            get { return this._Fecha; }
            set { this._Fecha = value; }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("PEDCAL_OBS")]
        public string Nota {
            get { return this._Observaciones; }
            set { this._Observaciones = value; }
        }

        /// <summary>
        /// obtener o establecer clave del creador del registro
        /// </summary>
        [DataNames("PEDCAL_USU_N")]
        public string Creo {
            get { return this._Creo; }
            set { this._Creo = value; }
        }

        [DataNames("PEDCAL_USU_F1")]
        public int algo { get; set; }

        [DataNames("PEDCAL_USU_FM")]
        public string Modifica {
            get { return this._PEDPRD_USU_M; }
            set {
                this._PEDPRD_USU_M = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PEDCAL_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._PEDPRD_FM >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FM;
                return null;
            }
            set {
                this._PEDPRD_FM = value;
                this.OnPropertyChanged();
            }
        }
    }
}
