﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Línea de Producción (PedPro)
    /// </summary>
    [SugarTable("pedpro", "linea de produccion")]
    public class LineaProduccionModel : BasePropertyChangeImplementation, IPedProModel, IRegistroProduccion {
        #region declaraciones
        private int index;
        private int activo;
        private int _PEDPRO_PEDCOM_ID;
        private int _PEDPRO_COTPRD_ID;
        private int _PEDPRO_COTPRO_ID;
        private int _PEDPRO_CEN_ID;
        private int _PEDPRO_PRO_ID;
        private int _PEDPRO_SEC;
        private int _PEDPRO_CAN_PROT;
        private int _PEDPRO_T_MAX;
        private int _PEDPRO_MINR;
        private int _PEDPRO_NT;
        private DateTime? _PEDPRO_FEC_MIN;
        private DateTime? _PEDPRO_FEC_MAX;
        private DateTime? _PEDPRO_FEC_INI;
        private DateTime? _PEDPRO_FEC_FIN;
        private DateTime? _PEDPRO_FPE;
        private DateTime? _PEDPRO_FPP;
        private DateTime? _PEDPRO_FPS;
        private DateTime? _PEDPRO_FRE;
        private DateTime? _PEDPRO_FRS;
        private int _PEDPRO_USU_FM;
        private string _PEDPRO_USU_M;
        private DateTime? _PEDPRO_FM;
        private int _PEDPRO_CEN_ORD;
        private int _PEDPRO_CAN_PROR;
        private string _PEDPRO_OPE;
        private string _PEDPRO_OBS;
        private int _PEDPRO_NTR;
        private int _Id5Clasificacion;
        #endregion

        public LineaProduccionModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_ID)
        /// </summary>
        [DataNames("PEDPRO_ID")]
        [SugarColumn(ColumnName = "pedpro_id", ColumnDescription = "PEDPRO_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPedPro {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (PEDPRO_A)
        /// </summary>
        [DataNames("PEDPRO_A")]
        [SugarColumn(ColumnName = "pedpro_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRO_SEC
        /// </summary>
        [DataNames("PEDPRO_SEC")]
        [SugarColumn(ColumnName = "pedpro_sec", ColumnDescription = "PEDPRO_SEC")]
        public int Secuencia {
            get { return this._PEDPRO_SEC; }
            set {
                this._PEDPRO_SEC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_CEN_ID)
        /// </summary>
        [DataNames("PEDPRO_CEN_ID")]
        [SugarColumn(ColumnName = "pedpro_cen_id", ColumnDescription = "PEDPRO_CEN_ID")]
        public int PEDPRO_CEN_ID {
            get { return this._PEDPRO_CEN_ID; }
            set {
                this._PEDPRO_CEN_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRO_CEN_ORD
        /// </summary>
        [DataNames("PEDPRO_CEN_ORD")]
        [SugarColumn(ColumnName = "pedpro_cen_ord", ColumnDescription = "PEDPRO_CEN_ORD")]
        public int PEDPRO_CEN_ORD {
            get { return this._PEDPRO_CEN_ORD; }
            set {
                this._PEDPRO_CEN_ORD = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_COTPRD_ID)
        /// </summary>
        [DataNames("PEDPRO_COTPRD_ID")]
        [SugarColumn(ColumnName = "pedpro_cotprd_id", ColumnDescription = "PEDPRO_COTPRD_ID")]
        public int PEDPRO_COTPRD_ID {
            get { return this._PEDPRO_COTPRD_ID; }
            set {
                this._PEDPRO_COTPRD_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_COTPRO_ID)
        /// </summary>
        [DataNames("PEDPRO_COTPRO_ID")]
        [SugarColumn(ColumnName = "pedpro_cotpro_id", ColumnDescription = "PEDPRO_COTPRO_ID")]
        public int PEDPRO_COTPRO_ID {
            get { return this._PEDPRO_COTPRO_ID; }
            set {
                this._PEDPRO_COTPRO_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_PEDCOM_ID)
        /// </summary>
        [DataNames("PEDPRO_PEDCOM_ID")]
        [SugarColumn(ColumnName = "pedpro_pedcom_id", ColumnDescription = "PEDPRO_PEDCOM_ID")]
        public int IdComponente {
            get { return this._PEDPRO_PEDCOM_ID; }
            set {
                this._PEDPRO_PEDCOM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRO_PRO_ID
        /// </summary>
        [DataNames("PEDPRO_PRO_ID")]
        [SugarColumn(ColumnName = "pedpro_pro_id", ColumnDescription = "PEDPRO_PRO_ID")]
        public int PEDPRO_PRO_ID {
            get { return this._PEDPRO_PRO_ID; }
            set {
                this._PEDPRO_PRO_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad estimada (PEDPRO_CAN_PROT)
        /// </summary>
        [DataNames("PEDPRO_CAN_PROT")]
        [SugarColumn(ColumnName = "pedpro_can_prot", ColumnDescription = "PEDPRO_CAN_PROT")]
        public int CantidadEstimada {
            get { return this._PEDPRO_CAN_PROT; }
            set {
                this._PEDPRO_CAN_PROT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad real producida (PEDPRO_CAN_PROR)
        /// </summary>
        [DataNames("PEDPRO_CAN_PROR")]
        [SugarColumn(ColumnName = "pedpro_can_pror", ColumnDescription = "PEDPRO_CAN_PROR")]
        public int CantidadReal {
            get { return this._PEDPRO_CAN_PROR; }
            set {
                this._PEDPRO_CAN_PROR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de trabajadores estimados (PEDPRO_NT)
        /// </summary>
        [DataNames("PEDPRO_NT")]
        [SugarColumn(ColumnName = "pedpro_nt", ColumnDescription = "PEDPRO_NT")]
        public int NumeroTrabajadoresEstimado {
            get { return this._PEDPRO_NT; }
            set {
                this._PEDPRO_NT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_NTR)
        /// </summary>
        [DataNames("PEDPRO_NTR")]
        [SugarColumn(ColumnName = "pedpro_ntr", ColumnDescription = "PEDPRO_NTR")]
        public int NumeroTrabajdoresReal {
            get { return this._PEDPRO_NTR; }
            set {
                this._PEDPRO_NTR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_T_MAX)
        /// </summary>
        [DataNames("PEDPRO_T_MAX")]
        [SugarColumn(ColumnName = "pedpro_t_max", ColumnDescription = "PEDPRO_T_MAX")]
        public int TiempoEstimado {
            get { return this._PEDPRO_T_MAX; }
            set {
                this._PEDPRO_T_MAX = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tiempo real ocupado, representado en minutos (PEDPRO_MINR)
        /// </summary>
        [DataNames("PEDPRO_MINR")]
        [SugarColumn(ColumnName = "pedpro_minr", ColumnDescription = "PEDPRO_MINR")]
        public int TiempoReal {
            get { return this._PEDPRO_MINR; }
            set {
                this._PEDPRO_MINR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_USU_FM)
        /// </summary>
        [DataNames("PEDPRO_USU_FM")]
        [SugarColumn(ColumnName = "pedpro_usu_fm", ColumnDescription = "PEDPRO_USU_FM")]
        public int PEDPRO_USU_FM {
            get { return this._PEDPRO_USU_FM; }
            set {
                this._PEDPRO_USU_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_FEC_MIN)
        /// </summary>
        [DataNames("PEDPRO_FEC_MIN")]
        [SugarColumn(ColumnName = "pedpro_fec_min", ColumnDescription = "PEDPRO_FEC_MIN")]
        public DateTime? FechaMinima {
            get {
                if (this._PEDPRO_FEC_MIN >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FEC_MIN;
                return null;
            }
            set {
                this._PEDPRO_FEC_MIN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_FEC_MAX)
        /// </summary>
        [DataNames("PEDPRO_FEC_MAX")]
        [SugarColumn(ColumnName = "pedpro_fec_max", ColumnDescription = "PEDPRO_FEC_MAX")]
        public DateTime? FechaMaxima {
            get {
                if (this._PEDPRO_FEC_MAX >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FEC_MAX;
                return null;
            }
            set {
                this._PEDPRO_FEC_MAX = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha inicial de produccion (pedpro_fec_ini)
        /// </summary>
        [DataNames("PEDPRO_FEC_INI")]
        [SugarColumn(ColumnName = "pedpro_fec_ini", ColumnDescription = "PEDPRO_FEC_INI")]
        public DateTime? FechaInicio {
            get {
                if (this._PEDPRO_FEC_INI >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FEC_INI;
                return null;
            }
            set {
                this._PEDPRO_FEC_INI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha final de produccion (PEDPRO_FEC_FIN)
        /// </summary>
        [DataNames("PEDPRO_FEC_FIN")]
        [SugarColumn(ColumnName = "pedpro_fec_fin", ColumnDescription = "PEDPRO_FEC_FIN")]
        public DateTime? FechaFin {
            get {
                if (this._PEDPRO_FEC_FIN >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FEC_FIN;
                return null;
            }
            set {
                this._PEDPRO_FEC_FIN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_FPE)
        /// </summary>
        [DataNames("PEDPRO_FPE")]
        [SugarColumn(ColumnName = "pedpro_fpe", ColumnDescription = "PEDPRO_FPE")]
        public DateTime? PEDPRO_FPE {
            get {
                if (this._PEDPRO_FPE >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FPE;
                return null;
            }
            set {
                this._PEDPRO_FPE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_FPP)
        /// </summary>
        [DataNames("PEDPRO_FPP")]
        [SugarColumn(ColumnName = "pedpro_fpp", ColumnDescription = "PEDPRO_FPP")]
        public DateTime? PEDPRO_FPP {
            get {
                if (this._PEDPRO_FPP >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FPP;
                return null;
            }
            set {
                this._PEDPRO_FPP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_FPS)
        /// </summary>
        [DataNames("PEDPRO_FPS")]
        [SugarColumn(ColumnName = "pedpro_fps", ColumnDescription = "PEDPRO_FPS")]
        public DateTime? PEDPRO_FPS {
            get {
                if (this._PEDPRO_FPS >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FPS;
                return null;
            }
            set {
                this._PEDPRO_FPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_FRE)
        /// </summary>
        [DataNames("PEDPRO_FRE")]
        [SugarColumn(ColumnName = "pedpro_fre", ColumnDescription = "PEDPRO_FRE")]
        public DateTime? PEDPRO_FRE {
            get {
                if (this._PEDPRO_FRE >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FRE;
                return null;
            }
            set {
                this._PEDPRO_FRE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_FRS)
        /// </summary>
        [DataNames("PEDPRO_FRS")]
        [SugarColumn(ColumnName = "pedpro_frs", ColumnDescription = "PEDPRO_FRS")]
        public DateTime? PEDPRO_FRS {
            get {
                if (this._PEDPRO_FRS >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FRS;
                return null;
            }
            set {
                this._PEDPRO_FRS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del operador u oficial (PEDPRO_OPE)
        /// </summary>
        [DataNames("PEDPRO_OPE")]
        [SugarColumn(ColumnName = "pedpro_ope", ColumnDescription = "PEDPRO_OPE", Length = 7)]
        public string Oficial {
            get { return this._PEDPRO_OPE; }
            set {
                this._PEDPRO_OPE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave deel ultimo usuario que modifica el registro (PEDPRO_USU_M)
        /// </summary>
        [DataNames("PEDPRO_USU_M")]
        [SugarColumn(ColumnName = "pedpro_usu_m", ColumnDescription = "PEDPRO_USU_M", Length = 7)]
        public string Modifica {
            get { return this._PEDPRO_USU_M; }
            set {
                this._PEDPRO_USU_M = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones (PEDPRO_OBS)
        /// </summary>
        [DataNames("PEDPRO_OBS")]
        [SugarColumn(ColumnName = "pedpro_obs", ColumnDescription = "PEDPRO_OBS", Length = 100)]
        public string Nota {
            get { return this._PEDPRO_OBS; }
            set {
                this._PEDPRO_OBS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRO_FM)
        /// </summary>
        [DataNames("PEDPRO_FM")]
        [SugarColumn(ColumnName = "pedpro_fm", ColumnDescription = "PEDPRO_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._PEDPRO_FM >= new DateTime(1900, 1, 1))
                    return this._PEDPRO_FM;
                return null;
            }
            set {
                this._PEDPRO_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        [DataNames("PEDPRO_CCAL_ID")]
        [SugarColumn(ColumnName = "PEDPRO_CCAL_ID", ColumnDescription = "PEDPRO_CCAL_ID")]
        public int Id5Clasificacion {
            get { return this._Id5Clasificacion; }
            set {
                this._Id5Clasificacion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
