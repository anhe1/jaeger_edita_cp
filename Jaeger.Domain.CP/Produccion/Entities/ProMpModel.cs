﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    ///<summary>
    /// Proceso Materia Prima (PROMP)
    ///</summary>
    [SugarTable("promp")]
    public class ProMpModel : BasePropertyChangeImplementation, IProMpModel {
        private int index;
        private int activo;
        private int? _PROMP_PRO_ID;
        private int? _PROMP_MP_ID;
        private int? _PROMP_MP4_ID;
        private decimal _PROMP_CAN;
        private int? _PROMP_MPMAR_ID;
        private decimal? _PROMP_CAN_ENT;
        private decimal? _PROMP__TOT;
        private int? secuencia;
        private int? _PROMP_LIB;
        private int? _PROMP_FYV;
        private decimal? _PROMP_TRA;
        private decimal? _PROMP_TRB;
        private decimal? _PROMP_TRC;
        private decimal? _PROMP_TPA;
        private decimal? _PROMP_TPB;
        private decimal? _PROMP_TPC;
        private decimal? _PROMP_MRA;
        private decimal? _PROMP_MRB;
        private decimal? _PROMP_MPA;
        private decimal? _PROMP_MPB;
        private decimal? _PROMP_CAN_PRO;
        private int? _PROMP_MUL;
        private int? _PROMP_USU_FM;
        private DateTime? fechaModifica;
        private int? _PROMP_COM_ID;

        public ProMpModel() {
            activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (PROMP_ID)
        /// </summary>
        [DataNames("PROMP_ID")]
        [SugarColumn(ColumnName = "promp_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdProMP {
            get { return index; }
            set {
                index = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (PROMP_A)
        /// </summary>
        [DataNames("PROMP_A")]
        [SugarColumn(ColumnName = "promp_a", ColumnDescription = "registro activo")]
        public int Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer secuencia (PROMP_SEC)
        /// </summary>
        [DataNames("PROMP_SEC")]
        [SugarColumn(ColumnName = "promp_sec", ColumnDescription = "")]
        public int? Secuencia {
            get { return secuencia; }
            set {
                secuencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_COM_ID)
        /// </summary>
        [DataNames("PROMP_COM_ID")]
        [SugarColumn(ColumnName = "promp_com_id", ColumnDescription = "")]
        public int? IdCom {
            get { return _PROMP_COM_ID; }
            set {
                _PROMP_COM_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_FYV)
        /// </summary>
        [DataNames("PROMP_FYV")]
        [SugarColumn(ColumnName = "promp_fyv", ColumnDescription = "")]
        public int? PROMP_FYV {
            get { return _PROMP_FYV; }
            set {
                _PROMP_FYV = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_LIB)
        /// </summary>
        [DataNames("PROMP_LIB")]
        [SugarColumn(ColumnName = "promp_lib", ColumnDescription = "")]
        public int? PROMP_LIB {
            get { return _PROMP_LIB; }
            set {
                _PROMP_LIB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_MP_ID)
        /// </summary>
        [DataNames("PROMP_MP_ID")]
        [SugarColumn(ColumnName = "promp_mp_id", ColumnDescription = "")]
        public int? PROMP_MP_ID {
            get { return _PROMP_MP_ID; }
            set {
                _PROMP_MP_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PROMP_MP4_ID
        /// </summary>
        [DataNames("PROMP_MP4_ID")]
        [SugarColumn(ColumnName = "promp_mp4_id", ColumnDescription = "")]
        public int? PROMP_MP4_ID {
            get { return _PROMP_MP4_ID; }
            set {
                _PROMP_MP4_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_MUL)
        /// </summary>
        [DataNames("PROMP_MUL")]
        [SugarColumn(ColumnName = "promp_mul", ColumnDescription = "")]
        public int? Repetir {
            get { return _PROMP_MUL; }
            set {
                _PROMP_MUL = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_PRO_ID)
        /// </summary>
        [DataNames("PROMP_PRO_ID")]
        [SugarColumn(ColumnName = "promp_pro_id", ColumnDescription = "")]
        public int? PROMP_PRO_ID {
            get { return _PROMP_PRO_ID; }
            set {
                _PROMP_PRO_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_MPMAR_ID)
        /// </summary>
        [DataNames("PROMP_MPMAR_ID")]
        [SugarColumn(ColumnName = "promp_mpmar_id", ColumnDescription = "")]
        public int? PROMP_MPMAR_ID {
            get { return _PROMP_MPMAR_ID; }
            set {
                _PROMP_MPMAR_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_USU_FM)
        /// </summary>
        [DataNames("PROMP_USU_FM")]
        [SugarColumn(ColumnName = "promp_usu_fm", ColumnDescription = "")]
        public int? PROMP_USU_FM {
            get { return _PROMP_USU_FM; }
            set {
                _PROMP_USU_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_$_TOT)
        /// </summary>
        [DataNames("PROMP_$_TOT")]
        [SugarColumn(ColumnName = "promp_tot", ColumnDescription = "PROMP_$_TOT", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP__TOT {
            get { return _PROMP__TOT; }
            set {
                _PROMP__TOT = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Unidad de Salida (PROMP_CAN)
        /// </summary>
        [DataNames("PROMP_CAN")]
        [SugarColumn(ColumnName = "promp_can", ColumnDescription = "")]
        public decimal CantidadSalida {
            get { return _PROMP_CAN; }
            set {
                _PROMP_CAN = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cantidad entrante (PROMP_CAN_ENT)
        /// </summary>
        [DataNames("PROMP_CAN_ENT")]
        [SugarColumn(ColumnName = "promp_can_ent", ColumnDescription = "")]
        public decimal? CantidadEntrada {
            get { return _PROMP_CAN_ENT; }
            set {
                _PROMP_CAN_ENT = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cantidad produccion (PROMP_CAN_PRO)
        /// </summary>
        [DataNames("PROMP_CAN_PRO")]
        [SugarColumn(ColumnName = "promp_can_pro", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? CantidadProduccion {
            get { return _PROMP_CAN_PRO; }
            set {
                _PROMP_CAN_PRO = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PROMP_MPA
        /// </summary>
        [DataNames("PROMP_MPA")]
        [SugarColumn(ColumnName = "promp_mpa", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_MPA {
            get { return _PROMP_MPA; }
            set {
                _PROMP_MPA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer TRIAL PROMP_MPB
        /// </summary>
        [DataNames("PROMP_MPB")]
        [SugarColumn(ColumnName = "promp_mpb", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_MPB {
            get { return _PROMP_MPB; }
            set {
                _PROMP_MPB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PROMP_MRA
        /// </summary>
        [DataNames("PROMP_MRA")]
        [SugarColumn(ColumnName = "promp_mra", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_MRA {
            get { return _PROMP_MRA; }
            set {
                _PROMP_MRA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PROMP_MRB
        /// </summary>
        [DataNames("PROMP_MRB")]
        [SugarColumn(ColumnName = "promp_mrb", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_MRB {
            get { return _PROMP_MRB; }
            set {
                _PROMP_MRB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PROMP_TPA
        /// </summary>
        [DataNames("PROMP_TPA")]
        [SugarColumn(ColumnName = "promp_tpa", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_TPA {
            get { return _PROMP_TPA; }
            set {
                _PROMP_TPA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PROMP_TPB
        /// </summary>
        [DataNames("PROMP_TPB")]
        [SugarColumn(ColumnName = "promp_tpb", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_TPB {
            get { return _PROMP_TPB; }
            set {
                _PROMP_TPB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PROMP_TPC
        /// </summary>
        [DataNames("PROMP_TPC")]
        [SugarColumn(ColumnName = "promp_tpc", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_TPC {
            get { return _PROMP_TPC; }
            set { _PROMP_TPC = value; 
                OnPropertyChanged(); }
        }

        /// <summary>
        /// obtener o establecer (PROMP_TRA)
        /// </summary>
        [DataNames("PROMP_TRA")]
        [SugarColumn(ColumnName = "promp_tra", ColumnDescription = "PROMP_TRA", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_TRA {
            get { return _PROMP_TRA; }
            set {
                _PROMP_TRA = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_TRB)
        /// </summary>
        [DataNames("PROMP_TRB")]
        [SugarColumn(ColumnName = "promp_trb", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_TRB {
            get { return _PROMP_TRB; }
            set {
                _PROMP_TRB = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PROMP_TRC)
        /// </summary>
        [DataNames("PROMP_TRC")]
        [SugarColumn(ColumnName = "promp_trc", ColumnDescription = "", Length = 18, DecimalDigits = 4)]
        public decimal? PROMP_TRC {
            get { return _PROMP_TRC; }
            set {
                _PROMP_TRC = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la utlima fecha de modificacion del registro (PROMP_FM)
        /// </summary>
        [DataNames("PROMP_FM")]
        [SugarColumn(ColumnName = "promp_fm", ColumnDescription = "")]
        public DateTime? FechaModifica {
            get {
                if (fechaModifica >= new DateTime(1900, 1, 1))
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
