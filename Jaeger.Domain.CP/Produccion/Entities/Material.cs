﻿using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Empresa.Entities;

/// purpose:
/// develop: ANHE1 29092020 1713
namespace Jaeger.Domain.CP.Produccion.Entities {
    public class Material {
        public Material() { }
        [DataNames("AlmMP_ID")]
        public int IdAlmacen {
            get; set;
        }

        [DataNames("AlmMP_A")]
        public int Activo {
            get;set;
        }

        [DataNames("AlmMP_OP_ID")]
        public int PedPrd {
            get; set;
        }

        [DataNames("ALMMP_EXI")]
        public decimal Existencia {
            get; set;
        }

        [DataNames("AlmMP_Sol")]
        public decimal Requerido {
            get; set;
        }

        [DataNames("AlmMP_Ped")]
        public decimal Ordenado {
            get; set;
        }

        public ComercioModel GetComercio() {
            return new ComercioModel { Activo = true };
        }

        public MateriaPrima4Model GetMateriaPrima4() {

            return new MateriaPrima4Model { Activo = true };
        }
    }
}
