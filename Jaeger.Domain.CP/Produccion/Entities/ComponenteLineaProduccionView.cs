﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;
using SqlSugar;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Línea de Producción incluye componente e informacion de la orden de produccion (PedPro)
    /// </summary>
    [SugarTable("pedpro", "componente linea de produccion")]
    public class ComponenteLineaProduccionView : LineaProduccionModel, IPedProModel {

        public ComponenteLineaProduccionView() {

        }

        [DataNames("PedST_Sta")]
        public string Status { get; set; }

        /// <summary>
        /// obtener o establecer (PEDPRD_ST_ID)
        /// </summary>
        [DataNames("PEDPRD_ST_ID")]
        public int IdStatus { get; set; }

        [DataNames("DIR_NOM")]
        public string Cliente { get; set; }

        [DataNames("PedPrd_Nom")]
        public string Descripcion { get; set; }

        [DataNames("PEDPRD_ID")] 
        public int IdPedPrd { get; set; }

        [DataNames("PEDPRD_Can")]
        public decimal CantidadPedido { get; set; }

        [DataNames("PedPrd_Fec_Ped")]
        public DateTime FechaPedido { get; set; }

        [DataNames("PEDPRD_Fec_OP")]
        public DateTime FechaOrden { get; set; }

        [DataNames("PEDPRD_Fec_Aco")]
        public DateTime FechaAcuerdo { get; set; }

        #region componente / ensamble

        /// <summary>
        /// obtener o establecer clave (PedCom_Cla)
        /// </summary>
        [DataNames("PedCom_Cla")]
        public string ComponenteClave { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del componente (PEDCOM_NOM)
        /// </summary>
        [DataNames("PedCom_Nom")]
        public string Componente { get; set; }

        /// <summary>
        /// obtener o establecer secuencia del componente (PEDCOM_SEC)
        /// </summary>
        [DataNames("PedCom_sec")]
        public int ComponenteSecuencia { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad del componente (PedCom_Can_Com)
        /// </summary>
        [DataNames("PedCom_Can_Com")]
        public int ComponenteCantidad { get; set; }

        /// <summary>
        /// obtener o establecer el tiempo estimado en minutos (PedCom_T_Max)
        /// </summary>
        [DataNames("PedCom_T_Max")]
        public int ComponenteTiempoEstimado { get; set; }

        /// <summary>
        /// obtener o establecer tiempo acumulado real (PEDCOM_MINR)
        /// </summary>
        [DataNames("PedCom_MinR")]
        public int ComponenteTiempoReal { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_MIN)
        /// </summary>
        [DataNames("PEDCOM_FEC_MIN")]
        [SugarColumn(ColumnName = "pedcom_fec_min", ColumnDescription = "PEDCOM_FEC_MIN")]
        public DateTime? ComponenteFechaMinima { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_MAX)
        /// </summary>
        [DataNames("PEDCOM_FEC_MAX")]
        [SugarColumn(ColumnName = "pedcom_fec_max", ColumnDescription = "PEDCOM_FEC_MAX")]
        public DateTime? ComponenteFechaMaxima { get; set; }

        /// <summary>
        /// obtener o establecer PEDCOM_FEC_INI
        /// </summary>
        [DataNames("PEDCOM_FEC_INI")]
        [SugarColumn(ColumnName = "pedcom_fec_ini", ColumnDescription = "PEDCOM_FEC_INI")]
        public DateTime? ComponenteFechaInicial { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_FIN)
        /// </summary>
        [DataNames("PEDCOM_FEC_FIN")]
        [SugarColumn(ColumnName = "pedcom_fec_fin", ColumnDescription = "PEDCOM_FEC_FIN")]
        public DateTime? ComponenteFechaFinal { get; set; }

        #endregion

        #region departamento

        [DataNames("EDP1_nom")]
        public string Departamento { get; set; }

        [DataNames("EDP2_sec")]
        public string Seccion { get; set; }

        [DataNames("EDP2_nom")]
        public string Centro { get; set; }

        [DataNames("EDP3_nom")]
        public string Proceso { get; set; }

        [DataNames("EDP2_val_min")]
        public decimal ValorMinuto { get; set; }

        #endregion
    }
}
