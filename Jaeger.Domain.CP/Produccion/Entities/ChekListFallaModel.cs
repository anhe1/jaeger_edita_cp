﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// posibles fallas (SCHKLST)
    /// </summary>
    [SugarTable("SCHKLST", "Falla probable")]
    public class ChekListFallaModel : BasePropertyChangeImplementation, IChekListFallaModel {
        private int idCheck;
        private bool activo;
        private int idCheckList;
        private int idProceso;
        private int secuencia;
        private string descripcion;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;

        public ChekListFallaModel() {
            activo = true;
        }

        /// <summary>
        /// obtener o establecer el indce de la tabla (SCHKLST_ID)
        /// </summary>
        [DataNames("SCHKLST_ID")]
        [SugarColumn(ColumnName = "SCHKLST_ID", ColumnDescription = "indice de la tabla", IsNullable = false, IsIdentity = true, IsPrimaryKey = true)]
        public int IdFalla {
            get { return idCheck; }
            set {
                idCheck = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (SCHKLST_A)
        /// </summary>
        [DataNames("SCHKLST_A")]
        [SugarColumn(ColumnName = "SCHKLST_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la lista de verificacion (SCHKLST_CHKLST_ID)
        /// </summary>
        [DataNames("SCHKLST_CHKLST_ID")]
        [SugarColumn(ColumnName = "SCHKLST_CHKLST_ID", ColumnDescription = "indice de relacion con el proceso", IsNullable = false)]
        public int IdCheckList {
            get { return idCheckList; }
            set {
                idCheckList = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el proceso (SCHKLST_PROC_ID)
        /// </summary>
        [DataNames("SCHKLST_PROC_ID")]
        [SugarColumn(ColumnName = "SCHKLST_PROC_ID", ColumnDescription = "indice de relacion con el proceso", IsNullable = false)]
        public int IdProceso {
            get { return idProceso; }
            set {
                idProceso = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia a presentar (SCHKLST_SEC)
        /// </summary>
        [DataNames("SCHKLST_SEC")]
        [SugarColumn(ColumnName = "SCHKLST_SEC", ColumnDescription = "secuencia", IsNullable = false)]
        public int Secuencia {
            get { return secuencia; }
            set {
                secuencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion (SCHKLST_DESC)
        /// </summary>
        [DataNames("SCHKLST_DESC")]
        [SugarColumn(ColumnName = "SCHKLST_DESC", ColumnDescription = "descripcion", IsNullable = false)]
        public string Descripcion {
            get { return descripcion; }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro (SCHKLST_USU_N)
        /// </summary>
        [DataNames("SCHKLST_USU_N")]
        [SugarColumn(ColumnName = "SCHKLST_USU_N", ColumnDescription = "clave del usuario que crea el registro", Length = 10, IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return creo; }
            set {
                creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro (SCHKLST_USU_M)
        /// </summary>
        [DataNames("SCHKLST_USU_M")]
        [SugarColumn(ColumnName = "SCHKLST_USU_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return modifica; }
            set {
                modifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del nuevo registro (SCHKLST_FN)
        /// </summary>
        [DataNames("SCHKLST_FN")]
        [SugarColumn(ColumnName = "SCHKLST_FN", ColumnDescription = "descripcion", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return fechaNuevo; }
            set {
                fechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro (SCHKLST_FM)
        /// </summary>
        [DataNames("SCHKLST_FM")]
        [SugarColumn(ColumnName = "SCHKLST_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
