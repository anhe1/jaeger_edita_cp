﻿using System.Collections.Generic;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// evaluacion del area de produccion, reporte
    /// </summary>
    public class EvaluacionProduccionDiarioReporte {
        protected internal List<OrdenProduccionEvaluacionItem> _DataSource;

        public EvaluacionProduccionDiarioReporte() { }

        public EvaluacionProduccionDiarioReporte(List<OrdenProduccionEvaluacionItem> items) {
            this._DataSource = items;
        }

        public List<OrdenProduccionEvaluacionItem> DataSource {
            get {
                return _DataSource;
            }
            set { this._DataSource = value; }
        }
    }
}
