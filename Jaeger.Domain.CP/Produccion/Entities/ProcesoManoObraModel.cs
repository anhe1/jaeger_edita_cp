﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// tabla de procesos mano de obra (recursos PROMO)
    /// </summary>
    [SugarTable("PROMO", "tabla de procesos mano de obra")]
    public class ProcesoManoObraModel : BasePropertyChangeImplementation {
        private int idPromo;
        private bool activo;
        private int idProceso;
        private int idPuesto;
        private DateTime? fechaModifica;

        public ProcesoManoObraModel() {
            activo = true;
            fechaModifica = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indce de la tabla
        /// </summary>
        [DataNames("PROMO_ID")]
        [SugarColumn(ColumnName = "PROMO_ID", ColumnDescription = "indice de la tabla", IsNullable = false, IsIdentity = true, IsPrimaryKey = true)]
        public int IdPromo {
            get { return idPromo; }
            set {
                idPromo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("PROMO_A")]
        [SugarColumn(ColumnName = "PROMO_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el proceso
        /// </summary>
        [DataNames("PROMO_PRO_ID")]
        [SugarColumn(ColumnName = "PROMO_PRO_ID", ColumnDescription = "indice de relacion con el proceso", IsNullable = false)]
        public int IdProceso {
            get { return idProceso; }
            set {
                idProceso = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion el puesto
        /// </summary>
        [DataNames("PROMO_PUE_ID")]
        [SugarColumn(ColumnName = "PROMO_PUE_ID", ColumnDescription = "indice de relacion del puesto", IsNullable = false)]
        public int IdPuesto {
            get { return idPuesto; }
            set {
                idPuesto = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("PROMO_FM")]
        [SugarColumn(ColumnName = "PROMO_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
