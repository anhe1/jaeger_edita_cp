﻿using SqlSugar;
using System.ComponentModel;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Contribuyentes.Entities;
using Jaeger.Domain.CP.Produccion.Contracts;

/// purpose:
/// develop: ANHE1 29092020 1713
namespace Jaeger.Domain.CP.Produccion.Entities {
    public class OrdenProduccion : OrdenProduccionModel, IOrdenProduccionModel, Base.Contracts.IEntityBase {
        #region declaraciones
        protected ContribuyenteModel contribuyente;
        protected BindingList<Componente> componentes;
        private BindingList<PedFechaAcuerdoView> fechasAcuerdo;
        private BindingList<PedFecRDetailModelView> cambioStatus;
        private string claveVendedor;
        #endregion

        public OrdenProduccion() : base() {
            this.contribuyente = new ContribuyenteModel();
            this.componentes = new BindingList<Componente>();
        }

        #region contribuyentes
        [DataNames("PEDST_STA")]
        public string Status {
            get; set;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (DIR_ID)
        /// </summary>
        [DataNames("DIR_ID")]
        [SugarColumn(ColumnName = "dir_id", ColumnDescription = "DIR_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDirectorio {
            get { return this.contribuyente.IdDirectorio; }
            set {
                this.contribuyente.IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer DIR_NOM
        /// </summary>
        [DataNames("DIR_NOM")]
        [SugarColumn(ColumnName = "dir_nom", ColumnDescription = "DIR_NOM", Length = 100)]
        public string Cliente {
            get { return this.contribuyente.Nombre; }
            set {
                this.contribuyente.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        public ContribuyenteModel Contribuyente {
            get { return this.contribuyente; }
        }
        #endregion

        /// <summary>
        /// obtener o establecer la clave del vendedor
        /// </summary>
        [DataNames("VEN_CLA")]
        public string ClaveVendedor {
            get { return this.claveVendedor; }
            set {
                this.claveVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener diferencia entre la cantidad solicitada y la remisionada
        /// </summary>
        public decimal Diferencia {
            get { return this.Remisionado - this.Cantidad; }
        }

        public int DiasTranscurridos {
            get {
                if (this.FechaLiberacion != null) {
                    if (this.FechaRemision2 != null) {
                        System.TimeSpan difFechas = this.FechaRemision2.Value - this.FechaLiberacion.Value;
                        int days = (int)difFechas.TotalDays;
                        return days;
                    } else if (this.FechaRemision1 != null) {
                        System.TimeSpan difFechas = this.FechaRemision1.Value - this.FechaLiberacion.Value;
                        int days = (int)difFechas.TotalDays;
                        return days;
                    }
                }
                return 0;
            }
        }

        public int Saldo {
            get { return (int)this.Remisionado - (int)this.Cantidad; }
        }

        public BindingList<Componente> Componentes {
            get { return this.componentes; }
            set {
                this.componentes = value;
            }
        }

        /// <summary>
        /// obtener o establecer litado de fechas 
        /// </summary>
        public BindingList<PedFechaAcuerdoView> FechasAcuerdo {
            get { return this.fechasAcuerdo; }
            set {
                this.fechasAcuerdo = value;
            }
        }

        /// <summary>
        /// obtener o establecer lista de cambios de status
        /// </summary>
        public BindingList<PedFecRDetailModelView> CambioStatus {
            get { return this.cambioStatus; }
            set {
                this.cambioStatus = value;
            }
        }

        public bool CumpleCantidad {
            get { return this.Remisionado >= this.Cantidad; }
        }

        public bool CumpleFechas {
            get { return this.FechaAcuerdo >= this.FechaRemision2; }
        }

        /// <summary>
        /// boton de verificacion
        /// </summary>
        public string Verificado {
            get {
                if (this.IdVerificado == 0)
                    return "No Verificado";
                return "Verificado";
            }
        }
    }
}
