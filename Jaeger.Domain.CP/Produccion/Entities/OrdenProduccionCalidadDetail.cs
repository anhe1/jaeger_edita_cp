﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    public class OrdenProduccionCalidadDetail : OrdenProduccionCalidadModel {
        public OrdenProduccionCalidadDetail() : base() { }

        [DataNames("DIR_NOM")]
        public string Empleado { get; set; }
        
        [DataNames("PEDFECL_TEX")]
        public string Motivo { get; set; }

        [DataNames("EDP2_NOM")]
        public string Centro { get; set; }
    }
}
