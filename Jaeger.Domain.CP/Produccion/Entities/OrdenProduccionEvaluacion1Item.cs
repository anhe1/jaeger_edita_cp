﻿using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// orden de produccion con informacion para la evaluacion de cumplimiento del area de produccion
    /// </summary>
    public class OrdenProduccionEvaluacion1Item : OrdenProduccionEvaluacionData {
        #region declaraciones
        private DateTime? _FechaRemisionProduccion;
        #endregion

        public OrdenProduccionEvaluacion1Item() : base() { }

        #region informacion de entregas de produccion
        /// <summary>
        /// obtener o establecer fecha de emision de la ultima remision de produccion
        /// </summary>
        [DataNames("RMSPED_FECEMS")]
        public virtual DateTime? FechaRemisionProduccion {
            get {
                if (this._FechaRemisionProduccion > new DateTime(1899, 1, 1))
                    return this._FechaRemisionProduccion;
                return null;
            }
            set { this._FechaRemisionProduccion = value; }
        }

        /// <summary>
        /// obtener o establecer cantidad remisionada del area de produccion
        /// </summary>
        [DataNames("RMSPED_REM")]
        public virtual decimal CantidadRemisionProduccion { get; set; }
        #endregion

        #region cumplimiento
        public override bool CumpleFechaAcuerdo {
            get { return this.FechaAcuerdo <= this.FechaRemisionProduccion; }
        }

        public override bool CumpleCantidad {
            get { return this.CantidadRemisionProduccion >= this.Cantidad; }
        }

        public override decimal Diferencia {
            get { return this.Cantidad - this.CantidadRemisionProduccion; }
        }
        #endregion
    }
}
