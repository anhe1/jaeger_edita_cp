﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    ///<summary>
    /// Cambio de fecha de acuerdo (PedFec)
    ///</summary>
    public class PedFechaAcuerdoDetailModel : PedFechaAcuerdoModel, IPedFechaAcuerdoModel {
        private string motivo;

        public PedFechaAcuerdoDetailModel() : base() {

        }

        /// <summary>
        /// obtener o establecer motivo del cambio de la fecha de acuerdo
        /// </summary>
        [DataNames("PEDFECL_TEX")]
        public string Motivo {
            get { return this.motivo; }
            set {
                this.motivo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
