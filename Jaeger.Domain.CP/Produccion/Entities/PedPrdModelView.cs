﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Vista de Orden de Producción con componentes y ensambles (PedPrd)
    /// </summary>
    public class PedPrdModelView : PedPrdDetailModel, IOrdenProduccionModel {
        private string cliente;
        private string claveVendedor;
        private string status;
        private int secStatus;
        private BindingList<ComponenteView> componentes;
        private BindingList<PedFechaAcuerdoView> fechasAcuerdo;
        private BindingList<PedFecRDetailModelView> cambioStatus;

        public PedPrdModelView() : base() {
            this.componentes = new BindingList<ComponenteView>();
            this.fechasAcuerdo = new BindingList<PedFechaAcuerdoView>();
            this.cambioStatus = new BindingList<PedFecRDetailModelView>();
        }

        /// <summary>
        /// obtener o establecer el status de la orden de produccion
        /// </summary>
        [DataNames("PEDST_STA")]
        public string Status {
            get { return this.status; }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status numerico
        /// </summary>
        [DataNames("PEDST_SEC")]
        public int St {
            get { return this.secStatus; }
            set {
                this.secStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del cliente
        /// </summary>
        [DataNames("DIR_NOM")]
        public string Cliente {
            get { return this.cliente; }
            set {
                this.cliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del vendedor
        /// </summary>
        [DataNames("VEN_CLA")]
        public string ClaveVendedor {
            get { return this.claveVendedor; }
            set {
                this.claveVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener diferencia entre la cantidad solicitada y la remisionada
        /// </summary>
        public decimal Diferencia {
            get { return this.Cantidad - this.Remisionado; }
        }

        /// <summary>
        /// obtener o establecer litado de fechas 
        /// </summary>
        public new BindingList<PedFechaAcuerdoView> FechasAcuerdo {
            get { return this.fechasAcuerdo; }
            set {
                this.fechasAcuerdo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de cambios de status
        /// </summary>
        public BindingList<PedFecRDetailModelView> CambioStatus {
            get { return this.cambioStatus; }
            set {
                this.cambioStatus = value;
                this.OnPropertyChanged();
            }
        }
    }
}
