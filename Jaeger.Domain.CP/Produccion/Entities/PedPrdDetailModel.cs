﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Orden de Produccion (PedPrd)
    /// </summary>
    public class PedPrdDetailModel : OrdenProduccionModel {
        private BindingList<PedFechaAcuerdoDetailModel> fechaAcuerdo;
        private BindingList<ComponenteDetailModel> componentes;

        /// <summary>
        /// constructor
        /// </summary>
        public PedPrdDetailModel() : base() {
            this.componentes = new BindingList<ComponenteDetailModel>();
            this.fechaAcuerdo = new BindingList<PedFechaAcuerdoDetailModel>();
        }

        /// <summary>
        /// Componentes / Ensambles de la orden de produccion (PedCom)
        /// </summary>
        public BindingList<ComponenteDetailModel> Componentes {
            get { return this.componentes; }
            set {
                this.componentes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los cambios de fechas de acuerdo (PedFec)
        /// </summary>
        public BindingList<PedFechaAcuerdoDetailModel> FechasAcuerdo {
            get { return this.fechaAcuerdo; }
            set {
                this.fechaAcuerdo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
