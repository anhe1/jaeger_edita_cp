﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Orden de Producción (PEDPRD)
    /// </summary>
    [SugarTable("pedprd")]
    public class OrdenProduccionModel : BasePropertyChangeImplementation, IOrdenProduccionModel, Base.Contracts.IEntityBase {
        #region declaraciones
        private int index;
        private int activo;
        private int _PEDPRD_CLA1;
        private int _PEDPRD_CLA2;
        private int _PEDPRD_DIR_ID;
        private int _PEDPRD_DIRCON_ID;
        private int _PEDPRD_PED_ID;
        private int _PEDPRD_COTPRD_ID;
        private int _PEDPRD_VEN_ID;
        private string _PEDPRD_NOM;
        private int _PEDPRD_CAN;
        private decimal _PEDPRD_UNI;
        private decimal _PEDPRD_UNI2;
        private decimal _PEDPRD_PED;
        private decimal _PEDPRD_COT;
        private decimal _PEDPRD_UTI;
        private DateTime? _PEDPRD_FEC_PED;
        private DateTime? _PEDPRD_FEC_REQ;
        private DateTime? _PEDPRD_FEC_ACO;
        private DateTime? _PEDPRD_FEC_FPS;
        private DateTime? _PEDPRD_FEC_REC;
        private DateTime? _PEDPRD_FEC_OP;
        private string nota;
        private int secuencia;
        private int idStatus;
        private int _PEDPRD_JER;
        private string _PEDPRD_USU_N;
        private string _PEDPRD_USU_M;
        private DateTime? _PEDPRD_FM;
        private decimal _PEDPRD_FAC;
        private decimal _PEDPRD_FAC2;
        private decimal _PEDPRD_FACP;
        private decimal _PEDPRD_REM;
        private decimal _PEDPRD_REM2;
        private decimal _PEDPRD_REMP;
        private int _PEDPRD_LP_ID;
        private DateTime? _PEDPRD_FEC_VOBO;
        private DateTime? _PEDPRD_FEC_REM1;
        private DateTime? _PEDPRD_FEC_REM2;
        private int _PEDPRD_COM_ID;
        private int _PEDPRD_TAFRM_ID;
        private DateTime? _PEDPRD_FEC_ALM2;
        private DateTime? _PEDPRD_FEC_VOBOC;
        #endregion

        public OrdenProduccionModel() {
            this.activo = 1;
        }

        /// <summary>
        /// obtener o establecer Orden de Produccion (PEDPRD_ID)
        /// </summary>
        [DataNames("PEDPRD_ID")]
        [SugarColumn(ColumnName = "pedprd_id", ColumnDescription = "PEDPRD_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPedPrd {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (PEDPRD_A)
        /// </summary>
        [DataNames("PEDPRD_A")]
        [SugarColumn(ColumnName = "pedprd_a", ColumnDescription = "PEDPRD_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio (PEDPRD_DIR_ID)
        /// </summary>
        [DataNames("PEDPRD_DIR_ID")]
        [SugarColumn(ColumnName = "pedprd_dir_id", ColumnDescription = "PEDPRD_DIR_ID")]
        public int IdCliente {
            get { return this._PEDPRD_DIR_ID; }
            set {
                this._PEDPRD_DIR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del contacto del directorio (PEDPRD_DIRCON_ID)
        /// </summary>
        [DataNames("PEDPRD_DIRCON_ID")]
        [SugarColumn(ColumnName = "pedprd_dircon_id", ColumnDescription = "PEDPRD_DIRCON_ID")]
        public int IdContacto {
            get { return this._PEDPRD_DIRCON_ID; }
            set {
                this._PEDPRD_DIRCON_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRD_ST_ID)
        /// </summary>
        [DataNames("PEDPRD_ST_ID")]
        [SugarColumn(ColumnName = "pedprd_st_id", ColumnDescription = "PEDPRD_ST_ID")]
        public int IdStatus {
            get { return this.idStatus; }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRD_VEN_ID)
        /// </summary>
        [DataNames("PEDPRD_VEN_ID")]
        [SugarColumn(ColumnName = "pedprd_ven_id", ColumnDescription = "PEDPRD_VEN_ID")]
        public int IdVendedor {
            get { return this._PEDPRD_VEN_ID; }
            set {
                this._PEDPRD_VEN_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la jerarquia (pedprd_jer)
        /// </summary>
        [DataNames("PEDPRD_JER")]
        [SugarColumn(ColumnName = "pedprd_jer", ColumnDescription = "PEDPRD_JER")]
        public int Jerarquia {
            get { return this._PEDPRD_JER; }
            set {
                this._PEDPRD_JER = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad total solicitada (PEDPRD_CAN)
        /// </summary>
        [DataNames("PEDPRD_CAN")]
        [SugarColumn(ColumnName = "pedprd_can", ColumnDescription = "PEDPRD_CAN")]
        public int Cantidad {
            get { return this._PEDPRD_CAN; }
            set {
                this._PEDPRD_CAN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRD_CLA1)
        /// </summary>
        [DataNames("PEDPRD_CLA1")]
        [SugarColumn(ColumnName = "pedprd_cla1", ColumnDescription = "PEDPRD_CLA1")]
        public int PEDPRD_CLA1 {
            get { return this._PEDPRD_CLA1; }
            set {
                this._PEDPRD_CLA1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRD_CLA2)
        /// </summary>
        [DataNames("PEDPRD_CLA2")]
        [SugarColumn(ColumnName = "pedprd_cla2", ColumnDescription = "PEDPRD_CLA2")]
        public int PEDPRD_CLA2 {
            get { return this._PEDPRD_CLA2; }
            set {
                this._PEDPRD_CLA2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        [DataNames("PEDPRD_COM_ID")]
        [SugarColumn(ColumnName = "pedprd_com_id", ColumnDescription = "PEDPRD_COM_ID")]
        public int PEDPRD_COM_ID {
            get { return this._PEDPRD_COM_ID; }
            set {
                this._PEDPRD_COM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_LP_ID
        /// </summary>
        [DataNames("PEDPRD_LP_ID")]
        [SugarColumn(ColumnName = "pedprd_lp_id", ColumnDescription = "PEDPRD_LP_ID")]
        public int PEDPRD_LP_ID {
            get { return this._PEDPRD_LP_ID; }
            set {
                this._PEDPRD_LP_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRD_PED_ID)
        /// </summary>
        [DataNames("PEDPRD_PED_ID")]
        [SugarColumn(ColumnName = "pedprd_ped_id", ColumnDescription = "PEDPRD_PED_ID")]
        public int PEDPRD_PED_ID {
            get { return this._PEDPRD_PED_ID; }
            set {
                this._PEDPRD_PED_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer  PEDPRD_SEC
        /// </summary>
        [DataNames("PEDPRD_SEC")]
        [SugarColumn(ColumnName = "pedprd_sec", ColumnDescription = "PEDPRD_SEC")]
        public int Secuencia {
            get { return this.secuencia; }
            set {
                this.secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de cotizacion (PEDPRD_COTPRD_ID)
        /// </summary>
        [DataNames("PEDPRD_COTPRD_ID")]
        [SugarColumn(ColumnName = "pedprd_cotprd_id", ColumnDescription = "PEDPRD_COTPRD_ID")]
        public int Cotizacion {
            get { return this._PEDPRD_COTPRD_ID; }
            set {
                this._PEDPRD_COTPRD_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el precio unitario esta verificado (PEDPRD_TAFRM_ID)
        /// </summary>
        [DataNames("PEDPRD_TAFRM_ID")]
        [SugarColumn(ColumnName = "pedprd_tafrm_id", ColumnDescription = "PEDPRD_TAFRM_ID")]
        public int IdVerificado {
            get { return this._PEDPRD_TAFRM_ID; }
            set {
                this._PEDPRD_TAFRM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de la venta de la orden de produccion o pedido (PEDPRD_$PED)
        /// </summary>
        [DataNames("PEDPRD_$PED")]
        [SugarColumn(ColumnName = "pedprd_$ped", ColumnDescription = "PEDPRD_$PED", Length = 18, DecimalDigits = 4)]
        public decimal TotalVenta {
            get { return this._PEDPRD_PED; }
            set {
                this._PEDPRD_PED = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad acumulada remisionada (PEDPRD_REM)
        /// </summary>
        [DataNames("PEDPRD_REM")]
        [SugarColumn(ColumnName = "pedprd_rem", ColumnDescription = "PEDPRD_REM", Length = 18, DecimalDigits = 4)]
        public decimal Remisionado {
            get { return this._PEDPRD_REM; }
            set {
                this._PEDPRD_REM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_$REM
        /// </summary>
        [DataNames("PEDPRD_$REM")]
        [SugarColumn(ColumnName = "pedprd_$rem", ColumnDescription = "PEDPRD_$REM", Length = 18, DecimalDigits = 4)]
        public decimal TotalRemisionado {
            get { return this._PEDPRD_REM2; }
            set {
                this._PEDPRD_REM2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_$REMP 
        /// </summary>
        [DataNames("PEDPRD_$REMP")]
        [SugarColumn(ColumnName = "pedprd_$remp", ColumnDescription = "PEDPRD_$REMP", Length = 18, DecimalDigits = 4)]
        public decimal PEDPRD_REMP {
            get { return this._PEDPRD_REMP; }
            set {
                this._PEDPRD_REMP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la utilidad del pedido u orden de produccion (PEDPRD_$UTI)
        /// </summary>
        [DataNames("PEDPRD_$UTI")]
        [SugarColumn(ColumnName = "pedprd_$uti", ColumnDescription = "PEDPRD_$UTI", Length = 18, DecimalDigits = 4)]
        public decimal Utilidad {
            get { return this._PEDPRD_UTI; }
            set {
                this._PEDPRD_UTI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRD_UNI)
        /// </summary>
        [DataNames("PEDPRD_UNI")]
        [SugarColumn(ColumnName = "pedprd_uni", ColumnDescription = "PEDPRD_UNI", Length = 18, DecimalDigits = 4)]
        public decimal Unidad {
            get { return this._PEDPRD_UNI; }
            set {
                this._PEDPRD_UNI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_$UNI
        /// </summary>
        [DataNames("PEDPRD_$UNI")]
        [SugarColumn(ColumnName = "pedprd_$uni", ColumnDescription = "PEDPRD_$UNI", Length = 18, DecimalDigits = 4)]
        public decimal Unitario {
            get { return this._PEDPRD_UNI2; }
            set {
                this._PEDPRD_UNI2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del costo (PEDPRD_$COT)
        /// </summary>
        [DataNames("PEDPRD_$COT")]
        [SugarColumn(ColumnName = "pedprd_$cot", ColumnDescription = "PEDPRD_$COT", Length = 18, DecimalDigits = 4)]
        public decimal TotalCosto {
            get { return this._PEDPRD_COT; }
            set {
                this._PEDPRD_COT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cantidad facturada (PEDPRD_FAC)
        /// </summary>
        [DataNames("PEDPRD_FAC")]
        [SugarColumn(ColumnName = "pedprd_fac", ColumnDescription = "PEDPRD_FAC", Length = 18, DecimalDigits = 4)]
        public decimal PEDPRD_FAC {
            get { return this._PEDPRD_FAC; }
            set {
                this._PEDPRD_FAC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe facturada (PEDPRD_$FAC)
        /// </summary>
        [DataNames("PEDPRD_$FAC")]
        [SugarColumn(ColumnName = "pedprd_$fac", ColumnDescription = "PEDPRD_$FAC", Length = 18, DecimalDigits = 4)]
        public decimal PEDPRD_FAC2 {
            get { return this._PEDPRD_FAC2; }
            set {
                this._PEDPRD_FAC2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe cobrado / pagado por factura (PEDPRD_$FACP)
        /// </summary>
        [DataNames("PEDPRD_$FACP")]
        [SugarColumn(ColumnName = "pedprd_$facp", ColumnDescription = "PEDPRD_$FACP", Length = 18, DecimalDigits = 4)]
        public decimal PEDPRD_FACP {
            get { return this._PEDPRD_FACP; }
            set {
                this._PEDPRD_FACP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del producto (PEDPRD_NOM)
        /// </summary>
        [DataNames("PEDPRD_NOM")]
        [SugarColumn(ColumnName = "pedprd_nom", ColumnDescription = "PEDPRD_NOM", Length = 100)]
        public string Descripcion {
            get { return this._PEDPRD_NOM; }
            set {
                this._PEDPRD_NOM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_OBS
        /// </summary>
        [DataNames("PEDPRD_OBS")]
        [SugarColumn(ColumnName = "pedprd_obs", ColumnDescription = "PEDPRD_OBS", Length = 255)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDPRD_FEC_PED)
        /// </summary>
        [DataNames("PEDPRD_FEC_PED")]
        [SugarColumn(ColumnName = "pedprd_fec_ped", ColumnDescription = "PEDPRD_FEC_PED")]
        public DateTime? FechaPedido {
            get {
                if (this._PEDPRD_FEC_PED >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_PED;
                return null;
            }
            set {
                this._PEDPRD_FEC_PED = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de la orden de producción (PEDPRD_FEC_OP)
        /// </summary>
        [DataNames("PEDPRD_FEC_OP")]
        [SugarColumn(ColumnName = "pedprd_fec_op", ColumnDescription = "PEDPRD_FEC_OP")]
        public DateTime? FechaOrden {
            get {
                if (this._PEDPRD_FEC_OP >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_OP;
                return null;
            }
            set {
                this._PEDPRD_FEC_OP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de solicitud del cliente (PEDPRD_FEC_REQ)
        /// </summary>
        [DataNames("PEDPRD_FEC_REQ")]
        [SugarColumn(ColumnName = "pedprd_fec_req", ColumnDescription = "PEDPRD_FEC_REQ")]
        public DateTime? FechaCliente {
            get {
                if (this._PEDPRD_FEC_REQ >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_REQ;
                return null;
            }
            set {
                this._PEDPRD_FEC_REQ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de acuerdo (PEDPRD_FEC_ACO)
        /// </summary>
        [DataNames("PEDPRD_FEC_ACO")]
        [SugarColumn(ColumnName = "pedprd_fec_aco", ColumnDescription = "PEDPRD_FEC_ACO")]
        public DateTime? FechaAcuerdo {
            get {
                if (this._PEDPRD_FEC_ACO >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_ACO;
                return null;
            }
            set {
                this._PEDPRD_FEC_ACO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision de la primera remision PEDPRD_FEC_REM1
        /// </summary>
        [DataNames("PEDPRD_FEC_REM1")]
        [SugarColumn(ColumnName = "pedprd_fec_rem1", ColumnDescription = "PEDPRD_FEC_REM1")]
        public DateTime? FechaRemision1 {
            get {
                if (this._PEDPRD_FEC_REM1 >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_REM1;
                return null;
            }
            set {
                this._PEDPRD_FEC_REM1 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer establecer la fecha de emision de la ultima remision (PEDPRD_FEC_REM2)
        /// </summary>
        [DataNames("PEDPRD_FEC_REM2")]
        [SugarColumn(ColumnName = "pedprd_fec_rem2", ColumnDescription = "PEDPRD_FEC_REM2")]
        public DateTime? FechaRemision2 {
            get {
                if (this._PEDPRD_FEC_REM2 >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_REM2;
                return null;
            }
            set {
                this._PEDPRD_FEC_REM2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de vobo de calidad (pedprd_fec_voboc)
        /// </summary>
        [DataNames("PEDPRD_FEC_VOBOC")]
        [SugarColumn(ColumnName = "pedprd_fec_voboc", ColumnDescription = "PEDPRD_FEC_VOBOC")]
        public DateTime? FechaVobo {
            get {
                if (this._PEDPRD_FEC_VOBOC >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_VOBOC;
                return null;
            }
            set {
                this._PEDPRD_FEC_VOBOC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de liberacion PEDPRD_FEC_ALM2
        /// </summary>
        [DataNames("PEDPRD_FEC_ALM2")]
        [SugarColumn(ColumnName = "pedprd_fec_alm2", ColumnDescription = "PEDPRD_FEC_ALM2")]
        public DateTime? FechaLiberacion {
            get {
                if (this._PEDPRD_FEC_ALM2 >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_ALM2;
                return null;
            }
            set {
                this._PEDPRD_FEC_ALM2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_FEC_FPS
        /// </summary>
        [DataNames("PEDPRD_FEC_FPS")]
        [SugarColumn(ColumnName = "pedprd_fec_fps", ColumnDescription = "PEDPRD_FEC_FPS")]
        public DateTime? PEDPRD_FEC_FPS {
            get {
                if (this._PEDPRD_FEC_FPS >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_FPS;
                return null;
            }
            set {
                this._PEDPRD_FEC_FPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de recompra (pedprd_fec_rec)
        /// </summary>
        [DataNames("PEDPRD_FEC_REC")]
        [SugarColumn(ColumnName = "pedprd_fec_rec", ColumnDescription = "PEDPRD_FEC_REC", IsNullable = true)]
        public DateTime? FechaReCompra {
            get {
                if (this._PEDPRD_FEC_REC >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_REC;
                return null;
            }
            set {
                this._PEDPRD_FEC_REC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha devisto bueno de calidad (PEDPRD_FEC_VOBO)
        /// </summary>
        [DataNames("PEDPRD_FEC_VOBO")]
        [SugarColumn(ColumnName = "pedprd_fec_vobo", ColumnDescription = "PEDPRD_FEC_VOBO")]
        public DateTime? PEDPRD_FEC_VOBO {
            get {
                if (this._PEDPRD_FEC_VOBO >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FEC_VOBO;
                return null;
            }
            set {
                this._PEDPRD_FEC_VOBO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_USU_N
        /// </summary>
        [DataNames("PEDPRD_USU_N")]
        [SugarColumn(ColumnName = "pedprd_usu_n", ColumnDescription = "PEDPRD_USU_N", Length = 7)]
        public string Creo {
            get { return this._PEDPRD_USU_N; }
            set {
                this._PEDPRD_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// solo para cumplir con el contrato, siempre regresa la fecha de pedido
        /// </summary>
        public DateTime FechaNuevo {
            get {
                if (this.FechaPedido != null)
                    return this.FechaPedido.Value;
                return DateTime.Now;
            }
            set {
                var nada = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_USU_M
        /// </summary>
        [DataNames("PEDPRD_USU_M")]
        [SugarColumn(ColumnName = "pedprd_usu_m", ColumnDescription = "PEDPRD_USU_M", Length = 7)]
        public string Modifica {
            get { return this._PEDPRD_USU_M; }
            set {
                this._PEDPRD_USU_M = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDPRD_FM
        /// </summary>
        [DataNames("PEDPRD_FM")]
        [SugarColumn(ColumnName = "pedprd_fm", ColumnDescription = "PEDPRD_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._PEDPRD_FM >= new DateTime(1900, 1, 1))
                    return this._PEDPRD_FM;
                return null;
            }
            set {
                this._PEDPRD_FM = value;
                this.OnPropertyChanged();
            }
        }
    }
}
