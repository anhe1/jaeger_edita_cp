﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Proceso
    /// </summary>
    public class ProcesoDetailModel : ProcesoModel, IProcesoModel {
        private BindingList<ProcesoEtapaDetailModel> procesoEtapaDetailModels;
        private BindingList<CheckListDetailModel> checkList;
        private BindingList<ProcesoManoObraModel> manoObra;
        private BindingList<ProMpDetailModelView> recusrosMP;
        private BindingList<ProcesoProcedimientoModel> procedimiento;

        /// <summary>
        /// constructor
        /// </summary>
        public ProcesoDetailModel() {
            Etapas = new BindingList<ProcesoEtapaDetailModel>();
            CheckList = new BindingList<CheckListDetailModel>();
            RecursosMO = new BindingList<ProcesoManoObraModel>();
            RecursosMP = new BindingList<ProMpDetailModelView>();
            procedimiento = new BindingList<ProcesoProcedimientoModel>();
        }

        public BindingList<ProcesoEtapaDetailModel> Etapas {
            get { return procesoEtapaDetailModels; }
            set {
                procesoEtapaDetailModels = value;
                OnPropertyChanged();
            }
        }

        public BindingList<CheckListDetailModel> CheckList {
            get { return checkList; }
            set {
                checkList = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// lista de recursos de mano de obra
        /// </summary>
        public BindingList<ProcesoManoObraModel> RecursosMO {
            get { return manoObra; }
            set {
                manoObra = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// listado de recursos de materia prima
        /// </summary>
        public BindingList<ProMpDetailModelView> RecursosMP {
            get { return recusrosMP; }
            set {
                recusrosMP = value;
                OnPropertyChanged();
            }
        }

        public BindingList<ProcesoProcedimientoModel> Procedimiento {
            get { return this.procedimiento; }
            set { this.procedimiento = value;
                this.OnPropertyChanged();
            }
        }
    }
}
