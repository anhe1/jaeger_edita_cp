﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// cambio de status (PedFecR)
    /// </summary>
    public class PedFecRDetailModel : PedFecRModel, IPedFecRModel {
        public PedFecRDetailModel() : base() {

        }
    }
}
