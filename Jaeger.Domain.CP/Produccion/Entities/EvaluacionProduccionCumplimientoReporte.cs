﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// evaluacion del area de produccion, reporte
    /// </summary>
    public class EvaluacionProduccionCumplimientoReporte {
        protected internal List<OrdenProduccionEvaluacion1Item> _OrdenProduccion;

        public EvaluacionProduccionCumplimientoReporte() { }

        public EvaluacionProduccionCumplimientoReporte(List<OrdenProduccionEvaluacion1Item> items) {
            this._OrdenProduccion = items;
        }

        public DateTime Fecha { get; set; }

        public List<OrdenProduccionEvaluacion1Item> DataSource {
            get {
                return _OrdenProduccion;
            }
            set { this._OrdenProduccion = value; }
        }

        public List<OrdenProduccionCalidadDetail> Calidad { get; set; }

        public int TotalMateriaPrima { get { return this.Calidad.Where(it => it.IdMotivo == 3).Count(); } }
        public int TotalMetodo { get { return this.Calidad.Where(it => it.IdMotivo == 5).Count(); } }
        public int TotalManoObra { get { return this.Calidad.Where(it => it.IdMotivo == 8).Count(); } }
        public int TotalMedioAmbiente {
            get {
                return this.Calidad.Where(it => it.IdMotivo == 9).Count();
            }
        }
         public int Maquinaria { get { return this.Calidad.Where(it => it.IdMotivo == 12).Count(); } }

        /// <summary>
        /// ordenes del periodo
        /// </summary>
        public List<OrdenProduccionEvaluacion1Item> DelDia {
            get { return this._OrdenProduccion.Where(it => it.FechaAcuerdo <= this.Fecha).ToList(); }
        }

        /// <summary>
        /// ordenes por vencer en el periodo
        /// </summary>
        public List<OrdenProduccionEvaluacion1Item> PorVencer {
            get { return this._OrdenProduccion.Where(it => it.FechaAcuerdo > this.Fecha).ToList(); }
        }

        public int TotalDiaAnterior {
            get { return this.DelDia.Where(it => it.FechaAcuerdo < this.Fecha).Count(); }
        }

        public int TotalDelDia {
            get { return this.DelDia.Where(it => it.FechaAcuerdo == this.Fecha).Count(); }
        }

        public int TotalEntregadas {
            get { return this.DelDia.Where(it => it.IdStatus == 9).Count(); }
        }

        public int TotalPorEntregar {
            get { return this.DelDia.Where(it => it.IdStatus != 9).Count(); }
        }

        /// <summary>
        /// ordenes que cumplen fechas de acuerdo
        /// </summary>
        public int TotalFechaAcuerdo {
            get { return this.DelDia.Where(it => it.CumpleFechaAcuerdo == true).Count(); }
        }

        public int TotalNoFechaAcuerdo {
            get { return this.DelDia.Where(it => it.CumpleFechaAcuerdo == false).Count(); }
        }

        public int TotalAEntregar {
            get { return this.TotalDelDia + this.TotalDiaAnterior; }
        }

        public int TotalSigPeriodo {
            get { return (this.TotalDelDia + this.TotalDiaAnterior) - this.TotalEntregadas; }
        }

        /// <summary>
        /// cumple con cantidad
        /// </summary>
        public int TotalCantidad {
            get { return this.DelDia.Where(it => it.CumpleCantidad == true).Count(); }
        }

        /// <summary>
        /// cumple con cantidad
        /// </summary>
        public int TotalNoCantidad {
            get { return this.DelDia.Where(it => it.CumpleCantidad == false).Count(); }
        }
    }
}
