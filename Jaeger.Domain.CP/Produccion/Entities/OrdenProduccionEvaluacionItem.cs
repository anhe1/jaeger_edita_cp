﻿using SqlSugar;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    public class OrdenProduccionEvaluacionItem : OrdenProduccionModel, IOrdenProduccionModel {
        public OrdenProduccionEvaluacionItem() : base() { }

        #region contribuyentes
        [DataNames("PEDST_STA")]
        public string Status { get; set; }

        /// <summary>
        /// obtener o establecer indice de la tabla (DIR_ID)
        /// </summary>
        [DataNames("DIR_ID")]
        [SugarColumn(ColumnName = "dir_id", ColumnDescription = "DIR_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer DIR_NOM
        /// </summary>
        [DataNames("DIR_NOM")]
        [SugarColumn(ColumnName = "dir_nom", ColumnDescription = "DIR_NOM", Length = 100)]
        public string Cliente { get; set; }
        #endregion

        #region informacion del vendedor
        /// <summary>
        /// obtener o establecer la clave del vendedor
        /// </summary>
        [DataNames("VEN_CLA")]
        public string ClaveVendedor { get; set; }
        #endregion
    }
}
