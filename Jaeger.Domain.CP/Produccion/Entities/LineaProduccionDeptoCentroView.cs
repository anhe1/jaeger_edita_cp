﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Vista de Línea de Producción con la combinacion de la vista del departamentos y centros (PedPro)
    /// </summary>
    public class LineaProduccionDeptoCentroView : LineaProduccionModel, IPedProModel, IProcesoView {
        /// <sumary>
        /// obtener o establecer (EDP3_ID)
        /// </sumary>
        [DataNames("EDP3_ID")]
        public int IdProceso { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_A)
        /// </sumary>
        //[DataNames("EDP3_A")]
        //public int Activo { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP1_ARE)
        /// </sumary>
        [DataNames("EDP1_ARE")]
        public string Area { get; set; }

        [DataNames("EDP1_ID")]
        public int IdDepto { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP1_NOM)
        /// </sumary>
        [DataNames("EDP1_NOM")]
        public string Departamento { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_SEC)
        /// </sumary>
        [DataNames("EDP2_SEC")]
        public string Seccion { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_NOM)
        /// </sumary>
        [DataNames("EDP2_NOM")]
        public string Centro { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_NOM)
        /// </sumary>
        [DataNames("EDP3_NOM")]
        public string Proceso { get; set; }

        public string Completo {
            get {
                return string.Concat(Seccion, " / ", Centro, " / ", Proceso);
            }
        }
    }
}
