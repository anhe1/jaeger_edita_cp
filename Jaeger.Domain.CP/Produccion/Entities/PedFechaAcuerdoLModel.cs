﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Motivo del cambio de fecha de acuerdo (PEDFECL)
    /// </summary>
    public class PedFechaAcuerdoLModel : BaseSingleModel {
        #region declaraciones
        private bool _Activo = true;
        private DateTime? _FechaModifica;
        #endregion

        public PedFechaAcuerdoLModel() { }

        public PedFechaAcuerdoLModel(int id, string descripcion) : base(id, descripcion) { }

        [DataNames("PEDFECL_ID")]
        public new int Id {
            get { return base.Id; }
            set {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PEDFECL_A")]
        public bool Activo {
            get { return _Activo; }
            set {
                _Activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PEDFECL_TEX")]
        public new string Descripcion {
            get { return base.Descripcion; }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PEDFECL_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._FechaModifica >= new DateTime(1900, 1, 1))
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
