﻿using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    public class OrdenProduccionReporte : OrdenProduccionEvaluacionData, IOrdenProduccionModel {
        public OrdenProduccionReporte() : base() { }

        /// <summary>
        /// obtener o establecer nota de la fecha de acuerdo
        /// </summary>
        [DataNames("PEDFEC_OBS")]
        public string NotaFechaAcuerdo { get; set; }

        /// <summary>
        /// obtener diferencia entre la cantidad solicitada y la remisionada
        /// </summary>
        public override decimal Diferencia {
            get { return this.Cantidad - this.Remisionado; }
        }

        public override int DiasTranscurridos {
            get { return (int)this.Cantidad; }
        }

        public override bool CumpleFechaAcuerdo {
            get { return this.FechaAcuerdo <= this.FechaRemision1; }
        }

        public override bool CumpleCantidad {
            get { return this.Remisionado >= this.Cantidad; }
        }
    }
}
