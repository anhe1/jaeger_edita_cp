﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    ///<summary>
    /// Cambio de fecha de acuerdo (PEDFEC)
    ///</summary>
    [SugarTable("pedfec")]
    public class PedFechaAcuerdoModel : BasePropertyChangeImplementation, IPedFechaAcuerdoModel {
        #region declaraciones
        private int index;
        private int activo;
        private int _PEDFEC_PEDPRD_ID;
        private int _PEDFEC_FECL_ID;
        private int _PEDFEC_CEN_ID;
        private int _PEDFEC_DIR_ID;
        private DateTime _PEDFEC_FEC;
        private string _PEDFEC_OBS;
        private string _PEDFEC_USU_N;
        private DateTime? _PEDFEC_USU_F;
        private int? _PEDFEC_USU_FM;
        private DateTime? _PEDFEC_FM;
        #endregion

        public PedFechaAcuerdoModel() {
            this.activo = 1;
            this.Fecha = DateTime.Now.AddDays(1);
        }

        /// <summary>
        /// obtener o establecer indice incremental PEDFEC_ID
        /// </summary>
        [DataNames("PEDFEC_ID")]
        [SugarColumn(ColumnName = "pedfec_id", ColumnDescription = "PEDFEC_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPedFec {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (PEDFEC_A)
        /// </summary>
        [DataNames("PEDFEC_A")]
        [SugarColumn(ColumnName = "pedfec_a", ColumnDescription = "PEDFEC_A")]
        public int Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la orden de produccion (PEDFEC_PEDPRD_ID)
        /// </summary>
        [DataNames("PEDFEC_PEDPRD_ID")]
        [SugarColumn(ColumnName = "pedfec_pedprd_id", ColumnDescription = "PEDFEC_PEDPRD_ID")]
        public int IdPedPrd {
            get { return this._PEDFEC_PEDPRD_ID; }
            set {
                this._PEDFEC_PEDPRD_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de lista de motivos de cambios de fechas (PEDFEC_FECL_ID)
        /// </summary>
        [DataNames("PEDFEC_FECL_ID")]
        [SugarColumn(ColumnName = "pedfec_fecl_id", ColumnDescription = "PEDFEC_FECL_ID")]
        public int PEDFEC_FECL_ID {
            get { return this._PEDFEC_FECL_ID; }
            set {
                this._PEDFEC_FECL_ID = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer indice del centro productivo (PEDFEC_CEN_ID)
        /// </summary>
        [DataNames("PEDFEC_CEN_ID")]
        [SugarColumn(ColumnName = "pedfec_cen_id", ColumnDescription = "PEDFEC_CEN_ID")]
        public int PEDFEC_CEN_ID {
            get { return this._PEDFEC_CEN_ID; }
            set {
                this._PEDFEC_CEN_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio (PEDFEC_DIR_ID)
        /// </summary>
        [DataNames("PEDFEC_DIR_ID")]
        [SugarColumn(ColumnName = "pedfec_dir_id", ColumnDescription = "PEDFEC_DIR_ID")]
        public int PEDFEC_DIR_ID {
            get { return this._PEDFEC_DIR_ID; }
            set {
                this._PEDFEC_DIR_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFEC_USU_FM
        /// </summary>
        [DataNames("PEDFEC_USU_FM")]
        [SugarColumn(ColumnName = "pedfec_usu_fm", ColumnDescription = "PEDFEC_USU_FM")]
        public int? PEDFEC_USU_FM {
            get { return this._PEDFEC_USU_FM; }
            set {
                this._PEDFEC_USU_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de acuerdo PEDFEC_FEC
        /// </summary>
        [DataNames("PEDFEC_FEC")]
        [SugarColumn(ColumnName = "pedfec_fec", ColumnDescription = "PEDFEC_FEC")]
        public DateTime Fecha {
            get { return this._PEDFEC_FEC; }
            set {
                this._PEDFEC_FEC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFEC_OBS
        /// </summary>
        [DataNames("PEDFEC_OBS")]
        [SugarColumn(ColumnName = "pedfec_obs", ColumnDescription = "PEDFEC_OBS", Length = 50)]
        public string Nota {
            get { return this._PEDFEC_OBS; }
            set {
                this._PEDFEC_OBS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFEC_USU_N
        /// </summary>
        [DataNames("PEDFEC_USU_N")]
        [SugarColumn(ColumnName = "pedfec_usu_n", ColumnDescription = "PEDFEC_USU_N", Length = 7)]
        public string Creo {
            get { return this._PEDFEC_USU_N; }
            set {
                this._PEDFEC_USU_N = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFEC_USU_F
        /// </summary>
        [DataNames("PEDFEC_USU_F")]
        [SugarColumn(ColumnName = "pedfec_usu_f", ColumnDescription = "PEDFEC_USU_F")]
        public DateTime? PEDFEC_USU_F {
            get {
                if (this._PEDFEC_USU_F >= new DateTime(1900, 1, 1))
                    return this._PEDFEC_USU_F;
                return null;
            }
            set {
                this._PEDFEC_USU_F = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDFEC_FM
        /// </summary>
        [DataNames("PEDFEC_FM")]
        [SugarColumn(ColumnName = "pedfec_fm", ColumnDescription = "PEDFEC_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._PEDFEC_FM >= new DateTime(1900, 1, 1))
                    return this._PEDFEC_FM;
                return null;
            }
            set {
                this._PEDFEC_FM = value;
                this.OnPropertyChanged();
            }
        }
    }
}
