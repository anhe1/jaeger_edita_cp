﻿using System.ComponentModel;
using Jaeger.Domain.Base.Services;

/// purpose:
/// develop: ANHE1 29092020 1713
namespace Jaeger.Domain.CP.Produccion.Entities {
    public class Componente : ComponenteModel {
        protected BindingList<LineaProduccion> lineaProduccions;
        public Componente() : base() {
        }

        public Componente(ComponenteModel model) : base() {
            MapperClassExtensions.MatchAndMap<ComponenteModel, Componente>(model, this);
        }

        public BindingList<LineaProduccion> LineaProduccion {
            get { return this.lineaProduccions; }
            set {
                this.lineaProduccions = value;
                this.OnPropertyChanged();
            }
        }
    }
}
