﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.CP.Produccion.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// linea de produccion (PedCom)
    /// </summary>
    public class ComponenteDetailModel : ComponenteModel, IPedComModel {
        private BindingList<LineaProduccionModel> lineaProduccion;

        /// <summary>
        /// constructor
        /// </summary>
        public ComponenteDetailModel() {

        }

        /// <summary>
        /// Linea de Produccion
        /// </summary>
        public BindingList<LineaProduccionModel> LineaProduccion {
            get { return this.lineaProduccion; }
            set {
                this.lineaProduccion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
