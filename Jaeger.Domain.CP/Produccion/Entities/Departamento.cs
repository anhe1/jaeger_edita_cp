﻿using Jaeger.Domain.Base.Services;
using Jaeger.Domain.CP.Produccion.Contracts;

/// purpose:
/// develop: ANHE1 29092020 1713
namespace Jaeger.Domain.CP.Produccion.Entities {
    public class Departamento : DepartamentoModel, IDepartamentoModel {
        public Departamento() : base() { }

        public Departamento(Departamento model) : base() {
            MapperClassExtensions.MatchAndMap<DepartamentoModel, Departamento>(model, this);
        }
    }
}
