﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// PRO3
    /// </summary>
    [SugarTable("PRO3", "Procedimiento")]
    public class ProcesoProcedimientoModel : BasePropertyChangeImplementation, IProcesoProcedimientoModel {
        private int idProcedimiento;
        private bool activo;
        private string descripcion;
        private int idPuesto;
        private string calidad;
        private int evaluacion;
        private DateTime? fechaModifica;

        public ProcesoProcedimientoModel() {
            activo = true;
        }

        /// <summary>
        /// obtener o establecer el indce de la tabla (PRO3_ID)
        /// </summary>
        [DataNames("PRO3_ID")]
        [SugarColumn(ColumnName = "PRO3_ID", ColumnDescription = "indice de la tabla", IsNullable = false, IsIdentity = true, IsPrimaryKey = true)]
        public int IdProcedimiento {
            get { return idProcedimiento; }
            set {
                idProcedimiento = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        /// obtener o establecer registro activo (PRO3_A9
        /// </summary>
        [DataNames("PRO3_A")]
        [SugarColumn(ColumnName = "PRO3_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion el puesto (PRO3_PUE_ID)
        /// </summary>
        [DataNames("PRO3_PUE_ID")]
        [SugarColumn(ColumnName = "PRO3_PUE_ID", ColumnDescription = "indice de relacion del puesto", IsNullable = false)]
        public int IdPuesto {
            get { return idPuesto; }
            set {
                idPuesto = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PRO3_PRO2_ID
        /// </summary>
        [DataNames("PRO3_PRO2_ID")]
        [SugarColumn(ColumnName = "PRO3_PRO2_ID", ColumnDescription = "", IsNullable = true)]
        public int PRO3_PRO2_ID { get; set; }

        /// <summary>
        /// PRO3_EVAL
        /// </summary>
        [DataNames("PRO3_EVAL")]
        [SugarColumn(ColumnName = "PRO3_EVAL", ColumnDescription = "evaluacion", IsNullable = false)]
        public int Evaluacion {
            get { return evaluacion; }
            set {
                evaluacion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PRO3_NOM
        /// </summary>
        [DataNames("PRO3_NOM")]
        [SugarColumn(ColumnName = "PRO3_NOM", ColumnDescription = "nombre del procedimiento", Length = 50, IsNullable = false)]
        public string Descripcion {
            get { return descripcion; }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PRO3_CAL
        /// </summary>
        [DataNames("PRO3_CAL")]
        [SugarColumn(ColumnName = "PRO3_CAL", ColumnDescription = "calidad", Length = 100, IsNullable = false)]
        public string Calidad {
            get { return calidad; }
            set {
                calidad = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("PRO3_FM")]
        [SugarColumn(ColumnName = "PRO3_FM", ColumnDescription = "ultima fecha de modificacion del registro", DefaultValue = "Timestamp", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
