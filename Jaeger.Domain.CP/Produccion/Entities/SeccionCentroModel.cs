﻿/// purpose:
/// develop: ANHE1 29092020 1713
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Seccion, Centro (EDP2)
    /// </summary>
    [SugarTable("EDP2", "Seccion centro")]
    public class SeccionCentroModel : BasePropertyChangeImplementation, ISeccionCentroModel {
        private int edp2id;
        private bool activo;
        private string seccion;
        private string centro;
        private int edp2edp1id;

        public SeccionCentroModel() {
            activo = true;
        }

        /// <sumary>
        /// obtener o establecer (EDP2_ID)
        /// </sumary>
        [DataNames("EDP2_ID")]
        [SugarColumn(ColumnName = "EDP2_ID", ColumnDescription = "IdSeccion", IsPrimaryKey = true, IsIdentity = true)]
        public int IdSeccion {
            get { return edp2id; }
            set {
                edp2id = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP2_A)
        /// </sumary>
        [DataNames("EDP2_A")]
        [SugarColumn(ColumnName = "EDP2_A", ColumnDescription = "Activo")]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP2_EDP1_ID)
        /// </sumary>
        [DataNames("EDP2_EDP1_ID")]
        [SugarColumn(ColumnName = "EDP2_EDP1_ID", ColumnDescription = "IdDepto")]
        public int IdDepto {
            get { return edp2edp1id; }
            set {
                edp2edp1id = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP2_SEC)
        /// </sumary>
        [DataNames("EDP2_SEC")]
        [SugarColumn(ColumnName = "EDP2_SEC", ColumnDescription = "Seccion", Length = 50)]
        public string Seccion {
            get { return seccion; }
            set {
                seccion = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP2_NOM)
        /// </sumary>
        [DataNames("EDP2_NOM")]
        [SugarColumn(ColumnName = "EDP2_NOM", ColumnDescription = "Centro", Length = 50)]
        public string Centro {
            get { return centro; }
            set {
                centro = value;
                OnPropertyChanged();
            }
        }
    }
}
