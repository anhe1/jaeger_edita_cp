﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// etapas del proceso (EDP4)
    /// </summary>
    public class ProcesoEtapaDetailModel : ProcesoEtapaModel, IProcesoEtapaModel {
        private BindingList<Edp4PueModelView> puestos;
        private BindingList<ProcesoProcedimientoModel> procedimiento;

        public ProcesoEtapaDetailModel() {
            puestos = new BindingList<Edp4PueModelView>();
            procedimiento = new BindingList<ProcesoProcedimientoModel>();
        }

        public BindingList<Edp4PueModelView> Puestos {
            get { return puestos; }
            set {
                puestos = value;
                OnPropertyChanged();
            }
        }

        public BindingList<ProcesoProcedimientoModel> Procedimiento {
            get { return procedimiento; }
            set {
                procedimiento = value;
                OnPropertyChanged();
            }
        }
    }
}
