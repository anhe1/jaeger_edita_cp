﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Equipos y Maquinaria EDP2
    /// </summary>
    [SugarTable("EDP2", "Equipos y Maquinaria")]
    public class CentroModel : BasePropertyChangeImplementation, ICentroModel {
        #region declaraciones
        private int edp2id;
        private bool activo;
        private string edp2mar;
        private string edp2mod;
        private string edp2cla;
        private string epd2numser;
        private decimal edp2ano;
        private decimal edp2val;
        private decimal epd2dia;
        private decimal epd2hor;
        private decimal edp2tm;
        private decimal edp2valmin;
        private string secuencia;
        private string nombre;
        private DateTime? _EDP2_FM;
        private int? _EDP2_USU_FM;
        private string _EDP2_CAL;
        private string _EDP2_PRO;
        private int _EDP2_EDP1_ID;
        #endregion

        public CentroModel() {
            activo = true;
        }

        /// <sumary>
        /// obtener o establecer indice del centro productivo (EDP2_ID)
        /// </sumary>
        [DataNames("EDP2_ID")]
        [SugarColumn(ColumnName = "EDP2_ID", ColumnDescription = "IdCentro", IsPrimaryKey = true, IsIdentity = true)]
        public int IdCentro {
            get { return edp2id; }
            set {
                edp2id = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer registro activo (EDP2_A)
        /// </sumary>
        [DataNames("EDP2_A")]
        [SugarColumn(ColumnName = "EDP2_A", ColumnDescription = "Activo")]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// EDP2_SEC
        /// </summary>
        [DataNames("EDP2_SEC")]
        [SugarColumn(ColumnName = "EDP2_SEC", ColumnDescription = "", Length = 50)]
        public string Secuencia {
            get { return secuencia; }
            set {
                secuencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del departamento (EDP2_SEC)
        /// </summary>
        [DataNames("EDP2_NOM")]
        [SugarColumn(ColumnName = "edp2_nom", ColumnDescription = "EDP2_NOM", Length = 50)]
        public string Nombre {
            get { return nombre; }
            set {
                nombre = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion del departamewnto (EDP2_EDP1_ID)
        /// </summary>
        [DataNames("EDP2_EDP1_ID")]
        [SugarColumn(ColumnName = "edp2_edp1_id", ColumnDescription = "EDP2_EDP1_ID")]
        public int IdDepartamento {
            get { return _EDP2_EDP1_ID; }
            set {
                _EDP2_EDP1_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer EDP2_PRO
        /// </summary>
        [DataNames("EDP2_PRO")]
        [SugarColumn(ColumnName = "edp2_pro", ColumnDescription = "EDP2_PRO", Length = 50)]
        public string EDP2_PRO {
            get { return _EDP2_PRO; }
            set {
                _EDP2_PRO = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer marca (EDP2_MAR)
        /// </sumary>
        [DataNames("EDP2_MAR")]
        [SugarColumn(ColumnName = "EDP2_MAR", ColumnDescription = "Marca", Length = 50)]
        public string Marca {
            get { return edp2mar; }
            set {
                edp2mar = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer modelo (EDP2_MOD)
        /// </sumary>
        [DataNames("EDP2_MOD")]
        [SugarColumn(ColumnName = "EDP2_MOD", ColumnDescription = "Modelo", Length = 50)]
        public string Modelo {
            get { return edp2mod; }
            set {
                edp2mod = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer clave (EDP2_CLA)
        /// </sumary>
        [DataNames("EDP2_CLA")]
        [SugarColumn(ColumnName = "EDP2_CLA", ColumnDescription = "Clave", Length = 50)]
        public string Clave {
            get { return edp2cla; }
            set {
                edp2cla = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer numero de serie (EDP2_NUM_SER)
        /// </sumary>
        [DataNames("EDP2_NUM_SER")]
        [SugarColumn(ColumnName = "EDP2_NUM_SER", ColumnDescription = "NoSerie", Length = 50)]
        public string NoSerie {
            get { return epd2numser; }
            set {
                epd2numser = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (EDP2_CAL)
        /// </summary>
        [DataNames("EDP2_CAL")]
        [SugarColumn(ColumnName = "edp2_cal", ColumnDescription = "EDP2_CAL", Length = 50)]
        public string EDP2_CAL {
            get { return _EDP2_CAL; }
            set {
                _EDP2_CAL = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer años de amortizacion (EDP2_ANO)
        /// </sumary>
        [DataNames("EDP2_ANO")]
        [SugarColumn(ColumnName = "EDP2_ANO", ColumnDescription = "AnioAmortizacion", Length = 18, DecimalDigits = 4)]
        public decimal AnioAmortizacion {
            get { return edp2ano; }
            set {
                edp2ano = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer el valor de la adquisicion (EDP2_VAL)
        /// </sumary>
        [DataNames("EDP2_VAL")]
        [SugarColumn(ColumnName = "EDP2_VAL", ColumnDescription = "Valor", Length = 18, DecimalDigits = 4)]
        public decimal Valor {
            get { return edp2val; }
            set {
                edp2val = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer dias por año (EDP2_DIA)
        /// </sumary>
        [DataNames("EDP2_DIA")]
        [SugarColumn(ColumnName = "EDP2_DIA", ColumnDescription = "DiasXAnio", Length = 18, DecimalDigits = 4)]
        public decimal DiasXAnio {
            get { return epd2dia; }
            set {
                epd2dia = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer horas por dia (EDP2_HOR)
        /// </sumary>
        [DataNames("EDP2_HOR")]
        [SugarColumn(ColumnName = "EDP2_HOR", ColumnDescription = "HoraXDia", Length = 18, DecimalDigits = 4)]
        public decimal HoraXDia {
            get { return epd2hor; }
            set {
                epd2hor = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer % de uso (EDP2_TM)
        /// </sumary>
        [DataNames("EDP2_TM")]
        [SugarColumn(ColumnName = "EDP2_TM", ColumnDescription = "Uso", Length = 18, DecimalDigits = 4)]
        public decimal Uso {
            get { return edp2tm; }
            set {
                edp2tm = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer valor por minuto (EDP2_VAL_MIN)
        /// </sumary>
        [DataNames("EDP2_VAL_MIN")]
        [SugarColumn(ColumnName = "EDP2_VAL_MIN", ColumnDescription = "ValorXMinuto", Length = 18, DecimalDigits = 4)]
        public decimal ValorXMinuto {
            get { return edp2valmin; }
            set {
                edp2valmin = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer EDP2_USU_FM
        /// </summary>
        [DataNames("EDP2_USU_FM")]
        [SugarColumn(ColumnName = "edp2_usu_fm", ColumnDescription = "EDP2_USU_FM")]
        public int? EDP2_USU_FM {
            get { return _EDP2_USU_FM; }
            set {
                _EDP2_USU_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro EDP2_FM
        /// </summary>
        [DataNames("EDP2_FM")]
        [SugarColumn(ColumnName = "edp2_fm", ColumnDescription = "EDP2_FM")]
        public DateTime? FechaModifica {
            get {
                if (_EDP2_FM >= new DateTime(1900, 1, 1))
                    return _EDP2_FM;
                return null;
            }
            set {
                _EDP2_FM = value;
                OnPropertyChanged();
            }
        }
    }
}
