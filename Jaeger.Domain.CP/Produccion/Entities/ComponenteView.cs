﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// vista de componente o ensamble de la orden de produccion, contiene partidas de la linea de produccion (PedCom)
    /// </summary>
    public class ComponenteView : ComponenteModel, IPedComModel {
        private BindingList<LineaProduccionDeptoCentroView> lineaProduccion;

        /// <summary>
        /// constructor
        /// </summary>
        public ComponenteView() {

        }

        /// <summary>
        /// Linea de Produccion (PedPro)
        /// </summary>
        public BindingList<LineaProduccionDeptoCentroView> LineaProduccion {
            get { return this.lineaProduccion; }
            set {
                this.lineaProduccion = value;
                this.OnPropertyChanged();
            }
        }
    }
}
