﻿using Jaeger.Domain.Base.Services;
using System.Reflection;
/// purpose:
/// develop: ANHE1 29092020 1713
namespace Jaeger.Domain.CP.Produccion.Entities {
    public class OrdenProduccionEtiquetaPrinter : OrdenProduccion {
        public OrdenProduccionEtiquetaPrinter() {
        }

        public OrdenProduccionEtiquetaPrinter(OrdenProduccion model, string usuario) {
            MapperClassExtensions.MatchAndMap<OrdenProduccion, OrdenProduccionEtiquetaPrinter>(model, this);
            this.Usuario = usuario;
        }

        public OrdenProduccionEtiquetaPrinter(OrdenProduccionEtiquetaPrinter model) {
            MapperClassExtensions.MatchAndMap<OrdenProduccionEtiquetaPrinter, OrdenProduccionEtiquetaPrinter>(model, this);
        }

        public decimal CantidadPorPaquete { get; set; }

        public string Usuario { get; set; }

        public int Duplicado { get; set; }

        public string[] QrText {
            get {
                return new string[] { "||ORDEN=", this.IdPedPrd.ToString("#000000"), "|FECHA=", this.FechaPedido.Value.ToString("dd-mm-yyyy"), "|Cliente=", this.Cliente, "|Vendedor=", this.ClaveVendedor.ToString(), "|Articulos=", "||" };
            }
        }
    }
}
