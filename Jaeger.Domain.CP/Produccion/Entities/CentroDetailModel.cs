﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Equipos y Maquinaria
    /// </summary>
    public class CentroDetailModel : CentroModel, ICentroModel {
        private string _Departamento;
        public CentroDetailModel() {

        }

        [DataNames("EDP1_nom")]
        public string Departamento {
            get { return this._Departamento; }
            set { this._Departamento = value;
                this.OnPropertyChanged();
            }
        }

        public string Descriptor {
            get { return string.Format("{0} - {1} - {2}", this.Departamento, this.Secuencia, this.Nombre); }
        }
    }
}
