﻿using System;

/// purpose:
/// develop: ANHE1 29092020 1713
namespace Jaeger.Domain.CP.Produccion.Entities {
    public class MembretePrinter {
        public MembretePrinter() {
            this.Fecha = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice o numero de orden de produccion
        /// </summary>
        public int IdPedPrd { get; set; }

        public string Titulo { get; set; }

        public string Cliente { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion de la orden de produccion del cliente
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// obtenero establecer el departamento
        /// </summary>
        public string Departamento { get; set; }

        public string Componente { get; set; }

        public string Proceso { get; set; }

        public string Centro { get; set; }

        public DateTime Fecha { get; set; }

        public decimal CantidadTotal { get; set; }

        public decimal Cantidad { get; set; }

        public string Usuario { get; set; }

        public string Unidad { get; set; }

        public int Formato { get; set; }

        public string Linea {
            get { return string.Format("{0} / {1} / {2}", this.Departamento, this.Centro, this.Proceso); }
        }
    }
}
