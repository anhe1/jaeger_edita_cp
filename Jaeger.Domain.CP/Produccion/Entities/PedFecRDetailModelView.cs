﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// cambio de status (PedFecR)
    /// </summary>
    public class PedFecRDetailModelView : PedFecRDetailModel, IPedFecRModel {
        private string status;
        private int secStatus;

        /// <summary>
        /// constructor
        /// </summary>
        public PedFecRDetailModelView() {

        }

        /// <summary>
        /// obtener o establecer el status de la orden de produccion
        /// </summary>
        [DataNames("PedST_Sta")]
        public string Status {
            get { return this.status; }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status numerico
        /// </summary>
        [DataNames("PedST_Sec")]
        public int St {
            get { return this.secStatus; }
            set {
                this.secStatus = value;
                this.OnPropertyChanged();
            }
        }
    }
}
