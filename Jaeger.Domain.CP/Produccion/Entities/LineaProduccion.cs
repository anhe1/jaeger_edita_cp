﻿using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Services;

/// purpose:
/// develop: ANHE1 29092020 1713
namespace Jaeger.Domain.CP.Produccion.Entities {
    public class LineaProduccion : LineaProduccionModel {
        private Departamento _Departamento = new Departamento();
        private string _Proceso;

        public LineaProduccion() : base() {
        }

        public LineaProduccion(LineaProduccionModel model) : base() {
            MapperClassExtensions.MatchAndMap<LineaProduccionModel, LineaProduccion>(model, this);
        }

        #region departamento
        [DataNames("EDP1_ID")]
        public int IdDepartamento {
            get { return this._Departamento.IdDepto; }
            set {
                this._Departamento.IdDepto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("EDP1_NOM")]
        public string Departamento {
            get { return this._Departamento.Nombre; }
            set {
                this._Departamento.Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("EDP2_SEC")]
        public string Seccion {
            get; set;
        }

        [DataNames("EDP2_NOM")]
        public string Centro {
            get; set;
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del proceso
        /// </summary>
        [DataNames("EDP3_nom")]
        public string Proceso {
            get { return this._Proceso; }
            set { this._Proceso = value;
                this.OnPropertyChanged();
            }
        }

        public Departamento GetDepartamento {
            get { return this._Departamento; }
            set {
                if (value != null) {
                    this._Departamento = value;
                    this.IdDepartamento = value.IdDepto;
                    this.Departamento = value.Nombre;
                }
            }
        }
        #endregion

        public string LineaP {
            get { return string.Format("{0} / {1} / {2}", this.Departamento, this.Centro, this.Proceso); }
        }
    }
}
