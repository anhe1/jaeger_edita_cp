﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Línea de Producción (PedPro)
    /// </summary>
    [SugarTable("pedpro", "linea de produccion")]
    public class ComponenteLineaProduccion : LineaProduccionModel {
        public ComponenteLineaProduccion() : base() { }

        #region departamento
        [DataNames("EDP1_nom")]
        public string Departamento { get; set; }

        [DataNames("EDP2_sec")]
        public string Seccion { get; set; }

        [DataNames("EDP2_nom")]
        public string Centro { get; set; }

        [DataNames("EDP3_nom")]
        public string Proceso { get; set; }

        [DataNames("EDP2_val_min")]
        public decimal ValorMinuto { get; set; }
        #endregion

        #region componente / ensamble
        /// <summary>
        /// obtener o establecer clave (PedCom_Cla)
        /// </summary>
        [DataNames("PEDCOM_CLA")]
        public string ComponenteClave { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del componente (PEDCOM_NOM)
        /// </summary>
        [DataNames("PEDCOM_NOM")]
        public string Componente { get; set; }

        /// <summary>
        /// obtener o establecer secuencia del componente (PEDCOM_SEC)
        /// </summary>
        [DataNames("PEDCOM_SEC")]
        public int ComponenteSecuencia { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad del componente (PedCom_Can_Com)
        /// </summary>
        [DataNames("PEDCOM_CAN_COM")]
        public int ComponenteCantidad { get; set; }

        /// <summary>
        /// obtener o establecer el tiempo estimado en minutos (PedCom_T_Max)
        /// </summary>
        [DataNames("PedCom_T_Max")]
        public int ComponenteTiempoEstimado { get; set; }

        /// <summary>
        /// obtener o establecer tiempo acumulado real (PEDCOM_MINR)
        /// </summary>
        [DataNames("PEDCOM_MINR")]
        public int ComponenteTiempoReal { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_MIN)
        /// </summary>
        [DataNames("PEDCOM_FEC_MIN")]
        [SugarColumn(ColumnName = "pedcom_fec_min", ColumnDescription = "PEDCOM_FEC_MIN")]
        public DateTime? ComponenteFechaMinima { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_MAX)
        /// </summary>
        [DataNames("PEDCOM_FEC_MAX")]
        [SugarColumn(ColumnName = "pedcom_fec_max", ColumnDescription = "PEDCOM_FEC_MAX")]
        public DateTime? ComponenteFechaMaxima { get; set; }

        /// <summary>
        /// obtener o establecer PEDCOM_FEC_INI
        /// </summary>
        [DataNames("PEDCOM_FEC_INI")]
        [SugarColumn(ColumnName = "pedcom_fec_ini", ColumnDescription = "PEDCOM_FEC_INI")]
        public DateTime? ComponenteFechaInicial { get; set; }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_FIN)
        /// </summary>
        [DataNames("PEDCOM_FEC_FIN")]
        [SugarColumn(ColumnName = "pedcom_fec_fin", ColumnDescription = "PEDCOM_FEC_FIN")]
        public DateTime? ComponenteFechaFinal { get; set; }
        #endregion

        public ComponenteModel PedComModel {
            get {
                return new ComponenteModel { 
                    Activo = this.Activo,
                    Nombre = this.Componente,
                    TiempoEstimado = this.ComponenteTiempoEstimado,
                    TiempoReal = this.ComponenteTiempoReal,
                };
            }
        }
    }
}
