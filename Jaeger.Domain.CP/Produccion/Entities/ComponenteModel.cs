﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Componentes/Ensambles de orden de produccion (PedCom)
    /// </summary>
    public class ComponenteModel : BasePropertyChangeImplementation, IPedComModel {
        #region declaraciones
        private int _IdComponente;
        private int _Activo;
        private int _PEDCOM_COTCOM_ID;
        private int _PEDCOM_PEDPRD_ID;
        private string _PEDCOM_CLA;
        private string _PEDCOM_NOM;
        private int _PEDCOM_NUM;
        private int _PEDCOM_CAN_COM;
        private int _PEDCOM_SEC;
        private decimal _PEDCOM_T_MAX;
        private int _PEDCOM_MINR;
        private DateTime? _PEDCOM_FEC_MIN;
        private DateTime? _PEDCOM_FEC_MAX;
        private DateTime? _PEDCOM_FEC_INI;
        private DateTime? _PEDCOM_FEC_FIN;
        private int _PEDCOM_USU_FM;
        private string modifica;
        private DateTime? fechaModifica;
        #endregion

        public ComponenteModel() {
            this._Activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice del Componentes o Ensambles (PEDCOM_ID)
        /// </summary>
        [DataNames("PEDCOM_ID")]
        [SugarColumn(ColumnName = "PEDCOM_ID", ColumnDescription = "PEDCOM_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdComponente {
            get { return this._IdComponente; }
            set {
                this._IdComponente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (pedcom_a)
        /// </summary>
        [DataNames("PEDCOM_A")]
        [SugarColumn(ColumnName = "PEDCOM_A", ColumnDescription = "PEDCOM_A")]
        public int Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDCOM_COTCOM_ID)
        /// </summary>
        [DataNames("PEDCOM_COTCOM_ID")]
        [SugarColumn(ColumnName = "PEDCOM_COTCOM_ID", ColumnDescription = "PEDCOM_COTCOM_ID")]
        public int PEDCOM_COTCOM_ID {
            get { return this._PEDCOM_COTCOM_ID; }
            set {
                this._PEDCOM_COTCOM_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer relacion con la orden de produccion (PedCom_PedPrd_ID)
        /// </summary>
        [DataNames("PEDCOM_PEDPRD_ID")]
        [SugarColumn(ColumnName = "PEDCOM_PEDPRD_ID", ColumnDescription = "PEDCOM_PEDPRD_ID")]
        public int IdPedPrd {
            get { return this._PEDCOM_PEDPRD_ID; }
            set {
                this._PEDCOM_PEDPRD_ID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDCOM_NUM no se esta utilizando
        /// </summary>
        [DataNames("PEDCOM_NUM")]
        [SugarColumn(ColumnName = "PEDCOM_NUM", ColumnDescription = "PEDCOM_NUM")]
        public int PEDCOM_NUM {
            get { return this._PEDCOM_NUM; }
            set {
                this._PEDCOM_NUM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer secuencia del componente (PEDCOM_SEC)
        /// </summary>
        [DataNames("PEDCOM_SEC")]
        [SugarColumn(ColumnName = "PEDCOM_SEC", ColumnDescription = "PEDCOM_SEC")]
        public int Secuencia {
            get { return this._PEDCOM_SEC; }
            set {
                this._PEDCOM_SEC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad del componente (PedCom_Can_Com)
        /// </summary>
        [DataNames("PEDCOM_CAN_COM")]
        [SugarColumn(ColumnName = "PEDCOM_CAN_COM", ColumnDescription = "PEDCOM_CAN_COM")]
        public int Cantidad {
            get { return this._PEDCOM_CAN_COM; }
            set {
                this._PEDCOM_CAN_COM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tiempo estimado en minutos (PedCom_T_Max)
        /// </summary>
        [DataNames("PEDCOM_T_MAX")]
        [SugarColumn(ColumnName = "PEDCOM_T_MAX", ColumnDescription = "PEDCOM_T_MAX", Length = 18, DecimalDigits = 4)]
        public decimal TiempoEstimado {
            get { return this._PEDCOM_T_MAX; }
            set {
                this._PEDCOM_T_MAX = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tiempo acumulado real (PEDCOM_MINR)
        /// </summary>
        [DataNames("PEDCOM_MINR")]
        [SugarColumn(ColumnName = "PEDCOM_MINR", ColumnDescription = "PEDCOM_MINR")]
        public int TiempoReal {
            get { return this._PEDCOM_MINR; }
            set {
                this._PEDCOM_MINR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDCOM_USU_FM)
        /// </summary>
        [DataNames("PEDCOM_USU_FM")]
        [SugarColumn(ColumnName = "PEDCOM_USU_FM", ColumnDescription = "PEDCOM_USU_FM")]
        public int PEDCOM_USU_FM {
            get { return this._PEDCOM_USU_FM; }
            set {
                this._PEDCOM_USU_FM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave (PedCom_Cla)
        /// </summary>
        [DataNames("PEDCOM_CLA")]
        [SugarColumn(ColumnName = "PEDCOM_CLA", ColumnDescription = "PEDCOM_CLA", Length = 50)]
        public string Clave {
            get { return this._PEDCOM_CLA; }
            set {
                this._PEDCOM_CLA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro (PEDCOM_USU_M)
        /// </summary>
        [DataNames("PEDCOM_USU_M")]
        [SugarColumn(ColumnName = "PEDCOM_USU_M", ColumnDescription = "PEDCOM_USU_M", Length = 7)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del componente (PEDCOM_NOM)
        /// </summary>
        [DataNames("PEDCOM_NOM")]
        [SugarColumn(ColumnName = "PEDCOM_NOM", ColumnDescription = "PEDCOM_NOM", Length = 50)]
        public string Nombre {
            get { return this._PEDCOM_NOM; }
            set {
                this._PEDCOM_NOM = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_MIN)
        /// </summary>
        [DataNames("PEDCOM_FEC_MIN")]
        [SugarColumn(ColumnName = "PEDCOM_FEC_MIN", ColumnDescription = "PEDCOM_FEC_MIN")]
        public DateTime? FechaMinima {
            get {
                if (this._PEDCOM_FEC_MIN >= new DateTime(1900, 1, 1))
                    return _PEDCOM_FEC_MIN;
                return null;
            }
            set {
                this._PEDCOM_FEC_MIN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_MAX)
        /// </summary>
        [DataNames("PEDCOM_FEC_MAX")]
        [SugarColumn(ColumnName = "PEDCOM_FEC_MAX", ColumnDescription = "PEDCOM_FEC_MAX")]
        public DateTime? FechaMaxima {
            get {
                if (this._PEDCOM_FEC_MAX >= new DateTime(1900, 1, 1))
                    return this._PEDCOM_FEC_MAX;
                return null;
            }
            set {
                this._PEDCOM_FEC_MAX = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PEDCOM_FEC_INI
        /// </summary>
        [DataNames("PEDCOM_FEC_INI")]
        [SugarColumn(ColumnName = "PEDCOM_FEC_INI", ColumnDescription = "PEDCOM_FEC_INI")]
        public DateTime? FechaInicial {
            get {
                if (this._PEDCOM_FEC_INI >= new DateTime(1900, 1, 1))
                    return this._PEDCOM_FEC_INI;
                return null;
            }
            set {
                this._PEDCOM_FEC_INI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (PEDCOM_FEC_FIN)
        /// </summary>
        [DataNames("PEDCOM_FEC_FIN")]
        [SugarColumn(ColumnName = "PEDCOM_FEC_FIN", ColumnDescription = "PEDCOM_FEC_FIN")]
        public DateTime? FechaFinal {
            get {
                if (this._PEDCOM_FEC_FIN >= new DateTime(1900, 1, 1))
                    return this._PEDCOM_FEC_FIN;
                return null;
            }
            set {
                this._PEDCOM_FEC_FIN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro (PEDCOM_FM)
        /// </summary>
        [DataNames("PEDCOM_FM")]
        [SugarColumn(ColumnName = "PEDCOM_FM", ColumnDescription = "PEDCOM_FM")]
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica >= new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
