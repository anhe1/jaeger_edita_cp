﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// clase area y departamento
    /// </summary>
    [SugarTable("EDP1", "Clase, Area y Departamento")]
    public class DepartamentoDetailModel : DepartamentoModel, IDepartamentoModel {
        private BindingList<SeccionCentroDetailModel> seccionCentroDetailModels;
        public DepartamentoDetailModel() {
            seccionCentroDetailModels = new BindingList<SeccionCentroDetailModel>();
        }

        public BindingList<SeccionCentroDetailModel> Centros {
            get { return seccionCentroDetailModels; }
            set {
                seccionCentroDetailModels = value;
                OnPropertyChanged();
            }
        }
    }
}
