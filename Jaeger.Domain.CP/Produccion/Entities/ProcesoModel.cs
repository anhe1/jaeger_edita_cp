﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// Proceso (EDP3)
    /// </summary>
    [SugarTable("EDP3", "Proceso")]
    public class ProcesoModel : BasePropertyChangeImplementation, IProcesoModel {
        private int edp3id;
        private int edp3a;
        private int edp3edp2id;
        private string edp3nom;
        private int edp3sec;
        private int edp3nt;
        private decimal edp3mo;
        private decimal edp3cen;
        private int edp3usufm;
        private DateTime edp3fm;
        private int edp3canini;
        private decimal edp3canext;

        public ProcesoModel() {
            edp3a = 1;
        }

        /// <sumary>
        /// obtener o establecer indice de la tabla(EDP3_ID)
        /// </sumary>
        [DataNames("EDP3_ID")]
        [SugarColumn(ColumnName = "EDP3_ID", ColumnDescription = "IdProceso", IsPrimaryKey = true, IsIdentity = true)]
        public int IdProceso {
            get { return edp3id; }
            set {
                edp3id = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_A)
        /// </sumary>
        [DataNames("EDP3_A")]
        [SugarColumn(ColumnName = "EDP3_A", ColumnDescription = "Activo")]
        public int Activo {
            get { return edp3a; }
            set {
                edp3a = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_NT)
        /// </sumary>
        [DataNames("EDP3_NT")]
        [SugarColumn(ColumnName = "EDP3_NT", ColumnDescription = "Edp3Nt")]
        public int Edp3Nt {
            get { return edp3nt; }
            set {
                edp3nt = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_EDP2_ID)
        /// </sumary>
        [DataNames("EDP3_EDP2_ID")]
        [SugarColumn(ColumnName = "EDP3_EDP2_ID", ColumnDescription = "IdCentro")]
        public int IdCentro {
            get { return edp3edp2id; }
            set {
                edp3edp2id = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_SEC)
        /// </sumary>
        [DataNames("EDP3_SEC")]
        [SugarColumn(ColumnName = "EDP3_SEC", ColumnDescription = "Seccion")]
        public int Secuencia {
            get { return edp3sec; }
            set {
                edp3sec = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_USU_FM)
        /// </sumary>
        [DataNames("EDP3_USU_FM")]
        [SugarColumn(ColumnName = "EDP3_USU_FM", ColumnDescription = "Edp3UsuFm")]
        public int Edp3UsuFm {
            get { return edp3usufm; }
            set {
                edp3usufm = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_CANINI)
        /// </sumary>
        [DataNames("EDP3_CANINI")]
        [SugarColumn(ColumnName = "EDP3_CANINI", ColumnDescription = "CantidadInicial")]
        public int CantidadInicial {
            get { return edp3canini; }
            set {
                edp3canini = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_CEN)
        /// </sumary>
        [DataNames("EDP3_CEN")]
        [SugarColumn(ColumnName = "EDP3_CEN", ColumnDescription = "ValorHora", Length = 18, DecimalDigits = 4)]
        public decimal ValorHora {
            get { return edp3cen; }
            set {
                edp3cen = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_CANEXT)
        /// </sumary>
        [DataNames("EDP3_CANEXT")]
        [SugarColumn(ColumnName = "EDP3_CANEXT", ColumnDescription = "CantidadExt", Length = 18, DecimalDigits = 4)]
        public decimal CantidadExt {
            get { return edp3canext; }
            set {
                edp3canext = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_MO)
        /// </sumary>
        [DataNames("EDP3_MO")]
        [SugarColumn(ColumnName = "EDP3_MO", ColumnDescription = "ManoObra", Length = 18, DecimalDigits = 4)]
        public decimal ManoObra {
            get { return edp3mo; }
            set {
                edp3mo = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_NOM)
        /// </sumary>
        [DataNames("EDP3_NOM")]
        [SugarColumn(ColumnName = "EDP3_NOM", ColumnDescription = "Nombre", Length = 255)]
        public string Nombre {
            get { return edp3nom; }
            set {
                edp3nom = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP3_FM)
        /// </sumary>
        [DataNames("EDP3_FM")]
        [SugarColumn(ColumnName = "EDP3_FM", ColumnDescription = "Edp3Fm")]
        public DateTime FechaModifica {
            get { return edp3fm; }
            set {
                edp3fm = value;
                OnPropertyChanged();
            }
        }
    }
}
