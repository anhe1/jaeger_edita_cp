﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using System.Globalization;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// vista de producto vendido (PedPrdModel)
    /// </summary>
    public class PedPrdPedidoModelView : OrdenProduccionModel, IOrdenProduccionModel, IOrdenProduccionC {

        #region informacion de la orden de produccion
        /// <summary>
        /// obtener o establcer el status
        /// </summary>
        [DataNames("PedST_Sta")]
        public string Status { get; set; }

        /// <summary>
        /// obtener o establecer el status de forma númerica
        /// </summary>
        [DataNames("PedST_Sec")]
        public int St { get; set; }

        [DataNames("DIR_NOM")]
        public string Persona { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del contacto
        /// </summary>
        [DataNames("DIRCON_con")]
        public string Contacto { get; set; }

        [DataNames("Ven_Cla")]
        public string ClaveVendedor { get; set; }
        #endregion

        #region informacion de la cotizacion
        /// <summary>
        /// obtener o establecer nombre del cliente de la cotizacion
        /// </summary>
        [DataNames("COTPRDS_CLI")]
        public string CotizacionCliente { get; set; }

        [DataNames("COTPRDS_LPTEX")]
        public string CotizacionProducto { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del modelo
        /// </summary>
        [DataNames("COTPRDS_NOM")]
        public string CotizacionModelo { get; set; }

        [DataNames("COTPRDS_$_ST2")]
        public decimal CotizacionSubTotal { get; set; }

        [DataNames("CotPrds_Can")]
        public decimal CotizacionCantidad { get; set; }

        [DataNames("CotPrds_Uni")]
        public decimal CotizacionUnidad { get; set; }

        [DataNames("CotPrds_$Uni")]
        public decimal CotizacionUnitario { get; set; }

        [DataNames("CotPrds_$")]
        public decimal CotizacionTotal { get; set; }

        [DataNames("CotPrds_Uti")]
        public decimal CotizacionUtilidad { get; set; }

        [DataNames("COTPRD_$_IND")]
        public decimal CostoIndirecto {
            get; set;
        }

        [DataNames("COTPRD_$_ADM")]
        public decimal CostoAdmon {
            get; set;
        }

        [DataNames("COTPRD_$_FIN")]
        public decimal CostoFinanciero {
            get; set;
        }

        [DataNames("COTPRD_$_VEN")]
        public decimal CostoVenta {
            get; set;
        }

        [DataNames("COTPRD_$_ST1")]
        public decimal SubTotal1 {
            get; set;
        }

        [DataNames("COTPRD_$_CEN")]
        public decimal CostoCentro {
            get; set;
        }

        [DataNames("COTPRD_$_PRO")]
        public decimal CostoMO {
            get; set;
        }

        [DataNames("COTPRD_$_MP")]
        public decimal CostoMP {
            get; set;
        }

        [DataNames("COTPRD_$_CNS")]
        public decimal CostoConsumibles {
            get; set;
        }
        #endregion

        public string Verificado {
            get { if (this.IdVerificado == 0)
                    return "No Verificado";
                return "Verificado";
            }
        }

        public int Semana {
            get {
                // formatea la fecha de acuerdo a la zona del computador
                var cul = CultureInfo.CurrentCulture;
                // Usa la fecha formateada y calcula el número de la semana
                return cul.Calendar.GetWeekOfYear(
                     this.FechaPedido.Value,
                     CalendarWeekRule.FirstDay,
                     DayOfWeek.Monday);
            }
        }
    }
}
