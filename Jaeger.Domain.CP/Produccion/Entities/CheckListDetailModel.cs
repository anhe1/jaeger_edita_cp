﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.CP.Produccion.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// checklist detallado que incluye listado de posibles fallas
    /// </summary>
    public class CheckListDetailModel : CheckListModel, ICheckListModel {
        private BindingList<ChekListFallaModel> posibleFalla;
        public CheckListDetailModel() {
            posibleFalla = new BindingList<ChekListFallaModel>();
        }

        /// <summary>
        /// posibles fallas
        /// </summary>
        public BindingList<ChekListFallaModel> PosibleFalla {
            get { return posibleFalla; }
            set {
                posibleFalla = value;
                OnPropertyChanged();
            }
        }
    }
}
