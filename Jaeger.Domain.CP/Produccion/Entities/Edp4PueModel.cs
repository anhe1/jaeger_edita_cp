﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    ///<summary>
    /// SubProcesos - Puestos (EDP4PUE)
    ///</summary>
    [SugarTable("edp4pue")]
    public class Edp4PueModel : BasePropertyChangeImplementation, IEdp4PueModel {
        private int _EDP4PUE_ID;
        private int _EDP4PUE_ID3;
        private int _EDP4PUE_A;
        private int _EDP4PUE_EDP4_ID;
        private int _EDP4PUE_PUE_ID4;
        private int _EDP4PUE_PUE_ID;
        private int _EDP4PUE_USU_FM;
        private DateTime? _EDP4PUE_FM;

        public Edp4PueModel() {
            Activo = 1;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (EDP4PUE_ID)
        /// </summary>
        [DataNames("EDP4PUE_ID")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "edp4pue_id", ColumnDescription = "")]
        public int IdEp4Pue {
            get { return _EDP4PUE_ID; }
            set {
                _EDP4PUE_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (EDP4PUE_A)
        /// </summary>
        [DataNames("EDP4PUE_A")]
        [SugarColumn(ColumnName = "edp4pue_a", ColumnDescription = "")]
        public int Activo {
            get { return _EDP4PUE_A; }
            set {
                _EDP4PUE_A = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (EDP4PUE_ID3)
        /// </summary>
        [DataNames("EDP4PUE_ID3")]
        [SugarColumn(ColumnName = "edp4pue_id3", ColumnDescription = "")]
        public int EDP4PUE_ID3 {
            get { return _EDP4PUE_ID3; }
            set {
                _EDP4PUE_ID3 = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (EDP4PUE_EDP4_ID)
        /// </summary>
        [DataNames("EDP4PUE_EDP4_ID")]
        [SugarColumn(ColumnName = "edp4pue_edp4_id", ColumnDescription = "")]
        public int EDP4PUE_EDP4_ID {
            get { return _EDP4PUE_EDP4_ID; }
            set {
                _EDP4PUE_EDP4_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (EDP4PUE_PUE_ID4)
        /// </summary>
        [DataNames("EDP4PUE_PUE_ID4")]
        [SugarColumn(ColumnName = "edp4pue_pue_id4", ColumnDescription = "")]
        public int EDP4PUE_PUE_ID4 {
            get { return _EDP4PUE_PUE_ID4; }
            set {
                _EDP4PUE_PUE_ID4 = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (EDP4PUE_PUE_ID)
        /// </summary>
        [DataNames("EDP4PUE_PUE_ID")]
        [SugarColumn(ColumnName = "edp4pue_pue_id", ColumnDescription = "")]
        public int EDP4PUE_PUE_ID {
            get { return _EDP4PUE_PUE_ID; }
            set {
                _EDP4PUE_PUE_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (EDP4PUE_USU_FM)
        /// </summary>
        [DataNames("EDP4PUE_USU_FM")]
        [SugarColumn(ColumnName = "edp4pue_usu_fm", ColumnDescription = "")]
        public int EDP4PUE_USU_FM {
            get { return _EDP4PUE_USU_FM; }
            set {
                _EDP4PUE_USU_FM = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (EDP4PUE_FM)
        /// </summary>
        [DataNames("EDP4PUE_FM")]
        [SugarColumn(ColumnName = "edp4pue_fm", ColumnDescription = "")]
        public DateTime? EDP4PUE_FM {
            get {
                if (_EDP4PUE_FM >= new DateTime(1900, 1, 1))
                    return _EDP4PUE_FM;
                return null;
            }
            set {
                _EDP4PUE_FM = value;
                OnPropertyChanged();
            }
        }
    }
}
