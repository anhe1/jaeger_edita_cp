﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    public class OrdenProduccionCostosModel {
        public OrdenProduccionCostosModel() { }

        [DataNames("PEDPRD_ID")]
        public int IdOrden { get; set; }

        [DataNames("PEDPRD_COTPRD_ID")]
        public int IdCotizacion { get; set; }

        [DataNames("DIR_NOM")]
        public string Cliente { get; set; }

        [DataNames("PEDPRD_NOM")]
        public string Descipcion { get; set; }

        [DataNames("CEN")]
        public decimal TotalCentro { get; set; }

        [DataNames("MO")]
        public decimal TotalManoObra { get; set; }

        [DataNames("MP")]
        public decimal TotalMateriaPrima { get; set; }

        [DataNames("CNS")]
        public decimal TotalConsumibles { get; set; }
    }
}
