﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// lista de verificacion del proceso (2357)
    /// </summary>
    [SugarTable("CHKLST", "lista de verificacion del proceso")]
    public class CheckListModel : BasePropertyChangeImplementation, ICheckListModel {
        private int idCheck;
        private bool activo;
        private int idProceso;
        private int secuencia;
        private string descripcion;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;

        public CheckListModel() {
            activo = true;
            fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indce de la tabla (CHKLST_ID)
        /// </summary>
        [DataNames("CHKLST_ID")]
        [SugarColumn(ColumnName = "CHKLST_ID", ColumnDescription = "indice de la tabla", IsNullable = false, IsIdentity = true, IsPrimaryKey = true)]
        public int IdCheckList {
            get { return idCheck; }
            set {
                idCheck = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (CHKLST_A)
        /// </summary>
        [DataNames("CHKLST_A")]
        [SugarColumn(ColumnName = "CHKLST_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el proceso (CHKLST_PROC_ID)
        /// </summary>
        [DataNames("CHKLST_PROC_ID")]
        [SugarColumn(ColumnName = "CHKLST_PROC_ID", ColumnDescription = "indice de relacion con el proceso", IsNullable = false)]
        public int IdProceso {
            get { return idProceso; }
            set {
                idProceso = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia a presentar (CHKLST_SEC)
        /// </summary>
        [DataNames("CHKLST_SEC")]
        [SugarColumn(ColumnName = "CHKLST_SEC", ColumnDescription = "secuencia", IsNullable = false)]
        public int Secuencia {
            get { return secuencia; }
            set {
                secuencia = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion (CHKLST_DESC)
        /// </summary>
        [DataNames("CHKLST_DESC")]
        [SugarColumn(ColumnName = "CHKLST_DESC", ColumnDescription = "descripcion", IsNullable = false)]
        public string Descripcion {
            get { return descripcion; }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro (CHKLST_USU_N)
        /// </summary>
        [DataNames("CHKLST_USU_N")]
        [SugarColumn(ColumnName = "CHKLST_USU_N", ColumnDescription = "clave del usuario que crea el registro", Length = 10, IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return creo; }
            set {
                creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro (CHKLST_USU_M)
        /// </summary>
        [DataNames("CHKLST_USU_M")]
        [SugarColumn(ColumnName = "CHKLST_USU_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return modifica; }
            set {
                modifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del nuevo registro (CHKLST_FM)
        /// </summary>
        [DataNames("CHKLST_FN")]
        [SugarColumn(ColumnName = "CHKLST_FN", ColumnDescription = "descripcion", IsNullable = false, IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return fechaNuevo; }
            set {
                fechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro (CHKLST_FM)
        /// </summary>
        [DataNames("CHKLST_FM")]
        [SugarColumn(ColumnName = "CHKLST_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
