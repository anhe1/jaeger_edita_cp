﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// PUE
    /// </summary>
    [SugarTable("PUE", "Depatamentos y Puestos")]
    public class PuestoModel : BasePropertyChangeImplementation, IPuestoModel {
        private int idPuesto;
        private bool activo;
        private int idDepartamento;
        private string descripcion;
        private decimal salario;
        private decimal premioProduccionFijo;
        private decimal premioProduccionFactor;
        private DateTime? fechaModifica;
        private int _PUE_PUE_ID;

        public PuestoModel() {
            fechaModifica = DateTime.Now;
            activo = true;
        }

        /// <summary>
        /// obtener o establecer el indce de la tabla
        /// </summary>
        [DataNames("PUE_ID")]
        [SugarColumn(ColumnName = "PUE_ID", ColumnDescription = "indice de la tabla", IsNullable = false, IsIdentity = true, IsPrimaryKey = true)]
        public int IdPuesto {
            get { return idPuesto; }
            set {
                idPuesto = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("PUE_A")]
        [SugarColumn(ColumnName = "PUE_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PUE_EDP1_ID
        /// </summary>
        [DataNames("PUE_EDP1_ID")]
        [SugarColumn(ColumnName = "PUE_EDP1_ID", ColumnDescription = "indice de relacion con el centro", DefaultValue = "0", IsNullable = false)]
        public int IdDepartamento {
            get { return idDepartamento; }
            set {
                idDepartamento = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PUE_EDP1_ID
        /// </summary>
        [DataNames("PUE_PUE_ID")]
        [SugarColumn(ColumnName = "PUE_PUE_ID", ColumnDescription = "indice de relacion con el centro", DefaultValue = "0", IsNullable = false)]
        public int PUE_PUE_ID {
            get { return _PUE_PUE_ID; }
            set {
                _PUE_PUE_ID = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PUE_PUE
        /// </summary>
        [DataNames("PUE_PUE")]
        [SugarColumn(ColumnName = "PUE_PUE", ColumnDescription = "descripcion del puesto", Length = 25, IsNullable = false)]
        public string Descripcion {
            get { return descripcion; }
            set {
                descripcion = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PUE_SAL
        /// </summary>
        [DataNames("PUE_SAL")]
        [SugarColumn(ColumnName = "PUE_SAL", ColumnDescription = "importe del salario", Length = 18, DecimalDigits = 4, IsNullable = false)]
        public decimal Salario {
            get { return salario; }
            set {
                salario = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PUE_PREPRO_FIJ
        /// </summary>
        [DataNames("PUE_PREPRO_FIJ")]
        [SugarColumn(ColumnName = "PUE_PREPRO_FIJ", ColumnDescription = "importe del premio de produccio fijo", Length = 18, DecimalDigits = 4, IsNullable = true)]
        public decimal PremioProduccionFijo {
            get { return premioProduccionFijo; }
            set {
                premioProduccionFijo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// PUE_PREPRO_FAC
        /// </summary>
        [DataNames("PUE_PREPRO_FAC")]
        [SugarColumn(ColumnName = "PUE_PREPRO_FAC", ColumnDescription = "factor del premio de produccion", Length = 18, DecimalDigits = 4, IsNullable = true)]
        public decimal PremioProduccionFactor {
            get { return premioProduccionFactor; }
            set {
                premioProduccionFactor = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro PUE_FM
        /// </summary>
        [DataNames("PUE_FM")]
        [SugarColumn(ColumnName = "PUE_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica >= firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
                OnPropertyChanged();
            }
        }
    }
}
