﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// vista de proceso, incluye areas, depto, seccion, centro y proceso
    /// </summary>
    public class ProcesoModelView : ProcesoModel, IProcesoModel {

        /// <sumary>
        /// obtener o establecer (EDP1_ARE)
        /// </sumary>
        [DataNames("EDP1_ARE")]
        public string Area { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP1_NOM)
        /// </sumary>
        [DataNames("EDP1_NOM")]
        public string Departamento { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_SEC)
        /// </sumary>
        [DataNames("EDP2_SEC")]
        public string Seccion { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP2_NOM)
        /// </sumary>
        [DataNames("EDP2_NOM")]
        public string Centro { get; set; }

        /// <sumary>
        /// obtener o establecer (EDP3_NOM)
        /// </sumary>
        [DataNames("EDP3_NOM")]
        public string Proceso { get; set; }

        public string Completo {
            get {
                return string.Concat(Seccion, " / ", Centro, " / ", Proceso);
            }
        }
    }
}
