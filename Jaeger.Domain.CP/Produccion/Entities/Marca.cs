﻿using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

/// purpose:
/// develop: ANHE1 29092020 1713
namespace Jaeger.Domain.CP.Produccion.Entities {
    public class Marca : BasePropertyChangeImplementation {
        #region declaraciones 
        private int _IdMarca;
        private int _Activo;
        private string _Nombre;
        private string _Creo;
        #endregion

        public Marca() { 
        }

        [DataNames("MPMAR_ID")]
        public int IdMarca {
            get { return this._IdMarca; }
            set {
                this._IdMarca = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MPMAR_ID")]
        public int Activo {
            get { return this._Activo; }
            set { this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MPMAR_NOM")]
        public string Descripcion {
            get { return this._Nombre; }
            set {
                this._Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("MPMAR_USU_FM")]
        public string Creo {
            get { return this._Creo; }
            set { this._Creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
