﻿using SqlSugar;
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// orden de produccion con informacion para la evaluacion de cumplimiento de empresa
    /// </summary>
    public class OrdenProduccionEvaluacionData : OrdenProduccionModel, IOrdenProduccionModel {
        public OrdenProduccionEvaluacionData() : base() { }

        #region contribuyentes
        [DataNames("PEDST_STA")]
        public string Status { get; set; }

        /// <summary>
        /// obtener o establecer indice de la tabla (DIR_ID)
        /// </summary>
        [DataNames("DIR_ID")]
        [SugarColumn(ColumnName = "dir_id", ColumnDescription = "DIR_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer DIR_NOM
        /// </summary>
        [DataNames("DIR_NOM")]
        [SugarColumn(ColumnName = "dir_nom", ColumnDescription = "DIR_NOM", Length = 100)]
        public string Cliente { get; set; }
        #endregion

        #region informacion del vendedor
        /// <summary>
        /// obtener o establecer la clave del vendedor
        /// </summary>
        [DataNames("VEN_CLA")]
        public string ClaveVendedor { get; set; }
        #endregion

        #region evaluacion
        /// <summary>
        /// obtener diferencia entre la cantidad solicitada y la remisionada
        /// </summary>
        public virtual decimal Diferencia {
            get { return this.Cantidad - this.Remisionado; }
        }

        public virtual int DiasTranscurridos {
            get { return (int)this.Cantidad; }
        }

        public virtual bool CumpleFechaAcuerdo {
            get { return this.FechaAcuerdo <= this.FechaRemision1; }
        }

        public virtual bool CumpleCantidad {
            get { return this.Remisionado >= this.Cantidad; }
        }
        #endregion
    }
}
