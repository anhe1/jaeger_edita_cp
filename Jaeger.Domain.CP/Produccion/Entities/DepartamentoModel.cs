﻿/// purpose:
/// develop: ANHE1 29092020 1713
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.CP.Produccion.Contracts;

namespace Jaeger.Domain.CP.Produccion.Entities {
    /// <summary>
    /// clase area y departamento EDP1
    /// </summary>
    [SugarTable("EDP1", "Clase, Area y Departamento")]
    public class DepartamentoModel : BasePropertyChangeImplementation, IDepartamentoModel {
        #region declaraciones
        private int edp1id;
        private int edp1a;
        private string edp1nom;
        private string edp1are;
        private int edp1sec;
        private int edp1usufm;
        private DateTime? fechaModifica;
        #endregion

        public DepartamentoModel() { }

        /// <sumary>
        /// obtener o establecer (EDP1_ID)
        /// </sumary>
        [DataNames("EDP1_ID")]
        [SugarColumn(ColumnName = "EDP1_ID", ColumnDescription = "IdDepto", IsPrimaryKey = true, IsIdentity = true)]
        public int IdDepto {
            get { return edp1id; }
            set {
                edp1id = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_A)
        /// </sumary>
        [DataNames("EDP1_A")]
        [SugarColumn(ColumnName = "EDP1_A", ColumnDescription = "Activo")]
        public int Activo {
            get { return edp1a; }
            set {
                edp1a = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer nombre o descripcion del departamento (EDP1_NOM)
        /// </sumary>
        [DataNames("EDP1_NOM")]
        [SugarColumn(ColumnName = "Nombre", ColumnDescription = "Nombre", Length = 25)]
        public string Nombre {
            get { return edp1nom; }
            set {
                edp1nom = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_ARE)
        /// </sumary>
        [DataNames("EDP1_ARE")]
        [SugarColumn(ColumnName = "EDP1_ARE", ColumnDescription = "Area", Length = 25)]
        public string Area {
            get { return edp1are; }
            set {
                edp1are = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_SEC)
        /// </sumary>
        [DataNames("EDP1_SEC")]
        [SugarColumn(ColumnName = "EDP1_SEC", ColumnDescription = "Secuencia")]
        public int Secuencia {
            get { return edp1sec; }
            set {
                edp1sec = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_USU_FM)
        /// </sumary>
        [DataNames("EDP1_USU_FM")]
        [SugarColumn(ColumnName = "EDP1_USU_FM", ColumnDescription = "Modifica")]
        public int Modifica {
            get { return edp1usufm; }
            set {
                edp1usufm = value;
                OnPropertyChanged();
            }
        }

        /// <sumary>
        /// obtener o establecer (EDP1_FM)
        /// </sumary>
        [DataNames("EDP1_FM")]
        [SugarColumn(ColumnName = "EDP1_FM", ColumnDescription = "FechaModifica")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (fechaModifica > firstGoodDate)
                    return fechaModifica;
                return null;
            }
            set {
                fechaModifica = value;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string AreaDepartamento {
            get { return string.Concat(Area, "/", Nombre); }
        }

        [SugarColumn(IsIgnore = true)]
        public string Descriptor {
            get { return string.Format("{0}: {1}", this.IdDepto.ToString("00"), this.Nombre); }
        }
    }
}
