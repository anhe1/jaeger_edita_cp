﻿/// purpose:
/// develop: ANHE1 29092020 1713
using Jaeger.Domain.CP.Produccion.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.CP.Produccion.Entities {
    public class Edp4PueModelView : Edp4PueModel, IEdp4PueModel {
        [DataNames("PUE_PUE")]
        public string Puesto { get; set; }
        [DataNames("EDP1_are")]
        public string Area { get; set; }
        [DataNames("EDP1_nom")]
        public string Departamento { get; set; }
    }
}
