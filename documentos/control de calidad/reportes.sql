/*
 *** CONSULTAS PARA REPORTES DE CONTROL DE CALIDAD
*/
-- NO CONFORMIDADES POR EJERCICIO Y PERIODO
WITH NOCONFORMIDAD AS (
    SELECT COUNT(NOCF.NOCF_ID) AS TOTAL, NOCF.NOCF_ANIO AS EJERCICIO, NOCF.NOCF_MES AS PERIODO
    FROM NOCF
    GROUP BY NOCF.NOCF_ANIO, NOCF.NOCF_MES
)
SELECT DISTINCT 
    EJERCICIO,
    SUM(IIF(PERIODO = 1, TOTAL,0)) ENERO,
    SUM(IIF(PERIODO = 2, TOTAL,0)) FEBRERO,
    SUM(IIF(PERIODO = 3, TOTAL,0)) MARZO,
    SUM(IIF(PERIODO = 4, TOTAL,0)) ABRIL,
    SUM(IIF(PERIODO = 5, TOTAL,0)) MAYO,
    SUM(IIF(PERIODO = 6, TOTAL,0)) JUNIO,
    SUM(IIF(PERIODO = 7, TOTAL,0)) JULIO,
    SUM(IIF(PERIODO = 8, TOTAL,0)) AGOSTO,
    SUM(IIF(PERIODO = 9, TOTAL,0)) SEPTIEMBRE,
    SUM(IIF(PERIODO = 10, TOTAL,0)) OCTUBRE,
    SUM(IIF(PERIODO = 11, TOTAL,0)) NOVIEMBRE,
    SUM(IIF(PERIODO = 12, TOTAL,0)) DICIEMBRE
FROM NOCONFORMIDAD
GROUP BY EJERCICIO

--- CONSULTA PARA OBTENER LISTADO DE ORDENES DE PRODUCCION CON COSTOS 
SELECT PEDPRD_ID, PEDPRD_COTPRD_ID, DIR_NOM, PEDPRD.PEDPRD_NOM, SUM (COTCOM_$_CEN) AS CEN, SUM(COTCOM_$_PRO) AS MO,SUM(COTCOM_$_MP) AS MP, SUM(COTCOM_$_CNS) AS CNS
FROM PEDPRD
LEFT JOIN DIR ON PEDPRD.PEDPRD_DIR_ID = DIR.DIR_ID
LEFT JOIN COTCOM ON PEDPRD_COTPRD_ID = COTCOM_COTPRD_ID AND PEDPRD_A > 0 AND COTCOM_A > 0
GROUP BY PEDPRD_ID, DIR_NOM, PEDPRD_NOM, PEDPRD_COTPRD_ID

--- CONSULTA PARA LISTADO DE EMPLEADOS
SELECT DIR.*
FROM DIR
LEFT JOIN DIRCLA ON DIR_DIRCLA_ID = DIRCLA_ID 
WHERE DIRCLA_C <>'0' AND DIRCLA_C <> '0' AND DIR_DIRCLA_ID = 9 AND LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DIR_NOM, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u')) || ',' || LOWER(DIR_CLA) || ','|| LOWER(DIR_RFC) LIKE '%antonio%'
ORDER BY DIR_NOM

--- CONSULTA DE NO CONFORMIDAD POR EJERCICIO, PERIODO Y DEPARTAMENTO
SELECT COUNT(NOCF.NOCF_ID) AS TOTAL, SUM(IIF(NOCF.NOCF_CTST_ID > 1, 1, 0)) AS RESUELTO, SUM(NOCF.NOCF_COSTO) AS COSTO, NOCF.NOCF_ANIO AS EJERCICIO, NOCF.NOCF_MES AS PERIODO, NOCF.NOCF_CTDPTO_ID
FROM NOCF 
GROUP BY NOCF.NOCF_ANIO, NOCF.NOCF_MES, NOCF.NOCF_CTDPTO_ID