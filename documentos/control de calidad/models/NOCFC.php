<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCFC 
*/
class NOCFC extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCFC';

	protected $primaryKey = 'NOCFC_ID';

	protected $hidden = [];

	protected $fillable = [
		'NOCFC_A',
		'NOCFC_NOCF_ID',
		'NOCFC_PEDPRD_ID',
		'NOCFC_NOM',
		'NOCFC_DESC',
		'NOCFC_MAQ',
		'NOCFC_MO',
		'NOCFC_MA',
		'NOCFC_MP',
		'NOCFC_CONS',
	];

	public $timestamps = false;

}
