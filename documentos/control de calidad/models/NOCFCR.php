<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCFCR 
*/
class NOCFCR extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCFCR';

	protected $primaryKey = 'NOCFCR_ID';

	protected $hidden = [];

	protected $fillable = [
		'NOCFCR_A',
		'NOCFCR_NOCF_ID',
		'NOCFCR_CLS5M_ID',
		'NOCFCR_CTEFE_ID',
		'NOCFCR_CTDEP_ID',
		'NOCFCR_CTUSR_ID',
		'NOCFCR_DESC',
		'NOCFCR_RESP',
		'NOCFCR_FN',
	];

	public $timestamps = false;

}
