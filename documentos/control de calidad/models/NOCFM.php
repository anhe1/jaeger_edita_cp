<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCFM 
*/
class NOCFM extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCFM';

	protected $primaryKey = 'NOCFM_ID';

	protected $hidden = [];

	protected $fillable = [
		'NOCFM_A',
		'NOCFM_NOCF_ID',
		'NOCFM_URL',
		'NOCFM_DESC',
	];

	public $timestamps = false;

}
