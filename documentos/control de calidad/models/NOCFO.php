<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCFO 
*/
class NOCFO extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCFO';

	protected $primaryKey = 'NOCFO_ID';

	protected $hidden = [];

	protected $fillable = [
		'NOCFO_A',
		'NOCFO_NOCF_ID',
		'NOCFO_PEDPRD_ID',
		'NOCFO_NOM',
		'NOCFO_DESC',
	];

	public $timestamps = false;

}
