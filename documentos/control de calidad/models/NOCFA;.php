<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCFA; 
*/
class NOCFA; extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCFA;';

	protected $primaryKey = '';

	protected $hidden = [];

	protected $fillable = [
		'NOCFA_ENE',
		'NOCFA_FEB',
		'NOCFA_MAR',
		'NOCFA_ABR',
		'NOCFA_MAY',
		'NOCFA_JUN',
		'NOCFA_JUL',
		'NOCFA_AGO',
		'NOCFA_SEP',
		'NOCFA_OCT',
		'NOCFA_NOV',
	];

	public $timestamps = false;

}
