<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCFAC 
*/
class NOCFAC extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCFAC';

	protected $primaryKey = 'NOCFAC_ID';

	protected $hidden = [];

	protected $fillable = [
		'NOCFAC_A',
		'NOCFAC_NOCF_ID',
		'NOCFAC_CTDEP_ID',
		'NOCFAC_CTTIP_ID',
		'NOCFAC_DESC',
		'NOCFAC_RESP',
		'NOCFAC_FEC',
	];

	public $timestamps = false;

}
