<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCFAP 
*/
class NOCFAP extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCFAP';

	protected $primaryKey = 'NOCFAP_ID';

	protected $hidden = [];

	protected $fillable = [
		'NOCFAP_A',
		'NOCFAP_NOCF_ID',
		'NOCFAP_FEC',
		'NOCFAP_DESC',
		'NOCFAP_RESP',
	];

	public $timestamps = false;

}
