<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCFS 
*/
class NOCFS extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCFS';

	protected $primaryKey = 'NOCFS_NOCF_ID';

	protected $hidden = [];

	protected $fillable = [
		'NOCFS_CTSTA_ID',
		'NOCFS_CTSTB_ID',
		'NOCFS_NOCFR_ID',
		'NOCFS_CVMTV',
		'NOCFS_NOTA',
		'NOCFS_USU_N',
	];

	public $timestamps = false;

}
