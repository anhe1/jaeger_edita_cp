<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/** 
* NOCF 
*/
class NOCF extends Model {
	use HasFactory;

	protected $connection = 'tenant';

	protected $table = 'NOCF';

	protected $primaryKey = 'NOCF_ID';

	protected $hidden = [];

	protected $fillable = [
		'NOCF_A',
		'NOCF_CTST_ID',
		'NOCF_CTTIP_ID',
		'NOCF_AUT_ID',
		'NOCF_CTUSR_ID',
		'NOCF_CTDPTO_ID',
		'NOCF_DESC',
		'NOCF_CANT',
		'NOCF_COSTO',
		'NOCF_NOM',
		'NOCF_DRCTR_ID',
		'NOCF_NOTA',
		'NOCF_FN',
		'NOCF_USU_N',
		'NOCF_FM',
		'NOCF_USU_M',
		'NOCF_ANIO',
	];

	public $timestamps = false;

}
