*** TIEMPO EXTRA
* Para reportar o solicitar tiempo extra para producción 
* Producción/Planeación/Tiempo Extra
***

- Orden/Componente-Proceso Cantidad y Tiempo estimado
- Registrar nombres de los participantes
- Fecha de emision
- Fecha de aplicación
- tiempo estimado o autorizado a cada trabajador
- version impresa
- autorización desde sistema?
- en el menú de producción debe aparecer como tiempo adicional
- es posible un historial?
- por supervisor
- motivo del tiempo adicional
- por departamento
- folio de control interno?

***********************************************************************
** FORMATO DE HORAS EXTRA
***********************************************************************
- Cliente
- Pedido / Descripción
- Componente
- Cantidad Est.
- Cantidad a Producir
- Tiempo Est.
- Tiempo Real
- Autoriza
- Nombre o nombres de los empleados


/*
*** 
*/

CREATE TABLE PEDPROH (
   PEDPROH_ID Inteher NOT NULL,
   PEDPROH_DRCTR_ID Integer,
   PEDPROH_DESC Varchar(150),
   PEDPROH_FN Timestamp,
   PEDPROH_USR_N Varchar(10),
);