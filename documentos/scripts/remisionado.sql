/*
    CREACION DE TABLAS PARA LA ACTUALIZACION DE CP PARA EL REMISIONADO AL CLIENTE, PARA LA PRIMERA CARGA DEBEMOS DESACTIVAR EL DISPARADOR PARA EL FOLIO
    REMISIONADO AL CLIENTE
*/
CREATE TABLE RMSN
(
  RMSN_ID Integer DEFAULT 0 NOT NULL,
  RMSN_A Smallint DEFAULT 0 NOT NULL,
  RMSN_CTDOC_ID Smallint NOT NULL,
  RMSN_DECI Smallint NOT NULL,
  RMSN_VER Varchar(3),
  RMSN_FOLIO Integer,
  RMSN_CTSR_ID Integer,
  RMSN_SERIE Varchar(10),
  RMSN_STTS_ID Integer,
  RMSN_PDD_ID Integer,
  RMSN_CTCLS_ID Integer,
  RMSN_CTENV_ID Integer,
  RMSN_DRCTR_ID Integer,
  RMSN_DRCCN_ID Integer,
  RMSN_VNDDR_ID Integer,
  RMSN_GUIA_ID Integer,
  RMSN_CTREL_ID Integer,
  RMSN_RFCE Varchar(14),
  RMSN_RFCR Varchar(14),
  RMSN_NOMR Varchar(255),
  RMSN_CLVR Varchar(10),
  RMSN_CNTCT Varchar(255),
  RMSN_UUID Varchar(36),
  RMSN_FECEMS Timestamp,
  RMSN_FECENT Timestamp,
  RMSN_FECUPC Timestamp,
  RMSN_FECCBR Timestamp,
  RMSN_FCCNCL Date,
  RMSN_FECVNC Date,
  RMSN_TPCMB Numeric(18,4),
  RMSN_SBTTL Numeric(18,4),
  RMSN_DSCNT Numeric(18,4),
  RMSN_TRSIVA Numeric(18,4),
  RMSN_GTOTAL Numeric(18,4),
  RMSN_FACIVA Numeric(18,4),
  RMSN_IVAPAC Numeric(18,4),
  RMSN_TOTAL Numeric(18,4),
  RMSN_FACPAC Numeric(18,4),
  RMSN_XCBPAC Numeric(18,4),
  RMSN_DESC Varchar(100),
  RMSN_FACDES Numeric(18,4),
  RMSN_DESCT Numeric(18,4),
  RMSN_XCBRR Numeric(18,4),
  RMSN_CBRD Numeric(18,4),
  RMSN_MONEDA Varchar(3),
  RMSN_MTDPG Varchar(3),
  RMSN_FRMPG Varchar(2),
  RMSN_NOTA Varchar(255),
  RMSN_VNDDR Varchar(10),
  RMSN_USR_C Varchar(10),
  RMSN_FN Timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  RMSN_USR_N Varchar(10),
  RMSN_FM Timestamp,
  RMSN_USR_M Varchar(10),
  RMSN_DRCCN Varchar(400),
  RMSN_CNDNS Varchar(30),
  RMSN_URL_PDF Varchar(255),
  CONSTRAINT PK_RMSN_ID PRIMARY KEY (RMSN_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'RMSN_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSN_A' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de documentos (generalmente para la remision a cliente es 26)'  where RDB$FIELD_NAME = 'RMSN_CTDOC_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de decimales'  where RDB$FIELD_NAME = 'RMSN_DECI' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'version de la remision'  where RDB$FIELD_NAME = 'RMSN_VER' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'folio de control interno'  where RDB$FIELD_NAME = 'RMSN_FOLIO' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de series y folios'  where RDB$FIELD_NAME = 'RMSN_CTSR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie de control interno del  documento en modo texto'  where RDB$FIELD_NAME = 'RMSN_SERIE' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status de la remision'  where RDB$FIELD_NAME = 'RMSN_STTS_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice o numero de pedido del cliente'  where RDB$FIELD_NAME = 'RMSN_PDD_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'RMSN_CTCLS_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla del metodo de envio'  where RDB$FIELD_NAME = 'RMSN_CTENV_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'RMSN_DRCTR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del domicilio del directorio'  where RDB$FIELD_NAME = 'RMSN_DRCCN_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id del vendedor'  where RDB$FIELD_NAME = 'RMSN_VNDDR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de guia o referencia del metodo de envio'  where RDB$FIELD_NAME = 'RMSN_GUIA_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice o clave de relacion con otros comprobantes'  where RDB$FIELD_NAME = 'RMSN_CTREL_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes del emisor'  where RDB$FIELD_NAME = 'RMSN_RFCE' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes del receptor'  where RDB$FIELD_NAME = 'RMSN_RFCR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o razon social del receptor'  where RDB$FIELD_NAME = 'RMSN_NOMR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de control interno del cliente'  where RDB$FIELD_NAME = 'RMSN_CLVR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del contacto'  where RDB$FIELD_NAME = 'RMSN_CNTCT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id de documento'  where RDB$FIELD_NAME = 'RMSN_UUID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de emision del comprobante'  where RDB$FIELD_NAME = 'RMSN_FECEMS' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de entrega'  where RDB$FIELD_NAME = 'RMSN_FECENT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de pago del comprobante'  where RDB$FIELD_NAME = 'RMSN_FECUPC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de recepcion de cobranza'  where RDB$FIELD_NAME = 'RMSN_FECCBR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de cancelacion del comprobante'  where RDB$FIELD_NAME = 'RMSN_FCCNCL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = ' fecha de vencimiento del pagare'  where RDB$FIELD_NAME = 'RMSN_FECVNC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de cambio'  where RDB$FIELD_NAME = 'RMSN_TPCMB' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'subtotal del comprobante'  where RDB$FIELD_NAME = 'RMSN_SBTTL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento'  where RDB$FIELD_NAME = 'RMSN_DSCNT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del impuesto trasladado IVA'  where RDB$FIELD_NAME = 'RMSN_TRSIVA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'gran total (subtotal - descuento + traslado de iva)'  where RDB$FIELD_NAME = 'RMSN_GTOTAL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor del iva pactado con el cliente'  where RDB$FIELD_NAME = 'RMSN_FACIVA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del iva pactado (SUBTOTAL * IVAPAC)'  where RDB$FIELD_NAME = 'RMSN_IVAPAC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del comprobante'  where RDB$FIELD_NAME = 'RMSN_TOTAL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor pactado, '  where RDB$FIELD_NAME = 'RMSN_FACPAC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'por cobrar pactado'  where RDB$FIELD_NAME = 'RMSN_XCBPAC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descripcion del tipo de descuento aplicado'  where RDB$FIELD_NAME = 'RMSN_DESC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor del descuento aplicado'  where RDB$FIELD_NAME = 'RMSN_FACDES' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe total del descuento (TOTAL X FACDES)'  where RDB$FIELD_NAME = 'RMSN_DESCT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe por cobrar del comprobante'  where RDB$FIELD_NAME = 'RMSN_XCBRR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'acumulado de la cobranza del comprobante'  where RDB$FIELD_NAME = 'RMSN_CBRD' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificacion ISO 4217.'  where RDB$FIELD_NAME = 'RMSN_MONEDA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del metodo de pago que aplica para este comprobante fiscal digital por Internet, conforme al Articulo 29-A fraccion VII incisos a y b del CFF.'  where RDB$FIELD_NAME = 'RMSN_MTDPG' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.'  where RDB$FIELD_NAME = 'RMSN_FRMPG' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'RMSN_NOTA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del vendedor asociada'  where RDB$FIELD_NAME = 'RMSN_VNDDR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela el comprobante'  where RDB$FIELD_NAME = 'RMSN_USR_C' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSN_FN' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que crea el registro'  where RDB$FIELD_NAME = 'RMSN_USR_N' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RMSN_FM' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica'  where RDB$FIELD_NAME = 'RMSN_USR_M' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'direccion del envio en modo texto'  where RDB$FIELD_NAME = 'RMSN_DRCCN' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '/remision/{0:36}.pdf'  where RDB$FIELD_NAME = 'RMSN_URL_PDF' and RDB$RELATION_NAME = 'RMSN';
ALTER TABLE RMSN ADD RMSN_SALDO COMPUTED BY (
    CAST((RMSN_XCBRR - RMSN_CBRD)
     AS NUMERIC(18, 4)));
CREATE INDEX IDX_RMSN_CTCLS_ID ON RMSN (RMSN_CTCLS_ID);
CREATE INDEX IDX_RMSN_CTENV_ID ON RMSN (RMSN_CTENV_ID);
CREATE INDEX IDX_RMSN_DRCTR_ID ON RMSN (RMSN_DRCTR_ID);
CREATE INDEX IDX_RMSN_PDD_ID ON RMSN (RMSN_PDD_ID);
CREATE INDEX IDX_RMSN_SERIE_ID ON RMSN (RMSN_CTSR_ID);
CREATE INDEX IDX_RMSN_STTS_ID ON RMSN (RMSN_STTS_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'remisiones del almacen de producto terminado, remision a cliente'
where RDB$RELATION_NAME = 'RMSN';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSN TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_RMSN_ID;

SET TERM ^ ;
CREATE TRIGGER RMSN_BI FOR RMSN ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 19072022 2053
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.RMSN_ID IS NULL) THEN
    NEW.RMSN_ID = GEN_ID(GEN_RMSN_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RMSN_ID, 0);
    if (tmp < new.RMSN_ID) then
      tmp = GEN_ID(GEN_RMSN_ID, new.RMSN_ID-tmp);
  END
END^
SET TERM ; ^


SET TERM ^ ;
CREATE TRIGGER RMSN_FOLIO FOR RMSN ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 191120211510
    Purpose: crear folio de control interno
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.RMSN_FOLIO IS NULL OR NEW.RMSN_FOLIO = 0) THEN
    BEGIN
        --SELECT MAX(RMSN.RMSN_FOLIO) + 1 FROM RMSN WHERE RMSN.RMSN_SERIE LIKE NEW.RMSN_SERIE INTO :tmp;
        SELECT MAX(RMSN.RMSN_FOLIO) + 1 FROM RMSN INTO :tmp;
        IF (tmp = 0 OR tmp IS NULL) THEN
            NEW.RMSN_FOLIO = 1;
        ELSE
            NEW.RMSN_FOLIO = tmp;
    END
  ELSE
  BEGIN
    
  END
END^
SET TERM ; ^

/*
    COMISIONES APLICABLES A REMISIONES
    */
CREATE TABLE RMSNC
(
  RMSNC_RMSN_ID Integer NOT NULL,
  RMSNC_A Smallint DEFAULT 1 NOT NULL,
  RMSNC_CTCMS_ID Integer,
  RMSNC_DRCTR_ID Integer,
  RMSNC_VNDR_ID Integer,
  RMSNC_CTDIS_ID Integer,
  RMSNC_DESC Varchar(150),
  RMSNC_FCPAC Numeric(18,4),
  RMSNC_FCACT Numeric(18,4),
  RMSNC_IMPR Numeric(18,4),
  RMSNC_ACUML Numeric(18,4),
  RMSNC_VNDR Varchar(10),
  RMSNC_FN Timestamp,
  RMSNC_USR_N Varchar(10),
  CONSTRAINT PK_RMSNC_RMSN_ID PRIMARY KEY (RMSNC_RMSN_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla remision'  where RDB$FIELD_NAME = 'RMSNC_RMSN_ID' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSNC_A' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de comisiones'  where RDB$FIELD_NAME = 'RMSNC_CTCMS_ID' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'RMSNC_DRCTR_ID' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de vendedores'  where RDB$FIELD_NAME = 'RMSNC_VNDR_ID' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descripcion'  where RDB$FIELD_NAME = 'RMSNC_DESC' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor pactado'  where RDB$FIELD_NAME = 'RMSNC_FCPAC' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor actualizado'  where RDB$FIELD_NAME = 'RMSNC_FCACT' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe calculado de la comision'  where RDB$FIELD_NAME = 'RMSNC_IMPR' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe pagado o acumulado'  where RDB$FIELD_NAME = 'RMSNC_ACUML' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del vendedor'  where RDB$FIELD_NAME = 'RMSNC_VNDR' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RMSNC_FN' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima clave del usuario que modifica el registro'  where RDB$FIELD_NAME = 'RMSNC_USR_N' and RDB$RELATION_NAME = 'RMSNC';
CREATE INDEX IDX_RMSNC_CTCMS_ID ON RMSNC (RMSNC_CTCMS_ID);
CREATE INDEX IDX_RMSNC_DRCTR_ID ON RMSNC (RMSNC_DRCTR_ID);
CREATE INDEX IDX_RMSNC_RMSN_ID ON RMSNC (RMSNC_RMSN_ID);
CREATE INDEX IDX_RMSNC_VNDR_ID ON RMSNC (RMSNC_VNDR_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'comisiones a vendedores'
where RDB$RELATION_NAME = 'RMSNC';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSNC TO  SYSDBA WITH GRANT OPTION;

/*
    TABLA DE RELACION DE ENTRE REMISIONES
    */
 CREATE TABLE RMSNR
(
  RMSNR_ID Integer DEFAULT 0 NOT NULL,
  RMSNR_A Smallint DEFAULT 1 NOT NULL,
  RMSNR_RMSN_ID Integer,
  RMSNR_DRCTR_ID Integer,
  RMSNR_CTREL_ID Integer,
  RMSNR_RELN Varchar(60),
  RMSNR_UUID Varchar(36),
  RMSNR_SERIE Varchar(25),
  RMSNR_FOLIO Integer,
  RMSNR_NOMR Varchar(255),
  RMSNR_FECEMS Date,
  RMSNR_TOTAL Numeric(18,4),
  RMSNR_USR_N Varchar(10),
  RMSNR_FN Timestamp,
  RMSNR_USR_M Varchar(10),
  RMSNR_FM Timestamp,
  CONSTRAINT PK_RMSNR_ID PRIMARY KEY (RMSNR_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'RMSNR_ID' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSNR_A' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de remisiones'  where RDB$FIELD_NAME = 'RMSNR_RMSN_ID' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del directorio'  where RDB$FIELD_NAME = 'RMSNR_DRCTR_ID' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de tipos de relacion'  where RDB$FIELD_NAME = 'RMSNR_CTREL_ID' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descripcion de la relacion del comprobante'  where RDB$FIELD_NAME = 'RMSNR_RELN' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres'  where RDB$FIELD_NAME = 'RMSNR_SERIE' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.'  where RDB$FIELD_NAME = 'RMSNR_FOLIO' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre(s), primer apellido, segundo apellido, segun corresponda, denominacion o razon social del contribuyente, inscrito en el RFC, del receptor del comprobante.'  where RDB$FIELD_NAME = 'RMSNR_NOMR' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha y hora de expedicion del comprobante.'  where RDB$FIELD_NAME = 'RMSNR_FECEMS' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe total del comprobante'  where RDB$FIELD_NAME = 'RMSNR_TOTAL' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'RMSNR_USR_N' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSNR_FN' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'RMSNR_USR_M' and RDB$RELATION_NAME = 'RMSNR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RMSNR_FM' and RDB$RELATION_NAME = 'RMSNR';
CREATE INDEX IDX_RMSNR_CTRMS_ID ON RMSNR (RMSNR_RMSN_ID);
CREATE INDEX IDX_RMSNR_DRCTR_ID ON RMSNR (RMSNR_DRCTR_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'comprobantes relacionados remisiones'
where RDB$RELATION_NAME = 'RMSNR';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSNR TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_RMSNR_ID;

SET TERM ^ ;
CREATE TRIGGER RMSNR_BI FOR RMSNR ACTIVE
BEFORE INSERT POSITION 0
AS
  DECLARE VARIABLE tmp DECIMAL(18,0);
  BEGIN
  /* -------------------------------------------------------------------------------------------------------------
      Develop: ANHE1 09022022 1841
      Purpose: Disparador para indice incremental
  ------------------------------------------------------------------------------------------------------------- */
    IF (NEW.RMSNR_ID IS NULL) THEN
      NEW.RMSNR_ID = GEN_ID(GEN_RMSNR_ID, 1);
    ELSE
    BEGIN
      tmp = GEN_ID(GEN_RMSNR_ID, 0);
      if (tmp < new.RMSNR_ID) then
        tmp = GEN_ID(GEN_RMSNR_ID, new.RMSNR_ID-tmp);
    END
  END^
SET TERM ; ^

/*
    TABLA DE REGISTRO DE CAMBIOS DE STATUS
*/
CREATE TABLE RMSNS
(
  RMSNS_ID Integer NOT NULL,
  RMSNS_A Smallint DEFAULT 1 NOT NULL,
  RMSNS_RMSN_ID Integer,
  RMSNS_RMSN_STTS_ID Integer,
  RMSNS_RMSN_STTSA_ID Integer,
  RMSNS_DRCTR_ID Integer,
  RMSNS_CVMTV Varchar(100),
  RMSNS_NOTA Varchar(100),
  RMSNS_USR_N Varchar(10),
  RMSNS_FN Timestamp DEFAULT 'Now' NOT NULL,
  CONSTRAINT PK_RMSNS_ID PRIMARY KEY (RMSNS_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice incremental'  where RDB$FIELD_NAME = 'RMSNS_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSNS_A' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de remisiones'  where RDB$FIELD_NAME = 'RMSNS_RMSN_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status anterior'  where RDB$FIELD_NAME = 'RMSNS_RMSN_STTS_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status autorizado'  where RDB$FIELD_NAME = 'RMSNS_RMSN_STTSA_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla del directorio'  where RDB$FIELD_NAME = 'RMSNS_DRCTR_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del motivo del cambio de status'  where RDB$FIELD_NAME = 'RMSNS_CVMTV' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'RMSNS_NOTA' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que autoriza el cambio'  where RDB$FIELD_NAME = 'RMSNS_USR_N' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSNS_FN' and RDB$RELATION_NAME = 'RMSNS';
CREATE INDEX IDX_RMSNS_DRCTR_ID ON RMSNS (RMSNS_DRCTR_ID);
CREATE INDEX IDX_RMSNS_RMSN_ID ON RMSNS (RMSNS_RMSN_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'remisionado: tabla de registro de cambios de status'
where RDB$RELATION_NAME = 'RMSNS';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSNS TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_RMSNS_ID;

SET TERM ^ ;
CREATE TRIGGER RMSNS_BI FOR RMSNS ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 2809202222008
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.RMSNS_ID IS NULL) THEN
    NEW.RMSNS_ID = GEN_ID(GEN_RMSNS_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RMSNS_ID, 0);
    if (tmp < new.RMSNS_ID) then
      tmp = GEN_ID(GEN_RMSNS_ID, new.RMSNS_ID-tmp);
  END
END^
SET TERM ; ^

/*
    ALMACEN PPT: movimientos del almacen de producto terminado
*/
CREATE TABLE MVAPT
(
  MVAPT_ID Integer DEFAULT 0 NOT NULL,
  MVAPT_A Smallint DEFAULT 1 NOT NULL,
  MVAPT_DOC_ID Integer DEFAULT 0,
  MVAPT_CTALM_ID Integer DEFAULT 0,
  MVAPT_ALMPT_ID Integer,
  MVAPT_RMSN_ID Integer DEFAULT 0,
  MVAPT_CTEFC_ID Smallint,
  MVAPT_DRCTR_ID Integer,
  MVAPT_PDCLN_ID Integer,
  MVAPT_CTDPT_ID Integer DEFAULT 0,
  MVAPT_CTPRD_ID Integer DEFAULT 0,
  MVAPT_CTMDL_ID Integer DEFAULT 0,
  MVAPT_CTPRC_ID Integer DEFAULT 0,
  MVAPT_CTESPC_ID Integer DEFAULT 0,
  MVAPT_CTUND_ID Integer DEFAULT 0,
  MVAPT_UNDF Numeric(18,4),
  MVAPT_UNDN Varchar(50),
  MVAPT_CANTE Numeric(18,4),
  MVAPT_CANTS Numeric(18,4),
  MVAPT_CTCLS Varchar(128),
  MVAPT_PRDN Varchar(128),
  MVAPT_MDLN Varchar(128),
  MVAPT_MRC Varchar(128),
  MVAPT_ESPC Varchar(128),
  MVAPT_ESPN Varchar(50),
  MVAPT_UNTC Numeric(18,4),
  MVAPT_UNDC Numeric(18,4),
  MVAPT_UNTR Numeric(18,4) DEFAULT 0,
  MVAPT_UNTR2 Numeric(18,4),
  MVAPT_SBTTL Numeric(18,4) DEFAULT 0,
  MVAPT_DESC Numeric(18,4),
  MVAPT_IMPRT Numeric(18,4),
  MVAPT_TSIVA Numeric(18,4) DEFAULT 0,
  MVAPT_TRIVA Numeric(18,4) DEFAULT 0,
  MVAPT_TOTAL Numeric(18,4) DEFAULT 0,
  MVAPT_SKU Varchar(20),
  MVAPT_USR_N Varchar(10),
  MVAPT_FN Timestamp DEFAULT 'now',
  MVAPT_USR_M Varchar(10),
  MVAPT_FM Date,
  CONSTRAINT PK_MVAPT_ID PRIMARY KEY (MVAPT_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'MVAPT_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'MVAPT_A' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de tipos de documento (26 = remisiones)'  where RDB$FIELD_NAME = 'MVAPT_DOC_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de almacenes'  where RDB$FIELD_NAME = 'MVAPT_CTALM_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla ALMPT'  where RDB$FIELD_NAME = 'MVAPT_ALMPT_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de remisiones'  where RDB$FIELD_NAME = 'MVAPT_RMSN_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'efecto del comprobante (1 = Ingreso, 2 = Egreso, 3 = Traslado)'  where RDB$FIELD_NAME = 'MVAPT_CTEFC_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el directorio, receptor (idCliente)'  where RDB$FIELD_NAME = 'MVAPT_DRCTR_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el numero de pedido'  where RDB$FIELD_NAME = 'MVAPT_PDCLN_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo del departamento al tratarse de un documento diferente a remision (26)'  where RDB$FIELD_NAME = 'MVAPT_CTDPT_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'MVAPT_CTPRD_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de modelos'  where RDB$FIELD_NAME = 'MVAPT_CTMDL_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de precios'  where RDB$FIELD_NAME = 'MVAPT_CTPRC_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de unidades'  where RDB$FIELD_NAME = 'MVAPT_CTUND_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor de unidad'  where RDB$FIELD_NAME = 'MVAPT_UNDF' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la unidad utilizada'  where RDB$FIELD_NAME = 'MVAPT_UNDN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad entrante'  where RDB$FIELD_NAME = 'MVAPT_CANTE' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad salida, generalmente el cantidad en una remision al cliente'  where RDB$FIELD_NAME = 'MVAPT_CANTS' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del catalogo'  where RDB$FIELD_NAME = 'MVAPT_CTCLS' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del modelo'  where RDB$FIELD_NAME = 'MVAPT_MDLN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la marca del producto o modelo'  where RDB$FIELD_NAME = 'MVAPT_MRC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'especificacion'  where RDB$FIELD_NAME = 'MVAPT_ESPC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la descripcion'  where RDB$FIELD_NAME = 'MVAPT_ESPN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'costo unitario del producto / modelo'  where RDB$FIELD_NAME = 'MVAPT_UNTC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'costo unitario por la unidad'  where RDB$FIELD_NAME = 'MVAPT_UNDC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'valor unitario por pieza'  where RDB$FIELD_NAME = 'MVAPT_UNTR' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'unitario de la unidad (valor unitario por el factor de la unidad)'  where RDB$FIELD_NAME = 'MVAPT_UNTR2' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sub total'  where RDB$FIELD_NAME = 'MVAPT_SBTTL' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento aplicado al producto'  where RDB$FIELD_NAME = 'MVAPT_DESC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe = subtotal - descuento'  where RDB$FIELD_NAME = 'MVAPT_IMPRT' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tasa del iva aplicable'  where RDB$FIELD_NAME = 'MVAPT_TSIVA' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del impuesto traslado IVA'  where RDB$FIELD_NAME = 'MVAPT_TRIVA' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total = importe + importe del iva'  where RDB$FIELD_NAME = 'MVAPT_TOTAL' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'identificador: IdProducto + IdModelo + IdEspecificacion (Tamanio)'  where RDB$FIELD_NAME = 'MVAPT_SKU' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que creo el registro'  where RDB$FIELD_NAME = 'MVAPT_USR_N' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'MVAPT_FN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del utlimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'MVAPT_USR_M' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'MVAPT_FM' and RDB$RELATION_NAME = 'MVAPT';
CREATE INDEX IDX_MVAPT_ALMPT_ID ON MVAPT (MVAPT_ALMPT_ID);
CREATE INDEX IDX_MVAPT_CTDPT_ID ON MVAPT (MVAPT_CTDPT_ID);
CREATE INDEX IDX_MVAPT_CTEFC_ID ON MVAPT (MVAPT_CTEFC_ID);
CREATE INDEX IDX_MVAPT_CTESPC_ID ON MVAPT (MVAPT_CTESPC_ID);
CREATE INDEX IDX_MVAPT_CTPRC_ID ON MVAPT (MVAPT_CTPRC_ID);
CREATE INDEX IDX_MVAPT_CTPRD_ID ON MVAPT (MVAPT_CTPRD_ID);
CREATE INDEX IDX_MVAPT_CTUND_ID ON MVAPT (MVAPT_CTUND_ID);
CREATE INDEX IDX_MVAPT_DOC_ID ON MVAPT (MVAPT_DOC_ID);
CREATE INDEX IDX_MVAPT_DRCTR_ID ON MVAPT (MVAPT_DRCTR_ID);
CREATE INDEX IDX_MVAPT_PDCLN_ID ON MVAPT (MVAPT_PDCLN_ID);
CREATE INDEX IDX_MVAPT_RMSN_ID ON MVAPT (MVAPT_RMSN_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'movimientos del almacen de producto terminado'
where RDB$RELATION_NAME = 'MVAPT';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON MVAPT TO  SYSDBA WITH GRANT OPTION;


CREATE GENERATOR GEN_MVAPT_ID;

SET TERM ^ ;
CREATE TRIGGER MVAPT_BI FOR MVAPT ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 21072022 1430
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.MVAPT_ID IS NULL) THEN
    NEW.MVAPT_ID = GEN_ID(GEN_MVAPT_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_MVAPT_ID, 0);
    if (tmp < new.MVAPT_ID) then
      tmp = GEN_ID(GEN_MVAPT_ID, new.MVAPT_ID-tmp);
  END
END^
SET TERM ; ^
