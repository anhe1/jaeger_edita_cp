/* ************************************************************************************************
     REQUERIMIENTOS DE COMPRA DE DEPARTAMENTOS

STATUS
 0 - CANCELADO
 1 - REQUERIDO
 2 - AUTORIZADO
 3 - ADQUIRIDO

************************************************************************************************ */
CREATE TABLE OCDP
(
  OCDP_ID Integer DEFAULT 0 NOT NULL,
  OCDP_VER Varchar(3) DEFAULT '2.0',
  OCDP_STTS_ID Integer,
  OCDP_DRCTR_ID Integer,
  OCDP_PRVDR_ID Integer,
  OCDP_CTDP_ID Integer,
  OCDP_PDCL_ID Integer,
  OCDP_TIPO_ID Integer,
  OCDP_NOMR Varchar(128),
  OCDP_NOMS Varchar(128),
  OCDP_CNTC Varchar(128),
  OCDP_NOTA Varchar(100),
  OCDP_CMAIL Varchar(60),
  OCDP_FECACO Date,
  OCDP_TEL Varchar(50),
  OCDP_SBTTL Numeric(18,4),
  OCDP_DESC Numeric(18,4),
  OCDP_TOTAL Numeric(18,4),
  OCDP_USR_A Varchar(10),
  OCDP_FEC_A Date,
  OCDP_FCCNCL Date,
  OCDP_USR_C Varchar(10),
  OCDP_CVCAN Smallint,
  OCDP_CLMTV Varchar(100),
  OCDP_CLNTA Varchar(100),
  OCDP_FN Timestamp DEFAULT 'NOW' NOT NULL,
  OCDP_USR_N Varchar(10),
  OCDP_FM Date,
  OCDP_USR_M Varchar(10),
  CONSTRAINT PK_OCDP_ID PRIMARY KEY (OCDP_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de tabla, folio incremental '  where RDB$FIELD_NAME = 'OCDP_ID' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'version del documento (default 2.0)'  where RDB$FIELD_NAME = 'OCDP_VER' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status (0-Cancelado, 1-Requerido, 2-Autorizado, 3-Adquirido)'  where RDB$FIELD_NAME = 'OCDP_STTS_ID' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de directorio'  where RDB$FIELD_NAME = 'OCDP_DRCTR_ID' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'OCDP_PRVDR_ID' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de departamento'  where RDB$FIELD_NAME = 'OCDP_CTDP_ID' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de orden de produccion'  where RDB$FIELD_NAME = 'OCDP_PDCL_ID' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de orden de compra (1-urgente, 2-)'  where RDB$FIELD_NAME = 'OCDP_TIPO_ID' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del proveedor'  where RDB$FIELD_NAME = 'OCDP_NOMR' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del proveedor sugerido'  where RDB$FIELD_NAME = 'OCDP_NOMS' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del contacto'  where RDB$FIELD_NAME = 'OCDP_CNTC' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'OCDP_NOTA' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'correco de contacto'  where RDB$FIELD_NAME = 'OCDP_CMAIL' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha requerida'  where RDB$FIELD_NAME = 'OCDP_FECACO' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'telefono de contacto'  where RDB$FIELD_NAME = 'OCDP_TEL' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del subtotal'  where RDB$FIELD_NAME = 'OCDP_SBTTL' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento aplicable'  where RDB$FIELD_NAME = 'OCDP_DESC' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe total'  where RDB$FIELD_NAME = 'OCDP_TOTAL' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del adiministrador que autoriza'  where RDB$FIELD_NAME = 'OCDP_USR_A' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de autorizacion'  where RDB$FIELD_NAME = 'OCDP_FEC_A' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de cancelacion del comprobante'  where RDB$FIELD_NAME = 'OCDP_FCCNCL' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela el comprobante'  where RDB$FIELD_NAME = 'OCDP_USR_C' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de cancelacion'  where RDB$FIELD_NAME = 'OCDP_CVCAN' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de cancelacion en modo texto {00:Descripcion}'  where RDB$FIELD_NAME = 'OCDP_CLMTV' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nota de la cancelacion'  where RDB$FIELD_NAME = 'OCDP_CLNTA' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'OCDP_FN' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'OCDP_USR_N' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'OCDP_FM' and RDB$RELATION_NAME = 'OCDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'OCDP_USR_M' and RDB$RELATION_NAME = 'OCDP';
CREATE INDEX IDX_OCDP_CTDP_ID ON OCDP (OCDP_CTDP_ID);
CREATE INDEX IDX_OCDP_DRCTR_ID ON OCDP (OCDP_DRCTR_ID);
CREATE INDEX IDX_OCDP_STTS_ID ON OCDP (OCDP_STTS_ID);
CREATE INDEX IDX_OCDP_TIPO_ID ON OCDP (OCDP_TIPO_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'orden de compra por departamento'
where RDB$RELATION_NAME = 'OCDP';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON OCDP TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_OCDP_ID;

SET TERM !! ;
CREATE TRIGGER OCDP_BI FOR OCDP
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 11102022 1620
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.OCDP_ID IS NULL) THEN
    NEW.OCDP_ID = GEN_ID(GEN_OCDP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_OCDP_ID, 0);
    if (tmp < new.OCDP_ID) then
      tmp = GEN_ID(GEN_OCDP_ID, new.OCDP_ID-tmp);
  END
END!!
SET TERM ; !!


/* ************************************************************************************************
     PARTIDAS DEL REQUERIMIENTO DE COMPRA
************************************************************************************************ */
CREATE TABLE OCDPP
(
  OCDPP_ID Integer DEFAULT 0 NOT NULL,
  OCDPP_A Smallint DEFAULT 1 NOT NULL,
  OCDPP_OCDP_ID Integer,
  OCDPP_CTPRD_ID Integer,
  OCDPP_CTMDL_ID Integer,
  OCDPP_CTDP_ID Integer,
  OCDPP_PDCL_ID Integer,
  OCDPP_PEDPRD Integer,
  OCDPP_CANT Numeric(18,4),
  OCDPP_UNDN Varchar(10),
  OCDPP_NOM Varchar(100),
  OCDPP_MRC Varchar(100),
  OCDPP_ESPC Varchar(100),
  OCDPP_NOTA Varchar(100),
  OCDPP_UNT Numeric(18,4),
  OCDPP_SBTTL Numeric(18,4),
  OCDPP_DESC Numeric(18,4),
  OCDPP_IMG_URL Varchar(500),
  OCDPP_USR_N Varchar(10),
  OCDPP_FN Timestamp DEFAULT 'NOW' NOT NULL,
  OCDPP_USR_M Varchar(10),
  OCDPP_FM Date,
  CONSTRAINT PK_OCDPP_ID PRIMARY KEY (OCDPP_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'OCDPP_ID' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'OCDPP_A' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de requerimiento de compras'  where RDB$FIELD_NAME = 'OCDPP_OCDP_ID' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'OCDPP_CTPRD_ID' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de modelos'  where RDB$FIELD_NAME = 'OCDPP_CTMDL_ID' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de departamentos (EDP1)'  where RDB$FIELD_NAME = 'OCDPP_CTDP_ID' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de pedido del cliente'  where RDB$FIELD_NAME = 'OCDPP_PDCL_ID' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de orden de produccion'  where RDB$FIELD_NAME = 'OCDPP_PEDPRD' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad'  where RDB$FIELD_NAME = 'OCDPP_CANT' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion de la unidad'  where RDB$FIELD_NAME = 'OCDPP_UNDN' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion del producto o servicio'  where RDB$FIELD_NAME = 'OCDPP_NOM' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'marca'  where RDB$FIELD_NAME = 'OCDPP_MRC' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'especificacion'  where RDB$FIELD_NAME = 'OCDPP_ESPC' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'OCDPP_NOTA' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'unitario'  where RDB$FIELD_NAME = 'OCDPP_UNT' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sub total'  where RDB$FIELD_NAME = 'OCDPP_SBTTL' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'imagen relacionada al producto solicitado /rfc/ocdp/image.ext'  where RDB$FIELD_NAME = 'OCDPP_IMG_URL' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'OCDPP_USR_N' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'OCDPP_FN' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'OCDPP_USR_M' and RDB$RELATION_NAME = 'OCDPP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion'  where RDB$FIELD_NAME = 'OCDPP_FM' and RDB$RELATION_NAME = 'OCDPP';
CREATE INDEX IDX_OCDPP_CTDP_ID ON OCDPP (OCDPP_CTDP_ID);
CREATE INDEX IDX_OCDPP_CTMDL_ID ON OCDPP (OCDPP_CTMDL_ID);
CREATE INDEX IDX_OCDPP_CTPRD_ID ON OCDPP (OCDPP_CTPRD_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'requerimiento de compra: partidas o productos'
where RDB$RELATION_NAME = 'OCDPP';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON OCDPP TO  SYSDBA WITH GRANT OPTION;


CREATE GENERATOR GEN_OCDPP_ID;

SET TERM !! ;
CREATE TRIGGER OCDPP_BI FOR OCDPP
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 11102022 1620
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.OCDPP_ID IS NULL) THEN
    NEW.OCDPP_ID = GEN_ID(GEN_OCDPP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_OCDPP_ID, 0);
    if (tmp < new.OCDPP_ID) then
      tmp = GEN_ID(GEN_OCDPP_ID, new.OCDPP_ID-tmp);
  END
END!!
SET TERM ; !!
