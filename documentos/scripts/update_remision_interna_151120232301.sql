/* ************************************************************************************************
    ACTUALIZACION DE REMISIONADO INTERNO
    15112023: tablas para el control de status de remisionado y relacion entre remisiones
************************************************************************************************ */
CREATE TABLE RMSDPS
(
  RMSDPS_ID Integer NOT NULL,
  RMSDPS_A Smallint DEFAULT 1 NOT NULL,
  RMSDPS_RMSN_ID Integer,
  RMSDPS_RMSN_STTS_ID Integer,
  RMSDPS_RMSN_STTSA_ID Integer,
  RMSDPS_DRCTR_ID Integer,
  RMSDPS_CVMTV Varchar(100),
  RMSDPS_NOTA Varchar(100),
  RMSDPS_USR_N Varchar(10),
  RMSDPS_FN Timestamp DEFAULT 'Now' NOT NULL,
  CONSTRAINT PK_RMSDPS_ID PRIMARY KEY (RMSDPS_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice incremental'  where RDB$FIELD_NAME = 'RMSDPS_ID' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSDPS_A' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de remisiones'  where RDB$FIELD_NAME = 'RMSDPS_RMSN_ID' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status anterior'  where RDB$FIELD_NAME = 'RMSDPS_RMSN_STTS_ID' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status autorizado'  where RDB$FIELD_NAME = 'RMSDPS_RMSN_STTSA_ID' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla del directorio'  where RDB$FIELD_NAME = 'RMSDPS_DRCTR_ID' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del motivo del cambio de status'  where RDB$FIELD_NAME = 'RMSDPS_CVMTV' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'RMSDPS_NOTA' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que autoriza el cambio'  where RDB$FIELD_NAME = 'RMSDPS_USR_N' and RDB$RELATION_NAME = 'RMSDPS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSDPS_FN' and RDB$RELATION_NAME = 'RMSDPS';
CREATE INDEX IDX_RMSDPS_DRCTR_ID ON RMSDPS (RMSDPS_DRCTR_ID);
CREATE INDEX IDX_RMSDPS_RMSN_ID ON RMSDPS (RMSDPS_RMSN_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'remisionado interno: tabla de registro de cambios de status'
where RDB$RELATION_NAME = 'RMSDPS';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSDPS TO  SYSDBA WITH GRANT OPTION;


CREATE TABLE RMSDPR
(
  RMSDPR_RMSN_ID Integer NOT NULL,
  RMSDPR_DRCTR_ID Integer NOT NULL,
  RMSDPR_CTREL_ID Integer NOT NULL,
  RMSDPR_RELN Varchar(60),
  RMSDPR_UUID Varchar(36) NOT NULL,
  RMSDPR_SERIE Varchar(25),
  RMSDPR_FOLIO Integer,
  RMSDPR_NOMR Varchar(255),
  RMSDPR_FECEMS Date,
  RMSDPR_TOTAL Numeric(18,4),
  RMSDPR_USR_N Varchar(10),
  RMSDPR_FN Timestamp,
  CONSTRAINT PK_RMSDPR PRIMARY KEY (RMSDPR_RMSN_ID,RMSDPR_DRCTR_ID,RMSDPR_CTREL_ID,RMSDPR_UUID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de remisiones'  where RDB$FIELD_NAME = 'RMSDPR_RMSN_ID' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del directorio'  where RDB$FIELD_NAME = 'RMSDPR_DRCTR_ID' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de tipos de relacion'  where RDB$FIELD_NAME = 'RMSDPR_CTREL_ID' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descripcion de la relacion del comprobante'  where RDB$FIELD_NAME = 'RMSDPR_RELN' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres'  where RDB$FIELD_NAME = 'RMSDPR_SERIE' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.'  where RDB$FIELD_NAME = 'RMSDPR_FOLIO' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre(s), primer apellido, segundo apellido, segun corresponda, denominacion o razon social del contribuyente, inscrito en el RFC, del receptor del comprobante.'  where RDB$FIELD_NAME = 'RMSDPR_NOMR' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha y hora de expedicion del comprobante.'  where RDB$FIELD_NAME = 'RMSDPR_FECEMS' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe total del comprobante'  where RDB$FIELD_NAME = 'RMSDPR_TOTAL' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'RMSDPR_USR_N' and RDB$RELATION_NAME = 'RMSDPR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSDPR_FN' and RDB$RELATION_NAME = 'RMSDPR';
CREATE INDEX IDX_RMSDPR_CTRMS_ID ON RMSDPR (RMSDPR_RMSN_ID);
CREATE INDEX IDX_RMSDPR_DRCTR_ID ON RMSDPR (RMSDPR_DRCTR_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'comprobantes relacionados remisiones de control interno'
where RDB$RELATION_NAME = 'RMSDPR';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSDPR TO  SYSDBA WITH GRANT OPTION;

