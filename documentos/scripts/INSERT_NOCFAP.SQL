/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 290820241129
    Purpose: METODO INSERT PARA ACCIONES PREVENTIVAS
------------------------------------------------------------------------------------------------------------- */

SET TERM ^ ;
CREATE PROCEDURE INSERT_NOCFAP (
    NOCFID Integer,
    FECHA Varchar(10),
    DESCR Varchar(510),
    RESP Varchar(256),
    PUST Varchar(256)
)
RETURNS (
    INDICE Integer
)
AS
DECLARE VARIABLE NINDEX INTEGER;
BEGIN
    SELECT MAX(NOCFAP.NOCFAP_ID) + 1 FROM NOCFAP
    INTO :NINDEX;
    IF (NINDEX IS NULL) THEN
        BEGIN
            NINDEX = 0;
        END 
        INSERT INTO NOCFAP (NOCFAP_ID, NOCFAP_A, NOCFAP_NOCF_ID, NOCFAP_FEC, NOCFAP_DESC, NOCFAP_RESP, NOCFAP_PUST) 
            VALUES (:NINDEX, 1, :NOCFID, :FECHA, :DESCR, :RESP, :PUST);
            INDICE = :NINDEX;
            SUSPEND;
        WHEN ANY DO
            BEGIN
                EXCEPTION;
                INDICE = -1;
            END 
END^
SET TERM ; ^
GRANT EXECUTE ON PROCEDURE INSERT_PEDFECR TO  SYSDBA;