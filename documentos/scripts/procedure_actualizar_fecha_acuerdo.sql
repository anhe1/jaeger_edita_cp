SET TERM ^ ;
CREATE PROCEDURE INSERT_PEDFEC (
    IDPEDPRD Integer,
    IDCENTRO Integer,
    IDEMPLEADO Integer,
    FECHAACUERDO Varchar(10),
    IDMOTIVO Integer,
    NOTA Varchar(50),
    CREO Varchar(10) )
RETURNS (
    INDICE Integer )
AS
DECLARE VARIABLE NINDEX INTEGER;
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 070920222155
    Purpose: METODO INSERT PARA FECHAS DE ACUERDO
------------------------------------------------------------------------------------------------------------- */
    
    SELECT MAX(PEDFEC.PEDFEC_ID) + 1 FROM PEDFEC
    INTO :NINDEX;
    
    IF (NINDEX IS NULL) THEN
        BEGIN
            NINDEX = 0;
        END 
            INSERT INTO PEDFEC (PEDFEC_ID, PEDFEC_A, PEDFEC_PEDPRD_ID, PEDFEC_CEN_ID, PEDFEC_DIR_ID,   PEDFEC_FEC, PEDFEC_FECL_ID, PEDFEC_OBS, PEDFEC_USU_N,      PEDFEC_USU_F) 
                        VALUES (  :NINDEX,      '1',        :IDPEDPRD,     :IDCENTRO,    :IDEMPLEADO,:FECHAACUERDO,      :IDMOTIVO,      :NOTA,        :CREO, CURRENT_TIMESTAMP);
            INDICE = :NINDEX;
            SUSPEND;
        WHEN ANY DO
            BEGIN
                EXCEPTION;
            INDICE = -1;
        END 

END^
SET TERM ; ^

UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice de la orden de produccion'
  where RDB$PARAMETER_NAME = 'IDPEDPRD' AND RDB$PROCEDURE_NAME = 'INSERT_PEDFEC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice de relacion con la tabla de departamentos, secciones y centros'
  where RDB$PARAMETER_NAME = 'IDCENTRO' AND RDB$PROCEDURE_NAME = 'INSERT_PEDFEC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice de relacion con el directorio de empleados'
  where RDB$PARAMETER_NAME = 'IDEMPLEADO' AND RDB$PROCEDURE_NAME = 'INSERT_PEDFEC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'fecha de acuerdo en modo texto ''DD.MM.YYYY'''
  where RDB$PARAMETER_NAME = 'FECHAACUERDO' AND RDB$PROCEDURE_NAME = 'INSERT_PEDFEC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice con la tabla de movitos del cambio de fechas de acuerdo'
  where RDB$PARAMETER_NAME = 'IDMOTIVO' AND RDB$PROCEDURE_NAME = 'INSERT_PEDFEC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'observaciones'
  where RDB$PARAMETER_NAME = 'NOTA' AND RDB$PROCEDURE_NAME = 'INSERT_PEDFEC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'clave del usuario creador del registro'
  where RDB$PARAMETER_NAME = 'CREO' AND RDB$PROCEDURE_NAME = 'INSERT_PEDFEC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'negativo si existe error'
  where RDB$PARAMETER_NAME = 'INDICE' AND RDB$PROCEDURE_NAME = 'INSERT_PEDFEC';
GRANT EXECUTE
 ON PROCEDURE INSERT_PEDFEC TO  SYSDBA;

