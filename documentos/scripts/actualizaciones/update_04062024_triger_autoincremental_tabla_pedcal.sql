CREATE GENERATOR GEN_PEDCAL_ID;

SET TERM !! ;
CREATE TRIGGER PEDCAL_BI FOR PEDCAL
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* ------------------------------------------------------------------------------------------------
    Develop: ANHE1 05072022 2152
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------ */
  IF (NEW.PEDCAL_ID IS NULL) THEN
    NEW.PEDCAL_ID = GEN_ID(GEN_PEDCAL_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_PEDCAL_ID, 0);
    if (tmp < new.PEDCAL_ID) then
      tmp = GEN_ID(GEN_PEDCAL_ID, new.PEDCAL_ID-tmp);
  END
END!!
SET TERM ; !!
