/* -------------------------------------------------------------------------------------------------------------
    DEVELOP: ANHE1 290520220943
    PURPOSE: TABLA PARA ALMACENAR LOS ACUMULADOS DEL REMISIONADO DE PRODUCCION AL ALMACEN DE MATERIA PRIMA
------------------------------------------------------------------------------------------------------------- */
CREATE TABLE RMSPED
(
  RMSPED_PEDPRD_ID Integer DEFAULT 0 NOT NULL,
  RMSPED_CTDEP_ID Integer DEFAULT 8 NOT NULL,
  RMSPED_FECEMS Timestamp,
  RMSPED_REM Numeric(18,4),
  CONSTRAINT PK_RMSPED_PEDPRD_ID PRIMARY KEY (RMSPED_PEDPRD_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la orden de produccion'  where RDB$FIELD_NAME = 'RMSPED_PEDPRD_ID' and RDB$RELATION_NAME = 'RMSPED';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del departamento emisor de la remision, por default 8:PosPrensa'  where RDB$FIELD_NAME = 'RMSPED_CTDEP_ID' and RDB$RELATION_NAME = 'RMSPED';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de emision de la remision'  where RDB$FIELD_NAME = 'RMSPED_FECEMS' and RDB$RELATION_NAME = 'RMSPED';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad remisionada por produccion'  where RDB$FIELD_NAME = 'RMSPED_REM' and RDB$RELATION_NAME = 'RMSPED';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'tabla de registro de cantidades remisionadas por produccion al almacen de producto terminado'
where RDB$RELATION_NAME = 'RMSPED';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSPED TO  SYSDBA WITH GRANT OPTION;


SET TERM ^ ;
CREATE PROCEDURE PEDPRD_RMSNDP_ACTUALIZA (
    PEDPRD_IDX Integer )
AS
DECLARE VARIABLE RP_REM NUMERIC(18,4);
DECLARE VARIABLE REM_FEC1 TIMESTAMP;
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    DEVELOP: ANHE1 290520240945
    PURPOSE:PROCEDIMIENTO PARA CREAR LOS ACUMULADOS DE LAS CANTIDADES REMISIONDAS
------------------------------------------------------------------------------------------------------------- */
    --SUMA VALORES DE REMISIONES
    SELECT MAX(RMSDP.RMSDP_FECEMS), SUM(MVADP.MVADP_CANTS)
    FROM MVADP
    LEFT JOIN RMSDP ON MVADP.MVADP_RMSN_ID = RMSDP.RMSDP_ID
    WHERE RMSDP.RMSDP_STTS_ID > 0 AND MVADP.MVADP_PDCLN_ID = :PEDPRD_IDX
    INTO REM_FEC1, :RP_REM;
    
    IF (RP_REM IS NULL) THEN
    BEGIN
        RP_REM = 0;
    END
    --ACTUALIZA O INSERTA EL REGISTRO SEGUN SEAL EL CASO
    UPDATE OR INSERT INTO RMSPED (RMSPED_PEDPRD_ID, RMSPED_FECEMS, RMSPED_REM)
    VALUES(:PEDPRD_IDX, :REM_FEC1, :RP_REM) MATCHING (RMSPED_PEDPRD_ID);
    
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE PEDPRD_RMSNDP_ACTUALIZA TO  SYSDBA;

SET TERM ^ ;
CREATE TRIGGER TR_MVADP_PEDPRD_RMSND_ACTUALIZA FOR MVADP ACTIVE
AFTER INSERT OR UPDATE POSITION 0
AS 
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    DEVELOP: ANHE1 29052024 0946
    PURPOSE: DISPARADOR PARA EJECUTAR PROCEDIMIENTO DE ACTUALIZACION DE CANTIDADES 
------------------------------------------------------------------------------------------------------------- */
	EXECUTE PROCEDURE PEDPRD_RMSNDP_ACTUALIZA(NEW.MVADP_PDCLN_ID);
END^
SET TERM ; ^