/* -------------------------------------------------------------------------------------------------------------
    -- PARA FECHAS DE ACUERDO, LAS CONSULTAS DESCRITAS ABAJO SON NECESARIAS PARA EL CAMBIO DE STATUS DE 
    LA ORDEN DE PRODUCCION
    
    
    PedFec_Cen_ID   -> Requerido
    PedFec_PedPrd_ID -> Requerido
    PedFec_Dir_ID   -> Requerido
    PedFec_Obs      -> opcional
    PedFec_Fec      -> Requerido
    PedFec_FecL_ID  -> Requerido
------------------------------------------------------------------------------------------------------------- */

-- CONSULTA DE LISTA DE DEPARTAMENTOS, SECCIONES Y CENTROS  
SELECT EDP2_ID, EDP2_A, EDP1_nom, EDP2_sec, EDP2_nom
FROM EDP1, EDP2
WHERE EDP2_EDP1_ID = EDP1_ID AND EDP2_A > 0

-- CONSULTA DE LISTA DE EMPLEADOS
SELECT DIR_ID, DIR_A, DIR_Cla, DIR_Nom
FROM DIR
WHERE DIR_DIRCLA_ID = 9 AND DIR_A > 0

-- MOTIVOS DEL CAMBIO DE FECHA DE ACUERDO
SELECT PedFecL_ID, PedFecL_A, PedFecL_Tex AS MOTIVO
FROM PedFecL
WHERE PedFecL_A > 0
ORDER BY PedFecL_Tex


-- CAMBIOS DE STATUS (TABLA)
SELECT PedFec_ID, PedFec_A, PedFec_PedPrd_ID, PedFec_Cen_ID, PedFec_Dir_ID, PedFec_Fec, PedFec_FecL_ID, PedFec_Obs, PedFec_Usu_N, PedFec_Usu_F
FROM PedFec
ORDER BY PedFec_Fec desc

-- CONSULTA PARA LA VISTA DE FECHAS DE ACUERDO
SELECT PedFec_ID, PedFec_PedPrd_ID, 
PedFec_Cen_ID, EDP1_nom AS DEPARTAMENTO, EDP2_sec AS SECCION, EDP2_nom AS CENTRO, 
PedFec_Dir_ID, DIR_Cla AS CLAVE, DIR_Nom SOLICITANTE,
PedFec_Fec AS FECHAACUERDO, 
PedFec_FecL_ID, PedFecL_Tex AS MOTIVO,
PedFec_Obs AS NOTA, PedFec_Usu_N AS CREO, PedFec_Usu_F FECHANUEVO
FROM PedFec, EDP1, EDP2, DIR, PedFecL
WHERE PedFec_A > 0 AND PedFec_Cen_ID = Edp2_ID AND EDP2_EDP1_ID = EDP1_ID AND EDP2_A > 0 AND PedFec_Dir_ID = Dir_ID AND PedFec_FecL_ID = PedFecL_ID
ORDER BY PedFec_Fec desc