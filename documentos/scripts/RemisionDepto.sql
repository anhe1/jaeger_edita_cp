000000
123456

1 - Aplicación
2 - Tab
3 - Grupo
4 - Sub grupo
5 - Boton
6 - Sub boton


CREATE TABLE RMSDP
(
  RMSDP_ID Integer DEFAULT 0 NOT NULL,
  RMSDP_A Smallint DEFAULT 0 NOT NULL,
  RMSDP_CTDEP_ID Integer,
  RMSDP_CTDOC_ID Smallint NOT NULL,
  RMSDP_DECI Smallint NOT NULL,
  RMSDP_STTS_ID Integer,
  RMSDP_CTSR_ID Integer,
  RMSDP_CTCLS_ID Integer,
  RMSDP_CTENV_ID Integer,
  RMSDP_DRCTR_ID Integer,
  RMSDP_DRCCN_ID Integer,
  RMSDP_VNDDR_ID Integer,
  RMSDP_GUIA_ID Integer,
  RMSDP_CTREL_ID Integer,
  RMSDP_VER Varchar(3),
  RMSDP_FOLIO Integer,
  RMSDP_SERIE Varchar(10),
  RMSDP_RFCE Varchar(14),
  RMSDP_CLVR Varchar(10),
  RMSDP_RFCR Varchar(14),
  RMSDP_NOMR Varchar(255),
  RMSDP_CNTCT Varchar(255),
  RMSDP_FECEMS Timestamp,
  RMSDP_FECENT Timestamp,
  RMSDP_TPCMB Numeric(18,4),
  RMSDP_SBTTL Numeric(18,4),
  RMSDP_DSCNT Numeric(18,4),
  RMSDP_TRSIVA Numeric(18,4),
  RMSDP_GTOTAL Numeric(18,4),
  RMSDP_MONEDA Varchar(3),
  RMSDP_DRCCN Varchar(400),
  RMSDP_NOTA Varchar(255),
  RMSDP_VNDDR Varchar(10),
  RMSDP_UUID Varchar(36),
  RMSDP_FCCNCL Date,
  RMSDP_USR_C Varchar(10),
  RMSDP_CVCAN Smallint,
  RMSDP_CLMTV Varchar(100),
  RMSDP_CLNTA Varchar(100),
  RMSDP_URL_PDF Varchar(100),
  RMSDP_FN Timestamp DEFAULT 'NOW' NOT NULL,
  RMSDP_USR_N Varchar(10),
  RMSDP_FM Timestamp,
  RMSDP_USR_M Varchar(10),
  CONSTRAINT PK_RMSDP_ID PRIMARY KEY (RMSDP_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'RMSDP_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSDP_A' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de departamento'  where RDB$FIELD_NAME = 'RMSDP_CTDEP_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de documentos (generalmente para la remision a cliente es 26)'  where RDB$FIELD_NAME = 'RMSDP_CTDOC_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de decimales'  where RDB$FIELD_NAME = 'RMSDP_DECI' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status de la remision'  where RDB$FIELD_NAME = 'RMSDP_STTS_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de series y folios'  where RDB$FIELD_NAME = 'RMSDP_CTSR_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'RMSDP_CTCLS_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla del metodo de envio'  where RDB$FIELD_NAME = 'RMSDP_CTENV_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'RMSDP_DRCTR_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del domicilio del directorio'  where RDB$FIELD_NAME = 'RMSDP_DRCCN_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id del vendedor'  where RDB$FIELD_NAME = 'RMSDP_VNDDR_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de guia o referencia del metodo de envio'  where RDB$FIELD_NAME = 'RMSDP_GUIA_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice o clave de relacion con otros comprobantes'  where RDB$FIELD_NAME = 'RMSDP_CTREL_ID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'version de la remision'  where RDB$FIELD_NAME = 'RMSDP_VER' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'folio de control interno'  where RDB$FIELD_NAME = 'RMSDP_FOLIO' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie de control interno del  documento en modo texto'  where RDB$FIELD_NAME = 'RMSDP_SERIE' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes del emisor'  where RDB$FIELD_NAME = 'RMSDP_RFCE' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de control interno del cliente'  where RDB$FIELD_NAME = 'RMSDP_CLVR' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes del receptor'  where RDB$FIELD_NAME = 'RMSDP_RFCR' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o razon social del receptor'  where RDB$FIELD_NAME = 'RMSDP_NOMR' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del contacto'  where RDB$FIELD_NAME = 'RMSDP_CNTCT' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de emision del comprobante'  where RDB$FIELD_NAME = 'RMSDP_FECEMS' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de entrega'  where RDB$FIELD_NAME = 'RMSDP_FECENT' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de cambio'  where RDB$FIELD_NAME = 'RMSDP_TPCMB' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'subtotal del comprobante'  where RDB$FIELD_NAME = 'RMSDP_SBTTL' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento'  where RDB$FIELD_NAME = 'RMSDP_DSCNT' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del impuesto trasladado IVA'  where RDB$FIELD_NAME = 'RMSDP_TRSIVA' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'gran total (subtotal - descuento + traslado de iva)'  where RDB$FIELD_NAME = 'RMSDP_GTOTAL' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificacion ISO 4217.'  where RDB$FIELD_NAME = 'RMSDP_MONEDA' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'direccion del envio en modo texto'  where RDB$FIELD_NAME = 'RMSDP_DRCCN' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'RMSDP_NOTA' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del vendedor asociada'  where RDB$FIELD_NAME = 'RMSDP_VNDDR' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id de documento'  where RDB$FIELD_NAME = 'RMSDP_UUID' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de cancelacion del comprobante'  where RDB$FIELD_NAME = 'RMSDP_FCCNCL' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela el comprobante'  where RDB$FIELD_NAME = 'RMSDP_USR_C' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de cancelacion'  where RDB$FIELD_NAME = 'RMSDP_CVCAN' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de cancelacion en modo texto {00:Descripcion}'  where RDB$FIELD_NAME = 'RMSDP_CLMTV' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nota de la cancelacion'  where RDB$FIELD_NAME = 'RMSDP_CLNTA' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '/remision/{0:36}.pdf'  where RDB$FIELD_NAME = 'RMSDP_URL_PDF' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSDP_FN' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que crea el registro'  where RDB$FIELD_NAME = 'RMSDP_USR_N' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RMSDP_FM' and RDB$RELATION_NAME = 'RMSDP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica'  where RDB$FIELD_NAME = 'RMSDP_USR_M' and RDB$RELATION_NAME = 'RMSDP';
CREATE INDEX IDX_RMSDP_CTCLS_ID ON RMSDP (RMSDP_CTCLS_ID);
CREATE INDEX IDX_RMSDP_CTENV_ID ON RMSDP (RMSDP_CTENV_ID);
CREATE INDEX IDX_RMSDP_DRCTR_ID ON RMSDP (RMSDP_DRCTR_ID);
CREATE INDEX IDX_RMSDP_SERIE_ID ON RMSDP (RMSDP_CTSR_ID);
CREATE INDEX IDX_RMSDP_STTS_ID ON RMSDP (RMSDP_STTS_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'remisionado de productos entre departamentos'
where RDB$RELATION_NAME = 'RMSDP';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSDP TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_RMSDP_ID;

SET TERM !! ;
CREATE TRIGGER RMSDP_BI FOR RMSDP
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 101020221844
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.RMSDP_ID IS NULL) THEN
    NEW.RMSDP_ID = GEN_ID(GEN_RMSDP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RMSDP_ID, 0);
    if (tmp < new.RMSDP_ID) then
      tmp = GEN_ID(GEN_RMSDP_ID, new.RMSDP_ID-tmp);
  END
END!!
SET TERM ; !!


SET TERM ^ ;
CREATE TRIGGER RMSDP_FO FOR RMSDP ACTIVE
BEFORE INSERT POSITION 0
AS 
DECLARE VARIABLE NFOLIO INTEGER;
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 101020221844
    Purpose: Disparador para crear folio consecutivo por departamento
------------------------------------------------------------------------------------------------------------- */
    IF (NEW.RMSDP_FOLIO IS NULL) THEN
        BEGIN
            SELECT MAX(RMSDP.RMSDP_FOLIO) + 1 
            FROM RMSDP 
            WHERE RMSDP.RMSDP_CTDEP_ID = NEW.RMSDP_CTDEP_ID
                INTO :NFOLIO;
            
            IF (NFOLIO IS NULL) THEN
                BEGIN
                    NFOLIO = 1;
                END
            NEW.RMSDP_FOLIO = NFOLIO;
        END

END^
SET TERM ; ^

CREATE TABLE MVADP
(
  MVADP_ID Integer DEFAULT 0 NOT NULL,
  MVADP_A Smallint DEFAULT 1 NOT NULL,
  MVADP_DOC_ID Integer DEFAULT 0,
  MVADP_CTALM_ID Integer DEFAULT 0,
  MVADP_ALMPT_ID Integer,
  MVADP_RMSN_ID Integer DEFAULT 0,
  MVADP_CTEFC_ID Smallint,
  MVADP_DRCTR_ID Integer,
  MVADP_PDCLN_ID Integer,
  MVADP_CTDPT_ID Integer DEFAULT 0,
  MVADP_CTPRD_ID Integer DEFAULT 0,
  MVADP_CTMDL_ID Integer DEFAULT 0,
  MVADP_CTUND_ID Integer DEFAULT 0,
  MVADP_UNDF Numeric(18,4),
  MVADP_UNDN Varchar(50),
  MVADP_CANTE Numeric(18,4),
  MVADP_CANTS Numeric(18,4),
  MVADP_NOMR Varchar(128),
  MVADP_PRDN Varchar(255),
  MVADP_COM Varchar(200),
  MVADP_UNTC Numeric(18,4),
  MVADP_UNDC Numeric(18,4),
  MVADP_UNTR Numeric(18,4) DEFAULT 0,
  MVADP_UNTR2 Numeric(18,4),
  MVADP_SBTTL Numeric(18,4) DEFAULT 0,
  MVADP_DESC Numeric(18,4),
  MVADP_IMPRT Numeric(18,4),
  MVADP_TSIVA Numeric(18,4) DEFAULT 0,
  MVADP_TRIVA Numeric(18,4) DEFAULT 0,
  MVADP_TOTAL Numeric(18,4) DEFAULT 0,
  MVADP_SKU Varchar(50),
  MVADP_USR_N Varchar(10),
  MVADP_FN Timestamp DEFAULT 'now',
  MVADP_USR_M Varchar(10),
  MVADP_FM Date,
  CONSTRAINT PK_MVADP_ID PRIMARY KEY (MVADP_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'MVADP_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'MVADP_A' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de tipos de documento (26 = remisiones)'  where RDB$FIELD_NAME = 'MVADP_DOC_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de almacenes'  where RDB$FIELD_NAME = 'MVADP_CTALM_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla ALMPT'  where RDB$FIELD_NAME = 'MVADP_ALMPT_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de remisiones'  where RDB$FIELD_NAME = 'MVADP_RMSN_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'efecto del comprobante (1 = Ingreso, 2 = Egreso, 3 = Traslado)'  where RDB$FIELD_NAME = 'MVADP_CTEFC_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el directorio, receptor (idCliente)'  where RDB$FIELD_NAME = 'MVADP_DRCTR_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el numero de pedido'  where RDB$FIELD_NAME = 'MVADP_PDCLN_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo del departamento al tratarse de un documento diferente a remision (26)'  where RDB$FIELD_NAME = 'MVADP_CTDPT_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'MVADP_CTPRD_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de modelos (id del componente)'  where RDB$FIELD_NAME = 'MVADP_CTMDL_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de unidades'  where RDB$FIELD_NAME = 'MVADP_CTUND_ID' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor de unidad'  where RDB$FIELD_NAME = 'MVADP_UNDF' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la unidad utilizada'  where RDB$FIELD_NAME = 'MVADP_UNDN' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad entrante'  where RDB$FIELD_NAME = 'MVADP_CANTE' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad salida, generalmente el cantidad en una remision al cliente'  where RDB$FIELD_NAME = 'MVADP_CANTS' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion del receptor (cliente)'  where RDB$FIELD_NAME = 'MVADP_NOMR' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del producto o descripcion'  where RDB$FIELD_NAME = 'MVADP_PRDN' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion del componente'  where RDB$FIELD_NAME = 'MVADP_COM' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'costo unitario del producto / modelo'  where RDB$FIELD_NAME = 'MVADP_UNTC' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'costo unitario por la unidad'  where RDB$FIELD_NAME = 'MVADP_UNDC' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'valor unitario por pieza'  where RDB$FIELD_NAME = 'MVADP_UNTR' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'unitario de la unidad (valor unitario por el factor de la unidad)'  where RDB$FIELD_NAME = 'MVADP_UNTR2' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sub total'  where RDB$FIELD_NAME = 'MVADP_SBTTL' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento aplicado al producto'  where RDB$FIELD_NAME = 'MVADP_DESC' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe = subtotal - descuento'  where RDB$FIELD_NAME = 'MVADP_IMPRT' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tasa del iva aplicable'  where RDB$FIELD_NAME = 'MVADP_TSIVA' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del impuesto traslado IVA'  where RDB$FIELD_NAME = 'MVADP_TRIVA' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total = importe + importe del iva'  where RDB$FIELD_NAME = 'MVADP_TOTAL' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'identificador: IdOrden + IdProducto + IdModelo'  where RDB$FIELD_NAME = 'MVADP_SKU' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que creo el registro'  where RDB$FIELD_NAME = 'MVADP_USR_N' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'MVADP_FN' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del utlimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'MVADP_USR_M' and RDB$RELATION_NAME = 'MVADP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'MVADP_FM' and RDB$RELATION_NAME = 'MVADP';
CREATE INDEX IDX_MVADP_ALMPT_ID ON MVADP (MVADP_ALMPT_ID);
CREATE INDEX IDX_MVADP_CTDPT_ID ON MVADP (MVADP_CTDPT_ID);
CREATE INDEX IDX_MVADP_CTEFC_ID ON MVADP (MVADP_CTEFC_ID);
CREATE INDEX IDX_MVADP_CTMDL_ID ON MVADP (MVADP_CTMDL_ID);
CREATE INDEX IDX_MVADP_CTPRD_ID ON MVADP (MVADP_CTPRD_ID);
CREATE INDEX IDX_MVADP_CTUND_ID ON MVADP (MVADP_CTUND_ID);
CREATE INDEX IDX_MVADP_DOC_ID ON MVADP (MVADP_DOC_ID);
CREATE INDEX IDX_MVADP_DRCTR_ID ON MVADP (MVADP_DRCTR_ID);
CREATE INDEX IDX_MVADP_PDCLN_ID ON MVADP (MVADP_PDCLN_ID);
CREATE INDEX IDX_MVADP_RMSN_ID ON MVADP (MVADP_RMSN_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'movimientos del las remisiones de departamento'
where RDB$RELATION_NAME = 'MVADP';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON MVADP TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_MVADP_ID;

SET TERM !! ;
CREATE TRIGGER MVADP_BI FOR MVADP
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 101020221844
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.MVADP_ID IS NULL) THEN
    NEW.MVADP_ID = GEN_ID(GEN_MVADP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_MVADP_ID, 0);
    if (tmp < new.MVADP_ID) then
      tmp = GEN_ID(GEN_MVADP_ID, new.MVADP_ID-tmp);
  END
END!!
SET TERM ; !!
