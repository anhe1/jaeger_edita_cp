﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CP.Beta {
    static class Program {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var login = new Forms.LoginForm(args);
            Application.Run(login);
            if (ConfigService.Piloto != null && ConfigService.Synapsis != null)
                Application.Run(new Forms.MainRibbonForm());
        }
    }
}
