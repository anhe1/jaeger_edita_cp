﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.CP.Beta.Forms {
    partial class MainRibbonForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainRibbonForm));
            this.BEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.RadDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.RadContainer = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.MenuRibbonBar = new Telerik.WinControls.UI.RadRibbonBar();
            this.TCPEmpresa = new Telerik.WinControls.UI.RibbonTab();
            this.cpemp_grp_directorio = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpemp_gdir_directorio = new Telerik.WinControls.UI.RadButtonElement();
            this.cpemp_grp_producto = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpemp_gprd_clasificacion = new Telerik.WinControls.UI.RadButtonElement();
            this.cpemp_gprd_catalogo = new Telerik.WinControls.UI.RadButtonElement();
            this.TCPCotizacion = new Telerik.WinControls.UI.RibbonTab();
            this.cpcot_grp_cotizacion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cpcot_gcot_productos = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cpcot_bprd_producto = new Telerik.WinControls.UI.RadMenuItem();
            this.cpcot_bprd_subproducto = new Telerik.WinControls.UI.RadMenuItem();
            this.cpcot_gcot_presupuesto = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_cotizaciones = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_cliente = new Telerik.WinControls.UI.RadButtonElement();
            this.cpcot_gcot_conf = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.TCPProduccion = new Telerik.WinControls.UI.RibbonTab();
            this.cppro_grp_produccion = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.cppro_gpro_orden = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_bord_historial = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_bord_proceso = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_reqcompra = new Telerik.WinControls.UI.RadButtonElement();
            this.cppro_gpro_planea = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_gpro_calendario = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_gantt = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_plan = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_tieadd = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_plan4 = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_sgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cppro_sgrp_depto = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_bdepto_trabaj = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_bdepto_proces = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_sgrp_avance = new Telerik.WinControls.UI.RadButtonElement();
            this.cppro_sgrp_evaluacion = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_beva_eprodof = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_beva_eproduccion = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_beva_eperiodo = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_gpro_remision = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cppro_brem_Emitido = new Telerik.WinControls.UI.RadMenuItem();
            this.cppro_brem_Recibido = new Telerik.WinControls.UI.RadMenuItem();
            this.TCPVentas = new Telerik.WinControls.UI.RibbonTab();
            this.cpvnt_grp_ventas = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.ppvnt_gdir_clientes = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_vendedor = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_prodvend = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_group1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cpvnt_gven_pedidos = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_comision = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_ventcosto = new Telerik.WinControls.UI.RadButtonElement();
            this.cpvnt_gven_remisionado = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cpvnt_brem_historial = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_brem_porcobrar = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_brem_rempart = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_gven_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.cpvnt_gven_reporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.cpvnt_gven_reporte2 = new Telerik.WinControls.UI.RadMenuItem();
            this.TCCalidad = new Telerik.WinControls.UI.RibbonTab();
            this.ccal_grp_nconf = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.ccal_gnco_new = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.ccal_gnco_todo = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_gnco_proceso = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_gnco_reporte = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.ccal_gnco_reporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_gnco_reporte2 = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_gnco_reporte3 = new Telerik.WinControls.UI.RadMenuItem();
            this.ccal_grp_prod = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.ccal_gprd_oprod = new Telerik.WinControls.UI.RadButtonElement();
            this.ccal_gprd_facue = new Telerik.WinControls.UI.RadButtonElement();
            this.ccal_gprd_remis = new Telerik.WinControls.UI.RadButtonElement();
            this.ttools = new Telerik.WinControls.UI.RibbonTab();
            this.dsk_grp_config = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gconfig_param = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gconfig_emisor = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_avanzado = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_menu = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_usuario = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_perfil = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_grp_theme = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gtheme_2010Black = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gtheme_2010Blue = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gtheme_2010Silver = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_grp_about = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_btn_manual = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_btn_about = new Telerik.WinControls.UI.RadButtonElement();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.office2010SilverTheme1 = new Telerik.WinControls.Themes.Office2010SilverTheme();
            this.descargarLogo = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).BeginInit();
            this.RadDock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // BEstado
            // 
            this.BEstado.Location = new System.Drawing.Point(0, 603);
            this.BEstado.Name = "BEstado";
            this.BEstado.Size = new System.Drawing.Size(1108, 26);
            this.BEstado.SizingGrip = false;
            this.BEstado.TabIndex = 1;
            // 
            // RadDock
            // 
            this.RadDock.AutoDetectMdiChildren = true;
            this.RadDock.Controls.Add(this.RadContainer);
            this.RadDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadDock.IsCleanUpTarget = true;
            this.RadDock.Location = new System.Drawing.Point(0, 162);
            this.RadDock.MainDocumentContainer = this.RadContainer;
            this.RadDock.Name = "RadDock";
            // 
            // 
            // 
            this.RadDock.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadDock.Size = new System.Drawing.Size(1108, 441);
            this.RadDock.TabIndex = 6;
            this.RadDock.TabStop = false;
            // 
            // RadContainer
            // 
            this.RadContainer.Name = "RadContainer";
            // 
            // 
            // 
            this.RadContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadContainer.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            // 
            // MenuRibbonBar
            // 
            this.MenuRibbonBar.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.TCPEmpresa,
            this.TCPCotizacion,
            this.TCPProduccion,
            this.TCPVentas,
            this.TCCalidad,
            this.ttools});
            // 
            // 
            // 
            this.MenuRibbonBar.ExitButton.Text = "Exit";
            this.MenuRibbonBar.ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.MenuRibbonBar.Location = new System.Drawing.Point(0, 0);
            this.MenuRibbonBar.Name = "MenuRibbonBar";
            // 
            // 
            // 
            this.MenuRibbonBar.OptionsButton.Text = "Options";
            this.MenuRibbonBar.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // 
            // 
            this.MenuRibbonBar.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.MenuRibbonBar.Size = new System.Drawing.Size(1108, 162);
            this.MenuRibbonBar.StartButtonImage = global::Jaeger.UI.CP.Beta.Properties.Resources.male_user_32px;
            this.MenuRibbonBar.TabIndex = 0;
            this.MenuRibbonBar.Text = "MainRibbonForm";
            this.MenuRibbonBar.Visible = false;
            // 
            // TCPEmpresa
            // 
            this.TCPEmpresa.IsSelected = false;
            this.TCPEmpresa.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_grp_directorio,
            this.cpemp_grp_producto});
            this.TCPEmpresa.Name = "TCPEmpresa";
            this.TCPEmpresa.Text = "TEmpresa";
            this.TCPEmpresa.UseMnemonic = false;
            // 
            // cpemp_grp_directorio
            // 
            this.cpemp_grp_directorio.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_gdir_directorio});
            this.cpemp_grp_directorio.Name = "cpemp_grp_directorio";
            this.cpemp_grp_directorio.Text = "bDirectorio";
            // 
            // cpemp_gdir_directorio
            // 
            this.cpemp_gdir_directorio.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.person_30px;
            this.cpemp_gdir_directorio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpemp_gdir_directorio.Name = "cpemp_gdir_directorio";
            this.cpemp_gdir_directorio.Text = "bDirectorio";
            this.cpemp_gdir_directorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpemp_gdir_directorio.UseCompatibleTextRendering = false;
            this.cpemp_gdir_directorio.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpemp_grp_producto
            // 
            this.cpemp_grp_producto.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpemp_gprd_clasificacion,
            this.cpemp_gprd_catalogo});
            this.cpemp_grp_producto.Name = "cpemp_grp_producto";
            this.cpemp_grp_producto.Text = "bProductos";
            // 
            // cpemp_gprd_clasificacion
            // 
            this.cpemp_gprd_clasificacion.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.categorize_30px;
            this.cpemp_gprd_clasificacion.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpemp_gprd_clasificacion.Name = "cpemp_gprd_clasificacion";
            this.cpemp_gprd_clasificacion.Text = "bClasificación";
            this.cpemp_gprd_clasificacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpemp_gprd_clasificacion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpemp_gprd_catalogo
            // 
            this.cpemp_gprd_catalogo.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.product_30px;
            this.cpemp_gprd_catalogo.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpemp_gprd_catalogo.Name = "cpemp_gprd_catalogo";
            this.cpemp_gprd_catalogo.Text = "bProductos";
            this.cpemp_gprd_catalogo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpemp_gprd_catalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TCPCotizacion
            // 
            this.TCPCotizacion.IsSelected = false;
            this.TCPCotizacion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpcot_grp_cotizacion});
            this.TCPCotizacion.Name = "TCPCotizacion";
            this.TCPCotizacion.Text = "TCPCotizacion";
            this.TCPCotizacion.UseMnemonic = false;
            // 
            // cpcot_grp_cotizacion
            // 
            this.cpcot_grp_cotizacion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpcot_gcot_productos,
            this.cpcot_gcot_presupuesto,
            this.cpcot_gcot_cotizaciones,
            this.cpcot_gcot_cliente,
            this.cpcot_gcot_conf});
            this.cpcot_grp_cotizacion.Name = "cpcot_grp_cotizacion";
            this.cpcot_grp_cotizacion.Text = "gCotizacion";
            // 
            // cpcot_gcot_productos
            // 
            this.cpcot_gcot_productos.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpcot_gcot_productos.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpcot_gcot_productos.ExpandArrowButton = false;
            this.cpcot_gcot_productos.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.product_30px;
            this.cpcot_gcot_productos.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_productos.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpcot_bprd_producto,
            this.cpcot_bprd_subproducto});
            this.cpcot_gcot_productos.Name = "cpcot_gcot_productos";
            this.cpcot_gcot_productos.Text = "bProductos";
            this.cpcot_gcot_productos.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_productos.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // cpcot_bprd_producto
            // 
            this.cpcot_bprd_producto.Name = "cpcot_bprd_producto";
            this.cpcot_bprd_producto.Text = "bProductos";
            // 
            // cpcot_bprd_subproducto
            // 
            this.cpcot_bprd_subproducto.Name = "cpcot_bprd_subproducto";
            this.cpcot_bprd_subproducto.Text = "bSubProductos";
            // 
            // cpcot_gcot_presupuesto
            // 
            this.cpcot_gcot_presupuesto.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.estimate_30px;
            this.cpcot_gcot_presupuesto.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_presupuesto.Name = "cpcot_gcot_presupuesto";
            this.cpcot_gcot_presupuesto.Text = "bPresupuestos";
            this.cpcot_gcot_presupuesto.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_presupuesto.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.cpcot_gcot_presupuesto.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_gcot_cotizaciones
            // 
            this.cpcot_gcot_cotizaciones.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.accounting_30px;
            this.cpcot_gcot_cotizaciones.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_cotizaciones.Name = "cpcot_gcot_cotizaciones";
            this.cpcot_gcot_cotizaciones.Text = "bCotizaciones";
            this.cpcot_gcot_cotizaciones.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_cotizaciones.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpcot_gcot_cotizaciones.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_gcot_cliente
            // 
            this.cpcot_gcot_cliente.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.person_30px;
            this.cpcot_gcot_cliente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_cliente.Name = "cpcot_gcot_cliente";
            this.cpcot_gcot_cliente.Text = "bCliente";
            this.cpcot_gcot_cliente.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_cliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpcot_gcot_cliente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpcot_gcot_conf
            // 
            this.cpcot_gcot_conf.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpcot_gcot_conf.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpcot_gcot_conf.ExpandArrowButton = false;
            this.cpcot_gcot_conf.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.administrative_tools_30px;
            this.cpcot_gcot_conf.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpcot_gcot_conf.Name = "cpcot_gcot_conf";
            this.cpcot_gcot_conf.Text = "bConfiguracion";
            this.cpcot_gcot_conf.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.cpcot_gcot_conf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // TCPProduccion
            // 
            this.TCPProduccion.IsSelected = false;
            this.TCPProduccion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_grp_produccion});
            this.TCPProduccion.Name = "TCPProduccion";
            this.TCPProduccion.Text = "TProducción";
            this.TCPProduccion.UseMnemonic = false;
            // 
            // cppro_grp_produccion
            // 
            this.cppro_grp_produccion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_gpro_orden,
            this.cppro_gpro_reqcompra,
            this.cppro_gpro_planea,
            this.cppro_gpro_sgrupo,
            this.cppro_gpro_remision});
            this.cppro_grp_produccion.Name = "cppro_grp_produccion";
            this.cppro_grp_produccion.Text = "gProduccion";
            // 
            // cppro_gpro_orden
            // 
            this.cppro_gpro_orden.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_gpro_orden.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_gpro_orden.ExpandArrowButton = false;
            this.cppro_gpro_orden.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.production_order_history_30px;
            this.cppro_gpro_orden.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_orden.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_bord_historial,
            this.cppro_bord_proceso});
            this.cppro_gpro_orden.Name = "cppro_gpro_orden";
            this.cppro_gpro_orden.Text = "bOrd.Produccion";
            this.cppro_gpro_orden.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_orden.UseCompatibleTextRendering = false;
            // 
            // cppro_bord_historial
            // 
            this.cppro_bord_historial.Name = "cppro_bord_historial";
            this.cppro_bord_historial.Text = "bHistorial";
            this.cppro_bord_historial.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_bord_proceso
            // 
            this.cppro_bord_proceso.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.production_in_progress_16px;
            this.cppro_bord_proceso.Name = "cppro_bord_proceso";
            this.cppro_bord_proceso.Text = "bEnProceso";
            this.cppro_bord_proceso.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_reqcompra
            // 
            this.cppro_gpro_reqcompra.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.archive_list_of_parts_30px;
            this.cppro_gpro_reqcompra.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_reqcompra.Name = "cppro_gpro_reqcompra";
            this.cppro_gpro_reqcompra.Text = "bReqCompra";
            this.cppro_gpro_reqcompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_reqcompra.TextWrap = true;
            this.cppro_gpro_reqcompra.UseCompatibleTextRendering = false;
            this.cppro_gpro_reqcompra.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_planea
            // 
            this.cppro_gpro_planea.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_gpro_planea.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_gpro_planea.ExpandArrowButton = false;
            this.cppro_gpro_planea.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.calendar_30px;
            this.cppro_gpro_planea.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_planea.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_gpro_calendario,
            this.cppro_gpro_gantt,
            this.cppro_gpro_plan,
            this.cppro_gpro_tieadd,
            this.cppro_gpro_plan4});
            this.cppro_gpro_planea.Name = "cppro_gpro_planea";
            this.cppro_gpro_planea.Text = "bCalendario";
            this.cppro_gpro_planea.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cppro_gpro_planea.UseCompatibleTextRendering = false;
            // 
            // cppro_gpro_calendario
            // 
            this.cppro_gpro_calendario.Name = "cppro_gpro_calendario";
            this.cppro_gpro_calendario.Text = "cppro_gpro_calendario";
            this.cppro_gpro_calendario.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_gantt
            // 
            this.cppro_gpro_gantt.Name = "cppro_gpro_gantt";
            this.cppro_gpro_gantt.Text = "cppro_gpro_gantt";
            this.cppro_gpro_gantt.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_plan
            // 
            this.cppro_gpro_plan.Name = "cppro_gpro_plan";
            this.cppro_gpro_plan.Text = "cppro_gpro_plan";
            this.cppro_gpro_plan.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_tieadd
            // 
            this.cppro_gpro_tieadd.Name = "cppro_gpro_tieadd";
            this.cppro_gpro_tieadd.Text = "b.TieExtra";
            // 
            // cppro_gpro_plan4
            // 
            this.cppro_gpro_plan4.Name = "cppro_gpro_plan4";
            this.cppro_gpro_plan4.Text = "cppro_gpro_plan4";
            this.cppro_gpro_plan4.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_sgrupo
            // 
            this.cppro_gpro_sgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_sgrp_depto,
            this.cppro_sgrp_avance,
            this.cppro_sgrp_evaluacion});
            this.cppro_gpro_sgrupo.Name = "cppro_gpro_sgrupo";
            this.cppro_gpro_sgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cppro_gpro_sgrupo.Text = "Sub Grupo";
            this.cppro_gpro_sgrupo.UseCompatibleTextRendering = false;
            // 
            // cppro_sgrp_depto
            // 
            this.cppro_sgrp_depto.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_sgrp_depto.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_sgrp_depto.ExpandArrowButton = false;
            this.cppro_sgrp_depto.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.calendar_16;
            this.cppro_sgrp_depto.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_bdepto_trabaj,
            this.cppro_bdepto_proces});
            this.cppro_sgrp_depto.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_depto.Name = "cppro_sgrp_depto";
            this.cppro_sgrp_depto.Text = "g.Departamento";
            this.cppro_sgrp_depto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_depto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // cppro_bdepto_trabaj
            // 
            this.cppro_bdepto_trabaj.Name = "cppro_bdepto_trabaj";
            this.cppro_bdepto_trabaj.Text = "b.Trabajdores";
            this.cppro_bdepto_trabaj.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_bdepto_proces
            // 
            this.cppro_bdepto_proces.Name = "cppro_bdepto_proces";
            this.cppro_bdepto_proces.Text = "b.Procesos";
            this.cppro_bdepto_proces.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_sgrp_avance
            // 
            this.cppro_sgrp_avance.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.production_in_progress_16px;
            this.cppro_sgrp_avance.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_avance.Name = "cppro_sgrp_avance";
            this.cppro_sgrp_avance.ShowBorder = false;
            this.cppro_sgrp_avance.Text = "bAvance";
            this.cppro_sgrp_avance.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_avance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cppro_sgrp_avance.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_sgrp_evaluacion
            // 
            this.cppro_sgrp_evaluacion.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_sgrp_evaluacion.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_sgrp_evaluacion.ExpandArrowButton = false;
            this.cppro_sgrp_evaluacion.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.edit_graph_report_16px;
            this.cppro_sgrp_evaluacion.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_beva_eprodof,
            this.cppro_beva_eproduccion,
            this.cppro_beva_eperiodo});
            this.cppro_sgrp_evaluacion.MinSize = new System.Drawing.Size(0, 20);
            this.cppro_sgrp_evaluacion.Name = "cppro_sgrp_evaluacion";
            this.cppro_sgrp_evaluacion.Text = "bEvaluacion";
            this.cppro_sgrp_evaluacion.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cppro_sgrp_evaluacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // cppro_beva_eprodof
            // 
            this.cppro_beva_eprodof.Name = "cppro_beva_eprodof";
            this.cppro_beva_eprodof.Text = "e.Evaluacion";
            this.cppro_beva_eprodof.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_beva_eproduccion
            // 
            this.cppro_beva_eproduccion.Name = "cppro_beva_eproduccion";
            this.cppro_beva_eproduccion.Text = "eProduccion";
            this.cppro_beva_eproduccion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_beva_eperiodo
            // 
            this.cppro_beva_eperiodo.Name = "cppro_beva_eperiodo";
            this.cppro_beva_eperiodo.Text = "bPeriodo";
            this.cppro_beva_eperiodo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_gpro_remision
            // 
            this.cppro_gpro_remision.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cppro_gpro_remision.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cppro_gpro_remision.ExpandArrowButton = false;
            this.cppro_gpro_remision.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.production_finished_30px;
            this.cppro_gpro_remision.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cppro_gpro_remision.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cppro_brem_Emitido,
            this.cppro_brem_Recibido});
            this.cppro_gpro_remision.Name = "cppro_gpro_remision";
            this.cppro_gpro_remision.Text = "bRemisionado";
            this.cppro_gpro_remision.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cppro_brem_Emitido
            // 
            this.cppro_brem_Emitido.Name = "cppro_brem_Emitido";
            this.cppro_brem_Emitido.Text = "bEmitido";
            this.cppro_brem_Emitido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cppro_brem_Recibido
            // 
            this.cppro_brem_Recibido.Name = "cppro_brem_Recibido";
            this.cppro_brem_Recibido.Text = "bRecibido";
            this.cppro_brem_Recibido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TCPVentas
            // 
            this.TCPVentas.IsSelected = true;
            this.TCPVentas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_grp_ventas});
            this.TCPVentas.Name = "TCPVentas";
            this.TCPVentas.Text = "TVentas";
            this.TCPVentas.UseMnemonic = false;
            // 
            // cpvnt_grp_ventas
            // 
            this.cpvnt_grp_ventas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ppvnt_gdir_clientes,
            this.cpvnt_gven_vendedor,
            this.cpvnt_gven_prodvend,
            this.cpvnt_gven_group1,
            this.cpvnt_gven_remisionado,
            this.cpvnt_gven_reportes});
            this.cpvnt_grp_ventas.Name = "cpvnt_grp_ventas";
            this.cpvnt_grp_ventas.Text = "tcpvnt_Ventas";
            // 
            // ppvnt_gdir_clientes
            // 
            this.ppvnt_gdir_clientes.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.person_30px;
            this.ppvnt_gdir_clientes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ppvnt_gdir_clientes.Name = "ppvnt_gdir_clientes";
            this.ppvnt_gdir_clientes.Text = "b.Clientes";
            this.ppvnt_gdir_clientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ppvnt_gdir_clientes.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_vendedor
            // 
            this.cpvnt_gven_vendedor.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.businessman_30px;
            this.cpvnt_gven_vendedor.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_vendedor.Name = "cpvnt_gven_vendedor";
            this.cpvnt_gven_vendedor.Text = "bVendedor";
            this.cpvnt_gven_vendedor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpvnt_gven_vendedor.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_prodvend
            // 
            this.cpvnt_gven_prodvend.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.new_product_30px;
            this.cpvnt_gven_prodvend.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_prodvend.Name = "cpvnt_gven_prodvend";
            this.cpvnt_gven_prodvend.Text = "bProductos";
            this.cpvnt_gven_prodvend.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cpvnt_gven_prodvend.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_group1
            // 
            this.cpvnt_gven_group1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_gven_pedidos,
            this.cpvnt_gven_comision,
            this.cpvnt_gven_ventcosto});
            this.cpvnt_gven_group1.Name = "cpvnt_gven_group1";
            this.cpvnt_gven_group1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.cpvnt_gven_group1.Text = "Grupo pedido";
            // 
            // cpvnt_gven_pedidos
            // 
            this.cpvnt_gven_pedidos.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_pedidos.Name = "cpvnt_gven_pedidos";
            this.cpvnt_gven_pedidos.ShowBorder = false;
            this.cpvnt_gven_pedidos.Text = "bPedido";
            this.cpvnt_gven_pedidos.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_pedidos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_pedidos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_comision
            // 
            this.cpvnt_gven_comision.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_comision.Name = "cpvnt_gven_comision";
            this.cpvnt_gven_comision.ShowBorder = false;
            this.cpvnt_gven_comision.Text = "bComision";
            this.cpvnt_gven_comision.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_comision.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_comision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_ventcosto
            // 
            this.cpvnt_gven_ventcosto.MinSize = new System.Drawing.Size(0, 20);
            this.cpvnt_gven_ventcosto.Name = "cpvnt_gven_ventcosto";
            this.cpvnt_gven_ventcosto.ShowBorder = false;
            this.cpvnt_gven_ventcosto.Text = "bVentaVsCosto";
            this.cpvnt_gven_ventcosto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpvnt_gven_ventcosto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cpvnt_gven_ventcosto.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_remisionado
            // 
            this.cpvnt_gven_remisionado.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpvnt_gven_remisionado.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpvnt_gven_remisionado.ExpandArrowButton = false;
            this.cpvnt_gven_remisionado.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.new_product_30px;
            this.cpvnt_gven_remisionado.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_remisionado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_brem_historial,
            this.cpvnt_brem_porcobrar,
            this.cpvnt_brem_rempart});
            this.cpvnt_gven_remisionado.Name = "cpvnt_gven_remisionado";
            this.cpvnt_gven_remisionado.Text = "bRemisionado";
            this.cpvnt_gven_remisionado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpvnt_brem_historial
            // 
            this.cpvnt_brem_historial.Name = "cpvnt_brem_historial";
            this.cpvnt_brem_historial.Text = "bRemision";
            this.cpvnt_brem_historial.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_brem_porcobrar
            // 
            this.cpvnt_brem_porcobrar.Name = "cpvnt_brem_porcobrar";
            this.cpvnt_brem_porcobrar.Text = "bPorCobrar";
            this.cpvnt_brem_porcobrar.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_brem_rempart
            // 
            this.cpvnt_brem_rempart.Name = "cpvnt_brem_rempart";
            this.cpvnt_brem_rempart.Text = "bPartida";
            this.cpvnt_brem_rempart.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_reportes
            // 
            this.cpvnt_gven_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.cpvnt_gven_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.cpvnt_gven_reportes.ExpandArrowButton = false;
            this.cpvnt_gven_reportes.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.ratings_30px;
            this.cpvnt_gven_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.cpvnt_gven_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cpvnt_gven_reporte1,
            this.cpvnt_gven_reporte2});
            this.cpvnt_gven_reportes.Name = "cpvnt_gven_reportes";
            this.cpvnt_gven_reportes.Text = "b.Reportes";
            this.cpvnt_gven_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cpvnt_gven_reporte1
            // 
            this.cpvnt_gven_reporte1.Name = "cpvnt_gven_reporte1";
            this.cpvnt_gven_reporte1.Text = "b.Reporte";
            this.cpvnt_gven_reporte1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // cpvnt_gven_reporte2
            // 
            this.cpvnt_gven_reporte2.Name = "cpvnt_gven_reporte2";
            this.cpvnt_gven_reporte2.Text = "b.Reporte";
            this.cpvnt_gven_reporte2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TCCalidad
            // 
            this.TCCalidad.IsSelected = false;
            this.TCCalidad.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ccal_grp_nconf,
            this.ccal_grp_prod});
            this.TCCalidad.Name = "TCCalidad";
            this.TCCalidad.Text = "TCalidad";
            this.TCCalidad.UseMnemonic = false;
            // 
            // ccal_grp_nconf
            // 
            this.ccal_grp_nconf.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ccal_gnco_new,
            this.ccal_gnco_reporte});
            this.ccal_grp_nconf.Name = "ccal_grp_nconf";
            this.ccal_grp_nconf.Text = "g.No Conformidad";
            // 
            // ccal_gnco_new
            // 
            this.ccal_gnco_new.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.ccal_gnco_new.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.ccal_gnco_new.ExpandArrowButton = false;
            this.ccal_gnco_new.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.documents_30px;
            this.ccal_gnco_new.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ccal_gnco_new.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ccal_gnco_todo,
            this.ccal_gnco_proceso});
            this.ccal_gnco_new.Name = "ccal_gnco_new";
            this.ccal_gnco_new.Text = "b.Nuevo";
            this.ccal_gnco_new.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // ccal_gnco_todo
            // 
            this.ccal_gnco_todo.Name = "ccal_gnco_todo";
            this.ccal_gnco_todo.Text = "b.Historial";
            this.ccal_gnco_todo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_gnco_proceso
            // 
            this.ccal_gnco_proceso.Name = "ccal_gnco_proceso";
            this.ccal_gnco_proceso.Text = "b.EnProceso";
            this.ccal_gnco_proceso.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_gnco_reporte
            // 
            this.ccal_gnco_reporte.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.ccal_gnco_reporte.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.ccal_gnco_reporte.ExpandArrowButton = false;
            this.ccal_gnco_reporte.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.ratings_30px;
            this.ccal_gnco_reporte.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ccal_gnco_reporte.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ccal_gnco_reporte1,
            this.ccal_gnco_reporte2,
            this.ccal_gnco_reporte3});
            this.ccal_gnco_reporte.Name = "ccal_gnco_reporte";
            this.ccal_gnco_reporte.Text = "b.Reportes";
            this.ccal_gnco_reporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // ccal_gnco_reporte1
            // 
            this.ccal_gnco_reporte1.Name = "ccal_gnco_reporte1";
            this.ccal_gnco_reporte1.Text = "b.Reporte 1";
            this.ccal_gnco_reporte1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_gnco_reporte2
            // 
            this.ccal_gnco_reporte2.Name = "ccal_gnco_reporte2";
            this.ccal_gnco_reporte2.Text = "b.Reporte 2";
            this.ccal_gnco_reporte2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_gnco_reporte3
            // 
            this.ccal_gnco_reporte3.Name = "ccal_gnco_reporte3";
            this.ccal_gnco_reporte3.Text = "b.Reporte 3";
            this.ccal_gnco_reporte3.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // ccal_grp_prod
            // 
            this.ccal_grp_prod.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ccal_gprd_oprod,
            this.ccal_gprd_facue,
            this.ccal_gprd_remis});
            this.ccal_grp_prod.Name = "ccal_grp_prod";
            this.ccal_grp_prod.Text = "g.Producción";
            // 
            // ccal_gprd_oprod
            // 
            this.ccal_gprd_oprod.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.production_order_history_30px;
            this.ccal_gprd_oprod.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ccal_gprd_oprod.Name = "ccal_gprd_oprod";
            this.ccal_gprd_oprod.Text = "b.Ord. de Producción";
            this.ccal_gprd_oprod.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // ccal_gprd_facue
            // 
            this.ccal_gprd_facue.Name = "ccal_gprd_facue";
            this.ccal_gprd_facue.Text = "b.F. de Acuerdo";
            this.ccal_gprd_facue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // ccal_gprd_remis
            // 
            this.ccal_gprd_remis.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.production_finished_30px;
            this.ccal_gprd_remis.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ccal_gprd_remis.Name = "ccal_gprd_remis";
            this.ccal_gprd_remis.Text = "b.Remisionado";
            this.ccal_gprd_remis.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // ttools
            // 
            this.ttools.IsSelected = false;
            this.ttools.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_grp_config,
            this.dsk_grp_theme,
            this.dsk_grp_about});
            this.ttools.Name = "ttools";
            this.ttools.Text = "THerramientas";
            this.ttools.UseMnemonic = false;
            // 
            // dsk_grp_config
            // 
            this.dsk_grp_config.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_param});
            this.dsk_grp_config.Name = "dsk_grp_config";
            this.dsk_grp_config.Text = "bConfiguración";
            // 
            // dsk_gconfig_param
            // 
            this.dsk_gconfig_param.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gconfig_param.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gconfig_param.ExpandArrowButton = false;
            this.dsk_gconfig_param.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.control_panel_30px;
            this.dsk_gconfig_param.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_param.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_emisor,
            this.dsk_gconfig_avanzado,
            this.dsk_gconfig_menu,
            this.dsk_gconfig_usuario,
            this.dsk_gconfig_perfil});
            this.dsk_gconfig_param.Name = "dsk_gconfig_param";
            this.dsk_gconfig_param.Text = "b.Parametros";
            this.dsk_gconfig_param.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gconfig_emisor
            // 
            this.dsk_gconfig_emisor.Name = "dsk_gconfig_emisor";
            this.dsk_gconfig_emisor.Text = "b.Emisor";
            this.dsk_gconfig_emisor.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_avanzado
            // 
            this.dsk_gconfig_avanzado.Name = "dsk_gconfig_avanzado";
            this.dsk_gconfig_avanzado.Text = "b.Avanzado";
            this.dsk_gconfig_avanzado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_menu
            // 
            this.dsk_gconfig_menu.Name = "dsk_gconfig_menu";
            this.dsk_gconfig_menu.Text = "b.Menus";
            this.dsk_gconfig_menu.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_usuario
            // 
            this.dsk_gconfig_usuario.Name = "dsk_gconfig_usuario";
            this.dsk_gconfig_usuario.Text = "b.Usuarios";
            this.dsk_gconfig_usuario.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_perfil
            // 
            this.dsk_gconfig_perfil.Name = "dsk_gconfig_perfil";
            this.dsk_gconfig_perfil.Text = "b.Perfil";
            this.dsk_gconfig_perfil.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_grp_theme
            // 
            this.dsk_grp_theme.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtheme_2010Black,
            this.dsk_gtheme_2010Blue,
            this.dsk_gtheme_2010Silver});
            this.dsk_grp_theme.Name = "dsk_grp_theme";
            this.dsk_grp_theme.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.dsk_grp_theme.Text = "gTheme";
            // 
            // dsk_gtheme_2010Black
            // 
            this.dsk_gtheme_2010Black.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Black.Image")));
            this.dsk_gtheme_2010Black.Name = "dsk_gtheme_2010Black";
            this.dsk_gtheme_2010Black.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Black.Tag = "Office2010Black";
            this.dsk_gtheme_2010Black.Text = "Office 2010 Black";
            this.dsk_gtheme_2010Black.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Black.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Black.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_gtheme_2010Blue
            // 
            this.dsk_gtheme_2010Blue.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Blue.Image")));
            this.dsk_gtheme_2010Blue.Name = "dsk_gtheme_2010Blue";
            this.dsk_gtheme_2010Blue.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Blue.Tag = "Office2010Blue";
            this.dsk_gtheme_2010Blue.Text = "Office 2010 Blue";
            this.dsk_gtheme_2010Blue.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Blue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Blue.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_gtheme_2010Silver
            // 
            this.dsk_gtheme_2010Silver.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Silver.Image")));
            this.dsk_gtheme_2010Silver.Name = "dsk_gtheme_2010Silver";
            this.dsk_gtheme_2010Silver.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Silver.Tag = "Office2010Silver";
            this.dsk_gtheme_2010Silver.Text = "Office 2010 Silver";
            this.dsk_gtheme_2010Silver.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Silver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Silver.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_grp_about
            // 
            this.dsk_grp_about.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_btn_manual,
            this.dsk_btn_about});
            this.dsk_grp_about.Name = "dsk_grp_about";
            this.dsk_grp_about.Text = "bAyuda";
            // 
            // dsk_btn_manual
            // 
            this.dsk_btn_manual.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.user_manual_30px;
            this.dsk_btn_manual.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_btn_manual.Name = "dsk_btn_manual";
            this.dsk_btn_manual.Text = "b.Manual";
            this.dsk_btn_manual.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_btn_about
            // 
            this.dsk_btn_about.Image = global::Jaeger.UI.CP.Beta.Properties.Resources.about_30px;
            this.dsk_btn_about.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_btn_about.Name = "dsk_btn_about";
            this.dsk_btn_about.Text = "bAcercaDe";
            this.dsk_btn_about.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dsk_btn_about.UseCompatibleTextRendering = false;
            this.dsk_btn_about.Click += new System.EventHandler(this.AcercaDe_Click);
            // 
            // MainRibbonForm
            // 
            this.AllowAero = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 629);
            this.Controls.Add(this.RadDock);
            this.Controls.Add(this.BEstado);
            this.Controls.Add(this.MenuRibbonBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = null;
            this.Name = "MainRibbonForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "MainRibbonForm";
            this.ThemeName = "Office2007Silver";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainRibbonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).EndInit();
            this.RadDock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar MenuRibbonBar;
        private Telerik.WinControls.UI.RadStatusStrip BEstado;
        private Telerik.WinControls.UI.RibbonTab ttools;
        private Telerik.WinControls.UI.Docking.RadDock RadDock;
        private Telerik.WinControls.UI.Docking.DocumentContainer RadContainer;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_config;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gconfig_param;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_avanzado;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_usuario;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_menu;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_perfil;
        private Telerik.WinControls.UI.RibbonTab TCPEmpresa;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpemp_grp_directorio;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gdir_directorio;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gprd_clasificacion;
        private Telerik.WinControls.UI.RibbonTab TCPProduccion;
        private Telerik.WinControls.UI.RadRibbonBarGroup cppro_grp_produccion;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_gpro_orden;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_gpro_planea;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cppro_gpro_sgrupo;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_sgrp_depto;
        private Telerik.WinControls.UI.RadButtonElement cppro_sgrp_avance;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_sgrp_evaluacion;
        private Telerik.WinControls.UI.RibbonTab TCPVentas;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpvnt_grp_ventas;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_prodvend;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpvnt_gven_remisionado;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_historial;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_rempart;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Office2010SilverTheme office2010SilverTheme1;
        private Telerik.WinControls.UI.RadButtonElement cpemp_gprd_catalogo;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpemp_grp_producto;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_about;
        private Telerik.WinControls.UI.RadButtonElement dsk_btn_about;
        private Telerik.WinControls.UI.RadMenuItem cppro_beva_eproduccion;
        private Telerik.WinControls.UI.RadMenuItem cppro_beva_eperiodo;
        private Telerik.WinControls.UI.RibbonTab TCPCotizacion;
        private Telerik.WinControls.UI.RadRibbonBarGroup cpcot_grp_cotizacion;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpcot_gcot_productos;
        private Telerik.WinControls.UI.RadMenuItem cpcot_bprd_producto;
        private Telerik.WinControls.UI.RadMenuItem cpcot_bprd_subproducto;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_presupuesto;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_cotizaciones;
        private Telerik.WinControls.UI.RadButtonElement cpcot_gcot_cliente;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpcot_gcot_conf;
        private Telerik.WinControls.UI.RadMenuItem cppro_bord_historial;
        private Telerik.WinControls.UI.RadMenuItem cppro_bord_proceso;
        private Telerik.WinControls.UI.RadDropDownButtonElement cppro_gpro_remision;
        private Telerik.WinControls.UI.RadMenuItem cppro_brem_Emitido;
        private Telerik.WinControls.UI.RadMenuItem cppro_brem_Recibido;
        private Telerik.WinControls.UI.RadButtonElement cppro_gpro_reqcompra;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_brem_porcobrar;
        private System.ComponentModel.BackgroundWorker descargarLogo;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup cpvnt_gven_group1;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_pedidos;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_comision;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_ventcosto;
        private Telerik.WinControls.UI.RadButtonElement cpvnt_gven_vendedor;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_theme;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Black;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Blue;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Silver;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_calendario;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_gantt;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_plan;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_tieadd;
        private Telerik.WinControls.UI.RadMenuItem cppro_beva_eprodof;
        private Telerik.WinControls.UI.RadMenuItem cppro_gpro_plan4;
        private Telerik.WinControls.UI.RibbonTab TCCalidad;
        private Telerik.WinControls.UI.RadRibbonBarGroup ccal_grp_nconf;
        private Telerik.WinControls.UI.RadDropDownButtonElement ccal_gnco_new;
        private Telerik.WinControls.UI.RadDropDownButtonElement ccal_gnco_reporte;
        private Telerik.WinControls.UI.RadRibbonBarGroup ccal_grp_prod;
        private Telerik.WinControls.UI.RadButtonElement ccal_gprd_oprod;
        private Telerik.WinControls.UI.RadButtonElement ccal_gprd_facue;
        private Telerik.WinControls.UI.RadButtonElement ccal_gprd_remis;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_reporte1;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_reporte2;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_reporte3;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_todo;
        private Telerik.WinControls.UI.RadMenuItem ccal_gnco_proceso;
        private Telerik.WinControls.UI.RadButtonElement ppvnt_gdir_clientes;
        private Telerik.WinControls.UI.RadDropDownButtonElement cpvnt_gven_reportes;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_gven_reporte1;
        private Telerik.WinControls.UI.RadMenuItem cpvnt_gven_reporte2;
        private RadMenuItem cppro_bdepto_trabaj;
        private RadMenuItem cppro_bdepto_proces;
        private RadMenuItem dsk_gconfig_emisor;
        private RadButtonElement dsk_btn_manual;
    }
}
