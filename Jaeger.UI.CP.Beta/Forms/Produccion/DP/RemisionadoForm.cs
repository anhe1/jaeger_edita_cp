﻿using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Almacen.DP {
    public class RemisionadoForm : CP.Forms.Almacen.DP.RemisionadoForm {
        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
        }

        public override void OnLoad() {
            this.Service = new RemisionadoInternoService();
            this.ordenProduccionService = new OrdenProduccionService();
        }

    }
}
