﻿using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Produccion {
    internal class OrdenProduccionHistorialForm : CP.Forms.Produccion.OrdenProduccionHistorialForm {
        public OrdenProduccionHistorialForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Produccion: Ordenes Historial";
            this.Load += OrdenProduccionHistorialForm_Load;
        }

        private void OrdenProduccionHistorialForm_Load(object sender, System.EventArgs e) {
            this.TOrden.Service = new OrdenProduccionService();
            this.RemisionService = new Aplication.CP.Service.RemisionadoService();
            this.RemisionInternaService = new Aplication.CP.Service.RemisionadoInternoService();
        }
    }
}
