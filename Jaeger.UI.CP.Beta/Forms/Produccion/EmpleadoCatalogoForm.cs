﻿using System;
using Jaeger.Aplication.CP.Service;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Produccion {
    internal class EmpleadoCatalogoForm : CP.Forms.Produccion.EmpleadoCatalogoForm {

        public EmpleadoCatalogoForm(UIMenuElement menuElement) {
            this.Load += EmpleadoCatalogoForm_Load;
        }

        private void EmpleadoCatalogoForm_Load(object sender, EventArgs e) {
            this.Service = new EmpleadosService();
        }
    }
}
