﻿using Jaeger.Aplication.Cotizador.Services;

namespace Jaeger.UI.CP.Beta.Forms.Produccion {
    internal class DepartamentoPuestosForm : Cotizador.Forms.Produccion.DepartamentoPuestosForm {
        public DepartamentoPuestosForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Load += DepartamentoPuestosForm_Load;
        }

        private void DepartamentoPuestosForm_Load(object sender, System.EventArgs e) {
            this._Service = new DepartamentosService();
        }
    }
}
