﻿using System;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Produccion {
    internal class DepartamentoProcesoForm : UI.Cotizador.Forms.Produccion.DepartamentoProcesoForm {
        public DepartamentoProcesoForm(UIMenuElement menuElement) : base() {
            this.Load += DepartamentoProcesoForm_Load;
        }

        private void DepartamentoProcesoForm_Load(object sender, EventArgs e) {
            this.departamentosService = new DepartamentosService();
        }
    }
}
