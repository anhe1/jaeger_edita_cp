﻿using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Produccion {
    internal class OrdenEnProcesoForm : CP.Forms.Produccion.OrdenEnProcesoForm {
        public OrdenEnProcesoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Producción: Orden en Proceso";
            this.TOrden.ShowPeriodo = false;
            this.TOrden.Cancelar.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        }
    }
}
