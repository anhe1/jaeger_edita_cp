﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Produccion {
    internal class ReqsCompraForm : CP.Forms.Adquisiciones.ReqsCompraForm {
        public ReqsCompraForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += ReqsCompraForm_Load;
        }

        private void ReqsCompraForm_Load(object sender, System.EventArgs e) {
            this.Text = "Prouducción: Req. Compra";
        }
    }
}
