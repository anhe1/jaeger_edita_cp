﻿using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Empresa {
    internal class ClasificacionCatalogoForm : Cotizador.Forms.Empresa.CategoriasForm {
        public ClasificacionCatalogoForm(UIMenuElement menuElement) : base() {
            this.Load += ClasificacionCatalogoForm_Load;
        }

        private void ClasificacionCatalogoForm_Load(object sender, System.EventArgs e) {
            this._Service = new ClasificacionService();
        }
    }
}
