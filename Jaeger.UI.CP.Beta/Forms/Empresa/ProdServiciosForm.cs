﻿using System;
using Jaeger.Aplication.Cotizador.Services;

namespace Jaeger.UI.CP.Beta.Forms.Empresa {
    internal class ProdServiciosForm : UI.Cotizador.Forms.Empresa.ProdServiciosForm {
        public ProdServiciosForm() : base() {
            this.Load += ProdServiciosForm_Load;
        }

        private void ProdServiciosForm_Load(object sender, EventArgs e) {
            this._Service = new ProductoServicioService();
        }
    }
}
