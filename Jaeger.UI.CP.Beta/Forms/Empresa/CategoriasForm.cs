﻿using System;
using Jaeger.Aplication.Cotizador.Services;

namespace Jaeger.UI.CP.Beta.Forms.Empresa {
    internal class CategoriasForm : UI.Cotizador.Forms.Empresa.CategoriasForm {
        public CategoriasForm() : base() {
            this.Load += CategoriasForm_Load;
        }

        private void CategoriasForm_Load(object sender, EventArgs e) {
            this.Text = "Clasificación/ Productos y Servicios";
            this._Service = new ClasificacionService();
        }
    }
}
