﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.WinControls.UI.Localization;
using Jaeger.UI.Common.Builder;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.CP.Beta.Forms {
    public partial class MainRibbonForm : RadRibbonForm {

        public MainRibbonForm() {
            InitializeComponent();
            this.AllowAero = false;
        }

        private void MainRibbonForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.MenuRibbonBar.Visible = false;

            this.descargarLogo = new BackgroundWorker();
            this.descargarLogo.DoWork += DescargarLogo_DoWork;
            this.descargarLogo.RunWorkerCompleted += DescargarLogo_RunWorkerCompleted;
            this.descargarLogo.RunWorkerAsync();

            RadGridLocalizationProvider.CurrentProvider = new LocalizationProviderCustomGrid();
            RadMessageLocalizationProvider.CurrentProvider = new LocalizationProviderCustomerMessageBox();

            using (var espera = new UI.Common.Forms.Waiting1Form(this.SetConfiguration)) {
                espera.Text = "Esperando respuesta del servidor ...";
                espera.ShowDialog(this);
            }

            this.Text = ConfigService.Titulo();
            this.BEstado.Items.Add(new ButtonEmpresaBuilder().Usuario(ConfigService.Piloto.Clave).Version(Application.ProductVersion.ToString())
                .Empresa(ConfigService.Synapsis.Empresa.RFC).DataBase(Aplication.Cotizador.Services.GeneralService.Configuration.DataBase.Database).Build());
            this.MenuRibbonBar.SetDefaultTab();
            this.MenuRibbonBar.Visible = true;
        }

        private void Menu_Main_Click(object sender, EventArgs e) {
            var item = (RadItem)sender;
            var element = ConfigService.GeMenuElement(item.Name);

            if (element != null) {
                var localAssembly = new object() as Assembly;
                var assembly = element.Assembly;
                if (element.Assembly == "Jaeger.UI.CP.Beta" | element.Assembly == "Jaeger.UI" | string.IsNullOrEmpty(element.Assembly)) {
                    assembly = "Jaeger.UI.CP.Beta";
                    localAssembly = this.GetAssembly("CP.Beta");
                } else {
                    assembly = element.Assembly;
                    localAssembly = this.GetAssembly(element.Assembly);
                }

                if (localAssembly != null) {
                    try {
                        Type type = localAssembly.GetType(string.Format("{0}.{1}", assembly, element.Form), true);
                        this.Activar_Form(type, element);
                    } catch (Exception ex) {
                        RadMessageBox.Show(this, "Main: " + ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                } else {
                    RadMessageBox.Show(this, "No se definio ensamblado.", "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private Assembly GetAssembly(string assembly) {
            try {
                return Assembly.Load(assembly);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private void Activar_Form(Type type, UIMenuElement sender) {
            try {
                Form form = (Form)Activator.CreateInstance(type, sender);
                if (form.WindowState == FormWindowState.Maximized) {
                    form.MdiParent = this;
                    form.WindowState = FormWindowState.Maximized;
                    form.Show();
                    this.RadDock.ActivateMdiChild(form);
                } else {
                    form.StartPosition = FormStartPosition.CenterParent;
                    form.ShowDialog(this);
                }
            } catch (Exception ex) {
                RadMessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void AcercaDe_Click(object sender, EventArgs e) {
            var acercade_form = new AboutBoxForm();
            acercade_form.ShowDialog(this);
        }

        private void Menu_UI_AplicarTema_Click(object sender, EventArgs e) {
            this.Change_Theme(((RadButtonElement)sender).Tag.ToString());
        }

        private void Change_Theme(string nameTheme) {
            ThemeResolutionService.ApplicationThemeName = nameTheme;
        }

        private void SetConfiguration() {
            // configura el servicio de configuracion que no utilaremos en la aplicacion
            ConfigService.Synapsis.RDS.CP = null;
            ConfigService.Synapsis.RDS.LiteCP = null;
            var clave = ConfigService.Synapsis.Empresa.Clave;
            var configuracion = new Aplication.CP.Service.ConfiguracionService();
            Aplication.Cotizador.Services.GeneralService.Configuration = configuracion.Get();
            ConfigService.Synapsis.Empresa = configuracion.GetEmpresa();
            ConfigService.Synapsis.Empresa.Clave = clave;
            // configura el servicio de perfil
            Aplication.Kaiju.Contracts.IProfileToService profile = new Aplication.CP.Service.ProfileService();
            ConfigService.Piloto = profile.CreateProfile(ConfigService.Piloto);
            ConfigService.Menus = profile.CreateMenus(ConfigService.Piloto.Id).ToList();

            // configura permisos de menu
            var MenuPermissions = new UIMenuItemPermission(UIPermissionEnum.Invisible);
            MenuPermissions.Load(ConfigService.Menus);
            UIMenuUtility.SetPermission(this, ConfigService.Piloto, MenuPermissions);

            this.MenuRibbonBar.Refresh();
        }

        private void DescargarLogo_DoWork(object sender, DoWorkEventArgs e) {
            string logo = ConfigService.UrlLogo();
            string archivo = ManagerPathService.JaegerPath(PathsEnum.Media, string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            Util.FileService.DownloadFile(logo, archivo);
        }

        private void DescargarLogo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Text = ConfigService.Titulo();
        }
    }
}
