﻿using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CP.Beta.Forms.Herramientas.Configuracion
{
    internal class ConfigurationForm : UI.Empresa.Forms.ConfigurationForm {
        private Telerik.WinControls.UI.RadPageViewPage pageCP = new Telerik.WinControls.UI.RadPageViewPage() { Text = " || CP" };

        public ConfigurationForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Load += ConfigurationForm_Load;
        }

        private void ConfigurationForm_Load(object sender, System.EventArgs e) {
            this.Title.Text = $"|| Configuración de la empresa registrada: {ConfigService.Synapsis.Empresa.RFC}";
            this.SynapsisProperty.SelectedObject = ConfigService.Synapsis;
            this.TabsControl.Pages.Add(pageCP);
            var cpControl = new UI.Cotizador.Forms.Empresa.ConfigurationControl();
            cpControl.SynapsisProperty.SelectedObject = Aplication.Cotizador.Services.GeneralService.Configuration.DataBase;
            cpControl.Folder.Text = Aplication.Cotizador.Services.GeneralService.Configuration.Folder;
            cpControl.ModoProductivo.Checked = Aplication.Cotizador.Services.GeneralService.Configuration.ModoProductivo;
            pageCP.Controls.Add(cpControl);
            this.SynapsisProperty.ReadOnly = true;
        }
    }
}
