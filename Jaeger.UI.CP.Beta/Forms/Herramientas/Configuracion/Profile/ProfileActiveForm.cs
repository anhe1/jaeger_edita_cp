﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Herramientas.Configuracion.Profile {
    internal class ProfileActiveForm : UI.Kaiju.Forms.ProfileActiveForm {
        public ProfileActiveForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.ProfileActiveForm_Load;
        }

        private void ProfileActiveForm_Load(object sender, System.EventArgs e) {
            this.Service = new Aplication.CP.Service.ProfileService();
        }
    }
}
