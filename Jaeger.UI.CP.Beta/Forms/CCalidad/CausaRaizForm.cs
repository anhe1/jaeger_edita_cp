﻿using Jaeger.Aplication.CCalidad.Contracts;
using Jaeger.Domain.CCalidad.Entities;

namespace Jaeger.UI.CP.Beta.Forms.CCalidad {
    internal class CausaRaizForm : UI.CCalidad.Forms.CausaRaizForm {
        public CausaRaizForm(INoConformidadesService service, CausaRaizDetailModel model) : base(service, model) {
            this.Load += CausaRaizForm_Load;
        }

        private void CausaRaizForm_Load(object sender, System.EventArgs e) {
            
        }
    }
}
