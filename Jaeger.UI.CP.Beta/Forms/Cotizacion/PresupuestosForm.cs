﻿using System;
using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Cotizacion {
    internal class PresupuestosForm : UI.Cotizador.Forms.Cotizacion.PresupuestosForm {
        public PresupuestosForm(UIMenuElement menuElement) : base() { this.Load += PresupuestosForm_Load; }

        private void PresupuestosForm_Load(object sender, EventArgs e) {
            this._Service = new PresupuestosService();
        }
    }
}
