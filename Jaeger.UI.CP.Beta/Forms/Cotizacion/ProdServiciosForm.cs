﻿using System;
using Jaeger.Aplication.Cotizador.Services;

namespace Jaeger.UI.CP.Beta.Forms.Cotizacion {
    internal class ProdServiciosForm : UI.Cotizador.Forms.Cotizacion.ProdServiciosForm {
        public ProdServiciosForm() : base() {
            this.Load += ProdServiciosForm_Load;
        }

        private void ProdServiciosForm_Load(object sender, EventArgs e) {
            this.Text = "Productos";
            this._Service = new ProductoServicioCotService();
        }
    }
}
