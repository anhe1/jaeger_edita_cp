﻿using System;
using Jaeger.Aplication.Cotizador.Services;

namespace Jaeger.UI.CP.Beta.Forms.Cotizacion {
    internal class SubProdServiciosForm : UI.Cotizador.Forms.Cotizacion.SubProdServiciosForm {
        public SubProdServiciosForm() : base() {
            this.Load += SubProdServiciosForm_Load;
        }

        private void SubProdServiciosForm_Load(object sender, EventArgs e) {
            this.Text = "Cotización: Sub-Productos";
            this._Service = new ProductoServicioCotService();
        }
    }
}
