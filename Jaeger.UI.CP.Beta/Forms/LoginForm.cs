﻿namespace Jaeger.UI.CP.Beta.Forms {
    /// <summary>
    /// login personalizado
    /// </summary>
    internal class LoginForm : UI.Login.Forms.LoginForm {
        public LoginForm(string[] args) : base(args) {
            this.Text = "CP - Beta";
            this.LogoPicture.Image = Properties.Resources.male_user_96px;
            this.Icon = Properties.Resources.male_user;
            this.LogoPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoPicture.Padding = new System.Windows.Forms.Padding(20, 20, 20, 20);
            this.LogoPicture.ClientSize = new System.Drawing.Size(120, 120);
            this.LogoPicture.Location = new System.Drawing.Point(90, 50);
            this.lblInformacion.Text = "CP - Beta";
        }
    }
}
