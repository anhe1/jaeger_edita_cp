﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.CP.Beta.Forms.Ventas {
    public class RemisionadoForm : UI.Almacen.PT.Forms.RemisionadoForm {
        protected internal RadMenuItem PorPartida = new RadMenuItem { Text = "Por Partidas", Name = "tadm_gcli_remxpartida" };

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = true;
            this.Text = "Ventas: Remisionado";
            this.Load += RemisionadoForm_Load;
        }

        private void RemisionadoForm_Load(object sender, EventArgs e) {
            this._Service = new Jaeger.Aplication.CP.Service.RemisionadoService();
        }

        #region barra de herramientas
        public override void TRemision_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new CP.Forms.Almacen.PT.RemisionFiscalForm(this._Service) { MdiParent = ParentForm };
            _nuevo.Show();
        }

        public override void TImprimir_Comprobante_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado.Version == "1.0") {
                using (var espera = new Waiting1Form(this.Imprimir)) {
                    espera.Text = "Imprimiendo versión anterior, espere ...";
                    espera.Show(this);
                }
            } else {
                base.TImprimir_Comprobante_Click(sender, e);
            }
        }

        public override void TRemision_PorPartida_Click(object sender, EventArgs e) {
            var porPartida = new CP.Forms.Almacen.PT.RemisionadoPartidaForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm, Text = this.PorPartida.Text };
            porPartida.Show();
        }

        private void Imprimir() {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            Aplication.CP.Service.RemisionadoService.InformesHTML(seleccionado.Folio.ToString());
        }
        #endregion
    }
}
