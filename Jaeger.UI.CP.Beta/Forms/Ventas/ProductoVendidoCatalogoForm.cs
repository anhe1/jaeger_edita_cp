﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Ventas {
    internal class ProductoVendidoCatalogoForm : CP.Forms.Ventas.ProductosVendidosForm {
        public ProductoVendidoCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Ventas: Productos Vendidos";
        }
    }
}
