﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Almacen.PT {
    public class RemisionadoForm : UI.CP.Forms.Almacen.PT.RemisionadoForm {

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = false;
            this.Text = "Almacén PT: Remisionado";
            this.Load += RemisionadoForm_Load;
        }

        private void RemisionadoForm_Load(object sender, EventArgs e) {
            this._Service = new Aplication.CP.Service.RemisionadoService();
        }
    }
}
