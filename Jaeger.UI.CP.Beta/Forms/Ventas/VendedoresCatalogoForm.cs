﻿using Jaeger.Aplication.Cotizador.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CP.Beta.Forms.Ventas {
    public class VendedoresCatalogoForm : Cotizador.Forms.Ventas.VendedoresCatalogoForm {
        public VendedoresCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += VendedoresCatalogoForm_Load;
        }

        private void VendedoresCatalogoForm_Load(object sender, System.EventArgs e) {
            this.Text = "Ventas: Vendedores";
            this.Service = new VendedorService();
        }
    }
}
