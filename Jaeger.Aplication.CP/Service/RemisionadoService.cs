﻿/// develop: ANHE1 27092020 01:41
using System.Collections.Generic;
using System.Diagnostics;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.CP.Service {
    public partial class RemisionadoService : RemisionService, Almacen.PT.Contracts.IRemisionadoService {
       
        public RemisionadoService() : base() {
            
        }
      
        /// <summary>
        /// Launch the application with some options set.
        /// </summary>
        public static void InformesHTML(string folio) {
            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo {
                CreateNoWindow = false,
                UseShellExecute = false,
                FileName = @"\\192.168.1.249\CP\bin\Informes_HTML.exe",
                WindowStyle = ProcessWindowStyle.Normal,
                Arguments = string.Format("\\\\192.168.1.249\\CP\\Informes\\Remision.htm;\\\\192.168.1.249\\cp\\cp_informes\\Remision\\Remision-{0}.htm;{1};CP_FB;1", folio, folio)
            };

            try {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo)) {
                    exeProcess.WaitForExit();
                }
            } catch {
                // Log error.
            }
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(Domain.Almacen.PT.Entities.RemisionReceptorModel))
                return this.remisionRepository.GetListR<T1>(conditionals);
            return this.remisionRepository.GetList<T1>(conditionals);
        }
    }
}
