﻿using Jaeger.Aplication.Empresa.Contracts;
using Jaeger.Domain.Cotizador.Empresa.Contracts;

namespace Jaeger.Aplication.CP.Service {
    public class ConfiguracionService : Cotizador.Services.ConfigurationService, IConfigurationService, Cotizador.Contracts.IConfigurationService, Contracts.IConfigurationService {
        #region declaraciones
        protected internal Domain.Empresa.Contracts.ISqlParametroRepository LocalParametrosRepository;
        #endregion

        public ConfiguracionService() : base() {
        }

        public override IConfiguration Get() {
            var parametros = this.Get(Domain.Empresa.Enums.ConfigGroupEnum.CP);
            var configuration = new Cotizador.Builder.ConfigurationBuilder().Build(parametros);
            return (IConfiguration)configuration;
        }

        public override IConfiguration Set(IConfiguration configuration) {
            var parameters = new Cotizador.Builder.ConfigurationBuilder().Build(configuration);
            this.Set(parameters);
            return configuration;
        }
    }
}
