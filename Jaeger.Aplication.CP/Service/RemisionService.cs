﻿/// develop: ANHE1 27092020 01:41
using Jaeger.Aplication.Base.Services;
using Jaeger.DataAccess.FB.Almacen.PT.Repositories;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Cotizador.Empresa.Entities;
using Jaeger.Domain.Cotizador.Produccion.Entities;

namespace Jaeger.Aplication.CP.Service {
    public class RemisionService : Almacen.PT.Services.RemisionService, Almacen.PT.Contracts.IRemisionService {
        protected ISqlContribuyenteRepository contribuyenteRepository;

        public RemisionService() : base() {
            this.remisionRepository = new SqlFbRemisionadoRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.movimientoAlmacenPT = new SqlFbMovimientoAlmacenRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.relacionesRepository = new SqlFbRemisionadoRRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.statusRepository = new SqlFbRemisionStatusRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.contribuyenteRepository = new DataAccess.FB.Contribuyentes.Repositories.SqlFbContribuyenteRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public override int DiasCredito(int idCliente) {
            var d = this.contribuyenteRepository.GetById<ContribuyenteModel>(idCliente);
            if (d != null) {
                return d.DiasCredito;
            }
            return 0;
        }

        public static RemisionConceptoDetailModel Convertir(OrdenProduccionDetailModel ordenProduccion) {
            var concepto = new RemisionConceptoDetailModel {
                IdProducto = ordenProduccion.PEDPRD_LP_ID,
                IdPedido = ordenProduccion.IdOrden,
                Identificador = string.Format("CP-{0}", ordenProduccion.IdOrden.ToString("#000000")),
                IdModelo = ordenProduccion.IdOrden,
                Activo = true,
                Catalogo = "",
                Especificacion = "",
                IdEspecificacion = 0,
                IdUnidad = 0,
                Marca = "",
                Modelo = ordenProduccion.Nombre,
                Producto = "",
                Tamanio = "",
                TasaIVA = 0,
                Unidad = ordenProduccion.Unidad.ToString(),
                UnidadFactor = ordenProduccion.Unidad,
                ValorUnitario = ordenProduccion.ValorUnitario,
                Unitario = ordenProduccion.ValorUnitario,
                Descuento = 0,
                Tipo = Domain.Almacen.PT.ValueObjects.TipoComprobanteEnum.Remision,
                Efecto = Domain.Almacen.ValueObjects.TipoMovimientoEnum.Egreso,
                Cantidad = ordenProduccion.Cantidad - ordenProduccion.CantidadRemisionado
            };
            // la diferencia
            if (ordenProduccion.CantidadRemisionado > ordenProduccion.Cantidad) {
                concepto.Cantidad = 0; // sino cero
            }

            return concepto;
        }
    }
}
