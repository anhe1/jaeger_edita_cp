﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Aplication.CP.Service {
    public class RemisionadoInternoService : RemisionInternaService, Almacen.DP.Contracts.IRemisionadoService {
        
        public RemisionadoInternoService() : base() {
        }

        public BindingList<T1> GetList<T1>(int year, int month = 0) where T1 : class, new() {
            var condiciones = new List<Conditional>() {
                new Conditional("EXTRACT(YEAR FROM RMSDP_FECEMS)", year.ToString()),
                new Conditional("RMSDP_CTDEP_ID", this._IdDepartamento.ToString()),
                new Conditional("RMSDP_A", "1")
            };

            if (month > 0) {
                condiciones.Add(new Conditional("EXTRACT(MONTH FROM RMSDP_FECEMS)", month.ToString()));
            }

            return new BindingList<T1>(this.remisionadoRepository.GetList<T1>(condiciones).ToList());
        }

        public BindingList<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return new BindingList<T1>(this.remisionadoRepository.GetList<T1>(conditionals).ToList());
        }
    }
}
