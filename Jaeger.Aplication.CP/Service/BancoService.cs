﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FB.Banco.Repositories;

namespace Jaeger.Aplication.CP.Service {
    public class BancoService : Aplication.Banco.BancoService, IBancoService, IBancoCuentaService {

        public BancoService() : base() { }

        public override void OnLoad() {
            this.movimientoBancarioRepository = new SqlFbMovimientoBancarioRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.formaPagoRepository = new SqlFbBancoFormaPagoRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.auxiliarRepository= new SqlFbMovimientoAuxiliarRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.conceptoRepository = new SqlFbMovimientoConceptoRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.s3 = new EditaBucketService();
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(MovimientoConceptoDetailModel)) {
                return this.conceptoRepository.GetList<T1>(conditionals).ToList();
            }
            return this.movimientoBancarioRepository.GetList<T1>(conditionals);
        }

        /// <summary>
        /// almacenar movmiento bancario
        /// </summary>
        public new IMovimientoBancarioDetailModel Save(IMovimientoBancarioDetailModel model) {
            if (model.Id == 0) {
                model.Creo = ConfigService.Piloto.Clave;
                model.FechaNuevo = DateTime.Now;
            }
            else {
                model.Modifica = ConfigService.Piloto.Clave;
                model.FechaModifica = DateTime.Now;
            }

            for (int i = 0; i < model.Comprobantes.Count; i++) {
                if (model.Comprobantes[i].Id == 0)
                    model.Comprobantes[i].Creo = model.Creo;
                else
                    model.Comprobantes[i].Creo = ConfigService.Piloto.Clave;
            }

            for (int i = 0; i < model.Auxiliar.Count; i++) {
                if (model.Auxiliar[i].IdAuxiliar == 0) {
                    model.Auxiliar[i].Tipo = this.s3.ContenType(model.Auxiliar[i].FileName);
                    model.Auxiliar[i].Creo = ConfigService.Piloto.Clave;
                }
            }

            // primero almacenamos el movimiento
            model = this.movimientoBancarioRepository.Save(model);

            // y despues si existen auxiliares los almacenamos con los datos necesarios
            for (int i = 0; i < model.Auxiliar.Count; i++) {
                if (model.Auxiliar[i].IdAuxiliar > 0) {
                    model.Auxiliar[i].URL = this.s3.Upload(model.Auxiliar[i].Base64, model.Auxiliar[i].FileName, string.Format("{0}-{1}{2}", model.Identificador, this.CreateGuid(new string[] { model.Auxiliar[i].Base64 }), System.IO.Path.GetExtension(model.Auxiliar[i].FileName)).ToLower());
                    if (ValidacionService.URL(model.Auxiliar[i].URL)) {
                        model.Auxiliar[i].Tipo = this.s3.ContenType(model.Auxiliar[i].FileName);
                        model.Auxiliar[i].Creo = ConfigService.Piloto.Clave;
                        this.auxiliarRepository.Update(model.Auxiliar[i]);
                    }
                }
            }

            if (model.TipoOperacion == BancoTipoOperacionEnum.TransferenciaEntreCuentas) {
                if (model.Id > 0) {
                    var model2 = this.TransferenciaEntreCuentas(model);
                    model2 = this.movimientoBancarioRepository.Save(model2);
                }
            }
            return model;
        }

        /// <summary>
        /// Crear movimiento entre cuentas
        /// </summary>
        private IMovimientoBancarioDetailModel TransferenciaEntreCuentas(IMovimientoBancarioDetailModel model1) {
            var model2 = new MovimientoBancarioDetailModel {
                IdConcepto = model1.IdConcepto,
                Abono = model1.Cargo,
                Cargo = model1.Abono,
                Activo = true,
                FechaNuevo = model1.FechaNuevo,
                Creo = model1.Creo,
                Concepto = model1.Concepto,
                IdCuentaP = model1.IdCuentaT,
                IdCuentaT = model1.IdCuentaP,
                BancoP = model1.BancoT,
                BancoT = model1.BancoP,
                BeneficiarioP = model1.BeneficiarioT,
                BeneficiarioT = model1.BeneficiarioP,
                BeneficiarioRFCP = model1.BeneficiarioRFCT,
                BeneficiarioRFCT = model1.BeneficiarioRFCP,
                ClaveBancoP = model1.ClaveBancoT,
                ClaveBancoT = model1.ClaveBancoP,
                ClaveFormaPago = model1.ClaveFormaPago,
                ClaveMoneda = model1.ClaveMoneda,
                CuentaCLABEP = model1.CuentaCLABET,
                CuentaCLABET = model1.CuentaCLABEP,
                FechaAplicacion = model1.FechaAplicacion,
                FechaBoveda = model1.FechaBoveda,
                FechaCancela = model1.FechaCancela,
                FechaDocto = model1.FechaDocto,
                FechaEmision = model1.FechaEmision,
                FechaModifica = model1.FechaModifica,
                FechaVence = model1.FechaVence,
                FormaPagoDescripcion = model1.FormaPagoDescripcion,
                Modifica = model1.Modifica,
                Nota = model1.Nota,
                Autoriza = model1.Autoriza,
                NumAutorizacion = model1.NumAutorizacion,
                NumDocto = model1.NumDocto,
                NumeroCuentaP = model1.NumeroCuentaT,
                NumeroCuentaT = model1.NumeroCuentaP,
                NumOperacion = model1.NumOperacion,
                Referencia = model1.Referencia,
                ReferenciaNumerica = model1.ReferenciaNumerica,
                IdStatus = model1.IdStatus,
                SucursalP = model1.SucursalT,
                SucursalT = model1.SucursalP,
                Tipo = MovimientoBancarioEfectoEnum.Ingreso,
                TipoCambio = model1.TipoCambio,
                IdTipoOperacion = model1.IdTipoOperacion,
                Cancela = model1.Cancela
            };

            return model2;
        }

        /// <summary>
        /// 
        /// </summary>
        public string CreateGuid(string[] datos) {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }
    }
}