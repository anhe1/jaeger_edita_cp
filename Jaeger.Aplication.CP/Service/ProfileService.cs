﻿using System.Collections.Generic;
using System.Linq;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.CP.Service {
    /// <summary>
    /// servicio de perfiles de sistema, implementacion para version beta
    /// </summary>
    public class ProfileService : Kaiju.Services.ProfileService, Kaiju.Contracts.IProfileService, Base.Contracts.IProfileService {
        /// <summary>
        /// constructor
        /// </summary>
        public ProfileService() : base() { }

        /// <summary>
        /// carga de repositorios basicos
        /// </summary>
        protected override void OnLoad() {
            base.OnLoad();
            this._ProfileRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUIProfileRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this._UserRolRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUserRolRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this._RelacionRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUserRolRelacionRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// crear menu del sistema
        /// </summary>
        public override List<Domain.Base.Abstractions.UIMenuElement> GetMenus() {
            return this._MenuRepository.GetCPBeta().ToList();
        }
    }
}
