﻿using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.CP.Service {
    public class NoConformidadesService : CCalidad.Services.NoConformidadesService, CCalidad.Contracts.INoConformidadesService, CCalidad.Contracts.INoConformidadService {
        public NoConformidadesService() : base() {
            this._repository = new DataAccess.FB.CCalidad.Repositories.SqlFbNoConformidadRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.sqlAccionCorrectivaRepository = new DataAccess.FB.CCalidad.Repositories.SqlFbAccionCorrectivaRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.sqlAccionPreventivaRepository = new DataAccess.FB.CCalidad.Repositories.SqlFbAccionPreventivaRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.sqlCausaRaizRepository = new DataAccess.FB.CCalidad.Repositories.SqlFbCausaRaizRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.sqlOrdenProduccionRepository = new DataAccess.FB.CCalidad.Repositories.SqlFbOrdenProduccionRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.sqlCostoNoCalidadRepository = new DataAccess.FB.CCalidad.Repositories.SqlFbCostoNoCalidadRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.sqlMediaRepository = new DataAccess.FB.CCalidad.Repositories.SqlFbMediaRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.s3 = new Base.Services.EditaBucketService();
            this.s3.Folder = "noconformidad";
        }
    }
}
