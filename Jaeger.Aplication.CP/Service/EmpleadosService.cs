﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.DataAccess.Nomina.Adapter.Repositories;

namespace Jaeger.Aplication.CP.Service {
    /// <summary>
    /// servicio para empleados
    /// </summary>
    public class EmpleadosService {
        #region declaraciones
        protected ISqlEmpleadoRepository empleadoRepository;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public EmpleadosService() {
            this.empleadoRepository = new SqlEmpleadoRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public bool Exists(IEmpleadoModel model) {
            var existe = this.empleadoRepository.GetList<EmpleadoModel>(new List<IConditional> { new Conditional("CTEMP_CLV", model.Clave) }).ToList();
            return existe.Count > 0;
        }

        public EmpleadoModel Save(EmpleadoModel model) {
            return (EmpleadoModel)this.empleadoRepository.Save((IEmpleadoModel)model);
        }

        /// <summary>
        /// obtener lista de objetos
        /// </summary>
        /// <typeparam name="T1">modelo</typeparam>
        /// <param name="conditionals">condiciones</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.empleadoRepository.GetList<T1>(conditionals);
        }
    }
}
