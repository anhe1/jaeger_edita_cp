﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Almacen.DP.Contracts;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Cotizador.Empresa.Contracts;
using Jaeger.DataAccess.Cotizador.Repositories;
using Jaeger.DataAccess.FB.Almacen.DP.Repositories;

namespace Jaeger.Aplication.CP.Service {
    public class RemisionInternaService : Almacen.DP.Services.RemisionService, Almacen.DP.Contracts.IRemisionService {
        protected ISqlRemisionadoRepository remisionadoRepository;
        protected ISqlRemisionPartidaRepository partidaRepository;
        protected ISqlDepartamentoRepository departamentoRepository;
        protected internal int _IdDepartamento = 8;

        public RemisionInternaService() {
            this.remisionadoRepository = new SqlFbRemisionadoRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.partidaRepository = new SqlFbRemisionPartidaRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.departamentoRepository = new SqlFbDepartamentoRepository(Cotizador.Services.GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public RemisionDetailModel GetNew() {
            return new RemisionDetailModel() { IdDepartamento = this._IdDepartamento, FechaEmision = DateTime.Now, Creo = ConfigService.Piloto.Clave };
        }

        public RemisionDetailModel GetRemision(int index) {
            var response = this.remisionadoRepository.GetById<RemisionDetailModel>(index);
            if (response != null) {
                var partidas = this.GetPartidas(index);

                if (partidas != null) {
                    response.Conceptos = new BindingList<RemisionConceptoDetailModel>(partidas);
                }
            }
            return response;
        }

        public RemisionDetailModel Save(RemisionDetailModel model) {
            model = this.remisionadoRepository.Save(model);
            if (model.IdRemision > 0) {
                if (model.Conceptos != null) {
                    for (int i = 0; i < model.Conceptos.Count; i++) {
                        model.Conceptos[i].IdRemision = model.IdRemision;
                        model.Conceptos[i] = this.partidaRepository.Save(model.Conceptos[i]);
                    }
                }
            }
            return model;
        }

        public RemisionDetailPrinter GetPrinter(int index) {
            var response = this.GetRemision(index);
            if (response != null) {
                var printer = new RemisionDetailPrinter(response);
                var lengua = this.departamentoRepository.GetById(response.IdDepartamento);
                printer.DepartamentoText = lengua.Descriptor;
                printer.MetodoEnvioText = GetMetodoEnvio(printer.IdMetodoEnvio).Descriptor;
                return printer;
            }
            return null;
        }

        public bool Cancelar(RemisionCancelacionModel model) {
            return this.remisionadoRepository.Cancelar(model);
        }

        public bool SetStatus(RemisionStatusModel model) {
            return this.remisionadoRepository.SetStatus(model);
        }

        public BindingList<RemisionConceptoDetailModel> GetPartidas(int index) {
            return new BindingList<RemisionConceptoDetailModel>(this.partidaRepository.GetList<RemisionConceptoDetailModel>(new List<Conditional> {
                    new Conditional("MVADP_RMSN_ID", index.ToString()),
                    new Conditional("MVADP_A", "1")
                }).ToList());
        }
    }
}
